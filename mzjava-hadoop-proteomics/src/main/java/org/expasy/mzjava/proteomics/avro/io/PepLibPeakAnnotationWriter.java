package org.expasy.mzjava.proteomics.avro.io;

import com.google.common.collect.Lists;
import org.apache.avro.Schema;
import org.apache.avro.io.Encoder;
import org.expasy.mzjava.avro.io.AbstractLibPeakAnnotationWriter;
import org.expasy.mzjava.avro.io.AvroWriter;
import org.expasy.mzjava.proteomics.ms.spectrum.PepLibPeakAnnotation;
import org.openide.util.lookup.ServiceProvider;

import java.io.IOException;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
@ServiceProvider(service=AvroWriter.class)
public class PepLibPeakAnnotationWriter extends AbstractLibPeakAnnotationWriter<PepLibPeakAnnotation> {

    private final PepFragAnnotationWriter pepFragAnnotationWriter = new PepFragAnnotationWriter();

    @Override
    public Class getObjectClass() {

        return PepLibPeakAnnotation.class;
    }

    @Override
    public void write(PepLibPeakAnnotation annotation, Encoder out) throws IOException {

        super.writeLibAnnotation(annotation, out);
        writeOptional(pepFragAnnotationWriter, annotation.getOptFragmentAnnotation(), out);
    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {

        super.createRecordFields(fields);
        fields.add(createSchemaField("fragAnnotation", Schema.createUnion(Lists.newArrayList(pepFragAnnotationWriter.createSchema(), Schema.create(Schema.Type.NULL)))));
    }
}
