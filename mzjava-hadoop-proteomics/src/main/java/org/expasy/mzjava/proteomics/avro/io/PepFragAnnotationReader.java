/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.avro.io;

import org.apache.avro.Schema;
import org.apache.avro.io.Decoder;
import org.expasy.mzjava.avro.io.AbstractAvroReader;
import org.expasy.mzjava.avro.io.AvroReader;
import org.expasy.mzjava.avro.io.CompositionReader;
import org.expasy.mzjava.avro.io.NumericMassReader;
import org.expasy.mzjava.core.mol.Mass;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.proteomics.mol.PeptideFragment;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.proteomics.ms.spectrum.PepFragAnnotation;
import org.openide.util.lookup.ServiceProvider;

import java.io.IOException;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
@ServiceProvider(service=AvroReader.class)
public class PepFragAnnotationReader extends AbstractAvroReader<PepFragAnnotation> {

    private final PeptideFragmentReader peptideReader = new PeptideFragmentReader();
    private final CompositionReader compositionReader = new CompositionReader();

    public PepFragAnnotationReader() {

        registerUnion(Mass.class, new NumericMassReader(), new CompositionReader());
    }

    @Override
    public Class getObjectClass() {

        return PepFragAnnotation.class;
    }

    @Override
    public PepFragAnnotation read(Decoder in) throws IOException {

        IonType ionType = IonType.values()[in.readEnum()];
        Composition isotopeComposition = compositionReader.read(in);
        int charge = in.readInt();
        PeptideFragment peptideFragment = peptideReader.read(in);
        Mass neutralLoss = readUnion(Mass.class, in);

        return new PepFragAnnotation.Builder(ionType, charge, peptideFragment).addIsotopeComposition(isotopeComposition).setNeutralLoss(neutralLoss).build();
    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {

        fields.add(createSchemaField("ionType", createEnumSchema(IonType.class, IonType.values())));
        fields.add(createSchemaField("isotopeComposition", compositionReader.createSchema()));
        fields.add(createSchemaField("charge", Schema.create(Schema.Type.INT)));
        fields.add(createSchemaField("peptideFragment", peptideReader.createSchema()));
        fields.add(createSchemaField("neutralLoss", createUnionSchema(Mass.class)));
    }
}
