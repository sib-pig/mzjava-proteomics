/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.avro.io;

import com.google.common.base.Optional;
import org.apache.avro.Schema;
import org.apache.avro.io.Encoder;
import org.expasy.mzjava.avro.io.AbstractAvroWriter;
import org.expasy.mzjava.avro.io.AbstractPeakListWriter;
import org.expasy.mzjava.avro.io.AvroWriter;
import org.expasy.mzjava.avro.io.PeakWriter;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.proteomics.ms.spectrum.PeptideSpectrum;
import org.openide.util.lookup.ServiceProvider;

import java.io.IOException;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
@ServiceProvider(service=AvroWriter.class)
public class PeptideSpectrumWriter extends AbstractPeakListWriter<PeptideSpectrum> {

    private final PeakWriter peakWriter = new PeakWriter();
    private final PeptideWriter peptideWriter = new PeptideWriter();

    public PeptideSpectrumWriter() {

        super(Optional.<PeakList.Precision>absent(), new PepFragAnnotationWriter());
    }

    public PeptideSpectrumWriter(Optional<PeakList.Precision> precisionOverride) {

        super(precisionOverride, new PepFragAnnotationWriter());
    }

    @Override
    public Class getObjectClass() {

        return PeptideSpectrum.class;
    }

    @Override
    public void write(PeptideSpectrum spectrum, Encoder out) throws IOException {

        peakWriter.write(spectrum.getPrecursor(), out);
        out.writeInt(spectrum.getMsLevel());
        out.writeInt(spectrum.getCharge());

        peptideWriter.write(spectrum.getPeptide(), out);
        writeStringArray(spectrum.getProteinAccessionNumbers(), out);

        super.writePeakList(spectrum, out);
    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {

        fields.add(createSchemaField("precursor", peakWriter.createSchema()));
        fields.add(createSchemaField("msLevel", Schema.create(Schema.Type.INT)));
        fields.add(createSchemaField("charge", Schema.create(Schema.Type.INT)));

        fields.add(createSchemaField("peptide", peptideWriter.createSchema()));
        fields.add(createSchemaField("proteinAccessionNumbers", Schema.createArray(Schema.create(Schema.Type.STRING))));

        super.createRecordFields(fields);
    }
}
