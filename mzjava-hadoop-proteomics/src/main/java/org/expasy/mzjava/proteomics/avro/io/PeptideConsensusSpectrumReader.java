/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.avro.io;

import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import org.apache.avro.Schema;
import org.apache.avro.io.Decoder;
import org.expasy.mzjava.avro.io.*;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessor;
import org.expasy.mzjava.core.ms.spectrum.RetentionTimeDiscrete;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;
import org.expasy.mzjava.proteomics.ms.spectrum.PepLibPeakAnnotation;
import org.openide.util.lookup.ServiceProvider;

import java.io.IOException;
import java.net.URI;
import java.util.*;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
@ServiceProvider(service=AvroReader.class)
public class PeptideConsensusSpectrumReader extends AbstractPeakListReader<PepLibPeakAnnotation, PeptideConsensusSpectrum> {

    protected final PeakReader precursorReader = new PeakReader();
    protected final PeptideReader peptideReader = new PeptideReader();
    protected final UUIDReader uuidReader = new UUIDReader();
    protected final RetentionTimeDiscreteReader rtReader = new RetentionTimeDiscreteReader();

    private Peptide peptide;

    public PeptideConsensusSpectrumReader() {

        this(Optional.<PeakList.Precision>absent(), Collections.EMPTY_LIST);
    }

    public PeptideConsensusSpectrumReader(Optional<PeakList.Precision> precisionOverride, List<PeakProcessor<PepLibPeakAnnotation, PepLibPeakAnnotation>> peakProcessors) {

        super(new PepLibPeakAnnotationReader[]{new PepLibPeakAnnotationReader()}, precisionOverride, peakProcessors);
    }

    @Override
    public Class getObjectClass() {

        return PeptideConsensusSpectrum.class;
    }

    @Override
    protected PeptideConsensusSpectrum newPeakList(PeakList.Precision precision, double constantIntensity) {

        return new PeptideConsensusSpectrum(peptide, precision, new HashSet<UUID>());
    }

    @Override
    public PeptideConsensusSpectrum read(Decoder in) throws IOException {

        Peak precursor = precursorReader.read(in);
        int msLevel = in.readInt();
        String name = in.readString();
        double simScoreMean = in.readDouble();
        double simScoreStdev = in.readDouble();
        double precursorMzMean = in.readDouble();
        double precursorMzStdev = in.readDouble();
        List<UUID> memberIds = readArray(uuidReader, new ArrayList<UUID>(), in);
        peptide = peptideReader.read(in);
        List<String> accessionNumbers = readStringArray(new ArrayList<String>(), in);
        URI source = URI.create(in.readString());
        String comment = in.readString();
        PeptideConsensusSpectrum.Status status = PeptideConsensusSpectrum.Status.values()[in.readEnum()];

        Optional<RetentionTimeDiscrete> retentionTime = readOptional(rtReader, in);

        PeptideConsensusSpectrum spectrum = super.read(in);

        spectrum.setPrecursor(precursor);
        spectrum.setMsLevel(msLevel);
        spectrum.setName(name);
        spectrum.setScoreStats(simScoreMean, simScoreStdev);
        spectrum.setPrecursorStats(precursorMzMean, precursorMzStdev);
        spectrum.addMemberIds(memberIds);
        spectrum.setPeptide(peptide);
        spectrum.addProteinAccessionNumbers(accessionNumbers);
        spectrum.setSpectrumSource(source);
        spectrum.setComment(comment);
        spectrum.setStatus(status);
        spectrum.setRetentionTime(retentionTime);

        return spectrum;
    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {

        fields.add(createSchemaField("precursor", precursorReader.createSchema()));
        fields.add(createSchemaField("msLevel", Schema.create(Schema.Type.INT)));
        fields.add(createSchemaField("name", Schema.create(Schema.Type.STRING)));
        fields.add(createSchemaField("simScoreMean", Schema.create(Schema.Type.DOUBLE)));
        fields.add(createSchemaField("simScoreStdev", Schema.create(Schema.Type.DOUBLE)));
        fields.add(createSchemaField("precursorMzMean", Schema.create(Schema.Type.DOUBLE)));
        fields.add(createSchemaField("precursorMzStdev", Schema.create(Schema.Type.DOUBLE)));
        fields.add(createSchemaField("memberIds", Schema.createArray(uuidReader.createSchema())));
        fields.add(createSchemaField("peptide", peptideReader.createSchema()));
        fields.add(createSchemaField("proteinAccessionNumbers", Schema.createArray(Schema.create(Schema.Type.STRING))));
        fields.add(createSchemaField("source", Schema.create(Schema.Type.STRING)));
        fields.add(createSchemaField("comment", Schema.create(Schema.Type.STRING)));
        fields.add(createSchemaField("status", createEnumSchema(PeptideConsensusSpectrum.Status.class, PeptideConsensusSpectrum.Status.values())));
        fields.add(createSchemaField("retentionTime", Schema.createUnion(Lists.newArrayList(rtReader.createSchema(),Schema.create(Schema.Type.NULL)))));

        super.createRecordFields(fields);
    }
}
