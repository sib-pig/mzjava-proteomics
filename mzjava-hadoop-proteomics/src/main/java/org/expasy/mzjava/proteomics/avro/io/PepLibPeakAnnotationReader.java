package org.expasy.mzjava.proteomics.avro.io;

import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import org.apache.avro.Schema;
import org.apache.avro.io.Decoder;
import org.expasy.mzjava.avro.io.AbstractAvroReader;
import org.expasy.mzjava.avro.io.AvroReader;
import org.expasy.mzjava.proteomics.ms.spectrum.PepFragAnnotation;
import org.expasy.mzjava.proteomics.ms.spectrum.PepLibPeakAnnotation;
import org.openide.util.lookup.ServiceProvider;

import java.io.IOException;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
@ServiceProvider(service=AvroReader.class)
public class PepLibPeakAnnotationReader extends AbstractAvroReader<PepLibPeakAnnotation> {

    private final PepFragAnnotationReader pepFragAnnotationReader = new PepFragAnnotationReader();

    @Override
    public Class getObjectClass() {

        return PepLibPeakAnnotation.class;
    }

    @Override
    public PepLibPeakAnnotation read(Decoder in) throws IOException {

        int count = in.readInt();
        double mzStd = in.readDouble();
        double intensityStd = in.readDouble();
        Optional<PepFragAnnotation> optAnnotation = readOptional(pepFragAnnotationReader, in);

        return new PepLibPeakAnnotation(count, mzStd, intensityStd, optAnnotation);
    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {

        fields.add(createSchemaField("mergedPeakCount", Schema.create(Schema.Type.INT)));
        fields.add(createSchemaField("mzStd", Schema.create(Schema.Type.DOUBLE)));
        fields.add(createSchemaField("intensityStd", Schema.create(Schema.Type.DOUBLE)));
        fields.add(createSchemaField("fragAnnotation", Schema.createUnion(Lists.newArrayList(pepFragAnnotationReader.createSchema(), Schema.create(Schema.Type.NULL)))));
    }
}
