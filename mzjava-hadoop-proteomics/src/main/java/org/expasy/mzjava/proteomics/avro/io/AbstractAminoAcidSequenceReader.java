/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.avro.io;

import org.apache.avro.Schema;
import org.apache.avro.io.Decoder;
import org.expasy.mzjava.avro.io.AbstractAvroReader;
import org.expasy.mzjava.proteomics.avro.io.ModificationReader;
import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.proteomics.mol.AminoAcidSequence;
import org.expasy.mzjava.proteomics.mol.PeptideBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public abstract class AbstractAminoAcidSequenceReader<O extends AminoAcidSequence> extends AbstractAvroReader<O> {

    private final ModificationReader modificationReader = new ModificationReader();

    public void readSequence(PeptideBuilder builder, Decoder in) throws IOException {

        AminoAcid[] aminoAcids = AminoAcid.values();
        for(long i = in.readArrayStart(); i != 0; i = in.arrayNext()) {
            for (long j = 0; j < i; j++) {

                builder.add(aminoAcids[in.readEnum()]);
            }
        }

        for(long i = in.readArrayStart(); i != 0; i = in.arrayNext()) {
            for (long j = 0; j < i; j++) {

                builder.addModification(in.readInt(), modificationReader.read(in));
            }
        }

        ModAttachment[] attachments = ModAttachment.values();
        for(long i = in.readArrayStart(); i != 0; i = in.arrayNext()) {
            for (long j = 0; j < i; j++) {

                builder.addModification(attachments[in.readEnum()], modificationReader.read(in));
            }
        }
    }

    protected void addSequenceFields(List<Schema.Field> fields) {

        fields.add(createSchemaField("seq", Schema.createArray(createEnumSchema(AminoAcid.class, AminoAcid.values()))));

        fields.add(createSchemaField("sideChainModMap", Schema.createArray(
                createSideChainModRecordSchema()
        )));

        fields.add(createSchemaField("termModMap", Schema.createArray(
                createTermModRecordSchema()
        )));
    }

    private Schema createSideChainModRecordSchema() {

        Schema schema = Schema.createRecord("SideChainModRecord", null, rewriteNameSpace(Modification.class), false);

        List<Schema.Field> fields = new ArrayList<>(2);
        fields.add(new Schema.Field("index", Schema.create(Schema.Type.INT), null, null));
        fields.add(new Schema.Field("modification", modificationReader.createSchema(), null, null));

        schema.setFields(fields);

        return schema;
    }

    private Schema createTermModRecordSchema() {

        Schema schema = Schema.createRecord("TermModRecord", null, rewriteNameSpace(Modification.class), false);

        List<Schema.Field> fields = new ArrayList<>(2);
        fields.add(new Schema.Field("attachment", createEnumSchema(ModAttachment.class, ModAttachment.values()), null, null));
        fields.add(new Schema.Field("modification", modificationReader.createSchema(), null, null));

        schema.setFields(fields);

        return schema;
    }
}
