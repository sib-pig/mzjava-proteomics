/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.avro.io;

import org.apache.avro.Schema;
import org.apache.avro.io.Decoder;
import org.apache.avro.io.DecoderFactory;
import org.expasy.mzjava.proteomics.avro.io.PeptideFragmentReader;
import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.core.mol.AtomicSymbol;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.proteomics.mol.PeptideFragment;
import org.expasy.mzjava.core.ms.spectrum.FragmentType;
import org.junit.Assert;
import org.junit.Test;

import java.util.EnumSet;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class PeptideFragmentReaderTest {

    @Test
    public void testReadSeqOnly() throws Exception {

        PeptideFragmentReader reader = new PeptideFragmentReader();
        Decoder in = DecoderFactory.get().jsonDecoder(reader.createSchema(),
                "{\"fragmentType\":\"INTERNAL\",\"seq\":[\"M\",\"Q\",\"R\",\"S\",\"T\",\"A\",\"T\",\"G\",\"C\",\"F\",\"K\",\"L\",\"X\"],\"sideChainModMap\":[],\"termModMap\":[]}");
        PeptideFragment fragment = reader.read(in);

        Assert.assertEquals(FragmentType.INTERNAL, fragment.getFragmentType());
        Assert.assertEquals(AminoAcid.M, fragment.getSymbol(0));
        Assert.assertEquals(AminoAcid.Q, fragment.getSymbol(1));
        Assert.assertEquals(AminoAcid.R, fragment.getSymbol(2));
        Assert.assertEquals(AminoAcid.S, fragment.getSymbol(3));
        Assert.assertEquals(AminoAcid.T, fragment.getSymbol(4));
        Assert.assertEquals(AminoAcid.A, fragment.getSymbol(5));
        Assert.assertEquals(AminoAcid.T, fragment.getSymbol(6));
        Assert.assertEquals(AminoAcid.G, fragment.getSymbol(7));
        Assert.assertEquals(AminoAcid.C, fragment.getSymbol(8));
        Assert.assertEquals(AminoAcid.F, fragment.getSymbol(9));
        Assert.assertEquals(AminoAcid.K, fragment.getSymbol(10));
        Assert.assertEquals(AminoAcid.L, fragment.getSymbol(11));
        Assert.assertEquals(AminoAcid.X, fragment.getSymbol(12));

        Assert.assertEquals(0, fragment.getModifiedResidueCount());
    }

    @Test
    public void testRead() throws Exception {

        PeptideFragmentReader reader = new PeptideFragmentReader();
        Decoder in = DecoderFactory.get().jsonDecoder(reader.createSchema(), "{" +
                "\"fragmentType\":\"INTACT\"," +
                "\"seq\":[\"M\",\"Q\",\"R\",\"S\",\"T\",\"A\",\"T\",\"G\",\"C\",\"F\",\"K\",\"L\",\"X\"]," +
                "\"sideChainModMap\":[" +
                    "{\"index\":2,\"modification\":{\"label\":\"O2\",\"mass\":{\"org.expasy.mzjava_avro.core.mol.Composition\":{\"composition\":{\"O\":2}}}}}," +
                    "{\"index\":6,\"modification\":{\"label\":\"C\",\"mass\":{\"org.expasy.mzjava_avro.core.mol.Composition\":{\"composition\":{\"C\":1}}}}}]," +
                "\"termModMap\":[]}");
        PeptideFragment fragment = reader.read(in);

        Assert.assertEquals(FragmentType.INTACT, fragment.getFragmentType());
        Assert.assertEquals(AminoAcid.M, fragment.getSymbol(0));
        Assert.assertEquals(AminoAcid.Q, fragment.getSymbol(1));
        Assert.assertEquals(AminoAcid.R, fragment.getSymbol(2));
        Assert.assertEquals(AminoAcid.S, fragment.getSymbol(3));
        Assert.assertEquals(AminoAcid.T, fragment.getSymbol(4));
        Assert.assertEquals(AminoAcid.A, fragment.getSymbol(5));
        Assert.assertEquals(AminoAcid.T, fragment.getSymbol(6));
        Assert.assertEquals(AminoAcid.G, fragment.getSymbol(7));
        Assert.assertEquals(AminoAcid.C, fragment.getSymbol(8));
        Assert.assertEquals(AminoAcid.F, fragment.getSymbol(9));
        Assert.assertEquals(AminoAcid.K, fragment.getSymbol(10));
        Assert.assertEquals(AminoAcid.L, fragment.getSymbol(11));
        Assert.assertEquals(AminoAcid.X, fragment.getSymbol(12));

        Assert.assertEquals(2, fragment.getModifiedResidueCount());
        Assert.assertArrayEquals(new int[]{2, 6}, fragment.getModificationIndexes(ModAttachment.all));

        List<Modification> mods1 = fragment.getModificationsAt(2, EnumSet.allOf(ModAttachment.class));
        Assert.assertEquals(1, mods1.size());

        Assert.assertEquals(Modification.parseModification("O2"), mods1.get(0));
        List<Modification> mods2 = fragment.getModificationsAt(6, EnumSet.allOf(ModAttachment.class));
        Assert.assertEquals(1, mods2.size());
        Assert.assertEquals(new Modification("C", new Composition.Builder(AtomicSymbol.C).build()), mods2.get(0));
    }

    @Test
    public void testRead2() throws Exception {

        PeptideFragmentReader reader = new PeptideFragmentReader();
        Decoder in = DecoderFactory.get().jsonDecoder(reader.createSchema(), "{" +
                "\"fragmentType\":\"FORWARD\"," +
                "\"seq\":[\"A\",\"C\",\"P\",\"L\",\"E\",\"K\"]," +
                "\"sideChainModMap\":[{\"index\":0,\"modification\":{\"label\":\"C3H9\",\"mass\":{\"org.expasy.mzjava_avro.core.mol.Composition\":{\"composition\":{\"H\":9,\"C\":3}}}}},{\"index\":0,\"modification\":{\"label\":\"C2H2O\",\"mass\":{\"org.expasy.mzjava_avro.core.mol.Composition\":{\"composition\":{\"H\":2,\"C\":2,\"O\":1}}}}}]," +
                "\"termModMap\":[{\"attachment\":\"C_TERM\",\"modification\":{\"label\":\"45.365\",\"mass\":{\"org.expasy.mzjava_avro.core.mol.NumericMass\":{\"mass\":45.365}}}}]}");
        PeptideFragment fragment = reader.read(in);
        Assert.assertEquals(FragmentType.FORWARD, fragment.getFragmentType());
        Assert.assertEquals("A(C3H9, C2H2O)CPLEK_(45.365)", fragment.toString());
    }

    @Test
    public void testSchema() throws Exception {

        String expected = "{\n" +
                "  \"type\" : \"record\",\n" +
                "  \"name\" : \"PeptideFragment\",\n" +
                "  \"namespace\" : \"org.expasy.mzjava_avro.proteomics.mol\",\n" +
                "  \"fields\" : [ {\n" +
                "    \"name\" : \"fragmentType\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"enum\",\n" +
                "      \"name\" : \"FragmentType\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava.core.ms.spectrum\",\n" +
                "      \"symbols\" : [ \"FORWARD\", \"REVERSE\", \"UNKNOWN\", \"INTERNAL\", \"MONOMER\", \"INTACT\" ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"seq\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"array\",\n" +
                "      \"items\" : {\n" +
                "        \"type\" : \"enum\",\n" +
                "        \"name\" : \"AminoAcid\",\n" +
                "        \"namespace\" : \"org.expasy.mzjava.proteomics.mol\",\n" +
                "        \"symbols\" : [ \"A\", \"R\", \"N\", \"D\", \"C\", \"E\", \"Q\", \"G\", \"H\", \"K\", \"M\", \"F\", \"P\", \"S\", \"T\", \"W\", \"Y\", \"V\", \"J\", \"I\", \"L\", \"O\", \"U\", \"X\", \"B\", \"Z\" ]\n" +
                "      }\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"sideChainModMap\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"array\",\n" +
                "      \"items\" : {\n" +
                "        \"type\" : \"record\",\n" +
                "        \"name\" : \"SideChainModRecord\",\n" +
                "        \"namespace\" : \"org.expasy.mzjava_avro.proteomics.mol.modification\",\n" +
                "        \"fields\" : [ {\n" +
                "          \"name\" : \"index\",\n" +
                "          \"type\" : \"int\"\n" +
                "        }, {\n" +
                "          \"name\" : \"modification\",\n" +
                "          \"type\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"Modification\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"label\",\n" +
                "              \"type\" : \"string\"\n" +
                "            }, {\n" +
                "              \"name\" : \"mass\",\n" +
                "              \"type\" : [ {\n" +
                "                \"type\" : \"record\",\n" +
                "                \"name\" : \"NumericMass\",\n" +
                "                \"namespace\" : \"org.expasy.mzjava_avro.core.mol\",\n" +
                "                \"fields\" : [ {\n" +
                "                  \"name\" : \"mass\",\n" +
                "                  \"type\" : \"double\"\n" +
                "                } ]\n" +
                "              }, {\n" +
                "                \"type\" : \"record\",\n" +
                "                \"name\" : \"Composition\",\n" +
                "                \"namespace\" : \"org.expasy.mzjava_avro.core.mol\",\n" +
                "                \"fields\" : [ {\n" +
                "                  \"name\" : \"composition\",\n" +
                "                  \"type\" : {\n" +
                "                    \"type\" : \"map\",\n" +
                "                    \"values\" : \"int\"\n" +
                "                  }\n" +
                "                } ]\n" +
                "              } ]\n" +
                "            } ]\n" +
                "          }\n" +
                "        } ]\n" +
                "      }\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"termModMap\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"array\",\n" +
                "      \"items\" : {\n" +
                "        \"type\" : \"record\",\n" +
                "        \"name\" : \"TermModRecord\",\n" +
                "        \"namespace\" : \"org.expasy.mzjava_avro.proteomics.mol.modification\",\n" +
                "        \"fields\" : [ {\n" +
                "          \"name\" : \"attachment\",\n" +
                "          \"type\" : {\n" +
                "            \"type\" : \"enum\",\n" +
                "            \"name\" : \"ModAttachment\",\n" +
                "            \"namespace\" : \"org.expasy.mzjava.proteomics.mol.modification\",\n" +
                "            \"symbols\" : [ \"SIDE_CHAIN\", \"N_TERM\", \"C_TERM\" ]\n" +
                "          }\n" +
                "        }, {\n" +
                "          \"name\" : \"modification\",\n" +
                "          \"type\" : \"Modification\"\n" +
                "        } ]\n" +
                "      }\n" +
                "    }\n" +
                "  } ]\n" +
                "}";

        Assert.assertEquals(new Schema.Parser().parse(expected).toString(true),
                new PeptideFragmentReader().createSchema().toString(true));
    }
}
