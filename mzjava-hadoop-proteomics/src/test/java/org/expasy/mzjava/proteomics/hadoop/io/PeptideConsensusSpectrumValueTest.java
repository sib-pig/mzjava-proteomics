/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.hadoop.io;

import com.google.common.base.Optional;
import com.google.common.collect.Sets;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.peaktransformer.SqrtTransformer;
import org.expasy.mzjava.core.ms.spectrum.FragmentType;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.core.ms.spectrum.RetentionTimeDiscrete;
import org.expasy.mzjava.hadoop.io.MockDataInput;
import org.expasy.mzjava.hadoop.io.MockDataOutput;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.mol.PeptideFragment;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;
import org.expasy.mzjava.proteomics.ms.spectrum.PepFragAnnotation;
import org.expasy.mzjava.proteomics.ms.spectrum.PepLibPeakAnnotation;
import org.expasy.mzjava.utils.URIBuilder;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class PeptideConsensusSpectrumValueTest {

    private final String base64 = "AAQEBgBmZmZm5qmqQAAAAAAAuIFABAh0ZXN0mpmZmZmZ6T+amZmZmZnJP1K4HoXrqapAexSuR+F6\n" +
            "hD8E9fONqvCY84mMAaeGvf/v1uzPuQG6iLr2/8ebr3flos3j/9nN16gBABIICgIUIiYoABoAAgYC\n" +
            "TwICAk8CAAAABgZJTlMQUElSNTIzNDYIYWRmaABGc29mdHdhcmU6Ly93d3cuZXhwYXN5Lm9yZy9s\n" +
            "aWJlcmF0b3IAAAICBAAKBOF6FK5HsW1AAAAAAADAVUACABiamZmZmZm5PwAAAAAAAAhAAgDhehSu\n" +
            "RyuCQAAAAAAAgEdAAgAquB6F61G4vj8AAAAAAAAUQAAMAAICBigAGgAAAAAAAAAAAAAAAAAA";

    @Test
    public void testWrite() throws Exception {

        Set<UUID> memberIds = new LinkedHashSet<>();
        memberIds.add(UUID.fromString("b9f6199c-7d5e-4305-a330-26a480085e6c"));
        memberIds.add(UUID.fromString("3baf371f-ff67-421d-aba8-649801c6574d"));
        PeptideConsensusSpectrum spectrum = new PeptideConsensusSpectrum(Peptide.parse("CERM(O)VILAS"), memberIds);
        spectrum.setSpectrumSource(new URIBuilder("www.expasy.org", "liberator").build());
        spectrum.setPrecursorStats(3412.96, 0.01);
        spectrum.setScoreStats(0.8, 0.2);
        spectrum.setName("test");
        spectrum.setId(new UUID(1, 2));
        spectrum.setMsLevel(2);
        spectrum.getPrecursor().setValues(3412.95, 567, 2, 3);
        spectrum.add(237.54, 87);
        spectrum.addAnnotation(0, new PepLibPeakAnnotation(12, 0.1, 3));
        spectrum.add(581.41, 47);
        spectrum.addAnnotation(1, new PepLibPeakAnnotation(21, 0.12, 5, Optional.of(new PepFragAnnotation(IonType.y, 1, Peptide.parse("LAS")))));
        spectrum.addProteinAccessionNumbers("INS", "PIR52346", "adfh");
        spectrum.setRetentionTime(Optional.<RetentionTimeDiscrete>absent());

        MockDataOutput out = new MockDataOutput(512);
        PeptideConsensusSpectrumValue value = new PeptideConsensusSpectrumValue(
                Optional.absent(),
                Collections.emptyList());

        value.set(spectrum);
        value.write(out);

        assertEquals(base64, out.getBase64());
    }

    @Test
    public void testRead() throws Exception {

        PeptideConsensusSpectrumValue value = new PeptideConsensusSpectrumValue();

        value.readFields(new MockDataInput(base64));

        PeptideConsensusSpectrum spectrum = value.get();

        assertEquals(Peptide.parse("CERM(O)VILAS"), spectrum.getPeptide());
        assertEquals(2, spectrum.getMsLevel());
        assertEquals(2, spectrum.size());
        assertEquals(237.54, spectrum.getMz(0), 0.00001);
        assertEquals(87, spectrum.getIntensity(0), 0.00001);

        assertEquals(3412.95, spectrum.getPrecursor().getMz(), 0.00000001);
        assertEquals(567, spectrum.getPrecursor().getIntensity(), 0.00000001);
        assertEquals(2, spectrum.getPrecursor().getCharge());
        assertArrayEquals(new int[]{2, 3}, spectrum.getPrecursor().getChargeList());

        assertEquals(new UUID(1, 2), spectrum.getId());
        assertEquals("software://www.expasy.org/liberator", spectrum.getSpectrumSource().toString());
        assertEquals("", spectrum.getComment());

        assertEquals(2, spectrum.size());
        assertArrayEquals(new int[]{0, 1}, spectrum.getAnnotationIndexes());
        List<PepLibPeakAnnotation> annotations = spectrum.getAnnotations(0);
        assertEquals(1, annotations.size());
        PepLibPeakAnnotation annotation = annotations.get(0);
        assertEquals(12, annotation.getMergedPeakCount());
        assertEquals(0.1, annotation.getMzStd(), 0.00001);
        assertEquals(3, annotation.getIntensityStd(), 0.01);
        assertEquals(false, annotation.getOptFragmentAnnotation().isPresent());

        annotations = spectrum.getAnnotations(1);
        assertEquals(1, annotations.size());
        annotation = annotations.get(0);
        assertEquals(21, annotation.getMergedPeakCount());
        assertEquals(0.12, annotation.getMzStd(), 0.00001);
        assertEquals(5, annotation.getIntensityStd(), 0.01);
        assertEquals(true, annotation.getOptFragmentAnnotation().isPresent());
        PepFragAnnotation fragAnnotation = annotation.getOptFragmentAnnotation().get();
        assertEquals(IonType.y, fragAnnotation.getIonType());
        assertEquals(PeptideFragment.parse("LAS", FragmentType.REVERSE), fragAnnotation.getFragment());
        assertEquals(1, fragAnnotation.getCharge());
        assertEquals(0, fragAnnotation.getIsotopeCount());

        assertEquals(Sets.newLinkedHashSet(Arrays.asList("INS", "PIR52346", "adfh")), spectrum.getProteinAccessionNumbers());
        assertFalse(spectrum.getRetentionTime().isPresent());
    }

    @Test
    public void testReadProcessed() throws Exception {

        PeptideConsensusSpectrumValue value = new PeptideConsensusSpectrumValue(
                Optional.absent(),
                Collections.singletonList(new SqrtTransformer<>()));

        value.readFields(new MockDataInput(base64));

        PeptideConsensusSpectrum spectrum = value.get();

        assertEquals(Peptide.parse("CERM(O)VILAS"), spectrum.getPeptide());
        assertEquals(2, spectrum.getMsLevel());
        assertEquals(2, spectrum.size());
        assertEquals(237.54, spectrum.getMz(0), 0.00001);
        assertEquals(9.327379053088816, spectrum.getIntensity(0), 0.00001);

        assertEquals(3412.95, spectrum.getPrecursor().getMz(), 0.00000001);
        assertEquals(567, spectrum.getPrecursor().getIntensity(), 0.00000001);
        assertEquals(2, spectrum.getPrecursor().getCharge());
        assertArrayEquals(new int[]{2, 3}, spectrum.getPrecursor().getChargeList());

        assertEquals(new UUID(1, 2), spectrum.getId());
        assertEquals("software://www.expasy.org/liberator", spectrum.getSpectrumSource().toString());
        assertEquals("", spectrum.getComment());

        assertArrayEquals(new int[]{0, 1}, spectrum.getAnnotationIndexes());
        List<PepLibPeakAnnotation> annotations = spectrum.getAnnotations(0);
        assertEquals(1, annotations.size());
        PepLibPeakAnnotation annotation = annotations.get(0);
        assertEquals(12, annotation.getMergedPeakCount());
        assertEquals(0.1, annotation.getMzStd(), 0.00001);
        assertEquals(3, annotation.getIntensityStd(), 0.01);
        assertEquals(false, annotation.getOptFragmentAnnotation().isPresent());

        assertEquals(spectrum.getPrecursorMzMean(),3412.96,0.0000001);
        assertEquals(spectrum.getPrecursorMzStdev(),0.01,0.0000001);
        assertEquals(spectrum.getSimScoreMean(),0.8,0.0000001);
        assertEquals(spectrum.getSimScoreStdev(),0.2,0.0000001);

        assertTrue(spectrum.getName().equals("test"));
        assertEquals(spectrum.getMemberIds().size(), 2);
        assertTrue(spectrum.getMemberIds().contains(UUID.fromString("3baf371f-ff67-421d-aba8-649801c6574d")));
        assertTrue(spectrum.getMemberIds().contains(UUID.fromString("b9f6199c-7d5e-4305-a330-26a480085e6c")));

        assertFalse(spectrum.getRetentionTime().isPresent());
    }

    @Test
    public void testReadProcessedCustomPeakPrecision() throws Exception {

        PeptideConsensusSpectrumValue value = new PeptideConsensusSpectrumValue(
                Optional.of(PeakList.Precision.FLOAT),
                Collections.singletonList(new SqrtTransformer<>()));

        value.readFields(new MockDataInput(base64));

        PeptideConsensusSpectrum spectrum = value.get();

        assertEquals(PeakList.Precision.FLOAT, spectrum.getPrecision());
        assertEquals(Peptide.parse("CERM(O)VILAS"), spectrum.getPeptide());
        assertEquals(2, spectrum.getMsLevel());
        assertEquals(2, spectrum.size());
        assertEquals(237.54, spectrum.getMz(0), 0.00001);
        assertEquals(9.327379053088816, spectrum.getIntensity(0), 0.00001);

        assertEquals(3412.95, spectrum.getPrecursor().getMz(), 0.00000001);
        assertEquals(567, spectrum.getPrecursor().getIntensity(), 0.00000001);
        assertEquals(2, spectrum.getPrecursor().getCharge());
        assertArrayEquals(new int[]{2, 3}, spectrum.getPrecursor().getChargeList());

        assertEquals(new UUID(1, 2), spectrum.getId());
        assertEquals("software://www.expasy.org/liberator", spectrum.getSpectrumSource().toString());
        assertEquals("", spectrum.getComment());

        assertArrayEquals(new int[]{0, 1
        }, spectrum.getAnnotationIndexes());
        List<PepLibPeakAnnotation> annotations = spectrum.getAnnotations(0);
        assertEquals(1, annotations.size());
        PepLibPeakAnnotation annotation = annotations.get(0);
        assertEquals(12, annotation.getMergedPeakCount());
        assertEquals(0.1, annotation.getMzStd(), 0.00001);
        assertEquals(3, annotation.getIntensityStd(), 0.01);
        assertEquals(false, annotation.getOptFragmentAnnotation().isPresent());

        assertFalse(spectrum.getRetentionTime().isPresent());
    }
}
