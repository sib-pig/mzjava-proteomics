/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.avro.io;

import org.apache.avro.Schema;
import org.apache.avro.io.Encoder;
import org.apache.avro.io.EncoderFactory;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.expasy.mzjava.core.mol.NumericMass;
import org.expasy.mzjava.proteomics.avro.io.PepFragAnnotationWriter;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.proteomics.ms.spectrum.PepFragAnnotation;
import org.junit.Assert;
import org.junit.Test;

import java.io.StringWriter;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class PepFragAnnotationWriterTest {

    @Test
    public void testJson() throws Exception {

        PepFragAnnotationWriter writer = new PepFragAnnotationWriter();

        StringWriter out = new StringWriter();
        JsonGenerator gen = new JsonFactory().createJsonGenerator(out);
        Encoder encoder = EncoderFactory.get().jsonEncoder(writer.createSchema(), gen);

        PepFragAnnotation annotation = new PepFragAnnotation.Builder(IonType.by_internal, -2, Peptide.parse("PEPTVALY")).addH2(1).setNeutralLoss(new NumericMass(15.999999)).build();

        writer.write(annotation, encoder);
        encoder.flush();

        Assert.assertEquals(
                "{" +
                    "\"ionType\":\"by_internal\"," +
                     "\"isotopeComposition\":{\"composition\":{\"H[2]\":1}}," +
                     "\"charge\":-2," +
                     "\"peptideFragment\":{" +
                        "\"fragmentType\":\"INTERNAL\"," +
                        "\"seq\":[\"P\",\"E\",\"P\",\"T\",\"V\",\"A\",\"L\",\"Y\"]," +
                        "\"sideChainModMap\":[]," +
                        "\"termModMap\":[]" +
                        "}," +
                    "\"neutralLoss\":{" +
                        "\"org.expasy.mzjava_avro.core.mol.NumericMass\":{" +
                            "\"mass\":15.999999" +
                        "}" +
                    "}" +
                "}"
                , out.toString());
    }

    @Test
    public void testCreateSchema() throws Exception {

        Schema expected = new Schema.Parser().parse("{\n" +
                "  \"type\" : \"record\",\n" +
                "  \"name\" : \"PepFragAnnotation\",\n" +
                "  \"namespace\" : \"org.expasy.mzjava_avro.proteomics.ms.spectrum\",\n" +
                "  \"fields\" : [ {\n" +
                "    \"name\" : \"ionType\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"enum\",\n" +
                "      \"name\" : \"IonType\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava.core.ms.spectrum\",\n" +
                "      \"symbols\" : [ \"a\", \"b\", \"c\", \"d\", \"w\", \"x\", \"y\", \"z\", \"aw_internal\", \"ax_internal\", \"ay_internal\", \"az_internal\", \"bw_internal\", \"bx_internal\", \"by_internal\", \"bz_internal\", \"cw_internal\", \"cx_internal\", \"cy_internal\", \"cz_internal\", \"dw_internal\", \"dx_internal\", \"dy_internal\", \"dz_internal\", \"i\", \"p\", \"unknown\" ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"isotopeComposition\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"Composition\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.mol\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"composition\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"map\",\n" +
                "          \"values\" : \"int\"\n" +
                "        }\n" +
                "      } ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"charge\",\n" +
                "    \"type\" : \"int\"\n" +
                "  }, {\n" +
                "    \"name\" : \"peptideFragment\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"PeptideFragment\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.proteomics.mol\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"fragmentType\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"enum\",\n" +
                "          \"name\" : \"FragmentType\",\n" +
                "          \"namespace\" : \"org.expasy.mzjava.core.ms.spectrum\",\n" +
                "          \"symbols\" : [ \"FORWARD\", \"REVERSE\", \"UNKNOWN\", \"INTERNAL\", \"MONOMER\", \"INTACT\" ]\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"seq\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"enum\",\n" +
                "            \"name\" : \"AminoAcid\",\n" +
                "            \"namespace\" : \"org.expasy.mzjava.proteomics.mol\",\n" +
                "            \"symbols\" : [ \"A\", \"R\", \"N\", \"D\", \"C\", \"E\", \"Q\", \"G\", \"H\", \"K\", \"M\", \"F\", \"P\", \"S\", \"T\", \"W\", \"Y\", \"V\", \"J\", \"I\", \"L\", \"O\", \"U\", \"X\", \"B\", \"Z\" ]\n" +
                "          }\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"sideChainModMap\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"SideChainModRecord\",\n" +
                "            \"namespace\" : \"org.expasy.mzjava_avro.proteomics.mol.modification\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"index\",\n" +
                "              \"type\" : \"int\"\n" +
                "            }, {\n" +
                "              \"name\" : \"modification\",\n" +
                "              \"type\" : {\n" +
                "                \"type\" : \"record\",\n" +
                "                \"name\" : \"Modification\",\n" +
                "                \"fields\" : [ {\n" +
                "                  \"name\" : \"label\",\n" +
                "                  \"type\" : \"string\"\n" +
                "                }, {\n" +
                "                  \"name\" : \"mass\",\n" +
                "                  \"type\" : [ {\n" +
                "                    \"type\" : \"record\",\n" +
                "                    \"name\" : \"NumericMass\",\n" +
                "                    \"namespace\" : \"org.expasy.mzjava_avro.core.mol\",\n" +
                "                    \"fields\" : [ {\n" +
                "                      \"name\" : \"mass\",\n" +
                "                      \"type\" : \"double\"\n" +
                "                    } ]\n" +
                "                  }, \"org.expasy.mzjava_avro.core.mol.Composition\" ]\n" +
                "                } ]\n" +
                "              }\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"termModMap\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"TermModRecord\",\n" +
                "            \"namespace\" : \"org.expasy.mzjava_avro.proteomics.mol.modification\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"attachment\",\n" +
                "              \"type\" : {\n" +
                "                \"type\" : \"enum\",\n" +
                "                \"name\" : \"ModAttachment\",\n" +
                "                \"namespace\" : \"org.expasy.mzjava.proteomics.mol.modification\",\n" +
                "                \"symbols\" : [ \"SIDE_CHAIN\", \"N_TERM\", \"C_TERM\" ]\n" +
                "              }\n" +
                "            }, {\n" +
                "              \"name\" : \"modification\",\n" +
                "              \"type\" : \"Modification\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"neutralLoss\",\n" +
                "    \"type\" : [ \"org.expasy.mzjava_avro.core.mol.NumericMass\", \"org.expasy.mzjava_avro.core.mol.Composition\" ]\n" +
                "  } ]\n" +
                "}");

        Assert.assertEquals(expected.toString(true), new PepFragAnnotationWriter().createSchema().toString(true));
    }
}
