/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.hadoop.io;

import com.google.common.base.Optional;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessor;
import org.expasy.mzjava.core.ms.peaklist.PeakSink;
import org.expasy.mzjava.core.ms.peaklist.peaktransformer.IdentityPeakProcessor;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.hadoop.io.MockDataInput;
import org.expasy.mzjava.hadoop.io.MockDataOutput;
import org.expasy.mzjava.proteomics.hadoop.io.PeptideSpectrumValue;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.ms.spectrum.PepFragAnnotation;
import org.expasy.mzjava.proteomics.ms.spectrum.PeptideSpectrum;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class PeptideSpectrumValueTest {

    private final String base64 = "AAIGAOxRuB6FXZpAAAAAAACAX0AEBhAICgIiJigAGgACDghIUE8zAgYCSAICTwYCUAIAAAAGBklO\n" +
            "UxBQSVI1MjM0NghhZGZoAIS30q3Ez8yeuQH92q+87rON88kBAAoIUrgehetfmkAAAAAAAHB+QAAp\n" +
            "XI/C9f+kQAAAAAAAcIhAAClcj8L1z6xAAAAAAAAAKEACAAwCCkNbMTNdBAACAgYoABoAAgQISFBP\n" +
            "MwIGAkgCAk8GAlACAAAAAAAAAAAAAAAAABSuR+H6T7JAAAAAAABAj0AAAA==";

    @Test
    public void testWrite() throws Exception {

        PeptideSpectrum spectrum = new PeptideSpectrum(Peptide.parse("CERVILAS(HPO3)"), 3, PeakList.Precision.DOUBLE);

        spectrum.getPrecursor().setValues(1687.38, 126, 3);
        spectrum.setMsLevel(2);
        spectrum.setId(UUID.fromString("5c9e993e-22da-4dc2-9b0c-e5308c3a0941"));

        spectrum.add(1687.98, 487);
        spectrum.add(2687.98, 782);
        spectrum.add(3687.98, 12);
        spectrum.add(4687.98, 1000);

        spectrum.addAnnotation(2, new PepFragAnnotation.Builder(IonType.y, 1, spectrum.getPeptide().subSequence(IonType.y, 3)).addC13(2).build());

        spectrum.addProteinAccessionNumbers("INS", "PIR52346", "adfh");

        PeptideSpectrumValue value = new PeptideSpectrumValue(Optional.<PeakList.Precision>absent(), Collections.<PeakProcessor<PepFragAnnotation, PepFragAnnotation>>emptyList());

        MockDataOutput out = new MockDataOutput(256);

        value.set(spectrum);
        value.write(out);

        assertEquals(base64, out.getBase64());
    }

    @Test
    public void testRead() throws Exception {

        PeptideSpectrumValue value = new PeptideSpectrumValue();

        value.readFields(new MockDataInput(base64));

        PeptideSpectrum spectrum = value.get();

        Peptide peptide = spectrum.getPeptide();
        assertEquals(Peptide.parse("CERVILAS(HPO3)"), peptide);

        assertEquals(2, spectrum.getMsLevel());
        assertEquals(1687.38, spectrum.getPrecursor().getMz(), 0.00000001);
        assertEquals(126, spectrum.getPrecursor().getIntensity(), 0.00000001);
        assertEquals(3, spectrum.getPrecursor().getCharge());
        assertEquals(UUID.fromString("5c9e993e-22da-4dc2-9b0c-e5308c3a0941"), spectrum.getId());

        assertEquals(PeakList.Precision.DOUBLE, spectrum.getPrecision());
        assertEquals(4, spectrum.size());

        assertEquals(1687.98, spectrum.getMz(0), 0.000001);
        assertEquals(487, spectrum.getIntensity(0), 0.000001);
        assertEquals(2687.98, spectrum.getMz(1), 0.000001);
        assertEquals(782, spectrum.getIntensity(1), 0.000001);
        assertEquals(3687.98, spectrum.getMz(2), 0.000001);
        assertEquals(12, spectrum.getIntensity(2), 0.000001);
        assertEquals(4687.98, spectrum.getMz(3), 0.000001);
        assertEquals(1000, spectrum.getIntensity(3), 0.000001);

        assertEquals(1, spectrum.getAnnotationIndexes().length);

        assertEquals(Arrays.asList("INS", "PIR52346", "adfh"), spectrum.getProteinAccessionNumbers());
    }

    @Test
    public void testRoundTrip() throws Exception {

        PeptideSpectrum spectrum = new PeptideSpectrum(Peptide.parse("CERVILAS(HPO3)"), 3, PeakList.Precision.DOUBLE);

        spectrum.setMsLevel(2);

        spectrum.add(1687.98, 487);
        spectrum.add(2687.98, 782);
        spectrum.add(3687.98, 12);
        spectrum.add(4687.98, 1000);

        spectrum.addAnnotation(2, new PepFragAnnotation.Builder(IonType.y, 1, spectrum.getPeptide().subSequence(IonType.y, 3)).addC13(2).build());

        PeptideSpectrumValue value = new PeptideSpectrumValue(Optional.<PeakList.Precision>absent(), Collections.<PeakProcessor<PepFragAnnotation, PepFragAnnotation>>emptyList());

        MockDataOutput out = new MockDataOutput(128*2);

        value.set(spectrum);
        value.write(out);

        String base64 = out.getBase64();

        value.readFields(new MockDataInput(base64));
        PeptideSpectrum readSpectrum = value.get();

        assertEquals(spectrum, readSpectrum);
    }

    @Test
    public void testProcessedCustomEncoding() throws Exception {

        IdentityPeakProcessor<PepFragAnnotation> peakProcessor = Mockito.spy(new IdentityPeakProcessor<PepFragAnnotation>());
        PeptideSpectrumValue value = new PeptideSpectrumValue(
                Optional.of(PeakList.Precision.FLOAT),
                Collections.<PeakProcessor<PepFragAnnotation, PepFragAnnotation>>singletonList(peakProcessor));

        value.readFields(new MockDataInput(base64));

        PeptideSpectrum spectrum = value.get();

        Peptide peptide = spectrum.getPeptide();
        assertEquals(Peptide.parse("CERVILAS(HPO3)"), peptide);

        assertEquals(2, spectrum.getMsLevel());

        assertEquals(PeakList.Precision.FLOAT, spectrum.getPrecision());
        assertEquals(4, spectrum.size());

        double delta = 0.01;
        Assert.assertEquals(new Peak(1687.38, 126, 3), spectrum.getPrecursor());
        assertEquals(1687.98, spectrum.getMz(0), delta);
        assertEquals(487, spectrum.getIntensity(0), delta);
        assertEquals(2687.98, spectrum.getMz(1), delta);
        assertEquals(782, spectrum.getIntensity(1), delta);
        assertEquals(3687.98, spectrum.getMz(2), delta);
        assertEquals(12, spectrum.getIntensity(2), delta);
        assertEquals(4687.98, spectrum.getMz(3), delta);
        assertEquals(1000, spectrum.getIntensity(3), delta);

        assertEquals(1, spectrum.getAnnotationIndexes().length);

        verify(peakProcessor).setSink(Mockito.<PeakSink<PepFragAnnotation>>any());
        verify(peakProcessor).start(4);
        verify(peakProcessor, times(4)).processPeak(anyDouble(), anyDouble(), Mockito.<List<PepFragAnnotation>>any());
        verify(peakProcessor).end();
        verifyNoMoreInteractions(peakProcessor);
    }

    @Test
    public void testProcessed() throws Exception {

        IdentityPeakProcessor<PepFragAnnotation> peakProcessor = Mockito.spy(new IdentityPeakProcessor<PepFragAnnotation>());
        PeptideSpectrumValue value = new PeptideSpectrumValue(
                Optional.<PeakList.Precision>absent(),
                Collections.<PeakProcessor<PepFragAnnotation, PepFragAnnotation>>singletonList(peakProcessor));

        value.readFields(new MockDataInput(base64));

        PeptideSpectrum spectrum = value.get();

        Peptide peptide = spectrum.getPeptide();
        assertEquals(Peptide.parse("CERVILAS(HPO3)"), peptide);

        assertEquals(2, spectrum.getMsLevel());

        //Precursor info is in the key so this will not be set

        assertEquals(PeakList.Precision.DOUBLE, spectrum.getPrecision());
        assertEquals(4, spectrum.size());

        double delta = 0.01;
        assertEquals(1687.98, spectrum.getMz(0), delta);
        assertEquals(487, spectrum.getIntensity(0), delta);
        assertEquals(2687.98, spectrum.getMz(1), delta);
        assertEquals(782, spectrum.getIntensity(1), delta);
        assertEquals(3687.98, spectrum.getMz(2), delta);
        assertEquals(12, spectrum.getIntensity(2), delta);
        assertEquals(4687.98, spectrum.getMz(3), delta);
        assertEquals(1000, spectrum.getIntensity(3), delta);

        assertEquals(1, spectrum.getAnnotationIndexes().length);

        verify(peakProcessor).setSink(Mockito.<PeakSink<PepFragAnnotation>>any());
        verify(peakProcessor).start(4);
        verify(peakProcessor, times(4)).processPeak(anyDouble(), anyDouble(), Mockito.<List<PepFragAnnotation>>any());
        verify(peakProcessor).end();
        verifyNoMoreInteractions(peakProcessor);
    }
}
