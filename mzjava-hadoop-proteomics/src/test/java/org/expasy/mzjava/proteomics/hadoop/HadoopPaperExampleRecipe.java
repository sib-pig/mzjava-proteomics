package org.expasy.mzjava.proteomics.hadoop;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.RawComparator;
import org.apache.hadoop.io.WritableComparator;
import org.apache.hadoop.mapred.JobContext;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.Tolerance;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.hadoop.io.SpectrumKey;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;
import org.expasy.mzjava.proteomics.ms.fragment.PeptideFragmenter;
import org.expasy.mzjava.proteomics.ms.spectrum.PeptideSpectrum;
import org.expasy.mzjava.utils.URIBuilder;

import java.io.IOException;
import java.io.Serializable;
import java.net.URI;
import java.util.Collections;
import java.util.EnumSet;
import java.util.logging.Logger;

import static org.expasy.mzjava.core.ms.peaklist.PeakList.Precision.FLOAT;
import static org.expasy.mzjava.core.ms.peaklist.peakfilter.AbstractMergePeakFilter.IntensityMode.SUM_INTENSITY;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
// # $RECIPE$ $NUMBER$ - Building a spectrum library using Hadoop #
//
// ## $PROBLEM$ ##
// You want to create a spectrum library using Hadoop
//
// ## $SOLUTION$ ##
public class HadoopPaperExampleRecipe extends Configured implements Tool {

    private static final Logger LOGGER = Logger.getLogger(HadoopPaperExampleRecipe.class.getName());

// The Mapper class which maps peptide spectra that are read from a sequence file to a peptide spectrum key value pair.
    //<SNIP>
    public static class PeptideMapper extends Mapper<SpectrumKey, PeptideSpectrum, Peptide, PeptideSpectrum> {

        @Override
        protected void map(SpectrumKey key, PeptideSpectrum value, Context context) throws IOException, InterruptedException {

            context.write(value.getPeptide(), value);
        }
    }
    //</SNIP>

// The reducer class which creates consensus spectra for each peptide. If a peptide has spectra which have
// more than one precursor charge one consensus spectrum is emitted for each charge.
    //<SNIP>
    public static class ConsensusReducer extends Reducer<Peptide, PeptideSpectrum, SpectrumKey, PeptideConsensusSpectrum> {

        private Tolerance tolerance;
        private PeptideFragmenter fragmenter;

        @Override
        protected void setup(Context context) throws IOException, InterruptedException {

            super.setup(context);

            tolerance = new AbsoluteTolerance(0.4);
            fragmenter = new PeptideFragmenter(EnumSet.of(IonType.b, IonType.y), FLOAT);
        }

        @Override
        protected void reduce(Peptide peptide, Iterable<PeptideSpectrum> values, Context context) throws IOException, InterruptedException {

            final Multimap<Integer, PeakList> chargeMap = ArrayListMultimap.create();

            for (PeptideSpectrum spectrum : values) {

                chargeMap.put(spectrum.getPrecursor().getCharge(), spectrum);
            }

            final URI source = new URIBuilder("org.expasy.mzjava", "hadoop-example").build();
            for (Integer charge : chargeMap.keySet()) {

                final PeptideConsensusSpectrum consensus = PeptideConsensusSpectrum.builder(FLOAT, source)
                        .setConsensusParameters(0.2, 0.2, SUM_INTENSITY)
                        .setAnnotationParameters(tolerance, fragmenter)
                        .setFilterParameters(0.2, 2)
                        .buildConsensus(charge, peptide, chargeMap.get(charge), Collections.<String>emptySet());

                double mz = peptide.calculateMz(charge);
                context.write(new SpectrumKey(mz, charge), consensus);
            }
        }
    }
    //</SNIP>

// Code to configure Hadoop
    //<SNIP>
    public static void main(String[] args) throws Exception {

        int exitCode = ToolRunner.run(new HadoopPaperExampleRecipe(), args);
        System.exit(exitCode);
    }

    @Override
    public int run(String[] args) throws Exception {

        Configuration configuration = getConf();
        //This line configures to Hadoop to use the MzJava serialization as well as the default Hadoop serialization
        configuration.set("io.serializations", "org.apache.hadoop.io.serializer.WritableSerialization,org.apache.hadoop.io.serializer.avro.AvroSpecificSerialization,org.apache.hadoop.io.serializer.avro.AvroReflectSerialization,org.expasy.mzjava.hadoop.io.MzJavaSerialization");
        //This line configures Hadoop to use the DefaultRawComparator to sort the keys
        configuration.setClass(JobContext.KEY_COMPARATOR, DefaultRawComparator.class, RawComparator.class);

        String queryFile = args[0];
        String outputPath = args[1];

        Job job = Job.getInstance(configuration, "Hadoop Example");
        job.setJarByClass(getClass());
        job.setInputFormatClass(SequenceFileInputFormat.class);
        SequenceFileInputFormat.addInputPath(job, new Path(queryFile));

        job.setOutputFormatClass(SequenceFileOutputFormat.class);
        Path outputDir = new Path(outputPath);
        FileOutputFormat.setOutputPath(job, outputDir);

        job.setMapperClass(PeptideMapper.class);
        job.setMapOutputKeyClass(Peptide.class);
        job.setMapOutputValueClass(PeptideSpectrum.class);

        job.setReducerClass(ConsensusReducer.class);
        job.setOutputKeyClass(SpectrumKey.class);
        job.setOutputValueClass(PeptideConsensusSpectrum.class);

        FileSystem fs = FileSystem.get(configuration);
        fs.delete(outputDir, true);

        if (job.waitForCompletion(true)) {

            return 0;
        } else {

            LOGGER.severe("wait for completion returned false");
            return 1;
        }
    }

    /**
     * Comparator for that is used by hadoop to compare the peptide keys
     */
    public static final class DefaultRawComparator implements RawComparator<Object>, Serializable {
        @Override
        public int compare(byte[] b1, int s1, int l1, byte[] b2, int s2, int l2) {

            return WritableComparator.compareBytes(b1, s1, l1, b2, s2, l2);
        }

        @Override
        public int compare(Object o1, Object o2) {

            throw new RuntimeException("Object comparison not supported");
        }
    }
    //</SNIP>

// ## $RELATED$ ##
// See $SparkPaperExampleRecipe$ for an example that uses Apache Spark
}