/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.avro.io;

import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.avro.Schema;
import org.apache.avro.io.Decoder;
import org.apache.avro.io.DecoderFactory;
import org.apache.avro.io.Encoder;
import org.apache.avro.io.EncoderFactory;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.util.DefaultPrettyPrinter;
import org.expasy.mzjava.core.mol.NumericMass;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessor;
import org.expasy.mzjava.core.ms.spectrum.FragmentType;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.core.ms.spectrum.RetentionTimeDiscrete;
import org.expasy.mzjava.core.ms.spectrum.TimeUnit;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.mol.PeptideFragment;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;
import org.expasy.mzjava.proteomics.ms.spectrum.PepFragAnnotation;
import org.expasy.mzjava.proteomics.ms.spectrum.PepLibPeakAnnotation;
import org.expasy.mzjava.utils.URIBuilder;
import org.junit.Assert;
import org.junit.Test;

import java.io.StringWriter;
import java.util.*;

import static org.junit.Assert.assertEquals;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class PeptideConsensusSpectrumReaderTest {

    @Test
    public void testRead() throws Exception {

        PeptideConsensusSpectrumReader reader = new PeptideConsensusSpectrumReader(
                Optional.<PeakList.Precision>absent(),
                Collections.<PeakProcessor<PepLibPeakAnnotation, PepLibPeakAnnotation>>emptyList());

        Decoder in = DecoderFactory.get().jsonDecoder(reader.createSchema(), "{\n" +
                "  \"precursor\" : {\n" +
                "  \"polarity\" : \"POSITIVE\"," +
                "    \"charge\" : [ 2 ],\n" +
                "    \"mz\" : 1789.98765,\n" +
                "    \"intensity\" : 875.68\n" +
                "  },\n" +
                "  \"msLevel\" : 2,\n" +
                "  \"name\" : \"cluster_1\",\n" +
                "  \"simScoreMean\" : 0.89,\n" +
                "  \"simScoreStdev\" : 0.001,\n" +
                "  \"precursorMzMean\" : 733.01,\n" +
                "  \"precursorMzStdev\" : 0.001,\n" +
                "  \"memberIds\" : [ {\n" +
                "    \"mostSignificantBits\" : 272555917829435557,\n" +
                "    \"leastSignificantBits\" : -502732256918155454\n" +
                "  }, {\n" +
                "    \"mostSignificantBits\" : 2954576207831929,\n" +
                "    \"leastSignificantBits\" : -7401919710944613243\n" +
                "  }, {\n" +
                "    \"mostSignificantBits\" : 6906448715414564879,\n" +
                "    \"leastSignificantBits\" : -3377628232451089806\n" +
                "  }, {\n" +
                "    \"mostSignificantBits\" : 8159020250014517468,\n" +
                "    \"leastSignificantBits\" : -3276528637398377287\n" +
                "  }, {\n" +
                "    \"mostSignificantBits\" : -6464087018820727044,\n" +
                "    \"leastSignificantBits\" : 9158344342526998714\n" +
                "  }, {\n" +
                "    \"mostSignificantBits\" : -7477072015556504347,\n" +
                "    \"leastSignificantBits\" : -4759461241505446441\n" +
                "  }, {\n" +
                "    \"mostSignificantBits\" : 6453290794183652199,\n" +
                "    \"leastSignificantBits\" : -7855891324758780007\n" +
                "  }, {\n" +
                "    \"mostSignificantBits\" : -2330906535205672779,\n" +
                "    \"leastSignificantBits\" : 3380397291928878215\n" +
                "  }, {\n" +
                "    \"mostSignificantBits\" : -2439663895838265701,\n" +
                "    \"leastSignificantBits\" : 5432637156828437242\n" +
                "  } ],\n" +
                "  \"peptide\" : {\n" +
                "    \"seq\" : [ \"P\", \"E\", \"P\", \"T\", \"I\", \"D\", \"E\" ],\n" +
                "    \"sideChainModMap\" : [ ],\n" +
                "    \"termModMap\" : [ ]\n" +
                "  },\n" +
                "  \"proteinAccessionNumbers\" : [ \"INS\", \"PIR52346\" ],\n" +
                "  \"source\" : \"software://www.expasy.org/liberator\",\n" +
                "  \"comment\" : \"comment\",\n" +
                "  \"status\" : \"NORMAL\",\n" +
                "  \"retentionTime\" : {\n" +
                "    \"org.expasy.mzjava_avro.core.ms.spectrum.RetentionTimeDiscrete\" : {\n" +
                "      \"time\" : 100.0\n" +
                "    }\n" +
                "  },\n" +
                "  \"id\" : {\n" +
                "    \"mostSignificantBits\" : 6881239458523073728,\n" +
                "    \"leastSignificantBits\" : -6090640400048236173\n" +
                "  },\n" +
                "  \"precision\" : \"DOUBLE\",\n" +
                "  \"peaks\" : {\n" +
                "    \"org.expasy.mzjava_avro.core.ms.peaklist.DoublePeakList\" : {\n" +
                "      \"peaks\" : [ {\n" +
                "        \"mz\" : 12.93234579876,\n" +
                "        \"i\" : 145.2587412563541\n" +
                "      }, {\n" +
                "        \"mz\" : 58.92698543776,\n" +
                "        \"i\" : 145.2587412563541\n" +
                "      }, {\n" +
                "        \"mz\" : 152.956783276,\n" +
                "        \"i\" : 145.2587412563541\n" +
                "      } ]\n" +
                "    }\n" +
                "  }\n" +
                "}");
        PeptideConsensusSpectrum spectrum = reader.read(in);

        Assert.assertEquals(2, spectrum.getMsLevel());
        Assert.assertEquals(Peptide.parse("PEPTIDE"), spectrum.getPeptide());
        Assert.assertEquals("comment", spectrum.getComment());

        assertEquals(3, spectrum.size());
        assertEquals(12.93234579876, spectrum.getMz(0), 0.00001);
        assertEquals(145.2587412563541, spectrum.getIntensity(0), 0.00001);
        assertEquals(58.92698543776, spectrum.getMz(1), 0.00001);
        assertEquals(145.2587412563541, spectrum.getIntensity(1), 0.00001);
        assertEquals(152.956783276, spectrum.getMz(2), 0.00001);
        assertEquals(145.2587412563541, spectrum.getIntensity(2), 0.00001);

        assertEquals(new Peak(1789.98765, 875.68, 2), spectrum.getPrecursor());

        assertEquals(UUID.fromString("5f7f12d4-43ab-4cc0-ab79-b2d3647b5973"), spectrum.getId());
        assertEquals("software://www.expasy.org/liberator", spectrum.getSpectrumSource().toString());
        assertEquals("comment", spectrum.getComment());

        Set<UUID> memberIds = new HashSet<>(Arrays.asList(
                new UUID(6453290794183652199l, -7855891324758780007l),
                new UUID(- 2330906535205672779l, 3380397291928878215l),
                new UUID(- 7477072015556504347l, -4759461241505446441l),
                new UUID(8159020250014517468l, -3276528637398377287l),
                new UUID(2954576207831929l, -7401919710944613243l),
                new UUID(- 6464087018820727044l, 9158344342526998714l),
                new UUID(272555917829435557l, -502732256918155454l),
                new UUID(- 2439663895838265701l, 5432637156828437242l),
                new UUID(6906448715414564879l, -3377628232451089806l)
        ));
        assertEquals(memberIds, spectrum.getMemberIds());

        assertEquals(100.0,spectrum.getRetentionTime().get().getTime(),0.0001);

        assertEquals(Sets.newLinkedHashSet(Lists.newArrayList("INS", "PIR52346")), spectrum.getProteinAccessionNumbers());
    }

    @Test
    public void testAnnotWriteAndRead() throws Exception {

            UUID[] spectraIds = new UUID[]{
                    new UUID(6453290794183652199l, -7855891324758780007l)
            };

            PeptideConsensusSpectrum spectrumIn = new PeptideConsensusSpectrum(Peptide.parse("PEPTIDE"),new HashSet<UUID>());

            spectrumIn.setSpectrumSource(new URIBuilder("www.expasy.org", "liberator").build());
            spectrumIn.setComment("comment");
            spectrumIn.setStatus(PeptideConsensusSpectrum.Status.NORMAL);
            spectrumIn.addMemberIds(Arrays.asList(spectraIds));
            spectrumIn.setName("name");
            spectrumIn.setPrecursorStats(3412.96, 0.01);
            spectrumIn.setScoreStats(0.8, 0.2);
            spectrumIn.addProteinAccessionNumbers("INS", "PIR52346");
            spectrumIn.setId(UUID.fromString("5f7f12d4-43ab-4cc0-ab79-b2d3647b5973"));
            spectrumIn.setMsLevel(2);
            spectrumIn.getPrecursor().setValues(1789.98765, 875.68, 2);

            PeptideFragment fragment = PeptideFragment.parse("CAR", FragmentType.INTERNAL);
            PepFragAnnotation annotation1 = new PepFragAnnotation.Builder(IonType.y, 2, fragment).build();
            PepFragAnnotation annotation2 = new PepFragAnnotation.Builder(IonType.ay_internal, -2, fragment).addC13(2).setNeutralLoss(new NumericMass(0.001)).build();

            spectrumIn.add(12.93234579876, 145.258741256354125);
            spectrumIn.add(58.92698543776, 145.258741256354125, new PepLibPeakAnnotation(10,1.0, 10.0));
            spectrumIn.add(152.956783276, 145.258741256354125, new PepLibPeakAnnotation(10,1.0, 10.0,Optional.of(annotation1)));
            spectrumIn.add(155.956783276, 100.000, new PepLibPeakAnnotation(10,1.0, 10.0,Optional.of(annotation2)));
            spectrumIn.setRetentionTime(Optional.of(new RetentionTimeDiscrete(100.0, TimeUnit.SECOND)));

            PeptideConsensusSpectrumWriter writer = new PeptideConsensusSpectrumWriter(Optional.<PeakList.Precision>absent());

            StringWriter out = new StringWriter();
            JsonGenerator gen = new JsonFactory().createJsonGenerator(out);
            gen.setPrettyPrinter(new DefaultPrettyPrinter());
            Encoder encoder = EncoderFactory.get().jsonEncoder(writer.createSchema(), gen);
            writer.write(spectrumIn, encoder);
            encoder.flush();
            PeptideConsensusSpectrumReader reader = new PeptideConsensusSpectrumReader(
                    Optional.<PeakList.Precision>absent(),
                    Collections.<PeakProcessor<PepLibPeakAnnotation, PepLibPeakAnnotation>>emptyList());

            Decoder in = DecoderFactory.get().jsonDecoder(reader.createSchema(), out.toString());

            PeptideConsensusSpectrum spectrumOut = reader.read(in);

            Assert.assertEquals(spectrumIn.getMsLevel(), spectrumOut.getMsLevel());
            Assert.assertEquals(spectrumIn.getPeptide(), spectrumOut.getPeptide());
            Assert.assertEquals(spectrumIn.getComment(), spectrumOut.getComment());

            assertEquals(spectrumIn.size(), spectrumOut.size());
            assertEquals(spectrumIn.getMz(0), spectrumOut.getMz(0), 0.00001);
            assertEquals(spectrumIn.getIntensity(0), spectrumOut.getIntensity(0), 0.00001);
            assertEquals(spectrumIn.getMz(1), spectrumOut.getMz(1), 0.00001);
            assertEquals(spectrumIn.getIntensity(1), spectrumOut.getIntensity(1), 0.00001);
            assertEquals(spectrumIn.getMz(2), spectrumOut.getMz(2), 0.00001);
            assertEquals(spectrumIn.getIntensity(2), spectrumOut.getIntensity(2), 0.00001);

            assertEquals(spectrumIn.getPrecursor(), spectrumOut.getPrecursor());

            assertEquals(spectrumIn.getId(), spectrumOut.getId());
            assertEquals(spectrumIn.getSpectrumSource().toString(), spectrumOut.getSpectrumSource().toString());

            assertEquals(spectrumIn.getMemberIds(), spectrumOut.getMemberIds());

            assertEquals(spectrumIn.getRetentionTime().get().getTime(),spectrumOut.getRetentionTime().get().getTime(),0.0001);

            assertEquals(spectrumIn.getProteinAccessionNumbers(), spectrumOut.getProteinAccessionNumbers());
    }

    @Test
    public void testCreateSchema() throws Exception {

        Schema expected = new Schema.Parser().parse("{\n" +
                "  \"type\" : \"record\",\n" +
                "  \"name\" : \"PeptideConsensusSpectrum\",\n" +
                "  \"namespace\" : \"org.expasy.mzjava_avro.proteomics.ms.consensus\",\n" +
                "  \"fields\" : [ {\n" +
                "    \"name\" : \"precursor\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"Peak\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"polarity\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"enum\",\n" +
                "          \"name\" : \"Polarity\",\n" +
                "          \"namespace\" : \"org.expasy.mzjava.core.ms.peaklist\",\n" +
                "          \"symbols\" : [ \"POSITIVE\", \"NEGATIVE\", \"UNKNOWN\" ]\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"charge\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : \"int\"\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"mz\",\n" +
                "        \"type\" : \"double\"\n" +
                "      }, {\n" +
                "        \"name\" : \"intensity\",\n" +
                "        \"type\" : \"double\"\n" +
                "      } ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"msLevel\",\n" +
                "    \"type\" : \"int\"\n" +
                "  }, {\n" +
                "    \"name\" : \"name\",\n" +
                "    \"type\" : \"string\"\n" +
                "  }, {\n" +
                "    \"name\" : \"simScoreMean\",\n" +
                "    \"type\" : \"double\"\n" +
                "  }, {\n" +
                "    \"name\" : \"simScoreStdev\",\n" +
                "    \"type\" : \"double\"\n" +
                "  }, {\n" +
                "    \"name\" : \"precursorMzMean\",\n" +
                "    \"type\" : \"double\"\n" +
                "  }, {\n" +
                "    \"name\" : \"precursorMzStdev\",\n" +
                "    \"type\" : \"double\"\n" +
                "  }, {\n" +
                "    \"name\" : \"memberIds\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"array\",\n" +
                "      \"items\" : {\n" +
                "        \"type\" : \"record\",\n" +
                "        \"name\" : \"UUID\",\n" +
                "        \"namespace\" : \"java.util\",\n" +
                "        \"fields\" : [ {\n" +
                "          \"name\" : \"mostSignificantBits\",\n" +
                "          \"type\" : \"long\"\n" +
                "        }, {\n" +
                "          \"name\" : \"leastSignificantBits\",\n" +
                "          \"type\" : \"long\"\n" +
                "        } ]\n" +
                "      }\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"peptide\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"Peptide\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.proteomics.mol\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"seq\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"enum\",\n" +
                "            \"name\" : \"AminoAcid\",\n" +
                "            \"namespace\" : \"org.expasy.mzjava.proteomics.mol\",\n" +
                "            \"symbols\" : [ \"A\", \"R\", \"N\", \"D\", \"C\", \"E\", \"Q\", \"G\", \"H\", \"K\", \"M\", \"F\", \"P\", \"S\", \"T\", \"W\", \"Y\", \"V\", \"J\", \"I\", \"L\", \"O\", \"U\", \"X\", \"B\", \"Z\" ]\n" +
                "          }\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"sideChainModMap\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"SideChainModRecord\",\n" +
                "            \"namespace\" : \"org.expasy.mzjava_avro.proteomics.mol.modification\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"index\",\n" +
                "              \"type\" : \"int\"\n" +
                "            }, {\n" +
                "              \"name\" : \"modification\",\n" +
                "              \"type\" : {\n" +
                "                \"type\" : \"record\",\n" +
                "                \"name\" : \"Modification\",\n" +
                "                \"fields\" : [ {\n" +
                "                  \"name\" : \"label\",\n" +
                "                  \"type\" : \"string\"\n" +
                "                }, {\n" +
                "                  \"name\" : \"mass\",\n" +
                "                  \"type\" : [ {\n" +
                "                    \"type\" : \"record\",\n" +
                "                    \"name\" : \"NumericMass\",\n" +
                "                    \"namespace\" : \"org.expasy.mzjava_avro.core.mol\",\n" +
                "                    \"fields\" : [ {\n" +
                "                      \"name\" : \"mass\",\n" +
                "                      \"type\" : \"double\"\n" +
                "                    } ]\n" +
                "                  }, {\n" +
                "                    \"type\" : \"record\",\n" +
                "                    \"name\" : \"Composition\",\n" +
                "                    \"namespace\" : \"org.expasy.mzjava_avro.core.mol\",\n" +
                "                    \"fields\" : [ {\n" +
                "                      \"name\" : \"composition\",\n" +
                "                      \"type\" : {\n" +
                "                        \"type\" : \"map\",\n" +
                "                        \"values\" : \"int\"\n" +
                "                      }\n" +
                "                    } ]\n" +
                "                  } ]\n" +
                "                } ]\n" +
                "              }\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"termModMap\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"TermModRecord\",\n" +
                "            \"namespace\" : \"org.expasy.mzjava_avro.proteomics.mol.modification\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"attachment\",\n" +
                "              \"type\" : {\n" +
                "                \"type\" : \"enum\",\n" +
                "                \"name\" : \"ModAttachment\",\n" +
                "                \"namespace\" : \"org.expasy.mzjava.proteomics.mol.modification\",\n" +
                "                \"symbols\" : [ \"SIDE_CHAIN\", \"N_TERM\", \"C_TERM\" ]\n" +
                "              }\n" +
                "            }, {\n" +
                "              \"name\" : \"modification\",\n" +
                "              \"type\" : \"Modification\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"proteinAccessionNumbers\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"array\",\n" +
                "      \"items\" : \"string\"\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"source\",\n" +
                "    \"type\" : \"string\"\n" +
                "  }, {\n" +
                "    \"name\" : \"comment\",\n" +
                "    \"type\" : \"string\"\n" +
                "  }, {\n" +
                "    \"name\" : \"status\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"enum\",\n" +
                "      \"name\" : \"Status\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava.proteomics.ms.consensus\",\n" +
                "      \"symbols\" : [ \"UNKNOWN\", \"SINGLETON\", \"NORMAL\", \"INQUORATE_UNCONFIRMED\", \"CONFLICTING_ID\", \"IMPURE\", \"DECOY\" ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"retentionTime\",\n" +
                "    \"type\" : [ {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"RetentionTimeDiscrete\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.spectrum\",\n" +
                "      \"doc\" : \"Retention time in seconds\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"time\",\n" +
                "        \"type\" : \"double\"\n" +
                "      } ]\n" +
                "    }, \"null\" ]\n" +
                "  }, {\n" +
                "    \"name\" : \"id\",\n" +
                "    \"type\" : \"java.util.UUID\"\n" +
                "  }, {\n" +
                "    \"name\" : \"precision\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"enum\",\n" +
                "      \"name\" : \"Precision\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava.core.ms.peaklist\",\n" +
                "      \"symbols\" : [ \"DOUBLE\", \"FLOAT\", \"DOUBLE_FLOAT\", \"DOUBLE_CONSTANT\", \"FLOAT_CONSTANT\" ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"peaks\",\n" +
                "    \"type\" : [ {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"DoublePeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"DoublePeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"double\"\n" +
                "            }, {\n" +
                "              \"name\" : \"i\",\n" +
                "              \"type\" : \"double\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"FloatPeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"FloatPeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"float\"\n" +
                "            }, {\n" +
                "              \"name\" : \"i\",\n" +
                "              \"type\" : \"float\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"DoubleFloatPeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"DoubleFloatPeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"double\"\n" +
                "            }, {\n" +
                "              \"name\" : \"i\",\n" +
                "              \"type\" : \"float\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"DoubleConstantPeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"intensity\",\n" +
                "        \"type\" : \"double\"\n" +
                "      }, {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"DoubleConstantPeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"double\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"FloatConstantPeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"intensity\",\n" +
                "        \"type\" : \"double\"\n" +
                "      }, {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"FloatConstantPeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"float\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"AnnotatedDoublePeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"AnnotatedDoublePeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"double\"\n" +
                "            }, {\n" +
                "              \"name\" : \"i\",\n" +
                "              \"type\" : \"double\"\n" +
                "            }, {\n" +
                "              \"name\" : \"annotations\",\n" +
                "              \"type\" : {\n" +
                "                \"type\" : \"array\",\n" +
                "                \"items\" : [ {\n" +
                "                  \"type\" : \"record\",\n" +
                "                  \"name\" : \"PepLibPeakAnnotation\",\n" +
                "                  \"namespace\" : \"org.expasy.mzjava_avro.proteomics.ms.spectrum\",\n" +
                "                  \"fields\" : [ {\n" +
                "                    \"name\" : \"mergedPeakCount\",\n" +
                "                    \"type\" : \"int\"\n" +
                "                  }, {\n" +
                "                    \"name\" : \"mzStd\",\n" +
                "                    \"type\" : \"double\"\n" +
                "                  }, {\n" +
                "                    \"name\" : \"intensityStd\",\n" +
                "                    \"type\" : \"double\"\n" +
                "                  }, {\n" +
                "                    \"name\" : \"fragAnnotation\",\n" +
                "                    \"type\" : [ {\n" +
                "                      \"type\" : \"record\",\n" +
                "                      \"name\" : \"PepFragAnnotation\",\n" +
                "                      \"fields\" : [ {\n" +
                "                        \"name\" : \"ionType\",\n" +
                "                        \"type\" : {\n" +
                "                          \"type\" : \"enum\",\n" +
                "                          \"name\" : \"IonType\",\n" +
                "                          \"namespace\" : \"org.expasy.mzjava.core.ms.spectrum\",\n" +
                "                          \"symbols\" : [ \"a\", \"b\", \"c\", \"d\", \"w\", \"x\", \"y\", \"z\", \"aw_internal\", \"ax_internal\", \"ay_internal\", \"az_internal\", \"bw_internal\", \"bx_internal\", \"by_internal\", \"bz_internal\", \"cw_internal\", \"cx_internal\", \"cy_internal\", \"cz_internal\", \"dw_internal\", \"dx_internal\", \"dy_internal\", \"dz_internal\", \"i\", \"p\", \"unknown\" ]\n" +
                "                        }\n" +
                "                      }, {\n" +
                "                        \"name\" : \"isotopeComposition\",\n" +
                "                        \"type\" : \"org.expasy.mzjava_avro.core.mol.Composition\"\n" +
                "                      }, {\n" +
                "                        \"name\" : \"charge\",\n" +
                "                        \"type\" : \"int\"\n" +
                "                      }, {\n" +
                "                        \"name\" : \"peptideFragment\",\n" +
                "                        \"type\" : {\n" +
                "                          \"type\" : \"record\",\n" +
                "                          \"name\" : \"PeptideFragment\",\n" +
                "                          \"namespace\" : \"org.expasy.mzjava_avro.proteomics.mol\",\n" +
                "                          \"fields\" : [ {\n" +
                "                            \"name\" : \"fragmentType\",\n" +
                "                            \"type\" : {\n" +
                "                              \"type\" : \"enum\",\n" +
                "                              \"name\" : \"FragmentType\",\n" +
                "                              \"namespace\" : \"org.expasy.mzjava.core.ms.spectrum\",\n" +
                "                              \"symbols\" : [ \"FORWARD\", \"REVERSE\", \"UNKNOWN\", \"INTERNAL\", \"MONOMER\", \"INTACT\" ]\n" +
                "                            }\n" +
                "                          }, {\n" +
                "                            \"name\" : \"seq\",\n" +
                "                            \"type\" : {\n" +
                "                              \"type\" : \"array\",\n" +
                "                              \"items\" : \"org.expasy.mzjava.proteomics.mol.AminoAcid\"\n" +
                "                            }\n" +
                "                          }, {\n" +
                "                            \"name\" : \"sideChainModMap\",\n" +
                "                            \"type\" : {\n" +
                "                              \"type\" : \"array\",\n" +
                "                              \"items\" : \"org.expasy.mzjava_avro.proteomics.mol.modification.SideChainModRecord\"\n" +
                "                            }\n" +
                "                          }, {\n" +
                "                            \"name\" : \"termModMap\",\n" +
                "                            \"type\" : {\n" +
                "                              \"type\" : \"array\",\n" +
                "                              \"items\" : \"org.expasy.mzjava_avro.proteomics.mol.modification.TermModRecord\"\n" +
                "                            }\n" +
                "                          } ]\n" +
                "                        }\n" +
                "                      }, {\n" +
                "                        \"name\" : \"neutralLoss\",\n" +
                "                        \"type\" : [ \"org.expasy.mzjava_avro.core.mol.NumericMass\", \"org.expasy.mzjava_avro.core.mol.Composition\" ]\n" +
                "                      } ]\n" +
                "                    }, \"null\" ]\n" +
                "                  } ]\n" +
                "                } ]\n" +
                "              }\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"AnnotatedFloatPeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"AnnotatedFloatPeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"float\"\n" +
                "            }, {\n" +
                "              \"name\" : \"i\",\n" +
                "              \"type\" : \"float\"\n" +
                "            }, {\n" +
                "              \"name\" : \"annotations\",\n" +
                "              \"type\" : {\n" +
                "                \"type\" : \"array\",\n" +
                "                \"items\" : [ \"org.expasy.mzjava_avro.proteomics.ms.spectrum.PepLibPeakAnnotation\" ]\n" +
                "              }\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"AnnotatedDoubleFloatPeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"AnnotatedDoubleFloatPeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"double\"\n" +
                "            }, {\n" +
                "              \"name\" : \"i\",\n" +
                "              \"type\" : \"float\"\n" +
                "            }, {\n" +
                "              \"name\" : \"annotations\",\n" +
                "              \"type\" : {\n" +
                "                \"type\" : \"array\",\n" +
                "                \"items\" : [ \"org.expasy.mzjava_avro.proteomics.ms.spectrum.PepLibPeakAnnotation\" ]\n" +
                "              }\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"AnnotatedDoubleConstantPeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"intensity\",\n" +
                "        \"type\" : \"double\"\n" +
                "      }, {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"AnnotatedDoubleConstantPeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"double\"\n" +
                "            }, {\n" +
                "              \"name\" : \"annotations\",\n" +
                "              \"type\" : {\n" +
                "                \"type\" : \"array\",\n" +
                "                \"items\" : [ \"org.expasy.mzjava_avro.proteomics.ms.spectrum.PepLibPeakAnnotation\" ]\n" +
                "              }\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"AnnotatedFloatConstantPeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"intensity\",\n" +
                "        \"type\" : \"double\"\n" +
                "      }, {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"AnnotatedFloatConstantPeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"float\"\n" +
                "            }, {\n" +
                "              \"name\" : \"annotations\",\n" +
                "              \"type\" : {\n" +
                "                \"type\" : \"array\",\n" +
                "                \"items\" : [ \"org.expasy.mzjava_avro.proteomics.ms.spectrum.PepLibPeakAnnotation\" ]\n" +
                "              }\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    } ]\n" +
                "  } ]\n" +
                "}");

        String actual = new PeptideConsensusSpectrumReader(
                Optional.<PeakList.Precision>absent(),
                Collections.<PeakProcessor<PepLibPeakAnnotation, PepLibPeakAnnotation>>emptyList()).createSchema().toString(true);
        Assert.assertEquals(expected.toString(true), new Schema.Parser().parse(actual).toString(true));
    }
}
