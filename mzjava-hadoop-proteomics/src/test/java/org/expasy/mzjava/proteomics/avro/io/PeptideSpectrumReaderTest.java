/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.avro.io;

import com.google.common.base.Optional;
import org.apache.avro.Schema;
import org.apache.avro.io.Decoder;
import org.apache.avro.io.DecoderFactory;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessor;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.proteomics.avro.io.PeptideSpectrumReader;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.ms.spectrum.PepFragAnnotation;
import org.expasy.mzjava.proteomics.ms.spectrum.PeptideSpectrum;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.UUID;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class PeptideSpectrumReaderTest {

    @Test
    public void testRead() throws Exception {

        PeptideSpectrumReader reader = new PeptideSpectrumReader(
                Optional.<PeakList.Precision>absent(),
                Collections.<PeakProcessor<PepFragAnnotation, PepFragAnnotation>>emptyList());

        Decoder in = DecoderFactory.get().jsonDecoder(reader.createSchema(), "{\n" +
                "  \"precursor\" : {\n" +
                "    \"polarity\" : \"POSITIVE\",\n" +
                "    \"charge\" : [ 2 ],\n" +
                "    \"mz\" : 1789.98765,\n" +
                "    \"intensity\" : 875.68\n" +
                "  },\n" +
                "  \"msLevel\" : 2,\n" +
                "  \"charge\" : 2,\n" +
                "  \"peptide\" : {\n" +
                "    \"seq\" : [ \"P\", \"E\", \"P\", \"T\", \"I\", \"D\", \"E\" ],\n" +
                "    \"sideChainModMap\" : [ ],\n" +
                "    \"termModMap\" : [ ]\n" +
                "  },\n" +
                "  \"proteinAccessionNumbers\" : [ \"INS\", \"PIR52346\" ],\n" +
                "  \"id\" : {\n" +
                "    \"mostSignificantBits\" : -2693133135828597770,\n" +
                "    \"leastSignificantBits\" : -4936060129817699777\n" +
                "  },\n" +
                "  \"precision\" : \"DOUBLE\",\n" +
                "  \"peaks\" : {\n" +
                "    \"org.expasy.mzjava_avro.core.ms.peaklist.DoublePeakList\" : {\n" +
                "      \"peaks\" : [ {\n" +
                "        \"mz\" : 12.93234579876,\n" +
                "        \"i\" : 145.2587412563541\n" +
                "      }, {\n" +
                "        \"mz\" : 58.92698543776,\n" +
                "        \"i\" : 145.2587412563541\n" +
                "      }, {\n" +
                "        \"mz\" : 152.956783276,\n" +
                "        \"i\" : 145.2587412563541\n" +
                "      } ]\n" +
                "    }\n" +
                "  }\n" +
                "}");
        PeptideSpectrum spectrum = reader.read(in);

        Assert.assertEquals(2, spectrum.getMsLevel());
        Assert.assertEquals(Peptide.parse("PEPTIDE"), spectrum.getPeptide());
        Assert.assertEquals(2, spectrum.getCharge());

        Assert.assertEquals(new Peak(1789.98765, 875.68, 2),spectrum.getPrecursor());
        Assert.assertEquals(UUID.fromString("daa011ae-8a1a-43f6-bb7f-9776dc3f5a3f"), spectrum.getId());
        Assert.assertEquals(12.93234579876, spectrum.getMz(0), 0.0000001);
        Assert.assertEquals(145.258741256354125, spectrum.getIntensity(0), 0.0000001);

        Assert.assertEquals(58.92698543776, spectrum.getMz(1), 0.0000001);
        Assert.assertEquals(145.258741256354125, spectrum.getIntensity(1), 0.0000001);

        Assert.assertEquals(152.956783276, spectrum.getMz(2), 0.0000001);
        Assert.assertEquals(145.258741256354125, spectrum.getIntensity(2), 0.0000001);
    }

    @Test
    public void testCreateSchema() throws Exception {

        Schema expected = new Schema.Parser().parse("{\n" +
                "  \"type\" : \"record\",\n" +
                "  \"name\" : \"PeptideSpectrum\",\n" +
                "  \"namespace\" : \"org.expasy.mzjava_avro.proteomics.ms.spectrum\",\n" +
                "  \"fields\" : [ {\n" +
                "    \"name\" : \"precursor\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"Peak\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"polarity\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"enum\",\n" +
                "          \"name\" : \"Polarity\",\n" +
                "          \"namespace\" : \"org.expasy.mzjava.core.ms.peaklist\",\n" +
                "          \"symbols\" : [ \"POSITIVE\", \"NEGATIVE\", \"UNKNOWN\" ]\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"charge\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : \"int\"\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"mz\",\n" +
                "        \"type\" : \"double\"\n" +
                "      }, {\n" +
                "        \"name\" : \"intensity\",\n" +
                "        \"type\" : \"double\"\n" +
                "      } ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"msLevel\",\n" +
                "    \"type\" : \"int\"\n" +
                "  }, {\n" +
                "    \"name\" : \"charge\",\n" +
                "    \"type\" : \"int\"\n" +
                "  }, {\n" +
                "    \"name\" : \"peptide\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"Peptide\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.proteomics.mol\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"seq\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"enum\",\n" +
                "            \"name\" : \"AminoAcid\",\n" +
                "            \"namespace\" : \"org.expasy.mzjava.proteomics.mol\",\n" +
                "            \"symbols\" : [ \"A\", \"R\", \"N\", \"D\", \"C\", \"E\", \"Q\", \"G\", \"H\", \"K\", \"M\", \"F\", \"P\", \"S\", \"T\", \"W\", \"Y\", \"V\", \"J\", \"I\", \"L\", \"O\", \"U\", \"X\", \"B\", \"Z\" ]\n" +
                "          }\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"sideChainModMap\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"SideChainModRecord\",\n" +
                "            \"namespace\" : \"org.expasy.mzjava_avro.proteomics.mol.modification\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"index\",\n" +
                "              \"type\" : \"int\"\n" +
                "            }, {\n" +
                "              \"name\" : \"modification\",\n" +
                "              \"type\" : {\n" +
                "                \"type\" : \"record\",\n" +
                "                \"name\" : \"Modification\",\n" +
                "                \"fields\" : [ {\n" +
                "                  \"name\" : \"label\",\n" +
                "                  \"type\" : \"string\"\n" +
                "                }, {\n" +
                "                  \"name\" : \"mass\",\n" +
                "                  \"type\" : [ {\n" +
                "                    \"type\" : \"record\",\n" +
                "                    \"name\" : \"NumericMass\",\n" +
                "                    \"namespace\" : \"org.expasy.mzjava_avro.core.mol\",\n" +
                "                    \"fields\" : [ {\n" +
                "                      \"name\" : \"mass\",\n" +
                "                      \"type\" : \"double\"\n" +
                "                    } ]\n" +
                "                  }, {\n" +
                "                    \"type\" : \"record\",\n" +
                "                    \"name\" : \"Composition\",\n" +
                "                    \"namespace\" : \"org.expasy.mzjava_avro.core.mol\",\n" +
                "                    \"fields\" : [ {\n" +
                "                      \"name\" : \"composition\",\n" +
                "                      \"type\" : {\n" +
                "                        \"type\" : \"map\",\n" +
                "                        \"values\" : \"int\"\n" +
                "                      }\n" +
                "                    } ]\n" +
                "                  } ]\n" +
                "                } ]\n" +
                "              }\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"termModMap\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"TermModRecord\",\n" +
                "            \"namespace\" : \"org.expasy.mzjava_avro.proteomics.mol.modification\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"attachment\",\n" +
                "              \"type\" : {\n" +
                "                \"type\" : \"enum\",\n" +
                "                \"name\" : \"ModAttachment\",\n" +
                "                \"namespace\" : \"org.expasy.mzjava.proteomics.mol.modification\",\n" +
                "                \"symbols\" : [ \"SIDE_CHAIN\", \"N_TERM\", \"C_TERM\" ]\n" +
                "              }\n" +
                "            }, {\n" +
                "              \"name\" : \"modification\",\n" +
                "              \"type\" : \"Modification\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"proteinAccessionNumbers\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"array\",\n" +
                "      \"items\" : \"string\"\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"id\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"UUID\",\n" +
                "      \"namespace\" : \"java.util\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"mostSignificantBits\",\n" +
                "        \"type\" : \"long\"\n" +
                "      }, {\n" +
                "        \"name\" : \"leastSignificantBits\",\n" +
                "        \"type\" : \"long\"\n" +
                "      } ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"precision\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"enum\",\n" +
                "      \"name\" : \"Precision\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava.core.ms.peaklist\",\n" +
                "      \"symbols\" : [ \"DOUBLE\", \"FLOAT\", \"DOUBLE_FLOAT\", \"DOUBLE_CONSTANT\", \"FLOAT_CONSTANT\" ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"peaks\",\n" +
                "    \"type\" : [ {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"DoublePeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"DoublePeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"double\"\n" +
                "            }, {\n" +
                "              \"name\" : \"i\",\n" +
                "              \"type\" : \"double\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"FloatPeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"FloatPeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"float\"\n" +
                "            }, {\n" +
                "              \"name\" : \"i\",\n" +
                "              \"type\" : \"float\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"DoubleFloatPeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"DoubleFloatPeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"double\"\n" +
                "            }, {\n" +
                "              \"name\" : \"i\",\n" +
                "              \"type\" : \"float\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"DoubleConstantPeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"intensity\",\n" +
                "        \"type\" : \"double\"\n" +
                "      }, {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"DoubleConstantPeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"double\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"FloatConstantPeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"intensity\",\n" +
                "        \"type\" : \"double\"\n" +
                "      }, {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"FloatConstantPeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"float\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"AnnotatedDoublePeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"AnnotatedDoublePeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"double\"\n" +
                "            }, {\n" +
                "              \"name\" : \"i\",\n" +
                "              \"type\" : \"double\"\n" +
                "            }, {\n" +
                "              \"name\" : \"annotations\",\n" +
                "              \"type\" : {\n" +
                "                \"type\" : \"array\",\n" +
                "                \"items\" : [ {\n" +
                "                  \"type\" : \"record\",\n" +
                "                  \"name\" : \"PepFragAnnotation\",\n" +
                "                  \"namespace\" : \"org.expasy.mzjava_avro.proteomics.ms.spectrum\",\n" +
                "                  \"fields\" : [ {\n" +
                "                    \"name\" : \"ionType\",\n" +
                "                    \"type\" : {\n" +
                "                      \"type\" : \"enum\",\n" +
                "                      \"name\" : \"IonType\",\n" +
                "                      \"namespace\" : \"org.expasy.mzjava.core.ms.spectrum\",\n" +
                "                      \"symbols\" : [ \"a\", \"b\", \"c\", \"d\", \"w\", \"x\", \"y\", \"z\", \"aw_internal\", \"ax_internal\", \"ay_internal\", \"az_internal\", \"bw_internal\", \"bx_internal\", \"by_internal\", \"bz_internal\", \"cw_internal\", \"cx_internal\", \"cy_internal\", \"cz_internal\", \"dw_internal\", \"dx_internal\", \"dy_internal\", \"dz_internal\", \"i\", \"p\", \"unknown\" ]\n" +
                "                    }\n" +
                "                  }, {\n" +
                "                    \"name\" : \"isotopeComposition\",\n" +
                "                    \"type\" : \"org.expasy.mzjava_avro.core.mol.Composition\"\n" +
                "                  }, {\n" +
                "                    \"name\" : \"charge\",\n" +
                "                    \"type\" : \"int\"\n" +
                "                  }, {\n" +
                "                    \"name\" : \"peptideFragment\",\n" +
                "                    \"type\" : {\n" +
                "                      \"type\" : \"record\",\n" +
                "                      \"name\" : \"PeptideFragment\",\n" +
                "                      \"namespace\" : \"org.expasy.mzjava_avro.proteomics.mol\",\n" +
                "                      \"fields\" : [ {\n" +
                "                        \"name\" : \"fragmentType\",\n" +
                "                        \"type\" : {\n" +
                "                          \"type\" : \"enum\",\n" +
                "                          \"name\" : \"FragmentType\",\n" +
                "                          \"namespace\" : \"org.expasy.mzjava.core.ms.spectrum\",\n" +
                "                          \"symbols\" : [ \"FORWARD\", \"REVERSE\", \"UNKNOWN\", \"INTERNAL\", \"MONOMER\", \"INTACT\" ]\n" +
                "                        }\n" +
                "                      }, {\n" +
                "                        \"name\" : \"seq\",\n" +
                "                        \"type\" : {\n" +
                "                          \"type\" : \"array\",\n" +
                "                          \"items\" : \"org.expasy.mzjava.proteomics.mol.AminoAcid\"\n" +
                "                        }\n" +
                "                      }, {\n" +
                "                        \"name\" : \"sideChainModMap\",\n" +
                "                        \"type\" : {\n" +
                "                          \"type\" : \"array\",\n" +
                "                          \"items\" : \"org.expasy.mzjava_avro.proteomics.mol.modification.SideChainModRecord\"\n" +
                "                        }\n" +
                "                      }, {\n" +
                "                        \"name\" : \"termModMap\",\n" +
                "                        \"type\" : {\n" +
                "                          \"type\" : \"array\",\n" +
                "                          \"items\" : \"org.expasy.mzjava_avro.proteomics.mol.modification.TermModRecord\"\n" +
                "                        }\n" +
                "                      } ]\n" +
                "                    }\n" +
                "                  }, {\n" +
                "                    \"name\" : \"neutralLoss\",\n" +
                "                    \"type\" : [ \"org.expasy.mzjava_avro.core.mol.NumericMass\", \"org.expasy.mzjava_avro.core.mol.Composition\" ]\n" +
                "                  } ]\n" +
                "                } ]\n" +
                "              }\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"AnnotatedFloatPeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"AnnotatedFloatPeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"float\"\n" +
                "            }, {\n" +
                "              \"name\" : \"i\",\n" +
                "              \"type\" : \"float\"\n" +
                "            }, {\n" +
                "              \"name\" : \"annotations\",\n" +
                "              \"type\" : {\n" +
                "                \"type\" : \"array\",\n" +
                "                \"items\" : [ \"org.expasy.mzjava_avro.proteomics.ms.spectrum.PepFragAnnotation\" ]\n" +
                "              }\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"AnnotatedDoubleFloatPeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"AnnotatedDoubleFloatPeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"double\"\n" +
                "            }, {\n" +
                "              \"name\" : \"i\",\n" +
                "              \"type\" : \"float\"\n" +
                "            }, {\n" +
                "              \"name\" : \"annotations\",\n" +
                "              \"type\" : {\n" +
                "                \"type\" : \"array\",\n" +
                "                \"items\" : [ \"org.expasy.mzjava_avro.proteomics.ms.spectrum.PepFragAnnotation\" ]\n" +
                "              }\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"AnnotatedDoubleConstantPeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"intensity\",\n" +
                "        \"type\" : \"double\"\n" +
                "      }, {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"AnnotatedDoubleConstantPeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"double\"\n" +
                "            }, {\n" +
                "              \"name\" : \"annotations\",\n" +
                "              \"type\" : {\n" +
                "                \"type\" : \"array\",\n" +
                "                \"items\" : [ \"org.expasy.mzjava_avro.proteomics.ms.spectrum.PepFragAnnotation\" ]\n" +
                "              }\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"AnnotatedFloatConstantPeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"intensity\",\n" +
                "        \"type\" : \"double\"\n" +
                "      }, {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"AnnotatedFloatConstantPeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"float\"\n" +
                "            }, {\n" +
                "              \"name\" : \"annotations\",\n" +
                "              \"type\" : {\n" +
                "                \"type\" : \"array\",\n" +
                "                \"items\" : [ \"org.expasy.mzjava_avro.proteomics.ms.spectrum.PepFragAnnotation\" ]\n" +
                "              }\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    } ]\n" +
                "  } ]\n" +
                "}");

        String actual = new PeptideSpectrumReader(
                Optional.<PeakList.Precision>absent(),
                Collections.<PeakProcessor<PepFragAnnotation, PepFragAnnotation>>emptyList()).createSchema().toString(true);

        Assert.assertEquals(expected.toString(true),
                new Schema.Parser().parse(actual).toString(true));
    }
}
