/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.avro.io;

import org.apache.avro.Schema;
import org.apache.avro.io.Encoder;
import org.apache.avro.io.EncoderFactory;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.expasy.mzjava.proteomics.avro.io.PeptideWriter;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.junit.Assert;
import org.junit.Test;

import java.io.StringWriter;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class PeptideWriterTest {

    @Test
    public void testSeqOnly() throws Exception {

        StringWriter out = new StringWriter();
        JsonGenerator gen = new JsonFactory().createJsonGenerator(out);
        PeptideWriter writer = new PeptideWriter();
        Encoder encoder = EncoderFactory.get().jsonEncoder(writer.createSchema(), gen);
        writer.write(Peptide.parse("MQRSTATGCFKLX"), encoder);
        encoder.flush();

        Assert.assertEquals("{\"seq\":[\"M\",\"Q\",\"R\",\"S\",\"T\",\"A\",\"T\",\"G\",\"C\",\"F\",\"K\",\"L\",\"X\"],\"sideChainModMap\":[],\"termModMap\":[]}",
                out.toString());
    }

    @Test
    public void test() throws Exception {

        Peptide peptide = Peptide.parse("MQR(O2)STAT(C)GCFKLX");

        StringWriter out = new StringWriter();
        JsonGenerator gen = new JsonFactory().createJsonGenerator(out);
        PeptideWriter writer = new PeptideWriter();
        Encoder encoder = EncoderFactory.get().jsonEncoder(writer.createSchema(), gen);
        writer.write(peptide, encoder);
        encoder.flush();

        Assert.assertEquals("{" +
                "\"seq\":[\"M\",\"Q\",\"R\",\"S\",\"T\",\"A\",\"T\",\"G\",\"C\",\"F\",\"K\",\"L\",\"X\"]," +
                "\"sideChainModMap\":[" +
                    "{\"index\":2,\"modification\":{\"label\":\"O2\",\"mass\":{\"org.expasy.mzjava_avro.core.mol.Composition\":{\"composition\":{\"O\":2}}}}}," +
                    "{\"index\":6,\"modification\":{\"label\":\"C\",\"mass\":{\"org.expasy.mzjava_avro.core.mol.Composition\":{\"composition\":{\"C\":1}}}}}]," +
                "\"termModMap\":[]}", out.toString());
    }

    @Test
    public void test2() throws Exception {

        Peptide peptide = Peptide.parse("A(C3H9, C2H2O)CPLEK_(45.365)");

        StringWriter out = new StringWriter();
        JsonGenerator gen = new JsonFactory().createJsonGenerator(out);
        PeptideWriter writer = new PeptideWriter();
        Encoder encoder = EncoderFactory.get().jsonEncoder(writer.createSchema(), gen);
        writer.write(peptide, encoder);
        encoder.flush();

        Assert.assertEquals("{" +
                "\"seq\":[\"A\",\"C\",\"P\",\"L\",\"E\",\"K\"]," +
                "\"sideChainModMap\":[{\"index\":0,\"modification\":{\"label\":\"C3H9\",\"mass\":{\"org.expasy.mzjava_avro.core.mol.Composition\":{\"composition\":{\"H\":9,\"C\":3}}}}},{\"index\":0,\"modification\":{\"label\":\"C2H2O\",\"mass\":{\"org.expasy.mzjava_avro.core.mol.Composition\":{\"composition\":{\"H\":2,\"C\":2,\"O\":1}}}}}]," +
                "\"termModMap\":[{\"attachment\":\"C_TERM\",\"modification\":{\"label\":\"45.365\",\"mass\":{\"org.expasy.mzjava_avro.core.mol.NumericMass\":{\"mass\":45.365}}}}]}", out.toString());
    }

    @Test
    public void testSchema() throws Exception {

        String expected = "{\n" +
                "  \"type\" : \"record\",\n" +
                "  \"name\" : \"Peptide\",\n" +
                "  \"namespace\" : \"org.expasy.mzjava_avro.proteomics.mol\",\n" +
                "  \"fields\" : [ {\n" +
                "    \"name\" : \"seq\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"array\",\n" +
                "      \"items\" : {\n" +
                "        \"type\" : \"enum\",\n" +
                "        \"name\" : \"AminoAcid\",\n" +
                "        \"namespace\" : \"org.expasy.mzjava.proteomics.mol\",\n" +
                "        \"symbols\" : [ \"A\", \"R\", \"N\", \"D\", \"C\", \"E\", \"Q\", \"G\", \"H\", \"K\", \"M\", \"F\", \"P\", \"S\", \"T\", \"W\", \"Y\", \"V\", \"J\", \"I\", \"L\", \"O\", \"U\", \"X\", \"B\", \"Z\" ]\n" +
                "      }\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"sideChainModMap\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"array\",\n" +
                "      \"items\" : {\n" +
                "        \"type\" : \"record\",\n" +
                "        \"name\" : \"SideChainModRecord\",\n" +
                "        \"namespace\" : \"org.expasy.mzjava_avro.proteomics.mol.modification\",\n" +
                "        \"fields\" : [ {\n" +
                "          \"name\" : \"index\",\n" +
                "          \"type\" : \"int\"\n" +
                "        }, {\n" +
                "          \"name\" : \"modification\",\n" +
                "          \"type\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"Modification\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"label\",\n" +
                "              \"type\" : \"string\"\n" +
                "            }, {\n" +
                "              \"name\" : \"mass\",\n" +
                "              \"type\" : [ {\n" +
                "                \"type\" : \"record\",\n" +
                "                \"name\" : \"NumericMass\",\n" +
                "                \"namespace\" : \"org.expasy.mzjava_avro.core.mol\",\n" +
                "                \"fields\" : [ {\n" +
                "                  \"name\" : \"mass\",\n" +
                "                  \"type\" : \"double\"\n" +
                "                } ]\n" +
                "              }, {\n" +
                "                \"type\" : \"record\",\n" +
                "                \"name\" : \"Composition\",\n" +
                "                \"namespace\" : \"org.expasy.mzjava_avro.core.mol\",\n" +
                "                \"fields\" : [ {\n" +
                "                  \"name\" : \"composition\",\n" +
                "                  \"type\" : {\n" +
                "                    \"type\" : \"map\",\n" +
                "                    \"values\" : \"int\"\n" +
                "                  }\n" +
                "                } ]\n" +
                "              } ]\n" +
                "            } ]\n" +
                "          }\n" +
                "        } ]\n" +
                "      }\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"termModMap\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"array\",\n" +
                "      \"items\" : {\n" +
                "        \"type\" : \"record\",\n" +
                "        \"name\" : \"TermModRecord\",\n" +
                "        \"namespace\" : \"org.expasy.mzjava_avro.proteomics.mol.modification\",\n" +
                "        \"fields\" : [ {\n" +
                "          \"name\" : \"attachment\",\n" +
                "          \"type\" : {\n" +
                "            \"type\" : \"enum\",\n" +
                "            \"name\" : \"ModAttachment\",\n" +
                "            \"namespace\" : \"org.expasy.mzjava.proteomics.mol.modification\",\n" +
                "            \"symbols\" : [ \"SIDE_CHAIN\", \"N_TERM\", \"C_TERM\" ]\n" +
                "          }\n" +
                "        }, {\n" +
                "          \"name\" : \"modification\",\n" +
                "          \"type\" : \"Modification\"\n" +
                "        } ]\n" +
                "      }\n" +
                "    }\n" +
                "  } ]\n" +
                "}";

        Assert.assertEquals(new Schema.Parser().parse(expected).toString(true),
                new PeptideWriter().createSchema().toString(true));
    }
}
