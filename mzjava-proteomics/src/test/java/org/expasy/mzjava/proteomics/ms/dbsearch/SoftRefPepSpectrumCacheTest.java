/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.ms.dbsearch;

import com.google.common.base.Optional;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.ms.spectrum.PeptideSpectrum;
import org.junit.Assert;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class SoftRefPepSpectrumCacheTest {

    @Test
    public void test() throws Exception {

        PeptideSpectrumCache cache = new SoftRefPepSpectrumCache();

        Peptide peptide1 = mock(Peptide.class);
        PeptideSpectrum spectrum1 = mockSpectrum(peptide1, 1);

        Peptide peptide2 = mock(Peptide.class);
        PeptideSpectrum spectrum2 = mockSpectrum(peptide2, 2);


        cache.put(spectrum1);
        cache.put(spectrum2);

        Assert.assertSame(spectrum1, cache.get(peptide1, 1).get());
        Assert.assertSame(spectrum2, cache.get(peptide2, 2).get());
        Assert.assertEquals(Optional.<PeptideSpectrum>absent(), cache.get(mock(Peptide.class), 1));
        Assert.assertEquals(Optional.<PeptideSpectrum>absent(), cache.get(peptide1, 2));
        Assert.assertEquals(Optional.<PeptideSpectrum>absent(), cache.get(peptide2, 1));
        Assert.assertEquals(Optional.<PeptideSpectrum>absent(), cache.get(peptide2, 3));
    }

    private PeptideSpectrum mockSpectrum(Peptide peptide, int charge) {

        PeptideSpectrum spectrum = mock(PeptideSpectrum.class);
        when(spectrum.getPeptide()).thenReturn(peptide);
        when(spectrum.getCharge()).thenReturn(charge);

        return spectrum;
    }
}
