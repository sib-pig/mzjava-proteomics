/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.mol;

import org.expasy.mzjava.core.mol.AtomicSymbol;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.core.mol.PeriodicTable;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Oliver Horlacher
 * @author fnikitin
 * @version 1.0
 */
public class AAMassCalculatorTest {

    @Test
    public void testGetMz() throws Exception {

        AAMassCalculator massTable = AAMassCalculator.getInstance();

        double delta = 0.00000000001;
        double deltaY = massTable.getDeltaMass(IonType.y);

        //With electron weight
        Assert.assertEquals(19.0178407937, deltaY, delta);

        Assert.assertEquals(60.0125584507, AAMassCalculator.getInstance().calculateMz(100 + deltaY, 2, IonType.y), delta);
        Assert.assertEquals(51.0072761077, AAMassCalculator.getInstance().calculateMz(100 + massTable.getDeltaMass(IonType.b), 2, IonType.b), delta);
    }

    @Test
    public void testIonTypeDelta() throws Exception {


        double yDelta = PeriodicTable.H_MASS * 3 + PeriodicTable.O_MASS - PeriodicTable.ELECTRON_MASS;       //H3O
        double bDelta = PeriodicTable.H_MASS - PeriodicTable.ELECTRON_MASS;
        double aDelta = -PeriodicTable.C_MASS - PeriodicTable.O_MASS + PeriodicTable.H_MASS - PeriodicTable.ELECTRON_MASS;  //-CO
        double intactDelta = PeriodicTable.H_MASS * 2 + PeriodicTable.O_MASS;      //H2O
        double immoniumDelta = -PeriodicTable.C_MASS - PeriodicTable.O_MASS + PeriodicTable.H_MASS - PeriodicTable.ELECTRON_MASS; //- C - O

        AAMassCalculator massTable = AAMassCalculator.getInstance();

        Assert.assertEquals(yDelta, massTable.getDeltaMass(IonType.y), 0.0000001);
        Assert.assertEquals(bDelta, massTable.getDeltaMass(IonType.b), 0.0000001);
        Assert.assertEquals(aDelta, massTable.getDeltaMass(IonType.a), 0.0000001);
        Assert.assertEquals(intactDelta, massTable.getDeltaMass(IonType.p), 0.0000001);
        Assert.assertEquals(immoniumDelta, massTable.getDeltaMass(IonType.i), 0.0000001);
    }

    @Test
    public void testGlycineBIon() throws Exception {

        //Composition of singly charged glycine b ion
        Composition bGlycineComposition = new Composition(
                new Composition.Builder(AtomicSymbol.C, 2).add(AtomicSymbol.H, 4).add(AtomicSymbol.N).add(AtomicSymbol.O).charge(1).build());

        AAMassCalculator massTable = AAMassCalculator.getInstance();
        Assert.assertEquals(
                bGlycineComposition.getMolecularMass(),
                AAMassCalculator.getInstance().calculateMz(AminoAcid.G.getMassOfMonomer() + massTable.getDeltaMass(IonType.b), 1, IonType.b), 0.00000000001);
        Assert.assertEquals(
                bGlycineComposition.getMolecularMass(),
                AminoAcid.G.getMassOfMonomer() + massTable.getDeltaMass(IonType.b), 0.00000000001);
    }

    @Test
    public void testRoundTripPositive() throws Exception {

        double mw = 483.26871;
        int charge = 2;

        double mz = AAMassCalculator.getInstance().calculateMz(mw, charge, IonType.p);
        Assert.assertEquals(242.6416311077, mz, 0.00001);
        Assert.assertEquals(mw, AAMassCalculator.getInstance().calculateNeutralMolecularMass(mz, charge), 0.00001);
    }

    @Test
    public void testCalculateNeutralMolecularMass() throws Exception {

        final double molecularMass = AAMassCalculator.getInstance().calculateNeutralMolecularMass(new Peak(242.6416311077, 10, 2));
        Assert.assertEquals(483.26871, molecularMass, 0.00001);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCalculateNeutralMolecularMassIllegalCharge() throws Exception {

        AAMassCalculator.getInstance().calculateNeutralMolecularMass(456.2135, -12);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCalculateMzIllegalCharge() throws Exception {

        AAMassCalculator.getInstance().calculateMz(845.9, -12, IonType.b);
    }

    @Test
    public void testGetDeltaMass() throws Exception {

        AAMassCalculator calculator = AAMassCalculator.getInstance();

        Assert.assertEquals(-26.9876385143, calculator.getDeltaMass(IonType.a), 0.000000001);
        Assert.assertEquals(1.0072761076999999, calculator.getDeltaMass(IonType.b), 0.000000001);
        Assert.assertEquals(18.0338252087, calculator.getDeltaMass(IonType.c), 0.000000001);
        Assert.assertEquals(44.9971053517, calculator.getDeltaMass(IonType.x), 0.000000001);
        Assert.assertEquals(19.0178407937, calculator.getDeltaMass(IonType.y), 0.000000001);
        Assert.assertEquals(1.9912916926999993, calculator.getDeltaMass(IonType.z), 0.000000001);
        Assert.assertEquals(-26.9876385143, calculator.getDeltaMass(IonType.i), 0.000000001);
        Assert.assertEquals(18.010564686, calculator.getDeltaMass(IonType.p), 0.000000001);
    }

    @Test(expected = IllegalStateException.class)
    public void testGetDeltaMassUnknownIonType() throws Exception {

        AAMassCalculator.getInstance().getDeltaMass(IonType.aw_internal);
    }

    @Test
    public void testGetDeltaComposition() throws Exception {

        AAMassCalculator calculator = AAMassCalculator.getInstance();

        Assert.assertEquals(Composition.parseComposition("C-1O-1H(+)"), calculator.getDeltaComposition(IonType.a));
        Assert.assertEquals(Composition.parseComposition("H(+)"), calculator.getDeltaComposition(IonType.b));
        Assert.assertEquals(Composition.parseComposition("NH4(+)"), calculator.getDeltaComposition(IonType.c));
        Assert.assertEquals(Composition.parseComposition("CO2H(+)"), calculator.getDeltaComposition(IonType.x));
        Assert.assertEquals(Composition.parseComposition("H3O(+)"), calculator.getDeltaComposition(IonType.y));
        Assert.assertEquals(Composition.parseComposition("N-1O(+)"), calculator.getDeltaComposition(IonType.z));
        Assert.assertEquals(Composition.parseComposition("C-1O-1H(+)"), calculator.getDeltaComposition(IonType.i));
        Assert.assertEquals(Composition.parseComposition("H2O"), calculator.getDeltaComposition(IonType.p));
    }

    @Test(expected = IllegalStateException.class)
    public void testGetDeltaCompositionUnknownIonType() throws Exception {

        AAMassCalculator.getInstance().getDeltaComposition(IonType.aw_internal);
    }
}
