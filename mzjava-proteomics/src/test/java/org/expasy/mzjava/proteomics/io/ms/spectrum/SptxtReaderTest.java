/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.io.ms.spectrum;

import com.google.common.base.Optional;
import org.expasy.mzjava.core.io.ms.spectrum.AbstractReaderTest;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.core.mol.Mass;
import org.expasy.mzjava.core.mol.NumericMass;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;
import org.expasy.mzjava.proteomics.ms.spectrum.PepFragAnnotation;
import org.expasy.mzjava.proteomics.ms.spectrum.PepLibPeakAnnotation;
import org.expasy.mzjava.utils.URIBuilder;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class SptxtReaderTest extends AbstractReaderTest<PepLibPeakAnnotation, PeptideConsensusSpectrum> {

    private final Mass waterLoss = Composition.parseComposition("(H2O)-1");
    private final Mass ammoniaLoss = Composition.parseComposition("(NH3)-1");
    private final Mass phosphateLoss = Composition.parseComposition("(HPO3)-1");

    @Test
    public void test() throws Exception {

        SptxtReader reader = SptxtReader.newBuilder(new File(getClass().getResource("sptxt_test.sptxt").getFile()), PeakList.Precision.DOUBLE)
                .useSpectraStAnnotationResolver()
                .build();

        Peak precursor;
        double delta = 0.000001;

        //Spectrum 0
        PeptideConsensusSpectrum spectrum = reader.next();
        precursor = spectrum.getPrecursor();
        Assert.assertEquals(384.1813, precursor.getMz(), 0.01);
        Assert.assertEquals(0.0, precursor.getIntensity(), delta);
        Assert.assertEquals(3, precursor.getCharge());

        Assert.assertEquals("AvePrecursorMz=384.4051 BinaryFileOffset=378 ConsFracAssignedPeaks=0.708 DeltaCn=0.1070 DotConsensus=0.85,0.02;0/2 FracUnassigned=0.00,0/5;0.02,2/20;0.06,19/65 Inst=0 MaxRepSN=47.9 Mods=1/3,Y,Phospho NAA=8 NMC=0 NTT=2 Nreps=2/2 OrigMaxIntensity=4.1e+03 Parent=384.181 Pep=Tryptic Prob=0.9332 ProbRange=0.989579,0.9332,0.8886,0.844 Protein=8/IPI00182717/IPI00000352/IPI00014344/IPI00215873/IPI00219250/IPI00219251/IPI00219252/IPI00332215 RepFracAssignedPeaks=0.492 RepNumPeaks=216.5/56.5 RetentionTime=0.0,0.0,0.0 SN=200.0 Sample=1/TiO2,2,2 Se=1^S2:dc=0.1320/0.0250,fv=2.0476/0.1727,pb=0.8886/0.0446,xc=2.4030/0.0450 Spec=Consensus XCorr=2.4480",
                spectrum.getComment());
        Assert.assertEquals(2, spectrum.getMsLevel());
        Assert.assertNotNull(spectrum.getId());
	    checkPeaksSpectrum0(spectrum, delta);

        int count = 1;
        while (reader.hasNext()) {

            reader.next();
            count += 1;
        }
        Assert.assertEquals(7, count);
    }

    private PepLibPeakAnnotation fragAnt(int mergedPeakCount, IonType ionType, int charge, int residueNumber, int isotopeCount, Mass massShift, PeptideConsensusSpectrum spectrum) {

        Peptide peptideFragment;
        if (ionType == IonType.p) {

            peptideFragment = spectrum.getPeptide();
        } else {

            peptideFragment = spectrum.getPeptide().subSequence(ionType, residueNumber);
        }
        return new PepLibPeakAnnotation(mergedPeakCount, 0, 0, Optional.of(new PepFragAnnotation.Builder(ionType, charge, peptideFragment).addC13(isotopeCount).setNeutralLoss(massShift).build()));
    }

    private PepLibPeakAnnotation unknownAnt(int mergedPeakCount) {

        return new PepLibPeakAnnotation(mergedPeakCount, 0, 0);
    }

    /**
     * Tests that sptxt files with more than one peak with the same m/z can be read
     *
     * @throws Exception because it is a test
     */
    @Test
    public void testReadDeliberatorOutput() throws Exception {

        StringReader stringReader = new StringReader("Name: QTVLPGAVSLTFGGTK/2\n" +
                "LibID: 0\n" +
                "MW: 1660.88\n" +
                "PrecursorMZ: 830.44\n" +
                "FullName: QTVLPGA(Acetyl)V(Acetyl)SLTFGGTK/2\n" +
                "Comment: Mz_exact=830.4406798827 Mz_diff=0.0000000000 Parent=830.4406798827 PrecursorIntensity=1.0000000000 TotalIonCurrent=11214.7190228701 Mods=2/6,A,Acetyl/7,V,Acetyl\n" +
                "NumPeaks: 7\n" +
                "200.24\t184.8787170549\t?\n" +
                "228.03\t117.7986311401\t?\n" +
                "239.05\t55.916716442\t?\n" +
                "239.05\t128.4803916584\t?\n" +
                "245.03\t66.5542929067\t?\n" +
                "247.32\t76.9717943561\t?\n" +
                "248.11\t1062.9090318199\ty2/-0.05");

        SptxtReader reader = SptxtReader.newBuilder(stringReader, new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE)
                .useSpectraStAnnotationResolver()
                .build();

        Assert.assertEquals(true, reader.hasNext());

        PeptideConsensusSpectrum spectrum = reader.next();
        Assert.assertEquals(6, spectrum.size());
        Assert.assertArrayEquals(new int[]{5}, spectrum.getAnnotationIndexes());

        Assert.assertEquals(false, reader.hasNext());
    }

    @Test
    public void testModificationsRead() throws Exception {

        StringReader stringReader = new StringReader("Name: QTVLPGAVSLTFGGTK/2\n" +
                "LibID: 0\n" +
                "MW: 1660.88\n" +
                "PrecursorMZ: 830.44\n" +
                "FullName: H_QTVLPGA(Deamidation)V(Acetyl)SLTFGGTK_HO/2\n" +
                "Comment: Mz_exact=830.4406798827 Mz_diff=0.0000000000 Parent=830.4406798827 PrecursorIntensity=1.0000000000 TotalIonCurrent=11214.7190228701 Mods=2/6,A,Deamidation/7,V,Acetyl\n" +
                "NumPeaks: 7\n" +
                "200.24\t184.8787170549\t?\n" +
                "228.03\t117.7986311401\t?\n" +
                "239.05\t55.916716442\t?\n" +
                "239.05\t128.4803916584\t?\n" +
                "245.03\t66.5542929067\t?\n" +
                "247.32\t76.9717943561\t?\n" +
                "248.11\t1062.9090318199\ty2/-0.05");

        SptxtReader reader = SptxtReader.newBuilder(stringReader, new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE)
                .useSpectraStAnnotationResolver()
                .build();

        Assert.assertEquals(true, reader.hasNext());

        PeptideConsensusSpectrum spectrum = reader.next();
        Assert.assertEquals(6, spectrum.size());
        Assert.assertArrayEquals(new int[]{5}, spectrum.getAnnotationIndexes());

        Assert.assertEquals(false, reader.hasNext());
    }

	@Test
	public void testReadUnsortedPeaks() throws Exception {

		FileReader fr = new FileReader(new File(getClass().getResource("sptxt-unsorted_test.sptxt").getFile()));

		SptxtReader reader = SptxtReader.newBuilder(fr, new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE)
                .useSpectraStAnnotationResolver()
                .acceptUnsortedSpectra()
                .build();

		//Spectrum 0
		PeptideConsensusSpectrum spectrum = reader.next();

		checkPeaksSpectrum0(spectrum, 0.000001);
	}

	@Test(expected = IOException.class)
	public void testReadUnsortedPeaksNotAccepted() throws Exception {

		FileReader fr = new FileReader(new File(getClass().getResource("sptxt-unsorted_test.sptxt").getFile()));

		SptxtReader reader = SptxtReader.newBuilder(fr, new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE)
                .useSpectraStAnnotationResolver()
                .build();

		//Spectrum 0
		reader.next();
	}

	private void checkPeaksSpectrum0(PeptideConsensusSpectrum spectrum, double delta) {

		Assert.assertEquals(72, spectrum.size());
		checkPeak(130.1072, 30.0, 0, spectrum, delta, fragAnt(2, IonType.y, 3, 3, 0, new NumericMass(0.0), spectrum), fragAnt(2, IonType.b, 3, 3, 0, ammoniaLoss, spectrum));
		checkPeak(171.0304, 64.7, 1, spectrum, delta, unknownAnt(2));
		checkPeak(174.9999, 136.1, 2, spectrum, delta, fragAnt(2, IonType.y, 1, 1, 0, new NumericMass(0.0), spectrum));
		checkPeak(186.6766, 177.6, 3, spectrum, delta, fragAnt(2, IonType.y, 2, 3, 0, waterLoss, spectrum), fragAnt(2, IonType.y, 2, 3, 0, ammoniaLoss, spectrum));
		checkPeak(191.0824, 45.3, 4, spectrum, delta, fragAnt(2, IonType.b, 3, 4, 0, phosphateLoss, spectrum));
		checkPeak(195.4363, 225.6, 5, spectrum, delta, fragAnt(2, IonType.y, 2, 3, 0, new NumericMass(0.0), spectrum), fragAnt(2, IonType.b, 2, 3, 0, ammoniaLoss, spectrum));
        checkPeak(197.0844, 30.3, 6, spectrum, delta, unknownAnt(2));
        checkPeak(215.0593, 43.7, 7, spectrum, delta, unknownAnt(2));
		checkPeak(215.9981, 140.3, 8, spectrum, delta, fragAnt(2, IonType.b, 3, 4, 0, new NumericMass(0.0), spectrum), fragAnt(2, IonType.y, 3, 5, 0, new NumericMass(-98.0), spectrum), fragAnt(2, IonType.b, 3, 5, 0, new NumericMass(-116.0), spectrum));
		checkPeak(226.8692, 14.9, 9, spectrum, delta, fragAnt(2, IonType.b, 3, 5, 0, phosphateLoss, spectrum));
		checkPeak(229.0984, 48.5, 10, spectrum, delta, fragAnt(2, IonType.y, 2, 4, 0, new NumericMass(-45.0), spectrum));
        checkPeak(231.0454, 10.0, 11, spectrum, delta, unknownAnt(2));
        checkPeak(242.139, 87.8, 12, spectrum, delta, unknownAnt(2));
		checkPeak(243.1096, 67.1, 13, spectrum, delta, fragAnt(2, IonType.y, 2, 4, 0, waterLoss, spectrum), fragAnt(2, IonType.y, 3, 5, 0, waterLoss, spectrum));
		checkPeak(243.9398, 51.4, 14, spectrum, delta, fragAnt(2, IonType.y, 1, 2, 0, waterLoss, spectrum), fragAnt(2, IonType.y, 2, 4, 0, ammoniaLoss, spectrum), fragAnt(2, IonType.y, 3, 5, 0, ammoniaLoss, spectrum));
		checkPeak(245.106, 24.0, 15, spectrum, delta, fragAnt(2, IonType.y, 1, 2, 0, ammoniaLoss, spectrum), fragAnt(2, IonType.a, 3, 5, 0, new NumericMass(0.0), spectrum));
		checkPeak(262.076, 2395.6, 16, spectrum, delta, fragAnt(2, IonType.y, 1, 2, 0, new NumericMass(0.0), spectrum));
		checkPeak(263.125, 189.8, 17, spectrum, delta, fragAnt(2, IonType.y, 1, 2, 1, new NumericMass(0.0), spectrum));
		checkPeak(276.6047, 35.8, 18, spectrum, delta, fragAnt(2, IonType.b, 1, 2, 0, new NumericMass(0.0), spectrum), fragAnt(2, IonType.y, 3, 6, 0, new NumericMass(-45.0), spectrum), fragAnt(2, IonType.b, 2, 4, 0, new NumericMass(-98.0), spectrum));
        checkPeak(280.2172, 31.6, 19, spectrum, delta, unknownAnt(2));
        checkPeak(290.1435, 53.4, 20, spectrum, delta, unknownAnt(2));
        checkPeak(309.1732, 47.5, 21, spectrum, delta, unknownAnt(2));
		checkPeak(324.3744, 215.8, 22, spectrum, delta, fragAnt(2, IonType.b, 2, 4, 0, new NumericMass(0.0), spectrum), fragAnt(2, IonType.y, 2, 5, 0, new NumericMass(-98.0), spectrum));
		checkPeak(325.0726, 105.5, 23, spectrum, delta, fragAnt(2, IonType.b, 2, 4, 1, new NumericMass(0.0), spectrum));
        checkPeak(329.0662, 55.3, 24, spectrum, delta, unknownAnt(2));
		checkPeak(340.1576, 98.1, 25, spectrum, delta, fragAnt(2, IonType.p, 3, 8, 0, new NumericMass(-134.0), spectrum), fragAnt(2, IonType.y, 3, 7, 0, ammoniaLoss, spectrum), fragAnt(2, IonType.y, 3, 7, 0, waterLoss, spectrum));
		checkPeak(341.1334, 25.8, 26, spectrum, delta, fragAnt(2, IonType.b, 2, 5, 0, phosphateLoss, spectrum));
        checkPeak(342.8666, 916.5, 27, spectrum, delta, unknownAnt(2));
        checkPeak(355.0901, 96.0, 28, spectrum, delta, unknownAnt(2));
        checkPeak(356.0571, 115.0, 29, spectrum, delta, unknownAnt(2));
		checkPeak(360.1717, 110.6, 30, spectrum, delta, fragAnt(2, IonType.b, 1, 3, 0, new NumericMass(-45.0), spectrum));
		checkPeak(361.0927, 34.7, 31, spectrum, delta, fragAnt(2, IonType.b, 1, 3, 1, new NumericMass(-45.0), spectrum));
        checkPeak(362.9919, 43.1, 32, spectrum, delta, unknownAnt(2));
		checkPeak(364.9947, 213.8, 33, spectrum, delta, fragAnt(2, IonType.y, 2, 5, 0, waterLoss, spectrum), fragAnt(2, IonType.y, 2, 5, 0, ammoniaLoss, spectrum));
		checkPeak(367.0821, 10000.0, 34, spectrum, delta, fragAnt(2, IonType.a, 2, 5, 0, new NumericMass(0.0), spectrum));
        checkPeak(368.4888, 205.7, 35, spectrum, delta, unknownAnt(2));
		checkPeak(369.41, 73.9, 36, spectrum, delta, fragAnt(2, IonType.p, 3, 8, 0, new NumericMass(-45.0), spectrum));
		checkPeak(372.2138, 1906.3, 37, spectrum, delta, fragAnt(2, IonType.y, 1, 3, 0, waterLoss, spectrum), fragAnt(2, IonType.p, 3, 8, 0, new NumericMass(-36.0), spectrum), fragAnt(2, IonType.p, 3, 8, 0, new NumericMass(-35.0), spectrum), fragAnt(2, IonType.b, 2, 5, 0, ammoniaLoss, spectrum));
		checkPeak(373.1972, 1194.2, 38, spectrum, delta, fragAnt(2, IonType.y, 2, 5, 0, new NumericMass(0.0), spectrum), fragAnt(2, IonType.y, 1, 3, 0, ammoniaLoss, spectrum), fragAnt(2, IonType.p, 3, 8, 0, new NumericMass(-34.0), spectrum));
		checkPeak(374.2436, 242.0, 39, spectrum, delta, fragAnt(2, IonType.y, 1, 3, 1, ammoniaLoss, spectrum));
		checkPeak(375.4273, 472.5, 40, spectrum, delta, fragAnt(2, IonType.y, 1, 3, 1, ammoniaLoss, spectrum));
		checkPeak(378.103, 702.5, 41, spectrum, delta, fragAnt(2, IonType.p, 3, 8, 0, waterLoss, spectrum), fragAnt(2, IonType.p, 3, 8, 0, ammoniaLoss, spectrum), fragAnt(2, IonType.a, 1, 3, 0, new NumericMass(0.0), spectrum));
		checkPeak(378.9725, 64.3, 42, spectrum, delta, fragAnt(2, IonType.y, 2, 6, 0, new NumericMass(-116.0), spectrum));
		checkPeak(384.8119, 391.4, 43, spectrum, delta, fragAnt(2, IonType.p, 3, 8, 0, new NumericMass(0.0), spectrum));
		checkPeak(390.0674, 7462.9, 44, spectrum, delta, fragAnt(2, IonType.y, 1, 3, 0, new NumericMass(0.0), spectrum));
		checkPeak(391.0985, 814.1, 45, spectrum, delta, fragAnt(2, IonType.y, 1, 3, 1, new NumericMass(0.0), spectrum));
        checkPeak(402.8832, 26.0, 46, spectrum, delta, unknownAnt(2));
		checkPeak(404.9691, 301.2, 47, spectrum, delta, fragAnt(2, IonType.b, 1, 3, 0, new NumericMass(0.0), spectrum), fragAnt(2, IonType.b, 2, 6, 0, phosphateLoss, spectrum));
		checkPeak(428.8291, 257.8, 48, spectrum, delta, fragAnt(2, IonType.y, 2, 6, 0, waterLoss, spectrum), fragAnt(2, IonType.y, 2, 6, 0, ammoniaLoss, spectrum));
		checkPeak(437.5736, 191.5, 49, spectrum, delta, fragAnt(2, IonType.y, 2, 6, 0, new NumericMass(0.0), spectrum), fragAnt(2, IonType.b, 2, 6, 0, ammoniaLoss, spectrum));
        checkPeak(438.3828, 48.7, 50, spectrum, delta, unknownAnt(2));
		checkPeak(444.9526, 1408.1, 51, spectrum, delta, fragAnt(2, IonType.b, 2, 6, 0, new NumericMass(0.0), spectrum));
		checkPeak(445.6008, 383.6, 52, spectrum, delta, fragAnt(2, IonType.b, 2, 6, 1, new NumericMass(0.0), spectrum));
        checkPeak(447.4074, 50.9, 53, spectrum, delta, unknownAnt(2));
		checkPeak(484.9427, 100.0, 54, spectrum, delta, fragAnt(2, IonType.y, 1, 4, 0, waterLoss, spectrum));
		checkPeak(486.2818, 40.7, 55, spectrum, delta, fragAnt(2, IonType.y, 1, 4, 0, ammoniaLoss, spectrum));
		checkPeak(488.7415, 148.0, 56, spectrum, delta, fragAnt(2, IonType.b, 2, 7, 0, new NumericMass(0.0), spectrum));
		checkPeak(503.1456, 99.5, 57, spectrum, delta, fragAnt(2, IonType.y, 1, 4, 0, new NumericMass(0.0), spectrum));
        checkPeak(525.0845, 59.0, 58, spectrum, delta, unknownAnt(2));
        checkPeak(536.204, 82.7, 59, spectrum, delta, unknownAnt(2));
		checkPeak(647.9426, 3016.5, 60, spectrum, delta, fragAnt(2, IonType.b, 1, 4, 0, new NumericMass(0.0), spectrum), fragAnt(2, IonType.y, 1, 5, 0, new NumericMass(-98.0), spectrum));
		checkPeak(648.9694, 990.9, 61, spectrum, delta, fragAnt(2, IonType.b, 1, 4, 1, new NumericMass(0.0), spectrum));
		checkPeak(703.3777, 2213.1, 62, spectrum, delta, fragAnt(5, IonType.z, 1, 6, 0, new NumericMass(0.0), spectrum), fragAnt(5, IonType.y, 2, 7, 0, new NumericMass(0.0), spectrum));
		checkPeak(704.3649, 590.2, 63, spectrum, delta, fragAnt(4, IonType.z, 1, 6, 1, new NumericMass(0.0), spectrum));
		checkPeak(705.4604, 55.3, 64, spectrum, delta, fragAnt(3, IonType.z, 1, 6, 1, new NumericMass(0.0), spectrum));
		checkPeak(709.4297, 688.9, 65, spectrum, delta, fragAnt(5, IonType.c, 2, 7, 0, new NumericMass(0.0), spectrum));
		checkPeak(710.1908, 126.9, 66, spectrum, delta, fragAnt(3, IonType.c, 2, 7, 1, new NumericMass(0.0), spectrum));
		checkPeak(743.7762, 25.7, 67, spectrum, delta, fragAnt(2, IonType.b, 1, 5, 0, ammoniaLoss, spectrum));
		checkPeak(761.0124, 546.3, 68, spectrum, delta, fragAnt(2, IonType.b, 1, 5, 0, new NumericMass(0.0), spectrum));
		checkPeak(761.9772, 62.8, 69, spectrum, delta, fragAnt(2, IonType.b, 1, 5, 1, new NumericMass(0.0), spectrum));
		checkPeak(762.1734, 10.2, 70, spectrum, delta, fragAnt(2, IonType.b, 1, 6, 0, new NumericMass(-13.0), spectrum));
		checkPeak(762.937, 42.3, 71, spectrum, delta, unknownAnt(2));
	}

    @Test
    public void testPhosphoPepSpTxt() throws Exception  {

        StringReader stringReader = new StringReader(
                "Name: C[160]AM[147]Y[243]E[143]LVKVGHNLVGE[143]VIRc[31]/3\n" +
                "LibID: 14227\n" +
                "MW: 2443.28\n" +
                "PrecursorMZ: 771.0494626293671\n" +
                "Status: Impure\n" +
                "FullName: G.C[160]AM[147]Y[243]E[143]LVKVGHNLVGE[143]VIRc[31].I/3\n" +
                "Comment: BinaryFileOffset=69834322 DeltaCn=0.7310 FracUnassigned=0.71,3/5;0.66,12/20;0.32,24/79 Mods=6/0,C,Carbamidomethyl/2,M,Oxidation/3,Y,Phospho/4,E,Methyl/15,E,Methyl/-2,R,Methyl NMC=1 NTT=1 Nreps=1/1 OrigMaxIntensity=11 Parent=814.426 Pep=Semi-tryp Prob=0.9873 Protein=2/UPTR:Q86Z53_YEAST/UPTR:Q874H1_YEAST RawSpectrum=B06-7355_c.7367.7367.3 RetentionTime=0,0,0 Sample=1/PAC,1,1 Se=1^S1:pb=0.9873/0,fv=5.4163/0,xc=0.7080/0,dc=0.7310/0 Spec=Single XCorr=0.7080\n" +
                "NumPeaks: 12\n" +
                "266.9167\t1010.8\ty7-17^3/0.75,b4-91^2/0.85,b6-82^3/0.82,b6-80^3/0.14\n" +
                "301.8333\t1187.0\ty2/-0.39,y5^2/0.65\n" +
                "399.2500\t1408.6\ty7-17^2/0.50,y7-18^2/0.99,b6-80^2/-0.41,b6-82^2/0.61,b10-64^3/-0.60\n" +
                "404.5502\t2847.3\t?\n" +
                "416.1667\t1004.0\tb6-46^2/-0.48,b10-18^3/0.99,b10-17^3/0.66\n" +
                "454.0833\t1104.2\t?\n" +
                "503.0833\t1178.5\t?\n" +
                "515.1667\t949.5\ty14-17^3/-0.63,y14-18^3/-0.30,b13-82^3/-0.56\n" +
                "529.0520\t3019.2\tb13-45^3/1.00,b13-44^3/0.65\n" +
                "538.0833\t2321.1\tb13-17^3/0.70\n" +
                "547.0833\t929.6\t?\n" +
                "580.2500\t1435.2\tb9-46^2/0.49\n");

        SptxtReader reader = SptxtReader.newBuilder(stringReader, new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE)
                .useSpectraStAnnotationResolver()
                .build();

        Assert.assertEquals(true, reader.hasNext());

        PeptideConsensusSpectrum spectrum = reader.next();

        Assert.assertEquals("CAMYELVKVGHNLVGEVIR", spectrum.getPeptide().toSymbolString());
        Assert.assertEquals("C(Carbamidomethyl)AM(Oxidation)Y(Phospho)E(Methyl)LVKVGHNLVGE(Methyl)VIR_(Methyl)", spectrum.getPeptide().toString());
        Assert.assertEquals(771.0494626293671, spectrum.getPrecursor().getMz(),0.001);
        Assert.assertEquals(3, spectrum.getPrecursor().getCharge());
        Assert.assertEquals(12, spectrum.size());
    }

    @Test
    public void testInvalidPhosphoPepSpTxt() throws Exception  {

        // Phospho pep contains aa named B and c[31] as c-term modif

        StringReader stringReader = new StringReader(
                "Name: C[160]AM[147]Y[243]E[143]LVKVGHBNLVGE[143]VIRc[31]/3\n" +
                        "LibID: 14227\n" +
                        "MW: 2443.28\n" +
                        "PrecursorMZ: 814.43\n" +
                        "Status: Impure\n" +
                        "FullName: G.C[160]AM[147]Y[243]E[143]LVKVGHBNLVGE[143]VIRc[31].I/3\n" +
                        "Comment: BinaryFileOffset=69834322 DeltaCn=0.7310 FracUnassigned=0.71,3/5;0.66,12/20;0.32,24/79 Mods=6/0,C,Carbamidomethyl/2,M,Oxidation/3,Y,Phospho/4,E,Methyl/16,E,Methyl/-2,R,Methyl NMC=1 NTT=1 Nreps=1/1 OrigMaxIntensity=11 Parent=814.426 Pep=Semi-tryp Prob=0.9873 Protein=2/UPTR:Q86Z53_YEAST/UPTR:Q874H1_YEAST RawSpectrum=B06-7355_c.7367.7367.3 RetentionTime=0,0,0 Sample=1/PAC,1,1 Se=1^S1:pb=0.9873/0,fv=5.4163/0,xc=0.7080/0,dc=0.7310/0 Spec=Single XCorr=0.7080\n" +
                        "NumPeaks: 12\n" +
                        "266.9167\t1010.8\ty7-17^3/0.75,b4-91^2/0.85,b6-82^3/0.82,b6-80^3/0.14\n" +
                        "301.8333\t1187.0\ty2/-0.39,y5^2/0.65\n" +
                        "399.2500\t1408.6\ty7-17^2/0.50,y7-18^2/0.99,b6-80^2/-0.41,b6-82^2/0.61,b10-64^3/-0.60\n" +
                        "404.5502\t2847.3\t?\n" +
                        "416.1667\t1004.0\tb6-46^2/-0.48,b10-18^3/0.99,b10-17^3/0.66\n" +
                        "454.0833\t1104.2\t?\n" +
                        "503.0833\t1178.5\t?\n" +
                        "515.1667\t949.5\ty14-17^3/-0.63,y14-18^3/-0.30,b13-82^3/-0.56\n" +
                        "529.0520\t3019.2\tb13-45^3/1.00,b13-44^3/0.65\n" +
                        "538.0833\t2321.1\tb13-17^3/0.70\n" +
                        "547.0833\t929.6\t?\n" +
                        "580.2500\t1435.2\tb9-46^2/0.49\n");

        SptxtReader reader = SptxtReader.newBuilder(stringReader, new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE)
                .useSpectraStAnnotationResolver()
                .build();

        Assert.assertEquals(true, reader.hasNext());

        String errorStr = "";
        try {
            reader.next();
        } catch(IOException e) {
            errorStr = e.toString();
        }

        Assert.assertTrue(errorStr.isEmpty());
    }

    @Test
    public void testReadMzJava() throws Exception {

        StringReader stringReader = new StringReader(
                "Name: QQVHVELDVAKPDLTAALK/2\n" +
                        "LibID: 37\n" +
                        "MW: 2059.130\n" +
                        "PrecursorMZ: 1029.565\n" +
                        "FullName: Q(H-3N-1)QVHVELDVAKPDLTAALK/2\n" +
                        "Comment: Mz_exact=1029.565 Mz_diff=-0.000 Parent=1029.565 PrecursorIntensity=18086142.391 TotalIonCurrent=8384622.857 Protein=unknown Mods=1/0,Q,H-3N-1\n" +
                        "NumPeaks: 545\n" +
                        "110.071\t2162.702\t?\n" +
                        "110.772\t13.040\t?\n" +
                        "111.071\t147.754\t?\n" +
                        "112.250\t11.768\tb1/-0.21\n" +
                        "114.331\t10.139\t?\n" +
                        "114.672\t20.676\t?\n" +
                        "115.087\t1.916\t?\n" +
                        "117.669\t2.548\t?\n" +
                        "119.038\t11.434\t?\n" +
                        "120.531\t0.240\tb2^2/0.02\n" +
                        "123.337\t21.392\t?\n" +
                        "125.261\t11.627\t?\n" +
                        "125.492\t9.430\t?\n" +
                        "129.102\t684.884\t?\n" +
                        "130.091\t241.950\t?\n" +
                        "130.427\t11.885\ty2^2/0.17\n" +
                        "135.155\t1.813\t?\n" +
                        "136.066\t10.428\t?\n" +
                        "137.889\t1.097\t?\n" +
                        "139.147\t19.586\t?\n" +
                        "139.491\t1.837\t?\n" +
                        "139.885\t12.411\t?\n" +
                        "141.803\t34.402\t?\n" +
                        "143.100\t2.487\t?\n" +
                        "145.097\t16.833\t?\n" +
                        "146.838\t18.821\ty1/0.27\n" +
                        "147.113\t663.818\t?\n" +
                        "148.078\t3.555\t?\n" +
                        "151.335\t10.715\t?\n" +
                        "151.937\t1.183\t?\n" +
                        "152.776\t23.524\t?\n" +
                        "155.117\t111.389\t?\n" +
                        "155.827\t1.188\t?\n" +
                        "156.819\t1.680\t?\n" +
                        "157.133\t70.264\t?\n" +
                        "166.061\t49.939\ty3^2/0.06\n");

        SptxtReader reader = SptxtReader.newBuilder(stringReader, new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE)
                .useSpectraStAnnotationResolver()
                .build();

        Assert.assertEquals(true, reader.hasNext());

        PeptideConsensusSpectrum spectrum = reader.next();
        Assert.assertEquals(Peptide.parse("Q(H-3N-1)QVHVELDVAKPDLTAALK"), spectrum.getPeptide());
    }
}
