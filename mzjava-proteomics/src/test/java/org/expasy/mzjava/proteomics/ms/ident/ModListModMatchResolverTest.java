/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.ms.ident;

import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.proteomics.mol.modification.unimod.UnimodManager;
import org.expasy.mzjava.proteomics.mol.modification.unimod.UnimodMod;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class ModListModMatchResolverTest {

    @Test
    public void testResolve() throws Exception {

        Optional<? extends Modification> oxidationOpt = UnimodManager.getModification("Oxidation");
        Optional<? extends Modification> phosphoOpt = UnimodManager.getModification("Phospho");
        Optional<? extends Modification> deamidatedOpt = UnimodManager.getModification("Deamidated");

        ModificationMatchResolver resolver = new ModListModMatchResolver(new AbsoluteTolerance(0.5),
                phosphoOpt.get(),
                oxidationOpt.get(),
                deamidatedOpt.get()
        );

        Assert.assertEquals(Optional.<Modification>absent(), resolver.resolve(newModMatch(oxidationOpt.get().getMolecularMass() - 0.51)));
        Assert.assertEquals(oxidationOpt, resolver.resolve(newModMatch(oxidationOpt.get().getMolecularMass() - 0.5)));
        Assert.assertEquals(oxidationOpt, resolver.resolve(newModMatch(16)));
        Assert.assertEquals(oxidationOpt, resolver.resolve(newModMatch(oxidationOpt.get().getMolecularMass() + 0.5)));
        Assert.assertEquals(Optional.<Modification>absent(), resolver.resolve(newModMatch(oxidationOpt.get().getMolecularMass() + 0.51)));

        Assert.assertEquals(Optional.<Modification>absent(), resolver.resolve(newModMatch(phosphoOpt.get().getMolecularMass() - 0.51)));
        Assert.assertEquals(phosphoOpt, resolver.resolve(newModMatch(phosphoOpt.get().getMolecularMass() - 0.5)));
        Assert.assertEquals(phosphoOpt, resolver.resolve(newModMatch(80)));
        Assert.assertEquals(phosphoOpt, resolver.resolve(newModMatch(phosphoOpt.get().getMolecularMass() + 0.5)));
        Assert.assertEquals(Optional.<Modification>absent(), resolver.resolve(newModMatch(phosphoOpt.get().getMolecularMass() + 0.51)));
    }

    @Test
    public void testResolveCollectionConstructor() throws Exception {

        Optional<? extends Modification> oxidationOpt = UnimodManager.getModification("Oxidation");
        Optional<? extends Modification> phosphoOpt = UnimodManager.getModification("Phospho");
        Optional<? extends Modification> deamidatedOpt = UnimodManager.getModification("Deamidated");

        ModificationMatchResolver resolver = new ModListModMatchResolver(new AbsoluteTolerance(0.5),
                Lists.newArrayList(phosphoOpt.get(), oxidationOpt.get(), deamidatedOpt.get())
        );

        Assert.assertEquals(Optional.<Modification>absent(), resolver.resolve(newModMatch(oxidationOpt.get().getMolecularMass() - 0.51)));
        Assert.assertEquals(oxidationOpt, resolver.resolve(newModMatch(oxidationOpt.get().getMolecularMass() - 0.5)));
        Assert.assertEquals(oxidationOpt, resolver.resolve(newModMatch(16)));
        Assert.assertEquals(oxidationOpt, resolver.resolve(newModMatch(oxidationOpt.get().getMolecularMass() + 0.5)));
        Assert.assertEquals(Optional.<Modification>absent(), resolver.resolve(newModMatch(oxidationOpt.get().getMolecularMass() + 0.51)));

        Assert.assertEquals(Optional.<Modification>absent(), resolver.resolve(newModMatch(phosphoOpt.get().getMolecularMass() - 0.51)));
        Assert.assertEquals(phosphoOpt, resolver.resolve(newModMatch(phosphoOpt.get().getMolecularMass() - 0.5)));
        Assert.assertEquals(phosphoOpt, resolver.resolve(newModMatch(80)));
        Assert.assertEquals(phosphoOpt, resolver.resolve(newModMatch(phosphoOpt.get().getMolecularMass() + 0.5)));
        Assert.assertEquals(Optional.<Modification>absent(), resolver.resolve(newModMatch(phosphoOpt.get().getMolecularMass() + 0.51)));
    }

    @Test(expected = IllegalStateException.class)
    public void testConflictingMods() throws Exception {

        Optional<UnimodMod> phosphoOpt = UnimodManager.getModification("Phospho");
        Optional<UnimodMod> sulfoOpt = UnimodManager.getModification("Sulfo");
        Optional<UnimodMod> glyGHisOpt = UnimodManager.getModification("Gly->His");

        new ModListModMatchResolver(new AbsoluteTolerance(0.05), phosphoOpt.get(), sulfoOpt.get(), glyGHisOpt.get());
    }

    @Test
    public void testNotConflicting() throws Exception {

        Optional<? extends Modification> sulfoOpt = UnimodManager.getModification("Sulfo");
        Optional<? extends Modification> glyGHisOpt = UnimodManager.getModification("Gly->His");

        ModificationMatchResolver resolver = new ModListModMatchResolver(new AbsoluteTolerance(0.04), glyGHisOpt.get(), sulfoOpt.get());

        Assert.assertEquals(sulfoOpt, resolver.resolve(newModMatch(sulfoOpt.get().getMolecularMass() + 0.04)));
        Assert.assertEquals(glyGHisOpt, resolver.resolve(newModMatch(sulfoOpt.get().getMolecularMass() + 0.05)));
    }

    private ModificationMatch newModMatch(double mass) {

        return new ModificationMatch(mass, AminoAcid.S, 12, ModAttachment.SIDE_CHAIN);
    }
}
