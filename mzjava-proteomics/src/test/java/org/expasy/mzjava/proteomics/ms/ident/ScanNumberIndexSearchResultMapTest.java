/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.ms.ident;

import com.google.common.base.Optional;
import org.expasy.mzjava.utils.URIBuilder;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.core.ms.spectrum.ScanNumberDiscrete;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.File;
import java.util.List;

import static org.mockito.Mockito.when;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class ScanNumberIndexSearchResultMapTest {

    @Test
    public void test() throws Exception {

        ScanNumberIndexSearchResultMap searchResultMap = new ScanNumberIndexSearchResultMap();

        SpectrumIdentifier identifier = new SpectrumIdentifier("92_P1_A10_135454533601.1.1.1");
        identifier.setIndex(1);
        identifier.setPrecursorNeutralMass(855.51127496);
        identifier.setAssumedCharge(1);
        identifier.addScanNumber(new ScanNumberDiscrete(11));

        PeptideMatch peptideMatch = new PeptideMatch("IDRSIRP");
        peptideMatch.setMassDiff(Optional.fromNullable(0.0186289359999));

        searchResultMap.put(identifier, peptideMatch);

        MsnSpectrum spectrum = Mockito.mock(MsnSpectrum.class);
        when(spectrum.getSpectrumSource()).thenReturn(new File("/Users/Oliver Horlacher/Documents/masspec/Data/Miguel/Swissbyspot_mgf400_rename/Swissbyspot_mgf400/92/92_P1_A10_135454533601.mgf").toURI());
        when(spectrum.getSpectrumIndex()).thenReturn(10);

        List<PeptideMatch> results = searchResultMap.getResults(spectrum);

        Assert.assertEquals(1, results.size());

        Assert.assertSame(peptideMatch, results.get(0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMalformedSource() throws Exception {

        ScanNumberIndexSearchResultMap searchResultMap = new ScanNumberIndexSearchResultMap();

        MsnSpectrum spectrum = Mockito.mock(MsnSpectrum.class);
        when(spectrum.getSpectrumSource()).thenReturn(new URIBuilder("www.expasy.ch", "test").build());
        List<PeptideMatch> results = searchResultMap.getResults(spectrum);

        Assert.assertEquals(0, results.size());
    }
}
