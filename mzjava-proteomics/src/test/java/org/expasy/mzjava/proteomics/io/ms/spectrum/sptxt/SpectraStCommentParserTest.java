/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.io.ms.spectrum.sptxt;

import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.junit.Test;

import static org.mockito.Mockito.*;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class SpectraStCommentParserTest {

    @Test
    public void testParseModsComment1() throws Exception {

        LibrarySpectrumBuilder builder = mock(LibrarySpectrumBuilder.class);

        SpectraStCommentParser parser = new SpectraStCommentParser();

        parser.parseModsComment("6/0,C,Carbamidomethyl/2,M,Oxidation/3,Y,Phospho/4,E,Methyl/16,E,Methyl/-2,R,Methyl", builder);

        verify(builder).addMod(0, "Carbamidomethyl");
        verify(builder).addMod(2, "Oxidation");
        verify(builder).addMod(3, "Phospho");
        verify(builder).addMod(4, "Methyl");
        verify(builder).addMod(16, "Methyl");
        verify(builder).addMod(ModAttachment.C_TERM, "Methyl");
        verifyNoMoreInteractions(builder);
    }

    @Test
    public void testParseModsComment2() throws Exception {

        LibrarySpectrumBuilder builder = mock(LibrarySpectrumBuilder.class);

        SpectraStCommentParser parser = new SpectraStCommentParser();

        parser.parseModsComment("0", builder);

        verifyNoMoreInteractions(builder);
    }

    @Test
    public void testParseModsComment3() throws Exception {

        LibrarySpectrumBuilder builder = mock(LibrarySpectrumBuilder.class);

        SpectraStCommentParser parser = new SpectraStCommentParser();

        parser.parseModsComment("2/-1,C,n-term/-2,M,c-term", builder);

        verify(builder).addMod(ModAttachment.N_TERM, "n-term");
        verify(builder).addMod(ModAttachment.C_TERM, "c-term");
        verifyNoMoreInteractions(builder);
    }

    @Test
    public void testParseProteinComment() throws Exception {

        LibrarySpectrumBuilder builder = mock(LibrarySpectrumBuilder.class);

        SpectraStCommentParser parser = new SpectraStCommentParser();

        parser.parseProteinComment("\"Q3UBS3\"",
                builder);

        verify(builder).addProteinAccessionNumber("Q3UBS3");
    }

    @Test
    public void testParseComment() throws Exception {

        LibrarySpectrumBuilder builder = mock(LibrarySpectrumBuilder.class);

        SpectraStCommentParser parser = new SpectraStCommentParser();

        String comment = "BinaryFileOffset=636655938 DUScorr=2.3/0.44/0.94 Dot_cons=0.937/0.043 Dotbest=0.92 Dotfull=0.882/0.038 Dottheory=0.87 Flags=0,0,0 Fullname=K.QWVNTVAGEK.L/2 Inst=it Max2med_orig=243.4/90.9 Missing=0.0687/0.0655 Mods=1/0,Q,Acetyl Mz_av=587.653 Mz_diff=0.582 Mz_exact=587.2991 Naa=10 Nreps=21/27 Organism=mouse Parent=587.299 Parent_med=587.8010/0.08 Pep=Tryptic Pfin=1.1e+005 Pfract=0 Prob=0.9978 Probcorr=1 Protein=\"Q3UBS3\" Pseq=90 Sample=4/bi_m2_control_cam,15,16/bi_m2_kras_p53_cam,4,5/hms_colon_apcd580_8585847_7_krastins_ltqft_prop,1,1/hms_colon_apcmin_122804_nanospray_3cutscx_cam,1,1 Se=1^O21:ex=0.0408/0.1907,td=260/1206,pr=2.91e-006/1.514e-005,bs=0.00899,b2=0.00993,bd=5560 Spec=Consensus Tfratio=4.6e+002 Unassign_all=0.148 Unassigned=0.128";
        parser.parseComment(comment, builder);

        verify(builder).addMod(0, "Acetyl");
        verify(builder).addProteinAccessionNumber("Q3UBS3");
        verifyNoMoreInteractions(builder);
    }

    @Test
    public void testParseComment2() throws Exception {

        LibrarySpectrumBuilder builder = mock(LibrarySpectrumBuilder.class);

        SpectraStCommentParser parser = new SpectraStCommentParser();

        String comment = "Spec=Consensus Pep=Tryptic Fullname=R.AAAAAAAAAAAAAAAGAGAGAK.Q/2 Mods=0 Parent=798.927 Inst=it Mz_diff=0.482 Mz_exact=798.9268 Mz_av=799.390 Protein=\"sp|P55011|S12A2_HUMAN Solute carrier family 12 member 2 [Homo sapiens]\" Pseq=36/1 Organism=\"human\" Se=4^X27:ex=1e-008/8.405e-005,td=8.57e+008/7.496e+012,sd=0/0,hs=68.1/9.537,bs=3e-013,b2=1.6e-012,bd=1.67e+014^O26:ex=1.201e-009/0.0001124,td=2.53e+008/4.237e+013,pr=5.815e-015/1.886e-010,bs=3.12e-017,b2=4.51e-015,bd=1.08e+015^P10:sc=35.2/3.81,dc=23.2/2.91,ps=3.4/0.704,bs=26.8,b2=27.6,bd=16^C1:ex=0.0098/0,td=5100/0,sd=0/0,hs=455/0,bs=0.0098,bd=5100 Sample=37/fcrf_anon_00040_none,3,3/fcrf_anon_00041_none,4,4/hlpp_e035_cam,0,1/micro2_01_02_cam,0,1/micro2_04_19_cam,0,1/micro_07_34_cam,0,1/ncta_gastric_mucosa_cam,1,2/pnnl_wqian_hsplasma_none,0,1/resing_hserythroleukq_r_cam,0,1/sri_jurkat_staurosporine_treated_cam,1,2/uchc_jurkat_depletion_none,0,1/ucsd_06_h293cocl2_total_try_2ul_std_a_200ug_2d34_ltq2_cam,0,2/ucsd_h293_total_try_a_200ug_2d34_081905_ltq1_cam,0,2/ucsd_h293b_total_try_2nd_digest_e_200ug_2d34_new_122305_ltq2_cam,0,1/ucsd_h293b_total_try_2nd_digest_g_200ug_2d34_123005_ltq1_cam,1,1/ucsd_h293b_total_try_b_200ug_2d34_091305_ltq1_cam,0,1/ucsd_h293cocl2_b2_alice_total_try_200ug_a_2d34_120805_ltq2_cam,1,1/ucsd_h293cocl2_total_try_a_200ug_2d34_100605_ltq1_cam,1,3/ucsd_h293cocl2_total_try_b_200ug_2d34_100605_ltq2_cam,1,1/ucsd_hek293_1stbatch_total_trypsin_c_200ug_2umbeads_f17tip_2d34_111105_ltq2_cam,1,1/ut_hek_mudpitvsofgel_cam,0,2/ut_lung_cancer_xenograft_cam,1,4/ut_placenta_hs_villous_cam,0,2/ut_prostate_cancer_t3_cam,1,2/ut_prostate_cancer_t3m_cam,0,3/ut_prostate_cancer_t7_cam,1,4/uta_hek293t_cam,1,2/vu_anon_00169_cam,0,1/vu_anon_00152_cam,3,3/vu_gastric_parietal_cam,1,3/vu_anon_00005_cam,0,2/vu_anon_00035_cam,1,1/vu_anon_00194_cam,0,1/vu_anon_00177_cam,0,2/vu_anon_00021_cam,0,1/vu_anon_00024_cam,2,2/vumca_colorectal_cancer_cam,3,4 Nreps=28/70 Missing=0.3852/0.0962 Parent_med=798.9340/0.49 Max2med_orig=100.0/0.0 Dotfull=0.600/0.052 Dot_cons=0.700/0.081 Dotbest=0.79 Flags=0,0,17 Unassign_all=0.053 Unassigned=0 Naa=22 DUScorr=0.79/3.8/2.9 Dottheory=0.79 Pfin=3.5e+011 Probcorr=20 Tfratio=1.9e+004 Pfract=0.16";
        parser.parseComment(comment, builder);

        verify(builder).addProteinAccessionNumber("sp|P55011|S12A2_HUMAN Solute carrier family 12 member 2 [Homo sapiens]");
        verifyNoMoreInteractions(builder);
    }

    @Test
    public void testParseMzJavaComment() throws Exception {

        LibrarySpectrumBuilder builder = mock(LibrarySpectrumBuilder.class);

        SpectraStCommentParser parser = new SpectraStCommentParser();

        String comment = "Mz_exact=1029.565 Mz_diff=-0.000 Parent=1029.565 PrecursorIntensity=18086142.391 TotalIonCurrent=8384622.857 Protein=unknown Mods=1/0,Q,H-3N-1";
        parser.parseComment(comment, builder);

        verify(builder).addProteinAccessionNumber("unknown");
        verify(builder).addMod(0, "H-3N-1");
        verifyNoMoreInteractions(builder);
    }

    @Test(expected = IllegalStateException.class)
    public void testUnclosed() throws Exception {

        LibrarySpectrumBuilder builder = mock(LibrarySpectrumBuilder.class);

        SpectraStCommentParser parser = new SpectraStCommentParser();

        String comment = "Mz_exact=1029.565 Mz_diff=\"-0.000 Parent=1029.565 PrecursorIntensity=18086142.391 TotalIonCurrent=8384622.857 Protein=unknown Mods=1/0,Q,H-3N-1";
        parser.parseComment(comment, builder);
    }
}
