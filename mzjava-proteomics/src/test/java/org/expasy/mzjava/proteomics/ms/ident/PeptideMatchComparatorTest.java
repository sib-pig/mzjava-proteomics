package org.expasy.mzjava.proteomics.ms.ident;

import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.when;

/**
 * @author fnikitin
 * Date: 2/28/14
 */
public class PeptideMatchComparatorTest {

    @Test (expected = NullPointerException.class)
    public void testNullName() {

        new PeptideMatchComparator(null);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testEmptyName() {

        new PeptideMatchComparator("");
    }

    @Test
    public void test() {

        String scoreName = "score";

        PeptideMatchComparator comp = new PeptideMatchComparator(scoreName);

        PeptideMatch pm1 = Mockito.mock(PeptideMatch.class);
        when(pm1.getScore(scoreName)).thenReturn(10.);

        PeptideMatch pm2 = Mockito.mock(PeptideMatch.class);
        when(pm2.getScore(scoreName)).thenReturn(100.);

        int ret = comp.compare(pm1, pm2);
        Assert.assertEquals(1, ret);

        ret = comp.compare(pm2, pm1);
        Assert.assertEquals(-1, ret);
    }

    @Test
    public void testEquals() {

        String scoreName = "score";

        PeptideMatchComparator comp = new PeptideMatchComparator(scoreName);

        PeptideMatch pm1 = Mockito.mock(PeptideMatch.class);
        when(pm1.getScore(scoreName)).thenReturn(10.);

        PeptideMatch pm2 = Mockito.mock(PeptideMatch.class);
        when(pm2.getScore(scoreName)).thenReturn(10.);

        int ret = comp.compare(pm1, pm2);
        Assert.assertEquals(0, ret);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testMissingScoreName() {

        PeptideMatchComparator comp = new PeptideMatchComparator("score");

        PeptideMatch pm1 = new PeptideMatch(AminoAcid.A);
        PeptideMatch pm2 = new PeptideMatch(AminoAcid.B);

        comp.compare(pm1, pm2);
    }
}
