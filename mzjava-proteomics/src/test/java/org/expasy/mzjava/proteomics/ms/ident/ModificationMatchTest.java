/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.ms.ident;

import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class ModificationMatchTest {

    @Test
    public void testSet() throws Exception {

        Set<ModificationMatch> set = new HashSet<ModificationMatch>();
        ModificationMatch modMatch1 = new ModificationMatch(0.1, AminoAcid.S, 12, ModAttachment.SIDE_CHAIN);
        ModificationMatch modMatch2 = new ModificationMatch(0.1 + 0.0009, AminoAcid.S, 12, ModAttachment.SIDE_CHAIN);

        Assert.assertEquals(true, set.add(modMatch1));
        Assert.assertEquals(1, set.size());
        Assert.assertEquals(true, set.add(modMatch2));
        Assert.assertEquals(2, set.size());
    }

    /**
     * As stated by the hasCode contract objects that are equals have to have the same hash code
     */
    @Test
    public void testHashCode() throws Exception {

        ModificationMatch modMatch1 = new ModificationMatch(0.1, AminoAcid.S, 12, ModAttachment.SIDE_CHAIN);
        ModificationMatch modMatch2 = new ModificationMatch(0.1, AminoAcid.S, 12, ModAttachment.SIDE_CHAIN);

        Assert.assertEquals(true, modMatch1.equals(modMatch2));
        Assert.assertEquals(modMatch1.hashCode(), modMatch2.hashCode());
    }

    @Test
    public void testIsWithinTolerance() throws Exception {

        ModificationMatch match = new ModificationMatch(79.9+0.5, AminoAcid.S, 12, ModAttachment.SIDE_CHAIN);
        match.addPotentialModification(Modification.parseModification("HPO3"));
        match.addPotentialModification(Modification.parseModification("O3S"));
        Assert.assertEquals(true, match.isWithinTolerance(new AbsoluteTolerance(0.6)));
        Assert.assertEquals(false, match.isWithinTolerance(new AbsoluteTolerance(0.3)));
    }
}
