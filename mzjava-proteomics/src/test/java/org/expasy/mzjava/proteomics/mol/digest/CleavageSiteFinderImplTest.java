package org.expasy.mzjava.proteomics.mol.digest;

import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import gnu.trove.list.TIntList;
import gnu.trove.list.array.TIntArrayList;
import org.expasy.mzjava.core.mol.SymbolSequence;
import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.proteomics.mol.Protein;
import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;
import java.util.Arrays;
import java.util.List;


public class CleavageSiteFinderImplTest {

    @Test
    public void testCountCleavageSites() throws ParseException {

        CleavageSiteFinder finder = new CleavageSiteFinderImpl(new CleavageSiteMatcher("[KR]|[^P]"));

        Assert.assertEquals(3, finder.countCleavageSites(new Protein("ACC1", "RESALYTNIKALASKR")));
    }

    @Test
    public void testCountCleavageSites2() throws ParseException {

        CleavageSiteFinder finder = new CleavageSiteFinderImpl(new CleavageSiteMatcher("[KR]|[^P]"));

        Assert.assertEquals(0, finder.countCleavageSites(new Protein("ACC1", "ESALYTNI")));
    }

    @Test
    public void testFindCleavageSites() throws ParseException {

        CleavageSiteFinder finder = new CleavageSiteFinderImpl(new CleavageSiteMatcher("[KR]|[^P]"));

        TIntList sites = new TIntArrayList();
        finder.find(new Protein("ACC1", "RESALYTNIKALASKR"), sites);

        Assert.assertEquals(3, sites.size());
        Assert.assertArrayEquals(new int[]{1, 10, 15}, sites.toArray());
    }

    @Test
    public void testDetectCleavageSitesWithAccu() throws ParseException {

        CleavageSiteFinderImpl finder = new CleavageSiteFinderImpl(new CleavageSiteMatcher("[KR]|[^P]"));

        class Accumulator implements CleavageSiteVisitor {

            private final List<Integer> sites;

            public Accumulator(List<Integer> acc) {

                sites = acc;
            }

            @Override
            public void visit(SymbolSequence<AminoAcid> aaSeq, int index) {

                sites.add(index);
            }
        }

        List<Integer> sites = Lists.newArrayList();

        int count = finder.find(new Protein("ACC1", "RESALYTNIKALASKR"), Optional.<CleavageSiteVisitor>of(new Accumulator(sites)));

        Assert.assertEquals(3, count);
        Assert.assertEquals(Arrays.asList(1, 10, 15), sites);
    }
}
