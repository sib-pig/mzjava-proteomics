/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.ms.dbsearch;

import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.Collections;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class DigestTest {

    @Test
    public void testEquals() throws Exception {

        Peptide pepA1 = new Peptide(AminoAcid.A);
        Peptide pepA2 = new Peptide(AminoAcid.A);
        Peptide pepB = new Peptide(AminoAcid.B);

        Digest digestA1 = new Digest(pepA1, "ACC1");
        Digest digestA2 = new Digest(pepA2, "ACC1");
        Digest digestA3 = new Digest(pepA2, "ACC2");
        Digest digestB = new Digest(pepB, "ACC3");

        Assert.assertEquals(true, digestA1.equals(digestA2));
        Assert.assertEquals(true, digestA2.equals(digestA1));
        Assert.assertEquals(digestA2.hashCode(), digestA1.hashCode());

        Assert.assertEquals(false, digestA1.equals(digestA3));
        Assert.assertEquals(false, digestA3.equals(digestA1));

        Assert.assertEquals(false, digestA1.equals(digestB));
        Assert.assertEquals(false, digestB.equals(digestA1));
    }

    @Test
    public void testProteinIdSet() throws Exception {

        Digest digestA1 = new Digest(new Peptide(AminoAcid.A), "ACC1");

        Assert.assertEquals(true, digestA1.getProteinIds().containsAll(Collections.singleton("ACC1")));

        digestA1.addProteinId("ACC2");
        Assert.assertEquals(true, digestA1.getProteinIds().containsAll(Arrays.asList("ACC1", "ACC2")));
    }

    @Test
    public void testPeptide() throws Exception {

        Peptide peptide = Mockito.mock(Peptide.class);

        Digest digest = new Digest(peptide);

        digest.getMolecularMass();

        Mockito.verify(peptide).getMolecularMass();
        Mockito.verifyNoMoreInteractions(peptide);

        Assert.assertEquals(peptide, digest.getPeptide());
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testImmutableSetAdd() {

        Digest digest = new Digest(Mockito.mock(Peptide.class));
        digest.getProteinIds().add("B");
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testImmutableSetRemove() {

        Digest digest = new Digest(Mockito.mock(Peptide.class));
        digest.getProteinIds().remove("A");
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testImmutableSetAddAll() {

        Digest digest = new Digest(Mockito.mock(Peptide.class));
        digest.getProteinIds().addAll(Collections.<String>emptySet());
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testImmutableSetRetainAll() {

        Digest digest = new Digest(Mockito.mock(Peptide.class));
        digest.getProteinIds().retainAll(Collections.emptySet());
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testImmutableSetRemoveAll() {

        Digest digest = new Digest(Mockito.mock(Peptide.class));
        digest.getProteinIds().removeAll(Collections.emptySet());
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testImmutableSetClear() {

        Digest digest = new Digest(Mockito.mock(Peptide.class));
        digest.getProteinIds().clear();
    }

    @Test
    public void testNoProteinId() throws Exception {

        Digest digest = new Digest(Mockito.mock(Peptide.class));

        Assert.assertEquals(Collections.emptySet(), digest.getProteinIds());
    }
}
