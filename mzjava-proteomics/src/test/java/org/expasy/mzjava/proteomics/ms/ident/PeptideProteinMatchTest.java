/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.ms.ident;

import com.google.common.base.Optional;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class PeptideProteinMatchTest {


    @Test
    public void testDatabaseSource() throws Exception {

        PeptideProteinMatch match = new PeptideProteinMatch("ACC1", Optional.of("SwissProt"), Optional.of("A"), Optional.of("R"), 1, 5, PeptideProteinMatch.HitType.UNKNOWN);

        Assert.assertEquals("SwissProt", match.getSearchDatabase().get());
        Assert.assertEquals("A", match.getPreviousAA().get());
        Assert.assertEquals("R", match.getNextAA().get());
        Assert.assertEquals("ACC1", match.getAccession());
        Assert.assertEquals(1, match.getStart());
        Assert.assertEquals(5, match.getEnd());
        Assert.assertEquals(PeptideProteinMatch.HitType.UNKNOWN, match.getHitType());

    }


    @Test
    public void testStartAndEnd() throws Exception {

        PeptideProteinMatch match = new PeptideProteinMatch("ACC1", Optional.<String>absent(), Optional.of("A"), Optional.of("R"), 1, 5, PeptideProteinMatch.HitType.TARGET);

        Assert.assertEquals(Optional.absent(), match.getSearchDatabase());
        Assert.assertEquals("A", match.getPreviousAA().get());
        Assert.assertEquals("R", match.getNextAA().get());
        Assert.assertEquals("ACC1", match.getAccession());
        Assert.assertEquals(1, match.getStart());
        Assert.assertEquals(5, match.getEnd());
        Assert.assertEquals(PeptideProteinMatch.HitType.TARGET, match.getHitType());

    }

    @Test
    public void testNTermAndCTerm() throws Exception {

        PeptideProteinMatch match = new PeptideProteinMatch("ACC1", Optional.<String>absent(), Optional.of("A"), Optional.of("R"), PeptideProteinMatch.HitType.DECOY);

        Assert.assertEquals("A", match.getPreviousAA().get());
        Assert.assertEquals("R", match.getNextAA().get());
        Assert.assertEquals("ACC1", match.getAccession());

        Assert.assertEquals(false, match.isNTerm());
        Assert.assertEquals(false, match.isCTerm());

        match = new PeptideProteinMatch("ACC1", Optional.<String>absent(), Optional.of("-"), Optional.of("R"), PeptideProteinMatch.HitType.DECOY);
        Assert.assertEquals(true, match.isNTerm());
        Assert.assertEquals(false, match.isCTerm());

        match = new PeptideProteinMatch("ACC1", Optional.<String>absent(), Optional.of("A"), Optional.of("-"), PeptideProteinMatch.HitType.DECOY);
        Assert.assertEquals(false, match.isNTerm());
        Assert.assertEquals(true, match.isCTerm());
    }

    @Test
    public void testEqualsAndHash() throws Exception {

        PeptideProteinMatch match1 = new PeptideProteinMatch("ACC1", Optional.<String>absent(), Optional.of("A"), Optional.of("R"), PeptideProteinMatch.HitType.DECOY);
        PeptideProteinMatch match1Again = new PeptideProteinMatch("ACC1", Optional.<String>absent(), Optional.of("A"), Optional.of("R"), PeptideProteinMatch.HitType.UNKNOWN);
        PeptideProteinMatch match2 = new PeptideProteinMatch("ACC1", Optional.<String>absent(), Optional.of("A"), Optional.of("R"), PeptideProteinMatch.HitType.DECOY);

        PeptideProteinMatch match3 = new PeptideProteinMatch("ACC2", Optional.of("db"),Optional.of("A"), Optional.of("R"), 33, 44, PeptideProteinMatch.HitType.DECOY);
        PeptideProteinMatch match4 = new PeptideProteinMatch("ACC2", Optional.of("db3"), Optional.<String>absent(), Optional.of("T"), 33, 44, PeptideProteinMatch.HitType.DECOY);


        Assert.assertFalse(match1.equals(match1Again));
        Assert.assertFalse(match1Again.equals(match1));
        Assert.assertNotEquals(match1.hashCode(), match1Again.hashCode());

        Assert.assertTrue(match1.equals(match2));
        Assert.assertEquals(match1.hashCode(), match2.hashCode());

        Assert.assertTrue(match3.equals(match4));
        Assert.assertEquals(match3.hashCode(), match4.hashCode());
    }

    @Test
    public void testEqualsAndHashWithPositions() throws Exception {

        PeptideProteinMatch match1 = new PeptideProteinMatch("ACC1", Optional.<String>absent(), Optional.of("A"), Optional.of("R"), 1, 5, PeptideProteinMatch.HitType.UNKNOWN);
        PeptideProteinMatch match2 = new PeptideProteinMatch("ACC1", Optional.<String>absent(), Optional.of("A"), Optional.of("R"), 2, 8, PeptideProteinMatch.HitType.UNKNOWN);
        PeptideProteinMatch match3 = new PeptideProteinMatch("ACC2", Optional.<String>absent(), Optional.of("A"), Optional.of("R"), 2, 8, PeptideProteinMatch.HitType.UNKNOWN);

        PeptideProteinMatch match1Again = new PeptideProteinMatch("ACC1", Optional.<String>absent(), Optional.of("A"), Optional.of("R"), 1, 5, PeptideProteinMatch.HitType.UNKNOWN);

        Assert.assertEquals(true, match1.equals(match1Again));
        Assert.assertEquals(true, match1Again.equals(match1));
        Assert.assertEquals(match1.hashCode(), match1Again.hashCode());

        Assert.assertEquals(false, match1.equals(match2));
        Assert.assertEquals(false, match2.equals(match1));

        Assert.assertEquals(false, match1.equals(match3));
        Assert.assertEquals(false, match3.equals(match1));
    }

    @Test(expected=IllegalArgumentException.class)
    public void testConstructorInvalidStart() throws Exception {

        PeptideProteinMatch matchNotValid = new PeptideProteinMatch("ACC1", Optional.<String>absent(), Optional.of("A"), Optional.of("R"), -1, 5, PeptideProteinMatch.HitType.UNKNOWN);
        matchNotValid.getAccession();
    }

    @Test(expected=IllegalArgumentException.class)
    public void testConstructorInvalidEnd() throws Exception {

        PeptideProteinMatch matchNotValid = new PeptideProteinMatch("ACC1", Optional.<String>absent(), Optional.of("A"), Optional.of("R"), 10, 5, PeptideProteinMatch.HitType.UNKNOWN);
        matchNotValid.getAccession();
    }

}
