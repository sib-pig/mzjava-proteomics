/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.mol;

import com.google.common.base.Optional;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import org.expasy.mzjava.core.mol.NumericMass;
import org.expasy.mzjava.core.mol.SymbolSequence;
import org.expasy.mzjava.core.mol.AtomicSymbol;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.proteomics.mol.modification.ModificationResolver;
import org.expasy.mzjava.core.ms.spectrum.FragmentType;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class PeptideTest {

    @Test
    public void testSubSequence() throws Exception {

        Peptide protein = Peptide.parse("MQR(O2)STAT(C)GCFKLX");

        Peptide peptide = protein.subSequence(3, 12);
        Assert.assertEquals("STATGCFKL", peptide.toSymbolString());
        Assert.assertEquals(1, peptide.getModifiedResidueCount());
        Assert.assertTrue(peptide.hasModificationAt(3));
    }

    @Test
    public void testSubSequence2() throws Exception {

        Peptide protein = Peptide.parse("MQR(O2)STAT(C)GCFKLX");

        Peptide peptide = protein.subSequence(0, 3);
        Assert.assertEquals("MQR", peptide.toSymbolString());
        Assert.assertEquals(1, peptide.getModifiedResidueCount());
        Assert.assertTrue(peptide.hasModificationAt(2));
    }

    @Test
    public void testSubSequence3() throws Exception {

        Peptide protein = Peptide.parse("MQR(O2)STAT(C)GCFKLX");

        Peptide peptide = protein.subSequence(0, protein.size());
        Assert.assertEquals("MQRSTATGCFKLX", peptide.toSymbolString());
        Assert.assertEquals(2, peptide.getModifiedResidueCount());
        Assert.assertTrue(peptide.hasModificationAt(2));
        Assert.assertTrue(peptide.hasModificationAt(6));
    }

    @Test
    public void testPeptide() throws Exception {

        Peptide peptide = Peptide.parse("UHRCUHHA(C4H12)U(C)EHE");

        Assert.assertTrue(peptide.hasModificationAt(7));
        Assert.assertTrue(peptide.hasModificationAt(8));
    }

    @Test
    public void testPeptideCopy() throws Exception {

        Peptide peptide = Peptide.parse("UHRCUHHA(C4H9)U(C)EHE");

        Peptide copy = new Peptide(peptide);

        Assert.assertEquals("UHRCUHHAUEHE", copy.toSymbolString());
        Assert.assertTrue(copy.hasModificationAt(7));
        Assert.assertTrue(copy.hasModificationAt(8));
    }

    @Test
    public void testPeptideWithNTermModsCopy() throws Exception {

        Peptide peptide = new PeptideBuilder("UHRCUHHAUEHE")
                .addModification(ModAttachment.N_TERM, mock(Modification.class))
                .build();

        Peptide copy = new Peptide(peptide);

        Assert.assertEquals("UHRCUHHAUEHE", copy.toSymbolString());
        Assert.assertTrue(copy.hasModificationAt(ModAttachment.N_TERM));
    }

    @Test
    public void testPeptideWithCTermModsCopy() throws Exception {

        Peptide peptide = new PeptideBuilder("UHRCUHHAUEHE")
                .addModification(ModAttachment.C_TERM, mock(Modification.class))
                .build();

        Peptide copy = new Peptide(peptide);

        Assert.assertEquals("UHRCUHHAUEHE", copy.toSymbolString());
        Assert.assertTrue(copy.hasModificationAt(ModAttachment.C_TERM));
    }

    @Test
    public void testHasModsAt() throws Exception {

        Peptide peptide = new PeptideBuilder("UHRCUHHAUEHE")
                .addModification(ModAttachment.C_TERM, mock(Modification.class))
                .build();

        Assert.assertEquals("UHRCUHHAUEHE", peptide.toSymbolString());
        Assert.assertTrue(peptide.hasModificationAt(EnumSet.of(ModAttachment.C_TERM, ModAttachment.N_TERM)));
    }

    @Test
    public void testSuffixPeptideWithNTermModsCopy() throws Exception {

        Peptide peptide = new PeptideBuilder("UHRCUHHAUEHE")
                .addModification(ModAttachment.N_TERM, mock(Modification.class))
                .build();

        Peptide copy = new Peptide(peptide, 1, 12);

        Assert.assertEquals("HRCUHHAUEHE", copy.toSymbolString());
        Assert.assertFalse(copy.hasModifications());
    }

    @Test
    public void testSuffixPeptideWithCTermModsCopy() throws Exception {

        Peptide peptide = new PeptideBuilder("UHRCUHHAUEHE")
                .addModification(ModAttachment.C_TERM, mock(Modification.class))
                .build();

        Peptide copy = new Peptide(peptide, 1, 12);

        Assert.assertEquals("HRCUHHAUEHE", copy.toSymbolString());
        Assert.assertTrue(copy.hasModificationAt(ModAttachment.C_TERM));
    }

    @Test
    public void testPrefixPeptideWithNTermModsCopy() throws Exception {

        Peptide peptide = new PeptideBuilder("UHRCUHHAUEHE")
                .addModification(ModAttachment.N_TERM, mock(Modification.class))
                .build();

        Peptide copy = new Peptide(peptide, 0, 5);

        Assert.assertEquals("UHRCU", copy.toSymbolString());
        Assert.assertTrue(copy.hasModificationAt(ModAttachment.N_TERM));
    }

    @Test
    public void testPrefixPeptideWithCTermModsCopy() throws Exception {

        Peptide peptide = new PeptideBuilder("UHRCUHHAUEHE")
                .addModification(ModAttachment.C_TERM, mock(Modification.class))
                .build();

        Peptide copy = new Peptide(peptide, 0, 5);

        Assert.assertEquals("UHRCU", copy.toSymbolString());
        Assert.assertFalse(copy.hasModifications());
    }

    @Test
    public void testStringConstructor() throws Exception {

        Peptide peptide = Peptide.parse("PEPTIDE");

        Assert.assertEquals(7, peptide.size());
        Assert.assertEquals(AminoAcid.P, peptide.getSymbol(0));
        Assert.assertEquals(AminoAcid.E, peptide.getSymbol(1));
        Assert.assertEquals(AminoAcid.P, peptide.getSymbol(2));
        Assert.assertEquals(AminoAcid.T, peptide.getSymbol(3));
        Assert.assertEquals(AminoAcid.I, peptide.getSymbol(4));
        Assert.assertEquals(AminoAcid.D, peptide.getSymbol(5));
        Assert.assertEquals(AminoAcid.E, peptide.getSymbol(6));
    }

    @Test
    public void testMods() throws Exception {

        Peptide peptide = new PeptideBuilder(AminoAcid.P, AminoAcid.E, AminoAcid.T)
                .addModification(1, new Modification("CH3", new Composition.Builder(AtomicSymbol.C).add(AtomicSymbol.H, 3).build()))
                .addModification(1, new Modification("O", new Composition.Builder(AtomicSymbol.O).build()))
                .build();

        String string = peptide.toString();

        Peptide peptide2 = Peptide.parse(string);

        Assert.assertEquals(AminoAcid.P, peptide2.getSymbol(0));
        Assert.assertEquals(AminoAcid.E, peptide2.getSymbol(1));
        Assert.assertEquals(AminoAcid.T, peptide2.getSymbol(2));

        List<Modification> mods = peptide2.getModificationsAt(1, ModAttachment.all);
        Assert.assertEquals(2, mods.size());

        Assert.assertEquals(Modification.parseModification("CH3"), mods.get(0));

        Assert.assertEquals(Modification.parseModification("O"), mods.get(1));
    }

    @Test
    public void testModificationList() throws Exception {


        Modification mod1 = mockMod("7.69", 7.69);
        Modification mod2 = mockMod("18.0", 18);

        Peptide peptide = new PeptideBuilder("CERVILAS")
                .addModification(0, mod1)
                .addModification(ModAttachment.N_TERM, mod2)
                .build();

        List<Modification> mods = peptide.getModificationsAt(0, ModAttachment.all);

        Assert.assertEquals(2, mods.size());
        Assert.assertEquals(mod1, mods.get(0));
        Assert.assertEquals(mod2, mods.get(1));

        Modification cTermMod = mockMod("cTerm", 873.69);
        Modification mod3 = mockMod("last", 18);

        peptide = new PeptideBuilder("CERVILAS")
                .addModification(0, mod1)
                .addModification(ModAttachment.N_TERM, mod2)
                .addModification(ModAttachment.C_TERM, cTermMod)
                .addModification(peptide.size() - 1, mod3)
                .build();
        List<Modification> cTermList = peptide.getModificationsAt(peptide.size() - 1, ModAttachment.all);
        Assert.assertEquals(2, cTermList.size());
        Assert.assertEquals(mod3, cTermList.get(0));
        Assert.assertEquals(cTermMod, cTermList.get(1));
        Assert.assertEquals(true, cTermList.contains(cTermMod));
    }

    @Test
    public void testGetAllModifications() throws Exception {


        Modification nTermMod = mockMod("18.0", 18);
        Modification mod1 = mockMod("7.69", 7.69);
        Modification mod2 = mockMod("last", 18);
        Modification cTermMod = mockMod("cTerm", 873.69);

        Peptide peptide = new PeptideBuilder("CERVILAS")
                .addModification(ModAttachment.N_TERM, nTermMod)
                .addModification(0, mod1)
                .addModification(7, mod2)
                .addModification(ModAttachment.C_TERM, cTermMod)
                .build();

        List<Modification> modifications = new ArrayList<Modification>(peptide.getModifications(ModAttachment.all));
        Collections.sort(modifications, new Comparator<Modification>() {
            @Override
            public int compare(Modification o1, Modification o2) {

                return o1.toString().compareTo(o2.toString());
            }
        });

        Assert.assertEquals(4, modifications.size());
        Assert.assertEquals(nTermMod, modifications.get(0));
        Assert.assertEquals(mod1, modifications.get(1));
        Assert.assertEquals(cTermMod, modifications.get(2));
        Assert.assertEquals(mod2, modifications.get(3));
    }

    @Test
    public void testGetAllModifications2() throws Exception {


        Modification nTermMod = mockMod("18.0", 18);
        Modification mod1 = mockMod("7.69", 7.69);
        Modification mod2 = mockMod("last", 18);
        Modification cTermMod = mockMod("cTerm", 873.69);

        Peptide peptide = new PeptideBuilder("CERVILAS")
                .addModification(ModAttachment.N_TERM, nTermMod)
                .addModification(0, mod1)
                .addModification(7, mod2)
                .addModification(ModAttachment.C_TERM, cTermMod)
                .build();

        List<Modification> modifications = new ArrayList<Modification>(peptide.getModifications(ModAttachment.sideChainSet));
        Collections.sort(modifications, new Comparator<Modification>() {
            @Override
            public int compare(Modification o1, Modification o2) {

                return o1.toString().compareTo(o2.toString());
            }
        });

        Assert.assertEquals(2, modifications.size());
        Assert.assertEquals(mod1, modifications.get(0));
        Assert.assertEquals(mod2, modifications.get(1));
    }

    @Test
    public void testModificationIndexes() throws Exception {

        Peptide peptide = new PeptideBuilder("CERVILAS")
                .addModification(ModAttachment.N_TERM, mockMod("18.0", 18))
                .addModification(1, mockMod("7.69", 7.69))
                .addModification(6, mockMod("last", 18))
                .addModification(ModAttachment.C_TERM, mockMod("cTerm", 873.69))
                .build();

        Assert.assertArrayEquals(new int[]{0, 1, 6, 7}, peptide.getModificationIndexes(ModAttachment.all));
        Assert.assertArrayEquals(new int[]{1, 6}, peptide.getModificationIndexes(ModAttachment.sideChainSet));
        Assert.assertArrayEquals(new int[]{0}, peptide.getModificationIndexes(ModAttachment.nTermSet));
        Assert.assertArrayEquals(new int[]{7}, peptide.getModificationIndexes(ModAttachment.cTermSet));
    }

    @Test
    public void testModificationIndexes2() throws Exception {

        Peptide peptide = new PeptideBuilder("CERVILAS")
                .addModification(ModAttachment.N_TERM, mockMod("18.0", 18))
                .addModification(1, mockMod("7.69", 7.69))
                .addModification(7, mockMod("last", 18))
                .addModification(ModAttachment.C_TERM, mockMod("cTerm", 873.69))
                .build();

        Assert.assertArrayEquals(new int[]{0, 1, 7}, peptide.getModificationIndexes(ModAttachment.all));
    }

    @Test
    public void testModificationIndexes3() throws Exception {

        Peptide peptide = new PeptideBuilder("CERVILAS")
                .addModification(ModAttachment.N_TERM, mockMod("18.0", 18))
                .addModification(0, mockMod("7.69", 7.69))
                .addModification(6, mockMod("last", 18))
                .addModification(ModAttachment.C_TERM, mockMod("cTerm", 873.69))
                .build();

        Assert.assertArrayEquals(new int[]{0, 6, 7}, peptide.getModificationIndexes(ModAttachment.all));
    }

    @Test
    public void testModResidueCount() throws Exception {

        Peptide peptide = new PeptideBuilder("CERVILAS")
                .addModification(ModAttachment.N_TERM, mockMod("1", 1))
                .addModification(ModAttachment.N_TERM, mockMod("2", 2))
                .addModification(0, mockMod("13.6", 13.6)).build();
        Assert.assertEquals(1, peptide.getModifiedResidueCount());

        peptide = new PeptideBuilder("CERVILAS")
                .addModification(ModAttachment.N_TERM, mockMod("1", 1))
                .addModification(ModAttachment.N_TERM, mockMod("2", 2))
                .addModification(0, mockMod("13.6", 13.6))
                .addModification(ModAttachment.C_TERM, mockMod("7.6", 7.6))
                .build();
        Assert.assertEquals(2, peptide.getModifiedResidueCount());
    }

    @Test
    public void testToString() throws Exception {

        Peptide peptide = new PeptideBuilder("CERVILAS")
                .addModification(0, new Modification("O", new Composition.Builder(AtomicSymbol.O).build()))
                .addModification(0, new Modification("CH3", new Composition.Builder(AtomicSymbol.C).add(AtomicSymbol.H, 3).build()))
                .addModification(ModAttachment.N_TERM, new Modification("H", new Composition.Builder(AtomicSymbol.H).build()))
                .build();

        Assert.assertEquals("(H)_C(O, CH3)ERVILAS", peptide.toString());
    }

    @Test
    public void testToString2() throws Exception {

        Peptide peptide = new PeptideBuilder("CERVILAS")
                .addModification(ModAttachment.N_TERM, new Modification("H", new Composition.Builder(AtomicSymbol.H).build()))
                .addModification(0, new Modification("O", new Composition.Builder(AtomicSymbol.O).build()))
                .addModification(0, new Modification("CH3", new Composition.Builder(AtomicSymbol.C).add(AtomicSymbol.H, 3).build()))
                .addModification(7, new Modification("CH3", new Composition.Builder(AtomicSymbol.C).add(AtomicSymbol.H, 3).build()))
                .addModification(ModAttachment.C_TERM, new Modification("F", new Composition.Builder(AtomicSymbol.F).build()))
                .build();

        Assert.assertEquals("(H)_C(O, CH3)ERVILAS(CH3)_(F)", peptide.toString());
    }

    @Test
    public void testCTermMods() throws Exception {


        Modification cTermMod = Modification.parseModification("O");
        Peptide peptide = new PeptideBuilder("PEPT")
                .addModification(ModAttachment.C_TERM, cTermMod)
                .addModification(3, Modification.parseModification("H"))
                .build();

        Assert.assertEquals(Modification.parseModification("O"), peptide.getModifications(ModAttachment.cTermSet).get(0));
    }

    @Test
    public void testNTermMods() throws Exception {


        Modification nTermMod = Modification.parseModification("O");
        Peptide peptide = new PeptideBuilder("PEPT")
                .addModification(ModAttachment.N_TERM, nTermMod)
                .addModification(0, Modification.parseModification("H"))
                .build();

        Assert.assertEquals(Modification.parseModification("O"), peptide.getModifications(ModAttachment.nTermSet).get(0));
    }

    @Test
    public void testCalculateMassJPL41() throws Exception {

        Peptide peptide = Peptide.parse("AAAAYIYEAVNK");

        Assert.assertEquals(1282.6557, peptide.getMolecularMass(), 0.0001);
    }

    @Test
    public void testCalculateMass2() throws Exception {

        Peptide peptide = new PeptideBuilder("AAAAYIYEAVNK")
                .addModification(5, mockMod("mod", 45))
                .build();

        Assert.assertEquals(1327.6557, peptide.getMolecularMass(), 0.0001);
    }

    @Test (expected = IllegalStateException.class)
    public void testCalculateUnknownMass() throws Exception {

        Peptide peptide = Peptide.parse("BEBTIDE");
        peptide.getMolecularMass();
    }

    @Test
    public void testHasknownComposition() throws Exception {

        Peptide peptide = Peptide.parse("PEPTIDE");

        Assert.assertEquals(false, peptide.hasAmbiguousAminoAcids());
        Assert.assertEquals(Collections.<AminoAcid>emptySet(), peptide.getAmbiguousAminoAcids());
    }

    @Test
    public void testHasUnknownComposition() throws Exception {

        Peptide peptide = Peptide.parse("BEBTIDE");

        Assert.assertEquals(true, peptide.hasAmbiguousAminoAcids());
        Assert.assertEquals(Sets.newHashSet(AminoAcid.B), peptide.getAmbiguousAminoAcids());
    }

    @Test
    public void testCopy() throws Exception {

        Peptide peptide = Peptide.parse("ANKER");
        Peptide copy = new Peptide(peptide);

        Assert.assertEquals(peptide, copy);
    }

    @Test
    public void testCopyWithMods() throws Exception {

        Peptide peptide = new PeptideBuilder("AAAAYIYEAVNK")
                .addModification(5, mockMod("mod", 45))
                .build();

        Peptide copy = new Peptide(peptide);

        Assert.assertEquals(peptide, copy);
    }

    @Test
    public void testCalculateMz() throws Exception {

        Peptide peptide = Peptide.parse("(H2C2O)_AAAAAAGAGPEMVR");

        Assert.assertEquals(642.8218860221999, peptide.calculateMz(2), 0.00000000000001);
    }

    @Test
    public void testCreateFragment() throws Exception {

        String seq = "PEPTIDE";

        Peptide peptide = Peptide.parse(seq);

        for (int i = 1; i < peptide.size(); i++) {

            Assert.assertEquals(seq.substring(0, i), peptide.createFragment(0, i, FragmentType.FORWARD).toString());
        }
    }

    @Test
    public void testCreateNtermFragmentFromModNtermPeptideHotFix14() throws Exception {

        Peptide peptide = new PeptideBuilder("VV")
                .addModification(ModAttachment.N_TERM, mock(Modification.class))
                .build();

        PeptideFragment fragment = peptide.createFragment(0, 1, FragmentType.FORWARD);

        Assert.assertTrue(fragment.hasModificationAt(ModAttachment.N_TERM));
    }

    @Test
    public void testCreateCtermFragmentFromModNtermPeptideHotFix14() throws Exception {

        Peptide peptide = new PeptideBuilder("VV")
                .addModification(ModAttachment.N_TERM, mock(Modification.class))
                .build();

        PeptideFragment fragment = peptide.createFragment(1, 2, FragmentType.REVERSE);

        Assert.assertFalse(fragment.hasModificationAt(ModAttachment.N_TERM));
    }

    @Test
    public void testCreateNtermFragmentFromModCtermPeptideHotFix14() throws Exception {

        Peptide peptide = new PeptideBuilder("VV")
                .addModification(ModAttachment.C_TERM, mock(Modification.class))
                .build();

        PeptideFragment fragment = peptide.createFragment(0, 1, FragmentType.FORWARD);

        Assert.assertFalse(fragment.hasModificationAt(ModAttachment.C_TERM));
    }

    @Test
    public void testCreateCtermFragmentFromModCtermPeptideHotFix14() throws Exception {

        Peptide peptide = new PeptideBuilder("VV")
                .addModification(ModAttachment.C_TERM, mock(Modification.class))
                .build();

        PeptideFragment fragment = peptide.createFragment(1, 2, FragmentType.REVERSE);

        Assert.assertTrue(fragment.hasModificationAt(ModAttachment.C_TERM));
    }

    @Test
    public void testHashAndEquals() throws Exception {

        Peptide peptide1 = Peptide.parse("STYY");
        Peptide peptide2 = Peptide.parse("STYY");
        Peptide peptide3 = new PeptideBuilder("STYY")
                .addModification(ModAttachment.N_TERM, mockMod("nTerm", 78.69))
                .build();

        Assert.assertEquals(true, peptide1.equals(peptide2));
        Assert.assertEquals(false, peptide1.equals(peptide3));
        Assert.assertEquals(false, peptide2.equals(peptide3));

        Assert.assertEquals(peptide1.hashCode(), peptide2.hashCode());
        Assert.assertNotSame(peptide1.hashCode(), peptide3.hashCode());
        Assert.assertNotSame(peptide2.hashCode(), peptide3.hashCode());
    }

    @Test
    public void testHashAndEquals2() throws Exception {

        Peptide peptide1 = Peptide.parse("STYY");
        Peptide peptide2 = Peptide.parse("STYY");
        Peptide peptide3 = new PeptideBuilder("STYY")
                .addModification(1, mockMod("nTerm", 78.69))
                .build();

        Assert.assertEquals(true, peptide1.equals(peptide2));
        Assert.assertEquals(false, peptide1.equals(peptide3));
        Assert.assertEquals(false, peptide2.equals(peptide3));

        Assert.assertEquals(peptide1.hashCode(), peptide2.hashCode());
        Assert.assertNotSame(peptide1.hashCode(), peptide3.hashCode());
        Assert.assertNotSame(peptide2.hashCode(), peptide3.hashCode());
    }

    @Test
    public void testHasSameSequence() throws Exception {

        Peptide peptide1 = Peptide.parse("STYY");
        Peptide peptide2 = Peptide.parse("STYY");
        Peptide peptide3 = new PeptideBuilder("STYY")
                .addModification(1, mockMod("nTerm", 78.69))
                .build();
        Peptide peptide4 = Peptide.parse("STYK");

        Assert.assertEquals(true, peptide1.hasSameSequence(peptide2));
        Assert.assertEquals(true, peptide2.hasSameSequence(peptide3));
        Assert.assertEquals(false, peptide2.hasSameSequence(peptide4));
    }

    @Test
    public void testHasSameSequence2() throws Exception {

        Peptide peptide = Peptide.parse("STYY");
        @SuppressWarnings("unchecked")
        SymbolSequence<AminoAcid> seqSame = (SymbolSequence<AminoAcid>) mock(SymbolSequence.class);
        when(seqSame.size()).thenReturn(4);
        when(seqSame.getSymbol(0)).thenReturn(AminoAcid.S);
        when(seqSame.getSymbol(1)).thenReturn(AminoAcid.T);
        when(seqSame.getSymbol(2)).thenReturn(AminoAcid.Y);
        when(seqSame.getSymbol(3)).thenReturn(AminoAcid.Y);

        @SuppressWarnings("unchecked")
        SymbolSequence<AminoAcid> seqDiffLen = (SymbolSequence<AminoAcid>) mock(SymbolSequence.class);
        when(seqDiffLen.size()).thenReturn(5);

        @SuppressWarnings("unchecked")
        SymbolSequence<AminoAcid> seqDiffSeq = (SymbolSequence<AminoAcid>) mock(SymbolSequence.class);
        when(seqDiffSeq.size()).thenReturn(4);
        when(seqDiffSeq.getSymbol(0)).thenReturn(AminoAcid.S);
        when(seqDiffSeq.getSymbol(1)).thenReturn(AminoAcid.Y);
        when(seqDiffSeq.getSymbol(2)).thenReturn(AminoAcid.T);
        when(seqDiffSeq.getSymbol(3)).thenReturn(AminoAcid.Y);

        Assert.assertEquals(true, peptide.hasSameSequence(seqSame));
        Assert.assertEquals(false, peptide.hasSameSequence(seqDiffLen));
        Assert.assertEquals(false, peptide.hasSameSequence(seqDiffSeq));
    }

    @Test
    public void testSearchAAIndices() throws Exception {

        Peptide peptide = Peptide.parse("PEPTIDE");

        int[] indices = peptide.getSymbolIndexes(AminoAcid.P);

        Assert.assertArrayEquals(new int[]{0, 2}, indices);
    }

    @Test
    public void testSearchAAIndicesNotFound() throws Exception {

        Peptide peptide = Peptide.parse("PEPTIDE");

        int[] indices = peptide.getSymbolIndexes(AminoAcid.R);

        Assert.assertArrayEquals(new int[]{}, indices);
    }

    @Test
    public void testIsSameSequence() throws Exception {

        Peptide peptide1 = new PeptideBuilder("CERVILAS")
                .addModification(3, mockMod("mod", 16.8))
                .build();
        Peptide peptide2 = Peptide.parse("CERVILAS");
        Peptide peptide3 = Peptide.parse("CERVILAK");
        Peptide peptide4 = Peptide.parse("CERV");

        Assert.assertEquals(true, peptide1.hasSameSequence(peptide2));
        Assert.assertEquals(false, peptide1.hasSameSequence(peptide3));
        Assert.assertEquals(false, peptide1.hasSameSequence(peptide4));
    }

    private Modification mockMod(String label, double mw) {

        Modification mod = mock(Modification.class);
        when(mod.getLabel()).thenReturn(label);
        when(mod.getMolecularMass()).thenReturn(mw);
        when(mod.toString()).thenReturn(label);

        return mod;
    }

    @Test
    public void testParse() throws Exception {

        Peptide peptide = Peptide.parse("MQRSTATGCFKLX");

        Assert.assertEquals(false, peptide.hasModifications());
        Assert.assertEquals("MQRSTATGCFKLX", peptide.toSymbolString());
    }

    @Test
    public void testGetModificationCount() throws Exception {

        Peptide peptide = new PeptideBuilder("CERVILAS")
                .addModification(ModAttachment.N_TERM, new Modification("H", new Composition.Builder(AtomicSymbol.H).build()))
                .addModification(0, new Modification("O", new Composition.Builder(AtomicSymbol.O).build()))
                .addModification(0, new Modification("CH3", new Composition.Builder(AtomicSymbol.C).add(AtomicSymbol.H, 3).build()))
                .addModification(7, new Modification("CH3", new Composition.Builder(AtomicSymbol.C).add(AtomicSymbol.H, 3).build()))
                .addModification(ModAttachment.C_TERM, new Modification("F", new Composition.Builder(AtomicSymbol.F).build()))
                .build();

        Assert.assertEquals(5, peptide.getModificationCount());
    }

    @Test
    public void testAAOccurrence() throws Exception {

        Peptide peptide = Peptide.parse("CEYRSPTSILK");

        Assert.assertEquals(4, peptide.countAminoAcidsIn(EnumSet.of(AminoAcid.S, AminoAcid.T, AminoAcid.Y)));
        Assert.assertEquals(0, peptide.countAminoAcidsIn(EnumSet.noneOf(AminoAcid.class)));
    }

    @Test
    public void testSunRegExpBugToFix20() throws Exception {

        String seq = "ATMYPDVPIGQLGQTVVCDVSVGLICKNEDQKPGGVIPMAFCLNYEINVQCCECVTQPTTMTTTTTENPTPPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTGPPTHTSTAPIAELTTSNPPPESSTPQTSR";

        Peptide.parse(seq);
    }

    @Test
    public void testSubSequence5() throws Exception {

        Peptide peptide = Peptide.parse("CERVILASK");

        Assert.assertEquals(Peptide.parse("C"), peptide.subSequence(IonType.a, 1));
        Assert.assertEquals(Peptide.parse("CER"), peptide.subSequence(IonType.a, 3));
        Assert.assertEquals(Peptide.parse("CERVILASK"), peptide.subSequence(IonType.a, 9));

        Assert.assertEquals(Peptide.parse("K"), peptide.subSequence(IonType.y, 1));
        Assert.assertEquals(Peptide.parse("ASK"), peptide.subSequence(IonType.y, 3));
        Assert.assertEquals(Peptide.parse("CERVILASK"), peptide.subSequence(IonType.y, 9));

        Assert.assertEquals(Peptide.parse("C"), peptide.subSequence(IonType.i, 1));
        Assert.assertEquals(Peptide.parse("V"), peptide.subSequence(IonType.i, 4));
        Assert.assertEquals(Peptide.parse("K"), peptide.subSequence(IonType.i, 9));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSubSequence6() throws Exception {

        Peptide peptide = Peptide.parse("MSK");

        peptide.subSequence(IonType.i, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSubSequence7() throws Exception {

        Peptide peptide = Peptide.parse("MSK");

        peptide.subSequence(IonType.i, 4);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSubSequence8() throws Exception {

        Peptide peptide = Peptide.parse("MSK");

        peptide.subSequence(IonType.p, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSubSequence9() throws Exception {

        Peptide peptide = Peptide.parse("MSK");

        peptide.subSequence(IonType.bw_internal, 1);
    }

    @Test
    public void testParse1() throws Exception {

        Peptide peptide = Peptide.parse("AAADLMAYCEAHAK");

        assertEquals("AAADLMAYCEAHAK", peptide.toSymbolString());
        assertEquals(0, peptide.getModificationCount());
    }

    @Test
    public void testParse2() throws Exception {

        Peptide peptide = Peptide.parse("AAADLMAYC(ICAT-D:2H(8))EAHAK");
        assertEquals("AAADLMAYCEAHAK", peptide.toSymbolString());
        assertEquals(1, peptide.getModificationCount());
        assertEquals(Modification.parseModification("ICAT-D:2H(8):H26H[2]8C20N4O5S"),
                peptide.getModificationsAt(8, EnumSet.allOf(ModAttachment.class)).get(0));
    }

    @Test
    public void testParse3() throws Exception {

        Modification nTermMod = mock(Modification.class);
        Modification cTermMod = mock(Modification.class);
        Modification internal = Modification.parseModification("ICAT-D:2H(8):H26H[2]8C20N4O5S");

        ModificationResolver modResolver = mock(ModificationResolver.class);
        when(modResolver.resolve("n-term")).thenReturn(Optional.of(nTermMod));
        when(modResolver.resolve("c-term")).thenReturn(Optional.of(cTermMod));
        when(modResolver.resolve("ICAT-D:2H(8)")).thenReturn(Optional.of(internal));

        Peptide peptide = Peptide.parse("(n-term)_AAADLMAYC(ICAT-D:2H(8))EAHAK_(c-term)", modResolver);

        assertEquals("AAADLMAYCEAHAK", peptide.toSymbolString());

        assertEquals(1, peptide.getModificationsAt(8, EnumSet.allOf(ModAttachment.class)).size());
        assertEquals(internal, peptide.getModificationsAt(8, EnumSet.allOf(ModAttachment.class)).get(0));

        Assert.assertEquals(1, peptide.getModifications(EnumSet.of(ModAttachment.N_TERM)).size());
        Assert.assertEquals(nTermMod, peptide.getModifications(EnumSet.of(ModAttachment.N_TERM)).get(0));

        Assert.assertEquals(1, peptide.getModifications(EnumSet.of(ModAttachment.C_TERM)).size());
        Assert.assertEquals(cTermMod, peptide.getModifications(EnumSet.of(ModAttachment.C_TERM)).get(0));
    }

    @Test
    public void testParse4() throws Exception {

        Peptide peptide = Peptide.parse("MQRSTATGCFKLX");

        assertEquals(false, peptide.hasModifications());
        assertEquals("MQRSTATGCFKLX", peptide.toSymbolString());
    }

    @Test
    public void testParse5() throws Exception {

        Peptide peptide = Peptide.parse("MQR(O2)STAT(C)GCFKLX");

        Assert.assertTrue(peptide.hasModifications());
        assertEquals(2, peptide.getModifiedResidueCount());

        List<Modification> mods1 = peptide.getModificationsAt(2, ModAttachment.all);
        assertEquals(1, mods1.size());
        List<Modification> mods2 = peptide.getModificationsAt(6, ModAttachment.all);
        assertEquals(1, mods2.size());

        assertEquals("MQRSTATGCFKLX", peptide.toSymbolString());
    }

    @Test
    public void testParse6() throws Exception {

        Peptide peptide = Peptide.parse("(O2)_MQRSTATGCFKLX_(C)");

        Assert.assertTrue(peptide.hasModifications());
        assertEquals(2, peptide.getModifiedResidueCount());

        List<Modification> mods1 = peptide.getModificationsAt(0, ModAttachment.all);
        assertEquals(1, mods1.size());
        List<Modification> mods2 = peptide.getModificationsAt(12, ModAttachment.all);
        assertEquals(1, mods2.size());

        assertEquals("MQRSTATGCFKLX", peptide.toSymbolString());
    }

    /**
     * Test that we can parse a large peptide
     */
    @Test
    public void testParse7() throws Exception {

        String seq = "MGLPLARLAAVCLALSLAGGSELQTEGRTRYHGRNVCSTWGNFHYKTFDGDVFRFPGLCD" +
                "YNFASDCRGSYKEFAVHLKRGPGQAEAPAGVESILLTIKDDTIYLTRHLAVLNGAVVSTP" +
                "HYSPGLLIEKSDAYTKVYSRAGLTLMWNREDALMLELDTKFRNHTCGLCGDYNGLQSYSE" +
                "FLSDGVLFSPLEFGNMQKINQPDVVCEDPEEEVAPASCSEHRAECERLLTAEAFADCQDL" +
                "VPLEPYLRACQQDRCRCPGGDTCVCSTVAEFSRQCSHAGGRPGNWRTATLCPKTCPGNLV" +
                "YLESGSPCMDTCSHLEVSSLCEEHRMDGCFCPEGTVYDDIGDSGCVPVSQCHCRLHGHLY" +
                "TPGQEITNDCEQCVCNAGRWVCKDLPCPGTCALEGGSHITTFDGKTYTFHGDCYYVLAKG" +
                "DHNDSYALLGELAPCGSTDKQTCLKTVVLLADKKKNAVVFKSDGSVLLNQLQVNLPHVTA" +
                "SFSVFRPSSYHIMVSMAIGVRLQVQLAPVMQLFVTLDQASQGQVQGLCGNFNGLEGDDFK" +
                "TASGLVEATGAGFANTWKAQSTCHDKLDWLDDPCSLNIESANYAEHWCSLLKKTETPFGR" +
                "CHSAVDPAEYYKRCKYDTCNCQNNEDCLCAALSSYARACTAKGVMLWGWREHVCNKDVGS" +
                "CPNSQVFLYNLTTCQQTCRSLSEADSHCLEGFAPVDGCGCPDHTFLDEKGRCVPLAKCSC" +
                "YHRGLYLEAGDVVVRQEERCVCRDGRLHCRQIRLIGQSCTAPKIHMDCSNLTALATSKPR" +
                "ALSCQTLAAGYYHTECVSGCVCPDGLMDDGRGGCVVEKECPCVHNNDLYSSGAKIKVDCN" +
                "TCTCKRGRWVCTQAVCHGTCSIYGSGHYITFDGKYYDFDGHCSYVAVQDYCGQNSSLGSF" +
                "SIITENVPCGTTGVTCSKAIKIFMGRTELKLEDKHRVVIQRDEGHHVAYTTREVGQYLVV" +
                "ESSTGIIVIWDKRTTVFIKLAPSYKGTVCGLCGNFDHRSNNDFTTRDHMVVSSELDFGNS" +
                "WKEAPTCPDVSTNPEPCSLNPHRRSWAEKQCSILKSSVFSICHSKVDPKPFYEACVHDSC" +
                "SCDTGGDCECFCSAVASYAQECTKEGACVFWRTPDLCPIFCDYYNPPHECEWHYEPCGNR" +
                "SFETCRTINGIHSNISVSYLEGCYPRCPKDRPIYEEDLKKCVTADKCGCYVEDTHYPPGA" +
                "SVPTEETCKSCVCTNSSQVVCRPEEGKILNQTQDGAFCYWEICGPNGTVEKHFNICSITT" +
                "RPSTLTTFTTITLPTTPTSFTTTTTTTTPTSSTVLSTTPKLCCLWSDWINEDHPSSGSDD" +
                "GDREPFDGVCGAPEDIECRSVKDPHLSLEQHGQKVQCDVSVGFICKNEDQFGNGPFGLCY" +
                "DYKIRVNCCWPMDKCITTPSPPTTTPSPPPTTTTTLPPTTTPSPPTTTTTTPPPTTTPSP" +
                "PITTTTTPLPTTTPSPPISTTTTPPPTTTPSPPTTTPSPPTTTPSPPTTTTTTPPPTTTP" +
                "SPPMTTPITPPASTTTLPPTTTPSPPTTTTTTPPPTTTPSPPTTTPITPPTSTTTLPPTT" +
                "TPSPPPTTTTTPPPTTTPSPPTTTTPSPPTITTTTPPPTTTPSPPTTTTTTPPPTTTPSP" +
                "PTTTPITPPTSTTTLPPTTTPSPPPTTTTTPPPTTTPSPPTTTTPSPPITTTTTPPPTTT" +
                "PSSPITTTPSPPTTTMTTPSPTTTPSSPITTTTTPSSTTTPSPPPTTMTTPSPTTTPSPP" +
                "TTTMTTLPPTTTSSPLTTTPLPPSITPPTFSPFSTTTPTTPCVPLCNWTGWLDSGKPNFH" +
                "KPGGDTELIGDVCGPGWAANISCRATMYPDVPIGQLGQTVVCDVSVGLICKNEDQKPGGV" +
                "IPMAFCLNYEINVQCCECVTQPTTMTTTTTENPTPPTTTPITTTTTVTPTPTPTGTQTPT" +
                "TTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTP" +
                "TPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPIT" +
                "TTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGT" +
                "QTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTV" +
                "TPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTT" +
                "TPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPT" +
                "PTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITT" +
                "TTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQ" +
                "TPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVT" +
                "PTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTT" +
                "PITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTP" +
                "TGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTT" +
                "TTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQT" +
                "PTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTP" +
                "TPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTP" +
                "ITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPT" +
                "GTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTT" +
                "TVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTP" +
                "TTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPT" +
                "PTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPI" +
                "TTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTG" +
                "TQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTT" +
                "VTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPT" +
                "TTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTP" +
                "TPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPIT" +
                "TTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGT" +
                "QTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTV" +
                "TPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTT" +
                "TPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPT" +
                "PTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITT" +
                "TTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQ" +
                "TPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVT" +
                "PTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTT" +
                "PITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTP" +
                "TGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTT" +
                "TTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQT" +
                "PTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTP" +
                "TPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTGPPTH" +
                "TSTAPIAELTTSNPPPESSTPQTSRSTSSPLTESTTLLSTLPPAIEMTSTAPPSTPTAPT" +
                "TTSGGHTLSPPPSTTTSPPGTPTRGTTTGSSSAPTPSTVQTTTTSAWTPTPTPLSTPSII" +
                "RTTGLRPYPSSVLICCVLNDTYYAPGEEVYNGTYGDTCYFVNCSLSCTLEFYNWSCPSTP" +
                "SPTPTPSKSTPTPSKPSSTPSKPTPGTKPPECPDFDPPRQENETWWLCDCFMATCKYNNT" +
                "VEIVKVECEPPPMPTCSNGLQPVRVEDPDGCCWHWECDCYCTGWGDPHYVTFDGLYYSYQ" +
                "GNCTYVLVEEISPSVDNFGVYIDNYHCDPNDKVSCPRTLIVRHETQEVLIKTVHMMPMQV" +
                "QVQVNRQAVALPYKKYGLEVYQSGINYVVDIPELGVLVSYNGLSFSVRLPYHRFGNNTKG" +
                "QCGTCTNTTSDDCILPSGEIVSNCEAAADQWLVNDPSKPHCPHSSSTTKRPAVTVPGGGK" +
                "TTPHKDCTPSPLCQLIKDSLFAQCHALVPPQHYYDACVFDSCFMPGSSLECASLQAYAAL" +
                "CAQQNICLDWRNHTHGACLVECPSHREYQACGPAEEPTCKSSSSQQNNTVLVEGCFCPEG" +
                "TMNYAPGFDVCVKTCGCVGPDNVPREFGEHFEFDCKNCVCLEGGSGIICQPKRCSQKPVT" +
                "HCVEDGTYLATEVNPADTCCNITVCKCNTSLCKEKPSVCPLGFEVKSKMVPGRCCPFYWC" +
                "ESKGVCVHGNAEYQPGSPVYSSKCQDCVCTDKVDNNTLLNVIACTHVPCNTSCSPGFELM" +
                "EAPGECCKKCEQTHCIIKRPDNQHVILKPGDFKSDPKNNCTFFSCVKIHNQLISSVSNIT" +
                "CPNFDASICIPGSITFMPNGCCKTCTPRNETRVPCSTVPVTTEVSYAGCTKTVLMNHCSG" +
                "SCGTFVMYSAKAQALDHSCSCCKEEKTSQREVVLSCPNGGSLTHTYTHIESCQCQDTVCG" +
                "LPTGTSRRARRSPRHLGSG";

        Peptide peptide = Peptide.parse(seq);
        assertEquals(seq, peptide.toSymbolString());
    }

    @Test
    public void testParse8() throws Exception {

        Peptide peptide = Peptide.parse("CERVILAS(HPO3)");
        assertEquals("CERVILAS", peptide.toSymbolString());
        assertEquals(1, peptide.getModificationCount());
        assertEquals(Modification.parseModification("HPO3"), peptide.getModifications(EnumSet.of(ModAttachment.SIDE_CHAIN)).get(0));
    }

    @Test
    public void testParse9() throws Exception {

        Peptide peptide = Peptide.parse("CERVILAS(HPO3, O)");
        assertEquals("CERVILAS", peptide.toSymbolString());
        assertEquals(2, peptide.getModificationCount());
        assertEquals(Modification.parseModification("HPO3"), peptide.getModifications(EnumSet.of(ModAttachment.SIDE_CHAIN)).get(0));
        assertEquals(Modification.parseModification("O"), peptide.getModifications(EnumSet.of(ModAttachment.SIDE_CHAIN)).get(1));
    }

    @Test
    public void testParse10() throws Exception {

        Peptide peptide = Peptide.parse("AAAAAAGAGPEM(O)VR");
        assertEquals("AAAAAAGAGPEMVR", peptide.toSymbolString());
        assertEquals(1, peptide.getModificationCount());
        assertEquals(Modification.parseModification("O"), peptide.getModifications(EnumSet.of(ModAttachment.SIDE_CHAIN)).get(0));
    }

    @Test (expected = PeptideParseException.class)
    public void testPeptideMissingTermMods() throws Exception {

        Peptide.parse("()_UHRCUHHA(C4H12)U(C)EHE_()");
    }

    @Test (expected = PeptideParseException.class)
    public void testParseIllegalString() {

        Peptide.parse("#!/bin/bash");
    }

    @Test(expected = PeptideParseException.class)
    public void testParseIllegalString2() {

        Peptide.parse("A(CH3)(H2C2O)CPLEK");
    }

    @Test(expected = PeptideParseException.class)
    public void testParseIllegalString3() {

        Peptide.parse("(CH3)");
    }

    @Test(expected = PeptideParseException.class)
    public void testParseIllegalString4() {

        Peptide.parse("(CH3)_");
    }

    @Test(expected = PeptideParseException.class)
    public void testParseIllegalString5() {

        Peptide.parse("_(H2C2O)");
    }

    @Test(expected = PeptideParseException.class)
    public void testParseIllegalString6() {

        Peptide.parse("");
    }

    @Test(expected = PeptideParseException.class)
    public void testParseIllegalString7() {

        Peptide.parse("CER<<>>V");
    }

    @Test(expected = PeptideParseException.class)
    public void testParseIllegalString8() {

        Peptide.parse("C_((H2C2O)");
    }

    @Test(expected = PeptideParseException.class)
    public void testParseIllegalString9() {

        Peptide.parse("C_((H2C2O))");
    }

    @Test(expected = PeptideParseException.class)
    public void testParseIllegalString10() {

        Peptide.parse("C___C___(H2C2O)");
    }

    @Test(expected = PeptideParseException.class)
    public void testParseIllegalString11() {

        Peptide.parse("(");
    }

    @Test(expected = PeptideParseException.class)
    public void testParseIllegalString12() {

        Peptide.parse(")");
    }

    @Test(expected = PeptideParseException.class)
    public void testParseIllegalString13() {

        Peptide.parse("(_");
    }

    @Test(expected = PeptideParseException.class)
    public void testParseIllegalString14() {

        Peptide.parse("_)");
    }

    @Test(expected = PeptideParseException.class)
    public void testParseIllegalString15() {

        Peptide.parse("CER_)");
    }

    @Test(expected = PeptideParseException.class)
    public void testParseIllegalString16() {

        Peptide.parse("CER)");
    }

    @Test(expected = PeptideParseException.class)
    public void testParseIllegalString17() {

        Peptide.parse("PEPT((phospho)IDE");
    }

    @Test(expected = PeptideParseException.class)
    public void testParseIllegal18() {

        Peptide.parse("AAAD()LMAYCEAHAK");
    }

    @Test(expected = PeptideParseException.class)
    public void testParseIllegal19() {

        Peptide.parse("AAAD(O,  )LMAYCEAHAK");
    }

    @Test
    public void testGetModificationCount2() throws Exception {

        List<AminoAcid> residues = Arrays.asList(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.K);
        Multimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
        sideChainModMap.put(2, mock(Modification.class));
        sideChainModMap.put(3, mock(Modification.class));
        sideChainModMap.put(3, mock(Modification.class));

        Multimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, mock(Modification.class));
        termModMap.put(ModAttachment.N_TERM, mock(Modification.class));
        termModMap.put(ModAttachment.N_TERM, mock(Modification.class));
        termModMap.put(ModAttachment.C_TERM, mock(Modification.class));
        termModMap.put(ModAttachment.C_TERM, mock(Modification.class));
        termModMap.put(ModAttachment.C_TERM, mock(Modification.class));
        termModMap.put(ModAttachment.C_TERM, mock(Modification.class));
        termModMap.put(ModAttachment.C_TERM, mock(Modification.class));
        termModMap.put(ModAttachment.C_TERM, mock(Modification.class));
        termModMap.put(ModAttachment.C_TERM, mock(Modification.class));

        Peptide peptide = new Peptide(residues, sideChainModMap, termModMap);

        assertEquals(3, peptide.getModificationCount(ModAttachment.SIDE_CHAIN));
        assertEquals(3, peptide.getModificationCount(ModAttachment.N_TERM));
        assertEquals(7, peptide.getModificationCount(ModAttachment.C_TERM));
    }

    @Test
    public void testReplace() throws Exception {

        Peptide peptide = Peptide.parse("ISL");
        Assert.assertEquals(Peptide.parse("JSJ"), peptide.replace(EnumSet.of(AminoAcid.I, AminoAcid.L), AminoAcid.J));

        Modification mod = mockMod("mod", 45);
        peptide = new PeptideBuilder("AALAYIYEAVNK")
                .addModification(5, mod)
                .build();

        Assert.assertEquals(new PeptideBuilder("AAJAYJYEAVNK")
                .addModification(5, mod)
                .build(), peptide.replace(EnumSet.of(AminoAcid.I, AminoAcid.L), AminoAcid.J));
    }

    @Test
    public void testComposition() {

        Optional<Composition> comp = Peptide.parse("ALMAMATER").getComposition();

        Assert.assertTrue(comp.isPresent());
        Assert.assertEquals(Composition.parseComposition("C40H72N12O13S2"), comp.get());

        Peptide peptide = new PeptideBuilder()
                .parseAndAdd("ALMAMATER")
                .addModification(2, new Modification("Oxid", Composition.parseComposition("O")))
                .addModification(6,new Modification("Phospho",Composition.parseComposition("PO4")))
                .addModification(ModAttachment.N_TERM, new Modification("Acet", Composition.parseComposition("C2OH2")))
                .build();

        comp = peptide.getComposition();

        Assert.assertTrue(comp.isPresent());
        Assert.assertEquals(Composition.parseComposition("C42H74N12O19PS2"), comp.get());


        peptide = new PeptideBuilder()
                .parseAndAdd("ALMAMATER")
                .addModification(2, new Modification("Oxid", new NumericMass(15.998)))
                .addModification(6,new Modification("Phospho",Composition.parseComposition("PO4")))
                .addModification(ModAttachment.N_TERM, new Modification("Acet", Composition.parseComposition("C2OH2")))
                .build();

        comp = peptide.getComposition();

        Assert.assertFalse(comp.isPresent());
    }


}
