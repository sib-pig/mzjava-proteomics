/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.mol.modification;

import org.expasy.mzjava.core.mol.AtomicSymbol;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.core.mol.PeriodicTable;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class ModificationTest {

    @Test
    public void test() throws Exception {

        Modification mod = new Modification("phospho", new Composition(new Composition.Builder(AtomicSymbol.H).add(AtomicSymbol.P).add(AtomicSymbol.O, 3).build()));
        Assert.assertEquals("phospho", mod.getLabel());
        Assert.assertEquals(79.966330398, mod.getMolecularMass(), 0.0000000000001);
    }

    @Test
    public void testParseModification() throws Exception {

        Modification modification = Modification.parseModification("H2H[2]PO4");

        Composition composition = (Composition)modification.getMass();
        Assert.assertEquals(2, composition.getCount(PeriodicTable.H));
        Assert.assertEquals(1, composition.getCount(PeriodicTable.getInstance().getAtom(AtomicSymbol.H, 2)));
        Assert.assertEquals(1, composition.getCount(PeriodicTable.P));
        Assert.assertEquals(4, composition.getCount(PeriodicTable.O));
        Assert.assertEquals("H2H[2]PO4", modification.getLabel());
    }

    @Test
    public void testParseModificationWithLabel() throws Exception {

        Modification modification = Modification.parseModification("Phospho:H[2]PO3");

        Composition composition = (Composition)modification.getMass();
        Assert.assertEquals(0, composition.getCount(PeriodicTable.H));
        Assert.assertEquals(1, composition.getCount(PeriodicTable.getInstance().getAtom(AtomicSymbol.H, 2)));
        Assert.assertEquals(1, composition.getCount(PeriodicTable.P));
        Assert.assertEquals(3, composition.getCount(PeriodicTable.O));
        Assert.assertEquals("Phospho", modification.getLabel());
    }

    @Test
    public void testParseModificationWithLabel2() throws Exception {

        Modification modification = Modification.parseModification("H[2]PO3:H[2]PO3");

        Composition composition = (Composition)modification.getMass();
        Assert.assertEquals(0, composition.getCount(PeriodicTable.H));
        Assert.assertEquals(1, composition.getCount(PeriodicTable.getInstance().getAtom(AtomicSymbol.H, 2)));
        Assert.assertEquals(1, composition.getCount(PeriodicTable.P));
        Assert.assertEquals(3, composition.getCount(PeriodicTable.O));
        Assert.assertEquals("H[2]PO3", modification.getLabel());
    }

    @Test
    public void testParseModificationWithLabel3() throws Exception {

        Modification modification = Modification.parseModification("Phosphate loss:H[2]-3P-1O-4");

        Composition composition = (Composition)modification.getMass();
        Assert.assertEquals(0, composition.getCount(PeriodicTable.H));
        Assert.assertEquals(-3, composition.getCount(PeriodicTable.getInstance().getAtom(AtomicSymbol.H, 2)));
        Assert.assertEquals(-1, composition.getCount(PeriodicTable.P));
        Assert.assertEquals(-4, composition.getCount(PeriodicTable.O));
        Assert.assertEquals("Phosphate loss", modification.getLabel());
    }

    @Test
    public void testParseModificationWithLabel4() throws Exception {

        Modification modification = Modification.parseModification("H3PO4 loss:H-3P-1O-4");

        Composition composition = (Composition)modification.getMass();
        Assert.assertEquals(-3, composition.getCount(PeriodicTable.H));
        Assert.assertEquals(-1, composition.getCount(PeriodicTable.P));
        Assert.assertEquals(-4, composition.getCount(PeriodicTable.O));
        Assert.assertEquals("H3PO4 loss", modification.getLabel());
    }

    @Test
    public void testParseModificationWithLabel5() throws Exception {

        Modification modification = Modification.parseModification("Loss:H(3) P O(4):H-3P-1O-4");

        Composition composition = (Composition)modification.getMass();
        Assert.assertEquals(-3, composition.getCount(PeriodicTable.H));
        Assert.assertEquals(-1, composition.getCount(PeriodicTable.P));
        Assert.assertEquals(-4, composition.getCount(PeriodicTable.O));
        Assert.assertEquals("Loss:H(3) P O(4)", modification.getLabel());
    }

    @Test
    public void testParseIllegalModification1() throws Exception {

        Modification modification = Modification.parseModification("H3PO4:loss:H-3P-1O-4");

        Composition composition = (Composition)modification.getMass();
        Assert.assertEquals(-3, composition.getCount(PeriodicTable.H));
        Assert.assertEquals(-1, composition.getCount(PeriodicTable.P));
        Assert.assertEquals(-4, composition.getCount(PeriodicTable.O));
        Assert.assertEquals("H3PO4:loss", modification.getLabel());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParseIllegalModification2() throws Exception {

        Modification.parseModification("H[2]]3PO4");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParseIllegalModification3() throws Exception {

        Modification.parseModification("H3P-O4");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParseIllegalModification5() throws Exception {

        Modification.parseModification("H3P+O4");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParseIllegalModification6() throws Exception {

        Modification.parseModification("H3PO4;");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParseIllegalModification7() throws Exception {

        Modification.parseModification(";H3PO4");
    }

    @Test
    public void testParseRoundTrip() throws Exception {

        String string = "Phospho:HPO3";
        Modification modification = Modification.parseModification(string);

        Composition composition = (Composition)modification.getMass();
        Assert.assertEquals(1, composition.getCount(PeriodicTable.H));
        Assert.assertEquals(1, composition.getCount(PeriodicTable.P));
        Assert.assertEquals(3, composition.getCount(PeriodicTable.O));

        Assert.assertEquals(string, modification.toString());
    }

    @Test
    public void testChargedModification() throws Exception {

        Modification mod = Modification.parseModification("C-1H-4S-1(+)");

        Composition composition = (Composition)mod.getMass();
        Assert.assertEquals(-1, composition.getCount(PeriodicTable.C));
        Assert.assertEquals(-4, composition.getCount(PeriodicTable.H));
        Assert.assertEquals(-1, composition.getCount(PeriodicTable.S));
        Assert.assertEquals(1, composition.getCharge());
    }

    @Test
    public void testChargedModification2() throws Exception {

        Modification mod = Modification.parseModification("C-1H-4S-1(2+)");

        Composition composition = (Composition)mod.getMass();
        Assert.assertEquals(-1, composition.getCount(PeriodicTable.C));
        Assert.assertEquals(-4, composition.getCount(PeriodicTable.H));
        Assert.assertEquals(-1, composition.getCount(PeriodicTable.S));
        Assert.assertEquals(2, composition.getCharge());
    }
}