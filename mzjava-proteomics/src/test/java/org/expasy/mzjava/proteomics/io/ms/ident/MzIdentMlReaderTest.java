/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.io.ms.ident;

import com.google.common.base.Optional;
import org.expasy.mzjava.core.ms.spectrum.TimeUnit;
import org.expasy.mzjava.proteomics.io.ms.ident.mzidentml.v110.ModificationType;
import org.expasy.mzjava.proteomics.io.ms.ident.mzidentml.v110.PeptideType;
import org.expasy.mzjava.proteomics.io.ms.ident.mzidentml.v110.SequenceCollectionType;
import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.proteomics.mol.modification.unimod.UnimodManager;
import org.expasy.mzjava.proteomics.ms.ident.ModificationMatch;
import org.expasy.mzjava.proteomics.ms.ident.PeptideMatch;
import org.expasy.mzjava.proteomics.ms.ident.PeptideProteinMatch;
import org.expasy.mzjava.proteomics.ms.ident.SpectrumIdentifier;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.*;

import static org.mockito.Mockito.mock;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class MzIdentMlReaderTest {

    private final Map<SpectrumIdentifier, List<PeptideMatch>> searchResultMap = new HashMap<>();

    PSMReaderCallback insertIdResultCB = new PSMReaderCallback() {
        @Override
        public void resultRead(SpectrumIdentifier identifier, PeptideMatch searchResult) {

            List<PeptideMatch> results;
            if (searchResultMap.containsKey(identifier)) {
                results = searchResultMap.get(identifier);
            } else {
                results = new ArrayList<>();
                searchResultMap.put(identifier, results);
            }

            results.add(searchResult);
        }
    };

    @Test
    public void testParse() throws Exception {

        InputStream fr = new FileInputStream(new File(getClass().getResource("mz_ident_ml.mzid").getFile()));

        PSMReaderCallback callback = mock(PSMReaderCallback.class);

        MzIdentMlReader reader = new MzIdentMlReader();

        reader.parse(fr, callback);

        SpectrumIdentifier identifier1 = newIdentifier("348: Scan 881 (rt=13.2621) [\\\\wzw.tum.de\\ipag\\Data\\MS_Data\\PIPELINE\\Thermo_LTQ_ORBITRAP_XL\\raw\\290709_Pepmix4_HCD.RAW]",
                881, 795.7248, 1562.6445077846, 2, 347, "Pepmix4_HCD.RAW.-1_0.mgf");
        SpectrumIdentifier identifier2 = newIdentifier("518: Scan 1085 (rt=16.1531) [\\\\wzw.tum.de\\ipag\\Data\\MS_Data\\PIPELINE\\Thermo_LTQ_ORBITRAP_XL\\raw\\290709_Pepmix4_HCD.RAW]",
                1085, 969.1855, 1737.8237077846, 2, 517, "Pepmix4_HCD.RAW.-1_0.mgf");

        Modification oxidation = UnimodManager.getModification("Oxidation").get();
        Modification phospho = UnimodManager.getModification("Phospho").get();
        Mockito.verify(callback).resultRead(identifier1,
                newSearchResult(1, "TGMGSGSAGKEGGPFK", false,
                        4.384999999729189E-4, 110.12, 1.63907907206418E-10, 98.52,
                        new ExpectedModMatch(2, 15.994915, oxidation),
                        new ExpectedModMatch(4, 79.966331, phospho)
                )
        );
        Mockito.verify(callback).resultRead(identifier1,
                newSearchResult(2, "TGMGSGSAGKEGGPFK", false,
                        4.384999999729189E-4, 91.67, 1.14709636939896E-8, 1.41,
                        new ExpectedModMatch(2, 15.994915, oxidation),
                        new ExpectedModMatch(6, 79.966331, phospho)
                ));
        Mockito.verify(callback).resultRead(identifier1,
                newSearchResult(3, "TGMGSGSAGKEGGPFK",false,
                        4.384999999729189E-4, 78.49, 2.38561251922769E-7, 0.07,
                        new ExpectedModMatch(0, 79.966331, phospho),
                        new ExpectedModMatch(2, 15.994915, oxidation)
                )
        );
        Mockito.verify(callback).resultRead(identifier2,
                newSearchResult(1, "ETTTSPKKYYLAEK",false,
                        5.609999999478532E-4, 82.79, 1.44128730991053E-7, 48.88,
                        new ExpectedModMatch(2, 79.966331, phospho)
                )
        );
        Mockito.verify(callback).resultRead(identifier2,
                newSearchResult(2, "ETTTSPKKYYLAEK",false,
                        5.609999999478532E-4, 82.79, 1.44128730991053E-7, 48.88,
                        new ExpectedModMatch(1, 79.966331, phospho)
                )
        );
        Mockito.verify(callback).resultRead(identifier2,
                newSearchResult(3, "ETTTSPKKYYLAEK",false,
                        5.609999999478532E-4, 69.24, 3.26400310199501E-6, 2.16,
                        new ExpectedModMatch(3, 79.966331, phospho)
                )
        );
        Mockito.verify(callback).resultRead(identifier2,
                newSearchResult(4, "ETTTSPKKYYLAEK",false,
                        5.609999999478532E-4, 55.0, 8.66464078886136E-5, 0.08,
                        new ExpectedModMatch(4, 79.966331, phospho)
                )
        );
        Mockito.verify(callback).resultRead(identifier2,
                newSearchResult(5, "ETTTSPKKYYLAEK",true,
                        5.609999999478532E-4, 5.43, 7.84784763754403, 0.0,
                        new ExpectedModMatch(8, 79.966331, phospho)
                )
        );
        Mockito.verify(callback).resultRead(identifier2,
                newSearchResult(6, "NVTNNLKSLEAQAEK",true,
                        -0.003040000000055443, 1.92, 17.6096434544863,
                        new ExpectedModMatch(7, 79.966331, phospho)
                )
        );
        Mockito.verify(callback).resultRead(identifier2,
                newSearchResult(7, "NVTNNLKSLEAQAEK",true,
                        -0.003040000000055443, 1.92, 17.6096434544863,
                        new ExpectedModMatch(2, 79.966331, phospho)
                )
        );
        Mockito.verify(callback).resultRead(identifier2,
                newSearchResult(8, "IYVWKGKGATKAEK",true,
                        -9.344999999711945E-4, 1.27, 20.4526959804358,
                        new ExpectedModMatch(1, 79.966331, phospho),
                        new ExpectedModMatch(9, 79.966331, phospho)
                )
        );

    }

    @Test
    public void testGetPeptide() throws Exception {

        MzIdentMlReader reader = new MzIdentMlReader();

        ModificationType mod0 = new ModificationType();
        mod0.setMonoisotopicMassDelta(42.0);
        mod0.setLocation(0);

        ModificationType mod1 = new ModificationType();
        mod1.setMonoisotopicMassDelta(50.0);
        mod1.setLocation(1);

        ModificationType mod8 = new ModificationType();
        mod8.setMonoisotopicMassDelta(80.0);
        mod8.setLocation(8);

        ModificationType mod9 = new ModificationType();
        mod9.setMonoisotopicMassDelta(12.0);
        mod9.setLocation(9);

        PeptideType peptideType = new PeptideType();
        peptideType.setId("peptide_3_5");
        peptideType.setPeptideSequence("CERVILAS");
        List<ModificationType> modifications = peptideType.getModification();
        modifications.add(mod0);
        modifications.add(mod1);
        modifications.add(mod8);
        modifications.add(mod9);

        SequenceCollectionType sequenceCollection = new SequenceCollectionType();
        List<PeptideType> peptides = sequenceCollection.getPeptide();
        peptides.add(peptideType);


        PeptideMatch peptideMatch = reader.readPeptide("peptide_3_5", sequenceCollection);


        Assert.assertEquals(4, peptideMatch.getModificationCount());
        Assert.assertEquals(42.0, peptideMatch.getModifications(EnumSet.of(ModAttachment.N_TERM)).get(0).getMassShift(), 0.00001);
        Assert.assertEquals(50.0, peptideMatch.getModifications(0, EnumSet.allOf(ModAttachment.class)).get(0).getMassShift(), 0.00001);
        Assert.assertEquals(80.0, peptideMatch.getModifications(7, EnumSet.allOf(ModAttachment.class)).get(0).getMassShift(), 0.00001);
        Assert.assertEquals(12.0, peptideMatch.getModifications(EnumSet.of(ModAttachment.C_TERM)).get(0).getMassShift(), 0.00001);
    }

    @Test
    public void testOMSSA() throws Exception {

        InputStream fr = new FileInputStream(new File(getClass().getResource("OMSSA_chludwig_B1103_079.mzid").getFile()));

        MzIdentMlReader reader = new MzIdentMlReader();

        searchResultMap.clear();
        reader.parse(fr, insertIdResultCB);

        Assert.assertEquals(searchResultMap.size(), 286);

        SpectrumIdentifier sp1 = getSpectrumIdentifier("chludwig_B1103_079.01905.01905.3");
        Assert.assertEquals(541, (int) sp1.getIndex().get());
        Assert.assertEquals(3, (int) sp1.getAssumedCharge().get());
        Assert.assertEquals(2509.15117, sp1.getPrecursorNeutralMass().get(), 0.0001);

        List<PeptideMatch> results = searchResultMap.get(sp1);
        Assert.assertEquals(6, results.size());
        PeptideMatch result = results.get(0);
        Assert.assertEquals(Optional.of(1), result.getRank());
        Assert.assertEquals("IKSSTSVQSSATPPSNTSSNPDIK", result.toSymbolString());
        Assert.assertEquals(1, result.getModificationCount());
        Assert.assertEquals(79.966331, result.getModifications(2, EnumSet.allOf(ModAttachment.class)).get(0).getMassShift(), 0.00000001);
        Assert.assertEquals(3.66372467650576e-5, result.getScore("OMSSA:evalue"), 1.0e-10);
        Assert.assertEquals(2.33283965393554e-9, result.getScore("OMSSA:pvalue"), 1.0e-15);
        Assert.assertTrue(result.containsProtein("YFL021W"));
        Assert.assertEquals(PeptideProteinMatch.HitType.TARGET, result.getProteinMatches().get(0).getHitType());

    }

    @Test
    public void testACandSearchDb() throws Exception {

        InputStream fr = new FileInputStream(new File(getClass().getResource("OMSSA_chludwig_B1103_079.mzid").getFile()));

        MzIdentMlReader reader = new MzIdentMlReader();

        searchResultMap.clear();
        reader.parse(fr, insertIdResultCB);

        Assert.assertEquals(searchResultMap.size(), 286);

        SpectrumIdentifier sp1 = getSpectrumIdentifier("chludwig_B1103_079.01905.01905.3");
        Assert.assertEquals(541, (int)sp1.getIndex().get());
        Assert.assertEquals(3, (int)sp1.getAssumedCharge().get());
        Assert.assertEquals(2509.15117, sp1.getPrecursorNeutralMass().get(), 0.0001);

        List<PeptideMatch> results = searchResultMap.get(sp1);
        Assert.assertEquals(6, results.size());
        PeptideMatch result = results.get(0);
        Assert.assertEquals(1, result.getProteinMatches().size());
        Assert.assertEquals("YFL021W", result.getProteinMatches().get(0).getAccession());
        Assert.assertEquals("SearchDB_1", result.getProteinMatches().get(0).getSearchDatabase().get());
        Assert.assertEquals(PeptideProteinMatch.HitType.TARGET, result.getProteinMatches().get(0).getHitType());

    }


    @Test
    public void testACandSearchDbMultiProt() throws Exception {

        InputStream fr = new FileInputStream(new File(getClass().getResource("MSGF_chludwig_B1103_079.mzid").getFile()));

        MzIdentMlReader reader = new MzIdentMlReader();

        searchResultMap.clear();
        reader.parse(fr, insertIdResultCB);

        Assert.assertEquals(1415, searchResultMap.size());
        SpectrumIdentifier sp1 = getSpectrumIdentifier("chludwig_B1103_079.01660.01660.3");
        Assert.assertEquals(437, (int)sp1.getIndex().get());
        Assert.assertEquals(3, (int)sp1.getAssumedCharge().get());
        Assert.assertEquals(2756.2109597, sp1.getPrecursorNeutralMass().get(), 0.0001);

        List<PeptideMatch> results = searchResultMap.get(sp1);
        Assert.assertEquals(2, results.size());

        // check first sequence
        PeptideMatch pepMatch1 = results.get(0);
        Assert.assertEquals("VKQFANSNNNNNDSGNNNQGDYVTK", pepMatch1.toSymbolString());
        Assert.assertEquals(Optional.of(1), pepMatch1.getRank());

        // check second sequence
        PeptideMatch pepMatch2 = results.get(1);
        Assert.assertEquals("YLIPSHISKLTINNVNKSKSVNK", pepMatch2.toSymbolString());
        Assert.assertEquals(Optional.of(2), pepMatch2.getRank());

        // check modifs
        Assert.assertEquals(2, pepMatch2.getModificationCount());
        List<ModificationMatch> modifs = pepMatch2.getModifications(EnumSet.of(ModAttachment.SIDE_CHAIN));
        Assert.assertEquals(2, modifs.size());
        Assert.assertEquals("S : 79.96633092500001:[Phospho:HO3P]", modifs.get(0).toString());

        // check size of protein matches
        List<PeptideProteinMatch> ppm = pepMatch2.getProteinMatches();
        Assert.assertEquals(10, ppm.size());

        // check first and last entries
        Assert.assertEquals("YBL100W-B", ppm.get(0).getAccession());
        Assert.assertEquals(566, ppm.get(0).getStart());
        Assert.assertEquals("YOR343W-B", ppm.get(9).getAccession());
        Assert.assertEquals(566, ppm.get(0).getStart());
        Assert.assertEquals("SearchDB_1", ppm.get(9).getSearchDatabase().get());

    }


    @Test
    public void testHitType() throws Exception {

        InputStream fr = new FileInputStream(new File(getClass().getResource("MSGF_chludwig_B1103_079.mzid").getFile()));

        MzIdentMlReader reader = new MzIdentMlReader();

        searchResultMap.clear();
        reader.parse(fr, insertIdResultCB);

        // load and check data from Decoy hit
        SpectrumIdentifier sp1 = getSpectrumIdentifier("chludwig_B1103_079.01617.01617.3");
        List<PeptideMatch> results = searchResultMap.get(sp1);
        Assert.assertEquals(PeptideProteinMatch.HitType.DECOY, results.get(1).getProteinMatches().get(0).getHitType());

        // load and check data from Target hit
        SpectrumIdentifier sp2 = getSpectrumIdentifier("chludwig_B1103_079.01894.01894.3");
        List<PeptideMatch> results2 = searchResultMap.get(sp2);
        Assert.assertEquals(PeptideProteinMatch.HitType.TARGET, results2.get(0).getProteinMatches().get(0).getHitType());

    }

    @Test
    public void testPeptidePosition() throws Exception {

        InputStream fr = new FileInputStream(new File(getClass().getResource("OMSSA_chludwig_B1103_079.mzid").getFile()));

        MzIdentMlReader reader = new MzIdentMlReader();

        searchResultMap.clear();
        reader.parse(fr, insertIdResultCB);

        Assert.assertEquals(searchResultMap.size(), 286);

        SpectrumIdentifier sp1 = getSpectrumIdentifier("chludwig_B1103_079.01905.01905.3");
        Assert.assertEquals(541, (int)sp1.getIndex().get());
        Assert.assertEquals(3, (int)sp1.getAssumedCharge().get());
        Assert.assertEquals(2509.15117, sp1.getPrecursorNeutralMass().get(), 0.0001);

        List<PeptideMatch> results = searchResultMap.get(sp1);
        Assert.assertEquals(6, results.size());
        PeptideMatch result = results.get(0);
        Assert.assertEquals(Optional.of(1), result.getRank());
        Assert.assertEquals(286, result.getProteinMatches().get(0).getStart());
        Assert.assertEquals(309, result.getProteinMatches().get(0).getEnd());
        Assert.assertEquals("IKSSTSVQSSATPPSNTSSNPDIK", result.toSymbolString());
        Assert.assertEquals(1, result.getModificationCount());
        Assert.assertEquals(79.966331, result.getModifications(2, EnumSet.allOf(ModAttachment.class)).get(0).getMassShift(), 0.00000001);
        Assert.assertEquals(3.66372467650576e-5, result.getScore("OMSSA:evalue"), 1.0e-10);
        Assert.assertEquals(2.33283965393554e-9, result.getScore("OMSSA:pvalue"), 1.0e-15);
        Assert.assertTrue(result.containsProtein("YFL021W"));

    }

    @Test
    public void testPeptidePosition2() throws Exception {

        InputStream fr = new FileInputStream(new File(getClass().getResource("F001303.mzid").getFile()));

        MzIdentMlReader reader = new MzIdentMlReader();

        searchResultMap.clear();
        reader.parse(fr, insertIdResultCB);

        Assert.assertEquals(searchResultMap.size(), 8502);

        SpectrumIdentifier sp1 = getSpectrumIdentifier("20140811_REFERENCESAMPLE_RFamp_switch_1.9071.9071.2");
        Assert.assertEquals(5812, (int) sp1.getIndex().get());
        Assert.assertEquals(2, (int) sp1.getAssumedCharge().get());
        Assert.assertEquals(687.367467, sp1.getPrecursorNeutralMass().get(), 0.0001);

        List<PeptideMatch> results = searchResultMap.get(sp1);
        Assert.assertEquals(1, results.size());
        PeptideMatch result = results.get(0);
        Assert.assertEquals(3, result.getProteinMatches().size());

        // check positions and info of first protein
        PeptideProteinMatch ppm1 = result.getProteinMatches().get(0);
        Assert.assertEquals("APAF_MOUSE", ppm1.getAccession());
        Assert.assertEquals(769, ppm1.getStart());
        Assert.assertEquals(773, ppm1.getEnd());
        Assert.assertEquals("SDB_SwissProt_ID", ppm1.getSearchDatabase().get());

        // check positions and info of last protein
        PeptideProteinMatch ppm2 = result.getProteinMatches().get(2);
        Assert.assertEquals("GBB2_MOUSE", ppm2.getAccession());
        Assert.assertEquals(210, ppm2.getStart());
        Assert.assertEquals(214, ppm2.getEnd());
        Assert.assertEquals("SDB_SwissProt_ID", ppm2.getSearchDatabase().get());

    }

    @Test
    public void testPeptidePosition3() throws Exception {

        InputStream fr = new FileInputStream(new File(getClass().getResource("F001303.mzid").getFile()));

        MzIdentMlReader reader = new MzIdentMlReader();

        searchResultMap.clear();
        reader.parse(fr, insertIdResultCB);

        Assert.assertEquals(searchResultMap.size(), 8502);

        // check second spectrum
        SpectrumIdentifier sp2 = getSpectrumIdentifier("20140811_REFERENCESAMPLE_RFamp_switch_1.10716.10716.2");
        List<PeptideMatch> results2 = searchResultMap.get(sp2);

        Assert.assertEquals(1, results2.size());
        PeptideMatch result = results2.get(0);
        Assert.assertEquals(2, result.getProteinMatches().size());

        // check positions and info of first protein
        PeptideProteinMatch ppm1 = result.getProteinMatches().get(0);
        Assert.assertEquals("HNRPF_MOUSE", ppm1.getAccession());
        Assert.assertEquals(180, ppm1.getStart());
        Assert.assertEquals(185, ppm1.getEnd());
        Assert.assertEquals("SDB_SwissProt_ID", ppm1.getSearchDatabase().get());

        // check positions and info of second protein
        PeptideProteinMatch ppm2 = result.getProteinMatches().get(1);
        Assert.assertEquals("HNRPF_MOUSE", ppm2.getAccession());
        Assert.assertEquals(82, ppm2.getStart());
        Assert.assertEquals(87, ppm2.getEnd());
        Assert.assertEquals("SDB_SwissProt_ID", ppm2.getSearchDatabase().get());

    }


    @Test
    public void testTandem() throws Exception {

        InputStream fr = new FileInputStream(new File(getClass().getResource("Tandem_chludwig_B1103_079.mzid").getFile()));

        MzIdentMlReader reader = new MzIdentMlReader();

        searchResultMap.clear();
        reader.parse(fr, insertIdResultCB);

        Assert.assertEquals(196, searchResultMap.size());

        SpectrumIdentifier sp1 = getSpectrumIdentifier("chludwig_B1103_079.01905.01905.3");
        Assert.assertEquals(541, (int)sp1.getIndex().get());
        Assert.assertEquals(3, (int)sp1.getAssumedCharge().get());
        Assert.assertEquals(2512.1758646769, sp1.getPrecursorNeutralMass().get(), 0.0000001);


        List<PeptideMatch> results = searchResultMap.get(sp1);
        Assert.assertEquals(results.size(), 2);
        PeptideMatch result = results.get(0);
        Assert.assertEquals(Optional.of(1), result.getRank());
        Assert.assertEquals("IKSSTSVQSSATPPSNTSSNPDIK", result.toSymbolString());
        Assert.assertEquals(1, result.getModificationCount());
        Assert.assertEquals(79.966, result.getModifications(2, EnumSet.allOf(ModAttachment.class)).get(0).getMassShift(), 0.00000001);
        Assert.assertEquals(0.024, result.getScore("X\\!Tandem:expect"), 0.0001);
        Assert.assertEquals(91.3, result.getScore("X\\!Tandem:hyperscore"), 0.00001);
    }

    @Test
    public void testMSGFPlus() throws Exception {

        InputStream fr = new FileInputStream(new File(getClass().getResource("MSGF_chludwig_B1103_079.mzid").getFile()));

        MzIdentMlReader reader = new MzIdentMlReader();

        searchResultMap.clear();
        reader.parse(fr, insertIdResultCB);

        Assert.assertEquals(1415, searchResultMap.size());

        SpectrumIdentifier sp1 = getSpectrumIdentifier("chludwig_B1103_079.01905.01905.3");
        Assert.assertEquals(541, (int)sp1.getIndex().get());
        Assert.assertEquals(3, (int)sp1.getAssumedCharge().get());
        Assert.assertEquals(2512.1758645479936, sp1.getPrecursorNeutralMass().get(), 0.000000000001);

        List<PeptideMatch> results = searchResultMap.get(sp1);
        Assert.assertEquals(2, results.size());
        PeptideMatch result = results.get(0);
        Assert.assertEquals(Optional.of(1), result.getRank());
        Assert.assertEquals("IKSSTSVQSSATPPSNTSSNPDIK", result.toSymbolString());
        Assert.assertEquals(1, result.getModificationCount());
        Assert.assertEquals(79.96633092500001, result.getModifications(3, EnumSet.allOf(ModAttachment.class)).get(0).getMassShift(), 0.00000001);
        Assert.assertEquals((double) 53, result.getScore("MS-GF:RawScore"), 0.0001);
        Assert.assertEquals((double) 93, result.getScore("MS-GF:DeNovoScore"), 0.00001);
        Assert.assertEquals(1.2524725E-11, result.getScore("MS-GF:SpecEValue"), 1.0e-15);
        Assert.assertEquals(6.925269E-5, result.getScore("MS-GF:EValue"), 1.0e-10);
        Assert.assertEquals(0.0, result.getScore("MS-GF:QValue"), 0.00001);
        Assert.assertEquals(0.0, result.getScore("MS-GF:PepQValue"), 0.00001);
    }

    private PeptideMatch newSearchResult(int rank, String seq, boolean isRejected, double massDiff, double ionscore, double expectationValue, ExpectedModMatch... modMatches) {

        PeptideMatch matchBuilder = new PeptideMatch(seq);
        matchBuilder.setRank(Optional.fromNullable(rank));
        matchBuilder.setMassDiff(Optional.fromNullable(massDiff));
        matchBuilder.addScore("Mascot:expectation value", expectationValue);
        matchBuilder.addScore("Mascot:score", ionscore);
        matchBuilder.setRejected(Optional.fromNullable(isRejected));

        for (ExpectedModMatch expectedModMatch : modMatches) {

            matchBuilder.addModificationMatch(expectedModMatch.getIndex(), expectedModMatch.getModMatch());
        }

        return matchBuilder;
    }

    private PeptideMatch newSearchResult(int rank, String seq, boolean isRejected, double massDiff, double ionscore, double expectationValue, double mdScore, ExpectedModMatch... expectedModMatches) {

        PeptideMatch searchResult = newSearchResult(rank, seq, isRejected, massDiff, ionscore, expectationValue, expectedModMatches);
        searchResult.addScore("Mascot:delta score", mdScore);

        return searchResult;
    }

    private SpectrumIdentifier newIdentifier(String spectrum, int scanNumber, double retentionTimeSeconds, double precursorNeutralMass, int assumedCharge, int index, String spectrumFile) {

        SpectrumIdentifier identifier = new SpectrumIdentifier(spectrum);
        identifier.addScanNumber(scanNumber);
        identifier.addRetentionTime(retentionTimeSeconds, TimeUnit.SECOND);
        identifier.setPrecursorNeutralMass(precursorNeutralMass);
        identifier.setAssumedCharge(assumedCharge);
        identifier.setIndex(index);
        identifier.setSpectrumFile(spectrumFile);

        return identifier;
    }

    SpectrumIdentifier getSpectrumIdentifier(String name) {

        SpectrumIdentifier matchSp = null;
        Set<SpectrumIdentifier> keys = searchResultMap.keySet();
        for (SpectrumIdentifier sp : keys) {
            if (sp.getSpectrum().equals(name)) {
                matchSp = sp;
            }
        }
        return matchSp;
    }

    private class ExpectedModMatch {

        private final int index;
        private final double mass;
        private final Modification mod;

        private ExpectedModMatch(int index, double mass, Modification mod) {

            this.index = index;
            this.mass = mass;
            this.mod = mod;
        }

        public int getIndex() {

            return index;
        }

        public ModificationMatch getModMatch() {

            ModificationMatch modMatch = new ModificationMatch(mass, AminoAcid.S, index, ModAttachment.SIDE_CHAIN);
            modMatch.addPotentialModification(mod);

            return modMatch;
        }
    }
}
