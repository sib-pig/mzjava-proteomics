package org.expasy.mzjava.proteomics.ms.fragment;

import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.peaklist.DoubleConstantPeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.ms.spectrum.PepFragAnnotation;
import org.junit.Assert;
import org.junit.Test;

import java.util.EnumSet;

// # $RECIPE$ $NUMBER$ - Annotating a PeakList #
//
// ## $PROBLEM$ ##
// You want to add annotations to *PeakList*
//
// ## $SOLUTION$ ##
//
// *FragmentAnnotator* is a facade that highly simplifies and hides the complexity of annotating a <code>PeakList</code>
// given a <code>peptide</code>.
// Annotating a peak list requires a theoretical spectra to be generated, the peaks of the theoretical and query spectra
// to be aligned and reporting annotations back to the <code>PeakList</code>.
public class AnnotatingSpectrumRecipe {

// Here is a java snippet to annotate a *PeakList* from a *Peptide*:
    @Test
    public void annotate() {

        //<SNIP>
        // the fragmenter is responsible for generating a theoretical spectrum from the peptide
        PeptideFragmenter fragmenter =
                new PeptideFragmenter(EnumSet.of(IonType.b, IonType.y), PeakList.Precision.DOUBLE);

        // the annotator needs to delegate to the fragmenter the fragmentation process and to
        // the internal aligner the tolerance for aligning peaks
        PeptideFragmentAnnotator annotator = new PeptideFragmentAnnotator(fragmenter, new AbsoluteTolerance(0.1));

        // the spectrum to annotate
        PeakList<PepFragAnnotation> spectrum = new DoubleConstantPeakList<>(1);

        double[] mzs = new double[] {234.1454, 321.1773, 322.1428, 330.1658, 353.1497, 359.1742, 365.1893, 365.219, 378.5135,
                402.6949, 402.718, 434.2426, 434.27, 434.2711, 439.2058, 456.2456, 471.2636, 491.2805, 508.2191, 553.2644,
                559.2601, 559.2625, 560.2307, 560.2513, 560.2559, 567.301, 575.7813, 576.2722, 576.3156, 592.3148, 592.3464,
                643.2743, 661.292, 674.309, 689.3341, 690.3297, 700.2772, 700.3068, 700.3408, 707.3326, 707.3625, 717.3297,
                718.2906, 718.3193, 735.3185, 735.3597, 780.1489, 786.3971, 804.3954, 804.4386};

        for (double mz : mzs) {

            spectrum.add(mz, 1);
        }

        // the peptide as a source for theoretical fragments with annotations
        Peptide peptide = Peptide.parse("QVHPDTGISSK");

        // start process: fragmenting peptide, aligning to the spectrum and reporting matched ions back to the spectrum
        annotator.annotate(spectrum, peptide, 3);
        //</SNIP>

        Assert.assertTrue(spectrum.hasAnnotations());
        Assert.assertEquals(11, spectrum.getAnnotationIndexes().length);
        Assert.assertArrayEquals(new int[] {0,1,6,10,12,16,17,29,40,45,48}, spectrum.getAnnotationIndexes());
    }

// ## $DISCUSSION$ ##

// ## $RELATED$ ##
// See $CreatingPeptideSpectrumRecipe$ for more explanations on fragmentation.
}