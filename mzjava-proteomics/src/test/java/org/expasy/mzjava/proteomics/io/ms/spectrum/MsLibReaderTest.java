package org.expasy.mzjava.proteomics.io.ms.spectrum;

import org.expasy.mzjava.proteomics.io.ms.spectrum.sptxt.LibrarySpectrumBuilder;
import org.junit.Test;
import org.mockito.Mockito;

public class MsLibReaderTest {

    @Test
    public void testHandlePeakLine() throws Exception {

        MsLibReader reader = Mockito.mock(MsLibReader.class, Mockito.CALLS_REAL_METHODS);
        LibrarySpectrumBuilder builder = Mockito.mock(LibrarySpectrumBuilder.class);

        reader.handlePeakLine(builder, "70.1\t88\t\"IRC/0.00,IPA/0.03 13/20 0.8\"");
        Mockito.verify(builder).addPeak(70.1, 88, "\"IRC/0.00,IPA/0.03 13/20 0.8\"");

        Mockito.reset(builder);
        reader.handlePeakLine(builder, "70.1    88      \"IRC/0.00,IPA/0.03 13/20 0.8\"");
        Mockito.verify(builder).addPeak(70.1, 88, "\"IRC/0.00,IPA/0.03 13/20 0.8\"");
    }

    @Test(expected = IllegalStateException.class)
    public void testHandleInvalidPeakLine() throws Exception {

        MsLibReader reader = Mockito.mock(MsLibReader.class, Mockito.CALLS_REAL_METHODS);
        LibrarySpectrumBuilder builder = Mockito.mock(LibrarySpectrumBuilder.class);

        reader.handlePeakLine(builder, "70.1");
    }

    @Test(expected = IllegalStateException.class)
    public void testHandleInvalidPeakLine2() throws Exception {

        MsLibReader reader = Mockito.mock(MsLibReader.class, Mockito.CALLS_REAL_METHODS);
        LibrarySpectrumBuilder builder = Mockito.mock(LibrarySpectrumBuilder.class);

        reader.handlePeakLine(builder, "aaa.bb\tbb.cc\t\"IRC/0.00,IPA/0.03 13/20 0.8\"");
    }

    @Test(expected = IllegalStateException.class)
    public void testHandleInvalidPeakLine3() throws Exception {

        MsLibReader reader = Mockito.mock(MsLibReader.class, Mockito.CALLS_REAL_METHODS);
        LibrarySpectrumBuilder builder = Mockito.mock(LibrarySpectrumBuilder.class);

        reader.handlePeakLine(builder, "120.5\tbb.cc\t\"IRC/0.00,IPA/0.03 13/20 0.8\"");
    }

    @Test(expected = IllegalStateException.class)
    public void testHandleInvalidPeakLine4() throws Exception {

        MsLibReader reader = Mockito.mock(MsLibReader.class, Mockito.CALLS_REAL_METHODS);
        LibrarySpectrumBuilder builder = Mockito.mock(LibrarySpectrumBuilder.class);

        reader.handlePeakLine(builder, "120.5\t87.9");
    }
}