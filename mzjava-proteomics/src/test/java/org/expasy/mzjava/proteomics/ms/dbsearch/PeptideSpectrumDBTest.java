package org.expasy.mzjava.proteomics.ms.dbsearch;

import com.google.common.base.Optional;
import com.google.common.collect.Sets;
import org.expasy.mzjava.core.io.IterativeReader;
import org.expasy.mzjava.core.io.IterativeReaders;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.mol.Protein;
import org.expasy.mzjava.proteomics.mol.digest.Protease;
import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.Tolerance;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessorChain;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.proteomics.ms.spectrum.PeptideSpectrum;
import org.expasy.mzjava.proteomics.ms.spectrum.PepFragAnnotation;
import org.expasy.mzjava.proteomics.ms.fragment.PeptideFragmenter;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.*;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.*;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class PeptideSpectrumDBTest {

    @Test
    public void test() throws Exception {

        PeptideSpectrumCache cache = Mockito.mock(PeptideSpectrumCache.class);
        when(cache.get(Mockito.any(Peptide.class), Mockito.anyInt())).thenReturn(Optional.<PeptideSpectrum>absent());

        final AbsoluteTolerance tolerance = new AbsoluteTolerance(3);

        DigestDB digestDB = Mockito.mock(DigestDB.class);
        final Digest peptide1 = new Digest(Peptide.parse("VISSIEQK"), "ID1");
        final Digest peptide2 = new Digest(Peptide.parse("NLLSVAYK"), "ID2");
        when(digestDB.getPeptides(anyDouble(), any(Tolerance.class))).thenReturn(Sets.newHashSet(
                peptide1,
                peptide2
        ));

        PeptideSpectrumDB peptideSpectrumDB = PeptideSpectrumDB.newBuilder()
                .setPrecursorTolerance(tolerance)
                .setDigestDB(digestDB)
                .cachePeptideSpectrumWith(cache)
                .generateSpectraWithIons(IonType.b, IonType.y)
                .build();

        List<PeptideSpectrum> peptides = peptideSpectrumDB.getSpectra(Peak.noIntensity(905.0, 1));

        Collections.sort(peptides, new Comparator<PeptideSpectrum>() {

            @Override
            public int compare(PeptideSpectrum ps1, PeptideSpectrum ps2) {

                return Double.compare(ps1.getPeptide().getMolecularMass(), ps2.getPeptide().getMolecularMass());
            }
        });

        Iterator<PeptideSpectrum> iterator = peptides.iterator();

        PeptideSpectrum ps = iterator.next();

        Assert.assertEquals("VISSIEQK", ps.getPeptide().toString());
        Assert.assertEquals(1, ps.getPrecursor().getCharge());
        Assert.assertEquals(902.507, ps.getPeptide().getMolecularMass(), 0.001);

        ps = iterator.next();

        Assert.assertEquals("NLLSVAYK", ps.getPeptide().toString());
        Assert.assertEquals(1, ps.getPrecursor().getCharge());
        Assert.assertEquals(906.517, ps.getPeptide().getMolecularMass(), 0.001);
    }

    @Test
    public void testWithDigest() throws Exception {

        PeptideSpectrumCache cache = Mockito.mock(PeptideSpectrumCache.class);
        when(cache.get(Mockito.any(Peptide.class), Mockito.anyInt())).thenReturn(Optional.<PeptideSpectrum>absent());

        final AbsoluteTolerance tolerance = new AbsoluteTolerance(3);

        IterativeReader<Protein> proteinReader = IterativeReaders.fromCollection((Arrays.asList(
                new Protein("ID1", "VISSIEQK"),
                new Protein("ID2", "NLLSVAYK"),
                new Protein("ID3", "NLLIIIIIIIIIIIIIIIIISVAYK"),
                new Protein("ID4", "NL")
        )));

        PeptideSpectrumDB peptideSpectrumDB = PeptideSpectrumDB.newBuilder()
                .setPrecursorTolerance(tolerance)
                .setProteinSource(proteinReader)
                .digestWith(Protease.TRYPSIN)
                .onAmbiguousAminoAcid(DigestDB.AmbiguousAminoAcidAction.THROW_EXCEPTION)
                .retainPeptidesOfLength(2, 9)
                .setMissedCleavagesTo(0)
                .cachePeptideSpectrumWith(cache)
                .processGeneratedSpectraWith(new PeakProcessorChain<PepFragAnnotation>())
                .generateSpectraWithIons(IonType.b, IonType.y)
                .build();

        List<PeptideSpectrum> peptides = peptideSpectrumDB.getSpectra(Peak.noIntensity(905.0, 1));

        Collections.sort(peptides, new Comparator<PeptideSpectrum>() {

            @Override
            public int compare(PeptideSpectrum ps1, PeptideSpectrum ps2) {

                return Double.compare(ps1.getPeptide().getMolecularMass(), ps2.getPeptide().getMolecularMass());
            }
        });

        Iterator<PeptideSpectrum> iterator = peptides.iterator();

        PeptideSpectrum ps = iterator.next();

        Assert.assertEquals("VISSIEQK", ps.getPeptide().toString());
        Assert.assertEquals(1, ps.getPrecursor().getCharge());
        Assert.assertEquals(902.507, ps.getPeptide().getMolecularMass(), 0.001);

        ps = iterator.next();

        Assert.assertEquals("NLLSVAYK", ps.getPeptide().toString());
        Assert.assertEquals(1, ps.getPrecursor().getCharge());
        Assert.assertEquals(906.517, ps.getPeptide().getMolecularMass(), 0.001);
    }

    @Test
    public void testAddToCache() throws Exception {

        Tolerance tolerance = mock(Tolerance.class);
        PeptideSpectrum peptideSpectrum = mock(PeptideSpectrum.class);
        PeptideFragmenter fragmenter = mock(PeptideFragmenter.class);
        when(fragmenter.fragment(any(Peptide.class), anyInt())).thenReturn(peptideSpectrum);

        Peptide peptide = mock(Peptide.class);
        Digest digest = new Digest(peptide);
        DigestDB digestDb = mock(DigestDB.class);
        when(digestDb.getPeptides(anyDouble(), any(Tolerance.class))).thenReturn(Collections.singleton(digest));

        PeptideSpectrumCache cache = mock(PeptideSpectrumCache.class);
        when(cache.get(any(Peptide.class), anyInt())).thenReturn(Optional.<PeptideSpectrum>absent());

        PeptideSpectrumDB peptideSpectrumDB = PeptideSpectrumDB.newBuilder()
                .setPrecursorTolerance(tolerance)
                .setDigestDB(digestDb)
                .cachePeptideSpectrumWith(cache)
                .generateSpectraWith(fragmenter)
                .build();

        List<PeptideSpectrum> spectra = peptideSpectrumDB.getSpectra(new Peak(154.0, 16, 1));
        Assert.assertEquals(1, spectra.size());

        verify(cache).get(peptide, 1);
        verify(cache).put(peptideSpectrum);
        verifyNoMoreInteractions(cache);
    }

    @Test
    public void testGetFromCache() throws Exception {

        Tolerance tolerance = mock(Tolerance.class);
        PeptideSpectrum peptideSpectrum = mock(PeptideSpectrum.class);
        PeptideFragmenter fragmenter = mock(PeptideFragmenter.class);
        when(fragmenter.fragment(any(Peptide.class), anyInt())).thenReturn(peptideSpectrum);

        Peptide peptide = mock(Peptide.class);
        Digest digest = new Digest(peptide);
        DigestDB digestDb = mock(DigestDB.class);
        when(digestDb.getPeptides(anyDouble(), any(Tolerance.class))).thenReturn(Collections.singleton(digest));

        PeptideSpectrumCache cache = mock(PeptideSpectrumCache.class);
        when(cache.get(any(Peptide.class), anyInt())).thenReturn(Optional.of(peptideSpectrum));

        PeptideSpectrumDB peptideSpectrumDB = new PeptideSpectrumDB(tolerance, fragmenter, digestDb, cache, new PeakProcessorChain<PepFragAnnotation>());

        List<PeptideSpectrum> spectra = peptideSpectrumDB.getSpectra(new Peak(154.0, 16, 1));
        Assert.assertEquals(1, spectra.size());

        verify(cache).get(peptide, 1);
        verifyNoMoreInteractions(cache);
    }

    @Test
    public void testSetPeakProcessorChain() throws Exception {

        Tolerance tolerance = mock(Tolerance.class);
        PeptideSpectrum peptideSpectrum = mock(PeptideSpectrum.class);
        PeptideFragmenter fragmenter = mock(PeptideFragmenter.class);
        when(fragmenter.fragment(any(Peptide.class), anyInt())).thenReturn(peptideSpectrum);

        Peptide peptide = mock(Peptide.class);
        Digest digest = new Digest(peptide);
        DigestDB digestDb = mock(DigestDB.class);
        when(digestDb.getPeptides(anyDouble(), any(Tolerance.class))).thenReturn(Collections.singleton(digest));

        PeptideSpectrumCache cache = mock(PeptideSpectrumCache.class);
        when(cache.get(any(Peptide.class), anyInt())).thenReturn(Optional.<PeptideSpectrum>absent());

        //Non empty processor chain
        @SuppressWarnings("unchecked")
        PeakProcessorChain<PepFragAnnotation> processorChain = mock(PeakProcessorChain.class);
        when(processorChain.isEmpty()).thenReturn(false);

        PeptideSpectrumDB peptideSpectrumDB = new PeptideSpectrumDB(tolerance, fragmenter, digestDb, cache, processorChain);

        List<PeptideSpectrum> spectra = peptideSpectrumDB.getSpectra(new Peak(154.0, 16, 1));
        Assert.assertEquals(1, spectra.size());

        verify(peptideSpectrum).addProteinAccessionNumbers(Collections.<String>emptySet());
        verify(peptideSpectrum).apply(processorChain);
        verify(peptideSpectrum).trimToSize();
        verifyNoMoreInteractions(peptideSpectrum);
    }
}
