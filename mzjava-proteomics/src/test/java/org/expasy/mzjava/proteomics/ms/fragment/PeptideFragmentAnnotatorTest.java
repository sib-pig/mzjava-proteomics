package org.expasy.mzjava.proteomics.ms.fragment;

import org.expasy.mzjava.core.ms.peaklist.DoublePeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.mol.PeptideFragment;
import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.spectrum.*;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.proteomics.ms.spectrum.PepFragAnnotation;
import org.expasy.mzjava.proteomics.ms.spectrum.PeptideSpectrum;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.text.ParseException;
import java.util.Arrays;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author fnikitin
 * Date: 6/6/13
 */
public class PeptideFragmentAnnotatorTest {

    @Test
    public void testAllPeaksMatchFragmentAnnotation() throws ParseException {

        PeptideFragmenter fragmenter = mock(PeptideFragmenter.class);

        when(fragmenter.fragment(Mockito.any(Peptide.class), anyInt(), Mockito.any(int[].class))).thenReturn(newPeptideSpectrum());

        PeptideFragmentAnnotator annotator = new PeptideFragmentAnnotator(fragmenter, new AbsoluteTolerance(0.1));

        PeakList<PepFragAnnotation> peaklist = newMsnSpectrum();
        PeakList<PepFragAnnotation> annotatedSpectrum = annotator.annotate(peaklist, Peptide.parse("CERVILAS"), 1);

        Assert.assertEquals(expectedAnnotatedMsnSpectrum(), annotatedSpectrum);
    }

    @Test
    public void testAllPeaksMatchFragmentAnnotation2() throws ParseException {

        PeptideFragmenter fragmenter = mock(PeptideFragmenter.class);

        when(fragmenter.fragment(Mockito.any(Peptide.class), anyInt(), Mockito.any(int[].class))).thenReturn(newPeptideSpectrum());

        PeptideFragmentAnnotator annotator = new PeptideFragmentAnnotator(fragmenter, new AbsoluteTolerance(0.1));

        PeakList<PepFragAnnotation> peaklist = newMsnSpectrum();
        PeakList<PepFragAnnotation> annotatedSpectrum = annotator.annotate(peaklist, Peptide.parse("CERVILAS"), 1);

        Assert.assertEquals(expectedAnnotatedMsnSpectrum(), annotatedSpectrum);
    }

    private static PeptideSpectrum newPeptideSpectrum() {

        PeptideSpectrum spectrum = new PeptideSpectrum(mock(Peptide.class), 2, PeakList.Precision.DOUBLE_CONSTANT);

        double[] mzs = new double[] {104.01646059469999, 106.04986920269998, 177.08698298969998, 233.0590536897,
                290.17104696870007, 389.16016471570003, 403.2551109477001, 488.22857863070004, 502.3235248627,
                601.3126426097, 658.4246358887, 714.3967065887, 785.4338203757, 787.4672289836999};

        String[] annots = new String[] {"b1", "y1", "y2", "b2", "y3", "b3", "y4", "b4", "y5", "b5", "y6", "b6", "b7", "y7"};

        for (int i=0 ;i< mzs.length ;i++) {

            spectrum.add(mzs[i], 1, Arrays.asList(newAnnot(annots[i])));
        }

        return spectrum;
    }

    public static <A extends PeakAnnotation> PeakList<A> newMsnSpectrum() {

        PeakList<A> spectrum = new DoublePeakList<>();

        double[] mzs = new double[] {104.01646059469999, 106.04986920269998, 177.08698298969998, 233.0590536897,
                290.17104696870007, 389.16016471570003, 403.2551109477001, 488.22857863070004, 502.3235248627,
                601.3126426097, 658.4246358887, 714.3967065887, 785.4338203757, 787.4672289836999};

        for (double mz : mzs) {

            spectrum.add(mz, mz);
        }

        return spectrum;
    }

    private static PeakList<PepFragAnnotation> expectedAnnotatedMsnSpectrum() {

        PeakList<PepFragAnnotation> spectrum = new DoublePeakList<>();

        double[] mzs = new double[] {104.01646059469999, 106.04986920269998, 177.08698298969998, 233.0590536897,
                290.17104696870007, 389.16016471570003, 403.2551109477001, 488.22857863070004, 502.3235248627,
                601.3126426097, 658.4246358887, 714.3967065887, 785.4338203757, 787.4672289836999};

        String[] annots = new String[] {"b1", "y1", "y2", "b2", "y3", "b3", "y4", "b4", "y5", "b5", "y6", "b6", "b7", "y7"};

        for (int i=0 ;i< mzs.length ;i++) {

            spectrum.add(mzs[i], mzs[i], Arrays.asList(newAnnot(annots[i])));
        }

        return spectrum;
    }

    private static PepFragAnnotation newAnnot(String annot) {

        IonType type = IonType.valueOf(String.valueOf(annot.charAt(0)));

        PeptideFragment fragment = PeptideFragment.parse("CERVILASPEPTIDE".substring(0, Integer.parseInt(String.valueOf(annot.charAt(1)))), type.getFragmentType());

        return new PepFragAnnotation(type, 1, fragment);
    }

}
