/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.io.ms.spectrum.sptxt;

import com.google.common.base.Optional;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.ms.spectrum.PepFragAnnotation;
import org.expasy.mzjava.proteomics.ms.spectrum.PepLibPeakAnnotation;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class SpectraStAnnotationResolverTest {

    @Test
    public void testResolveAnnotations() throws Exception {

        SpectraStAnnotationResolver annotationResolver = new SpectraStAnnotationResolver();

        Peptide peptide = Peptide.parse("Q(Gln->pyro-Glu)KINELNDYLDGK");
        List<PepLibPeakAnnotation> annotations = annotationResolver.resolveAnnotations("b3i/0.00,b6^2/-0.90\t2/1 3.6", peptide);
        Assert.assertEquals(new PepLibPeakAnnotation(2, 0, 0, Optional.of(new PepFragAnnotation.Builder(IonType.b, 1, Peptide.parse("Q(Gln->pyro-Glu)KI")).addC13(1).build())), annotations.get(0));
        Assert.assertEquals(new PepLibPeakAnnotation(2, 0, 0, Optional.of(new PepFragAnnotation(IonType.b, 2, Peptide.parse("Q(Gln->pyro-Glu)KINEL")))), annotations.get(1));
    }

    @Test
    public void testResolveAnnotations2() throws Exception {

        SpectraStAnnotationResolver annotationResolver = new SpectraStAnnotationResolver();

        Peptide peptide = Peptide.parse("Q(Gln->pyro-Glu)KINELNDYLDGK");
        List<PepLibPeakAnnotation> annotations = annotationResolver.resolveAnnotations("?\t2/1 1.9", peptide);

        Assert.assertEquals(1, annotations.size());
        Assert.assertEquals(new PepLibPeakAnnotation(2, 0, 0), annotations.get(0));
    }

    @Test
    public void testResolveAnnotations3() throws Exception {

        SpectraStAnnotationResolver annotationResolver = new SpectraStAnnotationResolver();

        Peptide peptide = Peptide.parse("Q(Gln->pyro-Glu)KINELNDYLDGK");
        List<PepLibPeakAnnotation> annotations = annotationResolver.resolveAnnotations("b4^2i/0.20\t2/2 0.0632|0.22", peptide);
        Assert.assertEquals(new PepLibPeakAnnotation(2, 0, 0, Optional.of(new PepFragAnnotation.Builder(IonType.b, 2, peptide.createFragment(IonType.b, 4)).addC13(1).build())), annotations.get(0));
    }

    @Test
    public void testResolveAnnotation4() throws Exception {

        SpectraStAnnotationResolver annotationResolver = new SpectraStAnnotationResolver();

        Peptide peptide = Peptide.parse("Q(Gln->pyro-Glu)KINELNDYLDGK");
        List<PepLibPeakAnnotation> annotations = annotationResolver.resolveAnnotations("b5i+/-0.01\t2/2 0.0309|0.35", peptide);
        Assert.assertEquals(new PepLibPeakAnnotation(2, 0, 0), annotations.get(0));
    }

    @Test
    public void testResolveAnnotation5() throws Exception {

        SpectraStAnnotationResolver annotationResolver = new SpectraStAnnotationResolver();

        Peptide peptide = Peptide.parse("Q(Gln->pyro-Glu)KINELNDYLDGK");
        List<PepLibPeakAnnotation> annotations = annotationResolver.resolveAnnotations("?", peptide);
        Assert.assertEquals(0, annotations.size());
    }
}
