package org.expasy.mzjava.proteomics.io.ms.ident;

import com.google.common.base.Optional;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author fnikitin
 * Date: 5/6/13
 */
public class ProteinPilotModificationResolverTest {

    @Test
    public void testResolveArgAdd() throws Exception {

        ProteinPilotModificationResolver resolver = new ProteinPilotModificationResolver();

        Optional<Modification> mod = resolver.resolve("Arg-add");

        Assert.assertTrue(mod.isPresent());
        Assert.assertEquals(156.101111, mod.get().getMolecularMass(), 0.000001);
    }

    @Test
    public void testResolveArgLoss() throws Exception {

        ProteinPilotModificationResolver resolver = new ProteinPilotModificationResolver();

        Optional<Modification> mod = resolver.resolve("Arg-loss");

        Assert.assertTrue(mod.isPresent());
        Assert.assertEquals(-156.101111, mod.get().getMolecularMass(), 0.000001);
    }

    @Test
    public void testResolveNoMethylThio() throws Exception {

        ProteinPilotModificationResolver resolver = new ProteinPilotModificationResolver();

        Optional<Modification> mod = resolver.resolve("No Methylthio");

        Assert.assertTrue(mod.isPresent());
        Assert.assertEquals(-45.987720764, mod.get().getMolecularMass(), 0.000001);
    }
}
