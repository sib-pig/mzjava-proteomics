package org.expasy.mzjava.proteomics.io.ms.ident;

import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.core.mol.NumericMass;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.proteomics.ms.ident.ModificationMatch;
import org.expasy.mzjava.proteomics.ms.ident.PeptideMatch;
import org.expasy.mzjava.proteomics.ms.ident.PeptideMatchComparator;
import org.expasy.mzjava.proteomics.ms.ident.PeptideProteinMatch;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.proteomics.io.ms.ident.pepxml.v117.EngineType;
import org.expasy.mzjava.proteomics.io.ms.ident.pepxml.v117.MassType;
import org.expasy.mzjava.proteomics.io.ms.ident.pepxml.v117.MsmsPipelineAnalysis;
import org.junit.Assert;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.List;

import static java.nio.file.Files.newBufferedReader;

/**
 * @author fnikitin
 * Date: 1/12/14
 */
public class PepXmlWriterTest {

    @Test
    public void testBuilder1() throws Exception {

        PepXmlWriter writer = PepXmlWriterBuilder.create(EngineType.X_TANDEM, "hyperscore")
                .enzyme(PepXmlWriterBuilder.createEnzyme("trypsin").specificity("C", "KR").noCut("P").endEnzyme())
                .parameter("output, maximum valid expectation value", "0.1")
                .parameter("output, sequences", "yes")
                .build();

        Assert.assertNotNull(writer);
    }

    @Test
    public void testBuilder2() throws Exception {

        PepXmlWriter writer = PepXmlWriterBuilder.create(EngineType.X_TANDEM, "hyperscore")
                .enzyme(PepXmlWriterBuilder.createEnzyme("trypsin").specificity("C", "KR").endEnzyme())
                .msManufacturer("Thermo Scientific").msModel("LTQ Orbitrap XL").msIonization("NSI").msMassAnalyzer("FTMS").msDetector("unknown")
                .precMassType(MassType.AVERAGE).fragMassType(MassType.MONOISOTOPIC).searchDatabase(new File("uniprot-human.fasta"))
                .parameter("output, maximum valid expectation value", "0.1")
                .parameter("output, sequences", "yes")
                .build();

        Assert.assertNotNull(writer);
    }

    @Test
    public void test1() throws Exception {

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        PepXmlWriter writer = newPepXmlWriter(EngineType.X_TANDEM);

        // spectrum query 1
        MsnSpectrum spectrum = new MsnSpectrum();

        spectrum.setSpectrumIndex(1);
        spectrum.addScanNumber(1729);
        spectrum.setSpectrumSource(new URI("file:///tmp/chludwig_B1103_063.mgf"));
        spectrum.setPrecursor(new Peak(2345.9457, 0, 2));

        PeptideMatch pm = new PeptideMatch("ATLEEAEGESGVEDDAATGSSNK");

        pm.setRank(Optional.fromNullable(1));
        pm.addProteinMatch(new PeptideProteinMatch("YLR196W", Optional.<String>absent(), Optional.<String>absent(), Optional.<String>absent(), PeptideProteinMatch.HitType.UNKNOWN));
        pm.setNumMatchedIons(Optional.fromNullable(33));
        pm.setTotalNumIons(Optional.fromNullable(44));
        pm.setNumMissedCleavages(Optional.fromNullable(0));
        pm.setRejected(Optional.fromNullable(false));
        pm.setMassDiff(Optional.fromNullable(0.008));
        Modification mod = new Modification("phospho", new NumericMass(79.966));
        pm.addModificationMatch(9, new ModificationMatch(mod, pm.getSymbol(9), 9, ModAttachment.SIDE_CHAIN));

        pm.addScore("hyperscore" , 123.4);
        pm.addScore("nextscore" , 72);
        pm.addScore("bscore" , 9.7);
        pm.addScore("yscore" , 11.1);
        pm.addScore("cscore" , 0);
        pm.addScore("zscore" , 0);
        pm.addScore("ascore" , 0);
        pm.addScore("xscore" , 0);
        pm.addScore("expect" , 7.4e-012);

        writer.add(spectrum, pm);

        // spectrum query 2
        spectrum = new MsnSpectrum();

        spectrum.setSpectrumIndex(2);
        spectrum.addScanNumber(966);
        spectrum.setSpectrumSource(new URI("file:///tmp/chludwig_B1103_063.mgf"));
        spectrum.setPrecursor(new Peak(2507.0729, 0, 2));

        pm = new PeptideMatch("AAEAGETGAATSATEGDNNNNTAAGDK");

        pm.setRank(Optional.fromNullable(1));
        pm.addProteinMatch(new PeptideProteinMatch("YDL084W", Optional.<String>absent(), Optional.<String>absent(), Optional.<String>absent(), PeptideProteinMatch.HitType.UNKNOWN));
        pm.setNumMatchedIons(Optional.fromNullable(37));
        pm.setTotalNumIons(Optional.fromNullable(52));
        pm.setNumMissedCleavages(Optional.fromNullable(0));
        pm.setRejected(Optional.fromNullable(false));
        pm.setMassDiff(Optional.fromNullable(0.008));

        pm.addScore("hyperscore" , 145.7);
        pm.addScore("nextscore" , 44.9);
        pm.addScore("bscore" , 11.4);
        pm.addScore("yscore" , 12.2);
        pm.addScore("cscore" , 0);
        pm.addScore("zscore" , 0);
        pm.addScore("ascore" , 0);
        pm.addScore("xscore" , 0);
        pm.addScore("expect" , 2.4e-011);

        writer.add(spectrum, pm);

        writer.write(bos, new URI("file:///tmp/chludwig_B1103_063.pep.xml"));

        String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
                "<msms_pipeline_analysis xmlns=\"http://regis-web.systemsbiology.net/pepXML\" date=\"\" summary_xml=\"/tmp/chludwig_B1103_063.pep.xml\">\n" +
                "    <msms_run_summary base_name=\"chludwig_B1103_063\" raw_data_type=\"raw\" raw_data=\"mgf\">\n" +
                "        <sample_enzyme name=\"trypsin\">\n" +
                "            <specificity sense=\"C\" cut=\"KR\" no_cut=\"P\"/>\n" +
                "        </sample_enzyme>\n" +
                "        <search_summary base_name=\"chludwig_B1103_063\" search_engine=\"X! Tandem\" precursor_mass_type=\"monoisotopic\" fragment_mass_type=\"monoisotopic\" search_id=\"1\">\n" +
                "            <enzymatic_search_constraint enzyme=\"trypsin\" max_num_internal_cleavages=\"1\" min_number_termini=\"2\"/>\n" +
                "            <aminoacid_modification aminoacid=\"S\" massdiff=\"79.966\" mass=\"166.99803\" variable=\"N\"/>\n" +
                "            <parameter name=\"output, maximum valid expectation value\" value=\"0.1\"/>\n" +
                "            <parameter name=\"output, sequences\" value=\"yes\"/>\n" +
                "        </search_summary>\n" +
                "        <spectrum_query spectrum=\"chludwig_B1103_063.1729.1729.2\" start_scan=\"1729\" end_scan=\"1729\" precursor_neutral_mass=\"4689.877\" assumed_charge=\"2\" index=\"1\">\n" +
                "            <search_result>\n" +
                "                <search_hit hit_rank=\"1\" peptide=\"ATLEEAEGESGVEDDAATGSSNK\" protein=\"YLR196W\" num_tot_proteins=\"1\" num_matched_ions=\"33\" tot_num_ions=\"44\" calc_neutral_pep_mass=\"2345.938\" massdiff=\"0.008\" num_missed_cleavages=\"0\" is_rejected=\"0\">\n" +
                "                    <modification_info>\n" +
                "                        <mod_aminoacid_mass position=\"10\" mass=\"166.99803161621094\"/>\n" +
                "                    </modification_info>\n" +
                "                    <search_score name=\"ascore\" value=\"0.0\"/>\n" +
                "                    <search_score name=\"bscore\" value=\"9.7\"/>\n" +
                "                    <search_score name=\"nextscore\" value=\"72.0\"/>\n" +
                "                    <search_score name=\"yscore\" value=\"11.1\"/>\n" +
                "                    <search_score name=\"cscore\" value=\"0.0\"/>\n" +
                "                    <search_score name=\"xscore\" value=\"0.0\"/>\n" +
                "                    <search_score name=\"zscore\" value=\"0.0\"/>\n" +
                "                    <search_score name=\"hyperscore\" value=\"123.4\"/>\n" +
                "                    <search_score name=\"expect\" value=\"7.4E-12\"/>\n" +
                "                </search_hit>\n" +
                "            </search_result>\n" +
                "        </spectrum_query>\n" +
                "        <spectrum_query spectrum=\"chludwig_B1103_063.966.966.2\" start_scan=\"966\" end_scan=\"966\" precursor_neutral_mass=\"5012.1313\" assumed_charge=\"2\" index=\"2\">\n" +
                "            <search_result>\n" +
                "                <search_hit hit_rank=\"1\" peptide=\"AAEAGETGAATSATEGDNNNNTAAGDK\" protein=\"YDL084W\" num_tot_proteins=\"1\" num_matched_ions=\"37\" tot_num_ions=\"52\" calc_neutral_pep_mass=\"2507.0645\" massdiff=\"0.008\" num_missed_cleavages=\"0\" is_rejected=\"0\">\n" +
                "                    <search_score name=\"ascore\" value=\"0.0\"/>\n" +
                "                    <search_score name=\"bscore\" value=\"11.4\"/>\n" +
                "                    <search_score name=\"nextscore\" value=\"44.9\"/>\n" +
                "                    <search_score name=\"yscore\" value=\"12.2\"/>\n" +
                "                    <search_score name=\"cscore\" value=\"0.0\"/>\n" +
                "                    <search_score name=\"xscore\" value=\"0.0\"/>\n" +
                "                    <search_score name=\"zscore\" value=\"0.0\"/>\n" +
                "                    <search_score name=\"hyperscore\" value=\"145.7\"/>\n" +
                "                    <search_score name=\"expect\" value=\"2.4E-11\"/>\n" +
                "                </search_hit>\n" +
                "            </search_result>\n" +
                "        </spectrum_query>\n" +
                "    </msms_run_summary>\n" +
                "</msms_pipeline_analysis>\n";

        Assert.assertEquals(expected, bos.toString().replaceAll("date=\"[^\"]+\"", "date=\"\""));
    }

    @Test
    public void test2() throws Exception {

        PepXmlWriter writer = newPepXmlWriter(EngineType.CUSTOM);

        // spectrum query 1
        MsnSpectrum spectrum = new MsnSpectrum();

        spectrum.setSpectrumIndex(1);
        spectrum.addScanNumber(1729);
        spectrum.setSpectrumSource(new URI("file:///tmp/chludwig_B1103_063.mgf"));
        spectrum.setPrecursor(new Peak(2345.9457, 0, 2));

        PeptideMatch pm = new PeptideMatch("ATLEEAEGESGVEDDAATGSSNK");

        pm.setRank(Optional.fromNullable(1));
        pm.addProteinMatch(new PeptideProteinMatch("YLR196W", Optional.<String>absent(), Optional.<String>absent(), Optional.<String>absent(), PeptideProteinMatch.HitType.UNKNOWN));
        pm.setNumMatchedIons(Optional.fromNullable(33));
        pm.setTotalNumIons(Optional.fromNullable(44));
        pm.setNumMissedCleavages(Optional.fromNullable(0));
        pm.setRejected(Optional.fromNullable(false));
        pm.setMassDiff(Optional.fromNullable(0.008));
        Modification mod = new Modification("phospho", new NumericMass(79.966));
        pm.addModificationMatch(9, new ModificationMatch(mod, pm.getSymbol(9), 9, ModAttachment.SIDE_CHAIN));

        pm.addScore("hyperscore" , 123.4);
        pm.addScore("nextscore" , 72);
        pm.addScore("bscore" , 9.7);
        pm.addScore("yscore" , 11.1);
        pm.addScore("cscore" , 0);
        pm.addScore("zscore" , 0);
        pm.addScore("ascore" , 0);
        pm.addScore("xscore" , 0);
        pm.addScore("expect" , 7.4e-012);

        writer.add(spectrum, pm);

        // spectrum query 2
        spectrum = new MsnSpectrum();

        spectrum.setSpectrumIndex(2);
        spectrum.addScanNumber(966);
        spectrum.setSpectrumSource(new URI("file:///tmp/chludwig_B1103_063.mgf"));
        spectrum.setPrecursor(new Peak(2507.0729, 0, 2));

        pm = new PeptideMatch("AAEAGETGAATSATEGDNNNNTAAGDK");

        pm.setRank(Optional.fromNullable(1));
        pm.addProteinMatch(new PeptideProteinMatch("YDL084W", Optional.<String>absent(), Optional.<String>absent(), Optional.<String>absent(), PeptideProteinMatch.HitType.UNKNOWN));
        pm.setNumMatchedIons(Optional.fromNullable(37));
        pm.setTotalNumIons(Optional.fromNullable(52));
        pm.setNumMissedCleavages(Optional.fromNullable(0));
        pm.setRejected(Optional.fromNullable(false));
        pm.setMassDiff(Optional.fromNullable(0.008));

        pm.addScore("hyperscore" , 145.7);
        pm.addScore("nextscore" , 44.9);
        pm.addScore("bscore" , 11.4);
        pm.addScore("yscore" , 12.2);
        pm.addScore("cscore" , 0);
        pm.addScore("zscore" , 0);
        pm.addScore("ascore" , 0);
        pm.addScore("xscore" , 0);
        pm.addScore("expect" , 2.4e-011);

        writer.add(spectrum, pm);

        // spectrum query 3
        spectrum = new MsnSpectrum();

        spectrum.setSpectrumIndex(43);
        spectrum.addScanNumber(96);
        spectrum.setSpectrumSource(new URI("file:///tmp/chludwig_B1103_263.mgf"));
        spectrum.setPrecursor(new Peak(2507.0729, 0, 3));

        pm = new PeptideMatch("AAEAGETGAATSATEGDAGDK");

        pm.setRank(Optional.fromNullable(1));
        pm.addProteinMatch(new PeptideProteinMatch("XPS084W", Optional.<String>absent(), Optional.<String>absent(), Optional.<String>absent(), PeptideProteinMatch.HitType.UNKNOWN));
        pm.setNumMatchedIons(Optional.fromNullable(27));
        pm.setTotalNumIons(Optional.fromNullable(32));
        pm.setNumMissedCleavages(Optional.fromNullable(0));
        pm.setRejected(Optional.fromNullable(false));
        pm.setMassDiff(Optional.fromNullable(0.008));

        pm.addScore("hyperscore" , 145.7);
        pm.addScore("nextscore" , 44.9);
        pm.addScore("bscore" , 11.4);
        pm.addScore("yscore" , 12.2);
        pm.addScore("cscore" , 0);
        pm.addScore("zscore" , 0);
        pm.addScore("ascore" , 0);
        pm.addScore("xscore" , 0);
        pm.addScore("expect" , 2.4e-011);

        writer.add(spectrum, pm);

        File file = File.createTempFile("mymsidents", ".pepxml");

        writer.write(file);

        String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
                "<msms_pipeline_analysis xmlns=\"http://regis-web.systemsbiology.net/pepXML\" date=\"\" summary_xml=\"\">\n" +
                "    <msms_run_summary base_name=\"chludwig_B1103_063\" raw_data_type=\"raw\" raw_data=\"mgf\">\n" +
                "        <sample_enzyme name=\"trypsin\">\n" +
                "            <specificity sense=\"C\" cut=\"KR\" no_cut=\"P\"/>\n" +
                "        </sample_enzyme>\n" +
                "        <search_summary base_name=\"chludwig_B1103_063\" search_engine=\"CUSTOM\" precursor_mass_type=\"monoisotopic\" fragment_mass_type=\"monoisotopic\" search_id=\"1\">\n" +
                "            <enzymatic_search_constraint enzyme=\"trypsin\" max_num_internal_cleavages=\"1\" min_number_termini=\"2\"/>\n"+
                "            <aminoacid_modification aminoacid=\"S\" massdiff=\"79.966\" mass=\"166.99803\" variable=\"N\"/>\n" +
                "            <parameter name=\"output, maximum valid expectation value\" value=\"0.1\"/>\n" +
                "            <parameter name=\"output, sequences\" value=\"yes\"/>\n" +
                "        </search_summary>\n" +
                "        <spectrum_query spectrum=\"chludwig_B1103_063.1729.1729.2\" start_scan=\"1729\" end_scan=\"1729\" precursor_neutral_mass=\"4689.877\" assumed_charge=\"2\" index=\"1\">\n" +
                "            <search_result>\n" +
                "                <search_hit hit_rank=\"1\" peptide=\"ATLEEAEGESGVEDDAATGSSNK\" protein=\"YLR196W\" num_tot_proteins=\"1\" num_matched_ions=\"33\" tot_num_ions=\"44\" calc_neutral_pep_mass=\"2345.938\" massdiff=\"0.008\" num_missed_cleavages=\"0\" is_rejected=\"0\">\n" +
                "                    <modification_info>\n" +
                "                        <mod_aminoacid_mass position=\"10\" mass=\"166.99803161621094\"/>\n" +
                "                    </modification_info>\n" +
                "                    <search_score name=\"ascore\" value=\"0.0\"/>\n" +
                "                    <search_score name=\"bscore\" value=\"9.7\"/>\n" +
                "                    <search_score name=\"nextscore\" value=\"72.0\"/>\n" +
                "                    <search_score name=\"yscore\" value=\"11.1\"/>\n" +
                "                    <search_score name=\"cscore\" value=\"0.0\"/>\n" +
                "                    <search_score name=\"xscore\" value=\"0.0\"/>\n" +
                "                    <search_score name=\"zscore\" value=\"0.0\"/>\n" +
                "                    <search_score name=\"hyperscore\" value=\"123.4\"/>\n" +
                "                    <search_score name=\"expect\" value=\"7.4E-12\"/>\n" +
                "                </search_hit>\n" +
                "            </search_result>\n" +
                "        </spectrum_query>\n" +
                "        <spectrum_query spectrum=\"chludwig_B1103_063.966.966.2\" start_scan=\"966\" end_scan=\"966\" precursor_neutral_mass=\"5012.1313\" assumed_charge=\"2\" index=\"2\">\n" +
                "            <search_result>\n" +
                "                <search_hit hit_rank=\"1\" peptide=\"AAEAGETGAATSATEGDNNNNTAAGDK\" protein=\"YDL084W\" num_tot_proteins=\"1\" num_matched_ions=\"37\" tot_num_ions=\"52\" calc_neutral_pep_mass=\"2507.0645\" massdiff=\"0.008\" num_missed_cleavages=\"0\" is_rejected=\"0\">\n" +
                "                    <search_score name=\"ascore\" value=\"0.0\"/>\n" +
                "                    <search_score name=\"bscore\" value=\"11.4\"/>\n" +
                "                    <search_score name=\"nextscore\" value=\"44.9\"/>\n" +
                "                    <search_score name=\"yscore\" value=\"12.2\"/>\n" +
                "                    <search_score name=\"cscore\" value=\"0.0\"/>\n" +
                "                    <search_score name=\"xscore\" value=\"0.0\"/>\n" +
                "                    <search_score name=\"zscore\" value=\"0.0\"/>\n" +
                "                    <search_score name=\"hyperscore\" value=\"145.7\"/>\n" +
                "                    <search_score name=\"expect\" value=\"2.4E-11\"/>\n" +
                "                </search_hit>\n" +
                "            </search_result>\n" +
                "        </spectrum_query>\n" +
                "    </msms_run_summary>\n" +
                "    <msms_run_summary base_name=\"chludwig_B1103_263\" raw_data_type=\"raw\" raw_data=\"mgf\">\n" +
                "        <sample_enzyme name=\"trypsin\">\n" +
                "            <specificity sense=\"C\" cut=\"KR\" no_cut=\"P\"/>\n" +
                "        </sample_enzyme>\n" +
                "        <search_summary base_name=\"chludwig_B1103_263\" search_engine=\"CUSTOM\" precursor_mass_type=\"monoisotopic\" fragment_mass_type=\"monoisotopic\" search_id=\"1\">\n" +
                "            <enzymatic_search_constraint enzyme=\"trypsin\" max_num_internal_cleavages=\"1\" min_number_termini=\"2\"/>\n"+
                "            <parameter name=\"output, maximum valid expectation value\" value=\"0.1\"/>\n" +
                "            <parameter name=\"output, sequences\" value=\"yes\"/>\n" +
                "        </search_summary>\n" +
                "        <spectrum_query spectrum=\"chludwig_B1103_263.96.96.3\" start_scan=\"96\" end_scan=\"96\" precursor_neutral_mass=\"7518.197\" assumed_charge=\"3\" index=\"43\">\n" +
                "            <search_result>\n" +
                "                <search_hit hit_rank=\"1\" peptide=\"AAEAGETGAATSATEGDAGDK\" protein=\"XPS084W\" num_tot_proteins=\"1\" num_matched_ions=\"27\" tot_num_ions=\"32\" calc_neutral_pep_mass=\"1878.8079\" massdiff=\"0.008\" num_missed_cleavages=\"0\" is_rejected=\"0\">\n" +
                "                    <search_score name=\"ascore\" value=\"0.0\"/>\n" +
                "                    <search_score name=\"bscore\" value=\"11.4\"/>\n" +
                "                    <search_score name=\"nextscore\" value=\"44.9\"/>\n" +
                "                    <search_score name=\"yscore\" value=\"12.2\"/>\n" +
                "                    <search_score name=\"cscore\" value=\"0.0\"/>\n" +
                "                    <search_score name=\"xscore\" value=\"0.0\"/>\n" +
                "                    <search_score name=\"zscore\" value=\"0.0\"/>\n" +
                "                    <search_score name=\"hyperscore\" value=\"145.7\"/>\n" +
                "                    <search_score name=\"expect\" value=\"2.4E-11\"/>\n" +
                "                </search_hit>\n" +
                "            </search_result>\n" +
                "        </spectrum_query>\n" +
                "    </msms_run_summary>\n" +
                "</msms_pipeline_analysis>\n";

        Assert.assertEquals(expected, getContent(file).replaceAll("date=\"[^\"]+\"", "date=\"\"").replaceAll("summary_xml=\"[^\"]+\"", "summary_xml=\"\""));
    }

    @Test
    public void test3() throws Exception {

        PepXmlWriter writer = newPepXmlWriter(EngineType.CUSTOM);

        // spectrum query 1
        MsnSpectrum spectrum = new MsnSpectrum();

        spectrum.setSpectrumIndex(1);
        spectrum.addScanNumber(1729);
        spectrum.setSpectrumSource(new URI("file:///tmp/chludwig_B1103_063.mgf"));
        spectrum.setPrecursor(new Peak(2345.9457, 0, 2));

        List<PeptideMatch> pms = Lists.newArrayList();

        PeptideMatch pm = new PeptideMatch("ATLEEAEGESGVEDDAATGSSNK");

        pm.setRank(Optional.fromNullable(1));
        pm.addProteinMatch(new PeptideProteinMatch("YLR196W", Optional.<String>absent(), Optional.<String>absent(), Optional.<String>absent(), PeptideProteinMatch.HitType.UNKNOWN));
        pm.setNumMatchedIons(Optional.fromNullable(33));
        pm.setTotalNumIons(Optional.fromNullable(44));
        pm.setNumMissedCleavages(Optional.fromNullable(0));
        pm.setRejected(Optional.fromNullable(false));
        pm.setMassDiff(Optional.fromNullable(0.008));
        Modification mod = new Modification("phospho", new NumericMass(79.966));
        pm.addModificationMatch(9, new ModificationMatch(mod, pm.getSymbol(9), 9, ModAttachment.SIDE_CHAIN));

        pm.addScore("hyperscore" , 123.4);
        pm.addScore("nextscore" , 72);
        pm.addScore("bscore" , 9.7);
        pm.addScore("yscore" , 11.1);
        pm.addScore("cscore" , 0);
        pm.addScore("zscore" , 0);
        pm.addScore("ascore" , 0);
        pm.addScore("xscore" , 0);
        pm.addScore("expect" , 7.4e-012);

        pms.add(pm);

        pm = new PeptideMatch("AAEAGETGAATSATEGDNNNNTAAGDK");

        pm.setRank(Optional.fromNullable(1));
        pm.addProteinMatch(new PeptideProteinMatch("YDL084W", Optional.<String>absent(), Optional.<String>absent(), Optional.<String>absent(), PeptideProteinMatch.HitType.UNKNOWN));
        pm.setNumMatchedIons(Optional.fromNullable(37));
        pm.setTotalNumIons(Optional.fromNullable(52));
        pm.setNumMissedCleavages(Optional.fromNullable(0));
        pm.setRejected(Optional.fromNullable(false));
        pm.setMassDiff(Optional.fromNullable(0.008));
        pm.addModificationMatch(11, new ModificationMatch(mod, pm.getSymbol(9), 9, ModAttachment.SIDE_CHAIN));

        pm.addScore("hyperscore" , 145.7);
        pm.addScore("nextscore" , 44.9);
        pm.addScore("bscore" , 11.4);
        pm.addScore("yscore" , 12.2);
        pm.addScore("cscore" , 0);
        pm.addScore("zscore" , 0);
        pm.addScore("ascore" , 0);
        pm.addScore("xscore" , 0);
        pm.addScore("expect" , 2.4e-011);

        pms.add(pm);

        pm = new PeptideMatch("AAEAGETGAATSATEGDAGDK");

        pm.setRank(Optional.fromNullable(1));
        pm.addProteinMatch(new PeptideProteinMatch("XPS084W", Optional.<String>absent(), Optional.<String>absent(), Optional.<String>absent(), PeptideProteinMatch.HitType.UNKNOWN));
        pm.setNumMatchedIons(Optional.fromNullable(27));
        pm.setTotalNumIons(Optional.fromNullable(32));
        pm.setNumMissedCleavages(Optional.fromNullable(0));
        pm.setRejected(Optional.fromNullable(false));
        pm.setMassDiff(Optional.fromNullable(0.008));

        pm.addScore("hyperscore" , 143.7);
        pm.addScore("nextscore" , 44.9);
        pm.addScore("bscore" , 11.4);
        pm.addScore("yscore" , 12.2);
        pm.addScore("cscore" , 0);
        pm.addScore("zscore" , 0);
        pm.addScore("ascore" , 0);
        pm.addScore("xscore" , 0);
        pm.addScore("expect" , 2.4e-011);

        pms.add(pm);

        writer.add(spectrum, pms);

        File file = File.createTempFile("mymsidents", ".pepxml");

        writer.write(file);

        String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
                "<msms_pipeline_analysis xmlns=\"http://regis-web.systemsbiology.net/pepXML\" date=\"\" summary_xml=\"\">\n" +
                "    <msms_run_summary base_name=\"chludwig_B1103_063\" raw_data_type=\"raw\" raw_data=\"mgf\">\n" +
                "        <sample_enzyme name=\"trypsin\">\n" +
                "            <specificity sense=\"C\" cut=\"KR\" no_cut=\"P\"/>\n" +
                "        </sample_enzyme>\n" +
                "        <search_summary base_name=\"chludwig_B1103_063\" search_engine=\"CUSTOM\" precursor_mass_type=\"monoisotopic\" fragment_mass_type=\"monoisotopic\" search_id=\"1\">\n" +
                "            <enzymatic_search_constraint enzyme=\"trypsin\" max_num_internal_cleavages=\"1\" min_number_termini=\"2\"/>\n" +
                "            <aminoacid_modification aminoacid=\"S\" massdiff=\"79.966\" mass=\"166.99803\" variable=\"N\"/>\n" +
                "            <parameter name=\"output, maximum valid expectation value\" value=\"0.1\"/>\n" +
                "            <parameter name=\"output, sequences\" value=\"yes\"/>\n" +
                "        </search_summary>\n" +
                "        <spectrum_query spectrum=\"chludwig_B1103_063.1729.1729.2\" start_scan=\"1729\" end_scan=\"1729\" precursor_neutral_mass=\"4689.877\" assumed_charge=\"2\" index=\"1\">\n" +
                "            <search_result>\n" +
                "                <search_hit hit_rank=\"1\" peptide=\"AAEAGETGAATSATEGDNNNNTAAGDK\" protein=\"YDL084W\" num_tot_proteins=\"1\" num_matched_ions=\"37\" tot_num_ions=\"52\" calc_neutral_pep_mass=\"2587.0305\" massdiff=\"0.008\" num_missed_cleavages=\"0\" is_rejected=\"0\">\n" +
                "                    <modification_info>\n" +
                "                        <mod_aminoacid_mass position=\"12\" mass=\"166.99803161621094\"/>\n" +
                "                    </modification_info>\n" +
                "                    <search_score name=\"ascore\" value=\"0.0\"/>\n" +
                "                    <search_score name=\"bscore\" value=\"11.4\"/>\n" +
                "                    <search_score name=\"nextscore\" value=\"44.9\"/>\n" +
                "                    <search_score name=\"yscore\" value=\"12.2\"/>\n" +
                "                    <search_score name=\"cscore\" value=\"0.0\"/>\n" +
                "                    <search_score name=\"xscore\" value=\"0.0\"/>\n" +
                "                    <search_score name=\"zscore\" value=\"0.0\"/>\n" +
                "                    <search_score name=\"hyperscore\" value=\"145.7\"/>\n" +
                "                    <search_score name=\"expect\" value=\"2.4E-11\"/>\n" +
                "                </search_hit>\n" +
                "                <search_hit hit_rank=\"2\" peptide=\"AAEAGETGAATSATEGDAGDK\" protein=\"XPS084W\" num_tot_proteins=\"1\" num_matched_ions=\"27\" tot_num_ions=\"32\" calc_neutral_pep_mass=\"1878.8079\" massdiff=\"0.008\" num_missed_cleavages=\"0\" is_rejected=\"0\">\n" +
                "                    <search_score name=\"ascore\" value=\"0.0\"/>\n" +
                "                    <search_score name=\"bscore\" value=\"11.4\"/>\n" +
                "                    <search_score name=\"nextscore\" value=\"44.9\"/>\n" +
                "                    <search_score name=\"yscore\" value=\"12.2\"/>\n" +
                "                    <search_score name=\"cscore\" value=\"0.0\"/>\n" +
                "                    <search_score name=\"xscore\" value=\"0.0\"/>\n" +
                "                    <search_score name=\"zscore\" value=\"0.0\"/>\n" +
                "                    <search_score name=\"hyperscore\" value=\"143.7\"/>\n" +
                "                    <search_score name=\"expect\" value=\"2.4E-11\"/>\n" +
                "                </search_hit>\n" +
                "                <search_hit hit_rank=\"3\" peptide=\"ATLEEAEGESGVEDDAATGSSNK\" protein=\"YLR196W\" num_tot_proteins=\"1\" num_matched_ions=\"33\" tot_num_ions=\"44\" calc_neutral_pep_mass=\"2345.938\" massdiff=\"0.008\" num_missed_cleavages=\"0\" is_rejected=\"0\">\n" +
                "                    <modification_info>\n" +
                "                        <mod_aminoacid_mass position=\"10\" mass=\"166.99803161621094\"/>\n" +
                "                    </modification_info>\n" +
                "                    <search_score name=\"ascore\" value=\"0.0\"/>\n" +
                "                    <search_score name=\"bscore\" value=\"9.7\"/>\n" +
                "                    <search_score name=\"nextscore\" value=\"72.0\"/>\n" +
                "                    <search_score name=\"yscore\" value=\"11.1\"/>\n" +
                "                    <search_score name=\"cscore\" value=\"0.0\"/>\n" +
                "                    <search_score name=\"xscore\" value=\"0.0\"/>\n" +
                "                    <search_score name=\"zscore\" value=\"0.0\"/>\n" +
                "                    <search_score name=\"hyperscore\" value=\"123.4\"/>\n" +
                "                    <search_score name=\"expect\" value=\"7.4E-12\"/>\n" +
                "                </search_hit>\n" +
                "            </search_result>\n" +
                "        </spectrum_query>\n" +
                "    </msms_run_summary>\n" +
                "</msms_pipeline_analysis>\n";

        Assert.assertEquals(expected, getContent(file).replaceAll("date=\"[^\"]+\"", "date=\"\"").replaceAll("summary_xml=\"[^\"]+\"", "summary_xml=\"\""));
    }

    private String getContent(File file) throws IOException {

        BufferedReader br = newBufferedReader(file.toPath(), Charset.defaultCharset());

        String line;
        StringBuilder sb = new StringBuilder();

        while ( (line = br.readLine()) != null) {

            sb.append(line);
            sb.append("\n");
        }

        return sb.toString();
    }

    @Test
    public void testToModAaMass() {

        PepXmlWriter writer = newPepXmlWriter(EngineType.CUSTOM);

        MsmsPipelineAnalysis.MsmsRunSummary.SpectrumQuery.SearchResult.SearchHit.ModificationInfo.ModAminoacidMass modAaMass = writer.toModAaMass(166.998f, 9);

        Assert.assertEquals(BigInteger.valueOf(10), modAaMass.getPosition());
    }

    @Test
    public void testToAminoacidModification() {

        PepXmlWriter writer = newPepXmlWriter(EngineType.CUSTOM);

        MsmsPipelineAnalysis.MsmsRunSummary.SpectrumQuery.SearchResult.SearchHit.ModificationInfo.ModAminoacidMass modAaMass = writer.toModAaMass(166.998f, 9);

        MsmsPipelineAnalysis.MsmsRunSummary.SearchSummary.AminoacidModification modifs =
                writer.toAminoacidModification(AminoAcid.S, modAaMass);

        Assert.assertEquals("79.96597", modifs.getMassdiff());
    }

    @Test
    public void testToSpectrumQuery() {

        PepXmlWriter writer = newPepXmlWriter(EngineType.CUSTOM);

        MsnSpectrum spectrum = new MsnSpectrum();

        spectrum.setSpectrumIndex(1);
        spectrum.addScanNumber(1729);
        spectrum.setSpectrumSource(new File("chludwig_B1103_063.mgf").toURI());
        spectrum.setPrecursor(new Peak(2345.9457, 1, 1));

        MsmsPipelineAnalysis.MsmsRunSummary.SpectrumQuery spectrumQuery =
                writer.toSpectrumQuery(spectrum);

        Assert.assertEquals("chludwig_B1103_063.1729.1729.1", spectrumQuery.getSpectrum());
        Assert.assertEquals(1729, spectrumQuery.getStartScan());
        Assert.assertEquals(1729, spectrumQuery.getEndScan());
        Assert.assertEquals(2344.9384, spectrumQuery.getPrecursorNeutralMass(), 0.01);
        Assert.assertEquals(BigInteger.ONE, spectrumQuery.getAssumedCharge());
        Assert.assertEquals(1, spectrumQuery.getIndex());
    }

    @Test (expected = IllegalStateException.class)
    public void testToSpectrumQueryScanNumberStrict1() {

        MsnSpectrum spectrum = new MsnSpectrum();

        spectrum.setSpectrumIndex(1);
        spectrum.setSpectrumSource(new File("chludwig_B1103_063.mgf").toURI());
        spectrum.setPrecursor(new Peak(2345.9457, 1, 1));

        PepXmlWriter writer = newPepXmlWriter(EngineType.CUSTOM);
        writer.toSpectrumQuery(spectrum);
    }

    @Test (expected = IllegalStateException.class)
    public void testToSpectrumQueryScanNumberStrict2() {

        MsnSpectrum spectrum = new MsnSpectrum();

        spectrum.setSpectrumIndex(1);
        spectrum.setSpectrumSource(new File("chludwig_B1103_063.mgf").toURI());
        spectrum.setPrecursor(new Peak(2345.9457, 1, 1));

        PepXmlWriter writer = PepXmlWriter.newStrictWriter(EngineType.CUSTOM, "hyperscore");
        writer.toSpectrumQuery(spectrum);
    }

    @Test
    public void testToSpectrumQueryScanNumberAbscenceTolerated1() throws URISyntaxException {

        MsnSpectrum spectrum = new MsnSpectrum();

        spectrum.setSpectrumIndex(1);
        spectrum.setSpectrumSource(new URI("file:///tmp/chludwig_B1103_063.mgf"));
        spectrum.setPrecursor(new Peak(2345.9457, 1, 1));

        PepXmlWriter writer = PepXmlWriter.newTolerantWriter(EngineType.CUSTOM, "hyperscore");
        writer.toSpectrumQuery(spectrum);
    }

    @Test
    public void testToSpectrumQueryScanNumberAbscenceTolerated2() throws URISyntaxException {

        MsnSpectrum spectrum = new MsnSpectrum();

        spectrum.setSpectrumIndex(1);
        spectrum.setSpectrumSource(new URI("file:///tmp/chludwig_B1103_063.mgf"));
        spectrum.setPrecursor(new Peak(2345.9457, 1, 1));

        PepXmlWriter writer = PepXmlWriter.newTolerantWriter(EngineType.CUSTOM, new PeptideMatchComparator("hyperscore"));
        writer.toSpectrumQuery(spectrum);
    }

    @Test
    public void testToSearchHit() {

        PeptideMatch pm = new PeptideMatch("ATLEEAEGESGVEDDAATGSSNK");

        pm.setRank(Optional.fromNullable(1));
        pm.addProteinMatch(new PeptideProteinMatch("YLR196W", Optional.<String>absent(), Optional.<String>absent(), Optional.<String>absent(), PeptideProteinMatch.HitType.UNKNOWN));
        pm.setNumMatchedIons(Optional.fromNullable(33));
        pm.setTotalNumIons(Optional.fromNullable(44));
        pm.setNumMissedCleavages(Optional.fromNullable(0));
        pm.setRejected(Optional.fromNullable(false));
        pm.setMassDiff(Optional.fromNullable(0.008));
        Modification mod = new Modification("phospho", new NumericMass(79.966));
        pm.addModificationMatch(9, new ModificationMatch(mod, pm.getSymbol(9), 9, ModAttachment.SIDE_CHAIN));

        pm.addScore("hyperscore" , 145.7);
        pm.addScore("nextscore" , 44.9);
        pm.addScore("bscore" , 11.4);
        pm.addScore("yscore" , 12.2);
        pm.addScore("cscore" , 0);
        pm.addScore("zscore" , 0);
        pm.addScore("ascore" , 0);
        pm.addScore("xscore" , 0);
        pm.addScore("expect" , 2.4e-011);

        PepXmlWriter writer = newPepXmlWriter(EngineType.CUSTOM);
        MsmsPipelineAnalysis.MsmsRunSummary.SpectrumQuery.SearchResult.SearchHit searchHit = writer.toSearchHit(pm);

        Assert.assertEquals(1, searchHit.getHitRank());
        Assert.assertEquals("YLR196W", searchHit.getProtein());
        Assert.assertEquals(BigInteger.valueOf(33), searchHit.getNumMatchedIons());
        Assert.assertEquals(BigInteger.valueOf(44), searchHit.getTotNumIons());
        Assert.assertEquals(1, searchHit.getNumTotProteins());
        Assert.assertEquals(BigInteger.ZERO, searchHit.getNumMissedCleavages());
        Assert.assertEquals(BigInteger.ZERO, searchHit.getIsRejected());
        Assert.assertEquals("0.008", searchHit.getMassdiff());
        Assert.assertEquals(9, searchHit.getSearchScore().size());

        MsmsPipelineAnalysis.MsmsRunSummary.SpectrumQuery.SearchResult.SearchHit.ModificationInfo modInfo = searchHit.getModificationInfo();
        List<MsmsPipelineAnalysis.MsmsRunSummary.SpectrumQuery.SearchResult.SearchHit.ModificationInfo.ModAminoacidMass> modAminoacidMassList = modInfo.getModAminoacidMass();

        Assert.assertEquals(1, modAminoacidMassList.size());
        Assert.assertNull(modInfo.getModNtermMass());
        Assert.assertNull(modInfo.getModCtermMass());
    }

    private static PepXmlWriter newPepXmlWriter(EngineType engineType) {

        return PepXmlWriterBuilder.create(engineType, "hyperscore")
                .enzymaticSearchConstraint(1, 2)
                .parameter("output, maximum valid expectation value", "0.1")
                .parameter("output, sequences", "yes")
                .build();
    }
}
