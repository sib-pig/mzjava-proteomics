package org.expasy.mzjava.proteomics.io.mol.fasta;

import org.expasy.mzjava.proteomics.mol.Protein;
import org.junit.Assert;
import org.junit.Test;

/**
 * @version sqrt -1
 * @author fnikitin
 * Date: 10/5/12
 */
public class UniprotFastaHeaderParserTest {

	@Test
	public void testSwissprot() throws Exception {

		FastaHeaderParser headerParser = new UniprotFastaHeaderParser();

		Protein.Builder builder = new Protein.Builder("PROTEIN");

		boolean parsed =
				headerParser.parseHeader("sp|Q0TET7|Q0TET7_ECOL5 Putative uncharacterized protein OS=Escherichia coli O6:K15:H31 (strain 536 / UPEC) GN=ECP_2553 PE=4 SV=1", builder);

		Assert.assertEquals(true, parsed);
		Assert.assertEquals("Q0TET7", builder.build().getAccessionId());
	}

    @Test
    public void testSwissprot2() throws Exception {

        FastaHeaderParser headerParser = new UniprotFastaHeaderParser();

        Protein.Builder builder = new Protein.Builder("PROTEIN");

        boolean parsed =
                headerParser.parseHeader("> sp|Q0TET7|Q0TET7_ECOL5 Putative uncharacterized protein OS=Escherichia coli O6:K15:H31 (strain 536 / UPEC) GN=ECP_2553 PE=4 SV=1", builder);

        Assert.assertEquals(true, parsed);
        Assert.assertEquals("Q0TET7", builder.build().getAccessionId());
    }

	@Test
	public void testTrembl() throws Exception {

		FastaHeaderParser headerParser = new UniprotFastaHeaderParser();

		Protein.Builder builder = new Protein.Builder("PROTEIN");

		boolean parsed =
				headerParser.parseHeader("tr|Q0TET7|Q0TET7_ECOL5 Putative uncharacterized protein OS=Escherichia coli O6:K15:H31 (strain 536 / UPEC) GN=ECP_2553 PE=4 SV=1", builder);

		Assert.assertEquals(true, parsed);
		Assert.assertEquals("Q0TET7", builder.build().getAccessionId());
	}

    @Test
    public void testTrembl2() throws Exception {

        FastaHeaderParser headerParser = new UniprotFastaHeaderParser();

        Protein.Builder builder = new Protein.Builder("PROTEIN");

        boolean parsed =
                headerParser.parseHeader("> tr|Q0TET7|Q0TET7_ECOL5 Putative uncharacterized protein OS=Escherichia coli O6:K15:H31 (strain 536 / UPEC) GN=ECP_2553 PE=4 SV=1", builder);

        Assert.assertEquals(true, parsed);
        Assert.assertEquals("Q0TET7", builder.build().getAccessionId());
    }
}
