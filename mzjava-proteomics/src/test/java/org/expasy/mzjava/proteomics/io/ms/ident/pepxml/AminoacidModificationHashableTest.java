package org.expasy.mzjava.proteomics.io.ms.ident.pepxml;

import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.proteomics.io.ms.ident.pepxml.v117.MsmsPipelineAnalysis;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author fnikitin
 * Date: 2/16/14
 */
public class AminoacidModificationHashableTest {

    @Test
    public void testAminoacidModificationNotEquals() {

        MsmsPipelineAnalysis.MsmsRunSummary.SearchSummary.AminoacidModification aminoacidModification = newAminoacidModification();

        Assert.assertNotEquals(aminoacidModification, newAminoacidModification());
    }

    @Test
    public void testEqualsAminoacidModificationHashable() {

        AminoacidModificationHashable aminoacidModificationHashable = new AminoacidModificationHashable(newAminoacidModification());

        Assert.assertEquals(aminoacidModificationHashable, new AminoacidModificationHashable(newAminoacidModification()));
    }

    @Test
    public void testNotEqualsAminoacidModificationHashable() {

        MsmsPipelineAnalysis.MsmsRunSummary.SearchSummary.AminoacidModification aam = newAminoacidModification();
        aam.setVariable("Y");

        AminoacidModificationHashable aminoacidModificationHashable = new AminoacidModificationHashable(aam);

        Assert.assertNotEquals(aminoacidModificationHashable, new AminoacidModificationHashable(newAminoacidModification()));
    }

    private MsmsPipelineAnalysis.MsmsRunSummary.SearchSummary.AminoacidModification newAminoacidModification() {

        MsmsPipelineAnalysis.MsmsRunSummary.SearchSummary.AminoacidModification aminoacidModification =
                new MsmsPipelineAnalysis.MsmsRunSummary.SearchSummary.AminoacidModification();

        aminoacidModification.setAminoacid(AminoAcid.C.getSymbol());
        aminoacidModification.setMassdiff("57.0220");
        aminoacidModification.setMass(160.0312f);
        aminoacidModification.setVariable("N");

        return aminoacidModification;
    }
}
