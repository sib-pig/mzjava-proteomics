/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.io.ms.spectrum;

import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrum.Spectrum;
import org.junit.Test;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author nikitin
 * @author Oliver Horlacher
 * @version 1.0
 */
public class DtaReaderTest {

    @Test(expected = IOException.class)
    public void testRead() throws IOException {

        DtaReader reader = new DtaReader(new FileReader(new File(getClass().getResource("dta_badfile.dta").getFile())), PeakList.Precision.DOUBLE);

        while (reader.hasNext()) {
            reader.next();
        }
    }

    @Test
    public void testReadTwice() throws ParseException, IOException {

        DtaReader reader = new DtaReader(new FileReader(new File(getClass().getResource("dta_test.dta").getFile())), PeakList.Precision.DOUBLE);

        MsnSpectrum sp = null;
        while (reader.hasNext()) {

            sp = reader.next();
        }

        assertNotNull(sp);
        assertEquals(59, sp.getScanNumbers().get(0).getValue());
    }

	@Test
	public void testReadUnsortedPeaks() throws IOException {

        DtaReader reader = new DtaReader(new FileReader(new File(getClass().getResource("dta-unsorted_test.dta").getFile())), PeakList.Precision.DOUBLE);
		reader.acceptUnsortedSpectra();

        Spectrum spectrum = reader.next();

		assertEquals(346, spectrum.size());
	}

	@Test (expected = IOException.class)
	public void testReadUnsortedPeaksNotAccepted() throws IOException {

        DtaReader reader = new DtaReader(new FileReader(new File(getClass().getResource("dta-unsorted_test.dta").getFile())), PeakList.Precision.DOUBLE);
		reader.next();
	}
}
