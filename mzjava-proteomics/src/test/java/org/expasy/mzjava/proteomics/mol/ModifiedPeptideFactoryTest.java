package org.expasy.mzjava.proteomics.mol;

import com.google.common.collect.Lists;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.List;

/**
 * @author fnikitin
 * Date: 11/7/13
 */
public class ModifiedPeptideFactoryTest {

    @Test
    public void testEmptyMods() {

        ModifiedPeptideFactory factory = new ModifiedPeptideFactory.Builder().build();

        List<Peptide> peptides = factory.createVarModPeptides(Peptide.parse("PEPTIDE"));

        Assert.assertEquals(0, peptides.size());
    }

    @Test
    public void testWithMods() {

        List<Peptide> expectedPeptides = Lists.newArrayList();

        expectedPeptides.add(Peptide.parse("(C2H2O)_P(H2O)EP(H2O)TIDE"));
        expectedPeptides.add(Peptide.parse("(C2H2O)_P(H2O)EP(H2O)TIDE_(OH)"));
        expectedPeptides.add(Peptide.parse("(C2H2O)_P(H2O)EPTIDE"));
        expectedPeptides.add(Peptide.parse("(C2H2O)_P(H2O)EPTIDE_(OH)"));
        expectedPeptides.add(Peptide.parse("(C2H2O)_PEP(H2O)TIDE"));
        expectedPeptides.add(Peptide.parse("(C2H2O)_PEP(H2O)TIDE_(OH)"));
        expectedPeptides.add(Peptide.parse("(C2H2O)_PEPTIDE"));
        expectedPeptides.add(Peptide.parse("(C2H2O)_PEPTIDE_(OH)"));
        expectedPeptides.add(Peptide.parse("P(H2O)EP(H2O)TIDE"));
        expectedPeptides.add(Peptide.parse("P(H2O)EP(H2O)TIDE_(OH)"));
        expectedPeptides.add(Peptide.parse("P(H2O)EPTIDE"));
        expectedPeptides.add(Peptide.parse("P(H2O)EPTIDE_(OH)"));
        expectedPeptides.add(Peptide.parse("PEP(H2O)TIDE"));
        expectedPeptides.add(Peptide.parse("PEP(H2O)TIDE_(OH)"));
        expectedPeptides.add(Peptide.parse("PEPTIDE_(OH)"));

        ModifiedPeptideFactory factory = new ModifiedPeptideFactory.Builder()
                .addTarget(EnumSet.of(AminoAcid.P), Modification.parseModification("H2O"))
                .addTarget(ModAttachment.N_TERM, Modification.parseModification("C2H2O"))
                .addTarget(ModAttachment.C_TERM, Modification.parseModification("OH"))
                .build();

        List<Peptide> peptides = factory.createVarModPeptides(Peptide.parse("PEPTIDE"));

        Collections.sort(peptides, new Comparator<Peptide>() {
            @Override
            public int compare(Peptide p1, Peptide p2) {

                return p1.toString().compareTo(p2.toString());
            }
        });

        Assert.assertEquals(15, peptides.size());

        for (int i=0; i<peptides.size() ; i++) {

            Assert.assertEquals(expectedPeptides.get(i), peptides.get(i));
        }
    }

    @Test
    public void testWithModsLimitSiteMax() {

        List<Peptide> expectedPeptides = Lists.newArrayList();

        expectedPeptides.add(Peptide.parse("(C2H2O)_P(H2O)EPTIDE"));
        expectedPeptides.add(Peptide.parse("(C2H2O)_PEP(H2O)TIDE"));
        expectedPeptides.add(Peptide.parse("(C2H2O)_PEPTIDE"));
        expectedPeptides.add(Peptide.parse("(C2H2O)_PEPTIDE_(OH)"));
        expectedPeptides.add(Peptide.parse("P(H2O)EP(H2O)TIDE"));
        expectedPeptides.add(Peptide.parse("P(H2O)EPTIDE"));
        expectedPeptides.add(Peptide.parse("P(H2O)EPTIDE_(OH)"));
        expectedPeptides.add(Peptide.parse("PEP(H2O)TIDE"));
        expectedPeptides.add(Peptide.parse("PEP(H2O)TIDE_(OH)"));
        expectedPeptides.add(Peptide.parse("PEPTIDE_(OH)"));

        ModifiedPeptideFactory factory = new ModifiedPeptideFactory.Builder()
                .addTarget(EnumSet.of(AminoAcid.P), Modification.parseModification("H2O"))
                .addTarget(ModAttachment.N_TERM, Modification.parseModification("C2H2O"))
                .addTarget(ModAttachment.C_TERM, Modification.parseModification("OH"))
                .limit(2).build();

        List<Peptide> peptides = factory.createVarModPeptides(Peptide.parse("PEPTIDE"));
        Collections.sort(peptides, new Comparator<Peptide>() {
            @Override
            public int compare(Peptide p1, Peptide p2) {

                return p1.toString().compareTo(p2.toString());
            }
        });

        Assert.assertEquals(10, peptides.size());

        for (int i=0; i<peptides.size() ; i++) {

            Assert.assertEquals(expectedPeptides.get(i), peptides.get(i));
        }
    }

    @Test
    public void testSideChainModOnly() {

        ModifiedPeptideFactory factory = new ModifiedPeptideFactory.Builder()
                .addTarget(EnumSet.of(AminoAcid.T, AminoAcid.Y, AminoAcid.S), ModAttachment.SIDE_CHAIN,
                        Modification.parseModification("PO3H-1"))
                .build();

        List<Peptide> peptides = factory.createVarModPeptides(Peptide.parse("SERYTE"));

        Assert.assertEquals(7, peptides.size());

        for (Peptide peptide : peptides) {

            for (int i : peptide.getModificationIndexes(ModAttachment.all)) {

                Assert.assertTrue(peptide.getModificationsAt(i, ModAttachment.termSet).isEmpty());
            }
        }
    }

    @Test
    public void testTermModOnly() {

        ModifiedPeptideFactory factory = new ModifiedPeptideFactory.Builder()
                .addTarget(EnumSet.of(AminoAcid.G, AminoAcid.A, AminoAcid.S, AminoAcid.T), ModAttachment.N_TERM,
                        Modification.parseModification("CH2CO"))
                .build();

        List<Peptide> peptides = factory.createVarModPeptides(Peptide.parse("SERYTE"));

        Assert.assertEquals(1, peptides.size());

        for (Peptide peptide : peptides) {

            for (int i : peptide.getModificationIndexes(ModAttachment.all)) {

                Assert.assertTrue(peptide.getModificationsAt(i, ModAttachment.sideChainSet).isEmpty());
            }
        }
    }
}
