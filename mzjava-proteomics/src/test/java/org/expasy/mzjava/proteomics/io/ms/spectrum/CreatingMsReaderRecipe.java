package org.expasy.mzjava.proteomics.io.ms.spectrum;

import org.expasy.mzjava.core.io.IterativeReader;
import org.expasy.mzjava.core.io.ms.spectrum.MgfReader;
import org.expasy.mzjava.core.io.ms.spectrum.MzxmlReader;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessorChain;
import org.expasy.mzjava.core.ms.peaklist.peakfilter.ThresholdFilter;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.EnumSet;

// # $RECIPE$ $NUMBER$ - Reading Mass Spectrometry data #
//
// ## $PROBLEM$ ##
// You want to read MS data iteratively from a character stream.
//
// ## $SOLUTION$ ##
//
// Use our *IterativeReader* implementing *AbstractMsReader*s to go through each parsed *Spectrum* using the "foreach" statement.
//
// In this recipe we will take as an example the MGF and MZXML readers.

public class CreatingMsReaderRecipe {

// ### Building MS Readers ###

// Our readers can be associated with a *File*
    @Test
    public void newMgfReaderFromFile() throws IOException {

        //<SNIP>
        String mgfFilename = "mgf_test.mgf";
        //<SKIP>
        mgfFilename = getClass().getResource(mgfFilename).getFile();
        //</SKIP>
        MgfReader reader = new MgfReader(new File(mgfFilename), PeakList.Precision.DOUBLE);
        //</SNIP>

        Assert.assertNotNull(reader);
    }

// or with a [java.io.Reader](http://docs.oracle.com/javase/6/docs/api/index.html?java/io/Reader.html)
    @Test
    public void newMgfReaderFromReader() throws IOException, URISyntaxException {

        //<SNIP>
        Reader sr = new StringReader("BEGIN IONS\n" +
                "TITLE=B06-1151_p.00478.00478.2\n" +
                "PEPMASS=476.23272705078125\n" +
                "CHARGE=2\n" +
                "135.032\t1.376\n" +
                "146.042\t5.997\n" +
                "158.052\t25.335\n" +
                "175.047\t202.932\n" +
                "186.047\t27.338\n" +
                "203.140\t60.595\n" +
                "213.230\t5.732\n" +
                "221.182\t1.083\n" +
                "231.047\t1159.671...");

        MgfReader reader = new MgfReader(sr, new URI("http://somewhere.ch/mymsdata.mgf") , PeakList.Precision.DOUBLE);
        //</SNIP>

        Assert.assertNotNull(reader);
    }

// We provide also some static factory methods that create the proper reader given the file type
    @Test
    public void newMgfReaderFromFactory() throws Exception {

        String mgfFilename = getClass().getResource("mgf_test.mgf").getFile();

        //<SNIP>
        IterativeReader reader =
                MsReaderFactory.newMsnSpectraReader(new File(mgfFilename), PeakList.Precision.DOUBLE);
        //</SNIP>

        Assert.assertNotNull(reader);
    }

// Some MS formats gives some informations that could be inconsistent with scanned spectra.
// As an example, mzxml format provides <scan> attributes like "peaksCount" or "totIonCurrent" that can
// conflict with the ones extracted from the decoded <peak> spectrum.

// *MzxmlReader* provides a system to control those consistency checks:
    @Test
    public void newMzxmlStrictReader() throws IOException, URISyntaxException {

        //<SNIP>
        String mzxmlFilename = "mzxml_test.mzXML";
        //<SKIP>
        mzxmlFilename = getClass().getResource(mzxmlFilename).getFile();
        //</SKIP>

        // default constructor strictly checks for inconsistencies
        MzxmlReader reader = new MzxmlReader(new File(mzxmlFilename), PeakList.Precision.DOUBLE);

        // we can then add/remove ConsistencyCheck
        reader.removeConsistencyChecks(EnumSet.of(MzxmlReader.ConsistencyCheck.TOTAL_ION_CURRENT));
        //</SNIP>

        Assert.assertNotNull(reader);
        Assert.assertEquals(3, reader.getExpectedScanCount());
    }

// MzxmlReader also provides static factory methods:
    @Test
    public void newMzxmlFactoryReader() throws IOException, URISyntaxException {

        String mzxmlFilename = getClass().getResource("mzxml_test.mzXML").getFile();

        //<SNIP>
        MzxmlReader tolerantReader = MzxmlReader.newTolerantReader(new File(mzxmlFilename), PeakList.Precision.DOUBLE);
        MzxmlReader strictReader = MzxmlReader.newStrictReader(new File(mzxmlFilename), PeakList.Precision.DOUBLE);
        //</SNIP>

        Assert.assertNotNull(tolerantReader);
        Assert.assertNotNull(strictReader);
    }

// Defining processing for future reading action
    @Test
    public void newFilterReader() throws IOException, URISyntaxException {

        String mzxmlFilename = getClass().getResource("mzxml_test.mzXML").getFile();

        //<SNIP>
        // this filter will no retain peaks with intensity of 0
        MzxmlReader reader = MzxmlReader.newStrictReader(new File(mzxmlFilename), PeakList.Precision.FLOAT, new PeakProcessorChain<>()
                .add(new ThresholdFilter<>(0, ThresholdFilter.Inequality.GREATER)));
        //</SNIP>

        Assert.assertNotNull(reader);
    }

// ### Reading MsnSpectrum ###

// Reading is done iteratively until there is no more spectra to get.
// Here is a snip example with MgfReader
    @Test
    public void read() throws IOException {

        String mgfFilename = getClass().getResource("mgf_test.mgf").getFile();

        //<SNIP>
        MgfReader reader = new MgfReader(new File(mgfFilename), PeakList.Precision.FLOAT);

        // hasNext() returns true if there is more spectrum to read
        while (reader.hasNext()) {

            // next() returns the next spectrum or throws an IOException is something went wrong
            MsnSpectrum spectrum = reader.next();

            // do some stuff with your spectrum
            Assert.assertTrue(spectrum.size()>0);
        }

        reader.close();
        //</SNIP>
    }
// ## $DISCUSSION$ ##

// ## $RELATED$ ##
// For an advanced use of *MgfReader* see also $CreatingMgfReaderTuningRecipe$
}