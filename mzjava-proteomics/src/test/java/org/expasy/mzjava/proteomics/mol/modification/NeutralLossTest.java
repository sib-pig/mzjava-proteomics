package org.expasy.mzjava.proteomics.mol.modification;

import com.google.common.collect.Sets;
import gnu.trove.map.TObjectIntMap;
import gnu.trove.map.hash.TObjectIntHashMap;
import org.expasy.mzjava.core.mol.Atom;
import org.expasy.mzjava.core.mol.PeriodicTable;
import org.junit.Assert;
import org.junit.Test;

public class NeutralLossTest {

    @Test(expected = UnsupportedOperationException.class)
    public void testGetSites() throws Exception {

        TObjectIntMap<Atom> atomCounterMap = new TObjectIntHashMap<Atom>(1);
        atomCounterMap.put(PeriodicTable.O, 1);
        atomCounterMap.put(PeriodicTable.H, 2);
        NeutralLoss neutralLoss = new NeutralLoss(atomCounterMap, 0, Sets.newHashSet("c-term", "protein-c-term", "K"));

        neutralLoss.getSites().clear();
    }

    @Test
    public void testEquals() throws Exception {

        TObjectIntMap<Atom> atomCounterMap = new TObjectIntHashMap<Atom>(1);
        atomCounterMap.put(PeriodicTable.O, 1);
        atomCounterMap.put(PeriodicTable.H, 2);
        NeutralLoss same1 = new NeutralLoss(atomCounterMap, 0, Sets.newHashSet("c-term", "protein-c-term", "K"));
        NeutralLoss same2 = new NeutralLoss(atomCounterMap, 0, Sets.newHashSet("c-term", "protein-c-term", "K"));
        NeutralLoss different = new NeutralLoss(atomCounterMap, 0, Sets.newHashSet("c-term", "K"));

        Assert.assertEquals(true, same1.equals(same2));
        Assert.assertEquals(true, same2.equals(same1));
        Assert.assertEquals(false, different.equals(same1));
    }

    @Test
    public void testHashCode() throws Exception {

        TObjectIntMap<Atom> atomCounterMap = new TObjectIntHashMap<Atom>(1);
        atomCounterMap.put(PeriodicTable.O, 1);
        atomCounterMap.put(PeriodicTable.H, 2);
        NeutralLoss same1 = new NeutralLoss(atomCounterMap, 0, Sets.newHashSet("c-term", "protein-c-term", "K"));
        NeutralLoss same2 = new NeutralLoss(atomCounterMap, 0, Sets.newHashSet("c-term", "protein-c-term", "K"));
        NeutralLoss different = new NeutralLoss(atomCounterMap, 0, Sets.newHashSet("c-term", "K"));

        Assert.assertEquals(same1.hashCode(), same2.hashCode());
        Assert.assertNotEquals(different.hashCode(), same1.hashCode());
    }
}