package org.expasy.mzjava.proteomics.io.ms.ident;

import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.core.mol.MassCalculator;
import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.proteomics.ms.ident.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @author Thibault Robin
 * @version 1.0
 */
public class BiomlPsmReaderTest {
    private final static String DECOY_TAG = "###REV###";

    final List<PeptideMatch> peptideMatches = new ArrayList<>();
    final List<SpectrumIdentifier> identifiers = new ArrayList<>();

    final List<String> modificationStrings =
            Lists.newArrayList("Oxidation:O", "Carbamidomethyl:C2H3NO", "Phospho:HO3P", "Deamidated:H-1N-1O", "Ammonia-loss:H-3N-1", "Acetyl:C2H2O", "Methyl:CH2", "Dimethyl:C2H4", "Trimethyl:C3H6", "Amidated:H-1", "Glu->pyro-Glu:H-2O-1", "Propionyl:C3H4O", "Pyro-carbamidomethyl:C2O");

    ModListModMatchResolver modMatchResolver;
    ;

    PSMReaderCallback callback = new PSMReaderCallback() {
        @Override
        public void resultRead(SpectrumIdentifier id, PeptideMatch pm) {
            peptideMatches.add(pm);
            identifiers.add(id);
        }
    };


    @Before
    public void setUp() throws Exception {

        List<Modification> modifications = new ArrayList<>();
        for (String modString : modificationStrings)
            modifications.add(Modification.parseModification(modString));

        modMatchResolver = new ModListModMatchResolver(new AbsoluteTolerance(0.015), modifications);


        InputStream inputStream = new FileInputStream(new File(getClass().getResource("bioml.xml").getFile()));
        BiomlPsmReader reader = new BiomlPsmReader(DECOY_TAG,modMatchResolver);
        reader.parse(inputStream, callback);
    }

    @Test
    public void testParse1() throws Exception {

        Assert.assertEquals(identifiers.size(), peptideMatches.size());
        Assert.assertEquals(5, peptideMatches.size());

        Assert.assertEquals(1, peptideMatches.get(0).getProteinMatches().size());
        Assert.assertEquals(1, peptideMatches.get(1).getProteinMatches().size());
        Assert.assertEquals(1, peptideMatches.get(2).getProteinMatches().size());
        Assert.assertEquals(1, peptideMatches.get(3).getProteinMatches().size());
        Assert.assertEquals(2, peptideMatches.get(4).getProteinMatches().size());

        SpectrumIdentifier identifier = new SpectrumIdentifier("00524_A01_P003819_B0A_A00_R2.8492.8492.3 File:\"00524_A01_P003819_B0A_A00_R2.raw\", NativeID:\"controllerType=0 controllerNumber=1 scan=8492\" RTINSECONDS=2486.1217 ");
        identifier.setAssumedCharge(3);
        identifier.setName("2577.spectrum");
        identifier.setIndex(2577);
        identifier.setPrecursorNeutralMass(1837.96 - MassCalculator.PROTON_MASS);

        Assert.assertEquals(identifiers.get(0), identifier);

        PeptideMatch peptideMatch = new PeptideMatch("HGVQELEIELQSQLSK");
        peptideMatch.setRank(Optional.of(1));
        peptideMatch.setNumMissedCleavages(Optional.of(0));
        peptideMatch.addScore("hyperscore", 38.8);
        peptideMatch.addScore("nextscore", 16.4);
        peptideMatch.addScore("deltascore", -0.0016);
        peptideMatch.addScore("evalue", 6.6e-08);

        Assert.assertEquals(peptideMatches.get(0), peptideMatch);
        Assert.assertEquals(0, peptideMatches.get(0).getModificationCount());

        PeptideProteinMatch proteinMatch = new PeptideProteinMatch(
                "mz|P35527|OS=Homo sapiens GN=uc002hxe.4 CR=17 ST=39722094 EN=39728244",
                Optional.of("../NCI-60/CCRF-CEM_Database_Peptides.fasta"),
                Optional.<String>absent(),
                Optional.<String>absent(),
                0,
                15,
                PeptideProteinMatch.HitType.TARGET
        );

        Assert.assertEquals(peptideMatches.get(0).getProteinMatches().get(0), proteinMatch);
    }

    @Test
    public void testParse2() throws Exception {

        SpectrumIdentifier identifier = new SpectrumIdentifier("00524_A01_P003819_B0A_A00_R2.2314.2314.3 File:\"00524_A01_P003819_B0A_A00_R2.raw\", NativeID:\"controllerType=0 controllerNumber=1 scan=2314\" RTINSECONDS=812.4655 ");
        identifier.setAssumedCharge(3);
        identifier.setName("85.spectrum");
        identifier.setIndex(85);
        identifier.setPrecursorNeutralMass(1791.72 - MassCalculator.PROTON_MASS);

        Assert.assertEquals(identifiers.get(1), identifier);

        PeptideMatch peptideMatch = new PeptideMatch("GGSGGSYGGGGSGGGYGGGSGSR");
        peptideMatch.setRank(Optional.of(1));
        peptideMatch.setNumMissedCleavages(Optional.of(0));
        peptideMatch.addScore("hyperscore", 35.6);
        peptideMatch.addScore("nextscore", 20.3);
        peptideMatch.addScore("deltascore", -0.0030);
        peptideMatch.addScore("evalue", 1.4e-06);

        Assert.assertEquals(peptideMatches.get(1), peptideMatch);

        PeptideProteinMatch proteinMatch = new PeptideProteinMatch(
                "mz|P35527|OS=Homo sapiens GN=uc002hxe.4 CR=17 ST=39722094 EN=39728244",
                Optional.of("../NCI-60/CCRF-CEM_Database_Peptides.fasta"),
                Optional.<String>absent(),
                Optional.<String>absent(),
                0,
                22,
                PeptideProteinMatch.HitType.TARGET
        );

        Assert.assertEquals(peptideMatches.get(1).getProteinMatches().get(0), proteinMatch);
    }

    @Test
    public void testParse3() throws Exception {

        SpectrumIdentifier identifier = new SpectrumIdentifier("00524_A01_P003819_B0A_A00_R2.11880.11880.4 File:\"00524_A01_P003819_B0A_A00_R2.raw\", NativeID:\"controllerType=0 controllerNumber=1 scan=11880\" RTINSECONDS=3459.067 ");
        identifier.setAssumedCharge(4);
        identifier.setName("3295.spectrum");
        identifier.setIndex(3295);
        identifier.setPrecursorNeutralMass(2240.13 - MassCalculator.PROTON_MASS);


        Assert.assertEquals(identifiers.get(2), identifier);

        PeptideMatch peptideMatch = new PeptideMatch("ADLEMQIESLTEELAYLKK");
        peptideMatch.setRank(Optional.of(1));
        peptideMatch.setNumMissedCleavages(Optional.of(0));
        peptideMatch.addScore("hyperscore", 38.9);
        peptideMatch.addScore("nextscore", 16.0);
        peptideMatch.addScore("deltascore", -0.0029);
        peptideMatch.addScore("evalue", 1.3e-08);
        peptideMatch.addModificationMatch(4, new Modification("Oxidation", Composition.parseComposition("O")));

        Assert.assertEquals(peptideMatches.get(2).getModificationCount(), peptideMatch.getModificationCount());
        Assert.assertEquals(peptideMatches.get(2).getModifications(4, ModAttachment.sideChainSet).get(0).getMassShift(), peptideMatch.getModifications(4, ModAttachment.sideChainSet).get(0).getMassShift(),0.00001);

        PeptideProteinMatch proteinMatch = new PeptideProteinMatch(
                "###REV###mz|NP_000412|OS=Homo sapiens GN=uc002hvi.3 CR=17 ST=38974369 EN=38978837",
                Optional.of("../NCI-60/CCRF-CEM_Database_Pddeptides.fasta"),
                Optional.<String>absent(),
                Optional.<String>absent(),
                0,
                18,
                PeptideProteinMatch.HitType.DECOY
        );

        Assert.assertEquals(peptideMatches.get(2).getProteinMatches().get(0), proteinMatch);
    }

    @Test
    public void testParse4() throws Exception {
        SpectrumIdentifier identifier = new SpectrumIdentifier("00524_A01_P003819_B0A_A00_R2.1737.1737.3 File:\"00524_A01_P003819_B0A_A00_R2.raw\", NativeID:\"controllerType=0 controllerNumber=1 scan=1737\" RTINSECONDS=632.114 ");
        identifier.setAssumedCharge(3);
        identifier.setName("9.spectrum");
        identifier.setIndex(9);
        identifier.setPrecursorNeutralMass(1947.9 - MassCalculator.PROTON_MASS);


        Assert.assertEquals(identifiers.get(3), identifier);

        PeptideMatch peptideMatch = new PeptideMatch("QSLGHGQHGSGSGQSPSPSR");
        peptideMatch.setRank(Optional.of(1));
        peptideMatch.setNumMissedCleavages(Optional.of(0));
        peptideMatch.addScore("hyperscore", 50.1);
        peptideMatch.addScore("nextscore", 14.9);
        peptideMatch.addScore("deltascore", -0.0048);
        peptideMatch.addScore("evalue", 3.1e-09);

        Assert.assertEquals(peptideMatches.get(3), peptideMatch);

        PeptideProteinMatch proteinMatch = new PeptideProteinMatch(
                "mz|Q86YZ3|OS=Homo sapiens GN=uc001ezt.2 CR=1 ST=152184552 EN=152195729",
                Optional.of("../NCI-60/CCRF-CEM_Database_Peptides.fasta"),
                Optional.<String>absent(),
                Optional.<String>absent(),
                0,
                19,
                PeptideProteinMatch.HitType.TARGET
        );

        Assert.assertEquals(peptideMatches.get(3).getProteinMatches().get(0), proteinMatch);
    }

    @Test
    public void testParse5() throws Exception {

        SpectrumIdentifier identifier = new SpectrumIdentifier("00524_A01_P003819_B0A_A00_R2.3085.3085.3 File:\"00524_A01_P003819_B0A_A00_R2.raw\", NativeID:\"controllerType=0 controllerNumber=1 scan=3085\" RTINSECONDS=1032.1499 ");
        identifier.setAssumedCharge(3);
        identifier.setName("307.spectrum");
        identifier.setIndex(307);
        identifier.setPrecursorNeutralMass(2383.95 - MassCalculator.PROTON_MASS);


        Assert.assertEquals(identifiers.get(4), identifier);

        PeptideMatch peptideMatch = new PeptideMatch("GGGGGGYGSGGSSYGSGGGSYGSGGGGGGGR");
        peptideMatch.setRank(Optional.of(1));
        peptideMatch.setNumMissedCleavages(Optional.of(0));
        peptideMatch.addScore("hyperscore", 64.3);
        peptideMatch.addScore("nextscore", 20.8);
        peptideMatch.addScore("deltascore", -0.0040);
        peptideMatch.addScore("evalue", 9.1e-14);

        Assert.assertEquals(peptideMatches.get(4), peptideMatch);

        ArrayList<PeptideProteinMatch> matches = new ArrayList<>();
        matches.add(new PeptideProteinMatch(
                "mz|P04264|OS=Homo sapiens GN=uc001sau.1 CR=12 ST=53068520 EN=53074132",
                Optional.of("../NCI-60/CCRF-CEM_Database_Peptides.fasta"),
                Optional.<String>absent(),
                Optional.<String>absent(),
                0,
                30,
                PeptideProteinMatch.HitType.TARGET
        ));
        matches.add(new PeptideProteinMatch(
                "mz|P04264|OS=Homo sapiens GN=uc001sav.1 CR=12 ST=53068520 EN=53074132",
                Optional.of("../NCI-60/CCRF-CEM_Database_Peptides.fasta"),
                Optional.<String>absent(),
                Optional.<String>absent(),
                0,
                30,
                PeptideProteinMatch.HitType.TARGET
        ));

        Assert.assertEquals(peptideMatches.get(4).getProteinMatches(), matches);
    }


    @Test
    public void testParse6() throws Exception {
        InputStream inputStream = new FileInputStream(new File(getClass().getResource("bioml2.xml").getFile()));

        final List<PeptideMatch> peptideMatches2 = new ArrayList<>();
        final List<SpectrumIdentifier> identifiers2 = new ArrayList<>();
        ;

        PSMReaderCallback callback2 = new PSMReaderCallback() {
            @Override
            public void resultRead(SpectrumIdentifier id, PeptideMatch pm) {
                peptideMatches2.add(pm);
                identifiers2.add(id);
            }
        };

        BiomlPsmReader reader = new BiomlPsmReader("decoy_",modMatchResolver);
        reader.parse(inputStream, callback2);

        SpectrumIdentifier identifier = new SpectrumIdentifier("20161103_06_Y1.7490.7490.2 File:\"20161103_06_Y1.raw\", NativeID:\"controllerType=0 controllerNumber=1 scan=7490\" RTINSECONDS=2342.714106 ");
        identifier.setAssumedCharge(2);
        identifier.setName("2229.spectrum");
        identifier.setIndex(2229);
        identifier.setPrecursorNeutralMass(1379.68 - MassCalculator.PROTON_MASS);


        Assert.assertEquals(identifier, identifiers2.get(0));

        PeptideMatch peptideMatch = new PeptideMatch("INDPTLDISLAK");
        peptideMatch.setRank(Optional.of(1));
        peptideMatch.setNumMissedCleavages(Optional.of(0));
        peptideMatch.addScore("hyperscore", 32.4);
        peptideMatch.addScore("nextscore", 23.4);
        peptideMatch.addScore("deltascore", -0.0040);
        peptideMatch.addScore("evalue", 2.5e-03);
        peptideMatch.addModificationMatch(4, 79.96633);

        Assert.assertEquals(3, peptideMatches2.size());
        Assert.assertEquals(2, peptideMatches2.get(0).getProteinMatches().size());
        Assert.assertEquals(2, peptideMatches2.get(1).getProteinMatches().size());

        PeptideProteinMatch proteinMatch1 = new PeptideProteinMatch(
                "decoy_13",
                Optional.of("/home/mullerm/data/NCCR/20150129_SwissProt_Scere_decoy.fasta"),
                Optional.of("K"),
                Optional.of("I"),
                32,
                43,
                PeptideProteinMatch.HitType.DECOY
        );

        PeptideProteinMatch proteinMatch2 = new PeptideProteinMatch(
                "decoy_14",
                Optional.of("/home/mullerm/data/NCCR/20150129_SwissProt_Scere_decoy.fasta"),
                Optional.of("K"),
                Optional.of("I"),
                32,
                43,
                PeptideProteinMatch.HitType.DECOY
        );

        Assert.assertEquals(peptideMatches2.get(0).getProteinMatches().get(0), proteinMatch1);
        Assert.assertEquals(peptideMatches2.get(0).getProteinMatches().get(1), proteinMatch2);
        Assert.assertEquals(peptideMatches2.get(1).getProteinMatches().get(0), proteinMatch1);
        Assert.assertEquals(peptideMatches2.get(1).getProteinMatches().get(1), proteinMatch2);

        Pattern p = Pattern.compile("decoy_");

        Assert.assertTrue(peptideMatches2.get(0).containsOnlyProteinMatch(p));

        Assert.assertEquals(1, peptideMatches2.get(0).getModificationCount());
        Assert.assertEquals(1, peptideMatches2.get(1).getModificationCount());
        Assert.assertEquals(2, peptideMatches2.get(2).getModificationCount());

        Assert.assertEquals(0, peptideMatches2.get(2).getModifications(ModAttachment.nTermSet).size());
        Assert.assertEquals(0, peptideMatches2.get(2).getModifications(ModAttachment.cTermSet).size());
        Assert.assertEquals(2, peptideMatches2.get(2).getModifications(ModAttachment.sideChainSet).size());

        List<ModificationMatch> modificationMatches = peptideMatches2.get(2).getModifications(ModAttachment.sideChainSet);
        Assert.assertEquals(0, modificationMatches.get(0).getPosition());
        Assert.assertEquals(3, modificationMatches.get(1).getPosition());
        Assert.assertEquals(57.02200, modificationMatches.get(0).getMassShift(), 0.000001);
        Assert.assertEquals(57.02200, modificationMatches.get(1).getMassShift(), 0.000001);
        Assert.assertEquals("C", modificationMatches.get(0).getResidue().getSymbol());
        Assert.assertEquals("C", modificationMatches.get(0).getResidue().getSymbol());
    }

}