/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.mol;

import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class PeptideBuilderTest {

    @Test
    public void addModificationToAll() throws Exception {

        Modification modif = mockMod("mod1", 10);
        Peptide peptide = new PeptideBuilder("PEPTIDE")
                .addModificationToAll(modif, AminoAcid.P)
                .build();

        Assert.assertTrue(peptide.hasModificationAt(0));
        Assert.assertTrue(peptide.hasModificationAt(2));
        assertEquals(modif, peptide.getModificationsAt(0, ModAttachment.all).get(0));
        assertEquals(modif, peptide.getModificationsAt(2, ModAttachment.all).get(0));
    }

    @Test
    public void testAdd2Modifs() throws Exception {


        Modification modif1 = mockMod("mod1", 10);
        Modification modif2 = mockMod("mod2", 100);

        Peptide peptide = new PeptideBuilder("PEPTIDE")
                .addModificationToAll(modif1, AminoAcid.P)
                .addModificationToAll(modif2, AminoAcid.P)
                .addModificationToAll(modif2, AminoAcid.E)
                .build();

        Assert.assertTrue(peptide.hasModificationAt(0));
        Assert.assertTrue(peptide.hasModificationAt(1));
        Assert.assertTrue(peptide.hasModificationAt(2));
        Assert.assertTrue(peptide.hasModificationAt(6));

        assertEquals(2, peptide.getModificationsAt(0, ModAttachment.all).size());
        assertEquals(2, peptide.getModificationsAt(2, ModAttachment.all).size());
        assertEquals(1, peptide.getModificationsAt(1, ModAttachment.all).size());
        assertEquals(1, peptide.getModificationsAt(6, ModAttachment.all).size());

        assertEquals(Arrays.asList(modif1, modif2), peptide.getModificationsAt(0, ModAttachment.all));
        assertEquals(Arrays.asList(modif2), peptide.getModificationsAt(1, ModAttachment.all));
        assertEquals(Arrays.asList(modif1, modif2), peptide.getModificationsAt(2, ModAttachment.all));
        assertEquals(Arrays.asList(modif2), peptide.getModificationsAt(6, ModAttachment.all));
    }

    @Test
    public void testAdd2Modifs2() throws Exception {

        Modification modif1 = mockMod("mod1", 10);
        Modification modif2 = mockMod("mod1", 100);

        Set<Modification> fixedModifs = new HashSet<Modification>();
        fixedModifs.add(modif1);
        fixedModifs.add(modif2);

        Peptide peptide = new PeptideBuilder("PEPTIDE")
                .addModificationToAll(fixedModifs, AminoAcid.P)
                .build();

        Assert.assertTrue(peptide.hasModificationAt(0));
        Assert.assertTrue(peptide.hasModificationAt(2));

        assertEquals(2, peptide.getModificationsAt(0, ModAttachment.all).size());
        assertEquals(2, peptide.getModificationsAt(2, ModAttachment.all).size());

        assertEquals(110.0, peptide.getModificationsAt(0, ModAttachment.all).getMolecularMass(), 0.01);
    }

    @Test
    public void testBuildSeq() throws Exception {

        PeptideBuilder builder = new PeptideBuilder();

        builder.add(AminoAcid.C).add(AminoAcid.E).add(AminoAcid.V).add(AminoAcid.K);

        Peptide peptide = builder.build();

        assertEquals(4, peptide.size());
        assertEquals(AminoAcid.C, peptide.getSymbol(0));
        assertEquals(AminoAcid.E, peptide.getSymbol(1));
        assertEquals(AminoAcid.V, peptide.getSymbol(2));
        assertEquals(AminoAcid.K, peptide.getSymbol(3));
    }

    @Test
    public void testBuildSeq2() throws Exception {

        PeptideBuilder builder = new PeptideBuilder("PEPT");

        builder.parseAndAdd("PEPTIDE");

        Peptide peptide = builder.build();

        assertEquals(11, peptide.size());
        assertEquals(AminoAcid.P, peptide.getSymbol(0));
        assertEquals(AminoAcid.E, peptide.getSymbol(1));
        assertEquals(AminoAcid.P, peptide.getSymbol(2));
        assertEquals(AminoAcid.T, peptide.getSymbol(3));
        assertEquals(AminoAcid.P, peptide.getSymbol(4));
        assertEquals(AminoAcid.E, peptide.getSymbol(5));
        assertEquals(AminoAcid.P, peptide.getSymbol(6));
        assertEquals(AminoAcid.T, peptide.getSymbol(7));
        assertEquals(AminoAcid.I, peptide.getSymbol(8));
        assertEquals(AminoAcid.D, peptide.getSymbol(9));
        assertEquals(AminoAcid.E, peptide.getSymbol(10));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testBuildIllegalSeq() throws Exception {

        PeptideBuilder builder = new PeptideBuilder();

        builder.addModification(12, mockMod("a", 12));
        builder.add(AminoAcid.C).add(AminoAcid.E).add(AminoAcid.V).add(AminoAcid.K);

        builder.build();
    }

    @Test
    public void testCopyConstructor() throws Exception {

        final AminoAcid[] residues = {AminoAcid.A, AminoAcid.B, AminoAcid.C, AminoAcid.D, AminoAcid.E, AminoAcid.F};
        PeptideBuilder builder = new PeptideBuilder(residues, 1, 2);

        Peptide peptide = builder.build();
        Assert.assertEquals(1, peptide.size());
        Assert.assertEquals(AminoAcid.B, peptide.getSymbol(0));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testCopyConstructorIndexCheck1() throws Exception {

        final AminoAcid[] residues = {AminoAcid.A, AminoAcid.B, AminoAcid.C, AminoAcid.D, AminoAcid.E, AminoAcid.F};
        PeptideBuilder builder = new PeptideBuilder(residues, -1, 2);

        builder.build();
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testCopyConstructorIndexCheck2() throws Exception {

        final AminoAcid[] residues = {AminoAcid.A, AminoAcid.B, AminoAcid.C, AminoAcid.D, AminoAcid.E, AminoAcid.F};
        PeptideBuilder builder = new PeptideBuilder(residues, 1, 12);

        builder.build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCopyConstructorIndexCheck3() throws Exception {

        final AminoAcid[] residues = {AminoAcid.A, AminoAcid.B, AminoAcid.C, AminoAcid.D, AminoAcid.E, AminoAcid.F};
        PeptideBuilder builder = new PeptideBuilder(residues, 2, 1);

        builder.build();
    }

    @Test
    public void testCopyConstructorIndexCheck4() throws Exception {

        final AminoAcid[] residues = {AminoAcid.A, AminoAcid.B, AminoAcid.C, AminoAcid.D, AminoAcid.E, AminoAcid.F};
        PeptideBuilder builder = new PeptideBuilder(residues, 0, 6);

        Peptide peptide = builder.build();
        Assert.assertEquals(6, peptide.size());
    }

    @Test
    public void testCopyConstructorIndexCheck5() throws Exception {

        final AminoAcid[] residues = {AminoAcid.A, AminoAcid.B, AminoAcid.C, AminoAcid.D, AminoAcid.E, AminoAcid.F};
        PeptideBuilder builder = new PeptideBuilder(residues, 0, 1);

        Peptide peptide = builder.build();
        Assert.assertEquals(1, peptide.size());
        Assert.assertEquals(AminoAcid.A, peptide.getSymbol(0));
    }

    @Test
    public void testCopyConstructorIndexCheck6() throws Exception {

        final AminoAcid[] residues = {AminoAcid.A, AminoAcid.B, AminoAcid.C, AminoAcid.D, AminoAcid.E, AminoAcid.F};
        PeptideBuilder builder = new PeptideBuilder(residues, 5, 6);

        Peptide peptide = builder.build();
        Assert.assertEquals(1, peptide.size());
        Assert.assertEquals(AminoAcid.F, peptide.getSymbol(0));
    }

    @Test(expected = IllegalStateException.class)
    public void testCopyConstructorIndexCheck7() throws Exception {

        final AminoAcid[] residues = {AminoAcid.A, AminoAcid.B, AminoAcid.C, AminoAcid.D, AminoAcid.E, AminoAcid.F};
        PeptideBuilder builder = new PeptideBuilder(residues, 1, 1);

        builder.build();
    }

    private Modification mockMod(String label, double mw) {

        Modification mod = mock(Modification.class);
        when(mod.getLabel()).thenReturn(label);
        when(mod.getMolecularMass()).thenReturn(mw);
        when(mod.toString()).thenReturn(label);

        return mod;
    }
}
