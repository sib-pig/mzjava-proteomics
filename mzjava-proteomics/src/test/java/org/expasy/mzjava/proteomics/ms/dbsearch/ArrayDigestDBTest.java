/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.ms.dbsearch;

import org.expasy.mzjava.core.io.IterativeReaders;
import org.expasy.mzjava.proteomics.mol.Protein;
import org.expasy.mzjava.proteomics.mol.digest.Protease;
import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.util.*;
import java.util.logging.Handler;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import static org.mockito.Mockito.verify;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class ArrayDigestDBTest {

    @Test
    public void test() throws Exception {

        Protein protein = new Protein("ACC1", "MVDREQLVQKARLAEQAERYDDMAAAMKSVTELNEALSNEERNLLSVAYKNVVGARRSSW" +
                "RVISSIEQKTSADGNEKKMEMVRAYREKIEKELETVCRDVLNLLDNFLIKNCNETQHESK" +
                "VFYLKMKGDYYRYLAEVATGEKRVGVVESSEKSYSEAHEISKEHMQPTHPIRLGLALNYS" +
                "VFYYEIQNAPEQACHLAKTAFDDAIAELDTLNEDSYKDSTLIMQLLRDNLTLWTSDQTDD" +
                "EGGETNN");

        ArrayDigestDB peptideSpectrumDB = new ArrayDigestDB.Builder(IterativeReaders.fromCollection(Arrays.asList(protein)), Protease.TRYPSIN, 2)
                .build();

        List<Digest> peptides = new ArrayList<Digest>(peptideSpectrumDB.getPeptides(905.0, new AbsoluteTolerance(3)));

        Collections.sort(peptides, new Comparator<Digest>() {

            @Override
            public int compare(Digest p1, Digest p2) {

                return Double.compare(p1.getMolecularMass(), p2.getMolecularMass());
            }
        });

        Iterator<Digest> iterator = peptides.iterator();

        Digest digest = iterator.next();

        Assert.assertEquals("VISSIEQK", digest.getPeptide().toString());

        Collection<String> proteins = digest.getProteinIds();
        Assert.assertEquals(1, proteins.size());

        digest = iterator.next();

        Assert.assertEquals("NLLSVAYK", digest.getPeptide().toString());
    }

    @Test(expected = IllegalStateException.class)
    public void testExceptionAmbiguousAA() throws Exception {

        Protein protein = new Protein("ACC1", "DQZDD");

        new ArrayDigestDB.Builder(IterativeReaders.fromCollection(Arrays.asList(protein)), Protease.TRYPSIN, 2)
                .build();
    }

    @Test
    public void testLogAmbiguousAA() throws Exception {

        Protein protein = new Protein("ACC1", "DQZDD");

        ArgumentCaptor<LogRecord> captorLoggingEvent = ArgumentCaptor.forClass(LogRecord.class);
        Handler mockAppender = Mockito.mock(Handler.class);
        final Logger logger = Logger.getLogger(ArrayDigestDB.class.getName());
        logger.setUseParentHandlers(false);
        logger.addHandler(mockAppender);

        new ArrayDigestDB.Builder(IterativeReaders.fromCollection(Arrays.asList(protein)), Protease.TRYPSIN, 2)
                .setAmbiguousAminoAcidAction(DigestDB.AmbiguousAminoAcidAction.LOG_AND_SKIP)
                .build();

        logger.removeHandler(mockAppender);
        logger.setUseParentHandlers(true);

        verify(mockAppender).publish(captorLoggingEvent.capture());
        LogRecord logRecord = captorLoggingEvent.getValue();

        Assert.assertEquals("Peptide DQZDD from protein ACC1 contains the following ambiguous amino acids [Z]", logRecord.getMessage());
    }
}
