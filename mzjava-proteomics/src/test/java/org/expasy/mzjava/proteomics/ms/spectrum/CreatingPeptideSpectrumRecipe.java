package org.expasy.mzjava.proteomics.ms.spectrum;

import com.google.common.collect.Lists;
import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.core.mol.Mass;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.ms.fragment.BackbonePeakGenerator;
import org.expasy.mzjava.proteomics.ms.fragment.PeptideNeutralLossPeakGenerator;
import org.expasy.mzjava.proteomics.ms.fragment.PeptidePeakGenerator;
import org.expasy.mzjava.proteomics.ms.fragment.PeptideFragmenter;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.proteomics.ms.spectrum.PeptideSpectrum;
import org.expasy.mzjava.proteomics.ms.spectrum.PepFragAnnotation;
import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

// # $RECIPE$ $NUMBER$ - Fragmenting a Peptide #
//
// ## $PROBLEM$ ##
// You want to fragment a *Peptide* in-silico
//
// ## $SOLUTION$ ##
//
// *PeptideFragmenter* will do the job

public class CreatingPeptideSpectrumRecipe {

// Here is how to create an instance of *PeptideFragmenter* that will produce fragments b and y
    @Test
    public void newPeptideFragmenter() {

        //<SNIP>
        PeptideFragmenter fragmenter = new PeptideFragmenter(EnumSet.of(IonType.b, IonType.y), PeakList.Precision.DOUBLE);
        //</SNIP>

        Assert.assertNotNull(fragmenter);
    }

// If you need to generate fragments with losses, you have to explicitly provide the peak generators to the constructor.
// Note that the previous code example was using a *BackbonePeakGenerator*.
    @Test
    public void newPeptideFragmenter2() {

        //<SNIP>
        Set<IonType> ionTypes = EnumSet.of(IonType.b, IonType.y, IonType.i, IonType.p);

        Mass waterLoss = Composition.parseComposition("H-2O-1");
        Mass ammoniumLoss = Composition.parseComposition("H-3N-1");

        List<PeptidePeakGenerator<PepFragAnnotation>> peakGenerators = Lists.newArrayList();

        peakGenerators.add(new BackbonePeakGenerator(ionTypes, 1));

        peakGenerators.add(new PeptideNeutralLossPeakGenerator(waterLoss,
                EnumSet.of(AminoAcid.S, AminoAcid.T, AminoAcid.D, AminoAcid.E), ionTypes, 1));

        peakGenerators.add(new PeptideNeutralLossPeakGenerator(ammoniumLoss,
                EnumSet.of(AminoAcid.Q, AminoAcid.K, AminoAcid.R, AminoAcid.N), ionTypes, 1));

        PeptideFragmenter fragmenter = new PeptideFragmenter(peakGenerators, PeakList.Precision.DOUBLE);
        //</SNIP>

        Assert.assertNotNull(fragmenter);
    }

// Generating n-1 x 4 (2 charges + 2 ion types) fragments.
    @Test
    public void fragmenting() throws ParseException {

        //<SNIP>
        PeptideFragmenter fragmenter = new PeptideFragmenter(EnumSet.of(IonType.b, IonType.y), PeakList.Precision.DOUBLE);

        Peptide peptide = Peptide.parse("GYDSPR");

        // generating peptide b and y fragments with charges +1 and +2
        PeptideSpectrum peptideSpectrum = fragmenter.fragment(peptide, 2);

        Assert.assertEquals(20, peptideSpectrum.size());
        //</SNIP>
    }

// ## $DISCUSSION$ ##

// ## $RELATED$ ##
// $CreatingPeptideRecipe$ to learn more about *Peptide*.
}