package org.expasy.mzjava.proteomics.io.ms.spectrum;

import com.google.common.base.Optional;
import org.expasy.mzjava.core.mol.NumericMass;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.proteomics.mol.PeptideBuilder;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.core.ms.spectrum.*;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;
import org.expasy.mzjava.proteomics.ms.spectrum.PepLibPeakAnnotation;
import org.expasy.mzjava.proteomics.ms.spectrum.PepFragAnnotation;
import org.expasy.mzjava.proteomics.ms.spectrum.PeptideSpectrum;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.StringWriter;
import java.io.Writer;
import java.text.ParseException;
import java.util.*;

/**
 * User: fnikitin
 * Date: 7/10/12
 * Time: 5:14 PM
 *
 * @author fnikitin
 * @author Oliver Horlacher
 */
public class SptxtWriterTest {

    public List<PeptideSpectrum> setup(PeakList.Precision prec) throws ParseException {

        List<PeptideSpectrum> spectra = new ArrayList<>();


        //Spectrum 0
        Peptide peptide = Peptide.parse("AAAAYIYEAVNK");
        PeptideSpectrum spectrum = new PeptideSpectrum(peptide, 1, prec);
        spectrum.setPrecursor(new Peak(1283.674, 6955, 1));
        spectrum.addSorted(new double[]{396.26 + 0.02, 414.26 - 0.17, 543.3 + -0.02}, new double[]{277.0, 216.0, 248.0});
        spectrum.addAnnotation(0, makeFragAnnotation(IonType.y, 1, 4, 0, new NumericMass(-35), peptide));
        spectrum.addAnnotation(1, makeFragAnnotation(IonType.y, 1, 4, 0, new NumericMass(-17), peptide));
        spectrum.addAnnotation(2, makeFragAnnotation(IonType.y, 1, 5, 0, new NumericMass(-17), peptide));
        spectrum.addAnnotation(2, makeFragAnnotation(IonType.b, 1, 6, 0, new NumericMass(-17), peptide));
        spectrum.setMsLevel(2);

        spectra.add(spectrum);

        //Spectrum 1
        peptide = new PeptideBuilder("AFNPVCGTDGVTYDNECLLC")
                .addModification(5, mockMod("Carbamidomethyl", 57.02146))
                .addModification(16, mockMod("Carbamidomethyl", 57.02146))
                .addModification(19, mockMod("Carbamidomethyl", 57.02146))
                .build();

        spectrum = new PeptideSpectrum(peptide, 2, prec);
        spectrum.setPrecursor(new Peak(1152.985, 6955, 2));
        spectrum.addSorted(new double[]{256.64 - 0.05}, new double[]{317.0});
        spectrum.addAnnotation(0, makeFragAnnotation(IonType.b, 2, 5, 0, new NumericMass(-17), peptide));
        spectrum.setMsLevel(2);

        spectra.add(spectrum);

        return spectra;
    }

    private static PepFragAnnotation makeFragAnnotation(IonType ionType, int charge, int residueNumber, int isotopeCount, NumericMass neutralLoss, Peptide peptide) {

        return new PepFragAnnotation.Builder(ionType, charge, peptide.subSequence(ionType, residueNumber)).addC13(isotopeCount).setNeutralLoss(neutralLoss).build();
    }

    @Test
    public void testWritingPeptideSpectrum() throws Exception {

        PeakList.Precision precision = PeakList.Precision.FLOAT;
        List<PeptideSpectrum> spectra = setup(precision);

        Writer sw = new StringWriter();

        SptxtWriter writer = new SptxtWriter(sw, precision);

        for (PeptideSpectrum spectrum : spectra) {

            writer.write(spectrum);
        }
        writer.close();

        String expectedString = "Name: AAAAYIYEAVNK/1\n" +
                "LibID: 1\n" +
                "MW: 1283.663\n" +
                "PrecursorMZ: 1283.663\n" +
                "FullName: AAAAYIYEAVNK/1\n" +
                "Comment: Mz_exact=1283.663 Mz_diff=0.011 Parent=1283.674 PrecursorIntensity=6955.000 TotalIonCurrent=741.000 Protein=unknown Mods=0\n" +
                "NumPeaks: 3\n" +
                "396.280\t10000.000\ty4-35/-0.02\n" +
                "414.090\t7797.834\ty4-17/0.17\n" +
                "543.280\t8953.069\ty5-17/0.02,b6-17/1.02\n\n" +
                "Name: AFNPVCGTDGVTYDNECLLC/2\n" +
                "LibID: 2\n" +
                "MW: 2305.964\n" +
                "PrecursorMZ: 1152.982\n" +
                "FullName: AFNPVC(Carbamidomethyl)GTDGVTYDNEC(Carbamidomethyl)LLC(Carbamidomethyl)/2\n" +
                "Comment: Mz_exact=1152.982 Mz_diff=0.003 Parent=1152.985 PrecursorIntensity=6955.000 TotalIonCurrent=317.000 Protein=unknown Mods=3/5,C,Carbamidomethyl/16,C,Carbamidomethyl/19,C,Carbamidomethyl\n" +
                "NumPeaks: 1\n" +
                "256.590\t10000.000\tb5-17^2/0.05\n\n";

        Assert.assertEquals(expectedString, sw.toString());
    }

    @Test
    public void testWritingAnnotLimit() throws Exception {

        PeakList.Precision precision = PeakList.Precision.FLOAT;
        List<PeptideSpectrum> spectra = setup(precision);

        Writer sw = new StringWriter();

        SptxtWriter writer = new SptxtWriter(sw, precision);
        writer.setMaxAnnotationToWrite(1);

        for (PeptideSpectrum spectrum : spectra) {

            writer.write(spectrum);
        }
        writer.close();

        String expectedString = "Name: AAAAYIYEAVNK/1\n" +
                "LibID: 1\n" +
                "MW: 1283.663\n" +
                "PrecursorMZ: 1283.663\n" +
                "FullName: AAAAYIYEAVNK/1\n" +
                "Comment: Mz_exact=1283.663 Mz_diff=0.011 Parent=1283.674 PrecursorIntensity=6955.000 TotalIonCurrent=741.000 Protein=unknown Mods=0\n" +
                "NumPeaks: 3\n" +
                "396.280\t10000.000\ty4-35/-0.02\n" +
                "414.090\t7797.834\ty4-17/0.17\n" +
                "543.280\t8953.069\ty5-17/0.02\n\n" +
                "Name: AFNPVCGTDGVTYDNECLLC/2\n" +
                "LibID: 2\n" +
                "MW: 2305.964\n" +
                "PrecursorMZ: 1152.982\n" +
                "FullName: AFNPVC(Carbamidomethyl)GTDGVTYDNEC(Carbamidomethyl)LLC(Carbamidomethyl)/2\n" +
                "Comment: Mz_exact=1152.982 Mz_diff=0.003 Parent=1152.985 PrecursorIntensity=6955.000 TotalIonCurrent=317.000 Protein=unknown Mods=3/5,C,Carbamidomethyl/16,C,Carbamidomethyl/19,C,Carbamidomethyl\n" +
                "NumPeaks: 1\n" +
                "256.590\t10000.000\tb5-17^2/0.05\n\n";
        Assert.assertEquals(expectedString, sw.toString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testChargeList() throws Exception {

        PeakList.Precision precision = PeakList.Precision.DOUBLE;
        PeptideSpectrum spectrum = new PeptideSpectrum(Peptide.parse("PEPTIDE"), 2, precision);
        spectrum.setMsLevel(2);
        spectrum.getPrecursor().setValues(562.876, 132, 2, 3);

        Writer sw = new StringWriter();

        SptxtWriter writer = new SptxtWriter(sw, precision);
        writer.setMaxAnnotationToWrite(1);

        writer.write(spectrum);
        writer.close();
    }

    @Test
    public void testWritingPeptideSpectrum2() throws Exception {

        Writer sw = new StringWriter();

        PeakList.Precision precision = PeakList.Precision.FLOAT;
        SptxtWriter writer = new SptxtWriter(sw, precision);


        //Spectrum 0
        Peptide peptide = Peptide.parse("AAAAYIYEAVNK");
        MsnSpectrum spectrum = new MsnSpectrum(precision);
        spectrum.setSpectrumIndex(0);
        spectrum.setPrecursor(new Peak(1283.674, 6955, 1));

        spectrum.addSorted(new double[]{396.26 + 0.02, 414.26 - 0.17, 543.3 - 0.02}, new double[]{277.0, 216.0, 248.0});
        spectrum.addAnnotation(0, makeFragAnnotation(IonType.y, 1, 4, 0, new NumericMass(-35), peptide));
        spectrum.addAnnotation(1, makeFragAnnotation(IonType.y, 1, 4, 0, new NumericMass(-17), peptide));
        spectrum.addAnnotation(2, makeFragAnnotation(IonType.y, 1, 5, 0, new NumericMass(-17), peptide));
        spectrum.addAnnotation(2, makeFragAnnotation(IonType.b, 1, 6, 0, new NumericMass(-17), peptide));
        spectrum.setMsLevel(2);

        writer.write(spectrum, peptide, Collections.<String>emptyList());

        //Spectrum 1
        peptide = new PeptideBuilder("AFNPVCGTDGVTYDNECLLC")
                .addModification(5, mockMod("Carbamidomethyl", 57.02146))
                .addModification(16, mockMod("Carbamidomethyl", 57.02146))
                .addModification(19, mockMod("Carbamidomethyl", 57.02146))
                .build();

        spectrum = new MsnSpectrum(precision);
        spectrum.setPrecursor(new Peak(1152.985, 6955, 2));
        spectrum.addSorted(new double[]{256.64 - 0.05}, new double[]{317.0});
        spectrum.addAnnotation(0, makeFragAnnotation(IonType.b, 2, 5, 0, new NumericMass(-17), peptide));
        spectrum.setMsLevel(2);

        writer.write(spectrum, peptide, Collections.<String>emptyList());

        writer.close();

        String expectedString = "Name: AAAAYIYEAVNK/1\n" +
                "LibID: 1\n" +
                "MW: 1283.663\n" +
                "PrecursorMZ: 1283.663\n" +
                "FullName: AAAAYIYEAVNK/1\n" +
                "Comment: Mz_exact=1283.663 Mz_diff=0.011 Parent=1283.674 PrecursorIntensity=6955.000 TotalIonCurrent=741.000 Protein=unknown Mods=0\n" +
                "NumPeaks: 3\n" +
                "396.280\t10000.000\ty4-35/-0.02\n" +
                "414.090\t7797.834\ty4-17/0.17\n" +
                "543.280\t8953.069\ty5-17/0.02,b6-17/1.02\n\n" +
                "Name: AFNPVCGTDGVTYDNECLLC/2\n" +
                "LibID: 2\n" +
                "MW: 2305.964\n" +
                "PrecursorMZ: 1152.982\n" +
                "FullName: AFNPVC(Carbamidomethyl)GTDGVTYDNEC(Carbamidomethyl)LLC(Carbamidomethyl)/2\n" +
                "Comment: Mz_exact=1152.982 Mz_diff=0.003 Parent=1152.985 PrecursorIntensity=6955.000 TotalIonCurrent=317.000 Protein=unknown Mods=3/5,C,Carbamidomethyl/16,C,Carbamidomethyl/19,C,Carbamidomethyl\n" +
                "NumPeaks: 1\n" +
                "256.590\t10000.000\tb5-17^2/0.05\n\n";

        Assert.assertEquals(expectedString, sw.toString());
    }

    @Test
    public void testWritingLibrarySpectrum() throws Exception {

        Writer sw = new StringWriter();

        PeakList.Precision precision = PeakList.Precision.FLOAT;
        SptxtWriter writer = new SptxtWriter(sw, precision);

        //Spectrum 0
        Peptide peptide = Peptide.parse("AAAAYIYEAVNK");
        PeptideConsensusSpectrum spectrum = new PeptideConsensusSpectrum(peptide, precision,new HashSet<UUID>());
        spectrum.setPrecursor(new Peak(1283.674, 6955, 1));

        spectrum.addSorted(new double[]{396.26 + 0.02, 414.26 - 0.17, 543.3 - 0.02}, new double[]{277.0, 216.0, 248.0});
        spectrum.addAnnotation(0, new PepLibPeakAnnotation(0, 0, 0, Optional.of(makeFragAnnotation(IonType.y, 1, 4, 0, new NumericMass(-35), peptide))));
        spectrum.addAnnotation(1, new PepLibPeakAnnotation(0, 0, 0, Optional.of(makeFragAnnotation(IonType.y, 1, 4, 0, new NumericMass(-17), peptide))));
        spectrum.addAnnotation(2, new PepLibPeakAnnotation(0, 0, 0, Optional.of(makeFragAnnotation(IonType.y, 1, 5, 0, new NumericMass(-17), peptide))));
        spectrum.addAnnotation(2, new PepLibPeakAnnotation(0, 0, 0, Optional.of(makeFragAnnotation(IonType.b, 1, 6, 0, new NumericMass(-17), peptide))));
        spectrum.setMsLevel(2);

        writer.write(spectrum);

        //Spectrum 1
        peptide = new PeptideBuilder("AFNPVCGTDGVTYDNECLLC")
                .addModification(5, mockMod("Carbamidomethyl", 57.02146))
                .addModification(16, mockMod("Carbamidomethyl", 57.02146))
                .addModification(19, mockMod("Carbamidomethyl", 57.02146))
                .build();

        spectrum = new PeptideConsensusSpectrum(peptide, precision,new HashSet<UUID>());
        spectrum.setPrecursor(new Peak(1152.985, 6955, 2));
        spectrum.addSorted(new double[]{256.64 - 0.05}, new double[]{317.0});

        spectrum.addAnnotation(0, new PepLibPeakAnnotation(0, 0, 0, Optional.of(makeFragAnnotation(IonType.b, 2, 5, 0, new NumericMass(-17), peptide))));
        spectrum.setMsLevel(2);

        writer.write(spectrum);

        writer.close();

        String expectedString = "Name: AAAAYIYEAVNK/1\n" +
                "LibID: 1\n" +
                "MW: 1283.663\n" +
                "PrecursorMZ: 1283.663\n" +
                "FullName: AAAAYIYEAVNK/1\n" +
                "Comment: Mz_exact=1283.663 Mz_diff=0.011 Parent=1283.674 PrecursorIntensity=6955.000 TotalIonCurrent=741.000 Protein=unknown Mods=0\n" +
                "NumPeaks: 3\n" +
                "396.280\t10000.000\ty4-35/-0.02\n" +
                "414.090\t7797.834\ty4-17/0.17\n" +
                "543.280\t8953.069\ty5-17/0.02,b6-17/1.02\n\n" +
                "Name: AFNPVCGTDGVTYDNECLLC/2\n" +
                "LibID: 2\n" +
                "MW: 2305.964\n" +
                "PrecursorMZ: 1152.982\n" +
                "FullName: AFNPVC(Carbamidomethyl)GTDGVTYDNEC(Carbamidomethyl)LLC(Carbamidomethyl)/2\n" +
                "Comment: Mz_exact=1152.982 Mz_diff=0.003 Parent=1152.985 PrecursorIntensity=6955.000 TotalIonCurrent=317.000 Protein=unknown Mods=3/5,C,Carbamidomethyl/16,C,Carbamidomethyl/19,C,Carbamidomethyl\n" +
                "NumPeaks: 1\n" +
                "256.590\t10000.000\tb5-17^2/0.05\n\n";

        Assert.assertEquals(expectedString, sw.toString());
    }

    private Modification mockMod(String label, double mw) {

        Modification mod = Mockito.mock(Modification.class);
        Mockito.when(mod.getLabel()).thenReturn(label);
        Mockito.when(mod.getMolecularMass()).thenReturn(mw);

        return mod;
    }
}


