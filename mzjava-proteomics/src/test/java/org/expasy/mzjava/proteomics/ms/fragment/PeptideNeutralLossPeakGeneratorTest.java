/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.ms.fragment;

import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.core.mol.Mass;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.mol.PeptideFragment;
import org.expasy.mzjava.core.ms.spectrum.AnnotatedPeak;
import org.expasy.mzjava.core.ms.spectrum.FragmentType;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.proteomics.ms.spectrum.PepFragAnnotation;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

import static org.expasy.mzjava.proteomics.ms.fragment.PeptideNeutralLossPeakGenerator.newModificationLossGenerator;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class PeptideNeutralLossPeakGeneratorTest {

    @Test
    public void testWaterLoss() throws Exception {

        Mass waterLoss = Composition.parseComposition("H-2O-1");
        Set<AminoAcid> aa = EnumSet.of(AminoAcid.S, AminoAcid.T, AminoAcid.D, AminoAcid.E);

        Peptide precursor = Peptide.parse("GGYDSPR");
        PeptideFragment fragment = PeptideFragment.parse("GGY", FragmentType.FORWARD);

        PeptideNeutralLossPeakGenerator peakGenerator = new PeptideNeutralLossPeakGenerator(waterLoss, aa, EnumSet.of(IonType.b), 1);

        List<AnnotatedPeak<PepFragAnnotation>> peaks = peakGenerator.generatePeaks(precursor, fragment, new int[]{1}, new ArrayList<AnnotatedPeak<PepFragAnnotation>>());
        Assert.assertEquals(0, peaks.size());

        fragment = PeptideFragment.parse("GGYDS", FragmentType.FORWARD);

        peaks = peakGenerator.generatePeaks(precursor, fragment, new int[]{1}, new ArrayList<AnnotatedPeak<PepFragAnnotation>>());
        Assert.assertEquals(1, peaks.size());
        Assert.assertEquals(new AnnotatedPeak<>(462.16193884469993, 1, 1, new PepFragAnnotation.Builder(IonType.b, 1, Peptide.parse("GGYDS")).setNeutralLoss(waterLoss).build()),
                peaks.get(0));
    }

    @Test
    public void testPhosphateLoss() throws Exception {

        Mass phophateLoss = Composition.parseComposition("H-1P-1O-3");
        Set<AminoAcid> aa = EnumSet.of(AminoAcid.S, AminoAcid.T);
        Set<Modification> modifications = Collections.singleton(Modification.parseModification("HPO3"));
        EnumSet<ModAttachment> modAttachments = EnumSet.of(ModAttachment.SIDE_CHAIN);

        Peptide precursor = Peptide.parse("GGYDS(HPO3)PR");
        PeptideFragment fragment = PeptideFragment.parse("S(HPO3)PR", FragmentType.REVERSE);

        PeptideNeutralLossPeakGenerator peakGenerator =
                newModificationLossGenerator(phophateLoss, aa, modifications, modAttachments, EnumSet.of(IonType.y), 1);

        List<AnnotatedPeak<PepFragAnnotation>> peaks = peakGenerator.generatePeaks(precursor, fragment, new int[]{1}, new ArrayList<AnnotatedPeak<PepFragAnnotation>>());
        Assert.assertEquals(1, peaks.size());
        Assert.assertEquals(peaks.get(0).getMz(), fragment.calculateMz(IonType.y, 1) - Composition.parseComposition("HPO3").getMolecularMass(), 0.0000001);

        peakGenerator = newModificationLossGenerator(phophateLoss, aa, modifications, modAttachments, EnumSet.of(IonType.b), 1);

        peaks = peakGenerator.generatePeaks(precursor, fragment, new int[]{1}, new ArrayList<AnnotatedPeak<PepFragAnnotation>>());
        Assert.assertEquals(0, peaks.size());

        fragment = PeptideFragment.parse("GGYDS(HPO3)", FragmentType.FORWARD);

        peaks = peakGenerator.generatePeaks(precursor, fragment, new int[]{1}, new ArrayList<AnnotatedPeak<PepFragAnnotation>>());
        Assert.assertEquals(1, peaks.size());
        Assert.assertEquals(peaks.get(0).getMz(), fragment.calculateMz(IonType.b, 1) - Composition.parseComposition("HPO3").getMolecularMass(), 0.0000001);

        fragment = PeptideFragment.parse("GGY(H1P1O3)DS", FragmentType.FORWARD);
        peaks = peakGenerator.generatePeaks(precursor, fragment, new int[]{1}, new ArrayList<AnnotatedPeak<PepFragAnnotation>>());
        Assert.assertEquals(0, peaks.size());

        fragment = PeptideFragment.parse("GGY(H1P1O3)DS(H1P1O3)", FragmentType.FORWARD);
        peaks = peakGenerator.generatePeaks(precursor, fragment, new int[]{1}, new ArrayList<AnnotatedPeak<PepFragAnnotation>>());
        Assert.assertEquals(1, peaks.size());
        Assert.assertEquals(peaks.get(0).getMz(), fragment.calculateMz(IonType.b, 1) - Composition.parseComposition("HPO3").getMolecularMass(), 0.0000001);

        fragment = PeptideFragment.parse("GGY(H1P1O3)DS(H2C)", FragmentType.FORWARD);
        peaks = peakGenerator.generatePeaks(precursor, fragment, new int[]{1}, new ArrayList<AnnotatedPeak<PepFragAnnotation>>());
        Assert.assertEquals(0, peaks.size());

        fragment = PeptideFragment.parse("GGYDS", FragmentType.FORWARD);
        peaks = peakGenerator.generatePeaks(precursor, fragment, new int[]{1}, new ArrayList<AnnotatedPeak<PepFragAnnotation>>());
        Assert.assertEquals(0, peaks.size());

        fragment = PeptideFragment.parse("GGYDS(H2C)", FragmentType.FORWARD);
        peaks = peakGenerator.generatePeaks(precursor, fragment, new int[]{1}, new ArrayList<AnnotatedPeak<PepFragAnnotation>>());
        Assert.assertEquals(0, peaks.size());

        fragment = PeptideFragment.parse("GGYDS(S)", FragmentType.FORWARD);
        peaks = peakGenerator.generatePeaks(precursor, fragment, new int[]{1}, new ArrayList<AnnotatedPeak<PepFragAnnotation>>());
        Assert.assertEquals(0, peaks.size());

        peakGenerator = newModificationLossGenerator(phophateLoss, aa, modifications, modAttachments, EnumSet.of(IonType.y), 1);

        fragment = PeptideFragment.parse("GGYDS(H1P1O3)", FragmentType.REVERSE);
        peaks = peakGenerator.generatePeaks(precursor, fragment, new int[]{1}, new ArrayList<AnnotatedPeak<PepFragAnnotation>>());
        Assert.assertEquals(1, peaks.size());
        Assert.assertEquals(peaks.get(0).getMz(), fragment.calculateMz(IonType.y, 1) - Composition.parseComposition("HPO3").getMolecularMass(), 0.0000001);

        fragment = PeptideFragment.parse("GGY(H1P1O3)DS", FragmentType.REVERSE);
        peaks = peakGenerator.generatePeaks(precursor, fragment, new int[]{1}, new ArrayList<AnnotatedPeak<PepFragAnnotation>>());
        Assert.assertEquals(0, peaks.size());

        fragment = PeptideFragment.parse("GGY(H1P1O3)DS(H1P1O3)", FragmentType.REVERSE);
        peaks = peakGenerator.generatePeaks(precursor, fragment, new int[]{1}, new ArrayList<AnnotatedPeak<PepFragAnnotation>>());
        Assert.assertEquals(1, peaks.size());
        Assert.assertEquals(peaks.get(0).getMz(),fragment.calculateMz(IonType.y,1)-Composition.parseComposition("HPO3").getMolecularMass(),0.0000001);

        fragment = PeptideFragment.parse("GGY(H1P1O3)DS(SO4)", FragmentType.REVERSE);
        peaks = peakGenerator.generatePeaks(precursor, fragment, new int[]{1}, new ArrayList<AnnotatedPeak<PepFragAnnotation>>());
        Assert.assertEquals(0, peaks.size());

        fragment = PeptideFragment.parse("GGYDS", FragmentType.REVERSE);
        peaks = peakGenerator.generatePeaks(precursor, fragment, new int[]{1}, new ArrayList<AnnotatedPeak<PepFragAnnotation>>());
        Assert.assertEquals(0, peaks.size());

        fragment = PeptideFragment.parse("GGYDS(H2C)", FragmentType.REVERSE);
        peaks = peakGenerator.generatePeaks(precursor, fragment, new int[]{1}, new ArrayList<AnnotatedPeak<PepFragAnnotation>>());
        Assert.assertEquals(0, peaks.size());

    }

    @Test
    public void testGetFragmentTypes() throws Exception {

        Mass waterLoss = Composition.parseComposition("H-2O-1");
        Set<AminoAcid> aa = EnumSet.of(AminoAcid.S, AminoAcid.T, AminoAcid.D, AminoAcid.E);

        PeptideNeutralLossPeakGenerator peakGenerator = new PeptideNeutralLossPeakGenerator(waterLoss, aa, EnumSet.of(IonType.b), 1);
        Assert.assertEquals(EnumSet.of(FragmentType.FORWARD), peakGenerator.getFragmentTypes());

        peakGenerator = new PeptideNeutralLossPeakGenerator(waterLoss, aa, EnumSet.of(IonType.y), 1);
        Assert.assertEquals(EnumSet.of(FragmentType.REVERSE), peakGenerator.getFragmentTypes());

        peakGenerator = new PeptideNeutralLossPeakGenerator(waterLoss, aa, EnumSet.of(IonType.i), 1);
        Assert.assertEquals(EnumSet.of(FragmentType.MONOMER), peakGenerator.getFragmentTypes());

        peakGenerator = new PeptideNeutralLossPeakGenerator(waterLoss, aa, EnumSet.of(IonType.y, IonType.b), 1);
        Assert.assertEquals(EnumSet.of(FragmentType.REVERSE, FragmentType.FORWARD), peakGenerator.getFragmentTypes());
    }

    @Test
    public void testStaticMethods() {

        Mass phophateLoss = Composition.parseComposition("H-1P-1O-3");
        Mass phophoricAcidLoss = Composition.parseComposition("H-3P-1O-4");
        Set<AminoAcid> aa = EnumSet.of(AminoAcid.S, AminoAcid.T);

        Peptide precursor = Peptide.parse("GGYDS(H1P1O3)PR");
        PeptideFragment fragment = PeptideFragment.parse("S(H1P1O3)PR", FragmentType.REVERSE);

        PeptideNeutralLossPeakGenerator peakGenerator =
                PeptideNeutralLossPeakGenerator.newModificationLossGenerator(phophateLoss, aa, Collections.singleton(Modification.parseModification("HPO3")), EnumSet.of(ModAttachment.SIDE_CHAIN), EnumSet.of(IonType.y), 1);

        List<AnnotatedPeak<PepFragAnnotation>> peaks = peakGenerator.generatePeaks(precursor, fragment, new int[]{1}, new ArrayList<AnnotatedPeak<PepFragAnnotation>>());

        peakGenerator = PeptideNeutralLossPeakGenerator.newPhosphateLossGenerator(EnumSet.of(IonType.y), 1);

        List<AnnotatedPeak<PepFragAnnotation>> peaks2 = peakGenerator.generatePeaks(precursor, fragment, new int[]{1}, new ArrayList<AnnotatedPeak<PepFragAnnotation>>());
        Assert.assertEquals(1, peaks.size());
        Assert.assertTrue(peaks.get(0).equals(peaks2.get(0)));

        peakGenerator =
                PeptideNeutralLossPeakGenerator.newModificationLossGenerator(phophoricAcidLoss, aa, Collections.singleton(Modification.parseModification("HPO3")), EnumSet.of(ModAttachment.SIDE_CHAIN), EnumSet.of(IonType.y), 1);

        peaks = peakGenerator.generatePeaks(precursor, fragment, new int[]{1}, new ArrayList<AnnotatedPeak<PepFragAnnotation>>());

        peakGenerator = PeptideNeutralLossPeakGenerator.newPhosphoricAcidLossGenerator(EnumSet.of(IonType.y), 1);

        peaks2 = peakGenerator.generatePeaks(precursor, fragment, new int[]{1}, new ArrayList<AnnotatedPeak<PepFragAnnotation>>());
        Assert.assertEquals(1, peaks.size());
        Assert.assertTrue(peaks.get(0).equals(peaks2.get(0)));

    }
}
