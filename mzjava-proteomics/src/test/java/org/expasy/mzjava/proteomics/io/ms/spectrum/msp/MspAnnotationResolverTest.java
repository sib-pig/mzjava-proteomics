/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.io.ms.spectrum.msp;

import com.google.common.base.Optional;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.core.mol.NumericMass;
import org.expasy.mzjava.core.mol.PeriodicTable;
import org.expasy.mzjava.core.ms.spectrum.FragmentType;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.mol.PeptideFragment;
import org.expasy.mzjava.proteomics.ms.spectrum.PepFragAnnotation;
import org.expasy.mzjava.proteomics.ms.spectrum.PepLibPeakAnnotation;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class MspAnnotationResolverTest {

    @Test
    public void testResolveAnnotations1() throws Exception {

        MspAnnotationResolver annotationResolver = new MspAnnotationResolver();

        Peptide peptide = Peptide.parse("AAAAAAGAGPEMVR");
        List<PepLibPeakAnnotation> annotations = annotationResolver.resolveAnnotations("\"b6-43^2/0.48 20/20 0.9\"", peptide);
        Assert.assertEquals(new PepLibPeakAnnotation(20, 0, 0, Optional.of(new PepFragAnnotation.Builder(IonType.b, 2, Peptide.parse("AAAAAA")).setNeutralLoss(new NumericMass(-43)).build())), annotations.get(0));
    }

    @Test
    public void testResolveAnnotations2() throws Exception {

        MspAnnotationResolver annotationResolver = new MspAnnotationResolver();

        Peptide peptide = Peptide.parse("AAAAAAGAGPEMVR");
        annotationResolver.resolveAnnotations("\"b6-43^2/1.48 17/18 0.3\"", peptide);
        List<PepLibPeakAnnotation> annotations = annotationResolver.resolveAnnotations("\"b6-43i^2/1.48 17/18 0.3\"", peptide);
        Assert.assertEquals(new PepLibPeakAnnotation(17, 0, 0, Optional.of(new PepFragAnnotation.Builder(IonType.b, 2, Peptide.parse("AAAAAA")).addC13(1).setNeutralLoss(new NumericMass(-43)).build())), annotations.get(0));
    }

    @Test
    public void testResolveAnnotations3() throws Exception {

        MspAnnotationResolver annotationResolver = new MspAnnotationResolver();

        Peptide peptide = Peptide.parse("AAAAAAGAGPEMVR");
        annotationResolver.resolveAnnotations("\"b6-18^2/1.48 17/18 0.3\"", peptide);
        List<PepLibPeakAnnotation> annotations = annotationResolver.resolveAnnotations("\"z6-18i^2/1.48 17/18 0.3\"", peptide);
        Assert.assertEquals(new PepLibPeakAnnotation(17, 0, 0,
                Optional.of(new PepFragAnnotation.Builder(IonType.z, 2, Peptide.parse("GPEMVR"))
                        .addC13(1)
                        .setNeutralLoss(new Composition.Builder().add(PeriodicTable.H, -2).add(PeriodicTable.O, -1).build())
                        .build())
        ), annotations.get(0));
    }

    @Test
    public void testResolve3() throws Exception {

        MspAnnotationResolver annotationResolver = new MspAnnotationResolver();
        Peptide peptide = Peptide.parse("AAAGEFADDPCSSVK");
        List<PepLibPeakAnnotation> annotations = annotationResolver.resolveAnnotations("\"ICACx/0.90 2/2 1.9\"", peptide);
        Assert.assertEquals(1, annotations.size());
        Assert.assertEquals(new PepLibPeakAnnotation(2, 0, 0), annotations.get(0));
    }

    /**
     * Tests that annotations with a * (i.e. ones that are dodgy) are skipped
     *
     * @throws Exception
     */
    @Test
    public void testResolve4() throws Exception {

        MspAnnotationResolver annotationResolver = new MspAnnotationResolver();
        Peptide peptide = Peptide.parse("AAAAAAGAGPEM(O)VR");
        List<PepLibPeakAnnotation> annotations = annotationResolver.resolveAnnotations("\"b13-44*^2/-0.46,b13-46^2/0.54 3/5 0.1\"", peptide);

        Assert.assertEquals(2, annotations.size());
        Assert.assertEquals(new PepLibPeakAnnotation(3, 0, 0), annotations.get(0));
        Assert.assertEquals(new PepLibPeakAnnotation(3, 0, 0, Optional.of(
                new PepFragAnnotation.Builder(IonType.b, 2, PeptideFragment.parse("AAAAAAGAGPEM(O)V", FragmentType.FORWARD))
                        .setNeutralLoss(new NumericMass(-46))
                        .build()
        )), annotations.get(1));
    }

    @Test(expected = IllegalStateException.class)
    public void testIllegalAnnotation() throws Exception {

        MspAnnotationResolver annotationResolver = new MspAnnotationResolver();
        annotationResolver.resolveAnnotations("profound annotation", Peptide.parse("AAAAAAGAGPEM(O)VR"));
    }

    @Test
    public void testResolve5() throws Exception {

        MspAnnotationResolver annotationResolver = new MspAnnotationResolver();
        List<PepLibPeakAnnotation> annotations = annotationResolver.resolveAnnotations("\"p/-0.52,y6-46/-0.05 14/20 1.1\"",
                Peptide.parse("AAAAAAGAGPEM(O)VR"));

        Assert.assertEquals(2, annotations.size());
        Assert.assertEquals(new PepLibPeakAnnotation(14, 0, 0, Optional.of(
                new PepFragAnnotation.Builder(IonType.p, 1, Peptide.parse("AAAAAAGAGPEM(O)VR"))
                        .build()
        )), annotations.get(0));
        Assert.assertEquals(new PepLibPeakAnnotation(14, 0, 0, Optional.of(
                new PepFragAnnotation.Builder(IonType.y, 1, PeptideFragment.parse("GPEM(O)VR", FragmentType.REVERSE))
                        .setNeutralLoss(new NumericMass(-46))
                        .build()
        )), annotations.get(1));
    }

    @Test
    public void testResolve6() throws Exception {

        MspAnnotationResolver annotationResolver = new MspAnnotationResolver();
        List<PepLibPeakAnnotation> annotations = annotationResolver.resolveAnnotations("\"IRC/0.00,IPA/0.03 13/20 0.8\"",
                Peptide.parse("AAAAAAGAGPEM(O)VR"));

        Assert.assertEquals(2, annotations.size());
        Assert.assertEquals(new PepLibPeakAnnotation(13, 0.0, 0, Optional.of(
                new PepFragAnnotation(IonType.i, 1, new PeptideFragment(FragmentType.INTERNAL, AminoAcid.R))
        )), annotations.get(0));
        Assert.assertEquals(new PepLibPeakAnnotation(13, 0.0, 0, Optional.of(
                new PepFragAnnotation(IonType.i, 1, new PeptideFragment(FragmentType.INTERNAL, AminoAcid.P))
        )), annotations.get(1));
    }

    @Test
    public void testResolve7() throws Exception {

        MspAnnotationResolver annotationResolver = new MspAnnotationResolver();
        List<PepLibPeakAnnotation> annotations = annotationResolver.resolveAnnotations("\"? 14/20 0.4\"",
                Peptide.parse("AAAAAAGAGPEM(O)VR"));

        Assert.assertEquals(1, annotations.size());
        Assert.assertEquals(new PepLibPeakAnnotation(14, 0.0, 0), annotations.get(0));
    }

    @Test
    public void testResolve8() throws Exception {

        MspAnnotationResolver annotationResolver = new MspAnnotationResolver();
        Peptide peptide = Peptide.parse("AAAAAAGAGPEMVR");
        List<PepLibPeakAnnotation> annotations = annotationResolver.resolveAnnotations("\"b3i/0.38,y2-18i/0.31 15/20 9.9\"",
                peptide);

        Assert.assertEquals(2, annotations.size());
        Assert.assertEquals(new PepLibPeakAnnotation(15, 0.0, 0.0,
                Optional.of(new PepFragAnnotation.Builder(
                        IonType.b, 1, peptide.createFragment(IonType.b, 3)
                ).addC13(1).build())
        ), annotations.get(0));
        Assert.assertEquals(new PepLibPeakAnnotation(15, 0.0, 0.0,
                Optional.of(new PepFragAnnotation.Builder(
                        IonType.y, 1, peptide.createFragment(IonType.y, 2)
                ).addC13(1).setNeutralLoss(Composition.parseComposition("(H2O)-1")).build())
        ), annotations.get(1));
    }
}
