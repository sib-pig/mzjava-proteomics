/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.mol.modification.unimod.jaxb;

import com.google.common.collect.Lists;
import gnu.trove.map.TObjectIntMap;
import org.expasy.mzjava.core.mol.Atom;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.core.mol.PeriodicTable;
import org.expasy.mzjava.proteomics.mol.modification.NeutralLoss;
import org.expasy.mzjava.proteomics.mol.modification.unimod.UnimodMod;
import org.junit.Assert;
import org.junit.Test;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class ModificationTypeAdapterTest {
    @Test
    public void testUnmarshal() throws Exception {

        ModificationTypeAdapter adapter = new ModificationTypeAdapter();

        List<ElemRefT> elements = new ArrayList<ElemRefT>();
        elements.add(mockElement("H", -1));
        elements.add(mockElement("C", 0));
        elements.add(mockElement("N", 1));
        elements.add(mockElement("O", 1));
        elements.add(mockElement("S", 0));

        CompositionT delta = mock(CompositionT.class);
        when(delta.getElement()).thenReturn(elements);

        ModT modT = mock(ModT.class);
        when(modT.getRecordId()).thenReturn(1l);
        when(modT.isApproved()).thenReturn(true);
        when(modT.getTitle()).thenReturn("The title");
        when(modT.getFullName()).thenReturn("The full name");
        when(modT.getDelta()).thenReturn(delta);

        UnimodMod unimodMod = adapter.unmarshal(modT);

        Assert.assertEquals("H-1NO", unimodMod.getMass().getFormula());

        //Test that 0 counts are not put in map
        Field field = Composition.class.getDeclaredField("atomCounterMap");
        field.setAccessible(true);
        @SuppressWarnings("unchecked")
        TObjectIntMap<Atom> atomCounterMap = (TObjectIntMap<Atom>) field.get(unimodMod.getMass());
        Assert.assertEquals(3, atomCounterMap.size());
        Assert.assertEquals(-1, atomCounterMap.get(PeriodicTable.H));
        Assert.assertEquals(1, atomCounterMap.get(PeriodicTable.N));
        Assert.assertEquals(1, atomCounterMap.get(PeriodicTable.O));
    }

    private ElemRefT mockElement(String symbol, int number) {

        ElemRefT element = mock(ElemRefT.class);
        when(element.getSymbol()).thenReturn(symbol);
        when(element.getNumber()).thenReturn(number);

        return element;
    }

    @Test
    public void testJaxb() throws Exception {

        String xml =
                "<umod:unimod xmlns:umod=\"http://www.unimod.org/xmlns/schema/unimod_2\"\n" +
                        "             xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" +
                        "             xsi:schemaLocation=\"http://www.unimod.org/xmlns/schema/unimod_2 http://www.unimod.org/xmlns/schema/unimod_2/unimod_2.xsd\"\n" +
                        "             majorVersion=\"2\"\n" +
                        "             minorVersion=\"0\">\n" +
                        "   <umod:modifications>\n" +
                        "      <umod:mod title=\"Methylthio\" full_name=\"Beta-methylthiolation\"\n" +
                        "                username_of_poster=\"unimod\"\n" +
                        "                group_of_poster=\"admin\"\n" +
                        "                date_time_posted=\"2002-08-19 19:17:11\"\n" +
                        "                date_time_modified=\"2011-11-24 16:48:21\"\n" +
                        "                approved=\"0\"\n" +
                        "                record_id=\"39\">\n" +
                        "         <umod:specificity hidden=\"0\" site=\"C\" position=\"Anywhere\" classification=\"Multiple\"\n" +
                        "                           spec_group=\"3\"/>\n" +
                        "         <umod:specificity hidden=\"1\" site=\"N\" position=\"Anywhere\" classification=\"Post-translational\"\n" +
                        "                           spec_group=\"2\"/>\n" +
                        "         <umod:specificity hidden=\"1\" site=\"D\" position=\"Anywhere\" classification=\"Post-translational\"\n" +
                        "                           spec_group=\"1\"/>\n" +
                        "         <umod:specificity hidden=\"1\" site=\"K\" position=\"Anywhere\" classification=\"Artefact\"\n" +
                        "                           spec_group=\"5\"/>\n" +
                        "         <umod:specificity hidden=\"1\" site=\"N-term\" position=\"Any N-term\" classification=\"Artefact\"\n" +
                        "                           spec_group=\"4\"/>\n" +
                        "         <umod:delta mono_mass=\"45.987721\" avge_mass=\"46.0916\" composition=\"H(2) C S\">\n" +
                        "            <umod:element symbol=\"H\" number=\"2\"/>\n" +
                        "            <umod:element symbol=\"C\" number=\"1\"/>\n" +
                        "            <umod:element symbol=\"S\" number=\"1\"/>\n" +
                        "         </umod:delta>\n" +
                        "         <umod:alt_name>Methyl methanethiosulfonate</umod:alt_name>\n" +
                        "         <umod:alt_name>MMTS</umod:alt_name>\n" +
                        "      </umod:mod>\n" +
                        "   </umod:modifications>\n" +
                        "</umod:unimod>";

        Class<UnimodT> docClass = UnimodT.class;

        String packageName = docClass.getPackage().getName();
        JAXBContext jc = JAXBContext.newInstance(packageName);
        Unmarshaller u = jc.createUnmarshaller();
        UnimodT doc = (UnimodT) ((JAXBElement) u.unmarshal(new StringReader(xml))).getValue();

        List<UnimodMod> mods = doc.getModifications().getMod();
        Assert.assertEquals(1, mods.size());

        UnimodMod mod = mods.get(0);

        Assert.assertEquals("Methylthio", mod.getLabel());
        Assert.assertEquals("CH2S", mod.getMass().getFormula());
        Assert.assertEquals(39, mod.getRecordId());

        Assert.assertEquals(Lists.newArrayList("C", "N", "D", "K", "N-term"), mod.getSites());
    }

    @Test
    public void testJaxb2() throws Exception {

        String xml =
                "<umod:unimod xmlns:umod=\"http://www.unimod.org/xmlns/schema/unimod_2\"\n" +
                        "             xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" +
                        "             xsi:schemaLocation=\"http://www.unimod.org/xmlns/schema/unimod_2 http://www.unimod.org/xmlns/schema/unimod_2/unimod_2.xsd\"\n" +
                        "             majorVersion=\"2\"\n" +
                        "             minorVersion=\"0\">\n" +
                        "   <umod:modifications>\n" +
                        "      <umod:mod title=\"Phospho\" full_name=\"Phosphorylation\" username_of_poster=\"unimod\"\n" +
                        "                group_of_poster=\"admin\"\n" +
                        "                date_time_posted=\"2002-08-19 19:17:11\"\n" +
                        "                date_time_modified=\"2011-11-25 10:55:54\"\n" +
                        "                approved=\"1\"\n" +
                        "                record_id=\"21\">\n" +
                        "         <umod:specificity hidden=\"1\" site=\"C\" position=\"Anywhere\" classification=\"Post-translational\"\n" +
                        "                           spec_group=\"5\">\n" +
                        "            <umod:misc_notes>Rare</umod:misc_notes>\n" +
                        "         </umod:specificity>\n" +
                        "         <umod:specificity hidden=\"1\" site=\"H\" position=\"Anywhere\" classification=\"Post-translational\"\n" +
                        "                           spec_group=\"4\">\n" +
                        "            <umod:misc_notes>Rare</umod:misc_notes>\n" +
                        "         </umod:specificity>\n" +
                        "         <umod:specificity hidden=\"1\" site=\"D\" position=\"Anywhere\" classification=\"Post-translational\"\n" +
                        "                           spec_group=\"3\">\n" +
                        "            <umod:misc_notes>Rare</umod:misc_notes>\n" +
                        "         </umod:specificity>\n" +
                        "         <umod:specificity hidden=\"0\" site=\"Y\" position=\"Anywhere\" classification=\"Post-translational\"\n" +
                        "                           spec_group=\"2\">\n" +
                        "            <umod:misc_notes>Usually don't see beta elimination of phosphate</umod:misc_notes>\n" +
                        "         </umod:specificity>\n" +
                        "         <umod:specificity hidden=\"0\" site=\"T\" position=\"Anywhere\" classification=\"Post-translational\"\n" +
                        "                           spec_group=\"1\">\n" +
                        "            <umod:NeutralLoss mono_mass=\"97.976896\" avge_mass=\"97.9952\" flag=\"false\"\n" +
                        "                              composition=\"H(3) O(4) P\">\n" +
                        "               <umod:element symbol=\"H\" number=\"3\"/>\n" +
                        "               <umod:element symbol=\"O\" number=\"4\"/>\n" +
                        "               <umod:element symbol=\"P\" number=\"1\"/>\n" +
                        "            </umod:NeutralLoss>\n" +
                        "            <umod:NeutralLoss mono_mass=\"0.000000\" avge_mass=\"0.0000\" flag=\"false\" composition=\"0\"/>\n" +
                        "         </umod:specificity>\n" +
                        "         <umod:specificity hidden=\"0\" site=\"S\" position=\"Anywhere\" classification=\"Post-translational\"\n" +
                        "                           spec_group=\"1\">\n" +
                        "            <umod:NeutralLoss mono_mass=\"0.000000\" avge_mass=\"0.0000\" flag=\"false\" composition=\"0\"/>\n" +
                        "            <umod:NeutralLoss mono_mass=\"97.976896\" avge_mass=\"97.9952\" flag=\"false\"\n" +
                        "                              composition=\"H(3) O(4) P\">\n" +
                        "               <umod:element symbol=\"H\" number=\"3\"/>\n" +
                        "               <umod:element symbol=\"O\" number=\"4\"/>\n" +
                        "               <umod:element symbol=\"P\" number=\"1\"/>\n" +
                        "            </umod:NeutralLoss>\n" +
                        "         </umod:specificity>\n" +
                        "         <umod:specificity hidden=\"1\" site=\"R\" position=\"Anywhere\" classification=\"Post-translational\"\n" +
                        "                           spec_group=\"6\"/>\n" +
                        "         <umod:specificity hidden=\"1\" site=\"K\" position=\"Anywhere\" classification=\"Other\" spec_group=\"7\">\n" +
                        "            <umod:misc_notes>from ProteinPilot</umod:misc_notes>\n" +
                        "         </umod:specificity>\n" +
                        "         <umod:delta mono_mass=\"79.966331\" avge_mass=\"79.9799\" composition=\"H O(3) P\">\n" +
                        "            <umod:element symbol=\"H\" number=\"1\"/>\n" +
                        "            <umod:element symbol=\"O\" number=\"3\"/>\n" +
                        "            <umod:element symbol=\"P\" number=\"1\"/>\n" +
                        "         </umod:delta>\n" +
                        "         <umod:misc_notes>Protein which is posttranslationally modified by the attachment of at least one phosphate group usually on serine, threonine or tyrosine residues, but also on aspartic acid or histidine residues.</umod:misc_notes>\n" +
                        "      </umod:mod>" +
                        "   </umod:modifications>\n" +
                        "</umod:unimod>";

        Class<UnimodT> docClass = UnimodT.class;

        String packageName = docClass.getPackage().getName();
        JAXBContext jc = JAXBContext.newInstance(packageName);
        Unmarshaller u = jc.createUnmarshaller();
        UnimodT doc = (UnimodT) ((JAXBElement) u.unmarshal(new StringReader(xml))).getValue();

        List<UnimodMod> mods = doc.getModifications().getMod();
        Assert.assertEquals(1, mods.size());

        UnimodMod mod = mods.get(0);

        Assert.assertEquals("Phospho", mod.getLabel());
        Assert.assertEquals("HO3P", mod.getMass().getFormula());
        Assert.assertEquals(21, mod.getRecordId());

        List<NeutralLoss> neutralLosses = mod.getNeutralLosses();
        Assert.assertEquals(2, neutralLosses.size());

        Assert.assertEquals(0.0, neutralLosses.get(0).getMolecularMass(), 0.000001);
        Assert.assertEquals(new HashSet<>(Arrays.asList("S", "T")), neutralLosses.get(0).getSites());

        Assert.assertEquals(97.97689508399999, neutralLosses.get(1).getMolecularMass(), 0.000000001);
        Assert.assertEquals(new HashSet<>(Arrays.asList("S", "T")), neutralLosses.get(1).getSites());

        Assert.assertEquals(Lists.newArrayList("C", "H", "D", "Y", "T", "S", "R", "K"), mod.getSites());
    }
}
