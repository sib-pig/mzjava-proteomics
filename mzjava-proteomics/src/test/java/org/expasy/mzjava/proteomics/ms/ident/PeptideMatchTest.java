/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.ms.ident;

import com.google.common.base.Optional;
import gnu.trove.map.TObjectDoubleMap;
import gnu.trove.map.hash.TObjectDoubleHashMap;
import org.expasy.mzjava.core.mol.SymbolSequence;
import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.core.mol.AtomicSymbol;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.core.mol.PeriodicTable;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.mol.PeptideBuilder;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;
import java.util.regex.Pattern;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class PeptideMatchTest {

    @Test
    public void testModifications() throws Exception {

        PeptideMatch peptideMatch = new PeptideMatch(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V);

        List<ModificationMatch> modsNTerm = makeMatchList(3);
        List<ModificationMatch> modsCTerm = makeMatchList(5);
        List<ModificationMatch> mods0 = makeMatchList(3);
        List<ModificationMatch> mods3 = makeMatchList(6);
        List<ModificationMatch> mods1 = makeMatchList(8);

        List<ModificationMatch> all0 = new ArrayList<ModificationMatch>(mods0);
        all0.addAll(modsNTerm);
        List<ModificationMatch> all3 = new ArrayList<ModificationMatch>(mods3);
        all3.addAll(modsCTerm);

        List<ModificationMatch> allSide = new ArrayList<ModificationMatch>(mods0);
        allSide.addAll(mods1);
        allSide.addAll(mods3);

        for(ModificationMatch match : modsNTerm) {

            peptideMatch.addModificationMatch(ModAttachment.N_TERM, match);
        }

        for(ModificationMatch match : modsCTerm) {

            peptideMatch.addModificationMatch(ModAttachment.C_TERM, match);
        }

        for(ModificationMatch match : mods0) {

            peptideMatch.addModificationMatch(0, match);
        }

        for(ModificationMatch match : mods1) {

            peptideMatch.addModificationMatch(1, match);
        }

        for(ModificationMatch match : mods3) {

            peptideMatch.addModificationMatch(3, match);
        }

        Assert.assertEquals(AminoAcid.C, peptideMatch.getSymbol(0));
        Assert.assertEquals(AminoAcid.E, peptideMatch.getSymbol(1));
        Assert.assertEquals(AminoAcid.R, peptideMatch.getSymbol(2));
        Assert.assertEquals(AminoAcid.V, peptideMatch.getSymbol(3));

        Assert.assertEquals(modsNTerm, peptideMatch.getModifications(EnumSet.of(ModAttachment.N_TERM)));
        Assert.assertEquals(modsNTerm, peptideMatch.getModifications(0, EnumSet.of(ModAttachment.N_TERM)));
        Assert.assertEquals(modsCTerm, peptideMatch.getModifications(EnumSet.of(ModAttachment.C_TERM)));
        Assert.assertEquals(modsCTerm, peptideMatch.getModifications(3, EnumSet.of(ModAttachment.C_TERM)));

        Assert.assertEquals(all0, peptideMatch.getModifications(0, EnumSet.allOf(ModAttachment.class)));
        Assert.assertEquals(all3, peptideMatch.getModifications(3, EnumSet.allOf(ModAttachment.class)));

        Assert.assertEquals(mods1, peptideMatch.getModifications(1, EnumSet.allOf(ModAttachment.class)));
        Assert.assertEquals(Collections.<ModificationMatch>emptyList(), peptideMatch.getModifications(2, EnumSet.allOf(ModAttachment.class)));

        Assert.assertEquals(allSide, peptideMatch.getModifications(ModAttachment.sideChainSet));

        Assert.assertEquals(25, peptideMatch.getModificationCount());
        Assert.assertEquals(4, peptideMatch.size());
    }

    private List<ModificationMatch> makeMatchList(int size) {

        List<ModificationMatch> matchList = new ArrayList<ModificationMatch>(size);

        for(int i = 0; i < size; i++) {

            matchList.add(mock(ModificationMatch.class));
        }

        return matchList;
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testModMatchIndexOutOfBounds1() throws Exception {

        PeptideMatch peptideMatch = new PeptideMatch(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V);

        peptideMatch.addModificationMatch(-1, mock(ModificationMatch.class));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testModMatchIndexOutOfBounds2() throws Exception {

        PeptideMatch peptideMatch = new PeptideMatch(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V);

        peptideMatch.addModificationMatch(4, mock(ModificationMatch.class));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNoIndexForSideChainMod() throws Exception {

        PeptideMatch peptideMatch = new PeptideMatch(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V);

        peptideMatch.addModificationMatch(ModAttachment.SIDE_CHAIN, mock(ModificationMatch.class));
    }

    @Test
    public void testStringConstructor() throws Exception {

        PeptideMatch peptideMatch = new PeptideMatch("PEPTIDEK");

        Assert.assertEquals("PEPTIDEK", peptideMatch.toSymbolString());
    }

    @Test
    public void testListConstructor() throws Exception {

        PeptideMatch peptideMatch = new PeptideMatch(Arrays.asList(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V));

        Assert.assertEquals("CERV", peptideMatch.toSymbolString());
    }

    @Test
    public void testSymbolSeqConstructor() throws Exception {

        @SuppressWarnings("unchecked") //Mock
        SymbolSequence<AminoAcid> symbolSeq = mock(SymbolSequence.class);
        when(symbolSeq.size()).thenReturn(4);
        when(symbolSeq.getSymbol(0)).thenReturn(AminoAcid.C);
        when(symbolSeq.getSymbol(1)).thenReturn(AminoAcid.E);
        when(symbolSeq.getSymbol(2)).thenReturn(AminoAcid.R);
        when(symbolSeq.getSymbol(3)).thenReturn(AminoAcid.V);

        PeptideMatch peptideMatch = new PeptideMatch(symbolSeq);

        Assert.assertEquals("CERV", peptideMatch.toSymbolString());
    }

    @Test
    public void testToPeptide() throws Exception {

        PeptideMatch peptideMatch = new PeptideMatch(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V);
        peptideMatch.setNumMissedCleavages(Optional.fromNullable(0));

        ModificationMatch modNTerm = makeMod("nTerm", AtomicSymbol.H);
        ModificationMatch mod0 = makeMod("mod0", AtomicSymbol.He);
        ModificationMatch mod1_1 = makeMod("mod1_1", AtomicSymbol.Li);
        ModificationMatch mod1_2 = makeMod("mod1_2", AtomicSymbol.Be);
        ModificationMatch mod3 = makeMod("mod3", AtomicSymbol.B);
        ModificationMatch modCTerm = makeMod("modCTerm", AtomicSymbol.C);

        peptideMatch.addModificationMatch(ModAttachment.N_TERM, modNTerm);
        peptideMatch.addModificationMatch(0, mod0);
        peptideMatch.addModificationMatch(1, mod1_1);
        peptideMatch.addModificationMatch(1, mod1_2);
        peptideMatch.addModificationMatch(3, mod3);
        peptideMatch.addModificationMatch(ModAttachment.C_TERM, modCTerm);

        Peptide expectedPeptide = new PeptideBuilder("CERV")
                .addModification(ModAttachment.N_TERM, modNTerm.getModificationCandidate(0))
                .addModification(0, mod0.getModificationCandidate(0))
                .addModification(1, mod1_1.getModificationCandidate(0))
                .addModification(1, mod1_2.getModificationCandidate(0))
                .addModification(3, mod3.getModificationCandidate(0))
                .addModification(ModAttachment.C_TERM, modCTerm.getModificationCandidate(0))
                .build();

        Peptide actualPeptide = peptideMatch.toPeptide();

        Assert.assertEquals(expectedPeptide, actualPeptide);
    }

    @Test (expected = UnresolvableModificationMatchException.class)
    public void testToPeptideUnknownModTerm() throws Exception {

        PeptideMatch peptideMatch = new PeptideMatch(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V);

        ModificationMatch modNTerm = mock(ModificationMatch.class);
        when(modNTerm.getCandidateCount()).thenReturn(0);

        peptideMatch.addModificationMatch(ModAttachment.N_TERM, modNTerm);

        peptideMatch.toPeptide();
    }

    @Test (expected = UnresolvableModificationMatchException.class)
    public void testToPeptideUnknownModSideChain() throws Exception {

        PeptideMatch peptideMatch = new PeptideMatch(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V);

        ModificationMatch modNTerm = mock(ModificationMatch.class);
        when(modNTerm.getCandidateCount()).thenReturn(0);

        peptideMatch.addModificationMatch(1, modNTerm);

        peptideMatch.toPeptide();
    }

    private ModificationMatch makeMod(String label, AtomicSymbol symbol) {

        ModificationMatch modificationMatch = mock(ModificationMatch.class);
        when(modificationMatch.getCandidateCount()).thenReturn(1);
        when(modificationMatch.getModificationCandidate(0)).thenReturn(new Modification(label, new Composition.Builder(symbol).build()));
        when(modificationMatch.getMassShift()).thenReturn(PeriodicTable.getInstance().getAtom(symbol).getMass());
        when(modificationMatch.toString()).thenReturn(symbol.toString());
        return modificationMatch;
    }

    @Test
    public void testCompare() throws Exception {

        PeptideMatch peptideMatch = new PeptideMatch(AminoAcid.C);
        peptideMatch.setRank(Optional.fromNullable(12));

        PeptideMatch match = new PeptideMatch();

        match.setRank(Optional.fromNullable(1));
        Assert.assertEquals(1, peptideMatch.compareTo(match));

        match.setRank(Optional.fromNullable(12));
        Assert.assertEquals(0, peptideMatch.compareTo(match));

        match.setRank(Optional.fromNullable(55));
        Assert.assertEquals(-1, peptideMatch.compareTo(match));
    }

    @Test
    public void testCompare2() throws Exception {

        PeptideMatch match12 = new PeptideMatch(AminoAcid.C);
        match12.setRank(Optional.fromNullable(12));

        PeptideMatch match1 = new PeptideMatch(AminoAcid.N);
        match1.setRank(Optional.fromNullable(1));

        PeptideMatch match55 = new PeptideMatch(AminoAcid.O);
        match55.setRank(Optional.fromNullable(12));


        List<PeptideMatch> list = new ArrayList<PeptideMatch>(3);
        list.add(match12);
        list.add(match1);
        list.add(match55);

        Collections.sort(list);

        Assert.assertEquals(match1, list.get(0));
        Assert.assertEquals(match12, list.get(1));
        Assert.assertEquals(match55, list.get(2));
    }

    @Test
    public void testToString() throws Exception {

        PeptideMatch peptideMatch = new PeptideMatch(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V);

        ModificationMatch modNTerm = makeMod("nTerm", AtomicSymbol.H);
        ModificationMatch mod0 = makeMod("mod0", AtomicSymbol.He);
        ModificationMatch mod1_1 = makeMod("mod1_1", AtomicSymbol.Li);
        ModificationMatch mod1_2 = makeMod("mod1_2", AtomicSymbol.Be);
        ModificationMatch mod3 = makeMod("mod3", AtomicSymbol.B);
        ModificationMatch modCTerm = makeMod("modCTerm", AtomicSymbol.C);

        peptideMatch.addModificationMatch(ModAttachment.N_TERM, modNTerm);
        peptideMatch.addModificationMatch(0, mod0);
        peptideMatch.addModificationMatch(1, mod1_1);
        peptideMatch.addModificationMatch(1, mod1_2);
        peptideMatch.addModificationMatch(3, mod3);
        peptideMatch.addModificationMatch(ModAttachment.C_TERM, modCTerm);

        Assert.assertEquals("PeptideMatch{rank=Optional.absent(), rejected=Optional.absent(), massDiff=Optional.absent(), numMatchedIons=Optional.absent(), numMissedCleavages=Optional.absent(), totalNumIons=Optional.absent(), peptide=(H)_C(He)E(Li, Be)RV(B)_(C)}", peptideMatch.toString());
    }

    @Test(expected = NullPointerException.class)
    public void testAddNullScore() throws Exception {

        PeptideMatch peptideMatch = new PeptideMatch(AminoAcid.J);

        peptideMatch.addScore(null, 12.3);
    }

    @Test
    public void testScores() throws Exception {

        PeptideMatch peptideMatch = new PeptideMatch(AminoAcid.J);

        peptideMatch.addScore("a", 12.3);
        peptideMatch.addScore("b", 345);
        peptideMatch.addScore("c", -836.21);
        peptideMatch.addScore("d", 0.000078);

        Assert.assertEquals(12.3, peptideMatch.getScore("a"), 0.000000001);
        Assert.assertEquals(345, peptideMatch.getScore("b"), 0.000000001);
        Assert.assertEquals(-836.21, peptideMatch.getScore("c"), 0.000000001);
        Assert.assertEquals(0.000078, peptideMatch.getScore("d"), 0.000000001);
        Assert.assertFalse(peptideMatch.hasScore("e"));

        TObjectDoubleMap<String> map = new TObjectDoubleHashMap<String>();
        map.put("a", 12.3);
        map.put("b", 345);
        map.put("c", -836.21);
        map.put("d", 0.000078);

        Assert.assertEquals(map, peptideMatch.getScoreMap());
    }

    @Test (expected = IllegalArgumentException.class)
    public void testMissingScore() throws Exception {

        PeptideMatch peptideMatch = new PeptideMatch(AminoAcid.J);

        peptideMatch.getScore("e");
    }

    @Test
    public void test_containsProtein() {
        PeptideMatch peptideMatch = new PeptideMatch(AminoAcid.C);

        PeptideProteinMatch match1 = new PeptideProteinMatch("P00001", Optional.<String>absent(),Optional.<String>absent(),Optional.<String>absent(), PeptideProteinMatch.HitType.DECOY);
        PeptideProteinMatch match2 = new PeptideProteinMatch("DECOY_P00001", Optional.<String>absent(),Optional.<String>absent(),Optional.<String>absent(), PeptideProteinMatch.HitType.DECOY);
        PeptideProteinMatch match3 = new PeptideProteinMatch("P00002", Optional.<String>absent(),Optional.<String>absent(),Optional.<String>absent(), PeptideProteinMatch.HitType.UNKNOWN);

        peptideMatch.addProteinMatch(match1);
        peptideMatch.addProteinMatch(match2);
        peptideMatch.addProteinMatch(match3);

        Assert.assertTrue(peptideMatch.containsProtein("P00001"));
        Assert.assertTrue(peptideMatch.containsProtein("P00002"));
        Assert.assertFalse(peptideMatch.containsProtein("P00003"));

        Assert.assertTrue(peptideMatch.containsProteinMatch("^P0.*"));
        Assert.assertFalse(peptideMatch.containsOnlyProteinMatch("^P00"));

        Pattern decoyPattern = Pattern.compile("DECOY_");

        Assert.assertTrue(peptideMatch.containsProteinMatch(decoyPattern));
        Assert.assertFalse(peptideMatch.containsOnlyProteinMatch(decoyPattern));

    }
}