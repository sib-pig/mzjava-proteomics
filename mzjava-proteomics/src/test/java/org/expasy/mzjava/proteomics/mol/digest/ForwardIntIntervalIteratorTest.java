package org.expasy.mzjava.proteomics.mol.digest;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * @author fnikitin
 * Date: 10/31/13
 */
public class ForwardIntIntervalIteratorTest {

    @Test
    public void test() {

        Iterator<IntIntervalCursor> iterator = new ForwardIntIntervalIterator(0, 4);

        List<Integer> expectedFirsts = Arrays.asList(0,0,0,0,1,1,1,2,2,3);
        List<Integer> expectedSeconds = Arrays.asList(1,2,3,4,2,3,4,3,4,4);

        int count=0;
        while (iterator.hasNext()) {

            IntIntervalCursor interval = iterator.next();

            Assert.assertEquals(expectedFirsts.get(count).intValue(), interval.getCurrentLowerBound());
            Assert.assertEquals(expectedSeconds.get(count).intValue(), interval.getCurrentUpperBound());

            count++;
        }

        Assert.assertEquals(10, count);
    }

    @Test
    public void test2() {

        Iterator<IntIntervalCursor> iterator = new ForwardIntIntervalIterator(0, 4, 5);

        List<Integer> expectedFirsts = Arrays.asList(0,0,0,0,1,1,1,2,2,3);
        List<Integer> expectedSeconds = Arrays.asList(1,2,3,4,2,3,4,3,4,4);

        int count=0;
        while (iterator.hasNext()) {

            IntIntervalCursor interval = iterator.next();

            Assert.assertEquals(expectedFirsts.get(count).intValue(), interval.getCurrentLowerBound());
            Assert.assertEquals(expectedSeconds.get(count).intValue(), interval.getCurrentUpperBound());

            count++;
        }

        Assert.assertEquals(10, count);
    }

    @Test
    public void testNoPairConsumedInLoop() {

        Iterator<IntIntervalCursor> iterator = new ForwardIntIntervalIterator(0, 4);

        // consume first interval
        IntIntervalCursor interval = iterator.next();

        Assert.assertEquals(0, interval.getCurrentLowerBound());
        Assert.assertEquals(1, interval.getCurrentUpperBound());

        int count=0;
        while (iterator.hasNext()) {

            count++;

            if (count == 15)
                break;
        }

        Assert.assertEquals(15, count);

        // consume second interval
        interval = iterator.next();

        Assert.assertEquals(0, interval.getCurrentLowerBound());
        Assert.assertEquals(2, interval.getCurrentUpperBound());

    }

    @Test
    public void testNoHasNext() {

        Iterator<IntIntervalCursor> iterator = new ForwardIntIntervalIterator(0, 4);

        List<Integer> expectedFirsts = Arrays.asList(0,0,0,0,1,1,1,2,2,3,-1,-1,-1,-1,-1);
        List<Integer> expectedSeconds = Arrays.asList(1,2,3,4,2,3,4,3,4,4,-1,-1,-1,-1,-1);

        int count=0;
        while (true) {

            IntIntervalCursor interval = iterator.next();

            Assert.assertEquals(expectedFirsts.get(count).intValue(), interval.getCurrentLowerBound());
            Assert.assertEquals(expectedSeconds.get(count).intValue(), interval.getCurrentUpperBound());

            count++;

            if (count == 15)
                break;
        }

        Assert.assertEquals(15, count);
    }

    @Test
    public void testWithDistConstraint() {

        Iterator<IntIntervalCursor> iterator = new ForwardIntIntervalIterator(0, 4, 2);

        List<Integer> expectedFirsts = Arrays.asList(0,0,1,1,2,2,3);
        List<Integer> expectedSeconds = Arrays.asList(1,2,2,3,3,4,4);

        int count=0;
        while (iterator.hasNext()) {

            IntIntervalCursor interval = iterator.next();

            Assert.assertEquals(expectedFirsts.get(count).intValue(), interval.getCurrentLowerBound());
            Assert.assertEquals(expectedSeconds.get(count).intValue(), interval.getCurrentUpperBound());

            count++;
        }

        Assert.assertEquals(7, count);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testBadConstructor() {

        new ForwardIntIntervalIterator(1, 1);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testBadRangeConstraintConstructor() {

        new ForwardIntIntervalIterator(1, 2, 0);
    }
}
