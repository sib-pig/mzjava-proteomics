/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.ms.ident;

import org.expasy.mzjava.core.ms.spectrum.TimeUnit;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class SpectrumIdentifierTest {

    @Test
    public void testEquals() throws Exception {

        SpectrumIdentifier identifier1 = newIdentifier("348: Scan 881 (rt=13.2621) [\\\\wzw.tum.de\\ipag\\Data\\MS_Data\\PIPELINE\\Thermo_LTQ_ORBITRAP_XL\\raw\\290709_Pepmix4_HCD.RAW]",
                881, 795.7248, 1562.6445077846, 2, 347);
        SpectrumIdentifier identifier2 = newIdentifier("518: Scan 1085 (rt=16.1531) [\\\\wzw.tum.de\\ipag\\Data\\MS_Data\\PIPELINE\\Thermo_LTQ_ORBITRAP_XL\\raw\\290709_Pepmix4_HCD.RAW]",
                1085, 969.1855, 1737.8237077846, 2, 517);
        SpectrumIdentifier identifier3 = newIdentifier("348: Scan 881 (rt=13.2621) [\\\\wzw.tum.de\\ipag\\Data\\MS_Data\\PIPELINE\\Thermo_LTQ_ORBITRAP_XL\\raw\\290709_Pepmix4_HCD.RAW]",
                881, 795.7248, 1562.6445077846, 2, 347);

        Assert.assertEquals(true, identifier1.equals(identifier3));
        Assert.assertEquals(true, identifier1.equals(identifier1));
        Assert.assertEquals(false, identifier1.equals(identifier2));
    }

    @Test
    public void testHashCode() throws Exception {

        SpectrumIdentifier identifier1 = newIdentifier("348: Scan 881 (rt=13.2621) [\\\\wzw.tum.de\\ipag\\Data\\MS_Data\\PIPELINE\\Thermo_LTQ_ORBITRAP_XL\\raw\\290709_Pepmix4_HCD.RAW]",
                881, 795.7248, 1562.6445077846, 2, 347);
        SpectrumIdentifier identifier2 = newIdentifier("518: Scan 1085 (rt=16.1531) [\\\\wzw.tum.de\\ipag\\Data\\MS_Data\\PIPELINE\\Thermo_LTQ_ORBITRAP_XL\\raw\\290709_Pepmix4_HCD.RAW]",
                1085, 969.1855, 1737.8237077846, 2, 517);
        SpectrumIdentifier identifier3 = newIdentifier("348: Scan 881 (rt=13.2621) [\\\\wzw.tum.de\\ipag\\Data\\MS_Data\\PIPELINE\\Thermo_LTQ_ORBITRAP_XL\\raw\\290709_Pepmix4_HCD.RAW]",
                881, 795.7248, 1562.6445077846, 2, 347);

        Assert.assertEquals(identifier1.hashCode(), identifier1.hashCode());
        Assert.assertEquals(identifier1.hashCode(), identifier3.hashCode());
        Assert.assertNotEquals(identifier1.hashCode(), identifier2.hashCode());
    }

    private SpectrumIdentifier newIdentifier(String spectrum, int scanNumber, double retentionTimeSeconds, double precursorNeutralMass, int assumedCharge, int index) {

        SpectrumIdentifier identifier = new SpectrumIdentifier(spectrum);
        identifier.addScanNumber(scanNumber);
        identifier.addRetentionTime(retentionTimeSeconds, TimeUnit.SECOND);
        identifier.setPrecursorNeutralMass(precursorNeutralMass);
        identifier.setAssumedCharge(assumedCharge);
        identifier.setIndex(index);

        return identifier;
    }
}
