/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.io.ms.ident;

import com.google.common.base.Optional;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import junit.framework.TestCase;
import org.expasy.mzjava.core.ms.spectrum.ScanNumberDiscrete;
import org.expasy.mzjava.core.ms.spectrum.ScanNumberInterval;
import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.proteomics.ms.ident.*;
import org.junit.Assert;
import org.mockito.Mockito;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class PepXmlReaderTest extends TestCase {

    public void test() throws Exception {

        InputStream fr = new FileInputStream(new File(getClass().getResource("pep_xml.pep.xml").getFile()));

        PSMReaderCallback callback = mock(PSMReaderCallback.class);

        PepXmlReader reader = new PepXmlReader(PepXmlReader.ModMassStorage.AA_MASS_PLUS_MOD_MASS, false);
        reader.parse(fr, callback);

        final String expectedPeptide = "ATLEEAEGESGVEDDAATGSSNK";

        final SpectrumIdentifier expectedIdentifier = new SpectrumIdentifier("chludwig_B1103_063.03375.03375.2");
        expectedIdentifier.addScanNumber(new ScanNumberDiscrete(1729));
        expectedIdentifier.setPrecursorNeutralMass(2345.94580078125);
        expectedIdentifier.setAssumedCharge(2);
        expectedIdentifier.setIndex(1);

        final PeptideMatch expectedPeptideMatch = new PeptideMatch(expectedPeptide);
        double mass = 166.9980 - AminoAcid.valueOf('S').getMassOfMonomer();
        expectedPeptideMatch.addModificationMatch(9, new ModificationMatch(mass, AminoAcid.S, 9, ModAttachment.SIDE_CHAIN));
        expectedPeptideMatch.setRank(Optional.of(1));
        expectedPeptideMatch.setNumMatchedIons(Optional.of(33));
        expectedPeptideMatch.setTotalNumIons(Optional.of(44));
        expectedPeptideMatch.setMassDiff(Optional.of(0.0080));
        expectedPeptideMatch.setNumMissedCleavages(Optional.of(0));
        expectedPeptideMatch.setRejected(Optional.of(false));
        expectedPeptideMatch.addScore("ascore", 0.0);
        expectedPeptideMatch.addScore("bscore", 9.7);
        expectedPeptideMatch.addScore("nextscore", 72.0);
        expectedPeptideMatch.addScore("yscore", 11.1);
        expectedPeptideMatch.addScore("cscore", 0.0);
        expectedPeptideMatch.addScore("xscore", 0.0);
        expectedPeptideMatch.addScore("zscore", 0.0);
        expectedPeptideMatch.addScore("hyperscore", 123.4);
        expectedPeptideMatch.addScore("expect", 7.4E-12);

        expectedPeptideMatch.addProteinMatch(new PeptideProteinMatch("YLR196W",  Optional.<String>absent(), Optional.of("A"), Optional.of("K"), PeptideProteinMatch.HitType.UNKNOWN));

        verify(callback).resultRead(expectedIdentifier, expectedPeptideMatch);
    }

    public void testWithResolver() throws Exception {

        InputStream fr = new FileInputStream(new File(getClass().getResource("pep_xml.pep.xml").getFile()));

        PSMReaderCallback callback = mock(PSMReaderCallback.class);

        ModificationMatchResolver modResolver = mock(ModificationMatchResolver.class);
        when(modResolver.resolve(Mockito.any(ModificationMatch.class))).thenReturn(Optional.of(Modification.parseModification("HPO3")));

        PepXmlReader reader = new PepXmlReader(PepXmlReader.ModMassStorage.AA_MASS_PLUS_MOD_MASS, false, modResolver);
        reader.parse(fr, callback);

        final String expectedPeptide = "ATLEEAEGESGVEDDAATGSSNK";

        final SpectrumIdentifier expectedIdentifier = new SpectrumIdentifier("chludwig_B1103_063.03375.03375.2");
        expectedIdentifier.addScanNumber(new ScanNumberDiscrete(1729));
        expectedIdentifier.setPrecursorNeutralMass(2345.94580078125);
        expectedIdentifier.setAssumedCharge(2);
        expectedIdentifier.setIndex(1);

        final PeptideMatch expectedPeptideMatch = new PeptideMatch(expectedPeptide);
        double mass = 166.9980 - AminoAcid.valueOf('S').getMassOfMonomer();
        ModificationMatch modificationMatch = new ModificationMatch(mass, AminoAcid.S, 9, ModAttachment.SIDE_CHAIN);
        modificationMatch.addPotentialModification(Modification.parseModification("HPO3"));
        expectedPeptideMatch.addModificationMatch(9, modificationMatch);
        expectedPeptideMatch.setRank(Optional.of(1));
        expectedPeptideMatch.setNumMatchedIons(Optional.of(33));
        expectedPeptideMatch.setTotalNumIons(Optional.of(44));
        expectedPeptideMatch.setMassDiff(Optional.of(0.0080));
        expectedPeptideMatch.setNumMissedCleavages(Optional.of(0));
        expectedPeptideMatch.setRejected(Optional.of(false));
        expectedPeptideMatch.addScore("ascore", 0.0);
        expectedPeptideMatch.addScore("bscore", 9.7);
        expectedPeptideMatch.addScore("nextscore", 72.0);
        expectedPeptideMatch.addScore("yscore", 11.1);
        expectedPeptideMatch.addScore("cscore", 0.0);
        expectedPeptideMatch.addScore("xscore", 0.0);
        expectedPeptideMatch.addScore("zscore", 0.0);
        expectedPeptideMatch.addScore("hyperscore", 123.4);
        expectedPeptideMatch.addScore("expect", 7.4E-12);

        expectedPeptideMatch.addProteinMatch(new PeptideProteinMatch("YLR196W", Optional.<String>absent(), Optional.of("A"), Optional.of("K"), PeptideProteinMatch.HitType.UNKNOWN));

        verify(callback).resultRead(expectedIdentifier, expectedPeptideMatch);
    }

    public void testMultipleRanks() throws Exception {


        final Multimap<String,PeptideMatch> results = ArrayListMultimap.create();

        InputStream fr = new FileInputStream(new File(getClass().getResource("pep_xml_4.pep.xml").getFile()));

        PSMReaderCallback callback = new PSMReaderCallback() {
            @Override
            public void resultRead(SpectrumIdentifier identifier, PeptideMatch searchResult) {

                results.put(identifier.getSpectrum(),searchResult);
            }
        };

        PepXmlReader reader = new PepXmlReader(PepXmlReader.ModMassStorage.AA_MASS_PLUS_MOD_MASS, false);
        reader.parse(fr, callback);

        Assert.assertEquals(4, results.size());

        Collection<PeptideMatch> psms = results.values();

        for (PeptideMatch psm : psms) {
            if (psm.getRank().get()==1)
                Assert.assertEquals(3,psm.getProteinMatches().size());
            if (psm.getRank().get()==2)
                Assert.assertEquals(5,psm.getProteinMatches().size());
            if (psm.getRank().get()==3)
                Assert.assertEquals(2,psm.getProteinMatches().size());
            if (psm.getRank().get()==4)
                Assert.assertEquals(1,psm.getProteinMatches().size());
        }

    }

    public void testFixedModif() throws Exception {

        InputStream fr = new FileInputStream(new File(getClass().getResource("fixed_modif.pep.xml").getFile()));

        PSMReaderCallback callback = mock(PSMReaderCallback.class);

        PepXmlReader reader = new PepXmlReader(PepXmlReader.ModMassStorage.AA_MASS_PLUS_MOD_MASS, false);
        reader.parse(fr, callback);

    }

    public void testDirectModRepresentation() throws Exception {

        InputStream fr = new FileInputStream(new File(getClass().getResource("pep_xml_direct_mod.pep.xml").getFile()));

        PSMReaderCallback callback = mock(PSMReaderCallback.class);

        PepXmlReader reader = new PepXmlReader(PepXmlReader.ModMassStorage.MOD_MASS, true);
        reader.parse(fr, callback);


        //PSM1

        SpectrumIdentifier expectedIdentifier = new SpectrumIdentifier("NK_RPLC_D1.raw31");
        expectedIdentifier.addScanNumber(new ScanNumberDiscrete(691));
        expectedIdentifier.setPrecursorIntensity(10945.2763671875);
        expectedIdentifier.setPrecursorNeutralMass(1152.5164794921875);
        expectedIdentifier.setAssumedCharge(2);
        expectedIdentifier.setIndex(32);

        String expectedPeptide = "KAMEGAGTDEK";

        PeptideMatch expectedPeptideMatch = new PeptideMatch(expectedPeptide);
        expectedPeptideMatch.addModificationMatch(2, new ModificationMatch(15.994915, AminoAcid.M, 2, ModAttachment.SIDE_CHAIN));
        expectedPeptideMatch.setRank(Optional.of(1));
        expectedPeptideMatch.setNumMatchedIons(Optional.of(7));
        expectedPeptideMatch.setMassDiff(Optional.of(0.0));
        expectedPeptideMatch.setRejected(Optional.of(false));
        expectedPeptideMatch.addScore("IonScore", 62.5);
        expectedPeptideMatch.addScore("Exp Value", 3.9363892763324449E-06);

        expectedPeptideMatch.addProteinMatch(new PeptideProteinMatch("71773329",  Optional.<String>absent(), Optional.<String>absent(), Optional.<String>absent(), PeptideProteinMatch.HitType.UNKNOWN));
        expectedPeptideMatch.addProteinMatch(new PeptideProteinMatch("302129652",  Optional.<String>absent(), Optional.<String>absent(), Optional.<String>absent(), PeptideProteinMatch.HitType.UNKNOWN));

        verify(callback).resultRead(expectedIdentifier, expectedPeptideMatch);


        //PSM2

        expectedIdentifier = new SpectrumIdentifier("NK_RPLC_D1.raw33");
        expectedIdentifier.addScanNumber(new ScanNumberDiscrete(702));
        expectedIdentifier.setPrecursorIntensity(16263.3720703125);
        expectedIdentifier.setPrecursorNeutralMass(1129.5494384765625);
        expectedIdentifier.setAssumedCharge(2);
        expectedIdentifier.setIndex(34);

        expectedPeptide = "QKAEADKNDK";

        expectedPeptideMatch = new PeptideMatch(expectedPeptide);
        expectedPeptideMatch.addModificationMatch(ModAttachment.N_TERM, -17.026549);
        expectedPeptideMatch.setRank(Optional.of(1));
        expectedPeptideMatch.setNumMatchedIons(Optional.of(8));
        expectedPeptideMatch.setMassDiff(Optional.of(0.0));
        expectedPeptideMatch.setRejected(Optional.of(false));
        expectedPeptideMatch.addScore("IonScore", 60.84);
        expectedPeptideMatch.addScore("Exp Value", 9.7660366629040692E-06);

        expectedPeptideMatch.addProteinMatch(new PeptideProteinMatch("153792590",  Optional.<String>absent(), Optional.<String>absent(), Optional.<String>absent(), PeptideProteinMatch.HitType.UNKNOWN));
        expectedPeptideMatch.addProteinMatch(new PeptideProteinMatch("154146191",  Optional.<String>absent(), Optional.<String>absent(), Optional.<String>absent(), PeptideProteinMatch.HitType.UNKNOWN));
        expectedPeptideMatch.addProteinMatch(new PeptideProteinMatch("20149594",  Optional.<String>absent(), Optional.<String>absent(), Optional.<String>absent(), PeptideProteinMatch.HitType.UNKNOWN));

        verify(callback).resultRead(expectedIdentifier, expectedPeptideMatch);

        //PSM3

        expectedIdentifier = new SpectrumIdentifier("NK_RPLC_D1.raw34");
        expectedIdentifier.addScanNumber(new ScanNumberInterval(712, 716));
        expectedIdentifier.setPrecursorIntensity(41857.39453125);
        expectedIdentifier.setPrecursorNeutralMass(1252.5701904296875);
        expectedIdentifier.setAssumedCharge(2);
        expectedIdentifier.setIndex(35);

        expectedPeptide = "QRDEKDEHGR";

        expectedPeptideMatch = new PeptideMatch(expectedPeptide);
        expectedPeptideMatch.addModificationMatch(ModAttachment.C_TERM, 17.026549);
        expectedPeptideMatch.setRank(Optional.of(1));
        expectedPeptideMatch.setNumMatchedIons(Optional.of(8));
        expectedPeptideMatch.setMassDiff(Optional.of(0.0));
        expectedPeptideMatch.setRejected(Optional.of(false));
        expectedPeptideMatch.addScore("IonScore", 69.11);
        expectedPeptideMatch.addScore("Exp Value", 1.0740093272636073E-06);

        expectedPeptideMatch.addProteinMatch(new PeptideProteinMatch("118601081",  Optional.<String>absent(), Optional.<String>absent(), Optional.<String>absent(), PeptideProteinMatch.HitType.UNKNOWN));

        verify(callback).resultRead(expectedIdentifier, expectedPeptideMatch);

        verifyNoMoreInteractions(callback);
    }

    public void test2() throws Exception {

        InputStream fr = new FileInputStream(new File(getClass().getResource("pep_xml_2.pep.xml").getFile()));

        PSMReaderCallback callback = mock(PSMReaderCallback.class);

        PepXmlReader reader = new PepXmlReader(PepXmlReader.ModMassStorage.AA_MASS_PLUS_MOD_MASS, false);
        reader.parse(fr, callback);

        verify(callback, times(2)).resultRead(any(SpectrumIdentifier.class), any(PeptideMatch.class));
    }

    public void testPeprmlXml() throws Exception {

        InputStream fr = new FileInputStream(new File(getClass().getResource("pep_xml_3.pep.xml").getFile()));

        MockPSMReaderCallback callback = new MockPSMReaderCallback();

        PepXmlReader reader = new PepXmlReader(PepXmlReader.ModMassStorage.AA_MASS_PLUS_MOD_MASS, false);
        reader.parse(fr, callback);

        Assert.assertEquals(2, callback.idList.size());
    }

    /**
     * Tests that a xml without xmlns="http://regis-web.systemsbiology.net/pepXML" as an attribute in msms_pipeline_analysis
     * is read correctly.
     *
     * @throws Exception
     */
    public void testNoXmlNs() throws Exception {

        InputStream fr = new FileInputStream(new File(getClass().getResource("pep_xml_no_xmlns.pep.xml").getFile()));

        PSMReaderCallback callback = mock(PSMReaderCallback.class);

        PepXmlReader reader = new PepXmlReader(PepXmlReader.ModMassStorage.AA_MASS_PLUS_MOD_MASS, false);
        reader.parse(fr, callback);

        verify(callback, times(2)).resultRead(any(SpectrumIdentifier.class), any(PeptideMatch.class));
    }

    /**
     * XML files are supposed to be UTF encoded but some files that we have are Cp1252 encoded.
     * Cp1252 encoding is a java encoding.
     *
     * @throws Exception
     */
    public void testCp1252Encoding() throws Exception {

        InputStream fr = new FileInputStream(new File(getClass().getResource("pep_xml_cp1252.pep.xml").getFile()));

        PSMReaderCallback callback = mock(PSMReaderCallback.class);

        PepXmlReader reader = new PepXmlReader(PepXmlReader.ModMassStorage.AA_MASS_PLUS_MOD_MASS, false);
        reader.parse(fr, callback);

        verify(callback, times(2)).resultRead(any(SpectrumIdentifier.class), any(PeptideMatch.class));
    }

    private class MockPSMReaderCallback implements PSMReaderCallback {

        List<SpectrumIdentifier> idList = new ArrayList<>();
        List<PeptideMatch> resultList = new ArrayList<>();

        @Override
        public void resultRead(SpectrumIdentifier identifier, PeptideMatch searchResult) {

            idList.add(identifier);
            resultList.add(searchResult);
        }
    }
}
