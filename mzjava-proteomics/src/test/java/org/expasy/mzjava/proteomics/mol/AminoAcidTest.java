/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.mol;

import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.core.mol.PeriodicTable;
import org.junit.Assert;
import org.junit.Test;

import java.util.EnumSet;
import java.util.Set;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class AminoAcidTest {

    @Test
    public void testValueOf() throws Exception {

        Assert.assertEquals(AminoAcid.A, AminoAcid.valueOf('A'));
        Assert.assertEquals(AminoAcid.A, AminoAcid.valueOf('a'));
        Assert.assertEquals(AminoAcid.R, AminoAcid.valueOf('R'));
        Assert.assertEquals(AminoAcid.R, AminoAcid.valueOf('r'));
        Assert.assertEquals(AminoAcid.N, AminoAcid.valueOf('N'));
        Assert.assertEquals(AminoAcid.N, AminoAcid.valueOf('n'));
        Assert.assertEquals(AminoAcid.D, AminoAcid.valueOf('D'));
        Assert.assertEquals(AminoAcid.D, AminoAcid.valueOf('d'));
        Assert.assertEquals(AminoAcid.C, AminoAcid.valueOf('C'));
        Assert.assertEquals(AminoAcid.C, AminoAcid.valueOf('c'));
        Assert.assertEquals(AminoAcid.E, AminoAcid.valueOf('E'));
        Assert.assertEquals(AminoAcid.E, AminoAcid.valueOf('e'));
        Assert.assertEquals(AminoAcid.Q, AminoAcid.valueOf('Q'));
        Assert.assertEquals(AminoAcid.Q, AminoAcid.valueOf('q'));
        Assert.assertEquals(AminoAcid.G, AminoAcid.valueOf('G'));
        Assert.assertEquals(AminoAcid.G, AminoAcid.valueOf('g'));
        Assert.assertEquals(AminoAcid.H, AminoAcid.valueOf('H'));
        Assert.assertEquals(AminoAcid.H, AminoAcid.valueOf('h'));
        Assert.assertEquals(AminoAcid.K, AminoAcid.valueOf('K'));
        Assert.assertEquals(AminoAcid.K, AminoAcid.valueOf('k'));
        Assert.assertEquals(AminoAcid.M, AminoAcid.valueOf('M'));
        Assert.assertEquals(AminoAcid.M, AminoAcid.valueOf('m'));
        Assert.assertEquals(AminoAcid.F, AminoAcid.valueOf('F'));
        Assert.assertEquals(AminoAcid.F, AminoAcid.valueOf('f'));
        Assert.assertEquals(AminoAcid.P, AminoAcid.valueOf('P'));
        Assert.assertEquals(AminoAcid.P, AminoAcid.valueOf('p'));
        Assert.assertEquals(AminoAcid.S, AminoAcid.valueOf('S'));
        Assert.assertEquals(AminoAcid.S, AminoAcid.valueOf('s'));
        Assert.assertEquals(AminoAcid.T, AminoAcid.valueOf('T'));
        Assert.assertEquals(AminoAcid.T, AminoAcid.valueOf('t'));
        Assert.assertEquals(AminoAcid.W, AminoAcid.valueOf('W'));
        Assert.assertEquals(AminoAcid.W, AminoAcid.valueOf('w'));
        Assert.assertEquals(AminoAcid.Y, AminoAcid.valueOf('Y'));
        Assert.assertEquals(AminoAcid.Y, AminoAcid.valueOf('y'));
        Assert.assertEquals(AminoAcid.V, AminoAcid.valueOf('V'));
        Assert.assertEquals(AminoAcid.V, AminoAcid.valueOf('v'));
        Assert.assertEquals(AminoAcid.J, AminoAcid.valueOf('J'));
        Assert.assertEquals(AminoAcid.J, AminoAcid.valueOf('j'));
        Assert.assertEquals(AminoAcid.I, AminoAcid.valueOf('I'));
        Assert.assertEquals(AminoAcid.I, AminoAcid.valueOf('i'));
        Assert.assertEquals(AminoAcid.L, AminoAcid.valueOf('L'));
        Assert.assertEquals(AminoAcid.L, AminoAcid.valueOf('l'));
        Assert.assertEquals(AminoAcid.O, AminoAcid.valueOf('O'));
        Assert.assertEquals(AminoAcid.O, AminoAcid.valueOf('o'));
        Assert.assertEquals(AminoAcid.U, AminoAcid.valueOf('U'));
        Assert.assertEquals(AminoAcid.U, AminoAcid.valueOf('u'));
        Assert.assertEquals(AminoAcid.X, AminoAcid.valueOf('X'));
        Assert.assertEquals(AminoAcid.X, AminoAcid.valueOf('x'));
        Assert.assertEquals(AminoAcid.B, AminoAcid.valueOf('B'));
        Assert.assertEquals(AminoAcid.B, AminoAcid.valueOf('b'));
        Assert.assertEquals(AminoAcid.Z, AminoAcid.valueOf('Z'));
        Assert.assertEquals(AminoAcid.Z, AminoAcid.valueOf('z'));
    }

    @Test
    public void testComposition() throws Exception {

        Assert.assertEquals(57.021463723, AminoAcid.G.getMassOfMonomer(), 0.00000000001);
        Assert.assertEquals(71.037113787, AminoAcid.A.getMassOfMonomer(), 0.00000000001);
        Assert.assertEquals(87.03202840899999, AminoAcid.S.getMassOfMonomer(), 0.00000000001);
        Assert.assertEquals(97.05276385100001, AminoAcid.P.getMassOfMonomer(), 0.00000000001);
        Assert.assertEquals(99.06841391500001, AminoAcid.V.getMassOfMonomer(), 0.00000000001);
        Assert.assertEquals(101.047678473, AminoAcid.T.getMassOfMonomer(), 0.00000000001);
        Assert.assertEquals(103.009184487, AminoAcid.C.getMassOfMonomer(), 0.00000000001);
        Assert.assertEquals(113.084063979, AminoAcid.J.getMassOfMonomer(), 0.00000000001);
        Assert.assertEquals(114.042927446, AminoAcid.N.getMassOfMonomer(), 0.00000000001);
        Assert.assertEquals(115.026943031, AminoAcid.D.getMassOfMonomer(), 0.00000000001);
        Assert.assertEquals(128.05857751, AminoAcid.Q.getMassOfMonomer(), 0.00000000001);
        Assert.assertEquals(128.094963016, AminoAcid.K.getMassOfMonomer(), 0.00000000001);
        Assert.assertEquals(129.042593095, AminoAcid.E.getMassOfMonomer(), 0.00000000001);
        Assert.assertEquals(131.040484615, AminoAcid.M.getMassOfMonomer(), 0.00000000001);
        Assert.assertEquals(137.058911861, AminoAcid.H.getMassOfMonomer(), 0.00000000001);
        Assert.assertEquals(147.068413915, AminoAcid.F.getMassOfMonomer(), 0.00000000001);
        Assert.assertEquals(156.101111026, AminoAcid.R.getMassOfMonomer(), 0.00000000001);
        Assert.assertEquals(163.063328537, AminoAcid.Y.getMassOfMonomer(), 0.00000000001);
        Assert.assertEquals(186.079312952, AminoAcid.W.getMassOfMonomer(), 0.00000000001);
        Assert.assertEquals(113.084063979, AminoAcid.I.getMassOfMonomer(), 0.00000000001);
        Assert.assertEquals(113.084063979, AminoAcid.L.getMassOfMonomer(), 0.00000000001);
        Assert.assertEquals(113.084063979, AminoAcid.J.getMassOfMonomer(), 0.00000000001);
    }

    @Test
    public void testGetSymbol() throws Exception {

        for(AminoAcid aa : AminoAcid.values()){

            Assert.assertEquals(aa.toString(), aa.getSymbol());
        }
    }

    @Test(expected = IllegalStateException.class)
    public void testUndefinedCompositionX() throws Exception {

        AminoAcid.X.getCompositionOfMonomer();
    }

    @Test(expected = IllegalStateException.class)
    public void testUndefinedCompositionB() throws Exception {

        AminoAcid.B.getCompositionOfMonomer();
    }

    @Test(expected = IllegalStateException.class)
    public void testUndefinedCompositionZ() throws Exception {

        AminoAcid.Z.getCompositionOfMonomer();
    }

    @Test
    public void testDefinedComposition() throws Exception {

        Set<AminoAcid> unDefined = EnumSet.of(AminoAcid.X, AminoAcid.B, AminoAcid.Z);
        Set<AminoAcid> defined = EnumSet.allOf(AminoAcid.class);
        defined.removeAll(unDefined);

        for(AminoAcid aa : defined) {

            Assert.assertEquals(true, aa.isUnambiguous());
        }

        for(AminoAcid aa : unDefined) {

            Assert.assertEquals(false, aa.isUnambiguous());
        }
    }

    @Test(expected = IllegalStateException.class)
    public void testGetMassOnUndefined() throws Exception {

        AminoAcid.X.getMassOfMonomer();
    }

    @Test(expected = IllegalStateException.class)
    public void testGetCompositionOnUndefined() throws Exception {

        AminoAcid.X.getCompositionOfMonomer();
    }

    @Test
    public void testGetCompositionOnDefined() throws Exception {

        Assert.assertEquals(AminoAcid.I.getCompositionOfMonomer(), AminoAcid.L.getCompositionOfMonomer());
    }

    @Test
    public void testAAComposition() throws Exception {

        Composition comp = AminoAcid.R.getCompositionOfMonomer();

        Assert.assertEquals(6, comp.getCount(PeriodicTable.C));
        Assert.assertEquals(12, comp.getCount(PeriodicTable.H));
        Assert.assertEquals(4, comp.getCount(PeriodicTable.N));
        Assert.assertEquals(1, comp.getCount(PeriodicTable.O));
    }

    @Test (expected = IllegalStateException.class)
    public void testAAUnknownComposition() throws Exception {

        AminoAcid.Z.getCompositionOfMonomer();
    }
}
