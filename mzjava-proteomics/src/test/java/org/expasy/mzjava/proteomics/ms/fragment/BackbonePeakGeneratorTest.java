/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.ms.fragment;

import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.mol.PeptideFragment;
import org.expasy.mzjava.core.ms.spectrum.AnnotatedPeak;
import org.expasy.mzjava.core.ms.spectrum.FragmentType;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.proteomics.ms.spectrum.PepFragAnnotation;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class BackbonePeakGeneratorTest {

    @Test
    public void testGeneratePeaks() throws Exception {

        Peptide precursor = Peptide.parse("GGYDSPR");
        PeptideFragment fragment = PeptideFragment.parse("GGY", FragmentType.FORWARD);

        BackbonePeakGenerator peakGenerator = new BackbonePeakGenerator(EnumSet.of(IonType.b), 1);

        List<AnnotatedPeak<PepFragAnnotation>> peaks = peakGenerator.generatePeaks(
                precursor, fragment, new int[]{1, 3}, null);
        Collections.sort(peaks, new Comparator<AnnotatedPeak<PepFragAnnotation>>() {
            @Override
            public int compare(AnnotatedPeak<PepFragAnnotation> p1, AnnotatedPeak<PepFragAnnotation> p2) {

                return Double.compare(p1.getMz(), p2.getMz());
            }
        });

        AnnotatedPeak<PepFragAnnotation> expectedPeak1 = new AnnotatedPeak<PepFragAnnotation>(93.37602810203333, 1, 3, new PepFragAnnotation(IonType.b, 3, Peptide.parse("GGY")));
        AnnotatedPeak<PepFragAnnotation> expectedPeak2 = new AnnotatedPeak<PepFragAnnotation>(278.1135320907, 1, 1, new PepFragAnnotation(IonType.b, 1, Peptide.parse("GGY")));
        Assert.assertEquals(2, peaks.size());
        Assert.assertEquals(expectedPeak1, peaks.get(0));
        Assert.assertEquals(expectedPeak2, peaks.get(1));
    }

    @Test
    public void testGeneratePeaks2() throws Exception {

        Peptide precursor = Peptide.parse("GGYDSPR");
        PeptideFragment fragment = precursor.createFragment(0, precursor.size(), FragmentType.INTACT);
        BackbonePeakGenerator peakGenerator = new BackbonePeakGenerator(EnumSet.of(IonType.p), 1);

        List<AnnotatedPeak<PepFragAnnotation>> peaks = peakGenerator.generatePeaks(
                precursor, fragment, new int[]{1}, null);

        Assert.assertEquals(1, peaks.size());

        AnnotatedPeak<PepFragAnnotation> peak = peaks.get(0);
        Assert.assertEquals(precursor.calculateMz(1), peak.getMz(), 0.000001);
        Assert.assertEquals(1, peak.getAnnotationCount());

        PepFragAnnotation annotation = peak.getAnnotation(0);
        Assert.assertEquals(IonType.p, annotation.getIonType());
        Assert.assertEquals(precursor.calculateMz(1), annotation.getTheoreticalMz(), 0.000001);
        Assert.assertEquals(precursor.size(), annotation.getFragment().size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyFragment() throws Exception {

        Peptide precursor = Peptide.parse("GGYDSPR");
        PeptideFragment empty = mock(PeptideFragment.class);
        when(empty.size()).thenReturn(0);
        when(empty.isEmpty()).thenReturn(true);

        BackbonePeakGenerator peakGenerator = new BackbonePeakGenerator(EnumSet.of(IonType.b), 1);

        peakGenerator.generatePeaks(precursor, empty, new int[]{1, 3}, null);
    }

    @Test(expected = NullPointerException.class)
    public void testNullFragment() throws Exception {

        Peptide precursor = Peptide.parse("GGYDSPR");

        BackbonePeakGenerator peakGenerator = new BackbonePeakGenerator(EnumSet.of(IonType.b), 1);

        peakGenerator.generatePeaks(precursor, null, new int[]{1, 3}, null);
    }

    @Test(expected = NullPointerException.class)
    public void testNullPrecursor() throws Exception {

        PeptideFragment fragment = PeptideFragment.parse("GGY", FragmentType.FORWARD);

        BackbonePeakGenerator peakGenerator = new BackbonePeakGenerator(EnumSet.of(IonType.b), 1);

        peakGenerator.generatePeaks(null, fragment, new int[]{1, 3}, null);
    }

    @Test(expected = NullPointerException.class)
    public void testNullCharge() throws Exception {

        Peptide precursor = Peptide.parse("GGYDSPR");
        PeptideFragment fragment = PeptideFragment.parse("GGY", FragmentType.FORWARD);

        BackbonePeakGenerator peakGenerator = new BackbonePeakGenerator(EnumSet.of(IonType.b), 1);

        peakGenerator.generatePeaks(precursor, fragment, null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyCharge() throws Exception {

        Peptide precursor = Peptide.parse("GGYDSPR");
        PeptideFragment fragment = PeptideFragment.parse("GGY", FragmentType.FORWARD);

        BackbonePeakGenerator peakGenerator = new BackbonePeakGenerator(EnumSet.of(IonType.b), 1);

        peakGenerator.generatePeaks(precursor, fragment, new int[]{}, null);
    }

    @Test
    public void testGetFragmentTypes() throws Exception {

        BackbonePeakGenerator peakGenerator = new BackbonePeakGenerator(EnumSet.of(IonType.b), 1);
        Assert.assertEquals(EnumSet.of(FragmentType.FORWARD), peakGenerator.getFragmentTypes());

        peakGenerator = new BackbonePeakGenerator(EnumSet.of(IonType.y), 1);
        Assert.assertEquals(EnumSet.of(FragmentType.REVERSE), peakGenerator.getFragmentTypes());

        peakGenerator = new BackbonePeakGenerator(EnumSet.of(IonType.i), 1);
        Assert.assertEquals(EnumSet.of(FragmentType.MONOMER), peakGenerator.getFragmentTypes());

        peakGenerator = new BackbonePeakGenerator(EnumSet.of(IonType.y, IonType.b), 1);
        Assert.assertEquals(EnumSet.of(FragmentType.REVERSE, FragmentType.FORWARD), peakGenerator.getFragmentTypes());
    }
}
