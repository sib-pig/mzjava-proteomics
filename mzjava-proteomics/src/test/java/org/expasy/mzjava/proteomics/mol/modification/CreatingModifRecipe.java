package org.expasy.mzjava.proteomics.mol.modification;

// # $RECIPE$ $NUMBER$ - Creating Modification #
//
// ## $PROBLEM$ ##
// You want to create *Modification*s.
//
// ## $SOLUTION$ ##
//
// A *Modification* is a labeled *Mass* that can be added to *AminoAcidSequence*.

import org.expasy.mzjava.core.mol.Mass;
import org.expasy.mzjava.core.mol.AtomicSymbol;
import org.expasy.mzjava.core.mol.Composition;
import org.junit.Assert;
import org.junit.Test;

public class CreatingModifRecipe {

// There are two ways to build an instance of *Modification* class,
// using the constructor with a label and a *Mass* (*NumericMass*, *Composition* and *NeutralLoss* are *Mass*es)
    @Test
    public void newModifFromMass() {

        //<SNIP>
        Modification mod = new Modification("phospho", new Composition.Builder().add(AtomicSymbol.H).add(AtomicSymbol.P)
                .add(AtomicSymbol.O, 3).build());
        //</SNIP>

        Assert.assertEquals(79.96633, mod.getMolecularMass(), 0.00001);
    }

// or using the static factory method parseModification from a string with the following format:
// (label:)?formula
    @Test
    public void newModifFromString1() {

        //<SNIP>
        Modification mod = Modification.parseModification("phospho:HPO3");
        //</SNIP>

        Assert.assertEquals(79.96633, mod.getMolecularMass(), 0.00001);
    }

// or the same method without label (in this case the label is the formula)
    @Test
    public void newModifFromString2() {

        //<SNIP>
        Modification mod = Modification.parseModification("HPO3");
        //</SNIP>

        Assert.assertEquals(79.96633, mod.getMolecularMass(), 0.00001);
    }

// here is a list of getters
    @Test
    public void getters() {

        Modification mod = Modification.parseModification("HPO3");
        //<SNIP>
        String label = mod.getLabel();
        Mass mass = mod.getMass();
        double molecularMass = mod.getMolecularMass();
        //</SNIP>

        Assert.assertEquals("HPO3", label);
    }
// ## $DISCUSSION$ ##

// ## $RELATED$ ##
// See also $CreatingMoleculeRecipe$
}