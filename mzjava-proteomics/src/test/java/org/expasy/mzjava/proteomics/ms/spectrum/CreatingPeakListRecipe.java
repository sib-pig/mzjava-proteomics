package org.expasy.mzjava.proteomics.ms.spectrum;

import org.expasy.mzjava.core.ms.peaklist.DoubleConstantPeakList;
import org.expasy.mzjava.core.ms.peaklist.DoublePeakList;
import org.expasy.mzjava.core.ms.peaklist.FloatPeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakCursor;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakListFactory;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.junit.Assert;
import org.junit.Test;

// # $RECIPE$ $NUMBER$ - Creating PeakList #

// ## $PROBLEM$ ##
// You want to make a new instance of *PeakList*.

// ## $SOLUTION$ ##

// A *PeakList* is an interface that handles a list of peaks defined by mz and intensity values with
// optional *PeakAnnotation*s.
//
// *Spectrum* is the base class that is used to represent the peak list and meta data associated with an msn
// spectrum. The peak list data is held by the classes that implement PeakList. For each peak an mz and
// intensity value as well as a list of peak annotations is stored. MzJava provides 5 flavors of PeakList
// which differ in the precision that is used to store the m/z and intensity values. M/z values are either
// stored in a float or double array and intensity values can be constant, an array of floats or an array
// of doubles. All implementations of PeakList guarantee that the peaks are sorted. Peak annotations are
// represented by classes that implement PeakAnnotation. The type of PeakAnnotation that a PeakList holds
// is specified using a generic. Each PeakList has a randomly generated UUID to simplify identification.

public class CreatingPeakListRecipe {

// We provide a bunch of *PeakList* implementations given the precision of m/zs and intensities.
//
// In the following, peaks are stored in a single-precision floating-point format:
    @Test
    public void singlePrecisionImpl() {

        // <SNIP>
        // single-precision floating-point m/zs and intensities
        PeakList<PeakAnnotation> peaklist = new FloatPeakList<PeakAnnotation>();
        // </SNIP>

        Assert.assertEquals(PeakList.Precision.FLOAT, peaklist.getPrecision());
        Assert.assertEquals(0, peaklist.size());
    }

// In the following, peaks are stored in a double-precision floating-point format:
    @Test
    public void doublePrecisionImpl() {

        // <SNIP>
        // double-precision floating-point m/zs and intensities
        PeakList<PeakAnnotation> peaklist = new DoublePeakList<PeakAnnotation>();
        // </SNIP>

        Assert.assertEquals(PeakList.Precision.DOUBLE, peaklist.getPrecision());
        Assert.assertEquals(0, peaklist.size());
    }

// Next are some implementations where peaks are stored in a mixed mz/intensity precision:
    @Test
    public void mixedPrecisionImpl() {

        // <SNIP>
        // double-precision floating-point m/zs and constant intensities of 1
        PeakList<PeakAnnotation> peaklist = new DoubleConstantPeakList<PeakAnnotation>(1);
        // </SNIP>

        Assert.assertEquals(PeakList.Precision.DOUBLE_CONSTANT, peaklist.getPrecision());
        Assert.assertEquals(0, peaklist.size());
    }

// We also provide factory to ease *PeakList* instanciation:
    @Test
    public void factoryImpl() {

        // <SNIP>
        PeakList<PeakAnnotation> peaklist = PeakListFactory.newPeakList(PeakList.Precision.DOUBLE_FLOAT);
        // </SNIP>

        Assert.assertEquals(PeakList.Precision.DOUBLE_FLOAT, peaklist.getPrecision());
        Assert.assertEquals(0, peaklist.size());
    }

// Adding peaks:
    @Test
    public void addingPeaks() {

        PeakList<PeakAnnotation> peaklist = new DoublePeakList<PeakAnnotation>();

        // <SNIP>
        peaklist.add(1, 2);
        // <SKIP>
        Assert.assertEquals(1, peaklist.size());
        // </SKIP>
        peaklist.add(2, 4);
        peaklist.add(3, 6);
        // </SNIP>

        Assert.assertEquals(3, peaklist.size());
        Assert.assertEquals(12, peaklist.getTotalIonCurrent(), 0.1);
    }

// Adding peak annotations:
    @Test
    public void addingAnnotations() {

        PeakList<PeakAnnotation> peaklist = new DoublePeakList<PeakAnnotation>();

        // <SNIP>
        peaklist.add(1, 2);
        peaklist.addAnnotation(0, new PepFragAnnotation(IonType.b, 2, Peptide.parse("CE")));
        // </SNIP>

        Assert.assertArrayEquals(new int[] {0}, peaklist.getAnnotationIndexes());
    }

// Traversing a Peaklist:
    @Test
    public void looping() {

        PeakList<PeakAnnotation> peaklist = new DoublePeakList<PeakAnnotation>();

        // <SNIP>
        for (int i=0 ; i<peaklist.size() ; i++) {

            peaklist.getMz(i);
            peaklist.getIntensity(i);
            peaklist.getAnnotations(i);
        }
        // </SNIP>
    }

// Manipulating a Peaklist using a cursor:
    @Test
    public void cursor() {

        // <SNIP>
        PeakList<PeakAnnotation> peaklist = new DoublePeakList<PeakAnnotation>();
        peaklist.addSorted(new double[] {101.0909, 202.1079, 203.1045, 244.128, 254.1056, 255.191, 270.2388, 272.2092},
                new double[]{15.4762, 21.4762, 5.3333, 14.381, 3.4286, 7.2381, 2.1905, 27.8095});

        PeakCursor<PeakAnnotation> cursor = peaklist.cursor();

        // move to next peak (peak.mz=101.0909)
        cursor.next();

        // <SKIP>
        Assert.assertEquals(101.0909, cursor.currMz(), 0.0001);
        Assert.assertEquals(15.4762, cursor.currIntensity(), 0.0001);
        // </SKIP>

        // move to next peak with constraint. Here the cursor moves as next peak.m/z <= 250 (current.mz=202.1079).
        cursor.next(250);
        // <SKIP>
        Assert.assertEquals(202.1079, cursor.currMz(), 0.0001);
        // </SKIP>

        // accessors
        cursor.currMz();
        cursor.currIntensity();
        cursor.currAnnotations();

        // move to next peak with constraint. Here the cursor does not move as next peak.m/z > 200 (current.mz=202.1079).
        cursor.next(200);
        // <SKIP>
        Assert.assertEquals(202.1079, cursor.currMz(), 0.0001);
        // </SKIP>

        // move to peak where m/z < 240 (current.mz=203.1045)
        cursor.moveBefore(240);
        // <SKIP>
        Assert.assertEquals(203.1045, cursor.currMz(), 0.0001);
        // </SKIP>

        // move to peak where m/z > 240 (current.mz=244.128)
        cursor.movePast(240);
        // <SKIP>
        Assert.assertEquals(244.128, cursor.currMz(), 0.0001);
        // </SKIP>

        // move to the closest peak (current.mz=202.1079)
        cursor.moveToClosest(200);
        // <SKIP>
        Assert.assertEquals(202.1079, cursor.currMz(), 0.0001);
        // </SKIP>

        // methods to retrieve a peak that is at n peaks distant from the current position without moving the cursor.
        if (cursor.canPeek(4)) {

            // here peeked peak has mz 255.191 and intensity 7.2381
            double mz = cursor.peekMz(4);
            double intensity = cursor.peekIntensity(4);

            // <SKIP>
            Assert.assertEquals(255.191, mz, 0.0001);
            Assert.assertEquals(7.2381, intensity, 0.0001);
            // </SKIP>
        }
        // </SNIP>
    }

// ## $DISCUSSION$ ##

// ## $RELATED$ ##

// See also $CreatingPeakAnnotationRecipe$
}