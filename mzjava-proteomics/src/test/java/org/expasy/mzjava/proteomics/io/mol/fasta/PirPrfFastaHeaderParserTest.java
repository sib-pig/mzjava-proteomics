package org.expasy.mzjava.proteomics.io.mol.fasta;

import org.expasy.mzjava.proteomics.mol.Protein;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author fnikitin
 * Date: 10/5/12
 */
public class PirPrfFastaHeaderParserTest {

	@Test
	public void testPir() throws Exception {

		FastaHeaderParser headerParser = new PirPrfFastaHeaderParser();

        Protein.Builder builder = new Protein.Builder("PROTEIN");

		boolean parsed =
				headerParser.parseHeader("pir||acc", builder);

		Assert.assertEquals(true, parsed);
		Assert.assertEquals("acc", builder.build().getAccessionId());
	}

    @Test
    public void testPir2() throws Exception {

        FastaHeaderParser headerParser = new PirPrfFastaHeaderParser();

        Protein.Builder builder = new Protein.Builder("PROTEIN");

        boolean parsed =
                headerParser.parseHeader("> pir||acc", builder);

        Assert.assertEquals(true, parsed);
        Assert.assertEquals("acc", builder.build().getAccessionId());
    }


    @Test
	public void testPrf() throws Exception {

		FastaHeaderParser headerParser = new PirPrfFastaHeaderParser();

		Protein.Builder sequence = new Protein.Builder("PROTEIN");

		boolean parsed =
				headerParser.parseHeader("prf||acc", sequence);

		Assert.assertEquals(true, parsed);
		Assert.assertEquals("acc", sequence.build().getAccessionId());
	}

    @Test
    public void testPrf2() throws Exception {

        FastaHeaderParser headerParser = new PirPrfFastaHeaderParser();

        Protein.Builder sequence = new Protein.Builder("PROTEIN");

        boolean parsed =
                headerParser.parseHeader(">prf||acc", sequence);

        Assert.assertEquals(true, parsed);
        Assert.assertEquals("acc", sequence.build().getAccessionId());
    }
}
