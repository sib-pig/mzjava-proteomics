package org.expasy.mzjava.proteomics.io.ms.spectrum;

import org.expasy.mzjava.core.io.IterativeReader;
import org.expasy.mzjava.core.io.ms.spectrum.MgfReader;
import org.expasy.mzjava.core.io.ms.spectrum.MzxmlReader;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

/**
 * User: fnikitin
 * Date: 7/6/12
 * Time: 9:56 AM
 */
public class MsReaderFactoryTest {

	@Test
	public void testDta() throws Exception {

        File file = new File(getClass().getResource("dta_test.dta").getFile());

        IterativeReader reader = MsReaderFactory.newMsnSpectraReader(file, PeakList.Precision.DOUBLE);

		Assert.assertTrue(reader instanceof DtaReader);
	}

	@Test
	public void testMgf() throws Exception {

        File file = new File(getClass().getResource("mgf_test.mgf").getFile());

        IterativeReader reader = MsReaderFactory.newMsnSpectraReader(file, PeakList.Precision.DOUBLE);

		Assert.assertTrue(reader instanceof MgfReader);
	}

	@Test
	public void testMzxml() throws Exception {

        File file = new File(getClass().getResource("mzxml_test.mzXML").getFile());

        IterativeReader reader = MsReaderFactory.newMsnSpectraReader(file, PeakList.Precision.DOUBLE);

		Assert.assertTrue(reader instanceof MzxmlReader);
	}

	@Test
	public void testMsp() throws Exception {

        File file = new File(getClass().getResource("msp_test.msp").getFile());

        IterativeReader<PeptideConsensusSpectrum> reader =
                MsReaderFactory.newLibrarySpectraReader(file, PeakList.Precision.DOUBLE);

		Assert.assertTrue(reader instanceof MspReader);
	}

	@Test
	public void testSptxt() throws Exception {

        File file = new File(getClass().getResource("sptxt_test.sptxt").getFile());

        IterativeReader<PeptideConsensusSpectrum> reader =
                MsReaderFactory.newLibrarySpectraReader(file, PeakList.Precision.DOUBLE);

		Assert.assertTrue(reader instanceof SptxtReader);
	}

    @Test(expected = IllegalStateException.class)
    public void testUnknownMsnFile() throws Exception {

        MsReaderFactory.newMsnSpectraReader(new File("/user/file.abc"), PeakList.Precision.DOUBLE);
    }

    @Test(expected = IllegalStateException.class)
    public void testUnknownLibFile() throws Exception {

        MsReaderFactory.newLibrarySpectraReader(new File("/user/file.abc"), PeakList.Precision.DOUBLE);
    }
}
