package org.expasy.mzjava.proteomics.mol;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.expasy.mzjava.core.mol.SymbolSequence;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

import static org.expasy.mzjava.proteomics.mol.AminoAcid.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AminoAcidSequenceTest {

    @Test
    public void testGetSymbolIndexes() throws Exception {

        AminoAcidSequence sequence = new MockAminoAcidSequence(new AminoAcid[]{C, E, R, V, I, L, A, S, A});
        Assert.assertArrayEquals(new int[]{6, 8}, sequence.getSymbolIndexes(A));
    }

    @Test
    public void testArrayConstructor() throws Exception {

        AminoAcidSequence sequence = new MockAminoAcidSequence(new AminoAcid[]{C, E, R, V, I, L, A, S});
        Assert.assertEquals(C, sequence.getSymbol(0));
        Assert.assertEquals(E, sequence.getSymbol(1));
        Assert.assertEquals(R, sequence.getSymbol(2));
        Assert.assertEquals(V, sequence.getSymbol(3));
        Assert.assertEquals(I, sequence.getSymbol(4));
        Assert.assertEquals(L, sequence.getSymbol(5));
        Assert.assertEquals(A, sequence.getSymbol(6));
        Assert.assertEquals(S, sequence.getSymbol(7));
    }

    @Test
    public void testVarargConstructor() throws Exception {

        AminoAcidSequence sequence = new MockAminoAcidSequence(C, E, R, V, I, L, A, S);
        Assert.assertEquals(C, sequence.getSymbol(0));
        Assert.assertEquals(E, sequence.getSymbol(1));
        Assert.assertEquals(R, sequence.getSymbol(2));
        Assert.assertEquals(V, sequence.getSymbol(3));
        Assert.assertEquals(I, sequence.getSymbol(4));
        Assert.assertEquals(L, sequence.getSymbol(5));
        Assert.assertEquals(A, sequence.getSymbol(6));
        Assert.assertEquals(S, sequence.getSymbol(7));
    }

    @Test
    public void testCopyConstructor1() throws Exception {

        final Modification mod0 = mockMod("mod0", 40);
        final Modification mod3_1 = mockMod("mod3_1", 31);
        final Modification mod3_2 = mockMod("mod3_2", 32);
        final Modification mod5 = mockMod("mod5", 5);

        final Modification nTerm1 = mockMod("n_term_1", 56);
        final Modification nTerm2 = mockMod("n_term_2", 52);
        final Modification cTerm = mockMod("c_term", .87);

        Multimap<Integer, Modification> sidechainModMap = ArrayListMultimap.create();
        sidechainModMap.put(0, mod0);
        sidechainModMap.put(3, mod3_1);
        sidechainModMap.put(3, mod3_2);
        sidechainModMap.put(5, mod5);

        Multimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, nTerm1);
        termModMap.put(ModAttachment.N_TERM, nTerm2);
        termModMap.put(ModAttachment.C_TERM, cTerm);

        AminoAcidSequence src = new MockAminoAcidSequence(Arrays.asList(C, E, R, V, I, L, A, S), sidechainModMap, termModMap);
        AminoAcidSequence dest = new MockAminoAcidSequence(src, 0, 8);

        Assert.assertEquals(8, dest.size());
        Assert.assertEquals(Arrays.asList(nTerm1, nTerm2), dest.getModifications(EnumSet.of(ModAttachment.N_TERM)));
        Assert.assertEquals(Arrays.asList(cTerm), dest.getModifications(EnumSet.of(ModAttachment.C_TERM)));
        Assert.assertEquals(Arrays.asList(mod0), dest.getModificationsAt(0, EnumSet.of(ModAttachment.SIDE_CHAIN)));
        Assert.assertEquals(Arrays.asList(mod3_1, mod3_2), dest.getModificationsAt(3, EnumSet.of(ModAttachment.SIDE_CHAIN)));
        Assert.assertEquals(Arrays.asList(mod5), dest.getModificationsAt(5, EnumSet.of(ModAttachment.SIDE_CHAIN)));
    }

    @Test
    public void testCopyConstructor2() throws Exception {

        final Modification mod0 = mockMod("mod0", 40);
        final Modification mod3_1 = mockMod("mod3_1", 31);
        final Modification mod3_2 = mockMod("mod3_2", 32);
        final Modification mod5 = mockMod("mod5", 5);

        final Modification nTerm1 = mockMod("n_term_1", 56);
        final Modification nTerm2 = mockMod("n_term_2", 52);
        final Modification cTerm = mockMod("c_term", .87);

        Multimap<Integer, Modification> sidechainModMap = ArrayListMultimap.create();
        sidechainModMap.put(0, mod0);
        sidechainModMap.put(3, mod3_1);
        sidechainModMap.put(3, mod3_2);
        sidechainModMap.put(5, mod5);

        Multimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, nTerm1);
        termModMap.put(ModAttachment.N_TERM, nTerm2);
        termModMap.put(ModAttachment.C_TERM, cTerm);

        AminoAcidSequence src = new MockAminoAcidSequence(Arrays.asList(C, E, R, V, I, L, A, S), sidechainModMap, termModMap);
        AminoAcidSequence dest = new MockAminoAcidSequence(src, 1, 8);

        Assert.assertEquals(7, dest.size());
        Assert.assertEquals(Collections.<Modification>emptyList(), dest.getModifications(EnumSet.of(ModAttachment.N_TERM)));
        Assert.assertEquals(Arrays.asList(cTerm), dest.getModifications(EnumSet.of(ModAttachment.C_TERM)));
        Assert.assertEquals(Collections.<Modification>emptyList(), dest.getModificationsAt(0, EnumSet.of(ModAttachment.SIDE_CHAIN)));
        Assert.assertEquals(Arrays.asList(mod3_1, mod3_2), dest.getModificationsAt(2, EnumSet.of(ModAttachment.SIDE_CHAIN)));
        Assert.assertEquals(Arrays.asList(mod5), dest.getModificationsAt(4, EnumSet.of(ModAttachment.SIDE_CHAIN)));
    }

    /**
     * Tests that the lazy initialization of the modMaps is the same for the multimap and copy constructor
     * @throws Exception because it is a test
     */
    @Test
    public void testCopyConstructor3() throws Exception {

        Multimap<Integer, Modification> sidechainModMap = ArrayListMultimap.create();
        sidechainModMap.put(0, mockMod("mod0", 40));
        sidechainModMap.put(3, mockMod("mod3_1", 31));
        sidechainModMap.put(3, mockMod("mod3_2", 32));
        sidechainModMap.put(5, mockMod("mod5", 5));

        Multimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_1", 56));
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_2", 52));

        AminoAcidSequence src = new MockAminoAcidSequence(Arrays.asList(C, E, R, V, I, L, A, S), sidechainModMap, termModMap);
        AminoAcidSequence dest = new MockAminoAcidSequence(src, 7, 8);


        Assert.assertEquals(new MockAminoAcidSequence(S), dest);
    }

    @Test
    public void testHasModifications() throws Exception {

        final Modification mod0 = mockMod("mod0", 40);
        final Modification mod3_1 = mockMod("mod3_1", 31);
        final Modification mod3_2 = mockMod("mod3_2", 32);
        final Modification mod5 = mockMod("mod5", 5);

        final Modification nTerm1 = mockMod("n_term_1", 56);
        final Modification nTerm2 = mockMod("n_term_2", 52);
        final Modification cTerm = mockMod("c_term", .87);

        Multimap<Integer, Modification> sidechainModMap = ArrayListMultimap.create();
        sidechainModMap.put(0, mod0);
        sidechainModMap.put(3, mod3_1);
        sidechainModMap.put(3, mod3_2);
        sidechainModMap.put(5, mod5);

        Multimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, nTerm1);
        termModMap.put(ModAttachment.N_TERM, nTerm2);
        termModMap.put(ModAttachment.C_TERM, cTerm);

        AminoAcidSequence modSeq = new MockAminoAcidSequence(Arrays.asList(C, E, R, V, I, L, A, S), sidechainModMap, termModMap);
        Assert.assertEquals(true, modSeq.hasModifications());

        AminoAcidSequence noModSeq = new MockAminoAcidSequence(C, E, R, V, I, L, A, S);
        Assert.assertEquals(false, noModSeq.hasModifications());
    }

    @Test
    public void testHasModificationAtIndex() throws Exception {

        final Modification mod0 = mockMod("mod0", 40);
        final Modification mod3_1 = mockMod("mod3_1", 31);
        final Modification mod3_2 = mockMod("mod3_2", 32);

        final Modification nTerm1 = mockMod("n_term_1", 56);
        final Modification nTerm2 = mockMod("n_term_2", 52);
        final Modification cTerm = mockMod("c_term", .87);

        Multimap<Integer, Modification> sidechainModMap = ArrayListMultimap.create();
        sidechainModMap.put(0, mod0);
        sidechainModMap.put(3, mod3_1);
        sidechainModMap.put(3, mod3_2);

        Multimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, nTerm1);
        termModMap.put(ModAttachment.N_TERM, nTerm2);
        termModMap.put(ModAttachment.C_TERM, cTerm);

        AminoAcidSequence modSeq = new MockAminoAcidSequence(Arrays.asList(C, E, R, V, I, L, A, S), sidechainModMap, termModMap);
        Assert.assertEquals(true, modSeq.hasModificationAt(0));
        Assert.assertEquals(false, modSeq.hasModificationAt(1));
        Assert.assertEquals(true, modSeq.hasModificationAt(3));
        Assert.assertEquals(true, modSeq.hasModificationAt(7));
    }

    @Test
    public void testHasModificationAtAttachment1() throws Exception {

        AminoAcidSequence seq = new MockAminoAcidSequence(C, E, R, V, I, L, A, S);
        Assert.assertEquals(false, seq.hasModificationAt(ModAttachment.C_TERM));
        Assert.assertEquals(false, seq.hasModificationAt(EnumSet.allOf(ModAttachment.class)));
    }

    @Test
    public void testHasModificationAtAttachment2() throws Exception {

        final Modification mod0 = mockMod("mod0", 40);
        final Modification mod3_1 = mockMod("mod3_1", 31);
        final Modification mod3_2 = mockMod("mod3_2", 32);

        Multimap<Integer, Modification> sidechainModMap = ArrayListMultimap.create();
        sidechainModMap.put(0, mod0);
        sidechainModMap.put(3, mod3_1);
        sidechainModMap.put(3, mod3_2);

        Multimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();

        AminoAcidSequence seq = new MockAminoAcidSequence(Arrays.asList(C, E, R, V, I, L, A, S), sidechainModMap, termModMap);
        Assert.assertEquals(true, seq.hasModificationAt(ModAttachment.SIDE_CHAIN));
        Assert.assertEquals(false, seq.hasModificationAt(ModAttachment.N_TERM));
        Assert.assertEquals(false, seq.hasModificationAt(ModAttachment.C_TERM));
        Assert.assertEquals(false, seq.hasModificationAt(EnumSet.of(ModAttachment.N_TERM, ModAttachment.C_TERM)));
        Assert.assertEquals(true, seq.hasModificationAt(EnumSet.of(ModAttachment.SIDE_CHAIN, ModAttachment.C_TERM)));
    }

    @Test
    public void testHasModificationAtAttachment3() throws Exception {

        final Modification nTerm1 = mockMod("n_term_1", 56);
        final Modification nTerm2 = mockMod("n_term_2", 52);
        final Modification cTerm = mockMod("c_term", .87);

        Multimap<Integer, Modification> sidechainModMap = ArrayListMultimap.create();

        Multimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, nTerm1);
        termModMap.put(ModAttachment.N_TERM, nTerm2);
        termModMap.put(ModAttachment.C_TERM, cTerm);

        AminoAcidSequence seq = new MockAminoAcidSequence(Arrays.asList(C, E, R, V, I, L, A, S), sidechainModMap, termModMap);
        Assert.assertEquals(false, seq.hasModificationAt(ModAttachment.SIDE_CHAIN));
        Assert.assertEquals(true, seq.hasModificationAt(ModAttachment.N_TERM));
        Assert.assertEquals(true, seq.hasModificationAt(ModAttachment.C_TERM));
        Assert.assertEquals(true, seq.hasModificationAt(EnumSet.of(ModAttachment.N_TERM, ModAttachment.C_TERM)));
        Assert.assertEquals(true, seq.hasModificationAt(EnumSet.of(ModAttachment.SIDE_CHAIN, ModAttachment.C_TERM)));
    }



    @Test
    public void testHasAmbiguousAA() throws Exception {

        Assert.assertEquals(false, new MockAminoAcidSequence(new AminoAcid[]{C, E, R, V, I, L, A, S, A}).hasAmbiguousAminoAcids());
        Assert.assertEquals(true, new MockAminoAcidSequence(new AminoAcid[]{X, E, R, V, I, L, A, S, A}).hasAmbiguousAminoAcids());
    }

    @Test
    public void testGetModificationsAt() throws Exception {

        final Modification mod0 = mockMod("mod0", 40);
        final Modification mod3_1 = mockMod("mod3_1", 31);
        final Modification mod3_2 = mockMod("mod3_2", 32);
        final Modification mod5 = mockMod("mod5", 5);

        final Modification nTerm1 = mockMod("n_term_1", 56);
        final Modification nTerm2 = mockMod("n_term_2", 52);
        final Modification cTerm = mockMod("c_term", .87);

        Multimap<Integer, Modification> sidechainModMap = ArrayListMultimap.create();
        sidechainModMap.put(0, mod0);
        sidechainModMap.put(3, mod3_1);
        sidechainModMap.put(3, mod3_2);
        sidechainModMap.put(5, mod5);

        Multimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, nTerm1);
        termModMap.put(ModAttachment.N_TERM, nTerm2);
        termModMap.put(ModAttachment.C_TERM, cTerm);

        AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(C, E, R, V, I, L, A, S), sidechainModMap, termModMap);

        Assert.assertEquals(Arrays.asList(nTerm1, nTerm2), sequence.getModificationsAt(0, EnumSet.of(ModAttachment.N_TERM)));
        Assert.assertEquals(Arrays.asList(mod0, nTerm1, nTerm2), sequence.getModificationsAt(0, EnumSet.allOf(ModAttachment.class)));
        Assert.assertEquals(Arrays.asList(mod0), sequence.getModificationsAt(0, EnumSet.of(ModAttachment.SIDE_CHAIN)));
        Assert.assertEquals(Collections.<Modification>emptyList(), sequence.getModificationsAt(3, EnumSet.of(ModAttachment.N_TERM)));
        Assert.assertEquals(Arrays.asList(mod3_1, mod3_2), sequence.getModificationsAt(3, EnumSet.of(ModAttachment.SIDE_CHAIN)));
        Assert.assertEquals(Arrays.asList(mod5), sequence.getModificationsAt(5, EnumSet.of(ModAttachment.SIDE_CHAIN)));
        Assert.assertEquals(Arrays.asList(cTerm), sequence.getModificationsAt(7, EnumSet.of(ModAttachment.C_TERM)));
    }


    @Test
    public void testGetModificationsAt2() throws Exception {

        final Modification mod = mockMod("mod", 7);
        final Modification nTerm = mockMod("n_term", 56);

        Multimap<Integer, Modification> sidechainModMap = ArrayListMultimap.create();
        sidechainModMap.put(7, mod);

        Multimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, nTerm);

        AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(C, E, R, V, I, L, A, S), sidechainModMap, termModMap);

        Assert.assertEquals(Arrays.asList(mod), sequence.getModificationsAt(7, ModAttachment.all));
        Assert.assertTrue(sequence.getModificationsAt(7, ModAttachment.cTermSet).isEmpty());
    }

    @Test
    public void testGetModificationsForAttachment() throws Exception {

        final Modification mod0 = mockMod("mod0", 40);
        final Modification mod3_1 = mockMod("mod3_1", 31);
        final Modification mod3_2 = mockMod("mod3_2", 32);
        final Modification mod5 = mockMod("mod5", 5);

        final Modification nTerm1 = mockMod("n_term_1", 56);
        final Modification nTerm2 = mockMod("n_term_2", 52);
        final Modification cTerm = mockMod("c_term", .87);

        Multimap<Integer, Modification> sidechainModMap = ArrayListMultimap.create();
        sidechainModMap.put(0, mod0);
        sidechainModMap.put(3, mod3_1);
        sidechainModMap.put(3, mod3_2);
        sidechainModMap.put(5, mod5);

        Multimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, nTerm1);
        termModMap.put(ModAttachment.N_TERM, nTerm2);
        termModMap.put(ModAttachment.C_TERM, cTerm);

        AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(C, E, R, V, I, L, A, S), sidechainModMap, termModMap);

        Assert.assertEquals(Arrays.asList(nTerm1, nTerm2), sequence.getModifications(EnumSet.of(ModAttachment.N_TERM)));
        Assert.assertEquals(Arrays.asList(nTerm1, nTerm2, cTerm, mod5, mod3_1, mod3_2, mod0), sequence.getModifications(EnumSet.allOf(ModAttachment.class)));
        Assert.assertEquals(Arrays.asList(mod5, mod3_1, mod3_2, mod0), sequence.getModifications(EnumSet.of(ModAttachment.SIDE_CHAIN)));
        Assert.assertEquals(Arrays.asList(cTerm), sequence.getModifications(EnumSet.of(ModAttachment.C_TERM)));
        Assert.assertEquals(Arrays.asList(nTerm1, nTerm2, cTerm), sequence.getModifications(ModAttachment.termSet));
    }

    @Test
    public void testHasModificationsAt() throws Exception {

        final Modification mod0 = mockMod("mod0", 40);
        final Modification mod3_1 = mockMod("mod3_1", 31);
        final Modification mod3_2 = mockMod("mod3_2", 32);
        final Modification mod5 = mockMod("mod5", 5);

        final Modification nTerm1 = mockMod("n_term_1", 56);
        final Modification nTerm2 = mockMod("n_term_2", 52);
        final Modification cTerm = mockMod("c_term", .87);

        Multimap<Integer, Modification> sidechainModMap = ArrayListMultimap.create();
        sidechainModMap.put(0, mod0);
        sidechainModMap.put(3, mod3_1);
        sidechainModMap.put(3, mod3_2);
        sidechainModMap.put(5, mod5);

        Multimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, nTerm1);
        termModMap.put(ModAttachment.N_TERM, nTerm2);
        termModMap.put(ModAttachment.C_TERM, cTerm);

        AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(C, E, R, V, I, L, A, S), sidechainModMap, termModMap);

        Assert.assertEquals(true, sequence.hasModificationAt(0, EnumSet.of(ModAttachment.N_TERM)));
        Assert.assertEquals(false, sequence.hasModificationAt(0, EnumSet.of(ModAttachment.C_TERM)));
        Assert.assertEquals(true, sequence.hasModificationAt(0, EnumSet.allOf(ModAttachment.class)));
        Assert.assertEquals(true, sequence.hasModificationAt(0, EnumSet.of(ModAttachment.SIDE_CHAIN)));
        Assert.assertEquals(false, sequence.hasModificationAt(1, EnumSet.of(ModAttachment.SIDE_CHAIN)));
        Assert.assertEquals(false, sequence.hasModificationAt(3, EnumSet.of(ModAttachment.N_TERM)));
        Assert.assertEquals(true, sequence.hasModificationAt(3, EnumSet.of(ModAttachment.SIDE_CHAIN)));
        Assert.assertEquals(true, sequence.hasModificationAt(5, EnumSet.of(ModAttachment.SIDE_CHAIN)));
        Assert.assertEquals(true, sequence.hasModificationAt(7, EnumSet.of(ModAttachment.C_TERM)));
        Assert.assertEquals(false, sequence.hasModificationAt(7, EnumSet.of(ModAttachment.SIDE_CHAIN)));
    }

    @Test
    public void testModificationIndexes() throws Exception {

        final Modification mod0 = mockMod("mod0", 40);
        final Modification mod3_1 = mockMod("mod3_1", 31);
        final Modification mod3_2 = mockMod("mod3_2", 32);
        final Modification mod5 = mockMod("mod5", 5);

        final Modification nTerm1 = mockMod("n_term_1", 56);
        final Modification nTerm2 = mockMod("n_term_2", 52);
        final Modification cTerm = mockMod("c_term", .87);

        Multimap<Integer, Modification> sidechainModMap = ArrayListMultimap.create();
        sidechainModMap.put(0, mod0);
        sidechainModMap.put(3, mod3_1);
        sidechainModMap.put(3, mod3_2);
        sidechainModMap.put(5, mod5);

        Multimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, nTerm1);
        termModMap.put(ModAttachment.N_TERM, nTerm2);
        termModMap.put(ModAttachment.C_TERM, cTerm);

        AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(C, E, R, V, I, L, A, S), sidechainModMap, termModMap);

        Assert.assertArrayEquals(new int[]{0, 3, 5, 7}, sequence.getModificationIndexes(ModAttachment.all));
        Assert.assertArrayEquals(new int[]{0}, sequence.getModificationIndexes(ModAttachment.nTermSet));
        Assert.assertArrayEquals(new int[]{7}, sequence.getModificationIndexes(ModAttachment.cTermSet));
    }

    @Test
    public void testCalculateMonomerMassSum() throws Exception {

        Multimap<Integer, Modification> sidechainModMap = ArrayListMultimap.create();
        sidechainModMap.put(0, Modification.parseModification("HPO3"));

        Multimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, Modification.parseModification("CH3"));
        termModMap.put(ModAttachment.C_TERM, Modification.parseModification("O"));

        AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(C, E, R, V, I, L, A, S), sidechainModMap, termModMap);

        Assert.assertEquals(982.443292793, sequence.calculateMonomerMassSum(), 0.000000000001);
    }

    @Test
    public void testCalculateMonomerMassDefect() throws Exception {

        Multimap<Integer, Modification> sidechainModMap = ArrayListMultimap.create();
        sidechainModMap.put(0, Modification.parseModification("HPO3"));

        Multimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, Modification.parseModification("CH3"));
        termModMap.put(ModAttachment.C_TERM, Modification.parseModification("O"));

        AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(C, E, R, V, I, L, A, S), sidechainModMap, termModMap);

        Assert.assertEquals(0.44329279300003144, sequence.calculateMonomerMassDefect(), 0.00000001);
    }

    @Test
    public void testCountAminoAcidsIn() throws Exception {

        AminoAcidSequence sequence = new MockAminoAcidSequence(C, E, R, V, I, L, A, S, S, S);

        Assert.assertEquals(1, sequence.countAminoAcidsIn(EnumSet.of(E)));
        Assert.assertEquals(2, sequence.countAminoAcidsIn(EnumSet.of(R, V)));
        Assert.assertEquals(0, sequence.countAminoAcidsIn(EnumSet.of(J)));
        Assert.assertEquals(3, sequence.countAminoAcidsIn(EnumSet.of(S)));
    }

    @Test
    public void testIsEmpty() throws Exception {

        Assert.assertEquals(false, new MockAminoAcidSequence(C, E, R, V, I, L, A, S, S, S).isEmpty());
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testModListImmutable1() throws Exception {

        Multimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
        sideChainModMap.put(0, mockMod("mod0", 40));
        sideChainModMap.put(3, mockMod("mod3_1", 31));
        sideChainModMap.put(3, mockMod("mod3_2", 32));
        sideChainModMap.put(5, mockMod("mod5", 5));

        Multimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_1", 56));
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_2", 52));
        termModMap.put(ModAttachment.C_TERM, mockMod("c_term", .87));

        AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(C, E, R, V, I, L, A, S), sideChainModMap, termModMap);

        sequence.getModifications(EnumSet.allOf(ModAttachment.class)).clear();
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testModListImmutable2() throws Exception {

        Multimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
        sideChainModMap.put(0, mockMod("mod0", 40));
        sideChainModMap.put(3, mockMod("mod3_1", 31));
        sideChainModMap.put(3, mockMod("mod3_2", 32));
        sideChainModMap.put(5, mockMod("mod5", 5));

        Multimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_1", 56));
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_2", 52));
        termModMap.put(ModAttachment.C_TERM, mockMod("c_term", .87));

        AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(C, E, R, V, I, L, A, S), sideChainModMap, termModMap);

        sequence.getModifications(EnumSet.allOf(ModAttachment.class)).add(mockMod("unused", 98));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testModListImmutable3() throws Exception {

        Multimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
        sideChainModMap.put(0, mockMod("mod0", 40));
        sideChainModMap.put(3, mockMod("mod3_1", 31));
        sideChainModMap.put(3, mockMod("mod3_2", 32));
        sideChainModMap.put(5, mockMod("mod5", 5));

        Multimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_1", 56));
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_2", 52));
        termModMap.put(ModAttachment.C_TERM, mockMod("c_term", .87));

        AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(C, E, R, V, I, L, A, S), sideChainModMap, termModMap);

        final Iterator<Modification> iterator = sequence.getModifications(EnumSet.allOf(ModAttachment.class)).iterator();
        iterator.next();
        iterator.remove();
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testModListImmutable4() throws Exception {

        final Modification mod0 = mockMod("mod0", 40);

        Multimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
        sideChainModMap.put(0, mod0);
        sideChainModMap.put(3, mockMod("mod3_1", 31));
        sideChainModMap.put(3, mockMod("mod3_2", 32));
        sideChainModMap.put(5, mockMod("mod5", 5));

        Multimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_1", 56));
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_2", 52));
        termModMap.put(ModAttachment.C_TERM, mockMod("c_term", .87));

        AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(C, E, R, V, I, L, A, S), sideChainModMap, termModMap);

        sequence.getModifications(EnumSet.allOf(ModAttachment.class)).remove(mod0);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testModListImmutable5() throws Exception {

        final Modification mod0 = mockMod("mod0", 40);
        final Modification mod3_1 = mockMod("mod3_1", 31);

        Multimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
        sideChainModMap.put(0, mod0);
        sideChainModMap.put(3, mod3_1);
        sideChainModMap.put(3, mockMod("mod3_2", 32));
        sideChainModMap.put(5, mockMod("mod5", 5));

        Multimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_1", 56));
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_2", 52));
        termModMap.put(ModAttachment.C_TERM, mockMod("c_term", .87));

        AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(C, E, R, V, I, L, A, S), sideChainModMap, termModMap);

        sequence.getModifications(EnumSet.allOf(ModAttachment.class)).addAll(Arrays.asList(mod0, mod3_1));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testModListImmutable6() throws Exception {

        final Modification mod0 = mockMod("mod0", 40);
        final Modification mod3_1 = mockMod("mod3_1", 31);

        Multimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
        sideChainModMap.put(0, mod0);
        sideChainModMap.put(3, mod3_1);
        sideChainModMap.put(3, mockMod("mod3_2", 32));
        sideChainModMap.put(5, mockMod("mod5", 5));

        Multimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_1", 56));
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_2", 52));
        termModMap.put(ModAttachment.C_TERM, mockMod("c_term", .87));

        AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(C, E, R, V, I, L, A, S), sideChainModMap, termModMap);

        sequence.getModifications(EnumSet.allOf(ModAttachment.class)).addAll(3, Arrays.asList(mod0, mod3_1));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testModListImmutable7() throws Exception {

        final Modification mod0 = mockMod("mod0", 40);
        final Modification mod3_1 = mockMod("mod3_1", 31);

        Multimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
        sideChainModMap.put(0, mod0);
        sideChainModMap.put(3, mod3_1);
        sideChainModMap.put(3, mockMod("mod3_2", 32));
        sideChainModMap.put(5, mockMod("mod5", 5));

        Multimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_1", 56));
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_2", 52));
        termModMap.put(ModAttachment.C_TERM, mockMod("c_term", .87));

        AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(C, E, R, V, I, L, A, S), sideChainModMap, termModMap);

        sequence.getModifications(EnumSet.allOf(ModAttachment.class)).removeAll(Arrays.asList(mod0, mod3_1));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testModListImmutable8() throws Exception {

        final Modification mod0 = mockMod("mod0", 40);
        final Modification mod3_1 = mockMod("mod3_1", 31);

        Multimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
        sideChainModMap.put(0, mod0);
        sideChainModMap.put(3, mod3_1);
        sideChainModMap.put(3, mockMod("mod3_2", 32));
        sideChainModMap.put(5, mockMod("mod5", 5));

        Multimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_1", 56));
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_2", 52));
        termModMap.put(ModAttachment.C_TERM, mockMod("c_term", .87));

        AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(C, E, R, V, I, L, A, S), sideChainModMap, termModMap);

        sequence.getModifications(EnumSet.allOf(ModAttachment.class)).retainAll(Arrays.asList(mod0, mod3_1));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testModListImmutable9() throws Exception {

        final Modification mod0 = mockMod("mod0", 40);
        final Modification mod3_1 = mockMod("mod3_1", 31);

        Multimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
        sideChainModMap.put(0, mod0);
        sideChainModMap.put(3, mod3_1);
        sideChainModMap.put(3, mockMod("mod3_2", 32));
        sideChainModMap.put(5, mockMod("mod5", 5));

        Multimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_1", 56));
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_2", 52));
        termModMap.put(ModAttachment.C_TERM, mockMod("c_term", .87));

        AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(C, E, R, V, I, L, A, S), sideChainModMap, termModMap);

        sequence.getModifications(EnumSet.allOf(ModAttachment.class)).set(1, mod0);
    }

    @Test
    public void testModListGetMolecularMass() throws Exception {

        final Modification mod0 = mockMod("mod0", 40);
        final Modification mod3_1 = mockMod("mod3_1", 31);

        Multimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
        sideChainModMap.put(0, mod0);
        sideChainModMap.put(3, mod3_1);
        sideChainModMap.put(3, mockMod("mod3_2", 32));
        sideChainModMap.put(5, mockMod("mod5", 5));

        Multimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_1", 56));
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_2", 52));
        termModMap.put(ModAttachment.C_TERM, mockMod("c_term", .87));

        AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(C, E, R, V, I, L, A, S), sideChainModMap, termModMap);

        Assert.assertEquals(216.87, sequence.getModifications(ModAttachment.all).getMolecularMass(), 0.00000001);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testModListImmutable10() throws Exception {

        final Modification mod0 = mockMod("mod0", 40);
        final Modification mod3_1 = mockMod("mod3_1", 31);

        Multimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
        sideChainModMap.put(0, mod0);
        sideChainModMap.put(3, mod3_1);
        sideChainModMap.put(3, mockMod("mod3_2", 32));
        sideChainModMap.put(5, mockMod("mod5", 5));

        Multimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_1", 56));
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_2", 52));
        termModMap.put(ModAttachment.C_TERM, mockMod("c_term", .87));

        AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(C, E, R, V, I, L, A, S), sideChainModMap, termModMap);

        sequence.getModifications(EnumSet.allOf(ModAttachment.class)).add(1, mod0);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testModListImmutable11() throws Exception {

        final Modification mod0 = mockMod("mod0", 40);
        final Modification mod3_1 = mockMod("mod3_1", 31);

        Multimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
        sideChainModMap.put(0, mod0);
        sideChainModMap.put(3, mod3_1);
        sideChainModMap.put(3, mockMod("mod3_2", 32));
        sideChainModMap.put(5, mockMod("mod5", 5));

        Multimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_1", 56));
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_2", 52));
        termModMap.put(ModAttachment.C_TERM, mockMod("c_term", .87));

        AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(C, E, R, V, I, L, A, S), sideChainModMap, termModMap);

        sequence.getModifications(EnumSet.allOf(ModAttachment.class)).remove(1);
    }
    @Test(expected = UnsupportedOperationException.class)
    public void testModListImmutable12() throws Exception {

        final Modification mod0 = mockMod("mod0", 40);
        final Modification mod3_1 = mockMod("mod3_1", 31);

        Multimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
        sideChainModMap.put(0, mod0);
        sideChainModMap.put(3, mod3_1);
        sideChainModMap.put(3, mockMod("mod3_2", 32));
        sideChainModMap.put(5, mockMod("mod5", 5));

        Multimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_1", 56));
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_2", 52));
        termModMap.put(ModAttachment.C_TERM, mockMod("c_term", .87));

        AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(C, E, R, V, I, L, A, S), sideChainModMap, termModMap);

        sequence.getModifications(EnumSet.allOf(ModAttachment.class)).subList(1, 3);
    }

    @Test
    public void testToString() throws Exception {

        Multimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
        sideChainModMap.put(0, mockMod("mod0", 40));
        sideChainModMap.put(3, mockMod("mod3_1", 31));
        sideChainModMap.put(3, mockMod("mod3_2", 32));
        sideChainModMap.put(5, mockMod("mod5", 5));

        Multimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_1", 56));
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_2", 52));
        termModMap.put(ModAttachment.C_TERM, mockMod("c_term", .87));

        AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(C, E, R, V, I, L, A, S), sideChainModMap, termModMap);

        Assert.assertEquals("(n_term_1, n_term_2)_C(mod0)ERV(mod3_1, mod3_2)IL(mod5)AS_(c_term)", sequence.toString());
        Assert.assertEquals("CERVILAS", sequence.toSymbolString());
    }

    @Test
    public void testHasSameSequence() throws Exception {

        Multimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
        sideChainModMap.put(0, mockMod("mod0", 40));
        sideChainModMap.put(3, mockMod("mod3_1", 31));
        sideChainModMap.put(3, mockMod("mod3_2", 32));
        sideChainModMap.put(5, mockMod("mod5", 5));

        Multimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_1", 56));
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_2", 52));
        termModMap.put(ModAttachment.C_TERM, mockMod("c_term", .87));

        AminoAcidSequence sequence1 = new MockAminoAcidSequence(Arrays.asList(C, E, R, V, I, L, A, S), sideChainModMap, termModMap);
        AminoAcidSequence sequence2 = new MockAminoAcidSequence(C, E, R, V, I, L, A, S);
        AminoAcidSequence sequence3 = new MockAminoAcidSequence(Arrays.asList(R, E, R, V, I, L, A, S), sideChainModMap, termModMap);

        Assert.assertEquals(true, sequence1.hasSameSequence(sequence2));
        Assert.assertEquals(true, sequence2.hasSameSequence(sequence1));
        Assert.assertEquals(false, sequence1.hasSameSequence(sequence3));
        Assert.assertEquals(false, sequence3.hasSameSequence(sequence1));
    }

    @Test
    public void testHasSameSymbolSequence() throws Exception {

        Multimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
        sideChainModMap.put(0, mockMod("mod0", 40));
        sideChainModMap.put(3, mockMod("mod3_1", 31));
        sideChainModMap.put(3, mockMod("mod3_2", 32));
        sideChainModMap.put(5, mockMod("mod5", 5));

        Multimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_1", 56));
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_2", 52));
        termModMap.put(ModAttachment.C_TERM, mockMod("c_term", .87));

        AminoAcidSequence sequence1 = new MockAminoAcidSequence(Arrays.asList(C, E, R, V, I, L, A, S), sideChainModMap, termModMap);
        SymbolSequence<AminoAcid> sequence2 = new MockAminoAcidSequence(C, E, R, V, I, L, A, S);
        SymbolSequence<AminoAcid> sequence3 = new MockAminoAcidSequence(Arrays.asList(R, E, R, V, I, L, A, S), sideChainModMap, termModMap);

        Assert.assertEquals(true, sequence1.hasSameSequence(sequence2));
        Assert.assertEquals(false, sequence1.hasSameSequence(sequence3));
    }

    @Test
    public void testEquals() throws Exception {

        Multimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
        sideChainModMap.put(0, mockMod("mod0", 40));
        sideChainModMap.put(3, mockMod("mod3_1", 31));
        sideChainModMap.put(3, mockMod("mod3_2", 32));
        sideChainModMap.put(5, mockMod("mod5", 5));

        Multimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_1", 56));
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_2", 52));
        termModMap.put(ModAttachment.C_TERM, mockMod("c_term", .87));

        AminoAcidSequence sequence1 = new MockAminoAcidSequence(Arrays.asList(C, E, R, V, I, L, A, S), sideChainModMap, termModMap);
        AminoAcidSequence sequence2 = new MockAminoAcidSequence(C, E, R, V, I, L, A, S);
        AminoAcidSequence sequence3 = new MockAminoAcidSequence(Arrays.asList(C, E, R, V, I, L, A, S), sideChainModMap, termModMap);

        Assert.assertEquals(false, sequence1.equals(sequence2));
        Assert.assertEquals(false, sequence2.equals(sequence1));
        Assert.assertEquals(true, sequence1.equals(sequence3));
        Assert.assertEquals(true, sequence3.equals(sequence1));
        Assert.assertEquals(sequence1.hashCode(), sequence3.hashCode());
    }

    @Test
    public void testGetModCount() throws Exception {

        Multimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
        sideChainModMap.put(0, mockMod("mod0", 40));
        sideChainModMap.put(3, mockMod("mod3_1", 31));
        sideChainModMap.put(3, mockMod("mod3_2", 32));
        sideChainModMap.put(5, mockMod("mod5", 5));

        Multimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_1", 56));
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_2", 52));
        termModMap.put(ModAttachment.C_TERM, mockMod("c_term", .87));

        AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(C, E, R, V, I, L, A, S), sideChainModMap, termModMap);

        Assert.assertEquals(7, sequence.getModificationCount());
        Assert.assertEquals(2, sequence.getModificationCount(ModAttachment.N_TERM));
        Assert.assertEquals(1, sequence.getModificationCount(ModAttachment.C_TERM));
        Assert.assertEquals(4, sequence.getModificationCount(ModAttachment.SIDE_CHAIN));

        sequence = new MockAminoAcidSequence(C, E, R, V, I, L, A, S);
        Assert.assertEquals(0, sequence.getModificationCount());
        Assert.assertEquals(0, sequence.getModificationCount(ModAttachment.N_TERM));
        Assert.assertEquals(0, sequence.getModificationCount(ModAttachment.C_TERM));
        Assert.assertEquals(0, sequence.getModificationCount(ModAttachment.SIDE_CHAIN));
    }

    @Test
    public void testGetAmbiguousAminoAcids() throws Exception {

        MockAminoAcidSequence sequence = new MockAminoAcidSequence(C, E, R, X, B, Z, V, I, L, A, S);
        Assert.assertEquals(EnumSet.of(X, B, Z), sequence.getAmbiguousAminoAcids());

        sequence = new MockAminoAcidSequence(C, E, R, V, I, L, A, S);
        Assert.assertEquals(Collections.<AminoAcid>emptySet(), sequence.getAmbiguousAminoAcids());
    }

    @Test
    public void testGetModifiedResidueCount() throws Exception {

        Multimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
        sideChainModMap.put(0, mockMod("mod0", 40));
        sideChainModMap.put(3, mockMod("mod3_1", 31));
        sideChainModMap.put(3, mockMod("mod3_2", 32));
        sideChainModMap.put(5, mockMod("mod5", 5));

        Multimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_1", 56));
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_2", 52));
        termModMap.put(ModAttachment.C_TERM, mockMod("c_term", .87));

        AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(C, E, R, V, I, L, A, S), sideChainModMap, termModMap);

        Assert.assertEquals(4, sequence.getModifiedResidueCount());

        sequence = new MockAminoAcidSequence(C, E, R, V, I, L, A, S);
        Assert.assertEquals(0, sequence.getModifiedResidueCount());


        sequence = new MockAminoAcidSequence(Arrays.asList(C, E, R, V, I, L, A, S), ArrayListMultimap.<Integer, Modification>create(), termModMap);
        Assert.assertEquals(2, sequence.getModifiedResidueCount());
    }

    private Modification mockMod(String label, double mw) {

        Modification mod = mock(Modification.class);
        when(mod.getLabel()).thenReturn(label);
        when(mod.getMolecularMass()).thenReturn(mw);
        when(mod.toString()).thenReturn(label);

        return mod;
    }

    private static class MockAminoAcidSequence extends AminoAcidSequence {

        private MockAminoAcidSequence(AminoAcid firstResidue, AminoAcid... residues) {

            super(firstResidue, residues);
        }

        private MockAminoAcidSequence(AminoAcid[] residues) {

            super(residues);
        }

        private MockAminoAcidSequence(AminoAcidSequence src, int beginIndex, int endIndex) {

            super(src, beginIndex, endIndex);
        }

        private MockAminoAcidSequence(List<AminoAcid> residues, Multimap<Integer, Modification> sideChainModMap, Multimap<ModAttachment, Modification> termModMap) {

            super(residues, sideChainModMap, termModMap);
        }
    }
}