/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.io.ms.spectrum;

import com.google.common.base.Optional;
import org.expasy.mzjava.core.io.ms.spectrum.AbstractReaderTest;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.core.mol.Mass;
import org.expasy.mzjava.core.mol.NumericMass;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessorChain;
import org.expasy.mzjava.core.ms.peaklist.peaktransformer.SqrtTransformer;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.proteomics.io.ms.spectrum.msp.MspCommentParser;
import org.expasy.mzjava.proteomics.io.ms.spectrum.sptxt.AnnotationResolver;
import org.expasy.mzjava.proteomics.io.ms.spectrum.sptxt.LibrarySpectrumBuilder;
import org.expasy.mzjava.proteomics.io.ms.spectrum.sptxt.SpectraLibCommentParser;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.proteomics.mol.modification.ModificationResolver;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;
import org.expasy.mzjava.proteomics.ms.spectrum.PepFragAnnotation;
import org.expasy.mzjava.proteomics.ms.spectrum.PepLibPeakAnnotation;
import org.expasy.mzjava.utils.URIBuilder;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.Collections;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class MspReaderTest extends AbstractReaderTest<PepLibPeakAnnotation, PeptideConsensusSpectrum> {

    private final Mass waterLoss = Composition.parseComposition("(H2O)-1");
    private final Mass ammoniaLoss = Composition.parseComposition("(NH3)-1");

    @Test
    public void test() throws Exception {

        FileReader fr = new FileReader(new File(getClass().getResource("msp_test.msp").getFile()));

        MspReader reader = MspReader.newBuilder(fr, new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE)
                .useMspAnnotationResolver()
                .build();

        Peak precursor;
        PeptideConsensusSpectrum spectrum;
        double delta = 0.000001;

        //Spectrum 0
        spectrum = reader.next();
        precursor = spectrum.getPrecursor();
        Assert.assertEquals("A(Acetyl)AAAAAGAGPEMVR", spectrum.getPeptide().toString());
        Assert.assertEquals(642.822, precursor.getMz(), delta);
        Assert.assertEquals(0.0, precursor.getIntensity(), delta);
        Assert.assertEquals(2, precursor.getCharge());

        Assert.assertEquals("Spec=Consensus Pep=Tryptic Fullname=-.AAAAAAGAGPEMVR.G/2 Mods=1/0,A,Acetyl Parent=642.822 Inst=qtof Mz_diff=-0.012 Mz_exact=642.8224 Mz_av=643.234 Protein=\"IPI00003479.1|SWISS-PROT:P28482|TREMBL:Q499G7|REFSEQ_NP:NP_620407;NP_002736|ENSEMBL:ENSP00000215832|H-INV:HIT000038155|VEGA:OTTHUMP00000028754 Tax_Id=9606 Mitogen-activated protein kinase 1\" Pseq=40 Organism=\"human\" Se=3^X19:ex=1.5e-006/1.796e-006,td=0/3.292e+006,sd=0/0,hs=48/1.942,bs=3.9e-008,b2=7e-008,bd=3.08e+007^O20:ex=6.74e-013/9.778e-011,td=415/3.73e+008,pr=5.125e-016/7.496e-014,bs=3.14e-014,b2=4.39e-014,bd=3740^P17:sc=32/1.147,dc=21.7/1.076,ps=3.59/0.1929,bs=0 Sample=6/ca_hela2_bentscheff_cam,2,2/ca_k562_2_bantscheff_cam,4,5/gu_platelets_cofradic_none,0,1/mpi_a459_cam,6,6/mpi_hct116_cam,3,3/mpi_jurkat_cam,5,5 Nreps=20/28 Missing=0.0702/0.0402 Parent_med=642.82/0.02 Max2med_orig=227.9/103.0 Dotfull=0.903/0.028 Dot_cons=0.945/0.032 Unassign_all=0.109 Unassigned=0.052 Dotbest=0.95 Flags=0,0,0 Naa=14 DUScorr=3.3/1.4/2.9 Dottheory=0.88 Pfin=1.1e+015 Probcorr=1 Tfratio=3e+009 Pfract=0",
                spectrum.getComment());
        Assert.assertEquals(2, spectrum.getMsLevel());
        Assert.assertNotNull(spectrum.getId());
        checkPeaksSpectrum0(spectrum, delta);

        //Spectrum 1
        spectrum = reader.next();
        precursor = spectrum.getPrecursor();
        Assert.assertEquals("A(Acetyl)AAAAAGAGPEM(Oxidation)VR", spectrum.getPeptide().toString());
        Assert.assertEquals(650.82, precursor.getMz(), delta);
        Assert.assertEquals(0.0, precursor.getIntensity(), delta);
        Assert.assertEquals(2, precursor.getCharge());

        Assert.assertEquals("Spec=Consensus Pep=Tryptic Fullname=-.AAAAAAGAGPEM(O)VR.G/2 Mods=2/0,A,Acetyl/11,M,Oxidation Parent=650.820 Inst=qtof Mz_diff=0.000 Mz_exact=650.8199 Mz_av=651.234 Protein=\"IPI00003479.1|SWISS-PROT:P28482|TREMBL:Q499G7|REFSEQ_NP:NP_620407;NP_002736|ENSEMBL:ENSP00000215832|H-INV:HIT000038155|VEGA:OTTHUMP00000028754 Tax_Id=9606 Mitogen-activated protein kinase 1\" Pseq=40 Organism=\"human\" Se=3^X8:ex=2.95e-005/0.0001596,td=0/1.663e+006,sd=0/0,hs=47.3/4.537,bs=6.3e-007,b2=4.3e-006,bd=1.33e+007^O8:ex=5.355e-011/1.843e-007,td=83950/1.668e+005,pr=3.5575e-014/1.015e-010,bs=2.1e-012,b2=8.8e-012,bd=17900^P8:sc=32.45/2.813,dc=21.05/2.4,ps=3.52/0.385,bs=0 Sample=6/ca_hela2_bentscheff_cam,0,1/ca_k562_2_bantscheff_cam,1,3/gu_platelets_cofradic_none,0,1/mpi_a459_cam,4,5/mpi_hct116_cam,0,1/mpi_jurkat_cam,3,5 Nreps=8/23 Missing=0.0755/0.0816 Parent_med=650.82/0.01 Max2med_orig=215.6/77.0 Dotfull=0.824/0.038 Dot_cons=0.935/0.054 Unassign_all=0.078 Unassigned=0.021 Dotbest=0.96 Flags=0,0,0 Naa=14 DUScorr=2.3/1.9/2.9 Dottheory=0.87 Pfin=6e+014 Probcorr=1 Tfratio=2e+009 Pfract=0",
                spectrum.getComment());
        Assert.assertEquals(2, spectrum.getMsLevel());
        Assert.assertNotNull(spectrum.getId());
        Assert.assertEquals(161, spectrum.size());
        checkPeaksSpectrum1(spectrum, delta);
        Assert.assertFalse(reader.hasNext());
    }

    private PepLibPeakAnnotation fragAnt(int mergedPeakCount, IonType ionType, int charge, int residueNumber, int isotopeCount, Mass massShift, Peptide peptide) {

        Peptide peptideFragment;
        peptideFragment = ionType == IonType.p ? peptide : peptide.subSequence(ionType, residueNumber);
        return new PepLibPeakAnnotation(mergedPeakCount, 0, 0, Optional.of(
                new PepFragAnnotation.Builder(ionType, charge, peptideFragment)
                        .addC13(isotopeCount)
                        .setNeutralLoss(massShift)
                        .build()
        ));
    }

    private PepLibPeakAnnotation unknownAnt(int mergedPeakCount) {

        return new PepLibPeakAnnotation(mergedPeakCount, 0, 0);
    }

    @Test
    public void testReadUnsortedPeaks() throws Exception {

        FileReader fr = new FileReader(new File(getClass().getResource("msp-unsorted_test.msp").getFile()));

        MspReader reader = MspReader.newBuilder(fr, new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE)
                .acceptUnsortedSpectra()
                .useMspAnnotationResolver()
                .build();

        //Spectrum 0
        PeptideConsensusSpectrum spectrum = reader.next();

        checkPeaksSpectrum0(spectrum, 0.000001);
    }

    @Test(expected = IOException.class)
    public void testReadUnsortedPeaksNotAccepted() throws Exception {

        FileReader fr = new FileReader(new File(getClass().getResource("msp-unsorted_test.msp").getFile()));

        MspReader reader = MspReader.newBuilder(fr, new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE).build();

        //Spectrum 0
        reader.next();
    }

    private void checkPeaksSpectrum0(PeptideConsensusSpectrum spectrum, double delta) {

        Assert.assertEquals(182, spectrum.size());
        checkPeak(70.1, 88.0, 0, spectrum, delta, fragAnt(13, IonType.i, 1, 14, 0, new NumericMass(0.0), spectrum.getPeptide()), fragAnt(13, IonType.i, 1, 10, 0, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(112.1, 45.0, 1, spectrum, delta, fragAnt(8, IonType.i, 1, 14, 0, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(114.1, 47.0, 2, spectrum, delta, fragAnt(8, IonType.b, 1, 1, 0, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(115.1, 119.0, 3, spectrum, delta, unknownAnt(14));
        checkPeak(143.1, 215.0, 4, spectrum, delta, unknownAnt(20));
        checkPeak(155.1, 70.0, 5, spectrum, delta, unknownAnt(14));
        checkPeak(157.1, 102.0, 6, spectrum, delta, fragAnt(18, IonType.y, 1, 1, 0, waterLoss, spectrum.getPeptide()), fragAnt(18, IonType.a, 1, 2, 0, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(158.1, 49.0, 7, spectrum, delta, fragAnt(10, IonType.y, 1, 1, 0, ammoniaLoss, spectrum.getPeptide()));
        checkPeak(169.1, 89.0, 8, spectrum, delta, unknownAnt(17));
        checkPeak(175.1, 543.0, 9, spectrum, delta, fragAnt(20, IonType.y, 1, 1, 0, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(176.1, 49.0, 10, spectrum, delta, fragAnt(13, IonType.y, 1, 1, 1, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(185.1, 1791.0, 11, spectrum, delta, fragAnt(20, IonType.b, 1, 2, 0, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(186.1, 180.0, 12, spectrum, delta, fragAnt(20, IonType.b, 1, 2, 1, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(200.1, 107.0, 13, spectrum, delta, unknownAnt(15));
        checkPeak(214.1, 386.0, 14, spectrum, delta, fragAnt(20, IonType.b, 2, 6, 0, new NumericMass(-43.0), spectrum.getPeptide()));
        checkPeak(215.1, 65.0, 15, spectrum, delta, fragAnt(17, IonType.b, 2, 6, 1, new NumericMass(-43.0), spectrum.getPeptide()));
        checkPeak(226.1, 54.0, 16, spectrum, delta, unknownAnt(13));
        checkPeak(227.1, 147.0, 17, spectrum, delta, unknownAnt(19));
        checkPeak(228.1, 902.0, 18, spectrum, delta, fragAnt(20, IonType.a, 1, 3, 0, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(229.1, 116.0, 19, spectrum, delta, fragAnt(20, IonType.a, 1, 3, 1, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(231.1, 65.0, 20, spectrum, delta, unknownAnt(13));
        checkPeak(240.1, 51.0, 21, spectrum, delta, unknownAnt(13));
        checkPeak(243.1, 70.0, 22, spectrum, delta, unknownAnt(19));
        checkPeak(244.1, 61.0, 23, spectrum, delta, fragAnt(14, IonType.y, 2, 4, 0, new NumericMass(-46.0), spectrum.getPeptide()));
        checkPeak(255.2, 74.0, 24, spectrum, delta, unknownAnt(13));
        checkPeak(256.1, 7426.0, 25, spectrum, delta, fragAnt(20, IonType.b, 1, 3, 0, new NumericMass(0.0), spectrum.getPeptide()), fragAnt(20, IonType.y, 1, 2, 0, waterLoss, spectrum.getPeptide()));
        checkPeak(256.5, 417.0, 26, spectrum, delta, fragAnt(15, IonType.b, 1, 3, 1, new NumericMass(0.0), spectrum.getPeptide()), fragAnt(15, IonType.y, 1, 2, 1, waterLoss, spectrum.getPeptide()));
        checkPeak(256.8, 62.0, 27, spectrum, delta, fragAnt(15, IonType.y, 1, 2, 0, ammoniaLoss, spectrum.getPeptide()));
        checkPeak(257.1, 1042.0, 28, spectrum, delta, fragAnt(20, IonType.y, 1, 2, 0, ammoniaLoss, spectrum.getPeptide()));
        checkPeak(257.5, 80.0, 29, spectrum, delta, fragAnt(13, IonType.y, 1, 2, 1, ammoniaLoss, spectrum.getPeptide()));
        checkPeak(258.1, 170.0, 30, spectrum, delta, fragAnt(20, IonType.y, 2, 4, 0, waterLoss, spectrum.getPeptide()));
        checkPeak(261.1, 52.0, 31, spectrum, delta, unknownAnt(16));
        checkPeak(271.1, 148.0, 32, spectrum, delta, unknownAnt(19));
        checkPeak(274.2, 257.0, 33, spectrum, delta, fragAnt(20, IonType.y, 1, 2, 0, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(275.2, 43.0, 34, spectrum, delta, fragAnt(10, IonType.y, 1, 2, 1, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(283.1, 54.0, 35, spectrum, delta, unknownAnt(9));
        checkPeak(284.1, 276.0, 36, spectrum, delta, fragAnt(20, IonType.b, 1, 4, 0, new NumericMass(-43.0), spectrum.getPeptide()));
        checkPeak(285.1, 240.0, 37, spectrum, delta, fragAnt(20, IonType.b, 1, 4, 1, new NumericMass(-43.0), spectrum.getPeptide()));
        checkPeak(286.1, 51.0, 38, spectrum, delta, fragAnt(12, IonType.b, 1, 4, 2, new NumericMass(-43.0), spectrum.getPeptide()));
        checkPeak(297.1, 73.0, 39, spectrum, delta, unknownAnt(19));
        checkPeak(299.2, 117.0, 40, spectrum, delta, fragAnt(20, IonType.b, 2, 8, 0, new NumericMass(0.0), spectrum.getPeptide()), fragAnt(20, IonType.a, 1, 4, 0, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(300.1, 51.0, 41, spectrum, delta, fragAnt(13, IonType.b, 2, 8, 1, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(301.1, 56.0, 42, spectrum, delta, fragAnt(10, IonType.b, 2, 8, 2, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(309.1, 64.0, 43, spectrum, delta, fragAnt(15, IonType.b, 1, 4, 0, waterLoss, spectrum.getPeptide()));
        checkPeak(311.2, 52.0, 44, spectrum, delta, unknownAnt(15));
        checkPeak(314.2, 94.0, 45, spectrum, delta, unknownAnt(20));
        checkPeak(316.2, 59.0, 46, spectrum, delta, fragAnt(12, IonType.y, 2, 5, 0, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(326.4, 80.0, 47, spectrum, delta, unknownAnt(19));
        checkPeak(327.2, 10000.0, 48, spectrum, delta, fragAnt(20, IonType.b, 1, 4, 0, new NumericMass(0.0), spectrum.getPeptide()), fragAnt(20, IonType.b, 2, 9, 0, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(328.2, 1407.0, 49, spectrum, delta, fragAnt(20, IonType.b, 2, 9, 0, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(328.5, 64.0, 50, spectrum, delta, unknownAnt(15));
        checkPeak(329.2, 216.0, 51, spectrum, delta, unknownAnt(20));
        checkPeak(330.1, 77.0, 52, spectrum, delta, unknownAnt(11));
        checkPeak(337.2, 66.0, 53, spectrum, delta, unknownAnt(13));
        checkPeak(339.2, 65.0, 54, spectrum, delta, unknownAnt(14));
        checkPeak(342.2, 129.0, 55, spectrum, delta, unknownAnt(20));
        checkPeak(344.7, 48.0, 56, spectrum, delta, fragAnt(12, IonType.y, 2, 6, 0, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(355.2, 75.0, 57, spectrum, delta, fragAnt(16, IonType.b, 1, 5, 0, new NumericMass(-43.0), spectrum.getPeptide()));
        checkPeak(356.2, 65.0, 58, spectrum, delta, fragAnt(12, IonType.b, 1, 5, 1, new NumericMass(-43.0), spectrum.getPeptide()));
        checkPeak(358.1, 213.0, 59, spectrum, delta, fragAnt(20, IonType.y, 2, 7, 0, new NumericMass(-44.0), spectrum.getPeptide()));
        checkPeak(359.1, 60.0, 60, spectrum, delta, fragAnt(13, IonType.y, 2, 7, 0, new NumericMass(-43.0), spectrum.getPeptide()));
        checkPeak(368.2, 53.0, 61, spectrum, delta, unknownAnt(17));
        checkPeak(370.2, 52.0, 62, spectrum, delta, fragAnt(12, IonType.a, 1, 5, 0, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(377.1, 24.0, 63, spectrum, delta, unknownAnt(2));
        checkPeak(380.2, 71.0, 64, spectrum, delta, fragAnt(20, IonType.y, 2, 7, 0, new NumericMass(0.0), spectrum.getPeptide()), fragAnt(20, IonType.b, 1, 5, 0, waterLoss, spectrum.getPeptide()));
        checkPeak(385.2, 54.0, 65, spectrum, delta, fragAnt(13, IonType.y, 2, 8, 0, new NumericMass(-46.0), spectrum.getPeptide()));
        checkPeak(387.2, 59.0, 66, spectrum, delta, fragAnt(13, IonType.y, 1, 3, 0, waterLoss, spectrum.getPeptide()), fragAnt(13, IonType.y, 2, 8, 0, new NumericMass(-43.0), spectrum.getPeptide()));
        checkPeak(388.2, 165.0, 67, spectrum, delta, fragAnt(20, IonType.y, 1, 3, 0, ammoniaLoss, spectrum.getPeptide()));
        checkPeak(394.2, 81.0, 68, spectrum, delta, unknownAnt(16));
        checkPeak(397.2, 88.0, 69, spectrum, delta, unknownAnt(12));
        checkPeak(397.8, 80.0, 70, spectrum, delta, fragAnt(16, IonType.b, 1, 5, 0, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(398.2, 5544.0, 71, spectrum, delta, fragAnt(20, IonType.b, 1, 5, 0, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(398.5, 79.0, 72, spectrum, delta, fragAnt(13, IonType.b, 1, 5, 1, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(399.2, 1010.0, 73, spectrum, delta, fragAnt(20, IonType.y, 2, 8, 0, waterLoss, spectrum.getPeptide()));
        checkPeak(399.7, 76.0, 74, spectrum, delta, fragAnt(16, IonType.y, 2, 8, 0, waterLoss, spectrum.getPeptide()));
        checkPeak(400.2, 190.0, 75, spectrum, delta, fragAnt(20, IonType.y, 2, 8, 0, ammoniaLoss, spectrum.getPeptide()), fragAnt(20, IonType.y, 2, 8, 0, waterLoss, spectrum.getPeptide()));
        checkPeak(405.2, 707.0, 76, spectrum, delta, fragAnt(20, IonType.y, 1, 3, 0, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(406.2, 132.0, 77, spectrum, delta, fragAnt(19, IonType.y, 1, 3, 1, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(407.2, 55.0, 78, spectrum, delta, fragAnt(14, IonType.y, 1, 3, 2, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(408.7, 49.0, 79, spectrum, delta, fragAnt(3, IonType.y, 2, 8, 0, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(410.2, 62.0, 80, spectrum, delta, unknownAnt(15));
        checkPeak(412.2, 71.0, 81, spectrum, delta, unknownAnt(19));
        checkPeak(413.2, 99.0, 82, spectrum, delta, unknownAnt(19));
        checkPeak(415.2, 149.0, 83, spectrum, delta, unknownAnt(20));
        checkPeak(429.2, 77.0, 84, spectrum, delta, unknownAnt(16));
        checkPeak(438.2, 67.0, 85, spectrum, delta, unknownAnt(14));
        checkPeak(445.2, 41.0, 86, spectrum, delta, unknownAnt(7));
        checkPeak(449.2, 27.0, 87, spectrum, delta, unknownAnt(5));
        checkPeak(450.2, 34.0, 88, spectrum, delta, unknownAnt(7));
        checkPeak(457.2, 101.0, 89, spectrum, delta, unknownAnt(18));
        checkPeak(465.2, 66.0, 90, spectrum, delta, unknownAnt(14));
        checkPeak(466.2, 56.0, 91, spectrum, delta, unknownAnt(12));
        checkPeak(467.2, 108.0, 92, spectrum, delta, unknownAnt(20));
        checkPeak(468.2, 71.0, 93, spectrum, delta, unknownAnt(16));
        checkPeak(469.2, 1544.0, 94, spectrum, delta, fragAnt(20, IonType.b, 1, 6, 0, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(470.2, 336.0, 95, spectrum, delta, fragAnt(20, IonType.y, 2, 10, 0, waterLoss, spectrum.getPeptide()));
        checkPeak(471.2, 77.0, 96, spectrum, delta, fragAnt(17, IonType.y, 2, 10, 0, ammoniaLoss, spectrum.getPeptide()), fragAnt(17, IonType.y, 2, 10, 0, waterLoss, spectrum.getPeptide()));
        checkPeak(483.2, 56.0, 97, spectrum, delta, unknownAnt(15), fragAnt(15, IonType.b, 1, 7, 0, new NumericMass(-43.0), spectrum.getPeptide()));
        checkPeak(486.2, 133.0, 98, spectrum, delta, unknownAnt(19));
        checkPeak(487.2, 42.0, 99, spectrum, delta, unknownAnt(8));
        checkPeak(496.2, 47.0, 100, spectrum, delta, unknownAnt(8));
        checkPeak(508.3, 38.0, 101, spectrum, delta, fragAnt(7, IonType.b, 1, 7, 0, waterLoss, spectrum.getPeptide()));
        checkPeak(514.2, 94.0, 102, spectrum, delta, unknownAnt(18));
        checkPeak(515.2, 55.0, 103, spectrum, delta, fragAnt(15, IonType.y, 2, 11, 0, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(516.2, 44.0, 104, spectrum, delta, fragAnt(9, IonType.y, 1, 4, 0, waterLoss, spectrum.getPeptide()));
        checkPeak(525.2, 68.0, 105, spectrum, delta, unknownAnt(19));
        checkPeak(526.2, 1164.0, 106, spectrum, delta, fragAnt(20, IonType.b, 1, 7, 0, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(527.2, 291.0, 107, spectrum, delta, fragAnt(19, IonType.b, 1, 7, 1, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(528.2, 77.0, 108, spectrum, delta, fragAnt(19, IonType.b, 1, 7, 2, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(534.3, 60.0, 109, spectrum, delta, fragAnt(16, IonType.y, 1, 4, 0, new NumericMass(0.0), spectrum.getPeptide()), fragAnt(16, IonType.b, 2, 13, 0, new NumericMass(-43.0), spectrum.getPeptide()));
        checkPeak(538.3, 56.0, 110, spectrum, delta, unknownAnt(11));
        checkPeak(543.2, 80.0, 111, spectrum, delta, unknownAnt(17));
        checkPeak(544.2, 49.0, 112, spectrum, delta, unknownAnt(10));
        checkPeak(554.3, 42.0, 113, spectrum, delta, fragAnt(10, IonType.b, 1, 8, 0, new NumericMass(-43.0), spectrum.getPeptide()));
        checkPeak(566.3, 43.0, 114, spectrum, delta, unknownAnt(11));
        checkPeak(571.3, 41.0, 115, spectrum, delta, unknownAnt(12));
        checkPeak(585.3, 46.0, 116, spectrum, delta, fragAnt(10, IonType.y, 1, 5, 0, new NumericMass(-46.0), spectrum.getPeptide()));
        checkPeak(596.3, 76.0, 117, spectrum, delta, unknownAnt(19));
        checkPeak(597.3, 185.0, 118, spectrum, delta, fragAnt(20, IonType.b, 1, 8, 0, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(598.3, 68.0, 119, spectrum, delta, fragAnt(12, IonType.b, 1, 8, 1, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(614.3, 211.0, 120, spectrum, delta, fragAnt(20, IonType.y, 1, 5, 0, ammoniaLoss, spectrum.getPeptide()));
        checkPeak(615.3, 69.0, 121, spectrum, delta, fragAnt(18, IonType.y, 1, 5, 1, ammoniaLoss, spectrum.getPeptide()));
        checkPeak(624.3, 79.0, 122, spectrum, delta, fragAnt(17, IonType.p, 1, 14, 0, new NumericMass(-36.0), spectrum.getPeptide()));
        checkPeak(625.3, 53.0, 123, spectrum, delta, fragAnt(9, IonType.p, 1, 14, 0, new NumericMass(-35.0), spectrum.getPeptide()), fragAnt(9, IonType.p, 1, 14, 0, new NumericMass(-36.0), spectrum.getPeptide()));
        checkPeak(626.3, 39.0, 124, spectrum, delta, fragAnt(6, IonType.p, 1, 14, 1, new NumericMass(-35.0), spectrum.getPeptide()));
        checkPeak(631.3, 1278.0, 125, spectrum, delta, fragAnt(20, IonType.y, 1, 5, 0, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(632.3, 325.0, 126, spectrum, delta, fragAnt(20, IonType.y, 1, 5, 1, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(633.3, 124.0, 127, spectrum, delta, fragAnt(19, IonType.p, 1, 14, 0, waterLoss, spectrum.getPeptide()));
        checkPeak(633.8, 88.0, 128, spectrum, delta, fragAnt(11, IonType.p, 1, 14, 0, waterLoss, spectrum.getPeptide()));
        checkPeak(634.3, 84.0, 129, spectrum, delta, fragAnt(12, IonType.p, 1, 14, 0, ammoniaLoss, spectrum.getPeptide()), fragAnt(12, IonType.p, 1, 14, 0, waterLoss, spectrum.getPeptide()));
        checkPeak(642.3, 126.0, 130, spectrum, delta, fragAnt(14, IonType.p, 1, 14, 0, new NumericMass(0.0), spectrum.getPeptide()), fragAnt(14, IonType.y, 1, 6, 0, new NumericMass(-46.0), spectrum.getPeptide()));
        checkPeak(654.3, 67.0, 131, spectrum, delta, fragAnt(15, IonType.b, 1, 9, 0, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(670.3, 61.0, 132, spectrum, delta, fragAnt(16, IonType.y, 1, 6, 0, waterLoss, spectrum.getPeptide()));
        checkPeak(671.3, 321.0, 133, spectrum, delta, fragAnt(20, IonType.y, 1, 6, 0, ammoniaLoss, spectrum.getPeptide()));
        checkPeak(672.3, 91.0, 134, spectrum, delta, fragAnt(20, IonType.y, 1, 6, 1, ammoniaLoss, spectrum.getPeptide()));
        checkPeak(685.3, 60.0, 135, spectrum, delta, unknownAnt(14));
        checkPeak(687.8, 72.0, 136, spectrum, delta, fragAnt(10, IonType.y, 1, 6, 0, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(688.3, 3772.0, 137, spectrum, delta, fragAnt(20, IonType.y, 1, 6, 0, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(688.8, 90.0, 138, spectrum, delta, fragAnt(10, IonType.y, 1, 6, 1, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(689.3, 1010.0, 139, spectrum, delta, unknownAnt(20));
        checkPeak(690.3, 383.0, 140, spectrum, delta, unknownAnt(20));
        checkPeak(690.8, 60.0, 141, spectrum, delta, unknownAnt(9));
        checkPeak(691.3, 93.0, 142, spectrum, delta, unknownAnt(18));
        checkPeak(695.4, 40.0, 143, spectrum, delta, unknownAnt(7));
        checkPeak(713.4, 49.0, 144, spectrum, delta, fragAnt(14, IonType.y, 1, 7, 0, new NumericMass(-46.0), spectrum.getPeptide()));
        checkPeak(742.4, 60.0, 145, spectrum, delta, fragAnt(16, IonType.y, 1, 7, 0, ammoniaLoss, spectrum.getPeptide()));
        checkPeak(759.3, 1310.0, 146, spectrum, delta, fragAnt(20, IonType.y, 1, 7, 0, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(760.3, 389.0, 147, spectrum, delta, fragAnt(20, IonType.y, 1, 7, 1, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(761.3, 148.0, 148, spectrum, delta, fragAnt(19, IonType.y, 1, 7, 2, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(781.3, 72.0, 149, spectrum, delta, unknownAnt(20));
        checkPeak(782.4, 40.0, 150, spectrum, delta, unknownAnt(7));
        checkPeak(798.4, 65.0, 151, spectrum, delta, fragAnt(17, IonType.y, 1, 8, 0, waterLoss, spectrum.getPeptide()));
        checkPeak(799.3, 249.0, 152, spectrum, delta, fragAnt(20, IonType.y, 1, 8, 0, ammoniaLoss, spectrum.getPeptide()));
        checkPeak(800.4, 84.0, 153, spectrum, delta, fragAnt(16, IonType.y, 1, 8, 1, ammoniaLoss, spectrum.getPeptide()));
        checkPeak(801.3, 50.0, 154, spectrum, delta, fragAnt(8, IonType.y, 1, 8, 2, ammoniaLoss, spectrum.getPeptide()));
        checkPeak(815.1, 59.0, 155, spectrum, delta, unknownAnt(13));
        checkPeak(815.8, 76.0, 156, spectrum, delta, unknownAnt(18));
        checkPeak(816.4, 5688.0, 157, spectrum, delta, fragAnt(20, IonType.y, 1, 8, 0, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(816.8, 74.0, 158, spectrum, delta, fragAnt(18, IonType.y, 1, 8, 1, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(817.4, 1799.0, 159, spectrum, delta, unknownAnt(20));
        checkPeak(817.9, 88.0, 160, spectrum, delta, unknownAnt(16));
        checkPeak(818.4, 701.0, 161, spectrum, delta, unknownAnt(20));
        checkPeak(819.4, 181.0, 162, spectrum, delta, unknownAnt(19));
        checkPeak(869.5, 53.0, 163, spectrum, delta, fragAnt(13, IonType.y, 1, 9, 0, waterLoss, spectrum.getPeptide()));
        checkPeak(870.4, 88.0, 164, spectrum, delta, fragAnt(18, IonType.y, 1, 9, 0, ammoniaLoss, spectrum.getPeptide()));
        checkPeak(871.5, 52.0, 165, spectrum, delta, fragAnt(10, IonType.y, 1, 9, 1, ammoniaLoss, spectrum.getPeptide()));
        checkPeak(887.4, 3434.0, 166, spectrum, delta, fragAnt(20, IonType.y, 1, 9, 0, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(888.4, 1172.0, 167, spectrum, delta, fragAnt(20, IonType.y, 1, 9, 1, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(889.4, 403.0, 168, spectrum, delta, fragAnt(20, IonType.y, 1, 9, 2, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(890.4, 130.0, 169, spectrum, delta, fragAnt(18, IonType.y, 1, 9, 3, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(923.5, 41.0, 170, spectrum, delta, unknownAnt(8));
        checkPeak(940.4, 48.0, 171, spectrum, delta, fragAnt(11, IonType.y, 1, 10, 0, waterLoss, spectrum.getPeptide()));
        checkPeak(941.4, 52.0, 172, spectrum, delta, fragAnt(11, IonType.y, 1, 10, 0, ammoniaLoss, spectrum.getPeptide()));
        checkPeak(958.4, 2337.0, 173, spectrum, delta, fragAnt(20, IonType.y, 1, 10, 0, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(959.4, 835.0, 174, spectrum, delta, fragAnt(20, IonType.y, 1, 10, 1, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(960.4, 323.0, 175, spectrum, delta, fragAnt(20, IonType.y, 1, 10, 2, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(961.4, 113.0, 176, spectrum, delta, fragAnt(18, IonType.y, 1, 10, 3, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(1029.4, 790.0, 177, spectrum, delta, fragAnt(20, IonType.y, 1, 11, 0, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(1030.4, 334.0, 178, spectrum, delta, fragAnt(19, IonType.y, 1, 11, 1, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(1031.4, 143.0, 179, spectrum, delta, fragAnt(20, IonType.y, 1, 11, 2, new NumericMass(0.0), spectrum.getPeptide()));
        checkPeak(1031.9, 66.0, 180, spectrum, delta, unknownAnt(10));
        checkPeak(1100.5, 80.0, 181, spectrum, delta, fragAnt(12, IonType.y, 1, 12, 0, new NumericMass(0.0), spectrum.getPeptide()));
    }

    private void checkPeaksSpectrum1(PeptideConsensusSpectrum spectrum, double delta) {

        Peptide peptide = spectrum.getPeptide();
        checkPeak(86.1, 176.0, 0, spectrum, delta, fragAnt(5, IonType.a, 1, 1, 0, new NumericMass(0.0), peptide));
        checkPeak(115.1, 87.0, 1, spectrum, delta, unknownAnt(5));
        checkPeak(143.1, 187.0, 2, spectrum, delta, unknownAnt(7));
        checkPeak(155.1, 141.0, 3, spectrum, delta, unknownAnt(5));
        checkPeak(157.1, 105.0, 4, spectrum, delta, fragAnt(6, IonType.y, 1, 1, 0, waterLoss, peptide), fragAnt(6, IonType.a, 1, 2, 0, new NumericMass(0.0), peptide));
        checkPeak(169.1, 79.0, 5, spectrum, delta, unknownAnt(5));
        checkPeak(175.1, 350.0, 6, spectrum, delta, fragAnt(8, IonType.y, 1, 1, 0, new NumericMass(0.0), peptide));
        checkPeak(185.1, 1511.0, 7, spectrum, delta, fragAnt(8, IonType.b, 1, 2, 0, new NumericMass(0.0), peptide));
        checkPeak(186.1, 134.0, 8, spectrum, delta, fragAnt(6, IonType.b, 1, 2, 1, new NumericMass(0.0), peptide));
        checkPeak(200.1, 90.0, 9, spectrum, delta, unknownAnt(7));
        checkPeak(214.1, 344.0, 10, spectrum, delta, fragAnt(8, IonType.b, 2, 6, 0, new NumericMass(-43.0), peptide));
        checkPeak(226.1, 68.0, 11, spectrum, delta, unknownAnt(4));
        checkPeak(227.1, 70.0, 12, spectrum, delta, unknownAnt(4));
        checkPeak(228.1, 845.0, 13, spectrum, delta, fragAnt(8, IonType.a, 1, 3, 0, new NumericMass(0.0), peptide));
        checkPeak(229.1, 112.0, 14, spectrum, delta, fragAnt(7, IonType.a, 1, 3, 1, new NumericMass(0.0), peptide));
        checkPeak(243.1, 132.0, 15, spectrum, delta, unknownAnt(8));
        checkPeak(255.1, 59.0, 16, spectrum, delta, unknownAnt(3));
        checkPeak(255.5, 60.0, 17, spectrum, delta, unknownAnt(5));
        checkPeak(256.1, 6855.0, 18, spectrum, delta, fragAnt(8, IonType.b, 1, 3, 0, new NumericMass(0.0), peptide), fragAnt(8, IonType.y, 1, 2, 0, waterLoss, peptide));
        checkPeak(257.1, 959.0, 19, spectrum, delta, fragAnt(8, IonType.y, 1, 2, 0, ammoniaLoss, peptide));
        checkPeak(258.1, 146.0, 20, spectrum, delta, fragAnt(8, IonType.y, 1, 2, 1, ammoniaLoss, peptide));
        checkPeak(258.7, 49.0, 21, spectrum, delta, unknownAnt(3));
        checkPeak(271.1, 180.0, 22, spectrum, delta, unknownAnt(7));
        checkPeak(274.2, 258.0, 23, spectrum, delta, fragAnt(7, IonType.y, 1, 2, 0, new NumericMass(0.0), peptide));
        checkPeak(275.2, 59.0, 24, spectrum, delta, fragAnt(3, IonType.y, 2, 4, 0, new NumericMass(0.0), peptide));
        checkPeak(284.1, 96.0, 25, spectrum, delta, fragAnt(6, IonType.b, 1, 4, 0, new NumericMass(-43.0), peptide));
        checkPeak(285.2, 202.0, 26, spectrum, delta, fragAnt(7, IonType.b, 1, 4, 1, new NumericMass(-43.0), peptide));
        checkPeak(297.1, 82.0, 27, spectrum, delta, unknownAnt(5));
        checkPeak(299.2, 151.0, 28, spectrum, delta, fragAnt(8, IonType.b, 2, 8, 0, new NumericMass(0.0), peptide), fragAnt(8, IonType.a, 1, 4, 0, new NumericMass(0.0), peptide));
        checkPeak(300.1, 121.0, 29, spectrum, delta, fragAnt(6, IonType.b, 2, 8, 1, new NumericMass(0.0), peptide));
        checkPeak(310.1, 92.0, 30, spectrum, delta, fragAnt(6, IonType.b, 1, 4, 0, ammoniaLoss, peptide));
        checkPeak(311.2, 67.0, 31, spectrum, delta, fragAnt(4, IonType.y, 2, 6, 0, new NumericMass(-82.0), peptide));
        checkPeak(314.2, 76.0, 32, spectrum, delta, unknownAnt(5));
        checkPeak(315.2, 66.0, 33, spectrum, delta, fragAnt(4, IonType.y, 2, 5, 0, waterLoss, peptide), fragAnt(4, IonType.y, 2, 5, 0, ammoniaLoss, peptide));
        checkPeak(324.2, 65.0, 34, spectrum, delta, fragAnt(5, IonType.y, 2, 5, 0, new NumericMass(0.0), peptide));
        checkPeak(326.2, 87.0, 35, spectrum, delta, unknownAnt(5));
        checkPeak(327.2, 10000.0, 36, spectrum, delta, fragAnt(8, IonType.b, 1, 4, 0, new NumericMass(0.0), peptide), fragAnt(8, IonType.b, 2, 9, 0, new NumericMass(0.0), peptide));
        checkPeak(327.7, 82.0, 37, spectrum, delta, fragAnt(5, IonType.b, 2, 9, 0, new NumericMass(0.0), peptide), fragAnt(5, IonType.b, 1, 4, 0, new NumericMass(0.0), peptide));
        checkPeak(328.2, 1420.0, 38, spectrum, delta, fragAnt(8, IonType.b, 2, 9, 0, new NumericMass(0.0), peptide));
        checkPeak(328.6, 84.0, 39, spectrum, delta, unknownAnt(6));
        checkPeak(329.2, 266.0, 40, spectrum, delta, fragAnt(7, IonType.y, 2, 6, 0, new NumericMass(-46.0), peptide));
        checkPeak(330.2, 63.0, 41, spectrum, delta, fragAnt(3, IonType.y, 2, 6, 0, new NumericMass(-44.0), peptide), fragAnt(3, IonType.y, 2, 6, 0, new NumericMass(-46.0), peptide));
        checkPeak(340.2, 92.0, 42, spectrum, delta, unknownAnt(5));
        checkPeak(342.2, 178.0, 43, spectrum, delta, unknownAnt(8));
        checkPeak(352.7, 82.0, 44, spectrum, delta, fragAnt(5, IonType.y, 2, 6, 0, new NumericMass(0.0), peptide));
        checkPeak(353.2, 51.0, 45, spectrum, delta, fragAnt(3, IonType.y, 2, 6, 1, new NumericMass(0.0), peptide));
        checkPeak(356.2, 104.0, 46, spectrum, delta, fragAnt(8, IonType.y, 2, 7, 0, new NumericMass(-64.0), peptide));
        checkPeak(357.2, 353.0, 47, spectrum, delta, fragAnt(8, IonType.y, 1, 3, 0, new NumericMass(-64.0), peptide));
        checkPeak(358.2, 86.0, 48, spectrum, delta, fragAnt(7, IonType.y, 1, 3, 1, new NumericMass(-64.0), peptide));
        checkPeak(367.1, 160.0, 49, spectrum, delta, fragAnt(6, IonType.y, 2, 7, 0, new NumericMass(-43.0), peptide));
        checkPeak(368.2, 75.0, 50, spectrum, delta, fragAnt(8, IonType.y, 2, 7, 2, new NumericMass(-43.0), peptide));
        checkPeak(385.1, 143.0, 51, spectrum, delta, fragAnt(7, IonType.y, 2, 8, 0, new NumericMass(-64.0), peptide));
        checkPeak(397.2, 77.0, 52, spectrum, delta, unknownAnt(4));
        checkPeak(398.2, 6178.0, 53, spectrum, delta, fragAnt(8, IonType.b, 1, 5, 0, new NumericMass(0.0), peptide));
        checkPeak(398.7, 98.0, 54, spectrum, delta, fragAnt(7, IonType.b, 1, 5, 1, new NumericMass(0.0), peptide));
        checkPeak(399.2, 1060.0, 55, spectrum, delta, unknownAnt(8));
        checkPeak(400.2, 214.0, 56, spectrum, delta, unknownAnt(8));
        checkPeak(404.2, 61.0, 57, spectrum, delta, fragAnt(3, IonType.y, 1, 3, 0, ammoniaLoss, peptide));
        checkPeak(413.2, 153.0, 58, spectrum, delta, unknownAnt(7));
        checkPeak(416.7, 44.0, 59, spectrum, delta, fragAnt(4, IonType.y, 2, 8, 0, new NumericMass(0.0), peptide));
        checkPeak(421.2, 220.0, 60, spectrum, delta, fragAnt(7, IonType.y, 1, 3, 0, new NumericMass(0.0), peptide));
        checkPeak(422.2, 53.0, 61, spectrum, delta, fragAnt(4, IonType.y, 1, 3, 1, new NumericMass(0.0), peptide));
        checkPeak(431.2, 49.0, 62, spectrum, delta, unknownAnt(3));
        checkPeak(438.2, 75.0, 63, spectrum, delta, unknownAnt(4));
        checkPeak(452.2, 88.0, 64, spectrum, delta, fragAnt(6, IonType.y, 2, 9, 0, new NumericMass(0.0), peptide), fragAnt(6, IonType.b, 1, 6, 0, ammoniaLoss, peptide));
        checkPeak(466.2, 80.0, 65, spectrum, delta, unknownAnt(7));
        checkPeak(469.2, 1896.0, 66, spectrum, delta, fragAnt(8, IonType.b, 1, 6, 0, new NumericMass(0.0), peptide));
        checkPeak(470.2, 397.0, 67, spectrum, delta, fragAnt(8, IonType.b, 1, 6, 1, new NumericMass(0.0), peptide));
        checkPeak(471.3, 90.0, 68, spectrum, delta, fragAnt(4, IonType.b, 1, 6, 2, new NumericMass(0.0), peptide));
        checkPeak(473.2, 52.0, 69, spectrum, delta, unknownAnt(3));
        checkPeak(484.2, 195.0, 70, spectrum, delta, unknownAnt(6));
        checkPeak(526.3, 1472.0, 71, spectrum, delta, fragAnt(8, IonType.b, 1, 7, 0, new NumericMass(0.0), peptide));
        checkPeak(527.3, 303.0, 72, spectrum, delta, fragAnt(8, IonType.b, 1, 7, 1, new NumericMass(0.0), peptide));
        checkPeak(528.3, 83.0, 73, spectrum, delta, fragAnt(6, IonType.b, 1, 7, 2, new NumericMass(0.0), peptide));
        checkPeak(541.3, 54.0, 74, spectrum, delta, unknownAnt(3), fragAnt(3, IonType.b, 2, 13, 0, new NumericMass(-46.0), peptide));
        checkPeak(550.3, 49.0, 75, spectrum, delta, fragAnt(3, IonType.y, 1, 4, 0, new NumericMass(0.0), peptide), fragAnt(3, IonType.y, 2, 12, 0, ammoniaLoss, peptide));
        checkPeak(551.3, 40.0, 76, spectrum, delta, fragAnt(2, IonType.y, 1, 4, 1, new NumericMass(0.0), peptide));
        checkPeak(554.3, 47.0, 77, spectrum, delta, unknownAnt(4), fragAnt(4, IonType.b, 1, 8, 0, new NumericMass(-43.0), peptide));
        checkPeak(555.3, 88.0, 78, spectrum, delta, unknownAnt(5));
        checkPeak(556.3, 68.0, 79, spectrum, delta, fragAnt(3, IonType.b, 2, 13, 2, waterLoss, peptide));
        checkPeak(559.2, 46.0, 80, spectrum, delta, fragAnt(3, IonType.y, 2, 12, 0, new NumericMass(0.0), peptide));
        checkPeak(566.3, 101.0, 81, spectrum, delta, unknownAnt(7));
        checkPeak(583.3, 1079.0, 82, spectrum, delta, fragAnt(8, IonType.y, 1, 5, 0, new NumericMass(-64.0), peptide));
        checkPeak(584.3, 295.0, 83, spectrum, delta, fragAnt(7, IonType.y, 1, 5, 1, new NumericMass(-64.0), peptide));
        checkPeak(585.3, 105.0, 84, spectrum, delta, unknownAnt(8), fragAnt(8, IonType.y, 2, 13, 0, ammoniaLoss, peptide));
        checkPeak(586.3, 92.0, 85, spectrum, delta, fragAnt(6, IonType.y, 2, 13, 0, ammoniaLoss, peptide));
        checkPeak(597.3, 199.0, 86, spectrum, delta, fragAnt(8, IonType.b, 1, 8, 0, new NumericMass(0.0), peptide));
        checkPeak(598.4, 83.0, 87, spectrum, delta, fragAnt(4, IonType.b, 1, 8, 1, new NumericMass(0.0), peptide));
        checkPeak(613.3, 125.0, 88, spectrum, delta, unknownAnt(8));
        checkPeak(622.3, 75.0, 89, spectrum, delta, fragAnt(4, IonType.y, 1, 6, 0, new NumericMass(-82.0), peptide));
        checkPeak(623.3, 191.0, 90, spectrum, delta, fragAnt(7, IonType.y, 1, 6, 1, new NumericMass(-82.0), peptide));
        checkPeak(624.4, 60.0, 91, spectrum, delta, fragAnt(5, IonType.y, 1, 6, 2, new NumericMass(-82.0), peptide));
        checkPeak(630.3, 86.0, 92, spectrum, delta, fragAnt(7, IonType.y, 1, 5, 0, ammoniaLoss, peptide));
        checkPeak(639.2, 77.0, 93, spectrum, delta, unknownAnt(2));
        checkPeak(640.3, 2368.0, 94, spectrum, delta, fragAnt(8, IonType.y, 1, 6, 0, new NumericMass(-64.0), peptide));
        checkPeak(641.3, 602.0, 95, spectrum, delta, fragAnt(8, IonType.p, 1, 14, 0, waterLoss, peptide));
        checkPeak(641.8, 102.0, 96, spectrum, delta, fragAnt(5, IonType.p, 1, 14, 0, waterLoss, peptide));
        checkPeak(642.3, 169.0, 97, spectrum, delta, fragAnt(8, IonType.p, 1, 14, 0, ammoniaLoss, peptide), fragAnt(8, IonType.p, 1, 14, 0, waterLoss, peptide));
        checkPeak(643.4, 69.0, 98, spectrum, delta, fragAnt(3, IonType.p, 1, 14, 1, ammoniaLoss, peptide));
        checkPeak(647.3, 1025.0, 99, spectrum, delta, fragAnt(7, IonType.y, 1, 5, 0, new NumericMass(0.0), peptide));
        checkPeak(648.3, 272.0, 100, spectrum, delta, fragAnt(7, IonType.y, 1, 5, 1, new NumericMass(0.0), peptide));
        checkPeak(649.3, 106.0, 101, spectrum, delta, fragAnt(5, IonType.y, 1, 5, 2, new NumericMass(0.0), peptide));
        checkPeak(650.3, 143.0, 102, spectrum, delta, fragAnt(7, IonType.p, 1, 14, 0, new NumericMass(0.0), peptide));
        checkPeak(652.3, 205.0, 103, spectrum, delta, unknownAnt(5));
        checkPeak(682.3, 81.0, 104, spectrum, delta, unknownAnt(4));
        checkPeak(684.3, 62.0, 105, spectrum, delta, unknownAnt(5));
        checkPeak(686.4, 66.0, 106, spectrum, delta, fragAnt(4, IonType.y, 1, 6, 0, waterLoss, peptide));
        checkPeak(687.3, 89.0, 107, spectrum, delta, fragAnt(7, IonType.y, 1, 6, 0, ammoniaLoss, peptide));
        checkPeak(689.3, 42.0, 108, spectrum, delta, unknownAnt(2));
        checkPeak(704.3, 2547.0, 109, spectrum, delta, fragAnt(8, IonType.y, 1, 6, 0, new NumericMass(0.0), peptide));
        checkPeak(705.3, 683.0, 110, spectrum, delta, fragAnt(7, IonType.y, 1, 6, 1, new NumericMass(0.0), peptide));
        checkPeak(705.8, 107.0, 111, spectrum, delta, unknownAnt(6));
        checkPeak(706.4, 205.0, 112, spectrum, delta, unknownAnt(7));
        checkPeak(711.4, 861.0, 113, spectrum, delta, fragAnt(8, IonType.y, 1, 7, 0, new NumericMass(-64.0), peptide));
        checkPeak(712.4, 290.0, 114, spectrum, delta, fragAnt(8, IonType.y, 1, 7, 1, new NumericMass(-64.0), peptide));
        checkPeak(713.5, 74.0, 115, spectrum, delta, fragAnt(4, IonType.y, 1, 7, 2, new NumericMass(-64.0), peptide));
        checkPeak(741.4, 152.0, 116, spectrum, delta, unknownAnt(6));
        checkPeak(742.3, 42.0, 117, spectrum, delta, unknownAnt(4));
        checkPeak(750.4, 49.0, 118, spectrum, delta, fragAnt(3, IonType.y, 1, 8, 0, new NumericMass(-82.0), peptide));
        checkPeak(751.4, 150.0, 119, spectrum, delta, fragAnt(8, IonType.b, 1, 10, 0, new NumericMass(0.0), peptide));
        checkPeak(752.4, 47.0, 120, spectrum, delta, fragAnt(3, IonType.b, 1, 10, 1, new NumericMass(0.0), peptide));
        checkPeak(767.7, 259.0, 121, spectrum, delta, unknownAnt(4));
        checkPeak(768.4, 3565.0, 122, spectrum, delta, fragAnt(8, IonType.y, 1, 8, 0, new NumericMass(-64.0), peptide));
        checkPeak(769.4, 1135.0, 123, spectrum, delta, fragAnt(8, IonType.y, 1, 8, 1, new NumericMass(-64.0), peptide));
        checkPeak(770.3, 277.0, 124, spectrum, delta, fragAnt(8, IonType.y, 1, 8, 2, new NumericMass(-64.0), peptide));
        checkPeak(770.9, 44.0, 125, spectrum, delta, unknownAnt(3));
        checkPeak(771.4, 53.0, 126, spectrum, delta, unknownAnt(5));
        checkPeak(775.4, 676.0, 127, spectrum, delta, fragAnt(8, IonType.y, 1, 7, 0, new NumericMass(0.0), peptide));
        checkPeak(775.8, 55.0, 128, spectrum, delta, fragAnt(3, IonType.y, 1, 7, 1, new NumericMass(0.0), peptide));
        checkPeak(776.4, 210.0, 129, spectrum, delta, unknownAnt(8));
        checkPeak(777.4, 95.0, 130, spectrum, delta, unknownAnt(6));
        checkPeak(812.4, 89.0, 131, spectrum, delta, unknownAnt(5));
        checkPeak(814.5, 60.0, 132, spectrum, delta, fragAnt(4, IonType.y, 1, 8, 0, waterLoss, peptide));
        checkPeak(815.3, 59.0, 133, spectrum, delta, fragAnt(5, IonType.y, 1, 8, 0, ammoniaLoss, peptide));
        checkPeak(817.3, 41.0, 134, spectrum, delta, unknownAnt(2));
        checkPeak(822.4, 82.0, 135, spectrum, delta, unknownAnt(4));
        checkPeak(832.4, 2937.0, 136, spectrum, delta, fragAnt(8, IonType.y, 1, 8, 0, new NumericMass(0.0), peptide));
        checkPeak(833.4, 904.0, 137, spectrum, delta, fragAnt(8, IonType.y, 1, 8, 1, new NumericMass(0.0), peptide));
        checkPeak(834.4, 321.0, 138, spectrum, delta, unknownAnt(8));
        checkPeak(835.4, 101.0, 139, spectrum, delta, fragAnt(5, IonType.b, 1, 11, 1, new NumericMass(-46.0), peptide));
        checkPeak(839.4, 1993.0, 140, spectrum, delta, fragAnt(8, IonType.y, 1, 9, 0, new NumericMass(-64.0), peptide));
        checkPeak(840.4, 595.0, 141, spectrum, delta, fragAnt(8, IonType.y, 1, 9, 1, new NumericMass(-64.0), peptide));
        checkPeak(840.8, 238.0, 142, spectrum, delta, unknownAnt(5));
        checkPeak(841.4, 148.0, 143, spectrum, delta, unknownAnt(7));
        checkPeak(883.4, 58.0, 144, spectrum, delta, unknownAnt(4));
        checkPeak(903.4, 1404.0, 145, spectrum, delta, fragAnt(8, IonType.y, 1, 9, 0, new NumericMass(0.0), peptide));
        checkPeak(904.4, 492.0, 146, spectrum, delta, fragAnt(8, IonType.y, 1, 9, 1, new NumericMass(0.0), peptide));
        checkPeak(905.5, 207.0, 147, spectrum, delta, fragAnt(7, IonType.y, 1, 9, 2, new NumericMass(0.0), peptide));
        checkPeak(910.5, 1183.0, 148, spectrum, delta, fragAnt(8, IonType.y, 1, 10, 0, new NumericMass(-64.0), peptide));
        checkPeak(911.5, 419.0, 149, spectrum, delta, fragAnt(8, IonType.y, 1, 10, 1, new NumericMass(-64.0), peptide));
        checkPeak(911.9, 56.0, 150, spectrum, delta, unknownAnt(4));
        checkPeak(912.5, 103.0, 151, spectrum, delta, unknownAnt(6));
        checkPeak(974.5, 797.0, 152, spectrum, delta, fragAnt(8, IonType.y, 1, 10, 0, new NumericMass(0.0), peptide));
        checkPeak(975.5, 239.0, 153, spectrum, delta, fragAnt(8, IonType.y, 1, 10, 1, new NumericMass(0.0), peptide));
        checkPeak(976.5, 123.0, 154, spectrum, delta, fragAnt(6, IonType.y, 1, 10, 2, new NumericMass(0.0), peptide));
        checkPeak(981.5, 396.0, 155, spectrum, delta, fragAnt(8, IonType.y, 1, 11, 0, new NumericMass(-64.0), peptide), fragAnt(8, IonType.b, 1, 12, 0, new NumericMass(-46.0), peptide));
        checkPeak(982.5, 178.0, 156, spectrum, delta, fragAnt(7, IonType.y, 1, 11, 1, new NumericMass(-64.0), peptide));
        checkPeak(982.9, 69.0, 157, spectrum, delta, fragAnt(4, IonType.b, 1, 12, 0, new NumericMass(-44.0), peptide));
        checkPeak(983.5, 64.0, 158, spectrum, delta, fragAnt(4, IonType.b, 1, 12, 0, new NumericMass(-44.0), peptide));
        checkPeak(1045.5, 283.0, 159, spectrum, delta, fragAnt(7, IonType.y, 1, 11, 0, new NumericMass(0.0), peptide));
        checkPeak(1046.6, 155.0, 160, spectrum, delta, fragAnt(5, IonType.y, 1, 11, 1, new NumericMass(0.0), peptide));
    }

    @Test
    public void testBuilder() throws Exception {

        StringReader stringReader = new StringReader("Name: AAAAAAGAGPEMVR/2\n" +
                "MW: 1285.645\n" +
                "Comment: Spec=Consensus Pep=Tryptic Fullname=-.AAAAAAGAGPEMVR.G/2 Mods=1/0,A,MyMod Parent=642.822 Inst=qtof Mz_diff=-0.012 Mz_exact=642.8224 Mz_av=643.234 Protein=\"IPI00003479.1|SWISS-PROT:P28482|TREMBL:Q499G7|REFSEQ_NP:NP_620407;NP_002736|ENSEMBL:ENSP00000215832|H-INV:HIT000038155|VEGA:OTTHUMP00000028754 Tax_Id=9606 Mitogen-activated protein kinase 1\" Pseq=40 Organism=\"human\" Se=3^X19:ex=1.5e-006/1.796e-006,td=0/3.292e+006,sd=0/0,hs=48/1.942,bs=3.9e-008,b2=7e-008,bd=3.08e+007^O20:ex=6.74e-013/9.778e-011,td=415/3.73e+008,pr=5.125e-016/7.496e-014,bs=3.14e-014,b2=4.39e-014,bd=3740^P17:sc=32/1.147,dc=21.7/1.076,ps=3.59/0.1929,bs=0 Sample=6/ca_hela2_bentscheff_cam,2,2/ca_k562_2_bantscheff_cam,4,5/gu_platelets_cofradic_none,0,1/mpi_a459_cam,6,6/mpi_hct116_cam,3,3/mpi_jurkat_cam,5,5 Nreps=20/28 Missing=0.0702/0.0402 Parent_med=642.82/0.02 Max2med_orig=227.9/103.0 Dotfull=0.903/0.028 Dot_cons=0.945/0.032 Unassign_all=0.109 Unassigned=0.052 Dotbest=0.95 Flags=0,0,0 Naa=14 DUScorr=3.3/1.4/2.9 Dottheory=0.88 Pfin=1.1e+015 Probcorr=1 Tfratio=3e+009 Pfract=0\n" +
                "Num peaks: 3\n" +
                "70.1    88      one\n" +
                "112.1   45      two\n" +
                "114.1   47      three\n" +
                "");


        ModificationResolver modResolver = mock(ModificationResolver.class);
        Modification modification = Modification.parseModification("O");
        when(modResolver.resolve("MyMod")).thenReturn(Optional.of(modification));

        PepLibPeakAnnotation peakAnnotation1 = new PepLibPeakAnnotation(1, 0.1, 0.01);
        PepLibPeakAnnotation peakAnnotation2 = new PepLibPeakAnnotation(2, 0.2, 0.02);
        PepLibPeakAnnotation peakAnnotation3 = new PepLibPeakAnnotation(3, 0.3, 0.03);

        AnnotationResolver annotationResolver = mock(AnnotationResolver.class);
        when(annotationResolver.resolveAnnotations(eq("one"), any(Peptide.class))).thenReturn(Collections.singletonList(peakAnnotation1));
        when(annotationResolver.resolveAnnotations(eq("two"), any(Peptide.class))).thenReturn(Collections.singletonList(peakAnnotation2));
        when(annotationResolver.resolveAnnotations(eq("three"), any(Peptide.class))).thenReturn(Collections.singletonList(peakAnnotation3));

        PeakProcessorChain<PepLibPeakAnnotation> processorChain = new PeakProcessorChain<PepLibPeakAnnotation>()
                .add(new SqrtTransformer<PepLibPeakAnnotation>());

        SpectraLibCommentParser commentParser = Mockito.spy(new MspCommentParser());

        MspReader reader = MspReader.newBuilder(stringReader, URIBuilder.UNDEFINED_URI, PeakList.Precision.FLOAT)
                .useModificationResolver(modResolver)
                .usePeakProcessorChain(processorChain)
                .useAnnotationResolver(annotationResolver)
                .useSpectraLibCommentParser(commentParser)
                .build();

        Assert.assertEquals(true, reader.hasNext());
        PeptideConsensusSpectrum spectrum = reader.next();

        Assert.assertEquals(PeakList.Precision.FLOAT, spectrum.getPrecision());
        Assert.assertEquals(Peptide.parse("A(O)AAAAAGAGPEMVR"), spectrum.getPeptide());

        Assert.assertEquals(Collections.singletonList(peakAnnotation1), spectrum.getAnnotations(0));
        Assert.assertEquals(Collections.singletonList(peakAnnotation2), spectrum.getAnnotations(1));
        Assert.assertEquals(Collections.singletonList(peakAnnotation3), spectrum.getAnnotations(2));

        double mzDelta = 0.00001;
        Assert.assertEquals(Math.sqrt(88), spectrum.getIntensity(0), mzDelta);
        Assert.assertEquals(Math.sqrt(45), spectrum.getIntensity(1), mzDelta);
        Assert.assertEquals(Math.sqrt(47), spectrum.getIntensity(2), mzDelta);

        verify(commentParser).parseComment(anyString(), any(LibrarySpectrumBuilder.class));
    }
}
