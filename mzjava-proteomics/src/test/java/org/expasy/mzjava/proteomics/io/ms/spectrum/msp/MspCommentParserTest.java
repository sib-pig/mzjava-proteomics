/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.io.ms.spectrum.msp;

import org.expasy.mzjava.proteomics.io.ms.spectrum.sptxt.LibrarySpectrumBuilder;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class MspCommentParserTest {

    @Test
    public void testParseComment() throws Exception {

        MspCommentParser commentParser = new MspCommentParser();

        LibrarySpectrumBuilder builder = mock(LibrarySpectrumBuilder.class);
        commentParser.parseComment("Spec=Consensus Pep=Tryptic Fullname=-.AAAAAAGAGPEMVR.G/2 Mods=1/0,A,Acetyl Parent=642.822 Inst=qtof Mz_diff=-0.012 Mz_exact=642.8224 Mz_av=643.234 Protein=\"IPI00003479.1|SWISS-PROT:P28482|TREMBL:Q499G7|REFSEQ_NP:NP_620407;NP_002736|ENSEMBL:ENSP00000215832|H-INV:HIT000038155|VEGA:OTTHUMP00000028754 Tax_Id=9606 Mitogen-activated protein kinase 1\" Pseq=40 Organism=\"human\" Se=3^X19:ex=1.5e-006/1.796e-006,td=0/3.292e+006,sd=0/0,hs=48/1.942,bs=3.9e-008,b2=7e-008,bd=3.08e+007^O20:ex=6.74e-013/9.778e-011,td=415/3.73e+008,pr=5.125e-016/7.496e-014,bs=3.14e-014,b2=4.39e-014,bd=3740^P17:sc=32/1.147,dc=21.7/1.076,ps=3.59/0.1929,bs=0 Sample=6/ca_hela2_bentscheff_cam,2,2/ca_k562_2_bantscheff_cam,4,5/gu_platelets_cofradic_none,0,1/mpi_a459_cam,6,6/mpi_hct116_cam,3,3/mpi_jurkat_cam,5,5 Nreps=20/28 Missing=0.0702/0.0402 Parent_med=642.82/0.02 Max2med_orig=227.9/103.0 Dotfull=0.903/0.028 Dot_cons=0.945/0.032 Unassign_all=0.109 Unassigned=0.052 Dotbest=0.95 Flags=0,0,0 Naa=14 DUScorr=3.3/1.4/2.9 Dottheory=0.88 Pfin=1.1e+015 Probcorr=1 Tfratio=3e+009 Pfract=0",
                builder);

        verify(builder).setPrecursorMz(642.822);
        verify(builder).addMod(0, "Acetyl");
        verify(builder).addProteinAccessionNumber("IPI00003479.1|SWISS-PROT:P28482|TREMBL:Q499G7|REFSEQ_NP:NP_620407;NP_002736|ENSEMBL:ENSP00000215832|H-INV:HIT000038155|VEGA:OTTHUMP00000028754 Tax_Id=9606 Mitogen-activated protein kinase 1");
        verifyNoMoreInteractions(builder);
    }
}
