/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.io.ms.ident;

import com.google.common.base.Optional;
import org.expasy.mzjava.core.mol.AtomicSymbol;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.proteomics.ms.ident.ModificationMatch;
import org.expasy.mzjava.proteomics.ms.ident.PeptideMatch;
import org.expasy.mzjava.proteomics.ms.ident.SpectrumIdentifier;
import org.expasy.mzjava.core.ms.spectrum.TimeUnit;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.File;
import java.sql.ResultSet;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class MaxQuantPsmReaderTest {
    @Test
    public void testParse() throws Exception {

        MaxQuantPsmReader maxQuantReader = new MaxQuantPsmReader();

        PSMReaderCallback callback = mock(PSMReaderCallback.class);

        maxQuantReader.parse(new File(getClass().getResource("max_quant_msms.txt").getFile()), callback);

        SpectrumIdentifier expectedIdentifier = new SpectrumIdentifier("KH63_cow_MFGM_band5.4155.4155.2");
        expectedIdentifier.setAssumedCharge(2);
        expectedIdentifier.setIndex(2383);
        expectedIdentifier.addScanNumber(4155);
        expectedIdentifier.addRetentionTime(38.689, TimeUnit.MINUTE);
        expectedIdentifier.setPrecursorNeutralMass(2260.9881);

        PeptideMatch expectedPeptideMatch = new PeptideMatch("AAANFFSASCVPCADQSSFPK");
        expectedPeptideMatch.setNumMissedCleavages(Optional.of(0));
        expectedPeptideMatch.setTotalNumIons(Optional.of(25));
        expectedPeptideMatch.addScore("score", 96.848);
        expectedPeptideMatch.addScore("delta-score", 68.299);
        expectedPeptideMatch.setMassDiff(Optional.of(0.046076));

        Mockito.verify(callback, times(1)).resultRead(expectedIdentifier, expectedPeptideMatch);
    }

    @Test
    public void testParseAll() throws Exception {

        MaxQuantPsmReader maxQuantReader = new MaxQuantPsmReader();

        PSMReaderCallback callback = mock(PSMReaderCallback.class);

        maxQuantReader.parse(new File(getClass().getResource("max_quant_2_msms.txt").getFile()), callback);

        Mockito.verify(callback, times(7490)).resultRead(any(SpectrumIdentifier.class), any(PeptideMatch.class));
    }

    @Test
    public void testMakePeptide() throws Exception {

        MaxQuantPsmReader reader = new MaxQuantPsmReader();

        ResultSet results = mock(ResultSet.class);
        Mockito.when(results.getString("Modified sequence")).thenReturn("_(ac)AALTQM(ox)PQFKK_");
        Optional<PeptideMatch> actualMatchBuilder = reader.makePeptideMatch(results);

        PeptideMatch expectedPeptideMatch = new PeptideMatch("AALTQMPQFKK");
        expectedPeptideMatch.addModificationMatch(ModAttachment.N_TERM, new ModificationMatch(new Modification("Acetyl", new Composition.Builder(AtomicSymbol.H, 2).add(AtomicSymbol.C, 2).add(AtomicSymbol.O).build()), AminoAcid.S, 12, ModAttachment.SIDE_CHAIN));
        expectedPeptideMatch.addModificationMatch(5, new ModificationMatch(new Modification("Oxidation", new Composition.Builder(AtomicSymbol.O).build()), AminoAcid.S, 12, ModAttachment.SIDE_CHAIN));

        Assert.assertEquals(expectedPeptideMatch, actualMatchBuilder.get());
    }
}
