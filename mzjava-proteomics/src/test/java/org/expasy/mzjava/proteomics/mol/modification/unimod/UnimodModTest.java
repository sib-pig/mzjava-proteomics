/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.mol.modification.unimod;

import gnu.trove.map.TObjectIntMap;
import gnu.trove.map.hash.TObjectIntHashMap;
import org.expasy.mzjava.core.mol.Atom;
import org.expasy.mzjava.core.mol.AtomicSymbol;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.core.mol.PeriodicTable;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class UnimodModTest {

    @Test
    public void testBuilder() throws Exception {

        UnimodMod.Builder builder = new UnimodMod.Builder();

        builder.setApproved(true);
        builder.setRecordId(12);
        builder.setInterimName("AB_old_ICATd8");
        builder.setPsiMsName("ICAT-D:2H(8)");
        builder.setFullName("Applied Biosystems original ICAT(TM) d8");

        //"H(26) 2H(8) C(20) N(4) O(5) S"
        TObjectIntMap<Atom> atomCountMap = new TObjectIntHashMap<Atom>();
        atomCountMap.put(PeriodicTable.H, 26);
        atomCountMap.put(PeriodicTable.getInstance().getAtom(AtomicSymbol.H, 2), 8);
        atomCountMap.put(PeriodicTable.C, 20);
        atomCountMap.put(PeriodicTable.N, 4);
        atomCountMap.put(PeriodicTable.O, 5);
        atomCountMap.put(PeriodicTable.S, 1);
        builder.setComposition(atomCountMap);

        UnimodMod mod = builder.build();
        Assert.assertEquals(true, mod.isApproved());
        Assert.assertEquals(12, mod.getRecordId());
        Assert.assertEquals("AB_old_ICATd8", mod.getInterimName());
        Assert.assertEquals("ICAT-D:2H(8)", mod.getLabel());
        Assert.assertEquals("Applied Biosystems original ICAT(TM) d8", mod.getFullName());
        Assert.assertEquals(450.275205, mod.getMolecularMass(), 0.000001);

        Composition composition = mod.getComposition();
        Assert.assertEquals(26, composition.getCount(PeriodicTable.H));
        Assert.assertEquals(8, composition.getCount(PeriodicTable.getInstance().getAtom(AtomicSymbol.H, 2)));
        Assert.assertEquals(20, composition.getCount(PeriodicTable.C));
        Assert.assertEquals(4, composition.getCount(PeriodicTable.N));
        Assert.assertEquals(5, composition.getCount(PeriodicTable.O));
        Assert.assertEquals(1, composition.getCount(PeriodicTable.S));
    }
}
