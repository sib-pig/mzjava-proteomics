/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.io.ms.spectrum.sptxt;

import com.google.common.base.Optional;
import org.expasy.mzjava.core.mol.NumericMass;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.ModificationResolver;
import org.expasy.mzjava.proteomics.mol.modification.unimod.UnimodManager;
import org.expasy.mzjava.proteomics.mol.modification.unimod.UnimodModificationResolver;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;
import org.expasy.mzjava.proteomics.ms.spectrum.PepFragAnnotation;
import org.expasy.mzjava.proteomics.ms.spectrum.PepLibPeakAnnotation;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class LibrarySpectrumBuilderTest {
    @Test
    public void testSetPeptideSequence() throws Exception {
        LibrarySpectrumBuilder spectrumBuilder = new LibrarySpectrumBuilder();

        spectrumBuilder.setPeptideSequence("PEPTIDE");
        spectrumBuilder.setPeptideSequence("PEPTIDE");

        try {

            spectrumBuilder.setPeptideSequence("FALSE");

        } catch (IllegalStateException e) {
            Assert.assertTrue(e.toString().contains("FALSE"));
            Assert.assertTrue(e.toString().contains("PEPTIDE"));
        }

    }

    @Test
    public void testBuild() throws Exception {
        LibrarySpectrumBuilder spectrumBuilder = new LibrarySpectrumBuilder();

        spectrumBuilder.setPeptideSequence("CAMYELVKVGHNLVGEVIR");
        spectrumBuilder.addMod(ModAttachment.N_TERM, "Acetyl");
        spectrumBuilder.addMod(3, "Phospho");

        spectrumBuilder.addPeak(416.1667,1004.0,"b6-46^2/-0.48,b10-18^3/0.99,b10-17^3/0.66");
        spectrumBuilder.addPeak(515.1667,949.5,"y14-17^3/-0.63,y14-18^3/-0.30,b13-82^3/-0.56");

        ModificationResolver modResolver = new UnimodModificationResolver();
        AnnotationResolver annotationResolver = new SpectraStAnnotationResolver();

        PeptideConsensusSpectrum spectrum = spectrumBuilder.build(PeakList.Precision.DOUBLE, modResolver, annotationResolver, false);

        Assert.assertEquals(spectrum.size(),2);
        Peptide peptide = Peptide.parse("(Acetyl)_CAMY(Phospho)EL");
        PepFragAnnotation fragAnnot1 = new PepFragAnnotation.Builder(IonType.b,2,peptide).setNeutralLoss(new NumericMass(-46.0)).build();
        Assert.assertEquals(spectrum.getAnnotations(0).get(0),new PepLibPeakAnnotation(0,0.0,0.0, Optional.of(fragAnnot1)));

        Assert.assertEquals(spectrum.getMz(0),416.1667,0.00001);
        Assert.assertEquals(spectrum.getIntensity(1),949.5,0.00001);

        Assert.assertTrue(spectrum.getPeptide().equals(Peptide.parse("(Acetyl)_CAMY(Phospho)ELVKVGHNLVGEVIR")));
        Assert.assertEquals(spectrum.getPeptide().getModificationsAt(3, ModAttachment.sideChainSet).get(0), UnimodManager.getModification("Phospho").get());
    }

}
