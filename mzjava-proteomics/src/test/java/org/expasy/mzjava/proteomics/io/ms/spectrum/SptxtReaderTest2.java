/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.io.ms.spectrum;

import com.google.common.base.Optional;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessorChain;
import org.expasy.mzjava.core.ms.peaklist.peaktransformer.SqrtTransformer;
import org.expasy.mzjava.proteomics.io.ms.spectrum.sptxt.AnnotationResolver;
import org.expasy.mzjava.proteomics.io.ms.spectrum.sptxt.LibrarySpectrumBuilder;
import org.expasy.mzjava.proteomics.io.ms.spectrum.sptxt.SpectraLibCommentParser;
import org.expasy.mzjava.proteomics.io.ms.spectrum.sptxt.SpectraStCommentParser;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.proteomics.mol.modification.ModificationResolver;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;
import org.expasy.mzjava.proteomics.ms.spectrum.PepLibPeakAnnotation;
import org.expasy.mzjava.utils.URIBuilder;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.Reader;
import java.io.StringReader;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class SptxtReaderTest2 {

    @Test
    public void testParsePrecursorMZLine() throws Exception {

        LibrarySpectrumBuilder builder = mock(LibrarySpectrumBuilder.class);

        SptxtReader reader = SptxtReader.newBuilder(mock(Reader.class), new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE)
                .useSpectraStAnnotationResolver()
                .build();

        boolean handled = reader.parsePrecursorMZLine("587.64", builder);
        Assert.assertEquals(true, handled);
        verify(builder).setPrecursorMz(587.64);
        verifyNoMoreInteractions(builder);
    }

    @Test
    public void testHandlePrecursorMZTag() throws Exception {

        LibrarySpectrumBuilder builder = mock(LibrarySpectrumBuilder.class);
        SptxtReader reader = SptxtReader.newBuilder(mock(Reader.class), new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE)
                .useSpectraStAnnotationResolver()
                .build();

        boolean handled = reader.handleTagLine("PrecursorMZ", "587.64", builder);
        assertEquals(true, handled);
        verify(builder).setPrecursorMz(587.64);
        verifyNoMoreInteractions(builder);
    }

    @Test
    public void testParseStatusLine() throws Exception {

        LibrarySpectrumBuilder builder = mock(LibrarySpectrumBuilder.class);

        SptxtReader reader = SptxtReader.newBuilder(mock(Reader.class), new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE)
                .useSpectraStAnnotationResolver()
                .build();

        reader.parseStatusLine("Normal", builder);

        verify(builder).setStatus(PeptideConsensusSpectrum.Status.NORMAL);
        verifyNoMoreInteractions(builder);
    }

    @Test
    public void testHandleStatusTag() throws Exception {

        LibrarySpectrumBuilder builder = mock(LibrarySpectrumBuilder.class);

        SptxtReader reader = SptxtReader.newBuilder(mock(Reader.class), new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE)
                .useSpectraStAnnotationResolver()
                .build();

        reader.handleTagLine("Status", "Normal", builder);

        verify(builder).setStatus(PeptideConsensusSpectrum.Status.NORMAL);
        verifyNoMoreInteractions(builder);
    }

    @Test
    public void testParseNumPeaksLine() throws Exception {

        LibrarySpectrumBuilder builder = mock(LibrarySpectrumBuilder.class);

        SptxtReader reader = SptxtReader.newBuilder(mock(Reader.class), new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE)
                .useSpectraStAnnotationResolver()
                .build();

        reader.parseNumPeaksLine("137", builder);

        verify(builder, times(1)).ensureCapacity(137);
        verifyNoMoreInteractions(builder);
    }

    @Test
    public void testHandelNumPeaksLine() throws Exception {

        LibrarySpectrumBuilder builder = mock(LibrarySpectrumBuilder.class);

        SptxtReader reader = SptxtReader.newBuilder(mock(Reader.class), new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE)
                .useSpectraStAnnotationResolver()
                .build();

        reader.handleTagLine("NumPeaks", "137", builder);

        verify(builder, times(1)).ensureCapacity(137);
        verifyNoMoreInteractions(builder);
    }

    @Test
    public void testParseCommentLine() throws Exception {

        LibrarySpectrumBuilder builder = mock(LibrarySpectrumBuilder.class);

        SptxtReader reader = SptxtReader.newBuilder(mock(Reader.class), new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE)
                .useSpectraStAnnotationResolver()
                .build();

        reader.parseCommentLine("BinaryFileOffset=636655938 DUScorr=2.3/0.44/0.94 Dot_cons=0.937/0.043 Dotbest=0.92 Dotfull=0.882/0.038 Dottheory=0.87 Flags=0,0,0 Fullname=K.QWVNTVAGEK.L/2 Inst=it Max2med_orig=243.4/90.9 Missing=0.0687/0.0655 Mods=1/0,Q,Acetyl Mz_av=587.653 Mz_diff=0.582 Mz_exact=587.2991 Naa=10 Nreps=21/27 Organism=mouse Parent=587.299 Parent_med=587.8010/0.08 Pep=Tryptic Pfin=1.1e+005 Pfract=0 Prob=0.9978 Probcorr=1 Protein=\"Q3UBS3\" Pseq=90 Sample=4/bi_m2_control_cam,15,16/bi_m2_kras_p53_cam,4,5/hms_colon_apcd580_8585847_7_krastins_ltqft_prop,1,1/hms_colon_apcmin_122804_nanospray_3cutscx_cam,1,1 Se=1^O21:ex=0.0408/0.1907,td=260/1206,pr=2.91e-006/1.514e-005,bs=0.00899,b2=0.00993,bd=5560 Spec=Consensus Tfratio=4.6e+002 Unassign_all=0.148 Unassigned=0.128",
                builder);

        verify(builder).addMod(0, "Acetyl");
        verify(builder).addProteinAccessionNumber("Q3UBS3");
    }

    @Test
    public void testParseNameLine() throws Exception {

        LibrarySpectrumBuilder builder = mock(LibrarySpectrumBuilder.class);

        SptxtReader reader = SptxtReader.newBuilder(mock(Reader.class), new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE)
                .useSpectraStAnnotationResolver()
                .build();

        boolean handled = reader.parseNameLine("n[43]QWVNTVAGEK/2", builder);
        assertEquals(true, handled);

        verify(builder).setPeptideSequence("QWVNTVAGEK");
        verify(builder).setPrecursorCharge(2);
    }

    @Test
    public void testHandlePeakLine() throws Exception {

        LibrarySpectrumBuilder builder = mock(LibrarySpectrumBuilder.class);

        SptxtReader reader = SptxtReader.newBuilder(mock(Reader.class), new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE)
                .useSpectraStAnnotationResolver()
                .build();

        boolean handled = reader.handlePeakLine(builder, "351.3000\t706.0\ty3/0.10,y6^2/-0.40\t4/5 3.1");
        assertEquals(true, handled);

        verify(builder).addPeak(351.3000, 706.0, "y3/0.10,y6^2/-0.40\t4/5 3.1");
    }

    @Test(expected = IllegalStateException.class)
    public void testHandleInvalidPeakLine() throws Exception {

        LibrarySpectrumBuilder builder = mock(LibrarySpectrumBuilder.class);

        SptxtReader reader = SptxtReader.newBuilder(mock(Reader.class), new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE)
                .useSpectraStAnnotationResolver()
                .build();

        reader.handlePeakLine(builder, "peaks");
    }

    @Test
    public void testParse() throws Exception {

        StringReader stringReader = new StringReader(
                        "Name: n[43]QWVNTVAGEK/2\n" +
                        "LibID: 112594\n" +
                        "MW: 1175.28\n" +
                        "PrecursorMZ: 587.64\n" +
                        "Status: Normal\n" +
                        "FullName: K.n[43]QWVNTVAGEK.L/2\n" +
                        "Comment: BinaryFileOffset=636655938 DUScorr=2.3/0.44/0.94 Dot_cons=0.937/0.043 Dotbest=0.92 Dotfull=0.882/0.038 Dottheory=0.87 Flags=0,0,0 Fullname=K.QWVNTVAGEK.L/2 Inst=it Max2med_orig=243.4/90.9 Missing=0.0687/0.0655 Mods=1/0,Q,Acetyl Mz_av=587.653 Mz_diff=0.582 Mz_exact=587.2991 Naa=10 Nreps=21/27 Organism=mouse Parent=587.299 Parent_med=587.8010/0.08 Pep=Tryptic Pfin=1.1e+005 Pfract=0 Prob=0.9978 Probcorr=1 Protein=\"tr|Q3UBS3|Q3UBS3_MOUSE Bone marrow macrophage cDNA, RIKEN full-length enriched library, clone:I830015M19 product:haptoglobin, full insert sequence (Osteoclast-like cell cDNA, RIKEN full-length enriched library, clone:I420027O20 product:haptoglobin, full insert sequence) (Bone marrow macrophage cDNA, RIKEN full-length enriched library, clone:I830082M22 product:haptoglobin, full insert sequence) (Bone marrow macrophage cDNA, RIKEN full-length enriched library, clone:I830058N19 product:haptoglobin, full insert sequence) (Bone marrow macrophage cDNA, RIKEN full-length enriched library, clone:I830007G13 product:haptoglobin, full insert sequence) [Mus musculus]\" Pseq=90 Sample=4/bi_m2_control_cam,15,16/bi_m2_kras_p53_cam,4,5/hms_colon_apcd580_8585847_7_krastins_ltqft_prop,1,1/hms_colon_apcmin_122804_nanospray_3cutscx_cam,1,1 Se=1^O21:ex=0.0408/0.1907,td=260/1206,pr=2.91e-006/1.514e-005,bs=0.00899,b2=0.00993,bd=5560 Spec=Consensus Tfratio=4.6e+002 Unassign_all=0.148 Unassigned=0.128\n" +
                        "NumPeaks: 1\n" +
                        "172.1000\t824.0\tIWG/1.00\t21/21 1.7\n" +
                        "\n" +
                        "Name: n[43]AATFGLILDDVSLTHLTFGK/2\n" +
                        "LibID: 1354\n" +
                        "MW: 2163.47\n" +
                        "PrecursorMZ: 1081.74\n" +
                        "Status: Normal\n" +
                        "FullName: R.n[43]AATFGLILDDVSLTHLTFGK.E/2\n" +
                        "Comment: BinaryFileOffset=7546411 DUScorr=10/0.71/2.9 Dot_cons=0.788/0.027 Dotbest=0.78 Dotfull=0.626/0.026 Dottheory=0.92 Flags=0,0,0 Fullname=R.AATFGLILDDVSLTHLTFGK.E/2 Inst=it Max2med_orig=91.0/33.4 Missing=0.2757/0.0460 Mods=1/0,A,Acetyl Mz_av=1081.749 Mz_diff=0.498 Mz_exact=1081.0812 Naa=20 Nreps=5/48 Organism=mouse Parent=1081.081 Parent_med=1081.0970/0.49 Pep=Tryptic Pfin=1.3e+011 Pfract=0 Prob=1.0000 Probcorr=1 Protein=\"sp|P67778|PHB_MOUSE Prohibitin (B-cell receptor-associated protein 32) (BAP 32) [Mus musculus]\" Pseq=19 Sample=5/bi_mitochondrial_adipose_gelcms_cam,1,1/bi_mitochondrial_placenta_gelcms_cam,1,1/bi_mitochondrial_testis_gelcms_cam,1,1/ut_mudpitvsoffgel_c2c12_cam,0,5/ut_placenta_mm_labyrinth_cam,2,23 Se=1^O5:ex=1.2e-005/7.953e-006,td=4.17e+006/1.565e+009,pr=2.43e-011/1.637e-011,bs=8.24e-009,b2=2.85e-008,bd=6.07e+009 Spec=Consensus Tfratio=6.1e+006 Unassign_all=0.181 Unassigned=0.097\n" +
                        "NumPeaks: 1\n" +
                        "351.3000\t706.0\ty3/0.10,y6^2/-0.40\t4/5 3.1\n"
        );

        SptxtReader reader = SptxtReader.newBuilder(stringReader, new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE)
                .useSpectraStAnnotationResolver()
                .build();

        assertEquals(true, reader.hasNext());
        PeptideConsensusSpectrum spectrum = reader.next();
        assertNotNull(spectrum);
        assertEquals("Q(Acetyl)WVNTVAGEK", spectrum.getPeptide().toString());
        assertEquals(587.64, spectrum.getPrecursor().getMz(), 0.000001);
        assertEquals(2, spectrum.getPrecursor().getCharge());
        assertEquals(1, spectrum.size());
        assertEquals(172.1000, spectrum.getMz(0), 0.00001);
        assertEquals(824.0, spectrum.getIntensity(0), 0.00001);

        assertEquals(true, reader.hasNext());
        spectrum = reader.next();
        assertNotNull(spectrum);
        assertEquals("A(Acetyl)ATFGLILDDVSLTHLTFGK", spectrum.getPeptide().toString());
        assertEquals(1081.74, spectrum.getPrecursor().getMz(), 0.000001);
        assertEquals(2, spectrum.getPrecursor().getCharge());
        assertEquals(1, spectrum.size());
        assertEquals(351.3000, spectrum.getMz(0), 0.00001);
        assertEquals(706.0, spectrum.getIntensity(0), 0.00001);
    }

    @Test
    public void testBuilder() throws Exception {

        StringReader stringReader = new StringReader("Name: IYQY[243]IQSR/3\n" +
                "LibID: 0\n" +
                "MW: 1152.5438\n" +
                "PrecursorMZ: 384.1813\n" +
                "Status: Normal\n" +
                "FullName: R.IYQY[243]IQSR.F/3\n" +
                "Comment: AvePrecursorMz=384.4051 BinaryFileOffset=378 ConsFracAssignedPeaks=0.708 DeltaCn=0.1070 DotConsensus=0.85,0.02;0/2 FracUnassigned=0.00,0/5;0.02,2/20;0.06,19/65 Inst=0 MaxRepSN=47.9 Mods=1/3,Y,MyMod NAA=8 NMC=0 NTT=2 Nreps=2/2 OrigMaxIntensity=4.1e+03 Parent=384.181 Pep=Tryptic Prob=0.9332 ProbRange=0.989579,0.9332,0.8886,0.844 Protein=8/IPI00182717/IPI00000352/IPI00014344/IPI00215873/IPI00219250/IPI00219251/IPI00219252/IPI00332215 RepFracAssignedPeaks=0.492 RepNumPeaks=216.5/56.5 RetentionTime=0.0,0.0,0.0 SN=200.0 Sample=1/TiO2,2,2 Se=1^S2:dc=0.1320/0.0250,fv=2.0476/0.1727,pb=0.8886/0.0446,xc=2.4030/0.0450 Spec=Consensus XCorr=2.4480\n" +
                "NumPeaks: 3\n" +
                "70.1\t88\tone\n" +
                "112.1\t45\ttwo\n" +
                "114.1\t47\tthree\n" +
                "");


        ModificationResolver modResolver = mock(ModificationResolver.class);
        Modification modification = Modification.parseModification("O");
        when(modResolver.resolve("MyMod")).thenReturn(Optional.of(modification));

        PepLibPeakAnnotation peakAnnotation1 = new PepLibPeakAnnotation(1, 0.1, 0.01);
        PepLibPeakAnnotation peakAnnotation2 = new PepLibPeakAnnotation(2, 0.2, 0.02);
        PepLibPeakAnnotation peakAnnotation3 = new PepLibPeakAnnotation(3, 0.3, 0.03);

        AnnotationResolver annotationResolver = mock(AnnotationResolver.class);
        when(annotationResolver.resolveAnnotations(eq("one"), any(Peptide.class))).thenReturn(Collections.singletonList(peakAnnotation1));
        when(annotationResolver.resolveAnnotations(eq("two"), any(Peptide.class))).thenReturn(Collections.singletonList(peakAnnotation2));
        when(annotationResolver.resolveAnnotations(eq("three"), any(Peptide.class))).thenReturn(Collections.singletonList(peakAnnotation3));

        PeakProcessorChain<PepLibPeakAnnotation> processorChain = new PeakProcessorChain<PepLibPeakAnnotation>()
                .add(new SqrtTransformer<PepLibPeakAnnotation>());

        SpectraLibCommentParser commentParser = Mockito.spy(new SpectraStCommentParser());

        SptxtReader reader = SptxtReader.newBuilder(stringReader, URIBuilder.UNDEFINED_URI, PeakList.Precision.FLOAT)
                .useModificationResolver(modResolver)
                .usePeakProcessorChain(processorChain)
                .useAnnotationResolver(annotationResolver)
                .useSpectraLibCommentParser(commentParser)
                .build();

        Assert.assertEquals(true, reader.hasNext());
        PeptideConsensusSpectrum spectrum = reader.next();

        Assert.assertEquals(PeakList.Precision.FLOAT, spectrum.getPrecision());
        Assert.assertEquals(Peptide.parse("IYQY(O)IQSR"), spectrum.getPeptide());

        Assert.assertEquals(Collections.singletonList(peakAnnotation1), spectrum.getAnnotations(0));
        Assert.assertEquals(Collections.singletonList(peakAnnotation2), spectrum.getAnnotations(1));
        Assert.assertEquals(Collections.singletonList(peakAnnotation3), spectrum.getAnnotations(2));

        double mzDelta = 0.00001;
        Assert.assertEquals(Math.sqrt(88), spectrum.getIntensity(0), mzDelta);
        Assert.assertEquals(Math.sqrt(45), spectrum.getIntensity(1), mzDelta);
        Assert.assertEquals(Math.sqrt(47), spectrum.getIntensity(2), mzDelta);

        verify(commentParser).parseComment(anyString(), any(LibrarySpectrumBuilder.class));
    }
}
