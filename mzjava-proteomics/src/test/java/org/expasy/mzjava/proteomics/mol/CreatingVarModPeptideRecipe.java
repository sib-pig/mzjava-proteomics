package org.expasy.mzjava.proteomics.mol;

import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.proteomics.mol.ModifiedPeptideFactory;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;

// # $RECIPE$ $NUMBER$ - Generating set of Peptides with variable modifications #
//
// ## $PROBLEM$ ##
// You want to generate all combinations of modified *Peptide*s given potential sites.
//
// ## $SOLUTION$ ##
// Variable modifications are those which may or may not be present in a *Peptide*.
// *ModifiedPeptideFactory* is able to generate all possible combinations of variable
// modifications given a *Peptide* source and a set of target AA -> potential modification.
public class CreatingVarModPeptideRecipe {

// Generating all combinations of phosphorylated peptides
    @Test
    public void newFactory() {

        // <SNIP>
        // the factory is configured and created from its internal builder
        ModifiedPeptideFactory factory = new ModifiedPeptideFactory.Builder()
                .addTarget(EnumSet.of(AminoAcid.Y, AminoAcid.T, AminoAcid.S), Modification.parseModification("PO3H-1"))
                .build();

        // the factory generate all the combinations of modified peptides from a source Peptide
        List<Peptide> peptides = factory.createVarModPeptides(Peptide.parse("PEPTYDE"));

        Assert.assertEquals(Arrays.asList(Peptide.parse("PEPTY(PO3H-1)DE"),
                Peptide.parse("PEPT(PO3H-1)YDE"), Peptide.parse("PEPT(PO3H-1)Y(PO3H-1)DE")), peptides);
        // </SNIP>
    }

// Factory can be configured to fine tune the targeting amino-acids
    @Test
    public void newFactory2() {

        // <SNIP>
        // here it phosphorylates the alpha-amino group of the first residue Y, T and S of the peptide
        // and methylates the carboxy group of any last residue.
        ModifiedPeptideFactory factory = new ModifiedPeptideFactory.Builder()
                .addTarget(EnumSet.of(AminoAcid.T, AminoAcid.Y, AminoAcid.S), ModAttachment.N_TERM, Modification.parseModification("CH2CO"))
                .addTarget(ModAttachment.C_TERM, Modification.parseModification("CH3"))
                .build();

        List<Peptide> peptides = factory.createVarModPeptides(Peptide.parse("SEPTTYDE"));

        Assert.assertEquals(Arrays.asList(Peptide.parse("SEPTTYDE_(CH3)"),
                Peptide.parse("(CH2CO)_SEPTTYDE"), Peptide.parse("(CH2CO)_SEPTTYDE_(CH3)")), peptides);
        // </SNIP>
    }

// Factory can be configured to limit the maximum number of variable sites
    @Test
    public void newFactory3() {

        // <SNIP>
        // the factory is configured and created from its internal builder
        ModifiedPeptideFactory factory = new ModifiedPeptideFactory.Builder()
                .addTarget(EnumSet.of(AminoAcid.Y, AminoAcid.T, AminoAcid.S), Modification.parseModification("PO3H-1"))
                .limit(3)
                .build();

        // the factory generate all the combinations of modified peptides from a source Peptide
        List<Peptide> peptides = factory.createVarModPeptides(Peptide.parse("PEPTTYDE"));

        Assert.assertEquals(Arrays.asList(Peptide.parse("PEPTTY(PO3H-1)DE"),
                Peptide.parse("PEPTT(PO3H-1)YDE"), Peptide.parse("PEPTT(PO3H-1)Y(PO3H-1)DE"),
                Peptide.parse("PEPT(PO3H-1)TYDE"), Peptide.parse("PEPT(PO3H-1)TY(PO3H-1)DE"),
                Peptide.parse("PEPT(PO3H-1)T(PO3H-1)YDE"), Peptide.parse("PEPT(PO3H-1)T(PO3H-1)Y(PO3H-1)DE")), peptides);
        // </SNIP>
    }

// ## $DISCUSSION$ ##

// ## $RELATED$ ##
}