package org.expasy.mzjava.proteomics.mol.digest;

import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;

/**
 * @author fnikitin
 * Date: 10/4/12
 */
public class CleavageSiteMatcherTest {

    @Test
    public void testPattern() throws ParseException {

        CleavageSiteMatcher cs = new CleavageSiteMatcher("M|X");

        Assert.assertEquals("(?<=M)(?=[A-Z])", cs.getRegex().pattern());
    }

    @Test (expected = CleavageSiteParseException.class)
    public void testMissingCleavageToken() throws CleavageSiteParseException {

        new CleavageSiteMatcher("[KR][^P]");
    }

    @Test (expected = CleavageSiteParseException.class)
    public void testTooManyCleavageTokens() throws CleavageSiteParseException {

        new CleavageSiteMatcher("[KR]|[^P]|K");
    }
}