/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.ms.fragment;

import com.google.common.collect.Lists;
import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.core.mol.Mass;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.mol.PeptideBuilder;
import org.expasy.mzjava.proteomics.mol.PeptideFragment;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.proteomics.ms.spectrum.PeptideSpectrum;
import org.expasy.mzjava.proteomics.ms.spectrum.PepFragAnnotation;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class PeptideFragmenterTest {

    @Test
    public void testGenerateForward() throws Exception {

        Peptide peptide = Peptide.parse("CERVILAS");

        PeptideFragmenter peptideFragmenter = new PeptideFragmenter(EnumSet.noneOf(IonType.class), PeakList.Precision.DOUBLE);

        List<PeptideFragment> frags = new ArrayList<PeptideFragment>();
        peptideFragmenter.generateForward(peptide, frags);

        Assert.assertEquals(7, frags.size());
        Assert.assertEquals("C", frags.get(0).toSymbolString());
        Assert.assertEquals("CE", frags.get(1).toSymbolString());
        Assert.assertEquals("CER", frags.get(2).toSymbolString());
        Assert.assertEquals("CERV", frags.get(3).toSymbolString());
        Assert.assertEquals("CERVI", frags.get(4).toSymbolString());
        Assert.assertEquals("CERVIL", frags.get(5).toSymbolString());
        Assert.assertEquals("CERVILA", frags.get(6).toSymbolString());
    }

    @Test
    public void testGenerateReverse() throws Exception {

        Peptide peptide = Peptide.parse("CERVILAS");

        PeptideFragmenter peptideFragmenter = new PeptideFragmenter(EnumSet.noneOf(IonType.class), PeakList.Precision.DOUBLE);

        List<PeptideFragment> frags = new ArrayList<PeptideFragment>();
        peptideFragmenter.generateReverse(peptide, frags);

        Assert.assertEquals(7, frags.size());
        Assert.assertEquals("S", frags.get(0).toSymbolString());
        Assert.assertEquals("AS", frags.get(1).toSymbolString());
        Assert.assertEquals("LAS", frags.get(2).toSymbolString());
        Assert.assertEquals("ILAS", frags.get(3).toSymbolString());
        Assert.assertEquals("VILAS", frags.get(4).toSymbolString());
        Assert.assertEquals("RVILAS", frags.get(5).toSymbolString());
        Assert.assertEquals("ERVILAS", frags.get(6).toSymbolString());
    }

    @Test
    public void testGGYDSPR() throws Exception {

        PeptideFragmenter peptideFragmenter = new PeptideFragmenter(EnumSet.of(IonType.b, IonType.y), PeakList.Precision.DOUBLE);

        Peptide peptide = Peptide.parse("GGYDSPR");

        PeptideSpectrum peakList = peptideFragmenter.fragment(peptide, 2);

        Assert.assertEquals(23, peakList.size());
        Assert.assertEquals(2, peakList.getPrecursor().getCharge());
        Assert.assertEquals(1, peakList.getPrecursor().getIntensity(), 0.0000001);
        Assert.assertEquals(376.17210960069997, peakList.getPrecursor().getMz(), 0.0000001);

        double delta = 0.000000001;

        int i = 0;

        Assert.assertEquals("b1^2", 29.5180079692, peakList.getMz(i++), delta);
        Assert.assertEquals("b1, b2^2", 58.0287398307, peakList.getMz(i++), delta);
        Assert.assertEquals("y1^2", 88.0631139637, peakList.getMz(i++), delta);
        Assert.assertEquals("b2", 115.05020355369999, peakList.getMz(i++), delta);
        Assert.assertEquals("y2^2", 136.5894958892, peakList.getMz(i++), delta);
        Assert.assertEquals("b3^2", 139.56040409919999, peakList.getMz(i++), delta);
        Assert.assertEquals("y1", 175.11895181970002, peakList.getMz(i++), delta);
        Assert.assertEquals("y3^2", 180.1055100937, peakList.getMz(i++), delta);
        Assert.assertEquals("b4^2", 197.07387561469997, peakList.getMz(i++), delta);
        Assert.assertEquals("y4^2", 237.61898160919998, peakList.getMz(i++), delta);
        Assert.assertEquals("b5^2", 240.58988981919995, peakList.getMz(i++), delta);
        Assert.assertEquals("y2", 272.17171567070005, peakList.getMz(i++), delta);
        Assert.assertEquals("b3", 278.1135320907, peakList.getMz(i++), delta);
        Assert.assertEquals("b6^2", 289.1162717447, peakList.getMz(i++), delta);
        Assert.assertEquals("y5^2", 319.1506458777, peakList.getMz(i++), delta);
        Assert.assertEquals("y6^2", 347.6613777392, peakList.getMz(i++), delta);
        Assert.assertEquals("y3", 359.2037440797, peakList.getMz(i++), delta);
        Assert.assertEquals("b4", 393.1404751217, peakList.getMz(i++), delta);
        Assert.assertEquals("y4", 474.2306871107, peakList.getMz(i++), delta);
        Assert.assertEquals("b5", 480.17250353069994, peakList.getMz(i++), delta);
        Assert.assertEquals("b6", 577.2252673817, peakList.getMz(i++), delta);
        Assert.assertEquals("y5", 637.2940156477, peakList.getMz(i++), delta);
        Assert.assertEquals("y6", 694.3154793707, peakList.getMz(i), delta);
    }

    /**
     * Comparing to http://db.systemsbiology.net:8080/proteomicsToolkit/FragIonServlet.html
     *
     * @throws Exception because it is a test
     */
    @Test
    public void testGGYDSPR2() throws Exception {

        PeptideFragmenter peptideFragmenter = new PeptideFragmenter(EnumSet.of(IonType.b, IonType.y), PeakList.Precision.DOUBLE);

        Peptide peptide = Peptide.parse("GGYDSPR");

        PeptideSpectrum peakList = peptideFragmenter.fragment(peptide, 2);

        Assert.assertEquals(23, peakList.size());

        double delta = 0.001;
        int i = 0;

        Assert.assertEquals("b1^2", 29.5186, peakList.getMz(i++), delta);
        Assert.assertEquals("b1, b2^2", 58.02933, peakList.getMz(i++), delta);
        Assert.assertEquals("y1^2", 88.06371, peakList.getMz(i++), delta);
        Assert.assertEquals("b2", 115.0508, peakList.getMz(i++), delta);
        Assert.assertEquals("y2^2", 136.59009, peakList.getMz(i++), delta);
        Assert.assertEquals("b3^2", 139.561, peakList.getMz(i++), delta);
        Assert.assertEquals("y1", 175.11955, peakList.getMz(i++), delta);
        Assert.assertEquals("y3^2", 180.1061, peakList.getMz(i++), delta);
        Assert.assertEquals("b4^2", 197.07447, peakList.getMz(i++), delta);
        Assert.assertEquals("y4^2", 237.61957, peakList.getMz(i++), delta);
        Assert.assertEquals("b5^2", 240.59048, peakList.getMz(i++), delta);
        Assert.assertEquals("y2", 272.17231, peakList.getMz(i++), delta);
        Assert.assertEquals("b3", 278.11413, peakList.getMz(i++), delta);
        Assert.assertEquals("b6^2", 289.11687, peakList.getMz(i++), delta);
        Assert.assertEquals("y5^2", 319.15124, peakList.getMz(i++), delta);
        Assert.assertEquals("y6^2", 347.66197, peakList.getMz(i++), delta);
        Assert.assertEquals("y3", 359.20434, peakList.getMz(i++), delta);
        Assert.assertEquals("b4", 393.14107, peakList.getMz(i++), delta);
        Assert.assertEquals("y4", 474.23128, peakList.getMz(i++), delta);
        Assert.assertEquals("b5", 480.1731, peakList.getMz(i++), delta);
        Assert.assertEquals("b6", 577.22586, peakList.getMz(i++), delta);
        Assert.assertEquals("y5", 637.29461, peakList.getMz(i++), delta);
        Assert.assertEquals("y6", 694.31607, peakList.getMz(i), delta);
    }

    @Test
    public void testVSQQQI() throws Exception {

        PeptideFragmenter peptideFragmenter = new PeptideFragmenter(EnumSet.of(IonType.b, IonType.y), PeakList.Precision.DOUBLE);

        Peptide peptide = Peptide.parse("VSQQQI");

        PeptideSpectrum peakList = peptideFragmenter.fragment(peptide, 2);

        Assert.assertEquals(20, peakList.size());

        Assert.assertEquals("b1^2", 50.541483065200005, peakList.getMz(0), 0.000000001);
        Assert.assertEquals("y1^2", 66.5545904402, peakList.getMz(1), 0.000000001);
        Assert.assertEquals("b2^2", 94.0574972697, peakList.getMz(2), 0.000000001);
        Assert.assertEquals("b1", 100.0756900227, peakList.getMz(3), 0.000000001);
        Assert.assertEquals("y2^2", 130.58387919519998, peakList.getMz(4), 0.000000001);
        Assert.assertEquals("y1", 132.1019047727, peakList.getMz(5), 0.000000001);
        Assert.assertEquals("b3^2", 158.0867860247, peakList.getMz(6), 0.000000001);
        Assert.assertEquals("b2", 187.1077184317, peakList.getMz(7), 0.000000001);
        Assert.assertEquals("y3^2", 194.61316795019997, peakList.getMz(8), 0.000000001);
        Assert.assertEquals("b4^2", 222.11607477969997, peakList.getMz(9), 0.000000001);
        Assert.assertEquals("y4^2", 258.6424567052, peakList.getMz(10), 0.000000001);
        Assert.assertEquals("y2", 260.1604822827, peakList.getMz(11), 0.000000001);
        Assert.assertEquals("b5^2", 286.1453635347, peakList.getMz(12), 0.000000001);
        Assert.assertEquals("y5^2", 302.15847090970004, peakList.getMz(13), 0.000000001);
        Assert.assertEquals("b3", 315.1662959417, peakList.getMz(14), 0.000000001);
        Assert.assertEquals("y3", 388.21905979269997, peakList.getMz(15), 0.000000001);
        Assert.assertEquals("b4", 443.2248734517, peakList.getMz(16), 0.000000001);
        Assert.assertEquals("y4", 516.2776373027, peakList.getMz(17), 0.000000001);
        Assert.assertEquals("b5", 571.2834509616999, peakList.getMz(18), 0.000000001);
        Assert.assertEquals("y5", 603.3096657117001, peakList.getMz(19), 0.000000001);
    }

    @Test
    public void compareToJPLv1() throws Exception {

        PeptideFragmenter peptideFragmenter = new PeptideFragmenter(EnumSet.of(IonType.b, IonType.y), PeakList.Precision.DOUBLE);

        Peptide peptide = Peptide.parse("CERVILAS");

        PeptideSpectrum peakList = peptideFragmenter.fragment(peptide, 2);

        Assert.assertEquals(28, peakList.size());

        Assert.assertEquals("b1^2", peakList.getAnnotations(0).get(0).toSptxtString());
        Assert.assertEquals("y1^2", peakList.getAnnotations(1).get(0).toSptxtString());
        Assert.assertEquals("y2^2", peakList.getAnnotations(2).get(0).toSptxtString());
        Assert.assertEquals("b1", peakList.getAnnotations(3).get(0).toSptxtString());
        Assert.assertEquals("y1", peakList.getAnnotations(4).get(0).toSptxtString());
        Assert.assertEquals("b2^2", peakList.getAnnotations(5).get(0).toSptxtString());
        Assert.assertEquals("y3^2", peakList.getAnnotations(6).get(0).toSptxtString());
        Assert.assertEquals("y2", peakList.getAnnotations(7).get(0).toSptxtString());
        Assert.assertEquals("b3^2", peakList.getAnnotations(8).get(0).toSptxtString());
        Assert.assertEquals("y4^2", peakList.getAnnotations(9).get(0).toSptxtString());
        Assert.assertEquals("b2", peakList.getAnnotations(10).get(0).toSptxtString());
        Assert.assertEquals("b4^2", peakList.getAnnotations(11).get(0).toSptxtString());
        Assert.assertEquals("y5^2", peakList.getAnnotations(12).get(0).toSptxtString());
        Assert.assertEquals("y3", peakList.getAnnotations(13).get(0).toSptxtString());
        Assert.assertEquals("b5^2", peakList.getAnnotations(14).get(0).toSptxtString());
        Assert.assertEquals("y6^2", peakList.getAnnotations(15).get(0).toSptxtString());
        Assert.assertEquals("b6^2", peakList.getAnnotations(16).get(0).toSptxtString());
        Assert.assertEquals("b3", peakList.getAnnotations(17).get(0).toSptxtString());
        Assert.assertEquals("b7^2", peakList.getAnnotations(18).get(0).toSptxtString());
        Assert.assertEquals("y7^2", peakList.getAnnotations(19).get(0).toSptxtString());
        Assert.assertEquals("y4", peakList.getAnnotations(20).get(0).toSptxtString());
        Assert.assertEquals("b4", peakList.getAnnotations(21).get(0).toSptxtString());
        Assert.assertEquals("y5", peakList.getAnnotations(22).get(0).toSptxtString());
        Assert.assertEquals("b5", peakList.getAnnotations(23).get(0).toSptxtString());
        Assert.assertEquals("y6", peakList.getAnnotations(24).get(0).toSptxtString());
        Assert.assertEquals("b6", peakList.getAnnotations(25).get(0).toSptxtString());
        Assert.assertEquals("b7", peakList.getAnnotations(26).get(0).toSptxtString());
        Assert.assertEquals("y7", peakList.getAnnotations(27).get(0).toSptxtString());

        double delta = 0.0001;
        Assert.assertEquals("b1^2", 52.5118683512, peakList.getMz(0), delta);
        Assert.assertEquals("y1^2", 53.52857265519998, peakList.getMz(1), delta);
        Assert.assertEquals("y2^2", 89.04712954869996, peakList.getMz(2), delta);
        Assert.assertEquals("b1", 104.01646059469999, peakList.getMz(3), delta);
        Assert.assertEquals("y1", 106.04986920269995, peakList.getMz(4), delta);
        Assert.assertEquals("b2^2", 117.0331648987, peakList.getMz(5), delta);
        Assert.assertEquals("y3^2", 145.58916153819996, peakList.getMz(6), delta);
        Assert.assertEquals("y2", 177.08698298969992, peakList.getMz(7), delta);
        Assert.assertEquals("b3^2", 195.0837204117, peakList.getMz(8), delta);
        Assert.assertEquals("y4^2", 202.13119352769993, peakList.getMz(9), delta);
        Assert.assertEquals("b2", 233.0590536897, peakList.getMz(10), delta);
        Assert.assertEquals("b4^2", 244.6179273692, peakList.getMz(11), delta);
        Assert.assertEquals("y5^2", 251.66540048519994, peakList.getMz(12), delta);
        Assert.assertEquals("y3", 290.17104696869995, peakList.getMz(13), delta);
        Assert.assertEquals("b5^2", 301.1599593587, peakList.getMz(14), delta);
        Assert.assertEquals("y6^2", 329.7159559982, peakList.getMz(15), delta);
        Assert.assertEquals("b6^2", 357.7019913482, peakList.getMz(16), delta);
        Assert.assertEquals("b3", 389.16016471570003, peakList.getMz(17), delta);
        Assert.assertEquals("b7^2", 393.2205482417, peakList.getMz(18), delta);
        Assert.assertEquals("y7^2", 394.2372525457, peakList.getMz(19), delta);
        Assert.assertEquals("y4", 403.2551109476999, peakList.getMz(20), delta);
        Assert.assertEquals("b4", 488.22857863070004, peakList.getMz(21), delta);
        Assert.assertEquals("y5", 502.3235248626999, peakList.getMz(22), delta);
        Assert.assertEquals("b5", 601.3126426097, peakList.getMz(23), delta);
        Assert.assertEquals("y6", 658.4246358887, peakList.getMz(24), delta);
        Assert.assertEquals("b6", 714.3967065887, peakList.getMz(25), delta);
        Assert.assertEquals("b7", 785.4338203757, peakList.getMz(26), delta);
        Assert.assertEquals("y7", 787.4672289837, peakList.getMz(27), delta);
    }

    @Test
    public void testCER() throws Exception {

        Peptide peptide = Peptide.parse("CER");

        PeptideFragmenter gen = new PeptideFragmenter(EnumSet.of(IonType.a), PeakList.Precision.DOUBLE);
        PeakList<PepFragAnnotation> aIons = gen.fragment(peptide, 1);

        gen = new PeptideFragmenter(EnumSet.of(IonType.b), PeakList.Precision.DOUBLE);
        PeakList<PepFragAnnotation> bIons = gen.fragment(peptide, 1);

        gen = new PeptideFragmenter(EnumSet.of(IonType.y), PeakList.Precision.DOUBLE);
        PeakList<PepFragAnnotation> yIons = gen.fragment(peptide, 1);

        /*
             Seq    #       A            B            Y         # (+1)

              C     1     76.02214    104.01706    407.17132    3
              E     2    205.06473    233.05965    304.16214    2
              R     3    361.16584    389.16076    175.11955    1

            Generated using http://db.systemsbiology.net:8080/proteomicsToolkit/FragIonServlet.html
         */

        Assert.assertEquals(2, yIons.size());
        double delta = 0.001; //FragIonServlet does not take the mass of the electron into account
        Assert.assertEquals(175.1195, yIons.getMz(0), delta);
        Assert.assertEquals(304.1621, yIons.getMz(1), delta);

        Assert.assertEquals(2, bIons.size());
        Assert.assertEquals(104.01706, bIons.getMz(0), delta);
        Assert.assertEquals(233.05965, bIons.getMz(1), delta);

        Assert.assertEquals(2, aIons.size());
        Assert.assertEquals(76.02214, aIons.getMz(0), delta);
        Assert.assertEquals(205.06473, aIons.getMz(1), delta);
    }

    /**
     * Data generated by:
     * http://db.systemsbiology.net:8080/proteomicsToolkit/FragIonServlet.html
     *
     * @throws Exception because it is a test
     */
    @Test
    public void testBSeries() throws Exception {

        PeptideFragmenter fragmenter = new PeptideFragmenter(EnumSet.of(IonType.b), PeakList.Precision.DOUBLE);

        Peptide peptide = Peptide.parse("CERVILAS");
        List<PeptideFragment> fragments = new ArrayList<PeptideFragment>();
        fragmenter.generateForward(peptide, fragments);

        PeakList<PepFragAnnotation> peakList = fragmenter.fragment(peptide, 2, fragments, new int[]{2});

        double delta = 0.001; //FragIonServlet does not take the mass of the electron into account
        Assert.assertEquals("b1^2", 52.51246, peakList.getMz(0), delta);
        Assert.assertEquals("b2^2", 117.03376, peakList.getMz(1), delta);
        Assert.assertEquals("b3^2", 195.08431, peakList.getMz(2), delta);
        Assert.assertEquals("b4^2", 244.61852, peakList.getMz(3), delta);
        Assert.assertEquals("b5^2", 301.16055, peakList.getMz(4), delta);
        Assert.assertEquals("b6^2", 357.70258, peakList.getMz(5), delta);
        Assert.assertEquals("b7^2", 393.22114, peakList.getMz(6), delta);
    }

    /**
     * Immonium data from https://www.abrf.org/ResearchGroups/MassSpectrometry/EPosters/ms97quiz/residueMasses.html
     * <p/>
     * Assuming that Immonium ion mass is a m/z and that z = 1.
     *
     * @throws Exception because it is a test
     */
    @Test
    public void testImmonium() throws Exception {

        PeptideFragmenter peptideFragmenter = new PeptideFragmenter(EnumSet.of(IonType.i), PeakList.Precision.DOUBLE);

        Peptide peptide = Peptide.parse("DELHIVEAEAMNYEGSPIK");

        PeptideSpectrum peakList = peptideFragmenter.fragment(peptide, 1);

        double delta = 0.001;

        Assert.assertEquals("iG", 30.03438, peakList.getMz(0), delta);
        Assert.assertEquals("iA", 44.05003, peakList.getMz(1), delta);
        Assert.assertEquals("iS", 60.04494, peakList.getMz(2), delta);
        Assert.assertEquals("iP", 70.06568, peakList.getMz(3), delta);
        Assert.assertEquals("iV", 72.08133, peakList.getMz(4), delta);
        Assert.assertEquals("iJ", 86.09698, peakList.getMz(5), delta);
        Assert.assertEquals("iN", 87.05584, peakList.getMz(6), delta);
        Assert.assertEquals("iD", 88.03986, peakList.getMz(7), delta);
        Assert.assertEquals("iK", 101.1079, peakList.getMz(8), delta);
        Assert.assertEquals("iE", 102.0555, peakList.getMz(9), delta);
        Assert.assertEquals("iM", 104.0534, peakList.getMz(10), delta);
        Assert.assertEquals("iH", 110.0718, peakList.getMz(11), delta);
        Assert.assertEquals("iY", 136.0762, peakList.getMz(12), delta);
    }

    /**
     * Data generated by:
     * http://db.systemsbiology.net:8080/proteomicsToolkit/FragIonServlet.html
     *
     * @throws Exception because it is a test
     */
    @Test
    public void testConstructor() throws Exception {

        List<PeptidePeakGenerator<PepFragAnnotation>> peakGeneratorList = Collections.<PeptidePeakGenerator<PepFragAnnotation>>singletonList(new BackbonePeakGenerator(EnumSet.of(IonType.b), 1));
        PeptideFragmenter fragmenter = new PeptideFragmenter(peakGeneratorList, PeakList.Precision.DOUBLE);

        Peptide peptide = Peptide.parse("CERVILAS");
        List<PeptideFragment> fragments = new ArrayList<PeptideFragment>();
        fragmenter.generateForward(peptide, fragments);

        PeakList<PepFragAnnotation> peakList = fragmenter.fragment(peptide, 2, fragments, new int[]{2});

        double delta = 0.001; //FragIonServlet does not take the mass of the electron into account
        Assert.assertEquals("b1^2", 52.51246, peakList.getMz(0), delta);
        Assert.assertEquals("b2^2", 117.03376, peakList.getMz(1), delta);
        Assert.assertEquals("b3^2", 195.08431, peakList.getMz(2), delta);
        Assert.assertEquals("b4^2", 244.61852, peakList.getMz(3), delta);
        Assert.assertEquals("b5^2", 301.16055, peakList.getMz(4), delta);
        Assert.assertEquals("b6^2", 357.70258, peakList.getMz(5), delta);
        Assert.assertEquals("b7^2", 393.22114, peakList.getMz(6), delta);
    }

    @Test
    public void testDuplicateMzs() throws Exception {

        PeptideFragmenter fragmenter = new PeptideFragmenter(EnumSet.of(IonType.b, IonType.y), PeakList.Precision.FLOAT);

        PeptideSpectrum spectrum = fragmenter.fragment(Peptide.parse("GGR"), 2);

        double lastMz = 0;
        for (int i = 0; i < spectrum.size(); i++) {

            double currMz = spectrum.getMz(i);

            if (lastMz == currMz) Assert.fail("Duplicate mz at " + i);

            lastMz = currMz;
        }
    }

    /**
     * Data generated by:
     * http://db.systemsbiology.net:8080/proteomicsToolkit/FragIonServlet.html
     */
    @Test
    public void testModY() throws Exception {

        PeptideFragmenter fragmenter = new PeptideFragmenter(EnumSet.of(IonType.y), PeakList.Precision.FLOAT);

        PeptideSpectrum spectrum = fragmenter.fragment(Peptide.parse("NAAQNIALNSS(HPO3)VAAPR"), 2, new int[]{1});

        Assert.assertEquals(15, spectrum.size());

        double delta = 0.1; //HPO3 mass was 79.98 and FragIonServlet does not take the mass of the electron into account
        int i = 0;

        Assert.assertEquals(175.11955, spectrum.getMz(i++), delta);
        Assert.assertEquals(272.17231, spectrum.getMz(i++), delta);
        Assert.assertEquals(343.20942, spectrum.getMz(i++), delta);
        Assert.assertEquals(414.24654, spectrum.getMz(i++), delta);
        Assert.assertEquals(513.31495, spectrum.getMz(i++), delta);
        Assert.assertEquals(680.32698, spectrum.getMz(i++), delta);
        Assert.assertEquals(767.35901, spectrum.getMz(i++), delta);
        Assert.assertEquals(881.40193, spectrum.getMz(i++), delta);
        Assert.assertEquals(994.486, spectrum.getMz(i++), delta);
        Assert.assertEquals(1065.52311, spectrum.getMz(i++), delta);
        Assert.assertEquals(1178.60717, spectrum.getMz(i++), delta);
        Assert.assertEquals(1292.6501, spectrum.getMz(i++), delta);
        Assert.assertEquals(1420.70868, spectrum.getMz(i++), delta);
        Assert.assertEquals(1491.74579, spectrum.getMz(i++), delta);
        Assert.assertEquals(1562.78291, spectrum.getMz(i), delta);
    }

    @Test
    public void testPrecursor() throws Exception {

        PeptideFragmenter fragmenter = new PeptideFragmenter(EnumSet.of(IonType.p), PeakList.Precision.DOUBLE);

        Peptide precursor = Peptide.parse("GGYDSPR");

        PeptideSpectrum spectrum = fragmenter.fragment(precursor, 2, new int[]{1});

        Assert.assertEquals(1, spectrum.size());
        Assert.assertEquals(precursor.calculateMz(1), spectrum.getMz(0), 0.000001);
    }

    @Test
    public void testC() throws Exception {

        PeptideFragmenter fragmenter = new PeptideFragmenter(EnumSet.of(IonType.c), PeakList.Precision.DOUBLE);

        Peptide precursor = Peptide.parse("PE");

        PeptideSpectrum spectrum = fragmenter.fragment(precursor, 2, new int[]{1});

        double delta = 0.001; //FragIonServlet does not take the mass of the electron into account
        Assert.assertEquals(1, spectrum.size());
        Assert.assertEquals(115.08718, spectrum.getMz(0), delta);
    }

    @Test
    public void testGenerateFragmentHotFix14ModCterm() throws Exception {

        Peptide precursor = new PeptideBuilder("VV")
                .addModification(ModAttachment.C_TERM, Modification.parseModification("OH"))
                .build();

        PeptideFragmenter fragmenter = new PeptideFragmenter(EnumSet.of(IonType.b), PeakList.Precision.DOUBLE);
        PeptideSpectrum spectrum = fragmenter.fragment(precursor, 2, new int[]{1});

        Assert.assertEquals(100.075, spectrum.getMz(0), 0.001);
    }

    @Test
    public void testGenerateFragmentHotFix14ModNterm() throws Exception {

        Peptide precursor = new PeptideBuilder("VV")
                .addModification(ModAttachment.N_TERM, Modification.parseModification("OH"))
                .build();

        PeptideFragmenter fragmenter = new PeptideFragmenter(EnumSet.of(IonType.b), PeakList.Precision.DOUBLE);
        PeptideSpectrum spectrum = fragmenter.fragment(precursor, 2, new int[]{1});

        Assert.assertEquals(117.078, spectrum.getMz(0), 0.001);
    }

    @Test
    public void testGenerateFragmentHotFix14ModInternalLastAA() throws Exception {

        Peptide precursor = new PeptideBuilder("VV")
                .addModification(1, Modification.parseModification("OH"))
                .build();

        PeptideFragmenter fragmenter = new PeptideFragmenter(EnumSet.of(IonType.b), PeakList.Precision.DOUBLE);
        PeptideSpectrum spectrum = fragmenter.fragment(precursor, 2, new int[]{1});

        Assert.assertEquals(100.075, spectrum.getMz(0), 0.001);
    }

    @Test
    public void testGenerateFragmentHotFix14ModInternalFirstAA() throws Exception {

        Peptide precursor = new PeptideBuilder("VV")
                .addModification(0, Modification.parseModification("OH"))
                .build();

        PeptideFragmenter fragmenter = new PeptideFragmenter(EnumSet.of(IonType.b), PeakList.Precision.DOUBLE);
        PeptideSpectrum spectrum = fragmenter.fragment(precursor, 2, new int[]{1});

        Assert.assertEquals(117.078, spectrum.getMz(0), 0.001);
    }

    /**
     * Data generated by: http://db.systemsbiology.net:8080/proteomicsToolkit/FragIonServlet.html
     */
    @Test
    public void testCZ() throws Exception {

        PeptideFragmenter fragmenter = new PeptideFragmenter(EnumSet.of(IonType.c, IonType.z), PeakList.Precision.FLOAT);

        PeptideSpectrum spectrum = fragmenter.fragment(Peptide.parse("PEPTIDE"), 2, new int[]{1});
        double delta = 0.001; //FragIonServlet does not take the mass of the electron into account

        Assert.assertEquals(12, spectrum.size());

        int i = 0;
        Assert.assertEquals("c1", 115.08718, spectrum.getMz(i++), delta);
        Assert.assertEquals("z1", 131.03448, spectrum.getMz(i++), delta);
        Assert.assertEquals("c2", 244.12978, spectrum.getMz(i++), delta);
        Assert.assertEquals("z2", 246.06142, spectrum.getMz(i++), delta);
        Assert.assertEquals("c3", 341.18254, spectrum.getMz(i++), delta);
        Assert.assertEquals("z3", 359.14548, spectrum.getMz(i++), delta);
        Assert.assertEquals("c4", 442.23022, spectrum.getMz(i++), delta);
        Assert.assertEquals("z4", 460.19316, spectrum.getMz(i++), delta);
        Assert.assertEquals("c5", 555.31428, spectrum.getMz(i++), delta);
        Assert.assertEquals("z5", 557.24593, spectrum.getMz(i++), delta);
        Assert.assertEquals("c6", 670.34122, spectrum.getMz(i++), delta);
        Assert.assertEquals("z6", 686.28852, spectrum.getMz(i), delta);
    }

    /**
     * Data generated by: http://db.systemsbiology.net:8080/proteomicsToolkit/FragIonServlet.html
     */
    @Test
    public void testX() throws Exception {

        PeptideFragmenter fragmenter = new PeptideFragmenter(EnumSet.of(IonType.x), PeakList.Precision.FLOAT);

        PeptideSpectrum spectrum = fragmenter.fragment(Peptide.parse("PEPTIDE"), 2, new int[]{1});
        double delta = 0.001; //FragIonServlet does not take the mass of the electron into account

        Assert.assertEquals(6, spectrum.size());

        int i = 0;
        Assert.assertEquals("x7", 174.04029, spectrum.getMz(i++), delta);
        Assert.assertEquals("x6", 289.06723, spectrum.getMz(i++), delta);
        Assert.assertEquals("x5", 402.1513, spectrum.getMz(i++), delta);
        Assert.assertEquals("x4", 503.19898, spectrum.getMz(i++), delta);
        Assert.assertEquals("x3", 600.25174, spectrum.getMz(i++), delta);
        Assert.assertEquals("x2", 729.29433, spectrum.getMz(i), delta);
    }

    @Test
    public void annotationTest() {

        Set<IonType> ionTypes = EnumSet.of(IonType.b, IonType.y, IonType.i, IonType.p);
        Mass waterLoss = Composition.parseComposition("H-2O-1");
        Mass ammoniumLoss = Composition.parseComposition("H-3N-1");
        List<PeptidePeakGenerator<PepFragAnnotation>> peakGenerators = Lists.newArrayList();
        peakGenerators.add(new BackbonePeakGenerator(ionTypes, 1));
        peakGenerators.add(new PeptideNeutralLossPeakGenerator(waterLoss,
                EnumSet.of(AminoAcid.S, AminoAcid.T, AminoAcid.D, AminoAcid.E), ionTypes, 1));
        peakGenerators.add(new PeptideNeutralLossPeakGenerator(ammoniumLoss,
                EnumSet.of(AminoAcid.Q, AminoAcid.K, AminoAcid.R, AminoAcid.N), ionTypes, 1));
        PeptideFragmenter fragmenter = new PeptideFragmenter(peakGenerators, PeakList.Precision.DOUBLE);

        Peptide peptide = Peptide.parse("DSPR");

        PeptideSpectrum peptideSpectrum = fragmenter.fragment(peptide, 2);

        for(int i = 0; i < peptideSpectrum.size(); i++) {


            StringBuilder buff = new StringBuilder();
            List<PepFragAnnotation> annotationList = peptideSpectrum.getAnnotations(i);
            if(!annotationList.isEmpty()) {

                buff.append(", Arrays.asList(");
                for(int a = 0; a < annotationList.size(); a++) {

                    if(a != 0)
                        buff.append(", ");
                    buff.append("Mockito.mock(PeakAnnotation.class)");
                }
                buff.append(")");
            }
        }
    }
}
