package org.expasy.mzjava.proteomics.mol.digest;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.*;
import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.core.mol.AtomicSymbol;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.mol.Protein;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * @author fnikitin
 * Date: 9/26/12
 */
public class ProteinDigesterTest {

    @Test
    public void testDigestion() {

        Protein prot = new Protein("ACC1", "RESALYTNIKALASKR");

        List<Peptide> digests = new ProteinDigester.Builder(Protease.TRYPSIN).build().digest(prot);

        Assert.assertEquals(4, digests.size());
        Assert.assertEquals("R", digests.get(0).toSymbolString());
        Assert.assertEquals("ESALYTNIK", digests.get(1).toSymbolString());
        Assert.assertEquals("ALASK", digests.get(2).toSymbolString());
        Assert.assertEquals("R", digests.get(3).toSymbolString());
    }

    @Test
    public void testDigestionEnterokinase() {

        Protein prot = new Protein("AC", "RESADEDKTNIDDKALAEEEKR");

        List<Peptide> digests = new ProteinDigester.Builder(Protease.ENTEROKINASE).build().digest(prot);

        Assert.assertEquals(3, digests.size());
        Assert.assertEquals("RESADEDK", digests.get(0).toSymbolString());
        Assert.assertEquals("TNIDDKALAEEEK", digests.get(1).toSymbolString());
        Assert.assertEquals("R", digests.get(2).toSymbolString());
    }

    @Test
    public void testDigestionWithContainer() {

        Protein prot = new Protein("ACC1", "RESALYTNIKALASKR");

        ProteinDigester digester = new ProteinDigester.Builder(Protease.TRYPSIN).build();

        List<Peptide> digests = Lists.newArrayList();

        digester.digest(prot, digests);

        Assert.assertEquals(4, digests.size());
        Assert.assertEquals("R", digests.get(0).toSymbolString());
        Assert.assertEquals("ESALYTNIK", digests.get(1).toSymbolString());
        Assert.assertEquals("ALASK", digests.get(2).toSymbolString());
        Assert.assertEquals("R", digests.get(3).toSymbolString());
    }

    @Test
    public void testDigestionWithMissedCleavages() {

        Protein prot = new Protein("ACC1", "RESALYTNIKALASKR");

        List<Peptide> digests = new ProteinDigester.Builder(Protease.TRYPSIN).missedCleavageMax(3).build().digest(prot);

        Assert.assertEquals(10, digests.size());

        class MissedCleavagePredicate implements Predicate<Peptide> {

            private final int mc;
            private final CleavageSiteFinder csf;

            MissedCleavagePredicate(int mc, CleavageSiteFinder csf) {

                this.mc = mc;
                this.csf = csf;
            }

            @Override
            public boolean apply(Peptide peptide) {

                return csf.countCleavageSites(peptide) == mc;
            }
        }

        List<Peptide> digests0 = Lists.newArrayList(Collections2.filter(digests, new MissedCleavagePredicate(0, Protease.TRYPSIN)));

        Assert.assertEquals(4, digests0.size());
        Assert.assertEquals("R", digests0.get(0).toSymbolString());
        Assert.assertEquals("ESALYTNIK", digests0.get(1).toSymbolString());
        Assert.assertEquals("ALASK", digests0.get(2).toSymbolString());
        Assert.assertEquals("R", digests0.get(3).toSymbolString());

        List<Peptide> digests1 = Lists.newArrayList(Collections2.filter(digests, new MissedCleavagePredicate(1, Protease.TRYPSIN)));

        Assert.assertEquals(3, digests1.size());
        Assert.assertEquals("RESALYTNIK", digests1.get(0).toSymbolString());
        Assert.assertEquals("ESALYTNIKALASK", digests1.get(1).toSymbolString());
        Assert.assertEquals("ALASKR", digests1.get(2).toSymbolString());

        List<Peptide> digests2 = Lists.newArrayList(Collections2.filter(digests, new MissedCleavagePredicate(2, Protease.TRYPSIN)));

        Assert.assertEquals(2, digests2.size());
        Assert.assertEquals("RESALYTNIKALASK", digests2.get(0).toSymbolString());
        Assert.assertEquals("ESALYTNIKALASKR", digests2.get(1).toSymbolString());

        List<Peptide> digests3 = Lists.newArrayList(Collections2.filter(digests, new MissedCleavagePredicate(3, Protease.TRYPSIN)));

        Assert.assertEquals(1, digests3.size());
        Assert.assertEquals("RESALYTNIKALASKR", digests3.get(0).toSymbolString());
    }

    @Test
    public void testDigestTermMod() {

        Modification nMod = new Modification("nmod", new Composition.Builder(AtomicSymbol.C, -3).add(AtomicSymbol.H, 2).build());
        Modification cMod = new Modification("cmod", new Composition.Builder(AtomicSymbol.H).add(AtomicSymbol.P).add(AtomicSymbol.O, 3).build());

        CleavageSiteMatcher cs = new CleavageSiteMatcher("M|X", Optional.of(nMod), Optional.of(cMod));

        ProteinDigester proteinDigester = new ProteinDigester.Builder(cs).build();

        Protein prot = new Protein("ACC1", "AHNGMRWPTG");

        List<Peptide> digests = proteinDigester.digest(prot);

        Assert.assertEquals(-34., digests.get(1).getModifications(ModAttachment.nTermSet).get(0).getMolecularMass(), 0.1);
        Assert.assertEquals(79.9, digests.get(0).getModifications(ModAttachment.cTermSet).get(0).getMolecularMass(), 0.1);
    }

    @Test
    public void testDigestionWithCnbr() {

        Protein prot = new Protein("ACC1", "AHNGMRWPTG");

        List<Peptide> digests = new ProteinDigester.Builder(Protease.CNBR).build().digest(prot);

        Assert.assertEquals(-48.0039, digests.get(0).getModifications(ModAttachment.cTermSet).get(0).getMolecularMass(), 0.01);
    }

    @Test
    public void testSemiTrypticDigestion() {

        Protein prot = new Protein("ACC1", "TFGQVVAR");

        ProteinDigester proteinDigester = new ProteinDigester.Builder(Protease.TRYPSIN).semi().build();

        List<Peptide> digests = proteinDigester.digest(prot);

        Assert.assertEquals(15, digests.size());
        Assert.assertEquals("TFGQVVAR", digests.get(0).toSymbolString());
        Assert.assertEquals("T", digests.get(1).toSymbolString());
        Assert.assertEquals("FGQVVAR", digests.get(2).toSymbolString());
        Assert.assertEquals("TF", digests.get(3).toSymbolString());
        Assert.assertEquals("GQVVAR", digests.get(4).toSymbolString());
        Assert.assertEquals("TFG", digests.get(5).toSymbolString());
        Assert.assertEquals("QVVAR", digests.get(6).toSymbolString());
        Assert.assertEquals("TFGQ", digests.get(7).toSymbolString());
        Assert.assertEquals("VVAR", digests.get(8).toSymbolString());
        Assert.assertEquals("TFGQV", digests.get(9).toSymbolString());
        Assert.assertEquals("VAR", digests.get(10).toSymbolString());
        Assert.assertEquals("TFGQVV", digests.get(11).toSymbolString());
        Assert.assertEquals("AR", digests.get(12).toSymbolString());
        Assert.assertEquals("TFGQVVA", digests.get(13).toSymbolString());
        Assert.assertEquals("R", digests.get(14).toSymbolString());
    }

    @Test
    public void testSemiTrypticDigestion2() {

        Protein prot = new Protein("ACC1", "TFGQVVAR");

        ProteinDigester proteinDigester = new ProteinDigester.Builder(Protease.TRYPSIN).semi().controller(new AcceptAllDigestionController() {
            @Override
            public boolean retainSemiDigest(Peptide digest) {

                return digest.size() > 1;
            }
        }).build();

        List<Peptide> digests = proteinDigester.digest(prot);

        Assert.assertEquals(13, digests.size());

        Assert.assertEquals("TFGQVVAR", digests.get(0).toSymbolString());
        Assert.assertEquals("FGQVVAR", digests.get(1).toSymbolString());
        Assert.assertEquals("TF", digests.get(2).toSymbolString());
        Assert.assertEquals("GQVVAR", digests.get(3).toSymbolString());
        Assert.assertEquals("TFG", digests.get(4).toSymbolString());
        Assert.assertEquals("QVVAR", digests.get(5).toSymbolString());
        Assert.assertEquals("TFGQ", digests.get(6).toSymbolString());
        Assert.assertEquals("VVAR", digests.get(7).toSymbolString());
        Assert.assertEquals("TFGQV", digests.get(8).toSymbolString());
        Assert.assertEquals("VAR", digests.get(9).toSymbolString());
        Assert.assertEquals("TFGQVV", digests.get(10).toSymbolString());
        Assert.assertEquals("AR", digests.get(11).toSymbolString());
        Assert.assertEquals("TFGQVVA", digests.get(12).toSymbolString());
    }

    @Test
    public void testDigestionWithMods() {

        Protein prot = new Protein("ACC1", "RESALYTNIKALASKR");

        Modification mod = new Modification("79.9", new Composition.Builder(AtomicSymbol.H).add(AtomicSymbol.P).add(AtomicSymbol.O, 3).build());

        ProteinDigester proteinDigester = new ProteinDigester.Builder(Protease.TRYPSIN)
                .addFixedMod(AminoAcid.Y, mod)
                .addFixedMod(AminoAcid.S, mod)
                .addFixedMod(AminoAcid.T, mod).build();

        List<Peptide> digests = proteinDigester.digest(prot);

        Assert.assertEquals(4, digests.size());
        Assert.assertFalse(digests.get(0).hasModifications());
        Assert.assertArrayEquals(new int[] {1, 4, 5}, digests.get(1).getModificationIndexes(ModAttachment.all));
        Assert.assertArrayEquals(new int[] {3}, digests.get(2).getModificationIndexes(ModAttachment.all));
        Assert.assertFalse(digests.get(3).hasModifications());
    }

    @Test
    public void testLargeProteinBugToFix20() {

        Protein prot = new Protein("ACC1",
                "MGLPLARLAAVCLALSLAGGSELQTEGRTRYHGRNVCSTWGNFHYKTFDGDVFRFPGLCD" +
                "YNFASDCRGSYKEFAVHLKRGPGQAEAPAGVESILLTIKDDTIYLTRHLAVLNGAVVSTP" +
                        "HYSPGLLIEKSDAYTKVYSRAGLTLMWNREDALMLELDTKFRNHTCGLCGDYNGLQSYSE" +
                        "FLSDGVLFSPLEFGNMQKINQPDVVCEDPEEEVAPASCSEHRAECERLLTAEAFADCQDL" +
                        "VPLEPYLRACQQDRCRCPGGDTCVCSTVAEFSRQCSHAGGRPGNWRTATLCPKTCPGNLV" +
                        "YLESGSPCMDTCSHLEVSSLCEEHRMDGCFCPEGTVYDDIGDSGCVPVSQCHCRLHGHLY" +
                        "TPGQEITNDCEQCVCNAGRWVCKDLPCPGTCALEGGSHITTFDGKTYTFHGDCYYVLAKG" +
                        "DHNDSYALLGELAPCGSTDKQTCLKTVVLLADKKKNAVVFKSDGSVLLNQLQVNLPHVTA" +
                        "SFSVFRPSSYHIMVSMAIGVRLQVQLAPVMQLFVTLDQASQGQVQGLCGNFNGLEGDDFK" +
                        "TASGLVEATGAGFANTWKAQSTCHDKLDWLDDPCSLNIESANYAEHWCSLLKKTETPFGR" +
                        "CHSAVDPAEYYKRCKYDTCNCQNNEDCLCAALSSYARACTAKGVMLWGWREHVCNKDVGS" +
                        "CPNSQVFLYNLTTCQQTCRSLSEADSHCLEGFAPVDGCGCPDHTFLDEKGRCVPLAKCSC" +
                        "YHRGLYLEAGDVVVRQEERCVCRDGRLHCRQIRLIGQSCTAPKIHMDCSNLTALATSKPR" +
                        "ALSCQTLAAGYYHTECVSGCVCPDGLMDDGRGGCVVEKECPCVHNNDLYSSGAKIKVDCN" +
                        "TCTCKRGRWVCTQAVCHGTCSIYGSGHYITFDGKYYDFDGHCSYVAVQDYCGQNSSLGSF" +
                        "SIITENVPCGTTGVTCSKAIKIFMGRTELKLEDKHRVVIQRDEGHHVAYTTREVGQYLVV" +
                        "ESSTGIIVIWDKRTTVFIKLAPSYKGTVCGLCGNFDHRSNNDFTTRDHMVVSSELDFGNS" +
                        "WKEAPTCPDVSTNPEPCSLNPHRRSWAEKQCSILKSSVFSICHSKVDPKPFYEACVHDSC" +
                        "SCDTGGDCECFCSAVASYAQECTKEGACVFWRTPDLCPIFCDYYNPPHECEWHYEPCGNR" +
                        "SFETCRTINGIHSNISVSYLEGCYPRCPKDRPIYEEDLKKCVTADKCGCYVEDTHYPPGA" +
                        "SVPTEETCKSCVCTNSSQVVCRPEEGKILNQTQDGAFCYWEICGPNGTVEKHFNICSITT" +
                        "RPSTLTTFTTITLPTTPTSFTTTTTTTTPTSSTVLSTTPKLCCLWSDWINEDHPSSGSDD" +
                        "GDREPFDGVCGAPEDIECRSVKDPHLSLEQHGQKVQCDVSVGFICKNEDQFGNGPFGLCY" +
                        "DYKIRVNCCWPMDKCITTPSPPTTTPSPPPTTTTTLPPTTTPSPPTTTTTTPPPTTTPSP" +
                        "PITTTTTPLPTTTPSPPISTTTTPPPTTTPSPPTTTPSPPTTTPSPPTTTTTTPPPTTTP" +
                        "SPPMTTPITPPASTTTLPPTTTPSPPTTTTTTPPPTTTPSPPTTTPITPPTSTTTLPPTT" +
                        "TPSPPPTTTTTPPPTTTPSPPTTTTPSPPTITTTTPPPTTTPSPPTTTTTTPPPTTTPSP" +
                        "PTTTPITPPTSTTTLPPTTTPSPPPTTTTTPPPTTTPSPPTTTTPSPPITTTTTPPPTTT" +
                        "PSSPITTTPSPPTTTMTTPSPTTTPSSPITTTTTPSSTTTPSPPPTTMTTPSPTTTPSPP" +
                        "TTTMTTLPPTTTSSPLTTTPLPPSITPPTFSPFSTTTPTTPCVPLCNWTGWLDSGKPNFH" +
                        "KPGGDTELIGDVCGPGWAANISCRATMYPDVPIGQLGQTVVCDVSVGLICKNEDQKPGGV" +
                        "IPMAFCLNYEINVQCCECVTQPTTMTTTTTENPTPPTTTPITTTTTVTPTPTPTGTQTPT" +
                        "TTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTP" +
                        "TPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPIT" +
                        "TTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGT" +
                        "QTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTV" +
                        "TPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTT" +
                        "TPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPT" +
                        "PTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITT" +
                        "TTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQ" +
                        "TPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVT" +
                        "PTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTT" +
                        "PITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTP" +
                        "TGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTT" +
                        "TTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQT" +
                        "PTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTP" +
                        "TPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTP" +
                        "ITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPT" +
                        "GTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTT" +
                        "TVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTP" +
                        "TTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPT" +
                        "PTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPI" +
                        "TTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTG" +
                        "TQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTT" +
                        "VTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPT" +
                        "TTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTP" +
                        "TPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPIT" +
                        "TTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGT" +
                        "QTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTV" +
                        "TPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTT" +
                        "TPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPT" +
                        "PTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITT" +
                        "TTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQ" +
                        "TPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVT" +
                        "PTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTT" +
                        "PITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTP" +
                        "TGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTT" +
                        "TTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQT" +
                        "PTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTP" +
                        "TPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTGPPTH" +
                        "TSTAPIAELTTSNPPPESSTPQTSRSTSSPLTESTTLLSTLPPAIEMTSTAPPSTPTAPT" +
                        "TTSGGHTLSPPPSTTTSPPGTPTRGTTTGSSSAPTPSTVQTTTTSAWTPTPTPLSTPSII" +
                        "RTTGLRPYPSSVLICCVLNDTYYAPGEEVYNGTYGDTCYFVNCSLSCTLEFYNWSCPSTP" +
                        "SPTPTPSKSTPTPSKPSSTPSKPTPGTKPPECPDFDPPRQENETWWLCDCFMATCKYNNT" +
                        "VEIVKVECEPPPMPTCSNGLQPVRVEDPDGCCWHWECDCYCTGWGDPHYVTFDGLYYSYQ" +
                        "GNCTYVLVEEISPSVDNFGVYIDNYHCDPNDKVSCPRTLIVRHETQEVLIKTVHMMPMQV" +
                        "QVQVNRQAVALPYKKYGLEVYQSGINYVVDIPELGVLVSYNGLSFSVRLPYHRFGNNTKG" +
                        "QCGTCTNTTSDDCILPSGEIVSNCEAAADQWLVNDPSKPHCPHSSSTTKRPAVTVPGGGK" +
                        "TTPHKDCTPSPLCQLIKDSLFAQCHALVPPQHYYDACVFDSCFMPGSSLECASLQAYAAL" +
                        "CAQQNICLDWRNHTHGACLVECPSHREYQACGPAEEPTCKSSSSQQNNTVLVEGCFCPEG" +
                        "TMNYAPGFDVCVKTCGCVGPDNVPREFGEHFEFDCKNCVCLEGGSGIICQPKRCSQKPVT" +
                        "HCVEDGTYLATEVNPADTCCNITVCKCNTSLCKEKPSVCPLGFEVKSKMVPGRCCPFYWC" +
                        "ESKGVCVHGNAEYQPGSPVYSSKCQDCVCTDKVDNNTLLNVIACTHVPCNTSCSPGFELM" +
                        "EAPGECCKKCEQTHCIIKRPDNQHVILKPGDFKSDPKNNCTFFSCVKIHNQLISSVSNIT" +
                        "CPNFDASICIPGSITFMPNGCCKTCTPRNETRVPCSTVPVTTEVSYAGCTKTVLMNHCSG" +
                        "SCGTFVMYSAKAQALDHSCSCCKEEKTSQREVVLSCPNGGSLTHTYTHIESCQCQDTVCG" +
                        "LPTGTSRRARRSPRHLGSG");

        List<Peptide> digests = new ProteinDigester.Builder(Protease.ARG_C).build().digest(prot);

        Assert.assertEquals(88, digests.size());
    }

    @Test
    public void testGetDigestPositions() {

        Protein prot = new Protein("ACC1", "RESALYTNIKALASKR");

        PositionAccu cb = new PositionAccu();

        ProteinDigester proteinDigester = new ProteinDigester.Builder(Protease.TRYPSIN).controller(cb).build();

        List<Peptide> digests = proteinDigester.digest(prot);
        Iterator<Integer> expectedFromPositions = Arrays.asList(0, 1, 10, 15).iterator();
        Iterator<Integer> expectedToPositions = Arrays.asList(0, 9, 14, 15).iterator();

        Assert.assertEquals(expectedFromPositions.next(), cb.getFrom(digests.get(0)).get(0));
        Assert.assertEquals(expectedToPositions.next(), cb.getTo(digests.get(0)).get(0));
        Assert.assertEquals(expectedFromPositions.next(), cb.getFrom(digests.get(1)).get(0));
        Assert.assertEquals(expectedToPositions.next(), cb.getTo(digests.get(1)).get(0));
        Assert.assertEquals(expectedFromPositions.next(), cb.getFrom(digests.get(2)).get(0));
        Assert.assertEquals(expectedToPositions.next(), cb.getTo(digests.get(2)).get(0));
        Assert.assertEquals(expectedFromPositions.next(), cb.getFrom(digests.get(3)).get(1));
        Assert.assertEquals(expectedToPositions.next(), cb.getTo(digests.get(3)).get(1));
    }

    @Test
    public void testGetNtDigests() {

        Protein prot = new Protein("ACC1", "RESALYTNIKALASKR");

        ProteinDigester proteinDigester = new ProteinDigester.Builder(Protease.TRYPSIN).missedCleavageMax(2)
                .controller(new NTermFilter()).build();

        List<Peptide> digests = proteinDigester.digest(prot);

        List<String> expectedDigestSequence = Lists.newArrayList("R", "RESALYTNIK", "RESALYTNIKALASK");
        List<Integer> expectedDigestMc = Lists.newArrayList(0, 1, 2);

        int count=0;
        for (Peptide digest : digests) {

            Assert.assertEquals(expectedDigestMc.get(count).intValue(), Protease.TRYPSIN.countCleavageSites(digest));
            Assert.assertEquals(expectedDigestSequence.get(count), digest.toSymbolString());
            count++;
        }
    }

    @Test
    public void testGetNtDigests2() {

        Protein prot = new Protein("ACC1", "RESALYTNIKALASKR");

        ProteinDigester proteinDigester = new ProteinDigester.Builder(Protease.TRYPSIN).missedCleavageMax(Integer.MAX_VALUE)
                .controller(new NTermFilter()).build();

        List<Peptide> digests = proteinDigester.digest(prot);

        List<String> expectedDigestSequence = Lists.newArrayList("R", "RESALYTNIK", "RESALYTNIKALASK", "RESALYTNIKALASKR");

        int mc=0;
        for (Peptide digest : digests) {

            Assert.assertEquals(expectedDigestSequence.get(mc), digest.toSymbolString());
            mc++;
        }
    }

    @Test
    public void testGetCtDigests() {

        Protein prot = new Protein("ACC1", "RESALYTNIKALASKR");

        ProteinDigester proteinDigester = new ProteinDigester.Builder(Protease.TRYPSIN).missedCleavageMax(2).controller(new CTermFilter()).build();

        List<Peptide> digests = proteinDigester.digest(prot);

        List<String> expectedDigestSequence = Lists.newArrayList("R", "ESALYTNIKALASKR", "ALASKR");
        List<Integer> expectedDigestMc = Lists.newArrayList(0, 2, 1);

        int i=0;
        for (Peptide digest : digests) {

            Assert.assertEquals(expectedDigestMc.get(i).intValue(), Protease.TRYPSIN.countCleavageSites(digest));
            Assert.assertEquals(expectedDigestSequence.get(i), digest.toSymbolString());

            i++;
        }
    }

    @Test
    public void testGetCtDigestsWithReverseIteration() {

        Protein prot = new Protein("ACC1", "RESALYTNIKALASKR");

        ProteinDigester proteinDigester = new ProteinDigester.Builder(Protease.TRYPSIN)
                .cleavageSiteIteration(ProteinDigester.CleavageSiteIteration.REVERSE)
                .missedCleavageMax(2).controller(new CTermFilter()).build();

        List<Peptide> digests = proteinDigester.digest(prot);

        List<String> expectedDigestSequence = Lists.newArrayList("R", "ESALYTNIKALASKR", "ALASKR");
        List<Integer> expectedDigestMc = Lists.newArrayList(0, 2, 1);

        int i=0;
        for (Peptide digest : digests) {

            Assert.assertEquals(expectedDigestMc.get(i).intValue(), Protease.TRYPSIN.countCleavageSites(digest));
            Assert.assertEquals(expectedDigestSequence.get(i), digest.toSymbolString());

            i++;
        }
    }

    @Test
    public void testGetNtMc2Digest() {

        Protein prot = new Protein("ACC1", "RESALYTNIKALASKR");

        ProteinDigester proteinDigester = new ProteinDigester.Builder(Protease.TRYPSIN)
                .missedCleavageMax(100).controller(new ExactMcNTermFilter(2)).build();

        List<Peptide> digests = proteinDigester.digest(prot);

        List<String> expectedDigestSequence = Lists.newArrayList("RESALYTNIKALASK");

        Assert.assertEquals(1, digests.size());
        Assert.assertEquals(expectedDigestSequence.get(0), digests.get(0).toSymbolString());
    }

    @Test
    public void testGetNtMc5OrMaxMcDigest() {

        Protein prot = new Protein("ACC1", "RESALYTNIKALASKR");

        ProteinDigester proteinDigester = new ProteinDigester.Builder(Protease.TRYPSIN).missedCleavageMax(100)
                .controller(new EitherExactOrGreatestMc(5)).build();

        List<Peptide> digests = proteinDigester.digest(prot);

        List<String> expectedDigestSequence = Lists.newArrayList("RESALYTNIKALASKR");

        Assert.assertEquals(1, digests.size());
        Assert.assertEquals(expectedDigestSequence.get(0), digests.get(0).toSymbolString());
    }

    @Test
    public void testGetNtMc3DigestsThenInterrupt() {

        Protein prot = new Protein("ACC1", "RESALYTNIKALASKR");

        ProteinDigester proteinDigester = new ProteinDigester.Builder(Protease.TRYPSIN).missedCleavageMax(3)
                .controller(new AcceptAllDigestionController() {
                    @Override
                    public boolean interruptDigestion(Protein protein, int proteinCleavageSiteCount, int inclusiveLowerBoundIndex, int inclusiveUpperBoundIndex) {

                        return inclusiveUpperBoundIndex == proteinCleavageSiteCount - 1;
                    }

                    @Override
                    public boolean retainDigest(Protein protein, Peptide digest, int digestPos) {

                        return digestPos == 0;
                    }
                })
                .build();

        List<Peptide> digests = proteinDigester.digest(prot);
        List<String> expectedDigestSequence = Lists.newArrayList("R", "RESALYTNIK", "RESALYTNIKALASK", "RESALYTNIKALASKR");

        for (int i=0 ; i< digests.size() ;i++) {

            Assert.assertEquals(expectedDigestSequence.get(i), digests.get(i).toSymbolString());
        }
    }

    @Test
    public void testGetNtMc3DigestOnlyThenInterrupt() {

        Protein prot = new Protein("ACC1", "RESALYTNIKALASKR");

        ProteinDigester proteinDigester = new ProteinDigester.Builder(Protease.TRYPSIN).missedCleavageMax(3)
                .controller(new AcceptAllDigestionController() {
                    @Override
                    public boolean interruptDigestion(Protein protein, int proteinCleavageSiteCount, int inclusiveLowerBoundIndex, int inclusiveUpperBoundIndex) {

                        return inclusiveUpperBoundIndex == proteinCleavageSiteCount;
                    }

                    @Override
                    public boolean makeDigest(Protein protein, int inclusiveProteinLowerIndex, int exclusiveProteinUpperIndex, int missedCleavagesCount) {

                        return missedCleavagesCount == 3;
                    }

                    @Override
                    public boolean retainDigest(Protein protein, Peptide digest, int digestIndex) {

                        return digestIndex == 0;
                    }
                })
                .build();

        List<Peptide> digests = proteinDigester.digest(prot);

        Assert.assertEquals(1, digests.size());
        Assert.assertEquals("RESALYTNIKALASKR", digests.get(0).toSymbolString());
    }


    @Test
    public void testSemi() {

        Peptide dp = new Peptide(Peptide.parse("PEPTIDE"));

        Peptide digest = new Peptide(dp, 0, 3);

        Assert.assertEquals(AminoAcid.P, digest.getSymbol(0));
        Assert.assertEquals(AminoAcid.E, digest.getSymbol(1));
        Assert.assertEquals(AminoAcid.P, digest.getSymbol(2));
    }

    @Test
    public void testSemi2() {

        Peptide dp = new Peptide(Peptide.parse("PEPT(OH)IDE"));

        Peptide digest = new Peptide(dp, 2, 4);

        Assert.assertEquals(17, digest.getModificationsAt(1, ModAttachment.sideChainSet).get(0).getMolecularMass(), 0.01);
    }

    @Test
    public void testDigestionWithMassRange() {

        Protein prot = new Protein("ACC1", "RESALYTNIKALASKR");

        ProteinDigester proteinDigester = new ProteinDigester.Builder(Protease.TRYPSIN)
                .controller(new AcceptAllDigestionController() {

                    @Override
                    public boolean retainDigest(Protein protein, Peptide digest, int digestIndex) {

                        return Range.closed(500., 2000.).contains(digest.getMolecularMass());
                    }
                })
                .build();

        List<Peptide> digests = proteinDigester.digest(prot);

        Assert.assertEquals(1, digests.size());
        Assert.assertEquals("ESALYTNIK", digests.get(0).toSymbolString());
        Assert.assertEquals(1037.539, digests.get(0).getMolecularMass(), 0.001);
    }

    @Test
    public void test() throws Exception {

        Protein protein = new Protein("ACC1", "MVDREQLVQKARLAEQAERYDDMAAAMKSVTELNEALSNEERNLLSVAYKNVVGARRSSW" +
                "RVISSIEQKTSADGNEKKMEMVRAYREKIEKELETVCRDVLNLLDNFLIKNCNETQHESK" +
                "VFYLKMKGDYYRYLAEVATGEKRVGVVESSEKSYSEAHEISKEHMQPTHPIRLGLALNYS" +
                "VFYYEIQNAPEQACHLAKTAFDDAIAELDTLNEDSYKDSTLIMQLLRDNLTLWTSDQZDD" +
                "EGGETNN");

        ProteinDigester digester = new ProteinDigester.Builder(Protease.TRYPSIN).build();

        List<Peptide> digests = digester.digest(protein);

        Assert.assertEquals(32, digests.size());
    }

    @Test
    public void testMC1() throws Exception {

        Protein protein = new Protein("ACC1", "MVDREQLVQKARLAEQAERYDDMAAAMKSVTELNEALSNEERNLLSVAYKNVVGARRSSW" +
                "RVISSIEQKTSADGNEKKMEMVRAYREKIEKELETVCRDVLNLLDNFLIKNCNETQHESK" +
                "VFYLKMKGDYYRYLAEVATGEKRVGVVESSEKSYSEAHEISKEHMQPTHPIRLGLALNYS" +
                "VFYYEIQNAPEQACHLAKTAFDDAIAELDTLNEDSYKDSTLIMQLLRDNLTLWTSDQZDD" +
                "EGGETNN");

        ProteinDigester digester = new ProteinDigester.Builder(Protease.TRYPSIN).missedCleavageMax(1).build();

        List<Peptide> digests = digester.digest(protein);

        Assert.assertEquals(63, digests.size());
    }

    @Test
    public void testMC2() throws Exception {

        Protein protein = new Protein("ACC1", "MVDREQLVQKARLAEQAERYDDMAAAMKSVTELNEALSNEERNLLSVAYKNVVGARRSSW" +
                "RVISSIEQKTSADGNEKKMEMVRAYREKIEKELETVCRDVLNLLDNFLIKNCNETQHESK" +
                "VFYLKMKGDYYRYLAEVATGEKRVGVVESSEKSYSEAHEISKEHMQPTHPIRLGLALNYS" +
                "VFYYEIQNAPEQACHLAKTAFDDAIAELDTLNEDSYKDSTLIMQLLRDNLTLWTSDQZDD" +
                "EGGETNN");

        ProteinDigester digester = new ProteinDigester.Builder(Protease.TRYPSIN).missedCleavageMax(2).build();

        List<Peptide> digests = digester.digest(protein);

        Assert.assertEquals(93, digests.size());
    }

    @Test
    public void testPEPSINE_PH_GT_2() throws Exception {

        Protein protein = new Protein("P1", "MTNHQLSTTEWNDETLYQEFNGLKKMNPKLKTLLAIGGWNFGTQKFTDMVATANNRQTFV");

        ProteinDigester digester = new ProteinDigester.Builder(Protease.PEPSINE_PH_GT_2).build();
        List<Peptide> digests = digester.digest(protein);

        Assert.assertEquals(21, digests.size());
        Assert.assertEquals(Peptide.parse("MTNHQ"), digests.get(0));
        Assert.assertEquals(Peptide.parse("LSTTE"), digests.get(1));
        Assert.assertEquals(Peptide.parse("W"), digests.get(2));
        Assert.assertEquals(Peptide.parse("NDET"), digests.get(3));
        Assert.assertEquals(Peptide.parse("L"), digests.get(4));
        Assert.assertEquals(Peptide.parse("Y"), digests.get(5));
        Assert.assertEquals(Peptide.parse("QE"), digests.get(6));
        Assert.assertEquals(Peptide.parse("F"), digests.get(7));
        Assert.assertEquals(Peptide.parse("NG"), digests.get(8));
        Assert.assertEquals(Peptide.parse("L"), digests.get(9));
        Assert.assertEquals(Peptide.parse("KKMNPKL"), digests.get(10));
        Assert.assertEquals(Peptide.parse("KT"), digests.get(11));
        Assert.assertEquals(Peptide.parse("LL"), digests.get(12));
        Assert.assertEquals(Peptide.parse("AIGG"), digests.get(13));
        Assert.assertEquals(Peptide.parse("W"), digests.get(14));
        Assert.assertEquals(Peptide.parse("N"), digests.get(15));
        Assert.assertEquals(Peptide.parse("F"), digests.get(16));
        Assert.assertEquals(Peptide.parse("GTQK"), digests.get(17));
        Assert.assertEquals(Peptide.parse("F"), digests.get(18));
        Assert.assertEquals(Peptide.parse("TDMVATANNRQTF"), digests.get(19));
        Assert.assertEquals(Peptide.parse("V"), digests.get(20));
    }

    @Test
    public void testPEPSINE_PH_1_3() throws Exception {

        Protein protein = new Protein("P1", "MTNHQLSTTEWNDETLYQEFNGLKKMNPKLKTLLAIGGWNFGTQKFTDMVATANNRQTFV");

        ProteinDigester digester = new ProteinDigester.Builder(Protease.PEPSINE_PH_1_3).build();
        List<Peptide> digests = digester.digest(protein);

        Assert.assertEquals(16, digests.size());
        Assert.assertEquals(Peptide.parse("MTNHQ"), digests.get(0));
        Assert.assertEquals(Peptide.parse("LSTTEWNDET"), digests.get(1));
        Assert.assertEquals(Peptide.parse("L"), digests.get(2));
        Assert.assertEquals(Peptide.parse("YQE"), digests.get(3));
        Assert.assertEquals(Peptide.parse("F"), digests.get(4));
        Assert.assertEquals(Peptide.parse("NG"), digests.get(5));
        Assert.assertEquals(Peptide.parse("L"), digests.get(6));
        Assert.assertEquals(Peptide.parse("KKMNPKL"), digests.get(7));
        Assert.assertEquals(Peptide.parse("KT"), digests.get(8));
        Assert.assertEquals(Peptide.parse("LL"), digests.get(9));
        Assert.assertEquals(Peptide.parse("AIGGWN"), digests.get(10));
        Assert.assertEquals(Peptide.parse("F"), digests.get(11));
        Assert.assertEquals(Peptide.parse("GTQK"), digests.get(12));
        Assert.assertEquals(Peptide.parse("F"), digests.get(13));
        Assert.assertEquals(Peptide.parse("TDMVATANNRQTF"), digests.get(14));
        Assert.assertEquals(Peptide.parse("V"), digests.get(15));
    }

    @Test
    public void testOrNTerm() throws Exception {

        ProteinDigester digester = new ProteinDigester.Builder(new CleavageSiteMatcher("ont{I}ont{I}L|K")).build();

        Assert.assertEquals(Arrays.asList(Peptide.parse("AIIL"), Peptide.parse("K")), digester.digest(new Protein("P1", "AIILK")));
        Assert.assertEquals(Arrays.asList(Peptide.parse("IIL"), Peptide.parse("K")), digester.digest(new Protein("P1", "IILK")));
        Assert.assertEquals(Arrays.asList(Peptide.parse("IL"), Peptide.parse("K")), digester.digest(new Protein("P1", "ILK")));
        Assert.assertEquals(Arrays.asList(Peptide.parse("L"), Peptide.parse("K")), digester.digest(new Protein("P1", "LK")));
    }

    @Test
    public void testOrCTerm() throws Exception {

        ProteinDigester digester = new ProteinDigester.Builder(new CleavageSiteMatcher("L|Koct{K}oct{K}")).build();

        Assert.assertEquals(Arrays.asList(Peptide.parse("L"), Peptide.parse("KKKR")), digester.digest(new Protein("P1", "LKKKR")));
        Assert.assertEquals(Arrays.asList(Peptide.parse("L"), Peptide.parse("KKK")), digester.digest(new Protein("P1", "LKKK")));
        Assert.assertEquals(Arrays.asList(Peptide.parse("L"), Peptide.parse("KK")), digester.digest(new Protein("P1", "LKK")));
        Assert.assertEquals(Arrays.asList(Peptide.parse("L"), Peptide.parse("K")), digester.digest(new Protein("P1", "LK")));
    }

    private static class NTermFilter extends AcceptAllDigestionController {

        @Override
        public boolean retainDigest(Protein protein, Peptide digest, int digestIndex) {

            return digestIndex == 0;
        }
    }

    private static class CTermFilter extends AcceptAllDigestionController {

        @Override
        public boolean retainDigest(Protein protein, Peptide digest, int digestIndex) {

            return digestIndex == protein.size()-1;
        }
    }

    private static class ExactMcNTermFilter extends AcceptAllDigestionController {

        private final int mc;

        private ExactMcNTermFilter(int mc) {

            this.mc = mc;
        }

        @Override
        public boolean makeDigest(Protein protein, int inclusiveProteinLowerIndex, int exclusiveProteinUpperIndex, int missedCleavagesCount) {

            return missedCleavagesCount == mc;
        }

        @Override
        public boolean retainDigest(Protein protein, Peptide digest, int digestPos) {

            return digestPos == 0;
        }
    }

    private static class PositionAccu extends AcceptAllDigestionController {

        private final Multimap<Peptide, Integer> from = HashMultimap.create();
        private final Multimap<Peptide, Integer> to = HashMultimap.create();

        @Override
        public boolean retainDigest(Protein protein, Peptide digest, int digestIndex) {

            from.put(digest, digestIndex);
            to.put(digest, digestIndex+digest.size()-1);

            return true;
        }

        public List<Integer> getFrom(Peptide digest) {

            return Lists.newArrayList(from.get(digest));
        }

        public List<Integer> getTo(Peptide digest) {

            return Lists.newArrayList(to.get(digest));
        }
    }

    private static class EitherExactOrGreatestMc extends AcceptAllDigestionController {

        private final int mc;

        EitherExactOrGreatestMc(int mc) {

            this.mc = mc;
        }

        @Override
        public boolean makeDigest(Protein protein, int inclusiveProteinLowerIndex, int exclusiveProteinUpperIndex, int missedCleavagesCount) {

            return missedCleavagesCount == mc || exclusiveProteinUpperIndex-inclusiveProteinLowerIndex == protein.size();
        }

        @Override
        public boolean retainDigest(Protein protein, Peptide digest, int digestIndex) {

            return digestIndex == 0 && digest.size() == protein.size();

        }
    }
}
