package org.expasy.mzjava.proteomics.mol;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author fnikitin
 * Date: 12/2/13
 */
public class ProteinTest {

    @Test
    public void testFromArrayConstr() throws Exception {

        Protein prot = new Protein("ACC1", AminoAcid.A, AminoAcid.B, AminoAcid.C);

        Assert.assertEquals("ABC", prot.toSymbolString());
        Assert.assertEquals("ACC1", prot.getAccessionId());
    }

    @Test
    public void testFromArrayConstr2() throws Exception {

        Protein prot = new Protein("ACC1");

        Assert.assertEquals(0, prot.size());
        Assert.assertEquals("ACC1", prot.getAccessionId());
    }

    @Test
    public void testFromSeqStringConstr() throws Exception {

        Protein prot = new Protein("ACC1", "ABC");

        Assert.assertEquals("ABC", prot.toSymbolString());
        Assert.assertEquals("ACC1", prot.getAccessionId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFromSeqStringConstrIllegal() throws Exception {

        new Protein("ACC1", "ABC_");
    }

    @Test
    public void testGetPeptide() {

        Protein prot = new Protein("ACC1", AminoAcid.A, AminoAcid.B, AminoAcid.C);

        Peptide peptide = prot.getPeptideBuilder(1, 3).build();
        Assert.assertEquals(AminoAcid.B, peptide.getSymbol(0));
        Assert.assertEquals(AminoAcid.C, peptide.getSymbol(1));
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void testGetPeptideIllegalIndex() {

        Protein prot = new Protein("ACC1", AminoAcid.A, AminoAcid.B, AminoAcid.C);

        prot.getPeptideBuilder(0, 4);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testGetPeptideIllegalArgument() {

        Protein prot = new Protein("ACC1", AminoAcid.A, AminoAcid.B, AminoAcid.C);

        prot.getPeptideBuilder(2, 1);
    }

    @Test
    public void testBuilder() throws Exception {

        Protein.Builder builder = new Protein.Builder(2);
        builder.setAccessionId("ACC1");

        builder.appendSequence("PEPT");
        builder.appendSequence("PEPT");

        Protein protein = builder.build();

        Assert.assertEquals("PEPTPEPT", protein.toSymbolString());
    }

    @Test
    public void testBuilderStringConstructor() throws Exception {

        Protein.Builder builder = new Protein.Builder("PEPT");
        builder.setAccessionId("ACC1");

        builder.appendSequence("PEPT");

        Protein protein = builder.build();

        Assert.assertEquals("ACC1", protein.getAccessionId());
        Assert.assertEquals("PEPTPEPT", protein.toSymbolString());

        builder.reset();

        builder.setAccessionId("ACC2");
        builder.setSequence("CE");
        builder.appendSequence("RVILAS");

        Protein protein2 = builder.build();
        Assert.assertEquals("ACC2", protein2.getAccessionId());
        Assert.assertEquals("CERVILAS", protein2.toSymbolString());
    }
}
