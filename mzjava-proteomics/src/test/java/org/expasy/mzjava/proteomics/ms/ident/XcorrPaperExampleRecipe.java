/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.ms.ident;

import org.expasy.mzjava.core.io.ms.spectrum.MgfReader;
import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.PpmTolerance;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessorChain;
import org.expasy.mzjava.core.ms.peaklist.peakfilter.BinnedSpectrumFilter;
import org.expasy.mzjava.core.ms.peaklist.peakfilter.NPeaksFilter;
import org.expasy.mzjava.core.ms.peaklist.peaktransformer.ContrastEnhancingTransformer;
import org.expasy.mzjava.core.ms.peaklist.peaktransformer.HighestPeakPerBinNormalizer;
import org.expasy.mzjava.core.ms.peaklist.peaktransformer.SqrtTransformer;
import org.expasy.mzjava.core.ms.spectrasim.DpSimFunc;
import org.expasy.mzjava.core.ms.spectrasim.SimFunc;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.proteomics.io.ms.ident.PepXmlWriter;
import org.expasy.mzjava.proteomics.io.ms.ident.PepXmlWriterBuilder;
import org.expasy.mzjava.proteomics.io.ms.ident.pepxml.v117.EngineType;
import org.expasy.mzjava.proteomics.mol.digest.Protease;
import org.expasy.mzjava.proteomics.ms.dbsearch.PeptideSpectrumDB;
import org.expasy.mzjava.proteomics.ms.spectrum.PepFragAnnotation;
import org.expasy.mzjava.proteomics.ms.spectrum.PeptideSpectrum;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.expasy.mzjava.core.ms.peaklist.PeakList.Precision;
import static org.expasy.mzjava.core.ms.peaklist.peakfilter.BinnedSpectrumFilter.IntensityMode;

/**
 * @author Oliver Horlacher
 * @author Markus Muller
 * @version 1.0.0
 */
// # $RECIPE$ $NUMBER$ - SEQUEST database search #
//
// ## $PROBLEM$ ##
// You want to use MzJava to calculate the xcorr
//
// ## $SOLUTION$ ##
public class XcorrPaperExampleRecipe {
    //<SNIP>
    public static void main(String[] args) throws Exception {

        // Create a database to store the digested peptides and cache theoretical spectra.
        PeptideSpectrumDB peptideSpectrumDB = PeptideSpectrumDB.newBuilder()
                .setPrecursorTolerance(new PpmTolerance(10))
                .setProteinSource(new File("C:\\proteins.fasta")).digestWith(Protease.TRYPSIN).retainPeptidesOfLength(6, 60)
                .setMissedCleavagesTo(2).generateSequestCidSpectra(Precision.DOUBLE).build();

        // Create an mgf reader that processes the read spectra using a peak processor
        // that processes experimental spectra so that the xcorr score can be calculated
        MgfReader reader = new MgfReader(new File("C:\\spectra.mgf"), Precision.FLOAT, new PeakProcessorChain<>()
                .add(new NPeaksFilter<>(200))
                .add(new SqrtTransformer<>())
                .add(new HighestPeakPerBinNormalizer<>(200, 50.0))
                .add(new BinnedSpectrumFilter<>(100.5, 2000.5, 1.0005, IntensityMode.HIGHEST))
                .add(new ContrastEnhancingTransformer<>(75.0)));

        // Create a similarity function for calculating the peptide spectrum match score
        SimFunc<PepFragAnnotation, PeakAnnotation> simFunc = new DpSimFunc<>(2, new AbsoluteTolerance(0.6));

        // Initialize the results writer, step through all the spectra in the file and calculate the xcorr score
        PepXmlWriter resultsWriter = PepXmlWriterBuilder.create(EngineType.CUSTOM, "my_xcorr").build();
        while (reader.hasNext()) {

            MsnSpectrum querySpectrum = reader.next();
            List<PeptideMatch> peptideMatchList = new ArrayList<>();
            for (PeptideSpectrum theoreticalSpectrum : peptideSpectrumDB.getSpectra(querySpectrum.getPrecursor())) {

                double score = 4.890205e-05 * simFunc.calcSimilarity(theoreticalSpectrum, querySpectrum) + 0.874;
                peptideMatchList.add(new PeptideMatch(theoreticalSpectrum, "my_xcorr", score));
            }
            resultsWriter.add(querySpectrum, peptideMatchList);
        }
        reader.close();
        resultsWriter.write(new File("C:\\results.pep.xml"));
    }
    //</SNIP>
}
