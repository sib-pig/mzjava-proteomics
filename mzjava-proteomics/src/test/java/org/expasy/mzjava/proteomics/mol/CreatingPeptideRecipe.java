package org.expasy.mzjava.proteomics.mol;

import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.proteomics.mol.modification.ModificationList;
import org.junit.Assert;
import org.junit.Test;

import java.util.EnumSet;
import java.util.Set;

// # $RECIPE$ $NUMBER$ - Creating Peptide #
//
// ## $PROBLEM$ ##
// You want to create an instance of *Peptide*.
//
// ## $SOLUTION$ ##
//
public class CreatingPeptideRecipe {

// Peptides can be created from a *String*.
    @Test
    public void constr1() {

        //<SNIP>
        Peptide peptide = Peptide.parse("PEPTIDE");
        //</SNIP>

        Assert.assertEquals(7, peptide.size());
    }

// Peptides with modifications can be created from a *String*.
// See [Peptide.parse(String seq)](http://mzjava.expasy.org/apidocs/org/expasy/mzjava/core/mol/polymer/pept/Peptide.html#parse(java.lang.String)).
    @Test
    public void constr1WithModifs() {

        //<SNIP>
        // Adding 2 modifications on Methionine 5 of peptide sequence PEPTMIDE
        Peptide peptide = Peptide.parse("PEPTM(O, Phospho)IDE");

        ModificationList mods = peptide.getModificationsAt(4, ModAttachment.sideChainSet);

        Assert.assertEquals(2, mods.size());
        Assert.assertEquals("O", mods.get(0).toString());
        Assert.assertEquals("Phospho", mods.get(1).getLabel());
        //</SNIP>

        Assert.assertEquals(8, peptide.size());
    }

// Peptides can be created from a list or array of *AminoAcid*s.
    @Test
    public void constr2() {

        //<SNIP>
        // vararg AminoAcids
        Peptide peptide = new Peptide(AminoAcid.P, AminoAcid.E, AminoAcid.P, AminoAcid.T, AminoAcid.I, AminoAcid.D, AminoAcid.E);
        //</SNIP>

        Assert.assertEquals(7, peptide.size());
    }

// Peptides can also be created from the *PeptideBuilder* class.
    @Test
    public void constr3() {

        //<SNIP>
        PeptideBuilder peptideBuilder = new PeptideBuilder("PEPTIDE");

        //To add a phosphorylation to the T at position 3
        peptideBuilder.addModification(3, Modification.parseModification("HPO3"));

        // the defined modification is added to the N-terminal of the first amino-acid
        peptideBuilder.addModification(ModAttachment.N_TERM, Modification.parseModification("C2H3O"));

        Peptide peptide = peptideBuilder.build();
        //</SNIP>

        Assert.assertEquals(7, peptide.size());
        Assert.assertTrue(peptide.hasModificationAt(3));
    }

// Peptide methods for handling *Peptide* sequence.
    @Test
    public void handlingSequence() {

        Peptide peptide = new PeptideBuilder("PEPTIDE").build();

        //<SNIP>
        AminoAcid aa = peptide.getSymbol(1);
        Assert.assertEquals(AminoAcid.E, aa);

        int count = peptide.countAminoAcidsIn(EnumSet.of(AminoAcid.E, AminoAcid.I));
        Assert.assertEquals(3, count);

        int size = peptide.size();
        Assert.assertEquals(7, size);

        Peptide subPeptide = peptide.subSequence(1, 4);
        Assert.assertEquals(Peptide.parse("EPT"), subPeptide);

        int[] indices = peptide.getSymbolIndexes(AminoAcid.P);
        Assert.assertArrayEquals(new int[]{0, 2}, indices);

        Set<AminoAcid> ambiguousAas = Peptide.parse("PBPTIBB").getAmbiguousAminoAcids();
        Assert.assertTrue(!ambiguousAas.isEmpty());
        Assert.assertTrue(ambiguousAas.contains(AminoAcid.B));
        //</SNIP>
    }

// Peptide methods for getting *Peptide*  mass and m/z.
    @Test
    public void getMass() {

        Peptide peptide = new PeptideBuilder("PEPTIDE").build();

        //<SNIP>
        AminoAcid aa = peptide.getSymbol(1);
        Assert.assertEquals(AminoAcid.E, aa);

        double mass = peptide.getMolecularMass();
        Assert.assertEquals(799.359, mass, 0.001);

        // calculate m/z at charge +2
        double mz = peptide.calculateMz(2);
        Assert.assertEquals(400.687, mz, 0.001);
        //</SNIP>

    }

// ## $DISCUSSION$ ##

// ## $RELATED$ ##
}