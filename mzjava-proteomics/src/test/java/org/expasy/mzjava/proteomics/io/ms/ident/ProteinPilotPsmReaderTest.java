package org.expasy.mzjava.proteomics.io.ms.ident;

import com.google.common.base.Optional;
import org.expasy.mzjava.core.mol.AtomicSymbol;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.proteomics.ms.ident.ModificationMatch;
import org.expasy.mzjava.proteomics.ms.ident.PeptideMatch;
import org.expasy.mzjava.proteomics.ms.ident.SpectrumIdentifier;
import org.expasy.mzjava.core.ms.spectrum.TimeUnit;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.File;
import java.net.URL;
import java.sql.ResultSet;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;

/**
 * @author fnikitin
 * Date: 4/9/13
 */
public class ProteinPilotPsmReaderTest {

    @Test
    public void testParseAll() throws Exception {

        ProteinPilotPsmReader reader = new ProteinPilotPsmReader();

        MockedUnknownModListener mockedPeptideMatchListener = new MockedUnknownModListener();

        reader.setUnknownModListener(mockedPeptideMatchListener);
        PSMReaderCallback callback = mock(PSMReaderCallback.class);

        URL resource = getClass().getResource("MS10GE_PeptideSummary.txt");
        File file = new File(resource.getFile());

        reader.parse(file, callback);

        Assert.assertEquals(0, mockedPeptideMatchListener.getCount());
        Mockito.verify(callback, times(28502)).resultRead(any(SpectrumIdentifier.class), any(PeptideMatch.class));
    }

    @Test (expected=IllegalStateException.class)
    public void testUnknownModPart() throws Exception {

        ProteinPilotPsmReader reader = new ProteinPilotPsmReader();

        PSMReaderCallback callback = mock(PSMReaderCallback.class);

        URL resource = getClass().getResource("MS10GE_part_PeptideSummary_unknown_mod.txt");
        File file = new File(resource.getFile());

        reader.parse(file, callback);
    }

    @Test
    public void testUnknownModPartWithListener() throws Exception {

        ProteinPilotPsmReader reader = new ProteinPilotPsmReader();

        MockedUnknownModListener mockedPeptideMatchListener = new MockedUnknownModListener();

        reader.setUnknownModListener(mockedPeptideMatchListener);
        PSMReaderCallback callback = mock(PSMReaderCallback.class);

        URL resource = getClass().getResource("MS10GE_part_PeptideSummary_unknown_mod.txt");
        File file = new File(resource.getFile());

        reader.parse(file, callback);

        Assert.assertEquals(1, mockedPeptideMatchListener.getCount());
        Mockito.verify(callback, times(0)).resultRead(any(SpectrumIdentifier.class), any(PeptideMatch.class));
    }

    @Test
    public void testParseFirstIdentification() throws Exception {

        ProteinPilotPsmReader reader = new ProteinPilotPsmReader();

        PSMReaderCallback callback = mock(PSMReaderCallback.class);

        reader.parse(new File(getClass().getResource("MS10GE_part_PeptideSummary.txt").getFile()), callback);

        SpectrumIdentifier expectedIdentifier = new SpectrumIdentifier("1.1.1.4285.2");
        expectedIdentifier.setAssumedCharge(2);
        expectedIdentifier.addRetentionTime(21.1751, TimeUnit.MINUTE);
        expectedIdentifier.setPrecursorNeutralMass(900.4577026);

        PeptideMatch expectedPeptideMatch = new PeptideMatch("ADIDVSGPK");
        expectedPeptideMatch.setRank(Optional.fromNullable(0));
        Optional<Integer> absentVal= Optional.absent();
        expectedPeptideMatch.setNumMissedCleavages(absentVal);
        expectedPeptideMatch.setMassDiff(Optional.fromNullable(0.00235074));
        expectedPeptideMatch.addScore("score", 14);

        Mockito.verify(callback, times(1)).resultRead(expectedIdentifier, expectedPeptideMatch);
    }

    @Test
    public void testMakePeptide() throws Exception {

        ProteinPilotPsmReader reader = new ProteinPilotPsmReader();

        ResultSet results = mock(ResultSet.class);
        Mockito.when(results.getString("Sequence")).thenReturn("LEGDLTGPSVDVEVPDVELECPDAK");
        Mockito.when(results.getString("Modifications")).thenReturn("Asp->Gly@11; Methylthio(C)@21");
        Optional<PeptideMatch> actualPeptideMatch = reader.makePeptideMatch(results);

        PeptideMatch expectedPeptideMatch = new PeptideMatch("LEGDLTGPSVDVEVPDVELECPDAK");
        Composition.Builder builder = new Composition.Builder(AtomicSymbol.C, -2).add(AtomicSymbol.H, -2).add(AtomicSymbol.O, -2);
        expectedPeptideMatch.addModificationMatch(10, new ModificationMatch(new Modification("Asp->Gly", builder.build()), AminoAcid.A, 12, ModAttachment.SIDE_CHAIN));
        expectedPeptideMatch.addModificationMatch(20, new ModificationMatch(new Modification("Methylthio", new Composition.Builder(AtomicSymbol.C).add(AtomicSymbol.H, 2).add(AtomicSymbol.S).build()), AminoAcid.M, 12, ModAttachment.SIDE_CHAIN));

        Assert.assertEquals(expectedPeptideMatch, actualPeptideMatch.get());
    }

    private static class MockedUnknownModListener implements UnknownModListener {

        private int count;

        @Override
        public void handleUnknownMod(String pepSeq, String mod, int lineNumber) {
            System.out.println("okokok");
            count++;
        }

        public int getCount() {

            return count;
        }
    }
}
