/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.mol;

import com.google.common.base.Optional;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.proteomics.mol.modification.ModificationResolver;
import org.expasy.mzjava.core.ms.spectrum.FragmentType;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.junit.Assert;
import org.junit.Test;

import java.util.EnumSet;

import static org.expasy.mzjava.proteomics.mol.AminoAcid.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class PeptideFragmentTest {

    @Test
    public void testForward() throws Exception {

        PeptideFragment fragment = PeptideFragment.parse("C", FragmentType.FORWARD);

        Assert.assertEquals(52.5118683512, fragment.calculateMz(IonType.b, 2), 0.000000001);
    }

    @Test
    public void testReverse() throws Exception {

        PeptideFragment fragment = PeptideFragment.parse("AS", FragmentType.REVERSE);
        Assert.assertEquals(89.04712954869996, fragment.calculateMz(IonType.y, 2), 0.000000001);
    }

    @Test
    public void testParseWithReslover() throws Exception {

        Modification modification = Modification.parseModification("p:HPO3");

        ModificationResolver modResolver = mock(ModificationResolver.class);
        when(modResolver.resolve("p")).thenReturn(Optional.of(modification));

        PeptideFragment fragment = PeptideFragment.parse("PEPS(p)IDE", FragmentType.FORWARD, modResolver);

        Assert.assertEquals(1, fragment.getModificationCount());
        Assert.assertEquals(modification, fragment.getModificationsAt(3, EnumSet.allOf(ModAttachment.class)).get(0));
        Assert.assertEquals(FragmentType.FORWARD, fragment.getFragmentType());
    }

    @Test
    public void testVarargConstructor() throws Exception {

        PeptideFragment frag = new PeptideFragment(FragmentType.FORWARD, C, E, R, V);

        Assert.assertEquals(FragmentType.FORWARD, frag.getFragmentType());
        Assert.assertEquals(4, frag.size());
        Assert.assertEquals(C, frag.getSymbol(0));
        Assert.assertEquals(E, frag.getSymbol(1));
        Assert.assertEquals(R, frag.getSymbol(2));
        Assert.assertEquals(V, frag.getSymbol(3));
    }

    @Test
    public void testSubSeqConstructorForward() throws Exception {

        Peptide peptide = Peptide.parse("CERVILAS");
        PeptideFragment frag = new PeptideFragment(FragmentType.FORWARD, peptide, 0, 3);

        Assert.assertEquals(FragmentType.FORWARD, frag.getFragmentType());
        Assert.assertEquals(3, frag.size());
        Assert.assertEquals(C, frag.getSymbol(0));
        Assert.assertEquals(E, frag.getSymbol(1));
        Assert.assertEquals(R, frag.getSymbol(2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSubSeqConstructorForwardIllegalIndex() throws Exception {

        Peptide peptide = Peptide.parse("CERVILAS");
        new PeptideFragment(FragmentType.FORWARD, peptide, 2, 3);
    }

    @Test
    public void testSubSeqConstructorReverse() throws Exception {

        Peptide peptide = Peptide.parse("CERVILAS");
        PeptideFragment frag = new PeptideFragment(FragmentType.REVERSE, peptide, 5, 8);

        Assert.assertEquals(FragmentType.REVERSE, frag.getFragmentType());
        Assert.assertEquals(3, frag.size());
        Assert.assertEquals(L, frag.getSymbol(0));
        Assert.assertEquals(A, frag.getSymbol(1));
        Assert.assertEquals(S, frag.getSymbol(2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSubSeqConstructorReverseIllegalIndex() throws Exception {

        Peptide peptide = Peptide.parse("CERVILAS");
        new PeptideFragment(FragmentType.REVERSE, peptide, 5, 7);
    }

    @Test
    public void testSubSeqConstructorMonomer() throws Exception {

        Peptide peptide = Peptide.parse("CERVILAS");
        PeptideFragment frag = new PeptideFragment(FragmentType.MONOMER, peptide, 3, 4);

        Assert.assertEquals(FragmentType.MONOMER, frag.getFragmentType());
        Assert.assertEquals(1, frag.size());
        Assert.assertEquals(V, frag.getSymbol(0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSubSeqConstructorMonomerIllegalIndex() throws Exception {

        Peptide peptide = Peptide.parse("CERVILAS");
        new PeptideFragment(FragmentType.MONOMER, peptide, 3, 5);
    }

    @Test
    public void testSubSeqConstructorInternal() throws Exception {

        Peptide peptide = Peptide.parse("CERVILAS");
        PeptideFragment frag = new PeptideFragment(FragmentType.INTERNAL, peptide, 1, 7);

        Assert.assertEquals(FragmentType.INTERNAL, frag.getFragmentType());
        Assert.assertEquals(6, frag.size());
        Assert.assertEquals(E, frag.getSymbol(0));
        Assert.assertEquals(R, frag.getSymbol(1));
        Assert.assertEquals(V, frag.getSymbol(2));
        Assert.assertEquals(I, frag.getSymbol(3));
        Assert.assertEquals(L, frag.getSymbol(4));
        Assert.assertEquals(A, frag.getSymbol(5));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSubSeqConstructorUnknown() throws Exception {

        Peptide peptide = Peptide.parse("CERVILAS");
        new PeptideFragment(FragmentType.UNKNOWN, peptide, 3, 6);
    }

    @Test
    public void testEquals() throws Exception {

        PeptideFragment same1 = new PeptideFragment(FragmentType.FORWARD, C, E, R, V);
        PeptideFragment same2 = new PeptideFragment(FragmentType.FORWARD, C, E, R, V);
        PeptideFragment different = new PeptideFragment(FragmentType.REVERSE, C, E, R, V);

        Assert.assertEquals(true, same1.equals(same2));
        Assert.assertEquals(true, same2.equals(same1));
        Assert.assertEquals(false, different.equals(same1));
        Assert.assertEquals(false, same2.equals(different));
    }

    @Test
    public void testHashCode() throws Exception {

        PeptideFragment same1 = new PeptideFragment(FragmentType.FORWARD, C, E, R, V);
        PeptideFragment same2 = new PeptideFragment(FragmentType.FORWARD, C, E, R, V);
        PeptideFragment different = new PeptideFragment(FragmentType.REVERSE, C, E, R, V);

        Assert.assertEquals(same1.hashCode(), same2.hashCode());
        Assert.assertNotEquals(different.hashCode(), same1.hashCode());
    }
}
