package org.expasy.mzjava.proteomics.ms.spectrum;

import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.core.ms.peaklist.FloatPeakList;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.proteomics.ms.spectrum.PepLibPeakAnnotation;
import org.expasy.mzjava.proteomics.ms.spectrum.PepFragAnnotation;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

// # $RECIPE$ $NUMBER$ - Creating PeakAnnotation #

// ## $PROBLEM$ ##
// You want to make a new instance of *PeakAnnotation*.

// ## $SOLUTION$ ##

// A *PeakAnnotation* is associated to *PeakList* peaks. It gives meta information about a given peak.

public class CreatingPeakAnnotationRecipe {

// We have actually two kind of implementations:
// *PepFragAnnotation* gives identification information about the associated theoretical fragment
    @Test
    public void fragmentAnnotation() {

        //<SNIP>
        Peptide peptideFragment = Peptide.parse("CER");
        int charge = 2;

        new PepFragAnnotation(IonType.b, charge, peptideFragment);
        //</SNIP>
    }

// *LibraryPeakAnnotation* stores statistical information about the consensus peaks
    @Test
    public void libraryPeakAnnotation() {

        //<SNIP>
        int mergedPeakCount = 10;
        double mzStd = 0.05;
        double intensityStd = 0.12;

        new PepLibPeakAnnotation(mergedPeakCount, mzStd, intensityStd);
        //</SNIP>
    }

// Then *PeakAnnotation*s is added to *PeakList* at specific peak index
    @Test
    public void addAnnotationAtIndex() {

        PepFragAnnotation annotation = new PepFragAnnotation(IonType.b, 2, Peptide.parse("CER"));

        //<SNIP>
        PeakList<PepFragAnnotation> peaklist = new FloatPeakList<PepFragAnnotation>();

        peaklist.add(200, 100, Arrays.asList(annotation));
        //</SNIP>

        Assert.assertEquals(1, peaklist.size());
        Assert.assertTrue(peaklist.hasAnnotationsAt(0));
    }

// ## $DISCUSSION$ ##

// ## $RELATED$ ##

// For more information about peaks, see $CreatingPeakListRecipe$
}