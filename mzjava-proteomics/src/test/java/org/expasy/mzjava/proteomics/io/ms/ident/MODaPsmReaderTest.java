package org.expasy.mzjava.proteomics.io.ms.ident;

import com.google.common.base.Optional;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.expasy.mzjava.core.mol.NumericMass;
import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.proteomics.ms.ident.ModificationMatch;
import org.expasy.mzjava.proteomics.ms.ident.ModificationMatchResolver;
import org.expasy.mzjava.proteomics.ms.ident.PeptideMatch;
import org.expasy.mzjava.proteomics.ms.ident.SpectrumIdentifier;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class MODaPsmReaderTest {

    private final Multimap<String, PeptideMatch> searchResultMap = ArrayListMultimap.create();

    PSMReaderCallback insertIdResultCB = new PSMReaderCallback() {
        @Override
        public void resultRead(SpectrumIdentifier identifier, PeptideMatch searchResult) {

            searchResultMap.put(identifier.getSpectrum(), searchResult);
        }
    };

    private final ModificationMatchResolver modificationMatchResolver = new ModificationMatchResolver() {
        @Override
        public Optional<Modification> resolve(ModificationMatch modMatch) {

            if (modMatch == null) return Optional.absent();

            String modName = String.format("%.3f",modMatch.getMassShift() );
            return Optional.of(new Modification(modName,new NumericMass(modMatch.getMassShift())));
        }
    };


    @Test
    public void testGetTabValues() {
        String line = ">>>>>+kljef  kjdbvij dq jb *1\t2\t\t4\tx ?%&2835";

        MODaPsmReader reader = new MODaPsmReader();

        String[] fields = reader.getTabValues(line);

        Assert.assertEquals(fields[0], ">>>>>+kljef  kjdbvij dq jb *1");
        Assert.assertEquals(fields[1], "2");
        Assert.assertEquals(fields[2], "");
        Assert.assertEquals(fields[3], "4");
        Assert.assertEquals(fields[4], "x ?%&2835");

        line = "\t>>>>>+kljef  kjdbvij dq jb *1\t2\t\t4\tx ?%&2835";

        fields = reader.getTabValues(line);

        Assert.assertEquals(fields.length,6);
        Assert.assertEquals(fields[0], "");
    }


    @Test
    public void testReadSpectrumInfo() {

        String line = ">>../query.mgf\t1\t760.3974\t2\t2831";

        MODaPsmReader reader = new MODaPsmReader();
        MODaPsmReader.SpectrumInfoParser parser = reader.new SpectrumInfoParser();

        SpectrumIdentifier identifier = parser.parse(line);

        Assert.assertEquals(identifier.getAssumedCharge().get().intValue(), 2);
        Assert.assertEquals(identifier.getIndex().get().intValue(), 1);
        Assert.assertEquals(identifier.getScanNumbers().getFirst().getValue(), 2831);
        Assert.assertEquals(identifier.getPrecursorNeutralMass().get(), 760.3974, 0.0000001);
        Assert.assertEquals(identifier.getSpectrumFile().get(), "../query.mgf");


        line = ">>../query.mgf\t1\t760.3974\t";

        boolean caught = false;
        try {
            parser.parse(line);
        } catch(RuntimeException e) {
            caught = true;
        }

        Assert.assertTrue(caught);

        line = ">>../query.mgf\t1\t760.3974";

        caught = false;
        try {
            parser.parse(line);
        } catch(RuntimeException e) {
            caught = true;
        }

        Assert.assertTrue(caught);
    }


    @Test
    public void testReadPSM() {

        String line = "760.3902\t0.0073\t8\t0.0553\tR.KEMTPR.Q\tsp|Q5T5P2\t1420~1425";

        MODaPsmReader reader = new MODaPsmReader();
        SpectrumIdentifier identifier = new SpectrumIdentifier("");
        identifier.setPrecursorNeutralMass(760.3974);
        identifier.setAssumedCharge(2);

        MODaPsmReader.PSMInfoParser psmInfoParser = reader.new PSMInfoParser();
        PeptideMatch peptideMatch = psmInfoParser.parse(line,identifier);


        Assert.assertEquals(peptideMatch.getNeutralPeptideMass(),760.3902,0.0000001);
        Assert.assertEquals(peptideMatch.getMassDiff().get(),760.3974-peptideMatch.toPeptide().getMolecularMass(),0.0000001);
        Assert.assertEquals(peptideMatch.getProteinMatches().size(),1);
        Assert.assertEquals(peptideMatch.getProteinMatches().get(0).getAccession(),"sp|Q5T5P2");
        Assert.assertEquals(peptideMatch.getProteinMatches().get(0).getStart(),1420);
        Assert.assertEquals(peptideMatch.getProteinMatches().get(0).getEnd(),1425);
        Assert.assertEquals(peptideMatch.getProteinMatches().get(0).getPreviousAA().get(),"R");
        Assert.assertEquals(peptideMatch.getProteinMatches().get(0).getNextAA().get(),"Q");
        Assert.assertEquals(peptideMatch.getScoreMap().size(),3);
        Assert.assertEquals(peptideMatch.getScoreMap().get("MODaScore"),8,0.0000001);
        Assert.assertEquals(peptideMatch.getScoreMap().get("MODaProbability"),0.0553,0.0000001);
        Assert.assertEquals(peptideMatch.getScoreMap().get("charge"),2,0.000001);
        Assert.assertEquals(peptideMatch.getModificationCount(), 0);
        Assert.assertEquals(peptideMatch.toPeptide().toString(), "KEMTPR");

        identifier.setPrecursorNeutralMass(819.4594);
        identifier.setAssumedCharge(2);
        line = "819.4513\t0.0082\t17\t0.8703\tK.Q-17ALAHGLK.C\tsp|Q99798\t402~409";

        peptideMatch = psmInfoParser.parse(line,identifier);

        Assert.assertEquals(peptideMatch.getNeutralPeptideMass(),819.4513,0.0000001);
        Assert.assertEquals(peptideMatch.getProteinMatches().size(),1);
        Assert.assertEquals(peptideMatch.getProteinMatches().get(0).getAccession(),"sp|Q99798");
        Assert.assertEquals(peptideMatch.getProteinMatches().get(0).getStart(),402);
        Assert.assertEquals(peptideMatch.getProteinMatches().get(0).getEnd(),409);
        Assert.assertEquals(peptideMatch.getProteinMatches().get(0).getPreviousAA().get(),"K");
        Assert.assertEquals(peptideMatch.getProteinMatches().get(0).getNextAA().get(),"C");
        Assert.assertEquals(peptideMatch.getScoreMap().size(),3);
        Assert.assertEquals(peptideMatch.getScoreMap().get("MODaScore"),17,0.0000001);
        Assert.assertEquals(peptideMatch.getScoreMap().get("MODaProbability"),0.8703,0.0000001);
        Assert.assertEquals(peptideMatch.getScoreMap().get("charge"),2,0.000001);
        Assert.assertEquals(peptideMatch.getModificationCount(),1);
        Assert.assertEquals(peptideMatch.toPeptide(modificationMatchResolver).toString(), "Q(-17.027)ALAHGLK");

        identifier.setPrecursorNeutralMass(819.4594);
        identifier.setAssumedCharge(2);
        line = "819.4513\t0.0082\t17\t0.8703\tK.Q-17ALAHGLK.C\tsp|Q99798\t402";

        boolean caught = false;
        try {
            psmInfoParser.parse(line, identifier);
        } catch(RuntimeException e) {
            caught = true;
        }

        Assert.assertTrue(caught);

        identifier.setPrecursorNeutralMass(819.4594);
        identifier.setAssumedCharge(2);
        line = "819.4513\t0.0082\t17\t0.8703\tK.Q-17ALAHGLK.C\tsp|Q99798";

        caught = false;
        try {
            psmInfoParser.parse(line, identifier);
        } catch(RuntimeException e) {
            caught = true;
        }

        Assert.assertTrue(caught);


    }

    @Test
    public void testParsePeptideInfo() {

        MODaPsmReader reader = new MODaPsmReader();

        String peptInfo = "KEM+16AEAR";

        MODaPsmReader.PeptideInfoParser parser = reader.new PeptideInfoParser();
        parser.parse(peptInfo);

        PeptideMatch peptideMatch = new PeptideMatch(parser.getSequence());
        parser.addModifs(peptideMatch, 16.0);


        Assert.assertEquals(7, parser.getSequence().size());
        Assert.assertEquals(AminoAcid.K, parser.getSequence().get(0));
        Assert.assertEquals(AminoAcid.E, parser.getSequence().get(1));
        Assert.assertEquals(AminoAcid.M, parser.getSequence().get(2));
        Assert.assertEquals(AminoAcid.A, parser.getSequence().get(3));
        Assert.assertEquals(AminoAcid.E, parser.getSequence().get(4));
        Assert.assertEquals(AminoAcid.A, parser.getSequence().get(5));
        Assert.assertEquals(AminoAcid.R, parser.getSequence().get(6));
        Assert.assertEquals(1,peptideMatch.getModificationCount());
        Assert.assertEquals(1,peptideMatch.getModifications(2, ModAttachment.sideChainSet).size());
        Assert.assertEquals(16.0,peptideMatch.getModifications(2,ModAttachment.sideChainSet).get(0).getMassShift(),0.0001);


        peptInfo = "ESDD-18EVEK";

        parser.parse(peptInfo);
        peptideMatch = new PeptideMatch(parser.getSequence());
        parser.addModifs(peptideMatch, -18.0);

        Assert.assertEquals(8,parser.getSequence().size());
        Assert.assertEquals(AminoAcid.E, parser.getSequence().get(0));
        Assert.assertEquals(AminoAcid.S, parser.getSequence().get(1));
        Assert.assertEquals(AminoAcid.D, parser.getSequence().get(2));
        Assert.assertEquals(AminoAcid.D, parser.getSequence().get(3));
        Assert.assertEquals(AminoAcid.E, parser.getSequence().get(4));
        Assert.assertEquals(AminoAcid.V, parser.getSequence().get(5));
        Assert.assertEquals(AminoAcid.E, parser.getSequence().get(6));
        Assert.assertEquals(AminoAcid.K, parser.getSequence().get(7));
        Assert.assertEquals(1,peptideMatch.getModificationCount());
        Assert.assertEquals(1,peptideMatch.getModifications(3, ModAttachment.sideChainSet).size());
        Assert.assertEquals(-18.0,peptideMatch.getModifications(3,ModAttachment.sideChainSet).get(0).getMassShift(),0.0001);

        peptInfo = "Q-17ESDD-18EVEK+14";

        parser.parse(peptInfo);
        peptideMatch = new PeptideMatch(parser.getSequence());
        parser.addModifs(peptideMatch, -21.0);

        Assert.assertEquals(9,parser.getSequence().size());
        Assert.assertEquals(AminoAcid.Q, parser.getSequence().get(0) );
        Assert.assertEquals(AminoAcid.E, parser.getSequence().get(1));
        Assert.assertEquals(AminoAcid.S, parser.getSequence().get(2));
        Assert.assertEquals(AminoAcid.D, parser.getSequence().get(3));
        Assert.assertEquals(AminoAcid.D, parser.getSequence().get(4));
        Assert.assertEquals(AminoAcid.E, parser.getSequence().get(5));
        Assert.assertEquals(AminoAcid.V, parser.getSequence().get(6));
        Assert.assertEquals(AminoAcid.E, parser.getSequence().get(7));
        Assert.assertEquals(AminoAcid.K, parser.getSequence().get(8));
        Assert.assertEquals(3,peptideMatch.getModificationCount());
        Assert.assertEquals(1,peptideMatch.getModifications(0, ModAttachment.sideChainSet).size());
        Assert.assertEquals(1,peptideMatch.getModifications(4, ModAttachment.sideChainSet).size());
        Assert.assertEquals(1,peptideMatch.getModifications(8, ModAttachment.sideChainSet).size());
        Assert.assertEquals(-17.0,peptideMatch.getModifications(0,ModAttachment.sideChainSet).get(0).getMassShift(),0.0001);
        Assert.assertEquals(-18.0,peptideMatch.getModifications(4,ModAttachment.sideChainSet).get(0).getMassShift(),0.0001);
        Assert.assertEquals(14.0,peptideMatch.getModifications(8,ModAttachment.sideChainSet).get(0).getMassShift(),0.0001);

    }

    @Test
    public void testMultipleModifs(){
        String line = "1977.8880	1.3708	42	0.2759	R.YPIEH+16GIVTNW+14DDMEK+2.T	TEST_PROTEIN	1050~1065";
        MODaPsmReader reader = new MODaPsmReader();

        SpectrumIdentifier identifier = new SpectrumIdentifier("");
        identifier.setPrecursorNeutralMass(1979.2588);
        identifier.setAssumedCharge(2);

        MODaPsmReader.PSMInfoParser psmInfoParser = reader.new PSMInfoParser();
        PeptideMatch peptideMatch = psmInfoParser.parse(line,identifier);

        Assert.assertEquals(1977.8880,peptideMatch.getNeutralPeptideMass(),0.00001);
        Assert.assertEquals(33.37078,peptideMatch.getMassDiff().get(),0.00001);
        Assert.assertEquals("YPIEHGIVTNWDDMEK",peptideMatch.toPeptide(modificationMatchResolver).toSymbolString());
        Assert.assertEquals(3,peptideMatch.getModificationCount());
        Assert.assertEquals(0, peptideMatch.getModifications(0, ModAttachment.termSet).size());
        Assert.assertEquals(0,peptideMatch.getModifications(0,ModAttachment.sideChainSet).size());
        Assert.assertEquals(1,peptideMatch.getModifications(4,ModAttachment.sideChainSet).size());
        Assert.assertEquals(16.0+1.3708/3,peptideMatch.getModifications(4,ModAttachment.sideChainSet).get(0).getMassShift(),0.00001);
        Assert.assertEquals(1,peptideMatch.getModifications(10, ModAttachment.sideChainSet).size());
        Assert.assertEquals(14.0+1.3708/3,peptideMatch.getModifications(10,ModAttachment.sideChainSet).get(0).getMassShift(),0.00001);
        Assert.assertEquals(1,peptideMatch.getModifications(15, ModAttachment.sideChainSet).size());
        Assert.assertEquals(2.0+1.3708/3,peptideMatch.getModifications(15,ModAttachment.sideChainSet).get(0).getMassShift(),0.00001);
    }

    @Test
    public void testParsePeptideInfoWithFixedModif() {

        List<AminoAcid> fixedModResidues = new ArrayList<>();
        fixedModResidues.add(AminoAcid.C);
        Multimap<Integer, Modification> sideChainFixedModMap = ArrayListMultimap.create();
        sideChainFixedModMap.put(0, Modification.parseModification("H3C2NO"));
        Multimap<ModAttachment, Modification> termFixedModMap = ArrayListMultimap.create();

        MODaPsmReader reader = new MODaPsmReader(fixedModResidues,sideChainFixedModMap,termFixedModMap);

        String line = "1611.6021\t0.0161\t21\t0.0539\tK.TCVADESAENCDK+114.S\tsp|P02768\t76~88";

        SpectrumIdentifier identifier = new SpectrumIdentifier("");
        identifier.setPrecursorNeutralMass(1611.6183);
        identifier.setAssumedCharge(2);

        MODaPsmReader.PSMInfoParser psmInfoParser = reader.new PSMInfoParser();
        PeptideMatch peptideMatch = psmInfoParser.parse(line, identifier);


        Assert.assertEquals(1611.6021,peptideMatch.getNeutralPeptideMass(),0.00001);
        Assert.assertEquals(114.04712780,peptideMatch.getMassDiff().get(),0.0000001);
        Assert.assertEquals("TCVADESAENCDK",peptideMatch.toPeptide(modificationMatchResolver).toSymbolString());
        Assert.assertEquals(3,peptideMatch.getModificationCount());
        Assert.assertEquals(0, peptideMatch.getModifications(0, ModAttachment.termSet).size());
        Assert.assertEquals(0,peptideMatch.getModifications(0,ModAttachment.sideChainSet).size());
        Assert.assertEquals(1,peptideMatch.getModifications(1,ModAttachment.sideChainSet).size());
        Assert.assertEquals(57.021464,peptideMatch.getModifications(1,ModAttachment.sideChainSet).get(0).getMassShift(),0.00001);
        Assert.assertEquals(1,peptideMatch.getModifications(10, ModAttachment.sideChainSet).size());
        Assert.assertEquals(57.021464,peptideMatch.getModifications(10,ModAttachment.sideChainSet).get(0).getMassShift(),0.00001);
        Assert.assertEquals(1,peptideMatch.getModifications(12, ModAttachment.sideChainSet).size());
        double massDiff = 1611.6183-2*57.021464- Peptide.parse("TCVADESAENCDK").getMolecularMass();
        Assert.assertEquals(massDiff,peptideMatch.getModifications(12,ModAttachment.sideChainSet).get(0).getMassShift(),0.00001);
    }

    @Test
    public void testParsePeptideInfoWithFixedModif2() {

        List<AminoAcid> fixedModResidues = new ArrayList<>();
        fixedModResidues.add(AminoAcid.C);
        Multimap<Integer, Modification> sideChainFixedModMap = ArrayListMultimap.create();
        sideChainFixedModMap.put(0, Modification.parseModification("H3C2NO"));
        Multimap<ModAttachment, Modification> termFixedModMap = ArrayListMultimap.create();
        termFixedModMap.put(ModAttachment.N_TERM, Modification.parseModification("H2C2O"));

        MODaPsmReader reader = new MODaPsmReader(fixedModResidues,sideChainFixedModMap,termFixedModMap);

        String line = "1653.612665\t0.0161\t21\t0.0539\tK.TCVADESAENCDK+114.S\tsp|P02768\t76~88";

        SpectrumIdentifier identifier = new SpectrumIdentifier("");
        identifier.setPrecursorNeutralMass(1611.6183+42.010565);
        identifier.setAssumedCharge(2);

        MODaPsmReader.PSMInfoParser psmInfoParser = reader.new PSMInfoParser();
        PeptideMatch peptideMatch = psmInfoParser.parse(line, identifier);


        Assert.assertEquals(1611.6021+42.010565,peptideMatch.getNeutralPeptideMass(),0.00001);
        Assert.assertEquals(114.04712780,peptideMatch.getMassDiff().get(),0.00001);
        Assert.assertEquals("TCVADESAENCDK",peptideMatch.toPeptide(modificationMatchResolver).toSymbolString());
        Assert.assertEquals(4,peptideMatch.getModificationCount());
        Assert.assertEquals(1,peptideMatch.getModifications(0,ModAttachment.termSet).size());
        Assert.assertEquals(42.010565,peptideMatch.getModifications(ModAttachment.termSet).get(0).getMassShift(),0.00001);
        Assert.assertEquals(0,peptideMatch.getModifications(0,ModAttachment.sideChainSet).size());
        Assert.assertEquals(1,peptideMatch.getModifications(1,ModAttachment.sideChainSet).size());
        Assert.assertEquals(57.021464,peptideMatch.getModifications(1,ModAttachment.sideChainSet).get(0).getMassShift(),0.00001);
        Assert.assertEquals(1,peptideMatch.getModifications(10, ModAttachment.sideChainSet).size());
        Assert.assertEquals(57.021464,peptideMatch.getModifications(10,ModAttachment.sideChainSet).get(0).getMassShift(),0.00001);
        Assert.assertEquals(1,peptideMatch.getModifications(12, ModAttachment.sideChainSet).size());
        double massDiff = 1611.6183-2*57.021464- Peptide.parse("TCVADESAENCDK").getMolecularMass();
        Assert.assertEquals(massDiff,peptideMatch.getModifications(12,ModAttachment.sideChainSet).get(0).getMassShift(),0.00001);
    }

    @Test
    public void test_parse(){
        File modaResultFile = new File(getClass().getResource("MODa_results.txt").getFile());

        MODaPsmReader reader = new MODaPsmReader();
        reader.parse(modaResultFile, insertIdResultCB);

        Assert.assertEquals(searchResultMap.size(),46);

        Collection<PeptideMatch> psms = searchResultMap.get("query.2831.2831.2");
        Assert.assertEquals(psms.size(),5);

        int cnt1 = 0;
        int cnt2 = 0;
        for (PeptideMatch psm : psms) {
            if (psm.getRank().get()==1) {
                Assert.assertEquals(psm.getScore("MODaScore"), 8, 0.000001);
                cnt1++;
            }
            if (psm.getRank().get()==2) {
                Assert.assertEquals(psm.getScore("MODaScore"), 5, 0.000001);
                cnt2++;
            }
        }

        Assert.assertEquals(cnt1,4);
        Assert.assertEquals(cnt2,1);

        psms = searchResultMap.get("query.2996.2996.2");
        Assert.assertEquals(psms.size(),1);

        cnt1 = 0;
        for (PeptideMatch psm : psms) {
            if (psm.getRank().get()==1) {
                Assert.assertEquals(psm.getScore("MODaScore"), 17, 0.000001);
                cnt1++;
            }
        }

        Assert.assertEquals(cnt1,1);

        psms = searchResultMap.get("query.5977.5977.2");
        Assert.assertEquals(psms.size(),5);

        cnt1 = 0;
        cnt2 = 0;
        int cnt3 = 0;
        int cnt4 = 0;
        for (PeptideMatch psm : psms) {
            if (psm.getRank().get()==1) {
                Assert.assertEquals(psm.getScore("MODaScore"), 18, 0.000001);
                cnt1++;
            }
            if (psm.getRank().get()==2) {
                Assert.assertEquals(psm.getScore("MODaScore"), 14, 0.000001);
                cnt2++;
            }
            if (psm.getRank().get()==3) {
                Assert.assertEquals(psm.getScore("MODaScore"), 13, 0.000001);
                cnt3++;
            }
            if (psm.getRank().get()==4) {
                Assert.assertEquals(psm.getScore("MODaScore"), 11, 0.000001);
                cnt4++;
            }
        }

        Assert.assertEquals(cnt1,1);
        Assert.assertEquals(cnt2,1);
        Assert.assertEquals(cnt3,1);
        Assert.assertEquals(cnt4,2);
    }
}