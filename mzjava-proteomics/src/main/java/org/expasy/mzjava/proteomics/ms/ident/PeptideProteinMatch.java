/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.ms.ident;

import com.google.common.base.Optional;
import org.expasy.mzjava.proteomics.mol.AminoAcid;

import java.util.HashSet;
import java.util.Set;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Class that stores the information where a peptide matches to a protein.
 * <p/>
 * Has fields for the protein accession, the previous amino acid and the next amino acid.
 * Has two fields which indicate the position of the peptide in the protein (start and end)
 * Peptides that are at the N-term or C-term have "-" as the previous or next amino acid.
 * "?" indicates that the amino acid is unknown
 * <p/>
 * The previous and next AA are wrapped in com.google.common.base.Optional so that it is
 * possible to differentiate between an unknown AA and when the information is not provided.
 *
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class PeptideProteinMatch {

    public enum HitType {TARGET, DECOY, UNKNOWN}

    private final HitType hitType;
    private final String accession;
    private final Optional<String> searchDatabase;
    private final Optional<String> previousAA;

    private final Optional<String> nextAA;
    private int start;
    private int end;

    private static final Set<String> allowedAaSymbols = new HashSet<>();

    static {

        for (AminoAcid aa : AminoAcid.values()) {

            allowedAaSymbols.add(aa.getSymbol());
        }
        allowedAaSymbols.add("-"); //to indicate start or end of protein
        allowedAaSymbols.add("?"); //to indicate an unknown AA, as described in the PeptideEvidenceType of MzIdentML
    }

    /**
     * Constructor, none of the parameters can be null.
     * <p/>
     * The previous and next AA are wrapped in com.google.common.base.Optional so that it is possible to differentiate
     * between a unknown AA and when the information is not available.
     *
     * @param accession the protein accession
     * @param searchDatabase the search database
     * @param previousAA the previous amino acid if the peptide is at the N-Term of the protein use "-", if the
     *                   previous amino acid is unknown use "?" and if the AA information is not given use Optional.absent
     * @param nextAA     the next amino acid if the peptide is at the C-Term of the protein use "-", if the
     *                   next amino acid is unknown use "?" and if the AA information is not given use Optional.absent
     * @param hitType   indicate what kind of hit it was. It can be: {TARGET, DECOY, UNKNOWN}
     */
    public PeptideProteinMatch(String accession, Optional<String> searchDatabase, Optional<String> previousAA, Optional<String> nextAA, HitType hitType) {

        checkNotNull(hitType);
        checkNotNull(searchDatabase);
        checkNotNull(accession);
        checkNotNull(previousAA);
        checkNotNull(nextAA);

        if (previousAA.isPresent()) {
            checkArgument(allowedAaSymbols.contains(previousAA.get()), previousAA.get());
        }
        if (nextAA.isPresent()) {
            checkArgument(allowedAaSymbols.contains(nextAA.get()), nextAA.get());
        }

        this.searchDatabase = searchDatabase;
        this.accession = accession;
        this.previousAA = previousAA;
        this.nextAA = nextAA;
        this.start = -1;
        this.end = -1;
        this.hitType = hitType;
    }

    /**
     * Constructor, none of the parameters can be null
     * <p/>
     * The previous and next AA are wrapped in com.google.common.base.Optional so that it is possible to differentiate
     * between a unknown AA and when the information is not available.
     *
     * @param accession the protein accession
     * @param searchDatabase the search database
     * @param previousAA the previous amino acid if the peptide is at the N-Term of the protein use "-", if the
     *                   previous amino acid is unknown use "?" and if the AA information is not given use Optional.absent
     * @param nextAA     the next amino acid if the peptide is at the C-Term of the protein use "-", if the
     *                   next amino acid is unknown use "?" and if the AA information is not given use Optional.absent
     * @param start      the start position of the peptide within the protein
     * @param end        the end position of the peptide within the protein
     */
    public PeptideProteinMatch(String accession, Optional<String> searchDatabase, Optional<String> previousAA, Optional<String> nextAA, int start, int end, HitType hitType) {
        this(accession, searchDatabase, previousAA, nextAA, hitType);

        checkArgument(start >= 0);
        checkArgument(end > start);

        this.start = start;
        this.end = end;
    }


    /**
     * Returns the protein accession number
     *
     * @return the protein accession number
     */
    public String getAccession() {

        return accession;
    }

    /**
     * Returns true if the peptide in on the N-Term of the protein, false otherwise
     *
     * @return true if the peptide in on the N-Term of the protein, false otherwise
     */
    public boolean isNTerm() {

        return previousAA.isPresent() && "-".equals(previousAA.get());
    }

    /**
     * Returns true if the peptide in on the C-Term of the protein, false otherwise
     *
     * @return true if the peptide in on the C-Term of the protein, false otherwise
     */
    public boolean isCTerm() {

        return nextAA.isPresent() && "-".equals(nextAA.get());
    }

    /**
     * Returns the amino acid that occurs before the start of the peptide.  If the peptide is
     * N-Term on the protein "-" is returned.
     *
     * @return the amino acid that occurs before the start of the peptide.  If the peptide is
     * N-Term on the protein "-" is returned.
     */
    public Optional<String> getPreviousAA() {

        return previousAA;
    }

    /**
     * Returns the amino acid that occurs after the end of the peptide.  If the peptide is
     * C-Term on the protein "-" is returned.
     *
     * @return the amino acid that occurs after the end of the peptide.  If the peptide is
     * C-Term on the protein "-" is returned.
     */
    public Optional<String> getNextAA() {

        return nextAA;
    }

    /**
     * Returns the start position of the peptide within the protein. If the value is not defined, it will be -1.
     *
     * @return the start position of the peptide within the protein. If the value is not defined, it will be -1.
     */
    public int getStart() {

        return start;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PeptideProteinMatch)) return false;

        PeptideProteinMatch that = (PeptideProteinMatch) o;

        if (start != that.start) return false;
        if (end != that.end) return false;
        if (hitType != that.hitType) return false;
        return accession.equals(that.accession);

    }

    @Override
    public int hashCode() {
        int result = hitType.hashCode();
        result = 31 * result + accession.hashCode();
        result = 31 * result + start;
        result = 31 * result + end;
        return result;
    }

    /**
     * Returns the end position of the peptide within the protein. If the value is not defined, it will be -1.
     *
     * @return the end position of the peptide within the protein. If the value is not defined, it will be -1.
     */
    public int getEnd() {

        return end;
    }

    /**
     * Returns the HitType. It can be: {TARGET, DECOY, UNKNOWN}
     *
     * @return the HitType. It can be: {TARGET, DECOY, UNKNOWN}
     */
    public HitType getHitType() {
        return hitType;
    }

    /**
     * Returns the search database contains this match. If the value is not defined, null
     * @return the search database contains this match. If the value is not defined, null
     */
    public Optional<String> getSearchDatabase() {
        return searchDatabase;
    }


}
