package org.expasy.mzjava.proteomics.mol.digest;

/**
 * IntIntervalCursor consists of currentLowerBound and currentUpperBound bound indices.
 *
 * @author fnikitin
 * Date: 10/30/13
 */
class IntIntervalCursor {

    private int currentLowerBound;
    private int currentUpperBound;

    void reset() {

        this.currentLowerBound = -1;
        this.currentUpperBound = -1;
    }

    public int getCurrentLowerBound() {
        return currentLowerBound;
    }

    public int getCurrentUpperBound() {
        return currentUpperBound;
    }

    public void setCurrentLowerBound(int currentLowerBound) {
        this.currentLowerBound = currentLowerBound;
    }

    public void setCurrentUpperBound(int currentUpperBound) {
        this.currentUpperBound = currentUpperBound;
    }

    public int getCurrentRange() {
        return currentUpperBound - currentLowerBound;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IntIntervalCursor bounds = (IntIntervalCursor) o;

        if (currentLowerBound != bounds.currentLowerBound) return false;
        if (currentUpperBound != bounds.currentUpperBound) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = currentLowerBound;
        result = 31 * result + currentUpperBound;
        return result;
    }

    @Override
    public String toString() {
        return "IntIntervalCursor{" +
                "currentLowerBound=" + currentLowerBound +
                ", currentUpperBound=" + currentUpperBound +
                '}';
    }
}
