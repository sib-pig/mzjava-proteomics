package org.expasy.mzjava.proteomics.io.mol.fasta;


import org.expasy.mzjava.proteomics.mol.Protein;
import org.openide.util.lookup.ServiceProvider;

@ServiceProvider(service = FastaHeaderParser.class)
public class PirPrfFastaHeaderParser implements FastaHeaderParser {

    // pir|Q0TET7|Q0TET7_ECOL5 Putative uncharacterized protein OS=Escherichia coli O6:K15:H31 (strain 536 / UPEC) GN=ECP_2553 PE=4 SV=1
    @Override
    public boolean parseHeader(String header, Protein.Builder protein) {

        if (header != null && protein != null) {

            String[] data = header.split(">\\s?|\\|");

            int offset = data[0].length() == 0 ? 1 : 0;

            if ("pir".equalsIgnoreCase(data[offset]) && data.length > 2) {

                protein.setAccessionId(data[offset + 2]);

                return true;
            } else if ("prf".equalsIgnoreCase(data[offset]) && data.length > 2) {

                protein.setAccessionId(data[offset + 2]);

                return true;
            }

        }
        return false;
    }

}
