
package org.expasy.mzjava.proteomics.io.ms.ident.bioml;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlMixed;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for domainType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="domainType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="aa" type="{}aaType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="start" type="{http://www.w3.org/2001/XMLSchema}Integer" />
 *       &lt;attribute name="end" type="{http://www.w3.org/2001/XMLSchema}Integer" />
 *       &lt;attribute name="expect" type="{http://www.w3.org/2001/XMLSchema}float" />
 *       &lt;attribute name="mh" type="{http://www.w3.org/2001/XMLSchema}float" />
 *       &lt;attribute name="delta" type="{http://www.w3.org/2001/XMLSchema}float" />
 *       &lt;attribute name="hyperscore" type="{http://www.w3.org/2001/XMLSchema}float" />
 *       &lt;attribute name="nextscore" type="{http://www.w3.org/2001/XMLSchema}float" />
 *       &lt;attribute name="y_score" type="{http://www.w3.org/2001/XMLSchema}float" />
 *       &lt;attribute name="y_ions" type="{http://www.w3.org/2001/XMLSchema}Integer" />
 *       &lt;attribute name="b_score" type="{http://www.w3.org/2001/XMLSchema}float" />
 *       &lt;attribute name="b_ions" type="{http://www.w3.org/2001/XMLSchema}Integer" />
 *       &lt;attribute name="pre" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="post" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="seq" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="missed_cleavages" type="{http://www.w3.org/2001/XMLSchema}Integer" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "domainType", propOrder = {
    "content"
})
public class DomainType {

    @XmlElementRef(name = "aa", type = JAXBElement.class, required = false)
    @XmlMixed
    protected List<Serializable> content;
    @XmlAttribute(name = "id")
    protected String id;
    @XmlAttribute(name = "start")
    protected Integer start;
    @XmlAttribute(name = "end")
    protected Integer end;
    @XmlAttribute(name = "expect")
    protected Double expect;
    @XmlAttribute(name = "mh")
    protected Double mh;
    @XmlAttribute(name = "delta")
    protected Double delta;
    @XmlAttribute(name = "hyperscore")
    protected Double hyperscore;
    @XmlAttribute(name = "nextscore")
    protected Double nextscore;
    @XmlAttribute(name = "y_score")
    protected Double yScore;
    @XmlAttribute(name = "y_ions")
    protected Integer yIons;
    @XmlAttribute(name = "b_score")
    protected Double bScore;
    @XmlAttribute(name = "b_ions")
    protected Integer bIons;
    @XmlAttribute(name = "pre")
    protected String pre;
    @XmlAttribute(name = "post")
    protected String post;
    @XmlAttribute(name = "seq")
    protected String seq;
    @XmlAttribute(name = "missed_cleavages")
    protected Integer missedCleavages;

    /**
     * Gets the value of the content property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the content property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link AaType }{@code >}
     * {@link String }
     * 
     * 
     */
    public List<Serializable> getContent() {
        if (content == null) {
            content = new ArrayList<Serializable>();
        }
        return this.content;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the start property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getStart() {
        return start;
    }

    /**
     * Sets the value of the start property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setStart(Integer value) {
        this.start = value;
    }

    /**
     * Gets the value of the end property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getEnd() {
        return end;
    }

    /**
     * Sets the value of the end property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setEnd(Integer value) {
        this.end = value;
    }

    /**
     * Gets the value of the expect property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getExpect() {
        return expect;
    }

    /**
     * Sets the value of the expect property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setExpect(Double value) {
        this.expect = value;
    }

    /**
     * Gets the value of the mh property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getMh() {
        return mh;
    }

    /**
     * Sets the value of the mh property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setMh(Double value) {
        this.mh = value;
    }

    /**
     * Gets the value of the delta property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getDelta() {
        return delta;
    }

    /**
     * Sets the value of the delta property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setDelta(Double value) {
        this.delta = value;
    }

    /**
     * Gets the value of the hyperscore property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getHyperscore() {
        return hyperscore;
    }

    /**
     * Sets the value of the hyperscore property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setHyperscore(Double value) {
        this.hyperscore = value;
    }

    /**
     * Gets the value of the nextscore property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getNextscore() {
        return nextscore;
    }

    /**
     * Sets the value of the nextscore property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setNextscore(Double value) {
        this.nextscore = value;
    }

    /**
     * Gets the value of the yScore property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getYScore() {
        return yScore;
    }

    /**
     * Sets the value of the yScore property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setYScore(Double value) {
        this.yScore = value;
    }

    /**
     * Gets the value of the yIons property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getYIons() {
        return yIons;
    }

    /**
     * Sets the value of the yIons property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setYIons(Integer value) {
        this.yIons = value;
    }

    /**
     * Gets the value of the bScore property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getBScore() {
        return bScore;
    }

    /**
     * Sets the value of the bScore property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setBScore(Double value) {
        this.bScore = value;
    }

    /**
     * Gets the value of the bIons property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBIons() {
        return bIons;
    }

    /**
     * Sets the value of the bIons property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBIons(Integer value) {
        this.bIons = value;
    }

    /**
     * Gets the value of the pre property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPre() {
        return pre;
    }

    /**
     * Sets the value of the pre property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPre(String value) {
        this.pre = value;
    }

    /**
     * Gets the value of the post property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPost() {
        return post;
    }

    /**
     * Sets the value of the post property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPost(String value) {
        this.post = value;
    }

    /**
     * Gets the value of the seq property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeq() {
        return seq;
    }

    /**
     * Sets the value of the seq property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeq(String value) {
        this.seq = value;
    }

    /**
     * Gets the value of the missedCleavages property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMissedCleavages() {
        return missedCleavages;
    }

    /**
     * Sets the value of the missedCleavages property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMissedCleavages(Integer value) {
        this.missedCleavages = value;
    }

}
