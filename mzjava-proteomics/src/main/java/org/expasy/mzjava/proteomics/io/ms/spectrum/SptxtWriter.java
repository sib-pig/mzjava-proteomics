/**
 * Copyright (c) 2010, SIB. All rights reserved.
 * 
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.io.ms.spectrum;


import com.google.common.base.Optional;
import org.expasy.mzjava.core.mol.Mass;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;
import org.expasy.mzjava.proteomics.ms.spectrum.PepLibPeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.proteomics.ms.spectrum.PeptideSpectrum;
import org.expasy.mzjava.proteomics.ms.spectrum.PepFragAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.DecimalFormat;
import java.text.NumberFormat;


/**
 * Write {@code PeptideSpectrum}s in {@code sptxt} format.
 * 
 * @author nikitin
 *
 */
public class SptxtWriter extends AbstractSptxtWriter {

    private int libId = 1;

    private int maxAnnotations = 1000;
    private final NumberFormat annotationFormat = DecimalFormat.getInstance();

    public SptxtWriter(Writer writer, PeakList.Precision precision) {

        super(writer, precision);
        annotationFormat.setMaximumFractionDigits(2);
    }

    public SptxtWriter(File file, boolean append, PeakList.Precision precision) throws IOException {

        this(new FileWriter(file, append), precision);
    }

    public void write(PeptideSpectrum spectrum) throws IOException {

        write(spectrum, spectrum.getPeptide(), spectrum.getProteinAccessionNumbers());
    }

    public void write(PeptideConsensusSpectrum spectrum) throws IOException {

        write(spectrum, spectrum.getPeptide(), spectrum.getProteinAccessionNumbers());
    }

    @Override
    protected String formatAnnotations(int peakIndex, PeakList<? extends PeakAnnotation> peakList) throws IOException {

        StringBuilder sb = new StringBuilder();
        if (!peakList.hasAnnotationsAt(peakIndex)) { // NA

            sb.append("?");
        } else {

            int count = 0;


            for (PeakAnnotation annotation : peakList.getAnnotations(peakIndex)) {

                if (count == maxAnnotations) {
                    break;
                }

                PepFragAnnotation annot;
                if(annotation instanceof PepFragAnnotation)

                    annot = (PepFragAnnotation)annotation;
                else if(annotation instanceof PepLibPeakAnnotation){

                    Optional<PepFragAnnotation> optFragmentAnnotation = ((PepLibPeakAnnotation) annotation).getOptFragmentAnnotation();
                    if (optFragmentAnnotation.isPresent()) {
                        annot = optFragmentAnnotation.get();
                    } else {
                        annot = null;
                    }
                } else {
                    annot = null;
                }
                if (annot != null) {
                    sb.append(annot.getIonType());
                    sb.append(annot.getFragment().size());
                    if (!annot.getNeutralLoss().equals(Mass.ZERO)) {

                        double mw = annot.getNeutralLoss().getMolecularMass();
                        if (mw > 0) {

                            sb.append('+');
                        }
                        sb.append(annotationFormat.format(mw));
                    }
                    if (annot.getCharge() != 1) {

                        sb.append('^');
                        sb.append(annot.getCharge());
                    }

                    double mzDiff = annot.getTheoreticalMz() - peakList.getMz(peakIndex);
                    sb.append("/");
                    sb.append(annotationFormat.format(mzDiff));

                    sb.append(",");

                    count++;
                }
            }

            // delete last comma
            if (sb.length()>0) sb.delete(sb.length() - 1, sb.length());
        }

        if (sb.length()==0) sb.append("?");

        return sb.toString();
    }

    @Override
    protected void writeLibId(PeakList peakList, Peptide peptide, Writer writer) throws IOException {

        writer.write("LibID: ");
        writer.write(Integer.toString(libId++));
        writer.write("\n");
    }

    public void setMaxAnnotationToWrite(int maxAnnotations) {

        this.maxAnnotations = maxAnnotations;
    }
}
