package org.expasy.mzjava.proteomics.io.ms.spectrum.sptxt;

import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.ms.spectrum.PepLibPeakAnnotation;

import java.util.Collections;
import java.util.List;

/**
 * Annotation resolver that ignores annotations
 *
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class SkipAnnotationResolver implements AnnotationResolver {
    @Override
    public List<PepLibPeakAnnotation> resolveAnnotations(String annotations, Peptide peptide) {

        return Collections.emptyList();
    }
}
