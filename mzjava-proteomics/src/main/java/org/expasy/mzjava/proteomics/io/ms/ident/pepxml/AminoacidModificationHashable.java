package org.expasy.mzjava.proteomics.io.ms.ident.pepxml;

import com.google.common.base.Preconditions;
import org.expasy.mzjava.proteomics.io.ms.ident.pepxml.v117.MsmsPipelineAnalysis;

/**
 * @author fnikitin
 *         Date: 2/16/14
 */
public class AminoacidModificationHashable extends MsmsPipelineAnalysis.MsmsRunSummary.SearchSummary.AminoacidModification {

    public AminoacidModificationHashable(MsmsPipelineAnalysis.MsmsRunSummary.SearchSummary.AminoacidModification aminoacidModification) {

        Preconditions.checkNotNull(aminoacidModification);

        mass = aminoacidModification.getMass();
        aminoacid = aminoacidModification.getAminoacid();
        binary = aminoacidModification.getBinary();
        description = aminoacidModification.getDescription();
        massdiff = aminoacidModification.getMassdiff();
        peptideTerminus = aminoacidModification.getPeptideTerminus();
        symbol = aminoacidModification.getSymbol();
        variable = aminoacidModification.getVariable();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AminoacidModificationHashable that = (AminoacidModificationHashable) o;

        if (Float.compare(that.mass, mass) != 0) return false;
        if (aminoacid != null ? !aminoacid.equals(that.aminoacid) : that.aminoacid != null) return false;
        if (binary != null ? !binary.equals(that.binary) : that.binary != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null)
            return false;
        if (massdiff != null ? !massdiff.equals(that.massdiff) : that.massdiff != null) return false;
        if (peptideTerminus != null ? !peptideTerminus.equals(that.peptideTerminus) : that.peptideTerminus != null)
            return false;
        if (symbol != null ? !symbol.equals(that.symbol) : that.symbol != null) return false;
        if (variable != null ? !variable.equals(that.variable) : that.variable != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = aminoacid != null ? aminoacid.hashCode() : 0;
        result = 31 * result + (massdiff != null ? massdiff.hashCode() : 0);
        result = 31 * result + (mass != +0.0f ? Float.floatToIntBits(mass) : 0);
        result = 31 * result + (variable != null ? variable.hashCode() : 0);
        result = 31 * result + (peptideTerminus != null ? peptideTerminus.hashCode() : 0);
        result = 31 * result + (symbol != null ? symbol.hashCode() : 0);
        result = 31 * result + (binary != null ? binary.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }
}
