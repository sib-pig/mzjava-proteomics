/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.io.ms.spectrum.sptxt;

import com.google.common.base.Optional;
import com.google.common.base.Splitter;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.core.mol.PeriodicTable;
import org.expasy.mzjava.core.ms.spectrum.FragmentType;
import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.proteomics.mol.PeptideFragment;
import org.expasy.mzjava.utils.RegexConstants;
import org.expasy.mzjava.core.mol.Mass;
import org.expasy.mzjava.core.mol.NumericMass;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.proteomics.ms.spectrum.PepLibPeakAnnotation;
import org.expasy.mzjava.proteomics.ms.spectrum.PepFragAnnotation;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class SpectraStAnnotationResolver implements AnnotationResolver {

    private final Pattern backboneAnnotationPattern =
            //              (ion type)(residue#)(neutral loss) (charge)  (isotope)         (mass difference)
            Pattern.compile("([abcxyz])(\\d+)((?:[+-]\\d+)?)((?:\\^\\d+)?)([ix]?)/(" + RegexConstants.REAL + ")");
    private final Pattern precursorAnnotationPattern =
            //                  (neutral loss) (charge)  (isotope)         (mass difference)
            Pattern.compile("p((?:[+-]\\d+)?)((?:\\^\\d+)?)([ix]?)/(" + RegexConstants.REAL + ")");
    private final Pattern internalAnnotationPattern =
            //(neutral loss)(isotope)         (mass difference)
            Pattern.compile("I([A-Z])[A-Z]([ix]?)/([+-]?\\d+\\.?\\d+)");

    private final Splitter commaSplitter = Splitter.on(',');

    private final Map<String, Composition> compositionMap = new HashMap<String, Composition>();

    public SpectraStAnnotationResolver() {

        compositionMap.put("-17", Composition.parseComposition("(NH3)-1"));
        compositionMap.put("-18", Composition.parseComposition("(H2O)-1"));
        compositionMap.put("-80", Composition.parseComposition("(HPO3)-1"));
    }

    @Override
    public List<PepLibPeakAnnotation> resolveAnnotations(String value, Peptide peptide) {

        if("?".startsWith(value))
            return Collections.emptyList();
        boolean hasPeakStats = true;

        int firstSpace = value.indexOf('\t');
        if(firstSpace == -1)
            firstSpace = value.indexOf(' ');
        if(firstSpace == -1) {

            firstSpace = value.length();
            hasPeakStats = false;
        }
//        if (firstSpace == -1)
//            throw new IllegalStateException("The annotation '" + value + "' cannot be recognised as a SpectraST annotation");
        int secondSpace = value.indexOf('\t', firstSpace + 1);
        if (secondSpace == -1)
            secondSpace = value.indexOf(' ', firstSpace + 1);
//        if (secondSpace == -1)
//            throw new IllegalStateException("The annotation '" + value + "' cannot be recognised as a SpectraST annotation");

        int beginIndex = 0;

        List<String> pepAnnotationSplits = commaSplitter.splitToList(value.substring(beginIndex, firstSpace));
        int mergedPeakCount = 0;
        if (hasPeakStats) {
            mergedPeakCount = parsePeakStatistics(value.substring(firstSpace + 1, secondSpace));
        }

        List<PepLibPeakAnnotation> annotations = new ArrayList<PepLibPeakAnnotation>(pepAnnotationSplits.size());
        for (String pepAnnotationValue : pepAnnotationSplits) {

            Optional<PepFragAnnotation> optPepFragAnnotation = parsePepFragAnnotation(pepAnnotationValue, peptide);
            annotations.add(new PepLibPeakAnnotation(mergedPeakCount, 0, 0, optPepFragAnnotation));
        }
        return annotations;
    }

    private int parsePeakStatistics(String value) {

        int index = value.indexOf('/');

        return Integer.parseInt(value.substring(0, index));
    }

    private Optional<PepFragAnnotation> parsePepFragAnnotation(String value, Peptide peptide) {

        char type = value.charAt(0);

        switch (type) {

            case 'I':
                return parseInternalAnnotation(value);
            case 'a':
            case 'b':
            case 'c':
            case 'x':
            case 'y':
            case 'z':
                return parseBackboneAnnotation(value, peptide);
            case 'p':
                return parsePrecursorAnnotation(value, peptide);
            default:
                return Optional.absent();
        }
    }

    /**
     * @param value the string to parse
     * @return an Optional containing the annotation if the value can be parsec or Optional.absent() if not
     */
    protected Optional<PepFragAnnotation> parseInternalAnnotation(String value) {

        Matcher matcher = internalAnnotationPattern.matcher(value);
        if (!matcher.matches())
            return Optional.absent();

        String sequence = matcher.group(1);
        String isotopeGroup = matcher.group(2);
        String massDiffGroup = matcher.group(3);

        Mass neutralLoss = Mass.ZERO;
        Composition isotopeComposition = isotopeGroup.isEmpty() ? PepFragAnnotation.EMPTY_COMPOSITION : resolveIsotope(massDiffGroup);

        PepFragAnnotation.Builder builder = new PepFragAnnotation.Builder(IonType.i, 1, new PeptideFragment(FragmentType.INTERNAL, AminoAcid.valueOf(sequence)))
                .addIsotopeComposition(isotopeComposition)
                .setNeutralLoss(neutralLoss);
        return Optional.of(builder.build());
    }

    /**
     * Parses annotations like
     * <p/>
     * <pre>
     *     p/0.13
     *     p-18/-0.06
     * </pre>
     *
     * @param value   the string to parse
     * @param peptide precursor peptide
     * @return an Optional containing the annotation if the value can be parsec or Optional.absent() if not
     */
    protected Optional<PepFragAnnotation> parsePrecursorAnnotation(String value, Peptide peptide) {

        Matcher matcher = precursorAnnotationPattern.matcher(value);
        if (!matcher.matches())
            return Optional.absent();

        String neutralLossGroup = matcher.group(1);
        String chargeGroup = matcher.group(2);
        String isotopeGroup = matcher.group(3);
        String massDiffGroup = matcher.group(4);

        int charge = chargeGroup.isEmpty() ? 1 : Integer.parseInt(chargeGroup.substring(1));
        Mass neutralLoss = neutralLossGroup.isEmpty() ? Mass.ZERO : resolveComposition(neutralLossGroup);
        Composition isotopeComposition = isotopeGroup.isEmpty() ? PepFragAnnotation.EMPTY_COMPOSITION : resolveIsotope(massDiffGroup);

        PepFragAnnotation.Builder builder = new PepFragAnnotation.Builder(IonType.p, charge, peptide)
                .addIsotopeComposition(isotopeComposition)
                .setNeutralLoss(neutralLoss);
        return Optional.of(builder.build());
    }

    /**
     * Parses annotations like
     * <p/>
     * <pre>
     *     b6-43i^2/1.48
     *     b3i/0.38
     *     z33/0.38
     * </pre>
     *
     * @param value   the string to parse
     * @param peptide precursor peptide
     * @return an Optional containing the annotation if the value can be parsec or Optional.absent() if not
     */
    protected Optional<PepFragAnnotation> parseBackboneAnnotation(String value, Peptide peptide) {

        Matcher matcher = backboneAnnotationPattern.matcher(value);
        if (!matcher.matches())
            return Optional.absent();

        IonType ionType = IonType.valueOf(matcher.group(1));
        int residueNumber = Integer.parseInt(matcher.group(2));

        String neutralLossGroup = matcher.group(3);
        String chargeGroup = matcher.group(4);
        String isotopeGroup = matcher.group(5);
        String massDiffGroup = matcher.group(6);

        int charge = chargeGroup.isEmpty() ? 1 : Integer.parseInt(chargeGroup.substring(1));
        Mass neutralLoss = neutralLossGroup.isEmpty() ? Mass.ZERO : resolveComposition(neutralLossGroup);
        PeptideFragment fragment = peptide.createFragment(ionType, residueNumber);
        Composition isotopeComposition = isotopeGroup.isEmpty() ? PepFragAnnotation.EMPTY_COMPOSITION : resolveIsotope(massDiffGroup);

        PepFragAnnotation.Builder builder = new PepFragAnnotation.Builder(ionType, charge, fragment)
                .addIsotopeComposition(isotopeComposition)
                .setNeutralLoss(neutralLoss);
        return Optional.of(builder.build());
    }

    private Composition resolveIsotope(String massDiffGroup) {

        double mzDiff = Double.parseDouble(massDiffGroup);

        int c13Count = Math.max(1, (int) Math.round(Math.abs(mzDiff)));
        return new Composition.Builder().add(PeriodicTable.C13, c13Count).build();
    }

    protected Mass resolveComposition(String neutralLossGroup) {

        Composition comp = compositionMap.get(neutralLossGroup);
        if (comp != null)
            return comp;
        else
            return new NumericMass(Double.parseDouble(neutralLossGroup));
    }
}
