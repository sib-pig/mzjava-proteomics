/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.io.ms.spectrum;


import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessorChain;
import org.expasy.mzjava.proteomics.io.ms.spectrum.sptxt.*;
import org.expasy.mzjava.proteomics.mol.modification.ModificationResolver;
import org.expasy.mzjava.proteomics.mol.modification.unimod.UnimodModificationResolver;
import org.expasy.mzjava.proteomics.ms.spectrum.PepLibPeakAnnotation;

import java.io.*;
import java.net.URI;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Oliver Horlacher
 *
 *  @version sqrt -1
 */
public class SptxtReader extends MsLibReader {

    public SptxtReader(File file, PeakList.Precision precision) throws IOException {

        this(new FileReader(file), file.toURI(), precision,
                new PeakProcessorChain<PepLibPeakAnnotation>(),
                new SkipAnnotationResolver(),
                new UnimodModificationResolver(),
                new SpectraStCommentParser()
        );
    }

    private SptxtReader(Reader reader, final URI spectraSource, PeakList.Precision precision,
                        PeakProcessorChain<PepLibPeakAnnotation> processorChain,
                        AnnotationResolver annotationResolver,
                        ModificationResolver modResolver,
                        SpectraLibCommentParser commentParser) throws IOException {

        super(reader, spectraSource, precision, commentParser, annotationResolver,
                processorChain, modResolver);
    }

    /**
     * Factory method to create a Builder for creating a SptxtReader to read an sptxt file from <code>file</code>
     *
     * @param file the file that is to be read
     * @return the new Builder
     * @throws IOException
     */
    public static Builder newBuilder(File file, PeakList.Precision precision) throws IOException {

        return new Builder(new FileReader(file), file.toURI(), precision);
    }

    /**
     * Factory method to create a Builder for creating a SptxtReader to read an sptxt file from the <code>reader</code>
     *
     * @param reader the reader that is to be read
     * @param source the source of the spectra
     * @return the new Builder
     */
    public static Builder newBuilder(Reader reader, URI source, PeakList.Precision precision) {

        return new Builder(reader, source, precision);
    }

    /**
     *
     */
    public static class Builder {

        private final Reader reader;
        private final URI source;
        private final PeakList.Precision precision;
        private PeakProcessorChain<PepLibPeakAnnotation> processorChain;
        private AnnotationResolver annotationResolver = null;
        private boolean acceptUnsortedSpectra = false;
        private ModificationResolver modResolver = null;
        private SpectraLibCommentParser commentParser = null;

        public Builder(Reader reader, URI source, PeakList.Precision precision) {

            this.reader = reader;
            this.source = source;
            this.precision = precision;
        }

        public Builder acceptUnsortedSpectra() {

            acceptUnsortedSpectra = true;
            return this;
        }

        public Builder useModificationResolver(ModificationResolver modResolver) {

            checkNotNull(modResolver);
            this.modResolver = modResolver;

            return this;
        }

        public Builder usePeakProcessorChain(PeakProcessorChain<PepLibPeakAnnotation> processorChain) {

            checkNotNull(processorChain);
            this.processorChain = processorChain;
            return this;
        }

        public Builder useSkipAnnotationResolver() {

            annotationResolver = new SkipAnnotationResolver();
            return this;
        }

        public Builder useAnnotationResolver(AnnotationResolver annotationResolver) {

            checkNotNull(annotationResolver);
            this.annotationResolver = annotationResolver;
            return this;
        }

        public Builder useSpectraStAnnotationResolver() {

            annotationResolver = new SpectraStAnnotationResolver();
            return this;
        }

        public Builder useSpectraLibCommentParser(SpectraLibCommentParser commentParser) {

            checkNotNull(commentParser);
            this.commentParser = commentParser;
            return this;
        }

        public SptxtReader build() throws IOException {

            if (processorChain == null)
                processorChain = new PeakProcessorChain<>();

            if(modResolver == null)
                modResolver = new UnimodModificationResolver();

            if(annotationResolver == null)
                annotationResolver = new SkipAnnotationResolver();

            if(commentParser == null)
                commentParser = new SpectraStCommentParser();

            SptxtReader mspReader = new SptxtReader(reader, source, precision, processorChain, annotationResolver, modResolver, commentParser);
            if (acceptUnsortedSpectra)
                mspReader.acceptUnsortedSpectra();

            return mspReader;
        }
    }
}
