/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.ms.dbsearch;

import com.google.common.base.Optional;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.ms.spectrum.PeptideSpectrum;

import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.Map;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * PeptideSpectrum cache that is memory-sensitive. If the VM runs out of memory the cached PeptideSpectra
 * can be garbage collected.
 *
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class SoftRefPepSpectrumCache implements PeptideSpectrumCache {

    protected final TIntObjectMap<Map<Peptide, SoftReference<PeptideSpectrum>>> cache;

    public SoftRefPepSpectrumCache() {

        cache = new TIntObjectHashMap<Map<Peptide, SoftReference<PeptideSpectrum>>>(4);
    }

    @Override
    public void put(PeptideSpectrum peptideSpectrum) {

        checkNotNull(peptideSpectrum);

        Map<Peptide, SoftReference<PeptideSpectrum>> spectrumMap = cache.get(peptideSpectrum.getCharge());
        if (spectrumMap == null) {

            spectrumMap = new HashMap<Peptide, SoftReference<PeptideSpectrum>>();
            cache.put(peptideSpectrum.getCharge(), spectrumMap);
        }

        spectrumMap.put(peptideSpectrum.getPeptide(), new SoftReference<PeptideSpectrum>(peptideSpectrum));
    }

    @Override
    public Optional<PeptideSpectrum> get(Peptide peptide, int charge) {

        checkNotNull(peptide);
        checkArgument(charge > 0);

        Map<Peptide, SoftReference<PeptideSpectrum>> spectrumMap = cache.get(charge);

        if (spectrumMap == null)
            return Optional.absent();

        final SoftReference<PeptideSpectrum> reference = spectrumMap.get(peptide);
        if(reference == null)
            return Optional.absent();

        return Optional.fromNullable(reference.get());
    }
}
