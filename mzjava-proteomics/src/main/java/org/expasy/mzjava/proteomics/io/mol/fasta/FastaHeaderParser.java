package org.expasy.mzjava.proteomics.io.mol.fasta;

import org.expasy.mzjava.proteomics.mol.Protein;

/**
 * This interface has to be implemented by parsers that can extract information from
 * the Fasta header line.
 *
 * @version 1.0
 */
public interface FastaHeaderParser {


    boolean parseHeader(String header, Protein.Builder protein);
}
