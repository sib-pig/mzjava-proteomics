/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.mol;


import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.expasy.mzjava.core.ms.spectrum.FragmentType;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;

import java.util.*;

import static com.google.common.base.Preconditions.*;

/**
 * A builder for constructing Peptides 
 * 
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class PeptideBuilder {

    private final List<AminoAcid> sequence;
    private final Multimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
    private final Multimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();

    /**
     * Construct a peptide builder initialised by parsing the <code>peptide</code> string
     *
     * @param peptide the initial state of the builder
     */
    public PeptideBuilder(String peptide) {

        checkNotNull(peptide);
        sequence = new ArrayList<AminoAcid>(peptide.length());
        PeptideParser.getInstance().parsePeptide(peptide, sequence, sideChainModMap, termModMap);
    }

    /**
     * Construct a PeptideBuilder initialised to contain the amino acids from <code>sequence</code>
     *
     * @param sequence the initial amino acids
     */
    public PeptideBuilder(List<AminoAcid> sequence) {

        checkNotNull(sequence);
        this.sequence = new ArrayList<AminoAcid>(sequence);
    }

    /**
     * Construct a PeptideBuilder initialised to contain the amino acids from <code>seq</code>
     *
     * @param seq the initial amino acids
     */
    public PeptideBuilder(AminoAcid... seq) {

        checkNotNull(seq);
        sequence = new ArrayList<AminoAcid>(seq.length);
        Collections.addAll(sequence, seq);
    }

    /**
     * Constructs an PeptideBuilder by copying the residues from <code>residues</code> starting at
     * <code>fromInclusive</code> to <code>toExclusive</code>.
     *
     * @param residues        the residues, the array cannot be empty
     * @param fromInclusive   the beginning index, inclusive
     * @param toExclusive     the ending index, exclusive
     */
    public PeptideBuilder(AminoAcid[] residues, int fromInclusive, int toExclusive) {

        checkNotNull(residues);
        checkElementIndex(fromInclusive, residues.length);
        checkPositionIndex(toExclusive, residues.length);
        checkArgument(toExclusive >= fromInclusive);

        sequence = new ArrayList<AminoAcid>(toExclusive - fromInclusive + 1);

        //non manual copy uses Arrays.asList and then makes a sub list, manual copying is cleaner
        //noinspection ManualArrayToCollectionCopy
        for(int i = fromInclusive; i < toExclusive; i++) {

            sequence.add(residues[i]);
        }
    }

    /**
     * Construct a PeptideBuilder initialised to contain the amino acids sequence and modifications
     * copied from the <code>peptide</code>
     *
     * @param peptide the peptide from which to copy the seqence and modifications
     */
    public PeptideBuilder(Peptide peptide) {

        checkNotNull(peptide);
        sequence = new ArrayList<AminoAcid>(peptide.size());

        for(int i = 0; i < peptide.size(); i++) {

            sequence.add(peptide.getSymbol(i));

            for(Modification mod : peptide.getModificationsAt(i, EnumSet.of(ModAttachment.SIDE_CHAIN))){

                sideChainModMap.put(i, mod);
            }
        }

        termModMap.putAll(ModAttachment.N_TERM, peptide.getModifications(EnumSet.of(ModAttachment.N_TERM)));
        termModMap.putAll(ModAttachment.C_TERM, peptide.getModifications(EnumSet.of(ModAttachment.C_TERM)));
    }

    /**
     * Add a the <code>modification</code> to either end of the the peptide. The end is
     * specified busing the <code>modAttachment</code>
     *
     * @param modAttachment specifies which end of the peptide the modification is to be added
     * @param modification the Modification that is to be added
     * @return the builder
     */
    public PeptideBuilder addModification(ModAttachment modAttachment, Modification modification) {

        checkNotNull(modAttachment);
        checkNotNull(modification);
        checkArgument(modAttachment != ModAttachment.SIDE_CHAIN, "To add a side chain modification a residue index is required. Use addModification(int index, Modification modification) instead");

        termModMap.put(modAttachment, modification);

        return this;
    }

    /**
     * Parse the <code>peptideSequence</code> string and add the sequence and modifications to this builder
     *
     * @param peptideSequence the string to parse
     * @return the builder
     */
    public PeptideBuilder parseAndAdd(String peptideSequence) {

        checkNotNull(peptideSequence);
        PeptideParser.getInstance().parsePeptide(peptideSequence, sequence, sideChainModMap, termModMap);
        return this;
    }

    /**
     * Add an amino acid
     *
     * @param aminoAcid the amino acid to add
     * @return this builder
     */
    public PeptideBuilder add(AminoAcid aminoAcid) {

        sequence.add(aminoAcid);
        return this;
    }

    /**
     * Add a the <code>modification</code> to the side chain of residue at <code>index</code>
     *
     * @param index the index of the reside where the Modification is to be added
     * @param modification the ModificationMatch that is to be added
     * @return this builder
     */
    public PeptideBuilder addModification(int index, Modification modification) {

        checkNotNull(modification);

        sideChainModMap.put(index, modification);
        return this;
    }

    /**
     * Add the <code>modification</code> to all amino acids that are equal to <code>aminoAcid</code>
     *
     * @param modification the modification to add
     * @param aminoAcid the amino acid to which the modification is to be added
     * @return this builder
     */
    public PeptideBuilder addModificationToAll(Modification modification, AminoAcid aminoAcid) {

        checkNotNull(aminoAcid);
        checkNotNull(modification);

        for (int i = 0, sequenceSize = sequence.size(); i < sequenceSize; i++) {

            AminoAcid aa = sequence.get(i);

            if (aa.equals(aminoAcid)) {

                addModification(i, modification);
            }
        }

        return this;
    }

    /**
     * Add all of the Modification's in <code>modifications</code> to all amino acids that are equal to <code>aminoAcid</code>
     *
     * @param modifications the modifications to add
     * @param aminoAcid the amino acid to which the modification is to be added
     * @return this builder
     */
    public PeptideBuilder addModificationToAll(Collection<Modification> modifications, AminoAcid aminoAcid) {

        checkNotNull(aminoAcid);
        checkNotNull(modifications);

        for (int i = 0, sequenceSize = sequence.size(); i < sequenceSize; i++) {

            AminoAcid aa = sequence.get(i);

            if (aa.equals(aminoAcid)) {

                sideChainModMap.putAll(i, modifications);
            }
        }

        return this;
    }

    /**
     * Reset this builder
     */
    public void clear(){

        sequence.clear();
        sideChainModMap.clear();
        termModMap.clear();
    }

    /**
     * Return the number of amino acids
     *
     * @return the number of amino acids
     */
    public int size() {

        return sequence.size();
    }

    /**
     * Build a Peptide
     *
     * @return the new Peptide
     */
    public Peptide build() {

        if(sequence.isEmpty()) throw new IllegalStateException("Cannot build a peptide with empty sequence");

        Peptide peptide = new Peptide(sequence, sideChainModMap, termModMap);
        clear();
        return peptide;
    }

    /**
     * Build a PeptideFragment
     *
     * @param fragmentType the fragment type for the PeptideFragment
     * @return the new PeptideFragment
     */
    public PeptideFragment buildFragment(FragmentType fragmentType) {

        if(sequence.isEmpty()) throw new IllegalStateException("Cannot build a peptide with empty sequence");

        PeptideFragment fragment = new PeptideFragment(fragmentType, sequence, sideChainModMap, termModMap);
        clear();
        return fragment;
    }
}
