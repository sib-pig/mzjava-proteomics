package org.expasy.mzjava.proteomics.mol.digest;

import java.util.Iterator;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * Generates all possible intervals of integers in the closed interval [lower, upper]
 *
 * @author fnikitin
 * Date: 10/31/13
 */
abstract class IntIntervalIterator implements Iterator<IntIntervalCursor> {

    protected final int lower, upper;
    protected int i, j;
    private final IntIntervalCursor intIntervalCursor;
    private final int rangeMax;
    protected boolean hasNext;
    private boolean consumed;

    protected IntIntervalIterator(int lower, int upper) {

        this(lower, upper, upper-lower);
    }

    protected IntIntervalIterator(int lower, int upper, int rangeMax) {

        checkArgument(lower<upper);
        checkArgument(rangeMax > 0);

        this.lower = lower;
        this.upper = upper;
        this.rangeMax = Math.min(upper-lower, rangeMax);

        intIntervalCursor = new IntIntervalCursor();
        intIntervalCursor.reset();

        hasNext = true;
        consumed = false;

        init();
    }

    protected abstract void init();
    protected abstract void nextIteration(int rangeMax);

    @Override
    public boolean hasNext() {

        // not yet consumed
        if (!consumed)
            nextValidIteration();

        return hasNext;
    }

    private void nextValidIteration() {

        nextIteration(rangeMax);

        consumed = true;
    }

    @Override
    public IntIntervalCursor next() {

        if (!consumed)
            nextValidIteration();

        if (hasNext) {
            intIntervalCursor.setCurrentLowerBound(i);
            intIntervalCursor.setCurrentUpperBound(j);
        } else {
            intIntervalCursor.setCurrentLowerBound(-1);
            intIntervalCursor.setCurrentUpperBound(-1);
        }

        consumed = false;

        return intIntervalCursor;
    }

    @Override
    public void remove() {

        throw new UnsupportedOperationException("");
    }
}
