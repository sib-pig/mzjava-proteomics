package org.expasy.mzjava.proteomics.mol.digest;

import gnu.trove.list.TIntList;
import org.expasy.mzjava.core.mol.SymbolSequence;
import org.expasy.mzjava.proteomics.mol.AminoAcid;


/**
 * @author fnikitin
 */
public interface CleavageSiteFinder {

    /** Find potential cleavage sites over the given amino-acid sequence */
    void find(SymbolSequence<AminoAcid> aaSeq, TIntList sites);

    /** Get the number of potential cleavage sites */
    int countCleavageSites(SymbolSequence<AminoAcid> aaSeq);

    /** Get the cleavage site matcher */
    CleavageSiteMatcher getCleavageSiteMatcher();
}
