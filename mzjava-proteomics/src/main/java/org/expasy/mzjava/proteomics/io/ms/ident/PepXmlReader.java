package org.expasy.mzjava.proteomics.io.ms.ident;

import com.google.common.base.Optional;
import org.expasy.mzjava.core.mol.PeriodicTable;
import org.expasy.mzjava.core.ms.spectrum.ScanNumberDiscrete;
import org.expasy.mzjava.core.ms.spectrum.ScanNumberInterval;
import org.expasy.mzjava.proteomics.io.ms.ident.pepxml.v117.NameValueType;
import org.expasy.mzjava.proteomics.io.ms.ident.pepxml.v117.PeptideprophetResult;
import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.proteomics.ms.ident.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.util.StreamReaderDelegate;
import java.io.*;
import java.math.BigInteger;
import java.util.BitSet;

import static org.expasy.mzjava.proteomics.io.ms.ident.pepxml.v117.MsmsPipelineAnalysis.MsmsRunSummary.SpectrumQuery;
import static org.expasy.mzjava.proteomics.io.ms.ident.pepxml.v117.MsmsPipelineAnalysis.MsmsRunSummary.SpectrumQuery.SearchResult.SearchHit;
import static org.expasy.mzjava.proteomics.io.ms.ident.pepxml.v117.MsmsPipelineAnalysis.MsmsRunSummary.SpectrumQuery.SearchResult.SearchHit.*;
import static org.expasy.mzjava.proteomics.io.ms.ident.pepxml.v117.MsmsPipelineAnalysis.MsmsRunSummary.SpectrumQuery.SearchResult.SearchHit.ModificationInfo.ModAminoacidMass;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class PepXmlReader implements PsmReader {

    public enum ModMassStorage {MOD_MASS, AA_MASS_PLUS_MOD_MASS}

    protected final ModMassStorage modMassStorage;
    protected final boolean discardAmbiguous;
    protected final ModificationMatchResolver modMatchResolver;

    protected static final BitSet UNKNOWN_AA = new BitSet();
    protected static final ModificationMatchResolver CONSTANT_RESOLVER = new ModificationMatchResolver() {

        @Override
        public Optional<Modification> resolve(ModificationMatch modificationMatch) {

            return Optional.absent();
        }
    };

    public PepXmlReader(ModMassStorage modMassStorage, boolean discardAmbiguousSequences) {

        this(modMassStorage, discardAmbiguousSequences, CONSTANT_RESOLVER);
    }

    public PepXmlReader(ModMassStorage modMassStorage, boolean discardAmbiguousSequences, ModificationMatchResolver modMatchResolver) {

        this.modMassStorage = modMassStorage;
        this.discardAmbiguous = discardAmbiguousSequences;
        this.modMatchResolver = modMatchResolver;

        UNKNOWN_AA.set('B');
        UNKNOWN_AA.set('J');
        UNKNOWN_AA.set('O');
        UNKNOWN_AA.set('U');
        UNKNOWN_AA.set('Z');
        UNKNOWN_AA.set('X');
    }

    @Override
    public void parse(File file, PSMReaderCallback callback) {

        try {

            parse(new NamespaceRewriteDelegate(XMLInputFactory.newFactory().createXMLStreamReader(new FileInputStream(file), "UTF-8")), Optional.of(file.getName()), callback);
        } catch (XMLStreamException | JAXBException | FileNotFoundException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void parse(InputStream inputStream, PSMReaderCallback callback) {

        try {

            parse(new NamespaceRewriteDelegate(XMLInputFactory.newFactory().createXMLStreamReader(inputStream, "UTF-8")), Optional.<String>absent(), callback);
        } catch (XMLStreamException | JAXBException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void parse(Reader reader, PSMReaderCallback callback) {

        try {

            parse(new NamespaceRewriteDelegate(XMLInputFactory.newFactory().createXMLStreamReader(reader)), Optional.<String>absent(), callback);
        } catch (XMLStreamException | JAXBException e) {
            throw new IllegalStateException(e);
        }
    }

    protected void parse(XMLStreamReader xsr, Optional<String> xmlFileName, PSMReaderCallback callback) throws XMLStreamException, JAXBException {

        JAXBContext jc = JAXBContext.newInstance(SpectrumQuery.class);
        Unmarshaller unmarshaller = jc.createUnmarshaller();

        do {

            xsr.next();
            if (xsr.isStartElement() && "spectrum_query".equals(xsr.getLocalName())) {

                JAXBElement<SpectrumQuery> jb = unmarshaller.unmarshal(xsr, SpectrumQuery.class);

                SpectrumQuery query = jb.getValue();

                SpectrumIdentifier identifier = new SpectrumIdentifier(query.getSpectrum());
                addScanNumber(identifier, (int) query.getStartScan(), (int) query.getEndScan());

                identifier.setPrecursorNeutralMass(query.getPrecursorNeutralMass());
                identifier.setAssumedCharge(intValue(query.getAssumedCharge(), 0));
                identifier.setIndex((int) query.getIndex());
                if (query.getPrecursorIntensity() != null)
                    identifier.setPrecursorIntensity(query.getPrecursorIntensity());
                if (xmlFileName.isPresent()) identifier.setName(xmlFileName.get());

                for (SpectrumQuery.SearchResult result : query.getSearchResult()) {

                    for (SearchHit searchHit : result.getSearchHit()) {

                        processSearchHit(callback, identifier, searchHit);
                    }
                }
            }

            if (xsr.isEndElement() && "msms_pipeline_analysis".equals(xsr.getLocalName()))
                break;
        } while (xsr.hasNext());
        xsr.close();
    }

    protected void addScanNumber(SpectrumIdentifier identifier, int startScan, int endScan) {

        if (startScan == endScan) {

            identifier.addScanNumber(new ScanNumberDiscrete(startScan));
        } else {

            identifier.addScanNumber(new ScanNumberInterval(startScan, endScan));
        }
    }

    protected void processSearchHit(PSMReaderCallback callback, SpectrumIdentifier identifier, SearchHit searchHit) {

        String peptideSequence = searchHit.getPeptide();

        if(discardAmbiguous && containsUnknownAA(peptideSequence))
            return;

        PeptideMatch peptideMatch = new PeptideMatch(peptideSequence);
        peptideMatch.setRank(Optional.fromNullable((int) searchHit.getHitRank()));

        Integer numMatchedIons = (searchHit.getNumMatchedIons() != null) ? searchHit.getNumMatchedIons().intValue() : null;
        peptideMatch.setNumMatchedIons(Optional.fromNullable(numMatchedIons));
        Integer totNumIons = (searchHit.getTotNumIons() != null) ? searchHit.getTotNumIons().intValue() : null;
        peptideMatch.setTotalNumIons(Optional.fromNullable(totNumIons));
        peptideMatch.setMassDiff(Optional.fromNullable(parseDouble(searchHit.getMassdiff())));
        Integer numMissedCleavages = (searchHit.getNumMissedCleavages() != null) ? searchHit.getNumMissedCleavages().intValue() : null;
        peptideMatch.setNumMissedCleavages(Optional.fromNullable(numMissedCleavages));
        peptideMatch.setRejected(Optional.fromNullable(parseBoolean(searchHit.getIsRejected())));
        peptideMatch.addProteinMatch(newProteinMatch(searchHit));
        for (AlternativeProtein protein : searchHit.getAlternativeProtein()) {

            peptideMatch.addProteinMatch(newProteinMatch(protein));
        }

        ModificationInfo modInfo = searchHit.getModificationInfo();
        copyModInfo(peptideMatch, modInfo);

        for (NameValueType searchScore : searchHit.getSearchScore()) {

            final String name = searchScore.getName();
            final double value = parseDouble(searchScore.getValueAttribute());
            peptideMatch.addScore(name, value);
        }

        readAnalysisResult(searchHit, peptideMatch);
        callback.resultRead(identifier, peptideMatch);
    }

    protected boolean containsUnknownAA(String peptideSequence) {

        for(int i = 0, size = peptideSequence.length(); i < size; i++){

            if(UNKNOWN_AA.get(peptideSequence.charAt(i)))
                return true;
        }

        return false;
    }

    protected void readAnalysisResult(SearchHit searchHit, PeptideMatch searchResult) {

        for (AnalysisResult analysisResult : searchHit.getAnalysisResult()) {

            Object any = analysisResult.getAny();
            if (any instanceof PeptideprophetResult) {

                PeptideprophetResult peptideprophetResult = (PeptideprophetResult) any;
                searchResult.addScore("peptideprophet", peptideprophetResult.getProbability());
            }
        }
    }

    protected PeptideProteinMatch newProteinMatch(AlternativeProtein protein) {

        return new PeptideProteinMatch(protein.getProtein(), Optional.<String>absent(), Optional.fromNullable(protein.getPeptidePrevAa()), Optional.fromNullable(protein.getPeptideNextAa()), PeptideProteinMatch.HitType.UNKNOWN);
    }

    protected PeptideProteinMatch newProteinMatch(SearchHit searchHit) {

        return new PeptideProteinMatch(searchHit.getProtein(), Optional.<String>absent(), Optional.fromNullable(searchHit.getPeptidePrevAa()), Optional.fromNullable(searchHit.getPeptideNextAa()), PeptideProteinMatch.HitType.UNKNOWN);
    }

    protected void copyModInfo(PeptideMatch peptideMatch, ModificationInfo modInfo) {

        if (modInfo == null) return;

        if (modInfo.getModAminoacidMass() != null) {
            for (ModAminoacidMass modAaMass : modInfo.getModAminoacidMass()) {

                int position = modAaMass.getPosition().intValue() - 1;
                AminoAcid residue = peptideMatch.getSymbol(position);
                ModificationMatch modMatch = peptideMatch.addModificationMatch(position, adjustMass(modAaMass.getMass(), residue));
                resolveMod(modMatch);
            }
        }
        if (modInfo.getModNtermMass() != null) {

            ModificationMatch modMatch = peptideMatch.addModificationMatch(ModAttachment.N_TERM, adjustMass(modInfo.getModNtermMass(), ModAttachment.N_TERM));
            resolveMod(modMatch);
        }
        if (modInfo.getModCtermMass() != null) {

            ModificationMatch modMatch = peptideMatch.addModificationMatch(ModAttachment.C_TERM, adjustMass(modInfo.getModCtermMass(), ModAttachment.C_TERM));
            resolveMod(modMatch);
        }
    }

    protected void resolveMod(ModificationMatch modMatch) {

        Optional<Modification> modOpt = modMatchResolver.resolve(modMatch);
        if(modOpt.isPresent())
            modMatch.addPotentialModification(modOpt.get());
    }

    protected double adjustMass(Double mass, AminoAcid residue) {

        switch (modMassStorage) {

            case MOD_MASS:
                return mass;
            case AA_MASS_PLUS_MOD_MASS:
                return mass - residue.getMassOfMonomer();
            default:
                throw new IllegalStateException("Unknown mod mas storage " + modMassStorage);
        }
    }

    protected double adjustMass(Double mass, ModAttachment modAttachment) {

        if(modMassStorage == ModMassStorage.MOD_MASS)
            return mass;

        switch (modAttachment) {

            case N_TERM:
                return mass - PeriodicTable.H_MASS;
            case C_TERM:
                return mass - PeriodicTable.H_MASS * 2 + PeriodicTable.O_MASS;
            case SIDE_CHAIN:
            default:
                throw new IllegalStateException("Unknown mod attachment " + modAttachment);
        }
    }

    protected int intValue(BigInteger integer, int missingValue) {

        return integer == null ? missingValue : integer.intValue();
    }

    protected double parseDouble(String number) {

        return number.startsWith("+-") ? Double.parseDouble(number.substring(2)) : Double.parseDouble(number);
    }

    protected boolean parseBoolean(BigInteger value) {

        return value != null && value.intValue() == 1;
    }

    protected static class NamespaceRewriteDelegate extends StreamReaderDelegate {

        public NamespaceRewriteDelegate(XMLStreamReader reader) {

            super(reader);
        }

        @Override
        public String getNamespaceURI() {

            return "http://regis-web.systemsbiology.net/pepXML";
        }
    }
}
