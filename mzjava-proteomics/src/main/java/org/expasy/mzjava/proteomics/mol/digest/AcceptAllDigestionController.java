package org.expasy.mzjava.proteomics.mol.digest;

import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.mol.Protein;

/**
 * DigestionController that accepts all peptides and does not interrupt the digestion.
 *
 * @author fnikitin
 * @author Oliver Horlacher
 */
public class AcceptAllDigestionController implements DigestionController {

    /**
     * {@inheritDoc}
     * <p/>
     * Digestion never interrupted.
     */
    @Override
    public boolean interruptDigestion(Protein protein, int proteinCleavageSiteCount,
                                      int inclusiveLowerBoundIndex, int inclusiveUpperBoundIndex) {
        return false;
    }

    /**
     * {@inheritDoc}
     * <p/>
     * Make all digest peptides.
     */
    @Override
    public boolean makeDigest(Protein protein, int inclusiveProteinLowerIndex, int exclusiveProteinUpperIndex, int missedCleavagesCount) {

        return true;
    }

    /**
     * {@inheritDoc}
     * <p/>
     * Retain all digests.
     */
    @Override
    public boolean retainDigest(Protein protein, Peptide digest, int digestIndex) {

        return true;
    }

    /**
     * {@inheritDoc}
     * <p/>
     * Retain all semi digests.
     */
    @Override
    public boolean retainSemiDigest(Peptide digest) {

        return true;
    }
}
