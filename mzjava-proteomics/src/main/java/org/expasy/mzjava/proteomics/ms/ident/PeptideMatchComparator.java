package org.expasy.mzjava.proteomics.ms.ident;

/**
 * This comparator compare PeptideMatch in decreasing order given a score name
 *
 * @author fnikitin
 * Date: 2/28/14
 */

import com.google.common.base.Preconditions;

import java.io.Serializable;
import java.util.Comparator;

public class PeptideMatchComparator implements Comparator<PeptideMatch>, Serializable {

    private final String scoreName;

    public PeptideMatchComparator(String scoreName) {

        Preconditions.checkNotNull(scoreName);
        Preconditions.checkArgument(!scoreName.isEmpty());

        this.scoreName = scoreName;
    }

    @Override
    public int compare(PeptideMatch m1, PeptideMatch m2) {

        return Double.compare(m2.getScore(scoreName), m1.getScore(scoreName));
    }
}