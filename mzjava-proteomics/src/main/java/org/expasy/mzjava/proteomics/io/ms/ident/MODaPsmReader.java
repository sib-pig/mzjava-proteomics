package org.expasy.mzjava.proteomics.io.ms.ident;

import com.google.common.base.Optional;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.proteomics.ms.ident.PeptideMatch;
import org.expasy.mzjava.proteomics.ms.ident.PeptideProteinMatch;
import org.expasy.mzjava.proteomics.ms.ident.SpectrumIdentifier;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Logger;
import java.util.regex.Pattern;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class MODaPsmReader implements PsmReader {

    protected class PSMInfoParser {

        private final int nrOfPSMFields;
        private final PeptideInfoParser peptideInfoParser;

        public PSMInfoParser() {
            nrOfPSMFields = 7;
            peptideInfoParser = new PeptideInfoParser();
        }

        /*
        Read line
        >>NeutralPrecMass\tMassDiff\tScore\tProbability\tPeptide\tProtein\tAAPosition
        e.g.
        760.3902	0.0073	8	0.0553	R.KEMTPR.Q	sp|Q5T5P2	1420~1425
         */
        public PeptideMatch parse(String line, SpectrumIdentifier identifier) {
            String[] fields = getTabValues(line);

            if (fields.length!=nrOfPSMFields) {
                String errorMsg = getErrorMessage("");
                LOGGER.warning(errorMsg);
                throw new CorruptedPSMFileException(errorMsg);
            }

            try {
                double neutralMass = Double.parseDouble(fields[0]);
                String peptInfo = fields[4];

                peptideInfoParser.parse(peptInfo.substring(2, peptInfo.length() - 2));

                PeptideMatch peptideMatch = new PeptideMatch(peptideInfoParser.getSequence());
                peptideMatch.setNeutralPeptideMass(neutralMass);
                peptideMatch.addScore(MODA_SCORE, Double.parseDouble(fields[2]));
                peptideMatch.addScore(MODA_PROB, Double.parseDouble(fields[3]));
                peptideMatch.addScore("charge", identifier.getAssumedCharge().get());

                addFixedModifs(peptideMatch);

                double massDiff = identifier.getPrecursorNeutralMass().get() - peptideMatch.toPeptide().getMolecularMass();
                peptideMatch.setMassDiff(Optional.fromNullable(massDiff));
                peptideInfoParser.addModifs(peptideMatch, massDiff);

                peptideMatch.addProteinMatch(readProteinMatch(fields));

                return peptideMatch;

            } catch (NumberFormatException e) {

                String errorMsg = getErrorMessage(e.getMessage());
                LOGGER.warning(errorMsg);
                throw new CorruptedPSMFileException(errorMsg);
            }
        }

        private void addFixedModifs(PeptideMatch peptideMatch) {

            if (!termFixedModMap.isEmpty()) {
                if (termFixedModMap.containsKey(ModAttachment.N_TERM)) {
                    peptideMatch.addModificationMatch(ModAttachment.N_TERM,termFixedModMap.get(ModAttachment.N_TERM).iterator().next());
                }
                if (termFixedModMap.containsKey(ModAttachment.C_TERM)) {
                    peptideMatch.addModificationMatch(ModAttachment.C_TERM, termFixedModMap.get(ModAttachment.C_TERM).iterator().next());
                }
            }


            if (!sideChainFixedModMap.isEmpty()) {
                for (int i=0;i<peptideMatch.size();i++) {
                    AminoAcid aa = peptideMatch.getSymbol(i);
                    for (int j=0;j<fixedModResidues.size();j++) {
                        if (fixedModResidues.get(j).equals(aa)){
                            peptideMatch.addModificationMatch(i,sideChainFixedModMap.get(j).iterator().next());
                            break;
                        }
                    }
                }
            }
        }

        private PeptideProteinMatch readProteinMatch(String[] fields) {

            String peptInfo = fields[4];
            String prevAA = peptInfo.substring(0, 1);
            String nextAA = peptInfo.substring(peptInfo.length() - 1, peptInfo.length());
            String ac = fields[5];
            int sepIndex = fields[6].indexOf('~');

            if (sepIndex<0) {
                String errorMsg = getErrorMessage("");
                LOGGER.warning(errorMsg);
                throw new CorruptedPSMFileException(errorMsg);
            }

            int startPos = Integer.parseInt(fields[6].substring(0, sepIndex));
            int endPos =  Integer.parseInt(fields[6].substring(sepIndex+1));

            return new PeptideProteinMatch(ac, Optional.absent(), Optional.of(prevAA), Optional.of(nextAA), startPos, endPos, PeptideProteinMatch.HitType.UNKNOWN);

        }

    }

    protected class PeptideInfoParser {

        private int seqPos;
        private int sign;
        private int startMass;
        private char prevCh;

        private final List<AminoAcid> sequenceBuffer;
        private final List<Integer> modifPosBuffer;
        private final List<Integer> modifMassBuffer;


        public PeptideInfoParser() {

            seqPos = -1;
            sign = 0;
            startMass = 0;
            prevCh = '?';

            sequenceBuffer = new ArrayList<>();
            modifPosBuffer = new ArrayList<>();
            modifMassBuffer = new ArrayList<>();
        }

        public void parse(String peptideInfo) {

            seqPos = -1;
            sign = 0;
            startMass = 0;
            prevCh = '?';

            sequenceBuffer.clear();
            modifPosBuffer.clear();
            modifMassBuffer.clear();

            for (int i=0;i<peptideInfo.length();i++) {

                char ch = peptideInfo.charAt(i);
                try {
                    if (ch>='A' && ch<='Z') {
                        sequenceBuffer.add(AminoAcid.valueOf(Character.toString(ch)));
                        seqPos++;
                    }

                    if (ch>='A' && ch<='Z' && prevCh>='0' && prevCh<='9'){
                        int modifMass = sign*Integer.parseInt(peptideInfo.substring(startMass,i));
                        modifMassBuffer.add(modifMass);
                    }

                    if (i==peptideInfo.length()-1 && ch>='0' && ch<='9') {
                        int modifMass = sign*Integer.parseInt(peptideInfo.substring(startMass,i+1));
                        modifMassBuffer.add(modifMass);
                    }

                    if (ch=='-') {
                        modifPosBuffer.add(seqPos);
                        startMass = i+1;
                        sign = -1;
                    }

                    if (ch=='+') {
                        modifPosBuffer.add(seqPos);
                        startMass = i+1;
                        sign = 1;
                    }

                } catch (IllegalArgumentException e) {

                    String errorMsg = getErrorMessage(e.getMessage());
                    LOGGER.warning(errorMsg);
                    throw new CorruptedPSMFileException(errorMsg);
                }

                prevCh = ch;
            }
        }

        public List<AminoAcid> getSequence() {
            return sequenceBuffer;
        }

        public int getModMass() {
            int intModifMass = 0;
            for (Integer aModifMassBuffer : modifMassBuffer) {

                intModifMass += aModifMassBuffer;
            }

            return intModifMass;
        }

        public void addModifs(PeptideMatch peptideMatch, double massDiff) {
            double delta = 0.0;
            if (!modifPosBuffer.isEmpty()) delta =(massDiff-getModMass())/modifPosBuffer.size();

            for (int i = 0; i < modifPosBuffer.size(); i++) {

                peptideMatch.addModificationMatch(modifPosBuffer.get(i), modifMassBuffer.get(i)+delta);
            }

        }

    }

    protected class SpectrumInfoParser {

        private final int nrOfSpectrumFields;


        public SpectrumInfoParser() {
            nrOfSpectrumFields = 4;
        }

        /*
        Read line
        >>Spectrum.File.Name\tSpectrumIndex\tNeutralPrecMass\tCharge\tScanNr
        e.g.
        >>../query.mgf	1	760.3974	2	2831
         */
        public SpectrumIdentifier parse(String line) {

            String[] fields = getTabValues(line);

            if (fields.length<nrOfSpectrumFields) {
                String errorMsg = getErrorMessage("");
                LOGGER.warning(errorMsg);
                throw new CorruptedPSMFileException(errorMsg);
            }


            String spectrumName = getSpectrumName(fields);
            String spectrumFileName = getSpectrumFileName(fields);


            SpectrumIdentifier identifier = new SpectrumIdentifier(spectrumName);

            try {
                identifier.setIndex(Integer.parseInt(fields[1]));
                identifier.setAssumedCharge(Integer.parseInt(fields[3]));
                identifier.setPrecursorNeutralMass(Double.parseDouble(fields[2]));
                identifier.setSpectrumFile(spectrumFileName);
                // scan number is not present in all files
                if (fields.length == 5) {
                    identifier.addScanNumber(Integer.parseInt(fields[4]));
                }

                return identifier;
            } catch (NumberFormatException e) {

                String errorMsg = getErrorMessage(e.getMessage());
                LOGGER.warning(errorMsg);
                throw new CorruptedPSMFileException(errorMsg);
            }
        }

        private String getSpectrumName(String[] fields) {
            int startIdx = fields[0].lastIndexOf('/');
            if (startIdx<0) startIdx = fields[0].lastIndexOf('>');
            int endIdx = fields[0].lastIndexOf('.');

            String spectrumName = fields[0].substring(startIdx+1,endIdx);
            if (fields.length == 5) {
                spectrumName += "."+fields[4]+"."+fields[4]+"."+fields[3];

            } else {
                spectrumName += "."+fields[1]+"."+fields[1]+"."+fields[3];
            }

            return spectrumName;
        }

        private String getSpectrumFileName(String[] fields) {
            int startIdx = fields[0].lastIndexOf('>');
            return fields[0].substring(startIdx+1,fields[0].length());
        }
    }


    private static final Logger LOGGER = Logger.getLogger(MODaPsmReader.class.getName());
    public static final String MODA_SCORE = "MODaScore";
    public static final String MODA_PROB = "MODaProbability";

    protected int lineNr;
    protected String readFile;
    protected List<PeptideMatch> psmBuffer;
    protected SpectrumIdentifier spectrumIdentifier;
    protected List<AminoAcid> fixedModResidues;
    protected Multimap<Integer, Modification> sideChainFixedModMap;
    protected Multimap<ModAttachment, Modification> termFixedModMap;
    protected Pattern excludeProteinPattern = null;
    protected Pattern includeProteinPattern = null;
    protected final PSMInfoParser psmInfoParser;
    protected final SpectrumInfoParser spectrumInfoParser;



    public MODaPsmReader(List<AminoAcid> fixedModResidues,
                         Multimap<Integer, Modification> sideChainFixedModMap,
                         Multimap<ModAttachment, Modification> termFixedModMap) {

        this.fixedModResidues = new ArrayList<>(fixedModResidues);
        this.sideChainFixedModMap = ArrayListMultimap.create(sideChainFixedModMap);
        this.termFixedModMap = ArrayListMultimap.create(termFixedModMap);

        psmBuffer = new ArrayList<>();
        lineNr = 0;
        readFile = null;
        spectrumIdentifier = null;
        psmInfoParser = new PSMInfoParser();
        spectrumInfoParser = new SpectrumInfoParser();
    }

    public MODaPsmReader() {

        this.fixedModResidues = new ArrayList<>();
        this.sideChainFixedModMap = ArrayListMultimap.create();
        this.termFixedModMap = ArrayListMultimap.create();

        psmBuffer = new ArrayList<>();
        lineNr = 0;
        readFile = null;
        spectrumIdentifier = null;
        psmInfoParser = new PSMInfoParser();
        spectrumInfoParser = new SpectrumInfoParser();
    }

    private void clear() {
        psmBuffer.clear();
        lineNr = 0;
        readFile = null;
        spectrumIdentifier = null;
    }

    @Override
    public void parse(File file, PSMReaderCallback callback) {

        lineNr = 0;
        readFile = file.getName();

        try (BufferedReader psmFileReader = new BufferedReader(new FileReader(file))){

            while (psmFileReader.ready()) {
                String line = psmFileReader.readLine();
                lineNr++;

                if (line==null || line.isEmpty()) continue;

                if (line.startsWith(">>")) {
                    setRanks();
                    submit(callback);

                    spectrumIdentifier = spectrumInfoParser.parse(line);

                } else if (line.charAt(0)>='1' && line.charAt(0)<='9') {
                    psmBuffer.add(psmInfoParser.parse(line,spectrumIdentifier));

                }
            }
        } catch (IOException e) {
            throw new CorruptedPSMFileException(e.getMessage());
        }

        clear();
    }

    protected void setRanks() {

        if (psmBuffer.isEmpty()) return;

        psmBuffer.sort((peptideMatch1, peptideMatch2) -> {
            // decreasing order
            return Double.compare(peptideMatch2.getScore(MODA_SCORE), peptideMatch1.getScore(MODA_SCORE));
        });

        int rank = 0;
        double prevScore = psmBuffer.get(0).getScore(MODA_SCORE)+1;
        for (PeptideMatch psm : psmBuffer) {
            if (Double.compare(psm.getScore(MODA_SCORE),prevScore)<0) {
                prevScore = psm.getScore(MODA_SCORE);
                rank++;
            }
            psm.setRank(Optional.fromNullable(rank));
        }
    }

    protected void submit(PSMReaderCallback callback) {

        for (PeptideMatch psm : psmBuffer) {
            callback.resultRead(spectrumIdentifier, psm);
        }
        spectrumIdentifier = null;
        psmBuffer.clear();
    }

    protected String[] getTabValues(String line) {

        List<Integer> fieldIdx = new ArrayList<>();

        int endIdx = -1;
        while (true) {
            endIdx = line.indexOf('\t',endIdx+1);
            if (endIdx<0) break;
            fieldIdx.add(endIdx);
        }

        String[] fields = new String[fieldIdx.size()+1];
        int startIdx = 0;
        for (int i=0;i<fieldIdx.size();i++) {
            endIdx = fieldIdx.get(i);
            fields[i] = line.substring(startIdx,endIdx);
            startIdx = endIdx+1;
        }
        fields[fieldIdx.size()] = line.substring(startIdx,line.length());

        return fields;
    }

    private String getErrorMessage(String exceptionMsg) {
        return  "Possibly corrupted file " + readFile + " on line " + lineNr + ". " + exceptionMsg;
    }



    @Override
    public void parse(InputStream inputStream, PSMReaderCallback callback) {

        throw new UnsupportedOperationException("Can only parse csv from files");
    }

    @Override
    public void parse(Reader reader, PSMReaderCallback callback) {

        throw new UnsupportedOperationException("Can only parse csv from files");
    }

}
