
package org.expasy.mzjava.proteomics.io.ms.ident.bioml;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for groupType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="groupType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="note" type="{}noteType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="protein" type="{}proteinType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="group" type="{}groupType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="label" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}short" />
 *       &lt;attribute name="mh" type="{http://www.w3.org/2001/XMLSchema}float" />
 *       &lt;attribute name="z" type="{http://www.w3.org/2001/XMLSchema}byte" />
 *       &lt;attribute name="rt" type="{http://www.w3.org/2001/XMLSchema}float" />
 *       &lt;attribute name="expect" type="{http://www.w3.org/2001/XMLSchema}float" />
 *       &lt;attribute name="sumI" type="{http://www.w3.org/2001/XMLSchema}float" />
 *       &lt;attribute name="maxI" type="{http://www.w3.org/2001/XMLSchema}float" />
 *       &lt;attribute name="fI" type="{http://www.w3.org/2001/XMLSchema}float" />
 *       &lt;attribute name="act" type="{http://www.w3.org/2001/XMLSchema}byte" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "groupType", propOrder = {
    "note",
    "protein",
    "group"
})
public class GroupType {

    protected List<NoteType> note;
    protected List<ProteinType> protein;
    protected List<GroupType> group;
    @XmlAttribute(name = "label")
    protected String label;
    @XmlAttribute(name = "type")
    protected String type;
    @XmlAttribute(name = "id")
    protected Short id;
    @XmlAttribute(name = "mh")
    protected Double mh;
    @XmlAttribute(name = "z")
    protected Byte z;
    @XmlAttribute(name = "rt")
    protected Double rt;
    @XmlAttribute(name = "expect")
    protected Double expect;
    @XmlAttribute(name = "sumI")
    protected Double sumI;
    @XmlAttribute(name = "maxI")
    protected Double maxI;
    @XmlAttribute(name = "fI")
    protected Double fi;
    @XmlAttribute(name = "act")
    protected Byte act;

    /**
     * Gets the value of the note property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the note property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNote().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NoteType }
     *
     *
     */
    public List<NoteType> getNote() {
        if (note == null) {
            note = new ArrayList<NoteType>();
        }
        return this.note;
    }

    /**
     * Gets the value of the protein property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the protein property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProtein().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProteinType }
     * 
     * 
     */
    public List<ProteinType> getProtein() {
        if (protein == null) {
            protein = new ArrayList<ProteinType>();
        }
        return this.protein;
    }

    /**
     * Gets the value of the group property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the group property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGroup().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GroupType }
     * 
     * 
     */
    public List<GroupType> getGroup() {
        if (group == null) {
            group = new ArrayList<GroupType>();
        }
        return this.group;
    }

    /**
     * Gets the value of the label property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets the value of the label property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLabel(String value) {
        this.label = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setId(Short value) {
        this.id = value;
    }

    /**
     * Gets the value of the mh property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getMh() {
        return mh;
    }

    /**
     * Sets the value of the mh property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setMh(Double value) {
        this.mh = value;
    }

    /**
     * Gets the value of the z property.
     * 
     * @return
     *     possible object is
     *     {@link Byte }
     *     
     */
    public Byte getZ() {
        return z;
    }

    /**
     * Sets the value of the z property.
     * 
     * @param value
     *     allowed object is
     *     {@link Byte }
     *     
     */
    public void setZ(Byte value) {
        this.z = value;
    }

    /**
     * Gets the value of the rt property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getRt() {
        return rt;
    }

    /**
     * Sets the value of the rt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setRt(Double value) {
        this.rt = value;
    }

    /**
     * Gets the value of the expect property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getExpect() {
        return expect;
    }

    /**
     * Sets the value of the expect property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setExpect(Double value) {
        this.expect = value;
    }

    /**
     * Gets the value of the sumI property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getSumI() {
        return sumI;
    }

    /**
     * Sets the value of the sumI property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setSumI(Double value) {
        this.sumI = value;
    }

    /**
     * Gets the value of the maxI property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getMaxI() {
        return maxI;
    }

    /**
     * Sets the value of the maxI property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setMaxI(Double value) {
        this.maxI = value;
    }

    /**
     * Gets the value of the fi property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getFI() {
        return fi;
    }

    /**
     * Sets the value of the fi property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setFI(Double value) {
        this.fi = value;
    }

    /**
     * Gets the value of the act property.
     * 
     * @return
     *     possible object is
     *     {@link Byte }
     *     
     */
    public Byte getAct() {
        return act;
    }

    /**
     * Sets the value of the act property.
     * 
     * @param value
     *     allowed object is
     *     {@link Byte }
     *     
     */
    public void setAct(Byte value) {
        this.act = value;
    }

}
