/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.io.ms.spectrum.sptxt;

import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class SpectraStCommentParser implements SpectraLibCommentParser {

    @Override
    public boolean parseComment(String comment, LibrarySpectrumBuilder builder) {

        boolean handled = false;

        int length = comment.length();
        boolean inQuotes = false;
        int last = 0;
        for(int i = 0; i < length; i++){

            char current = comment.charAt(i);

            if(!inQuotes && current == ' ') {

                try {
                    handled = handled | handle(comment.substring(last, i), builder);
                } catch (Exception e) {

                    throw new IllegalStateException("Comment [" + comment + "] is not a valid comment.", e);
                }
                last = i + 1;
            } else if(current == '"') {

                inQuotes = !inQuotes;
            }
        }

        if(inQuotes)
            throw new IllegalStateException("Unclosed Quotes in : " + comment);
        try {
            handled = handled | handle(comment.substring(last, length), builder);
        } catch (Exception e) {

            throw new IllegalStateException("Comment [" + comment + "] is not a valid comment.", e);
        }

        return handled;
    }

    private boolean handle(String split, LibrarySpectrumBuilder builder) {

        int index = split.indexOf('=');
        if(index == -1) {
            throw new IllegalStateException("Split [" + split + "] does not contain =");
        }

        boolean handled;
        String tag = split.substring(0, index);
        String value = split.substring(index + 1);
        if(tag.equals("Mods")) {

            handled = parseModsComment(value, builder);
        } else if(tag.equals("Protein")) {

            handled = parseProteinComment(value, builder);
        } else {

            handled = parseUnknownCommentTag(tag, value, builder);
        }

        return handled;
    }

    protected boolean parseModsComment(String value, LibrarySpectrumBuilder builder) {

        String[] split = value.split("/");

        for(int i = 1; i < split.length; i++) { //starting at 1 because split 0 is the mod count

            String[] modSplit = split[i].split(",");

            int index = Integer.parseInt(modSplit[0]);

            if(index == -1)
                builder.addMod(ModAttachment.N_TERM, modSplit[2]);
            else if(index == -2)
                builder.addMod(ModAttachment.C_TERM, modSplit[2]);
            else
                builder.addMod(index, modSplit[2]);
        }

        return false;
    }

    protected boolean parseProteinComment(String value, LibrarySpectrumBuilder builder) {
        value = value.replaceAll("\"","");
        builder.addProteinAccessionNumber(value);
        return false;
    }

    protected boolean parseUnknownCommentTag(String tag, String value, LibrarySpectrumBuilder builder) {

        return false;
    }
}
