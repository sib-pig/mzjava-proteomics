package org.expasy.mzjava.proteomics.mol;

import org.expasy.mzjava.core.mol.SymbolSequence;

import java.util.Arrays;

import static com.google.common.base.Preconditions.checkElementIndex;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author fnikitin
 * @author Oliver Horlacher
 * Date: 12/2/13
 */
public class Protein implements SymbolSequence<AminoAcid> {

    private final AminoAcid[] aminoAcids;
    private final String accessionId;

    /**
     * Create an instance from a sequence string
     *
     * @param sequence the string to create a Protein from
     * @throws IllegalArgumentException if the amino-acid sequence is invalid
     */
    public Protein(String accessionId, String sequence) {

        checkNotNull(sequence);
        checkNotNull(accessionId);

        aminoAcids = new AminoAcid[sequence.length()];
        this.accessionId = accessionId;

        for (int i = 0; i < sequence.length(); i++) {

            aminoAcids[i] = AminoAcid.valueOf(sequence.charAt(i));
        }
    }

    /**
     * Create an instance from a list of AminoAcids
     * @param aas the list of amino-acids
     */
    public Protein(String accessionId, AminoAcid... aas) {

        checkNotNull(aas);
        checkNotNull(accessionId);

        final int length = aas.length;
        aminoAcids = new AminoAcid[length];
        System.arraycopy(aas, 0, aminoAcids, 0, length);
        this.accessionId = accessionId;
    }

    private Protein(String accessionId, AminoAcid[] aas, int length) {

        checkNotNull(aas);
        checkNotNull(accessionId);

        aminoAcids = new AminoAcid[length];
        System.arraycopy(aas, 0, aminoAcids, 0, length);
        this.accessionId = accessionId;
    }

    public String getAccessionId() {

        return accessionId;
    }

    /**
     * Create a PeptideBuilder that has the sequence initialised by copying this Protein's amino acids starting at
     * <code>fromInclusive</code> to <code>toExclusive</code>.
     *
     * @param fromInclusive from index protein
     * @param toExclusive to index protein
     * @return a new PeptideBuilder
     */
    public PeptideBuilder getPeptideBuilder(int fromInclusive, int toExclusive) {

        return new PeptideBuilder(aminoAcids, fromInclusive, toExclusive);
    }

    @Override
    public AminoAcid getSymbol(int index) {

        checkElementIndex(index, size());

        return aminoAcids[index];
    }

    @Override
    public String toSymbolString() {

        StringBuilder buff = new StringBuilder();

        for (AminoAcid aa : aminoAcids) {

            buff.append(aa.getSymbol());
        }

        return buff.toString();
    }

    @Override
    public int size() {

        return aminoAcids.length;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Protein protein = (Protein) o;

        return Arrays.equals(aminoAcids, protein.aminoAcids) && accessionId.equals(protein.accessionId);
    }

    @Override
    public int hashCode() {

        int result = Arrays.hashCode(aminoAcids);
        result = 31 * result + accessionId.hashCode();
        return result;
    }

    public String toString() {

        return toSymbolString();
    }

    public final static class Builder {

        private String accessionId;
        private AminoAcid[] aminoAcids;
        private int size = 0;

        public Builder(int size) {

            aminoAcids = new AminoAcid[size];
        }

        public Builder(String sequence) {

            aminoAcids = new AminoAcid[sequence.length()];
            setSequence(sequence);
        }

        public Builder setAccessionId(String accessionId){

            this.accessionId = accessionId;
            return this;
        }

        public Builder setSequence(String sequence) {

            size = 0;

            appendSequence(sequence);

            return this;
        }

        public Builder appendSequence(String sequence) {

            if (aminoAcids.length < size + sequence.length()) {

                AminoAcid[] tmp = new AminoAcid[size + sequence.length()];
                System.arraycopy(aminoAcids, 0, tmp, 0, size);

                aminoAcids = tmp;
            }

            for (int i = 0; i < sequence.length(); i++) {

                aminoAcids[size++] = AminoAcid.valueOf(sequence.charAt(i));
            }

            return this;
        }

        public Protein build(){

            return new Protein(accessionId, aminoAcids, size);
        }

        public void reset(){

            size = 0;
            accessionId = null;
        }

        public String getAccessionId() {

            return accessionId;
        }
    }
}
