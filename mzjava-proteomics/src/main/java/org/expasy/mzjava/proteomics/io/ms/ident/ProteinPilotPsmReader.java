/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.io.ms.ident;


import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import org.expasy.mzjava.utils.RegexConstants;
import org.expasy.mzjava.proteomics.mol.modification.CompositeModResolver;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.proteomics.mol.modification.ModificationResolver;
import org.expasy.mzjava.proteomics.ms.ident.ModificationMatch;
import org.expasy.mzjava.proteomics.ms.ident.PeptideMatch;
import org.expasy.mzjava.proteomics.ms.ident.SpectrumIdentifier;
import org.expasy.mzjava.core.ms.spectrum.TimeUnit;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

/**
 * A peptide spectrum match reader for ProteinPilot Peptide Summary Overview tsv file.
 * <p/>
 * <h2>Unknown modifications</h2>
 * By default, an IllegalStateException runtime exception is thrown if the reader meets an unknown modification.
 * If this crash is not an option, user should give an implementation of UnknownModListener to the reader.
 * The {@code makePeptideMatch} method will return Optional.absent()).
 *
 * @author fnikitin
 * @author Oliver Horlacher
 *         <p/>
 *         Date: 4/9/13
 */
public class ProteinPilotPsmReader extends AbstractPsmCsvReader {

    private final ModificationResolver modResolver;
    private UnknownModListener listener;

    /**
     * Default constructor with default resolver ProteinPilotModificationResolver
     */
    public ProteinPilotPsmReader() {

        this(new ProteinPilotModificationResolver());
    }

    /**
     * @param modResolver the modification resolver
     */
    public ProteinPilotPsmReader(ModificationResolver modResolver) {

        this.modResolver = new CompositeModResolver(modResolver, new ProteinPilotModificationResolver());
    }

    /**
     * Set the UnknownModListener
     *
     * @param listener the listener that handle unknown modifications
     */
    public void setUnknownModListener(UnknownModListener listener) {

        Preconditions.checkNotNull(listener);

        this.listener = listener;
    }

    @Override
    protected SpectrumIdentifier getSpectrumId(ResultSet results) throws SQLException {

        int charge = results.getInt("Theor z");
        double precMw = results.getDouble("Prec MW");
        String spectrum = results.getString("Spectrum");
        double rt = results.getDouble("Time");

        SpectrumIdentifier identifier = new SpectrumIdentifier(spectrum);
        identifier.setAssumedCharge(charge);
        identifier.setPrecursorNeutralMass(precMw);
        identifier.addRetentionTime(rt, TimeUnit.MINUTE);

        return identifier;
    }

    @Override
    protected Collection<String> getAccessionNumbers(ResultSet results) throws SQLException {

        return parseAccessionNumbers(results.getString("Accessions"));
    }

    @Override
    protected Optional<PeptideMatch> makePeptideMatch(ResultSet results) throws SQLException {

        PeptideMatch peptideMatch = new PeptideMatch(results.getString("Sequence"));
        String unimods = results.getString("Modifications");
        if (unimods == null || unimods.length() == 0)
            return Optional.of(peptideMatch);

        for (String mod : unimods.split("; ")) {

            String[] modAndPos = mod.split("@");

            // Methylthio(C) -> Methylthio
            String unimodId = modAndPos[0].split("\\(")[0];

            Optional<Modification> modOpt = modResolver.resolve(unimodId);
            if (modOpt.isPresent()) {

                if (modAndPos[1].matches(RegexConstants.INTEGER)) {

                    int index = Integer.parseInt(modAndPos[1]) - 1;
                    peptideMatch.addModificationMatch(index, modOpt.get());
                } else if ("N-term".equals(modAndPos[1])) {

                    peptideMatch.addModificationMatch(ModAttachment.N_TERM, modOpt.get());
                } else if ("C-term".equals(modAndPos[1])) {

                    int index = peptideMatch.size() - 1;
                    peptideMatch.addModificationMatch(ModAttachment.C_TERM, new ModificationMatch(
                                    modOpt.get(),
                                    peptideMatch.getSymbol(index),
                                    index,
                                    ModAttachment.C_TERM
                            )
                    );
                }
            } else if (listener != null) {

                listener.handleUnknownMod(results.getString("Sequence"), mod, getLineNumber());
                return Optional.absent();
            } else {

                throw new IllegalStateException("modification \"" + unimodId + "\" not found!");
            }
        }

        return Optional.of(peptideMatch);
    }

    @Override
    protected void setValues(PeptideMatch peptideMatch, ResultSet results) throws SQLException {

        double deltaMass = results.getDouble("dMass");
        double score = results.getDouble("Sc");

        peptideMatch.setRank(Optional.fromNullable(0));
        peptideMatch.setMassDiff(Optional.fromNullable(deltaMass));

        peptideMatch.addScore("score", score);
    }

    private List<String> parseAccessionNumbers(String headers) {

        List<String> accessions = Lists.newArrayList();

        if (headers != null && headers.length() > 0) {

            for (String header : headers.split(";")) {

                accessions.add(header.split("\\|")[1]);
            }
        }
        return accessions;
    }
}
