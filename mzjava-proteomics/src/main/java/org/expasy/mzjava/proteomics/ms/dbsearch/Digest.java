/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.ms.dbsearch;

import com.google.common.collect.Iterators;
import org.expasy.mzjava.core.mol.Weighable;
import org.expasy.mzjava.proteomics.mol.Peptide;

import java.util.*;

/**
 * A wrapper for Peptide that has a set to hold the ids of the proteins that
 * the peptide belongs to.
 * <p>
 * This protein id's are stored in a custom collection that has been optimised
 * to use a small amount of ram and cpu.
 *
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public final class Digest implements Weighable {

    private final Peptide peptide;
    private final ProteinIdSet proteinIds = new ProteinIdSet();

    Digest(Peptide peptide) {

        this.peptide = peptide;
    }

    Digest(Peptide peptide, String proteinId) {

        this(peptide);

        proteinIds.doAdd(proteinId);
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Digest digest = (Digest) o;
        compact();
        digest.compact();

        return peptide.equals(digest.peptide) && proteinIds.equals(digest.proteinIds);
    }

    @Override
    public int hashCode() {

        int result = peptide.hashCode();
        result = 31 * result + proteinIds.hashCode();
        return result;
    }

    /**
     * Add a proteinId.
     *
     * @param proteinId the id to add
     */
    public void addProteinId(String proteinId){

        proteinIds.doAdd(proteinId);
    }

    /**
     * Returns an Immutable set containing the protein ids
     *
     * @return an Immutable set containing the protein ids
     */
    public Set<String> getProteinIds() {

        proteinIds.compact();
        return proteinIds;
    }

    @Override
    public double getMolecularMass() {

        return peptide.getMolecularMass();
    }

    /**
     * Return the peptide.
     *
     * @return the peptide
     */
    public Peptide getPeptide() {

        return peptide;
    }

    /**
     * Free up any resources that are no longer needed.
     */
    public void compact() {

        proteinIds.compact();
    }

    @Override
    public String toString() {

        return "Digest{" +
                "peptide=" + peptide +
                ", proteinIds=" + proteinIds +
                '}';
    }

    /**
     * A lightweight mostly immutable set that is backed by an array.
     * <p>
     * ProteinIdSet is mostly immutable because it can be modified by
     * the Digest class.  Ids are added by appending to the array. To create
     * a proper set duplicates need to be removed. This is done by calling compact.
     * <p>
     * Before any set methods are called the ProteinIdSet is compacted
     */
    private static class ProteinIdSet extends AbstractSet<String> {

        private String[] proteinIds;
        private int size = 0;
        private static final int INITIAL_SIZE = 10;
        private boolean compacted = false;

        private ProteinIdSet() {

            proteinIds = new String[INITIAL_SIZE];
        }

        private void compact(){

            if(compacted)
                return;

            if(size <= 1) {

                proteinIds = new String[]{proteinIds[0]};
                return;
            }

            Set<String> ids = new HashSet<String>(size);
            int arraySize = size;
            //noinspection ManualArrayToCollectionCopy
            for(int i = 0; i < arraySize; i++) {

                ids.add(proteinIds[i]);
            }

            this.proteinIds = new String[ids.size()];

            int index = 0;
            for(String id : ids) {

                proteinIds[index++] = id;
            }

            size = ids.size();

            compacted = true;
        }

        private void doAdd(String id) {

            if (size > proteinIds.length - 1) {

                String[] tmp = new String[proteinIds.length + INITIAL_SIZE];

                System.arraycopy(proteinIds, 0, tmp, 0, proteinIds.length);

                proteinIds = tmp;
            }

            proteinIds[size++] = id;
        }

        @Override
        public int size() {

            return size;
        }

        @Override
        public boolean contains(Object o) {

            if(!compacted)
                compact();

            for(String id : proteinIds) {

                if(id.equals(o))
                    return true;
            }

            return false;
        }

        @Override
        public Iterator<String> iterator() {

            if(!compacted)
                compact();

            return Iterators.forArray(proteinIds);
        }

        @Override
        public boolean add(String s) {

            throw new UnsupportedOperationException();
        }

        @Override
        public boolean remove(Object o) {

            throw new UnsupportedOperationException();
        }

        @Override
        public boolean addAll(Collection<? extends String> c) {

            throw new UnsupportedOperationException();
        }

        @Override
        public boolean retainAll(Collection<?> c) {

            throw new UnsupportedOperationException();
        }

        @Override
        public boolean removeAll(Collection<?> c) {

            throw new UnsupportedOperationException();
        }

        @Override
        public void clear() {

            throw new UnsupportedOperationException();
        }
    }
}
