
package org.expasy.mzjava.proteomics.io.ms.ident.bioml;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.expasy.mzjava.proteomics.io.ms.ident.bioml package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Bioml_QNAME = new QName("", "bioml");
    private final static QName _PeptideTypeDomain_QNAME = new QName("", "domain");
    private final static QName _DomainTypeAa_QNAME = new QName("", "aa");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.expasy.mzjava.proteomics.io.ms.ident.bioml
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link BiomlType }
     * 
     */
    public BiomlType createBiomlType() {
        return new BiomlType();
    }

    /**
     * Create an instance of {@link PeptideType }
     * 
     */
    public PeptideType createPeptideType() {
        return new PeptideType();
    }

    /**
     * Create an instance of {@link GroupType }
     * 
     */
    public GroupType createGroupType() {
        return new GroupType();
    }

    /**
     * Create an instance of {@link NoteType }
     * 
     */
    public NoteType createNoteType() {
        return new NoteType();
    }

    /**
     * Create an instance of {@link ProteinType }
     * 
     */
    public ProteinType createProteinType() {
        return new ProteinType();
    }

    /**
     * Create an instance of {@link DomainType }
     * 
     */
    public DomainType createDomainType() {
        return new DomainType();
    }

    /**
     * Create an instance of {@link FileType }
     * 
     */
    public FileType createFileType() {
        return new FileType();
    }

    /**
     * Create an instance of {@link AaType }
     * 
     */
    public AaType createAaType() {
        return new AaType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BiomlType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "bioml")
    public JAXBElement<BiomlType> createBioml(BiomlType value) {
        return new JAXBElement<BiomlType>(_Bioml_QNAME, BiomlType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DomainType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "domain", scope = PeptideType.class)
    public JAXBElement<DomainType> createPeptideTypeDomain(DomainType value) {
        return new JAXBElement<DomainType>(_PeptideTypeDomain_QNAME, DomainType.class, PeptideType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "aa", scope = DomainType.class)
    public JAXBElement<AaType> createDomainTypeAa(AaType value) {
        return new JAXBElement<AaType>(_DomainTypeAa_QNAME, AaType.class, DomainType.class, value);
    }

}
