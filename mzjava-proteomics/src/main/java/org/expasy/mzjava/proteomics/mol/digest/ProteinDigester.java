/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.mol.digest;


import com.google.common.base.Optional;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.SetMultimap;
import gnu.trove.list.TIntList;
import gnu.trove.list.array.TIntArrayList;
import org.expasy.mzjava.core.mol.SymbolSequence;
import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.mol.PeptideBuilder;
import org.expasy.mzjava.proteomics.mol.Protein;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;


/**
 * A {@code ProteinDigester} is defined by a name and a cleavage site matcher
 * that match the sites to be digested.
 *
 * <h4>Terminal Modification</h4>
 * <p>
 * In rare case, a peptidase may modify the extremity of released digests. As an
 * example, CNBr modify the end terminal Methionine of the C-terminal digest in
 * homoserine lactone.
 * </p>
 *
 * This object is immutable.
 *
 * There are 2 modes of digestion:
 * <dl>
 *     <dt>Mode.STANDARD</dt>
 *      <dd>no-missed-cleavage and missed-cleavage digests produced depending on MC param</dd>
 *     <dt>Mode.SEMI</dt>
 *      <dd>a supplementary step generates all the semi digests from the no-missed-cleavage digests (http://thegpm.org/TANDEM/api/pcsemi.html)</dd>
 * </dl>
 *
 * @author nikitin
 * @author Oliver Horlacher
 *
 */
public class ProteinDigester {

    public enum CleavageSiteIteration { FORWARD, REVERSE }

    /** the cleavage site pattern */
    private final CleavageSiteFinder cleavageSiteFinder;
    private final int missedCleavageMax;
    private final boolean semi;
    private final CleavageSiteIteration iterMode;
    private final SetMultimap<AminoAcid, Modification> fixedMods;
    private final DigestionController digestProcessor;

    public static class Builder {

        private final CleavageSiteFinder cleavageSiteFinder;

        private int missedCleavageMax = 0;
        private boolean semi = false;
        private DigestionController digestProcessor = new AcceptAllDigestionController();

        private CleavageSiteIteration iterMode = CleavageSiteIteration.FORWARD;

        private final SetMultimap<AminoAcid, Modification> fixedMods;

        public Builder(Protease protease) {

            checkNotNull(protease);

            cleavageSiteFinder = protease;
            fixedMods = HashMultimap.create();
        }

        public Builder(CleavageSiteMatcher cleavageSiteMatcher) {

            checkNotNull(cleavageSiteMatcher);

            cleavageSiteFinder = new CleavageSiteFinderImpl(cleavageSiteMatcher);
            fixedMods = HashMultimap.create();
        }

        public Builder semi() {

            this.semi = true;
            return this;
        }

        public Builder cleavageSiteIteration(CleavageSiteIteration mode) {

            checkNotNull(mode);

            this.iterMode = mode;
            return this;
        }

        public Builder controller(DigestionController cb) {

            checkNotNull(cb);

            this.digestProcessor = cb;

            return this;
        }

        public Builder missedCleavageMax(int max) {

            checkArgument(max >= 0);

            missedCleavageMax = max;
            return this;
        }

        public Builder addFixedMod(AminoAcid target, Modification mod) {

            checkNotNull(target);
            checkNotNull(mod);

            fixedMods.put(target, mod);

            return this;
        }

        public ProteinDigester build() {

            return new ProteinDigester(this);
        }
    }

    public ProteinDigester(Builder builder) {

        cleavageSiteFinder = builder.cleavageSiteFinder;
        missedCleavageMax = builder.missedCleavageMax;
        semi = builder.semi;
        iterMode = builder.iterMode;
        fixedMods = builder.fixedMods;
        digestProcessor = builder.digestProcessor;
    }

    /**
     * Digest {@code protein}
     *
     * @param protein the protein to digest.
     *
     * @return the digests.
     */
    public List<Peptide> digest(Protein protein) {

        // container for future digests
        List<Peptide> digests = Lists.newArrayList();

        digest(protein, digests);

        return digests;
    }

    /**
     * Digest {@code protein} given a container for digests
     *
     * @param protein the protein to digest
     * @param digests a list of digests
     *
     * @return the digests.
     */
    public Collection<Peptide> digest(Protein protein, Collection<Peptide> digests) {

        checkNotNull(protein);

        TIntList bounds = new TIntArrayList();
        bounds.add(0);
        // 1. detect all digests bounds
        cleavageSiteFinder.find(protein, bounds);
        bounds.add(protein.size());

        // 2. generate combinations of all digest given MC#
        generateDigests(protein, bounds, digests);

        // 3. SEMI digestion for MC#0 digests
        if (semi) {

            List<Peptide> semiDigests = Lists.newArrayList();

            for (Peptide digest : digests) {

                if (cleavageSiteFinder.countCleavageSites(digest) == 0)
                    semiDigests.addAll(generateSemiDigests(digest));
            }

            digests.addAll(semiDigests);
        }

        return digests;
    }

    private List<Peptide> generateSemiDigests(Peptide digestedPeptide) {

        List<Peptide> semiDigests = Lists.newArrayList();

        for (int i = 0 ; i < digestedPeptide.size() - 1;  i++) {

            Optional<Peptide> nSemiDigest =
                    newOptSemiDigestedPeptide(digestedPeptide, 0, i+1);

            Optional<Peptide> cSemiDigest =
                    newOptSemiDigestedPeptide(digestedPeptide, i+1, digestedPeptide.size());

            if (nSemiDigest.isPresent())
                semiDigests.add(nSemiDigest.get());

            if (cSemiDigest.isPresent())
                semiDigests.add(cSemiDigest.get());
        }


        return semiDigests;
    }

    private void generateDigests(Protein protein, TIntList sites, Collection<Peptide> digests) {

        Iterator<IntIntervalCursor> iterator;

        int rangeMax = (missedCleavageMax==Integer.MAX_VALUE) ? missedCleavageMax : missedCleavageMax+1;

        switch (iterMode) {

            case FORWARD:
                iterator = new ForwardIntIntervalIterator(0, sites.size()-1, rangeMax);
                break;
            case REVERSE:
                iterator = new ReverseIntIntervalIterator(0, sites.size()-1, rangeMax);
                break;
            default:
                throw new IllegalStateException("Unknown iteration mode " + iterMode);
        }

        while (iterator.hasNext()) {

            IntIntervalCursor cursor = iterator.next();

            int currentMissedCleavages = cursor.getCurrentRange()-1;
            int digestLowerIndex = sites.get(cursor.getCurrentLowerBound());
            int digestUpperIndex = sites.get(cursor.getCurrentUpperBound());

            if (!digestProcessor.makeDigest(protein, digestLowerIndex, digestUpperIndex, currentMissedCleavages))
                continue;

            PeptideBuilder peptideBuilder = protein.getPeptideBuilder(digestLowerIndex, digestUpperIndex);
            // adding mods
            addModificationsToAll(peptideBuilder, protein, digestLowerIndex, digestUpperIndex, fixedMods);
            addPotentialModsCausedByDigester(peptideBuilder, protein, digestLowerIndex, digestUpperIndex);

            Peptide peptide = peptideBuilder.build();

            if (digestProcessor.retainDigest(protein, peptide, digestLowerIndex))
                digests.add(peptide);

            if (digestProcessor.interruptDigestion(protein, sites.size(), cursor.getCurrentLowerBound(), cursor.getCurrentUpperBound()))
                break;
        }
    }

    private Optional<Peptide> newOptSemiDigestedPeptide(Peptide parent, int leftBound, int rightBound) {

        Peptide digest = new Peptide(parent, leftBound, rightBound);

        if (digestProcessor.retainSemiDigest(digest))

            return Optional.of(digest);

        return Optional.absent();
    }

    /**
     * Some ProteinDigester can modify digest's N or C terminal
     */
    private void addPotentialModsCausedByDigester(PeptideBuilder peptideBuilder, Protein protein, int leftBoundIndex, int rightBoundIndex) {

        CleavageSiteMatcher cs = cleavageSiteFinder.getCleavageSiteMatcher();

        boolean addCterm = cs.hasCtermMod() && rightBoundIndex < protein.size() - 1;
        boolean addNterm = cs.hasNtermMod() && leftBoundIndex > 0;

        if(!addCterm && !addNterm)
            return;

        // mod potentially done by protease if not the last digest
        if (addCterm) {

            peptideBuilder.addModification(ModAttachment.C_TERM, cs.getCtermMod().get());
        }

        // mod potentially added by protease if not the first digest
        if (addNterm)
            peptideBuilder.addModification(ModAttachment.N_TERM, cs.getNtermMod().get());
    }

    private void addModificationsToAll(PeptideBuilder peptideBuilder, Protein protein, int start, int end, Multimap<AminoAcid, Modification> fixedModifications) {

        if (fixedMods.isEmpty())
            return;

        for(AminoAcid targetAA : fixedModifications.keys()) {

            Collection<Modification> modifications = fixedModifications.get(targetAA);
            for (int i = start; i < end; i++) {

                AminoAcid aa = protein.getSymbol(i);

                if (aa.equals(targetAA)) {

                    final int index = i - start;
                    for (Modification mod : modifications) {

                        peptideBuilder.addModification(index, mod);
                    }
                }
            }
        }
    }

    public int countCleavageSites(SymbolSequence<AminoAcid> aaSeq) {

        return cleavageSiteFinder.countCleavageSites(aaSeq);
    }
}
