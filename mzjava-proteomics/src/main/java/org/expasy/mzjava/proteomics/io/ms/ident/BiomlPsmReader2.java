package org.expasy.mzjava.proteomics.io.ms.ident;

import com.google.common.base.Optional;
import org.expasy.mzjava.core.ms.spectrum.ScanNumberList;
import org.expasy.mzjava.core.ms.spectrum.TimeUnit;
import org.expasy.mzjava.proteomics.io.ms.ident.bioml.*;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.ms.ident.PeptideMatch;
import org.expasy.mzjava.proteomics.ms.ident.PeptideProteinMatch;
import org.expasy.mzjava.proteomics.ms.ident.PeptideProteinMatch.HitType;
import org.expasy.mzjava.proteomics.ms.ident.SpectrumIdentifier;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.sax.SAXSource;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @author Thibault Robin
 * @version 1.0
 */
public class BiomlPsmReader2 implements PsmReader {
    private final String decoyTag;

    private List<PeptideProteinMatch> proteinMatches = new ArrayList<>();

    public BiomlPsmReader2(String decoyTag) {
        this.decoyTag = decoyTag;
    }

    @Override
    public void parse(File file, PSMReaderCallback callback) {
        try {
            parse(new BufferedInputStream(new FileInputStream(file)), callback);
        } catch (FileNotFoundException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void parse(InputStream inputStream, PSMReaderCallback callback) {
        try {
            parse(new InputSource(inputStream), callback);
        } catch (SAXException | JAXBException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void parse(Reader reader, PSMReaderCallback callback) {
        try {
            parse(new InputSource(reader), callback);
        } catch (SAXException | JAXBException e) {
            throw new IllegalStateException(e);
        }
    }

    public void parse(InputSource inputSource, PSMReaderCallback callback) throws SAXException, JAXBException {
        List<Double> nTermMods = new ArrayList<>();
        List<Double> cTermMods = new ArrayList<>();

        XMLReader xmlReader = XMLReaderFactory.createXMLReader();

        SAXSource saxSource = new SAXSource(xmlReader, inputSource);
        Unmarshaller unmarshaller = JAXBContext.newInstance(BiomlType.class).createUnmarshaller();

        BiomlType biomlType = (BiomlType) ((JAXBElement) unmarshaller.unmarshal(saxSource)).getValue();

        //compile the regex patterns outside of the loops
        Pattern pattern1 = Pattern.compile(",");
        Pattern pattern2 = Pattern.compile(" ");
        Pattern pattern3 = Pattern.compile("\\.");
        Pattern pattern4 = Pattern.compile("=");
        Pattern pattern5 = Pattern.compile("\"");

        //add default X!Tandem N-Term modifications
        nTermMods.add(-18.01056);
        nTermMods.add(-17.02655);

        //Go look for the N-Term & C-Term modifications that were set for the search
        for (NoteType noteType : biomlType.getGroup().get(biomlType.getGroup().size() - 2).getNote()) {
            if (noteType.getLabel().equals("refine, potential N-terminus modifications")) {
                for (String mod : pattern1.split(noteType.getValue())) {
                    if (!mod.equals("")) {
                        nTermMods.add(roundDouble(Double.valueOf(mod.substring(0, mod.length() - 2)), 5));
                    }
                }
            }
            if (noteType.getLabel().equals("refine, potential C-terminus modifications")) {
                for (String mod : pattern1.split(noteType.getValue())) {
                    if (!mod.equals("")) {
                        cTermMods.add(roundDouble(Double.valueOf(mod.substring(0, mod.length() - 2)), 5));
                    }
                }
            }
        }

        for (GroupType groupType : biomlType.getGroup()) {

            if (groupType.getLabel().equalsIgnoreCase("unused input parameters")) continue;
            if (groupType.getLabel().equalsIgnoreCase("input parameters")) continue;
            if (groupType.getLabel().equalsIgnoreCase("performance parameters")) continue;

            SpectrumIdentifier identifier = null;

            List<PeptideMatch> peptideMatches = new ArrayList<>();

            for (ProteinType proteinType : groupType.getProtein()) {

                String[] array = pattern2.split(groupType.getGroup().get(1).getNote().get(0).getValue());

                ScanNumberList scanNumbers = new ScanNumberList();
                scanNumbers.add(Integer.valueOf(pattern3.split(array[0])[1]));
                scanNumbers.add(Integer.valueOf(pattern3.split(array[0])[2]));

                identifier = new SpectrumIdentifier(array[0]);
                identifier.addRetentionTime(Double.valueOf(pattern4.split(array[5])[1]), TimeUnit.SECOND);
                identifier.addScanNumbers(scanNumbers);
                identifier.setAssumedCharge(Integer.valueOf(pattern3.split(array[0])[3]));
                identifier.setSpectrumFile(pattern5.split(array[1])[1]);
//            identifier.setPrecursorNeutralMass(array);


                for (Serializable serializable1 : proteinType.getPeptide().getContent()) {
                    if (serializable1.getClass() == JAXBElement.class) {
                        DomainType domainType = (DomainType) ((JAXBElement) serializable1).getValue();


                        PeptideMatch peptideMatch = new PeptideMatch(domainType.getSeq());
                        peptideMatch.setRank(Optional.of(1));
                        peptideMatch.setNumMissedCleavages(Optional.of(domainType.getMissedCleavages().intValue()));
                        peptideMatch.addScore("hyperscore", domainType.getHyperscore());
                        peptideMatch.addScore("nextscore", domainType.getNextscore());
                        peptideMatch.addScore("deltascore", domainType.getDelta());
                        peptideMatch.addScore("evalue", domainType.getExpect());
                        peptideMatches.add(peptideMatch);

                        //look for the decoy tag in protein headers to determine the hit type
                        HitType hitType;
                        if (proteinType.getNote().getValue().contains(decoyTag)) {
                            hitType = HitType.DECOY;
                        } else {
                            hitType = HitType.TARGET;
                        }

                        Optional<String> previousAA;
                        int last = domainType.getPre().length() - 1;
                        if (domainType.getPre().isEmpty() || domainType.getPre().charAt(last) == '[') {
                            previousAA = Optional.absent();
                        } else {
                            previousAA = Optional.of(domainType.getPre().substring(last));
                        }

                        Optional<String> nextAA;
                        if (domainType.getPost().isEmpty() || domainType.getPost().charAt(0) == ']') {
                            nextAA = Optional.absent();
                        } else {
                            nextAA = Optional.of(domainType.getPost().substring(0, 1));
                        }

                        PeptideProteinMatch proteinMatch = new PeptideProteinMatch(
                                proteinType.getNote().getValue(),
                                Optional.of(proteinType.getFile().getURL()),
                                previousAA,
                                nextAA,
                                domainType.getStart() - 1,
                                domainType.getEnd() - 1,
                                hitType
                        );
                        proteinMatches.add(proteinMatch);

                        for (Serializable serializable2 : domainType.getContent()) {
                            if (serializable2.getClass().equals(JAXBElement.class)) {
                                AaType aaType = (AaType) ((JAXBElement) serializable2).getValue();
                                int index = aaType.getAt() - 1;

                                if (index == 0 && nTermMods.contains(aaType.getModified())) {
                                    peptideMatch.addModificationMatch(ModAttachment.N_TERM,aaType.getModified());
                                } else if (index == peptideMatch.toBarePeptide().size() - 1 &&
                                        cTermMods.contains(aaType.getModified())) {
                                    peptideMatch.addModificationMatch(ModAttachment.C_TERM,aaType.getModified());
                                } else {
                                    peptideMatch.addModificationMatch(index - domainType.getStart() + 1,aaType.getModified());
                                }
                            }
                        }
                    }
                }
            }
            if (!peptideMatches.isEmpty()) {

                for (int i=0;i< peptideMatches.size();i++) {

                    peptideMatches.get(i).addProteinMatches(proteinMatches);
                    callback.resultRead(identifier, peptideMatches.get(i));
                }
                proteinMatches.clear();
            }
        }
    }

    private Double roundDouble(double number, double precision) {
        precision = Math.pow(10, precision);
        number = Math.round(number * precision);

        return number / precision;
    }

    public List<PeptideProteinMatch> getProteinMatches() {
        return proteinMatches;
    }
}
