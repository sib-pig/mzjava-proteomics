/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.ms.ident;

import com.google.common.base.Optional;
import org.expasy.mzjava.core.ms.spectrum.*;

import java.util.Objects;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class SpectrumIdentifier {

    private final String spectrum;
    private Optional<String> name = Optional.absent();
    private final ScanNumberList scanNumbers = new ScanNumberList();
    private final RetentionTimeList retentionTimes = new RetentionTimeList();
    private Optional<Double> precursorNeutralMass = Optional.absent();
    private Optional<Double> precursorIntensity = Optional.absent();
    private Optional<Double> precursorMz = Optional.absent();
    private Optional<Integer> assumedCharge = Optional.absent();
    private Optional<Integer> index = Optional.absent();
    private Optional<String> spectrumFile = Optional.absent();

    public SpectrumIdentifier(String spectrum) {

        checkNotNull(spectrum);
        this.spectrum = spectrum;
    }

    public Optional<String> getSpectrumFile() {

        return spectrumFile;
    }

    public void setSpectrumFile(String spectrumFile) {

        this.spectrumFile = Optional.fromNullable(spectrumFile);
    }

    public String getSpectrum() {

        return spectrum;
    }

    public ScanNumberList getScanNumbers() {

        return scanNumbers;
    }

    public void addScanNumbers(ScanNumberList scanNumbers) {

        this.scanNumbers.addAll(scanNumbers);
    }

    public void addScanNumber(ScanNumber scanNumber) {

        this.scanNumbers.add(scanNumber);
    }

    public void addScanNumber(int scanNumber) {

        this.scanNumbers.add(scanNumber);
    }

    public void addRetentionTime(double time, TimeUnit timeUnit) {

        retentionTimes.add(new RetentionTimeDiscrete(time, timeUnit));
    }

    public void addRetentionTime(RetentionTime retentionTime) {

        retentionTimes.add(retentionTime);
    }

    public RetentionTimeList getRetentionTimes() {

        return retentionTimes;
    }

    public Optional<Double> getPrecursorNeutralMass() {

        return precursorNeutralMass;
    }

    public void setPrecursorNeutralMass(double precursorNeutralMass) {

        this.precursorNeutralMass = Optional.fromNullable(precursorNeutralMass);
    }

    public Optional<Double> getPrecursorMz() {

        return precursorMz;
    }

    public void setPrecursorMz(double precursorMz) {

        this.precursorMz = Optional.fromNullable(precursorMz);
    }

    public Optional<Integer> getAssumedCharge() {

        return assumedCharge;
    }

    public void setAssumedCharge(int assumedCharge) {

        this.assumedCharge = Optional.fromNullable(assumedCharge);
    }

    public Optional<Integer> getIndex() {

        return index;
    }

    public void setIndex(int index) {

        this.index = Optional.fromNullable(index);
    }

    public Optional<Double> getPrecursorIntensity() {

        return precursorIntensity;
    }

    public void setPrecursorIntensity(double precursorIntensity) {

        this.precursorIntensity = Optional.fromNullable(precursorIntensity);
    }

    public Optional<String> getName() {

        return name;
    }

    public void setName(String name) {

        this.name = Optional.fromNullable(name);
    }

    @Override
    public String toString() {

        return "SpectrumIdentifier{" +
                "spectrum='" + spectrum + '\'' +
                ", name='" + name + '\'' +
                ", scanNumbers=" + scanNumbers +
                ", retentionTimes=" + retentionTimes +
                ", precursorNeutralMass=" + precursorNeutralMass +
                ", precursorIntensity=" + precursorIntensity +
                ", precursorMz=" + precursorMz +
                ", assumedCharge=" + assumedCharge +
                ", index=" + index +
                ", spectrumFile=" + spectrumFile +
                '}';
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (!(o instanceof SpectrumIdentifier)) return false;

        SpectrumIdentifier that = (SpectrumIdentifier) o;

        return Objects.equals(assumedCharge.orNull(), that.assumedCharge.orNull()) &&
                Objects.equals(index.orNull(), that.index.orNull()) &&
                Objects.equals(name.orNull(), that.name.orNull()) &&
                Objects.equals(precursorIntensity.orNull(), that.precursorIntensity.orNull()) &&
                Objects.equals(precursorMz.orNull(), that.precursorMz.orNull()) &&
                Objects.equals(precursorNeutralMass.orNull(), that.precursorNeutralMass.orNull()) &&
                Objects.equals(retentionTimes, that.retentionTimes) &&
                Objects.equals(scanNumbers, that.scanNumbers) &&
                Objects.equals(spectrum, that.spectrum) &&
                Objects.equals(spectrumFile.orNull(), that.spectrumFile.orNull());
    }

    @Override
    public int hashCode() {

        int result = spectrum.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + scanNumbers.hashCode();
        result = 31 * result + retentionTimes.hashCode();
        result = 31 * result + precursorNeutralMass.hashCode();
        result = 31 * result + precursorIntensity.hashCode();
        result = 31 * result + precursorMz.hashCode();
        result = 31 * result + assumedCharge.hashCode();
        result = 31 * result + index.hashCode();
        result = 31 * result + spectrumFile.hashCode();
        return result;
    }

    public void addRetentionTime(RetentionTimeList retentionTimes) {

        for (RetentionTime retentionTime : retentionTimes) {

            this.retentionTimes.add(retentionTime);
        }
    }

    public void addScanNumber(ScanNumberList scanNumbers) {

        for (ScanNumber scanNumber : scanNumbers) {

            this.scanNumbers.add(scanNumber);
        }
    }
}
