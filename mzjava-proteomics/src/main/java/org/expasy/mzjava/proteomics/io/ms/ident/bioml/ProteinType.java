
package org.expasy.mzjava.proteomics.io.ms.ident.bioml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for proteinType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="proteinType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="note" type="{}noteType"/>
 *         &lt;element name="file" type="{}fileType"/>
 *         &lt;element name="peptide" type="{}peptideType"/>
 *       &lt;/sequence>
 *       &lt;attribute name="expect" type="{http://www.w3.org/2001/XMLSchema}float" />
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}float" />
 *       &lt;attribute name="uid" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="label" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="sumI" type="{http://www.w3.org/2001/XMLSchema}float" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "proteinType", propOrder = {
    "note",
    "file",
    "peptide"
})
public class ProteinType {

    @XmlElement(required = true)
    protected NoteType note;
    @XmlElement(required = true)
    protected FileType file;
    @XmlElement(required = true)
    protected PeptideType peptide;
    @XmlAttribute(name = "expect")
    protected Double expect;
    @XmlAttribute(name = "id")
    protected Double id;
    @XmlAttribute(name = "uid")
    protected Integer uid;
    @XmlAttribute(name = "label")
    protected String label;
    @XmlAttribute(name = "sumI")
    protected Double sumI;

    /**
     * Gets the value of the note property.
     * 
     * @return
     *     possible object is
     *     {@link NoteType }
     *     
     */
    public NoteType getNote() {
        return note;
    }

    /**
     * Sets the value of the note property.
     * 
     * @param value
     *     allowed object is
     *     {@link NoteType }
     *     
     */
    public void setNote(NoteType value) {
        this.note = value;
    }

    /**
     * Gets the value of the file property.
     * 
     * @return
     *     possible object is
     *     {@link FileType }
     *     
     */
    public FileType getFile() {
        return file;
    }

    /**
     * Sets the value of the file property.
     * 
     * @param value
     *     allowed object is
     *     {@link FileType }
     *     
     */
    public void setFile(FileType value) {
        this.file = value;
    }

    /**
     * Gets the value of the peptide property.
     * 
     * @return
     *     possible object is
     *     {@link PeptideType }
     *     
     */
    public PeptideType getPeptide() {
        return peptide;
    }

    /**
     * Sets the value of the peptide property.
     * 
     * @param value
     *     allowed object is
     *     {@link PeptideType }
     *     
     */
    public void setPeptide(PeptideType value) {
        this.peptide = value;
    }

    /**
     * Gets the value of the expect property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getExpect() {
        return expect;
    }

    /**
     * Sets the value of the expect property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setExpect(Double value) {
        this.expect = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setId(Double value) {
        this.id = value;
    }

    /**
     * Gets the value of the uid property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getUid() {
        return uid;
    }

    /**
     * Sets the value of the uid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setUid(Integer value) {
        this.uid = value;
    }

    /**
     * Gets the value of the label property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets the value of the label property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLabel(String value) {
        this.label = value;
    }

    /**
     * Gets the value of the sumI property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getSumI() {
        return sumI;
    }

    /**
     * Sets the value of the sumI property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setSumI(Double value) {
        this.sumI = value;
    }

}
