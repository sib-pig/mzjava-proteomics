/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.io.ms.spectrum;


import org.expasy.mzjava.utils.RegexConstants;
import org.expasy.mzjava.core.io.ms.spectrum.AbstractMsReader;
import org.expasy.mzjava.proteomics.mol.AAMassCalculator;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessorChain;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;

import java.io.*;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * A simple DTA reader.
 * <p/>
 * http://www.matrixscience.com/help/data_file_help.html#DTA
 *
 * @author nikitin
 * @author Oliver Horlacher
 * @version 1.0
 */
public final class DtaReader extends AbstractMsReader<PeakAnnotation, MsnSpectrum> {

    private static final Pattern PAT_PREC =
            Pattern.compile("(" + RegexConstants.REAL + ")\\s+("
                    + RegexConstants.INTEGER + ").*");

    private static final Pattern PAT_PEAK =
            Pattern.compile("(" + RegexConstants.REAL + ")\\s+("
                    + RegexConstants.REAL + ").*");

    public DtaReader(File file, PeakList.Precision precision) throws IOException {

        this(new FileReader(file), file.toURI(), precision);
    }

    public DtaReader(Reader reader, PeakList.Precision precision) throws IOException {

        this(reader, URI.create("unknown"), precision, new PeakProcessorChain<>());
    }

    public DtaReader(Reader reader, URI spectraSource, PeakList.Precision precision) throws IOException {

        this(reader, spectraSource, precision, new PeakProcessorChain<>());
    }

    public DtaReader(Reader reader, URI spectraSource, PeakList.Precision precision, PeakProcessorChain<PeakAnnotation> processorChain) throws IOException {

        super(reader, spectraSource, precision, processorChain);
    }

    @Override
    protected String parseHeader(ParseContext context) throws IOException {

        return "";
    }

    @Override
    protected MsnSpectrum parseNextEntry(ParseContext context) throws IOException {

        List<String> buffer;

        // get the next spectrum from the file
        buffer = nextBuffer();

        // nothing more to read
        if (buffer.isEmpty()) {

            return null;
        }

        double masses[] = new double[buffer.size() - 1];
        double intensities[] = new double[buffer.size() - 1];

        String precursorBuffer = buffer.remove(0);

        Matcher matcher = PAT_PREC.matcher(precursorBuffer);

        MsnSpectrum spectrum = new MsnSpectrum(precision);
        spectrum.setSpectrumIndex(context.getNumberOfParsedEntry());
        spectrum.addScanNumber(spectrum.getSpectrumIndex() + 1);
        spectrum.setSpectrumSource(context.getSource());

        spectrum.setMsLevel(2);

        // 1. get precursor mass + charge
        // the precursor peptide mass is an MH+ value independent
        // of the charge state followed by a charge state.
        if (matcher.matches()) {

            // get the precursor neutral mass M = MH+ - H+
            double neutralMass = Double.parseDouble(matcher.group(1)) - 1;
            int z = Integer.parseInt(matcher.group(2));

            double mz = AAMassCalculator.getInstance().calculateMz(neutralMass, z, IonType.p);

            Peak precursor = spectrum.getPrecursor();
            precursor.setValues(mz, 1, z);
        } else {

            throw new IOException(
                    "cannot extract precursor m/z charge from [" + precursorBuffer
                            + "]"
            );
        }

        // 2. get MS2 peaks
        for (int i = 0; i < buffer.size(); i++) {

            matcher = PAT_PEAK.matcher(buffer.get(i));

            if (matcher.matches()) {

                masses[i] = Double.parseDouble(matcher.group(1));
                intensities[i] = Double.parseDouble(matcher.group(2));
            } else {

                throw new IOException(
                        "cannot extract fragment m/z intensity from ["
                                + buffer.get(i) + "]"
                );
            }
        }

        addPeaksToSpectrum(spectrum, masses, intensities, masses.length);

        return spectrum;
    }

    /**
     * @return the next buffer lines.
     * @throws IOException if reading problem.
     */
    private List<String> nextBuffer() throws IOException {

        LineNumberReader contextReader = getContext().getCurrentReader();
        String line;

        List<String> buf = new ArrayList<>();

        while ((line = contextReader.readLine()) != null) {

            if (line.trim().length() == 0) {

                if (!buf.isEmpty()) {

                    return buf;
                }
            } else if (line.charAt(0) != '#') {

                buf.add(line);
            }
        }
        return buf;
    }

    /**
     * Overridden so that the compiler knows that MsnSpectra are returned by next().
     * If this method is missing the compiler thinks Spectrum are returned
     *
     * @inheritDoc
     */
    @Override
    public MsnSpectrum next() throws IOException {

        return super.next();
    }
}
