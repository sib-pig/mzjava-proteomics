package org.expasy.mzjava.proteomics.io.ms.spectrum;


import com.google.common.io.Files;
import org.expasy.mzjava.core.io.ms.spectrum.MgfReader;
import org.expasy.mzjava.core.io.ms.spectrum.MzxmlReader;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.io.IterativeReader;

import java.io.File;
import java.io.IOException;


/**
 * This class is a MS reader factory that provides new instance of MsReaders.
 *
 * @author nikitin
 * @version 1.0
 */
public class MsReaderFactory {

    public MsReaderFactory() {

    }

    public static IterativeReader<MsnSpectrum> newMsnSpectraReader(File file, PeakList.Precision precision) throws IOException {

        String extension = Files.getFileExtension(file.getName()).toLowerCase();

        if ("dta".equals(extension)) {

            return new DtaReader(file, precision);
        } else if ("mgf".equals(extension)) {

            return new MgfReader(file, precision);
        } else if ("mzxml".equals(extension)) {

            return new MzxmlReader(file, precision);
        } else {

            throw new IllegalStateException("Do not have a reader that can read " + extension + " files");
        }
    }

    public static IterativeReader<PeptideConsensusSpectrum> newLibrarySpectraReader(File file, PeakList.Precision precision) throws IOException {

        String extension = Files.getFileExtension(file.getName()).toLowerCase();

        if ("msp".equals(extension)) {

            return MspReader.newBuilder(file, precision).useSkipAnnotationResolver().build();
        } else if ("sptxt".equals(extension)) {

            return SptxtReader.newBuilder(file, precision).useSkipAnnotationResolver().build();
        } else {

            throw new IllegalStateException("Do not have a reader that can read " + extension + " files");
        }
    }
}
