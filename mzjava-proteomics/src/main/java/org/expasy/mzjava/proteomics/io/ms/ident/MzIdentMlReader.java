/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.io.ms.ident;

import com.google.common.base.Optional;
import org.expasy.mzjava.core.ms.spectrum.RetentionTimeList;
import org.expasy.mzjava.core.ms.spectrum.ScanNumberList;
import org.expasy.mzjava.core.ms.spectrum.TimeUnit;
import org.expasy.mzjava.proteomics.io.ms.ident.mzidentml.v110.*;
import org.expasy.mzjava.proteomics.mol.AAMassCalculator;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.proteomics.mol.modification.ModificationResolver;
import org.expasy.mzjava.proteomics.mol.modification.unimod.UnimodModificationResolver;
import org.expasy.mzjava.proteomics.ms.ident.ModificationMatch;
import org.expasy.mzjava.proteomics.ms.ident.PeptideMatch;
import org.expasy.mzjava.proteomics.ms.ident.PeptideProteinMatch;
import org.expasy.mzjava.proteomics.ms.ident.SpectrumIdentifier;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.sax.SAXSource;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class MzIdentMlReader implements PsmReader {

    public static final String SCAN_NUMBER_CV = "MS:1000797";
    public static final String RETENTION_TIME_CV = "MS:1001114";
    public static final String TITLE_CV = "MS:1000796";

    private final Map<String, String> cvScoreMap = new HashMap<>();

    private final Pattern patternInteger = Pattern.compile("(\\d+)");
    private Map<String, PeptideType> peptideRefMap = null;
    // the first position of the String corresponds to the AC, the second to the databaseRef
    private Map<String, List<String[]>> peptideIDProteinMap = null;
    private Map<String, List<PeptideEvidenceType>> peptideIDPositionMap = null;

    private final ModificationResolver modResolver;

    public MzIdentMlReader() {

        this(new UnimodModificationResolver());
    }

    public MzIdentMlReader(ModificationResolver modResolver) {

        this.modResolver = modResolver;

        cvScoreMap.put("MS:1001330", "X\\!Tandem:expect");
        cvScoreMap.put("MS:1001331", "X\\!Tandem:hyperscore");
        cvScoreMap.put("MS:1001328", "OMSSA:evalue");
        cvScoreMap.put("MS:1001329", "OMSSA:pvalue");
        cvScoreMap.put("MS:1002049", "MS-GF:RawScore");
        cvScoreMap.put("MS:1002050", "MS-GF:DeNovoScore");
        cvScoreMap.put("MS:1002052", "MS-GF:SpecEValue");
        cvScoreMap.put("MS:1002053", "MS-GF:EValue");
        cvScoreMap.put("MS:1002054", "MS-GF:QValue");
        cvScoreMap.put("MS:1002055", "MS-GF:PepQValue");
        cvScoreMap.put("MS:1001171", "Mascot:score");
        cvScoreMap.put("MS:1001172", "Mascot:expectation value");
        cvScoreMap.put("MS:1002012", "Mascot:delta score");
    }

    @Override
    public void parse(File file, PSMReaderCallback callback) {

        try {

            parse(new BufferedInputStream(new FileInputStream(file)), callback);
        } catch (FileNotFoundException e) {

            throw new IllegalStateException(e);
        }
    }

    @Override
    public void parse(Reader reader, PSMReaderCallback callback) {

        try {
            parse(new InputSource(reader), callback);
        } catch (SAXException | JAXBException e) {
            throw new IllegalStateException(e);
        }
    }

    public void parse(InputStream inputStream, PSMReaderCallback callback) {

        try {
            parse(new InputSource(inputStream), callback);
        } catch (SAXException | JAXBException e) {
            throw new IllegalStateException(e);
        }
    }

    public void parse(InputSource is, PSMReaderCallback callback) throws SAXException, JAXBException {

        peptideRefMap = null;

        Class<MzIdentMLType> docClass = MzIdentMLType.class;

        XMLReader reader = XMLReaderFactory.createXMLReader();
        reader.setFeature("http://apache.org/xml/features/allow-java-encodings", true);

        NamespaceFilterXMLReader filter = new NamespaceFilterXMLReader("http://psidev.info/psi/pi/mzIdentML/1.1");
        filter.setParent(reader);

        SAXSource ss = new SAXSource(filter, is);

        String packageName = docClass.getPackage().getName();
        JAXBContext jc = JAXBContext.newInstance(packageName);
        Unmarshaller u = jc.createUnmarshaller();
        MzIdentMLType doc = (MzIdentMLType) ((JAXBElement) u.unmarshal(ss)).getValue();

        readPeptideIDProteinMap(doc);
        readPeptideIDProteinPositionMap(doc);

        SpectraDataType spectraDataType = doc.getDataCollection().getInputs().getSpectraData().get(0);

        String msFileLocation = spectraDataType.getLocation();

        String fileName = null;
        if (msFileLocation != null) {
            File tmp = new File(msFileLocation);
            fileName = tmp.getName();
        }

        for (SpectrumIdentificationListType identList : doc.getDataCollection().getAnalysisData().getSpectrumIdentificationList()) {

            for (SpectrumIdentificationResultType idResult : identList.getSpectrumIdentificationResult()) {

                CVParamMap idResultCVParamMap = convertToCVMap(idResult.getParamGroup());

                ScanNumberList scanNumbers = idResultCVParamMap.getScanNumberList(SCAN_NUMBER_CV);
                RetentionTimeList retentionTimes = idResultCVParamMap.getRetentionTimeList(RETENTION_TIME_CV);
                int index = extractInteger(idResult.getSpectrumID());
                String title = idResultCVParamMap.getString(TITLE_CV, "");



                for (SpectrumIdentificationItemType idItem : idResult.getSpectrumIdentificationItem()) {

                    CVParamMap idItemCVParamMap = convertToCVMap(idItem.getParamGroup());

                    SpectrumIdentifier identifier = new SpectrumIdentifier(title);

                    identifier.setSpectrumFile(fileName);
                    identifier.addRetentionTime(retentionTimes);
                    identifier.addScanNumber(scanNumbers);
                    identifier.setIndex(index);
                    identifier.setAssumedCharge(idItem.getChargeState());
                    identifier.setPrecursorNeutralMass(AAMassCalculator.getInstance().calculateNeutralMolecularMass(idItem.getExperimentalMassToCharge(), idItem.getChargeState()));

                    PeptideMatch peptideMatch = readPeptide(idItem.getPeptideRef(), doc.getSequenceCollection());

                    for (String cv : cvScoreMap.keySet()) {
                        if (idItemCVParamMap.containsKey(cv)) {
                            peptideMatch.addScore(cvScoreMap.get(cv), idItemCVParamMap.getDouble(cv, Double.NaN));
                        }
                    }
                    peptideMatch.setRank(Optional.fromNullable(idItem.getRank()));
                    peptideMatch.setMassDiff(Optional.fromNullable(idItem.getExperimentalMassToCharge() - idItem.getCalculatedMassToCharge()));
                    peptideMatch.setRejected(Optional.fromNullable(!idItem.isPassThreshold()));

                    callback.resultRead(identifier, peptideMatch);
                }
            }
        }
    }

    protected void readPeptideIDProteinMap(MzIdentMLType doc) {

        SequenceCollectionType seqCollection = doc.getSequenceCollection();
        if (seqCollection == null) return;

        List<DBSequenceType> proteins = seqCollection.getDBSequence();
        if (proteins == null || proteins.isEmpty()) return;

        Map<String, String[]> protIDACMap = new HashMap<>();
        for (DBSequenceType protein : proteins) {
            // the first position corresponds to the AC, the second to the databaseRef
            String[] protAcAndRef = {protein.getAccession(), protein.getSearchDatabaseRef()};
            protIDACMap.put(protein.getId(), protAcAndRef);
        }

        List<PeptideEvidenceType> peptideProteinMap = seqCollection.getPeptideEvidence();
        if (peptideProteinMap == null || peptideProteinMap.isEmpty()) return;

        peptideIDProteinMap = new HashMap<>();
        for (PeptideEvidenceType entry : peptideProteinMap) {

            if (peptideIDProteinMap.containsKey(entry.getPeptideRef())) {
                List<String[]> acAndSearchDbs = peptideIDProteinMap.get(entry.getPeptideRef());
                acAndSearchDbs.add(protIDACMap.get(entry.getDBSequenceRef()));
            } else {
                List<String[]> acAndSearchDbs = new ArrayList<>();
                acAndSearchDbs.add(protIDACMap.get(entry.getDBSequenceRef()));
                peptideIDProteinMap.put(entry.getPeptideRef(), acAndSearchDbs);
            }
        }
    }

    protected void readPeptideIDProteinPositionMap(MzIdentMLType doc) {

        SequenceCollectionType seqCollection = doc.getSequenceCollection();
        if (seqCollection == null) return;

        List<PeptideEvidenceType> peptideEvidences = seqCollection.getPeptideEvidence();
        if (peptideEvidences == null || peptideEvidences.isEmpty()) return;

        peptideIDPositionMap = new HashMap<>();
        for (PeptideEvidenceType entry : peptideEvidences) {
            if(peptideIDPositionMap.containsKey(entry.getPeptideRef())){
                peptideIDPositionMap.get(entry.getPeptideRef()).add(entry);
            }else {
                List<PeptideEvidenceType> newList = new ArrayList<>();
                newList.add(entry);
                peptideIDPositionMap.put(entry.getPeptideRef(), newList);
            }
        }
    }

    protected PeptideMatch readPeptide(String peptideRef, SequenceCollectionType sequenceCollection) {

        if (peptideRefMap == null) {
            peptideRefMap = new HashMap<>();

            for (PeptideType peptideType : sequenceCollection.getPeptide()) {

                peptideRefMap.put(peptideType.getId(), peptideType);
            }
        }

        if (peptideRefMap.containsKey(peptideRef)) {

            PeptideType peptideType = peptideRefMap.get(peptideRef);
            PeptideMatch peptideMatch = new PeptideMatch(peptideType.getPeptideSequence());

            for (ModificationType modType : peptideType.getModification()) {

                Double deltaMass = modType.getMonoisotopicMassDelta();
                List<CVParamType> modTypes = modType.getCvParam();

                int position = modType.getLocation() - 1;
                ModAttachment modAttachment;
                if (position < 0) {

                    modAttachment = ModAttachment.N_TERM;
                    position = 0;
                } else if (position == peptideType.getPeptideSequence().length()) {

                    modAttachment = ModAttachment.C_TERM;
                    position = peptideMatch.size() - 1;
                } else {

                    modAttachment = ModAttachment.SIDE_CHAIN;
                }

                ModificationMatch modificationMatch = new ModificationMatch(
                        deltaMass,
                        peptideMatch.getSymbol(position),
                        position,
                        modAttachment
                );

                if (!modTypes.isEmpty()) {
                    Optional<Modification> modOpt = modResolver.resolve(modTypes.get(0).getName());
                    if (modOpt.isPresent()) modificationMatch.addPotentialModification(modOpt.get());
                    else throw new IllegalStateException("Cannot find a modification for " + modTypes.get(0).getName());
                }

                if (modAttachment == ModAttachment.N_TERM || modAttachment == ModAttachment.C_TERM) {

                    peptideMatch.addModificationMatch(modAttachment, modificationMatch);
                } else {

                    peptideMatch.addModificationMatch(position, modificationMatch);
                }
            }

            if (peptideIDProteinMap != null && peptideIDProteinMap.containsKey(peptideRef)) {

                List<PeptideEvidenceType> peptideEvidenceTypeList = peptideIDPositionMap.get(peptideRef);
                List<String[]> references = peptideIDProteinMap.get(peptideRef);

                if(peptideEvidenceTypeList.size() != references.size()) throw new IllegalStateException("number of peptide evidences is incoherent");

                for(int i=0; i<peptideEvidenceTypeList.size(); i++){

                    String[] acAndRef = references.get(i);
                    PeptideEvidenceType peptideEvidenceType = peptideEvidenceTypeList.get(i);
                    PeptideProteinMatch.HitType hitType = peptideEvidenceType.isIsDecoy() ? PeptideProteinMatch.HitType.DECOY : PeptideProteinMatch.HitType.TARGET;
                    peptideMatch.addProteinMatch(new PeptideProteinMatch(acAndRef[0], Optional.fromNullable(acAndRef[1]), Optional.fromNullable(peptideEvidenceType.getPre()), Optional.fromNullable(peptideEvidenceType.getPost()), peptideEvidenceType.getStart(), peptideEvidenceType.getEnd(), hitType));

                }

            }
            return peptideMatch;
        } else {

            throw new IllegalStateException("Do not have peptide " + peptideRef);
        }
    }

    private int extractInteger(String value) {

        Matcher matcher = patternInteger.matcher(value);

        if (matcher.find()) {

            return Integer.parseInt(matcher.group(0));
        } else {

            throw new IllegalStateException("Cannot extract integer from " + value);
        }
    }

    private CVParamMap convertToCVMap(List<AbstractParamType> paramGroup) {

        if (paramGroup == null || paramGroup.isEmpty()) return new CVParamMap();

        CVParamMap paramMap = new CVParamMap();
        for (AbstractParamType paramType : paramGroup) {

            if (paramType instanceof CVParamType) {

                CVParamType cvParamType = (CVParamType) paramType;
                paramMap.put(cvParamType.getAccession(), cvParamType);
            }
        }

        return paramMap;
    }

    /**
     * Provides convenient methods to parse values and provide default values if a key is missing
     */
    private static class CVParamMap extends HashMap<String, CVParamType> {

        public String getString(String accession, String defaultValue) {

            CVParamType paramType = get(accession);
            if (paramType == null) return defaultValue;
            else return paramType.getValue();
        }

        public ScanNumberList getScanNumberList(String accession) {

            CVParamType cvParamType = get(accession);
            if (cvParamType == null) return new ScanNumberList();

            try {
                int scanNumber = Integer.parseInt(cvParamType.getValue());

                return new ScanNumberList(scanNumber);
            } catch (NumberFormatException e) {

                throw new UnsupportedOperationException("Cannot yet parse scan number like " + cvParamType.getValue(), e);
            }
        }

        public RetentionTimeList getRetentionTimeList(String accession) {

            CVParamType cvParamType = get(accession);
            if (cvParamType == null) return new RetentionTimeList();

            String unit = cvParamType.getUnitName();

            TimeUnit timeUnit;
            if ("second".equals(unit)) {

                timeUnit = TimeUnit.SECOND;
            } else {

                throw new UnsupportedOperationException("Cannot parse units = " + unit);
            }

            double time;
            try {
                time = Double.parseDouble(cvParamType.getValue());
            } catch (NumberFormatException e) {

                throw new UnsupportedOperationException("Time values of " + cvParamType.getValue() + " are not yet suported", e);
            }

            RetentionTimeList retentionTimes = new RetentionTimeList();
            retentionTimes.add(time, timeUnit);

            return retentionTimes;
        }

        public double getDouble(String accession, double defaultValue) {

            CVParamType cvParamType = get(accession);
            if (cvParamType == null) return defaultValue;

            return Double.parseDouble(cvParamType.getValue());
        }
    }
}
