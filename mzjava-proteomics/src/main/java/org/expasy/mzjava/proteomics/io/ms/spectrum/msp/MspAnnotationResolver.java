/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.io.ms.spectrum.msp;

import com.google.common.base.Optional;
import com.google.common.base.Splitter;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.core.mol.Mass;
import org.expasy.mzjava.core.mol.NumericMass;
import org.expasy.mzjava.core.mol.PeriodicTable;
import org.expasy.mzjava.core.ms.spectrum.FragmentType;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.proteomics.io.ms.spectrum.sptxt.AnnotationResolver;
import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.mol.PeptideFragment;
import org.expasy.mzjava.proteomics.ms.spectrum.PepFragAnnotation;
import org.expasy.mzjava.proteomics.ms.spectrum.PepLibPeakAnnotation;
import org.expasy.mzjava.utils.RegexConstants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parses annotations from Msp files. The documentation for the msp format can be found in "NIST Library of Peptide
 * Ion Fragmentation Spectra: Version June 2006" and is:
 * <p/>
 * <h1>Documentation from NIST</h1>
 * Each annotation field begins with one of the following characters: y, b, a, p, I, or ?. The first three denote
 * ion types formed in peptide fragmentation. p denotes the parent ion, I denotes an immonium ion or internal ion
 * and ? indicates that the peak cannot be assigned to a known cleavage type (including common neutral losses).
 * Symbols y, b or a are followed by an integer, representing the cleaved position in the parent peptide, and then,
 * for neutral loss ions, by –n, where n is the neutral loss in Daltons (note – this is actual mass units, water
 * loss is always -18, for example). 13C isotopic peaks are denoted with a following i (x is used in place of
 * possible isotopic peaks that do not have preceding non-isotopic peaks). If the peak assignment is suspect, an
 * asterisk follows. Multiply charged ions are represented by ^c, where c is the charge. A ‘/’ follows, followed by
 * the difference between measured m/z and theoretical (exact) m/z. Internal ions are represents as Int/seq/ where
 * seq is the sequence of the internal ion. In qtof spectra, immonium ions are represented as IXY, where X is the
 * parent amino acid and Y are letters A,B,C.. used to distinguish different immonium products of the amino acid X.
 * <p/>
 * For these assignments, a tolerance of 0.8 m/z was used. In cases where more than one assignment is given peaks
 * are ordered with the following preference: neutral loss from parent (X-n), y, b or a with no neutral loss, y, b or a
 * with -18 or -17 neutral loss, other losses. If a second assignment is possible, a second, comma-separated full
 * assignment is given. For two assignments of the same class described above, the m/z closer to theoretical is
 * preferred. Also, if multiple losses from the parent are possible, only the one closest to theoretical m/z is kept.
 * <p/>
 * For consensus spectra, a space follows the assignment(s), after which is given the number of replicate spectra
 * having that peak and the minimum number of spectra required for that peak to have been reported – these numbers
 * are separated by a ‘/’. A space follows, after which the median deviation of m/z (in 1/100th of an m/z) of the
 * peaks in the original spectra used to create that peak.
 * <p/>
 * <h1>Examples</h1>
 * <pre>
 * "? 13/18 0.4"
 * "a3/-0.02 20/20 1.9"
 * "a3i/0.98 20/20 0.4"
 * </pre>
 * <h1>Notes</h1>
 * <h2>Isotopes</h2>
 * Fragments that are isotopes have an i or x after the residue number. From the documentation it is not clear how
 * to distinguish between the 1st, 2nd, 3rd etc. isotopes.  From looking at .msp files it appears that the difference
 * between measured m/z and theoretical m/z is relative to the fragment without a isotope. Therefore b6x/0.98 can be
 * interpreted as the 1st isotope of b6.
 *
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class MspAnnotationResolver implements AnnotationResolver {

    private final Pattern backboneAnnotationPattern =
            //              (ion type)(residue#)(neutral loss)(isotope)(charge)         (mass difference)
            Pattern.compile("([abcxyz])(\\d+)((?:[+-]\\d+)?)([ix]?)((?:\\^\\d+)?)/(" + RegexConstants.REAL + ")");
    private final Pattern precursorAnnotationPattern =
            //                  (neutral loss)(isotope)(charge)         (mass difference)
            Pattern.compile("p((?:[+-]\\d+)?)([ix]?)((?:\\^\\d+)?)/(" + RegexConstants.REAL + ")");
    private final Pattern internalAnnotationPattern =
            //(neutral loss)(isotope)(charge)         (mass difference)
            Pattern.compile("I([A-Z])[A-Z]([ix]?)/([+-]?\\d+\\.?\\d+)");

    private final Splitter commaSplitter = Splitter.on(',');

    private final Map<String, Composition> compositionMap = new HashMap<String, Composition>();

    public MspAnnotationResolver() {

        compositionMap.put("-17", Composition.parseComposition("(NH3)-1"));
        compositionMap.put("-18", Composition.parseComposition("(H2O)-1"));
        compositionMap.put("-80", Composition.parseComposition("(HPO3)-1"));
    }

    @Override
    public List<PepLibPeakAnnotation> resolveAnnotations(String value, Peptide peptide) {

        int firstSpace = value.indexOf(' ');
        if (firstSpace == -1)
            firstSpace = value.indexOf('\t');
        if (firstSpace == -1)
            throw new IllegalStateException("The annotation '" + value + "' cannot be recognised as a msp annotation");
        int secondSpace = value.indexOf(' ', firstSpace + 1);
        if (secondSpace == -1)
            secondSpace = value.indexOf('\t', firstSpace + 1);
        if (secondSpace == -1)
            throw new IllegalStateException("The annotation '" + value + "' cannot be recognised as a msp annotation");

        int beginIndex = value.charAt(0) == '"' ? 1 : 0;

        List<String> pepAnnotationSplits = commaSplitter.splitToList(value.substring(beginIndex, firstSpace));
        int mergedPeakCount = parsePeakStatistics(value.substring(firstSpace + 1, secondSpace));

        List<PepLibPeakAnnotation> annotations = new ArrayList<PepLibPeakAnnotation>(pepAnnotationSplits.size());
        for (String pepAnnotationValue : pepAnnotationSplits) {

            Optional<PepFragAnnotation> optPepFragAnnotation = parsePepFragAnnotation(pepAnnotationValue, peptide);
            annotations.add(new PepLibPeakAnnotation(mergedPeakCount, 0, 0, optPepFragAnnotation));
        }
        return annotations;
    }

    private int parsePeakStatistics(String value) {

        int index = value.indexOf('/');

        return Integer.parseInt(value.substring(0, index));
    }

    private Optional<PepFragAnnotation> parsePepFragAnnotation(String value, Peptide peptide) {

        char type = value.charAt(0);

        switch (type) {

            case 'I':
                return parseInternalAnnotation(value);
            case 'a':
            case 'b':
            case 'c':
            case 'x':
            case 'y':
            case 'z':
                return parseBackboneAnnotation(value, peptide);
            case 'p':
                return parsePrecursorAnnotation(value, peptide);
            default:
                return Optional.absent();
        }
    }

    /**
     * @param value the string to parse
     * @return an Optional containing the annotation if the value can be parsec or Optional.absent() if not
     */
    protected Optional<PepFragAnnotation> parseInternalAnnotation(String value) {

        Matcher matcher = internalAnnotationPattern.matcher(value);
        if (!matcher.matches())
            return Optional.absent();

        String sequence = matcher.group(1);
        String isotopeGroup = matcher.group(2);
        String massDiffGroup = matcher.group(3);

        Mass neutralLoss = Mass.ZERO;
        Composition isotopeComposition = isotopeGroup.isEmpty() ? PepFragAnnotation.EMPTY_COMPOSITION : resolveIsotope(massDiffGroup);

        PepFragAnnotation.Builder builder = new PepFragAnnotation.Builder(IonType.i, 1, new PeptideFragment(FragmentType.INTERNAL, AminoAcid.valueOf(sequence)))
                .addIsotopeComposition(isotopeComposition)
                .setNeutralLoss(neutralLoss);
        return Optional.of(builder.build());
    }

    /**
     * Parses annotations like
     * <p/>
     * <pre>
     *     p/0.13
     *     p-18/-0.06
     * </pre>
     *
     * @param value   the string to parse
     * @param peptide precursor peptide
     * @return an Optional containing the annotation if the value can be parsec or Optional.absent() if not
     */
    protected Optional<PepFragAnnotation> parsePrecursorAnnotation(String value, Peptide peptide) {

        Matcher matcher = precursorAnnotationPattern.matcher(value);
        if (!matcher.matches())
            return Optional.absent();

        String neutralLossGroup = matcher.group(1);
        String isotopeGroup = matcher.group(2);
        String chargeGroup = matcher.group(3);
        String massDiffGroup = matcher.group(4);

        int charge = chargeGroup.isEmpty() ? 1 : Integer.parseInt(chargeGroup.substring(1));
        Mass neutralLoss = neutralLossGroup.isEmpty() ? Mass.ZERO : resolveComposition(neutralLossGroup);
        Composition isotopeComposition = isotopeGroup.isEmpty() ? PepFragAnnotation.EMPTY_COMPOSITION : resolveIsotope(massDiffGroup);

        PepFragAnnotation.Builder builder = new PepFragAnnotation.Builder(IonType.p, charge, peptide)
                .addIsotopeComposition(isotopeComposition)
                .setNeutralLoss(neutralLoss);
        return Optional.of(builder.build());
    }

    /**
     * Parses annotations like
     * <p/>
     * <pre>
     *     b6-43i^2/1.48
     *     b3i/0.38
     *     z33/0.38
     * </pre>
     *
     * @param value   the string to parse
     * @param peptide precursor peptide
     * @return an Optional containing the annotation if the value can be parsec or Optional.absent() if not
     */
    protected Optional<PepFragAnnotation> parseBackboneAnnotation(String value, Peptide peptide) {

        Matcher matcher = backboneAnnotationPattern.matcher(value);
        if (!matcher.matches())
            return Optional.absent();

        IonType ionType = IonType.valueOf(matcher.group(1));
        int residueNumber = Integer.parseInt(matcher.group(2));

        String neutralLossGroup = matcher.group(3);
        String isotopeGroup = matcher.group(4);
        String chargeGroup = matcher.group(5);
        String massDiffGroup = matcher.group(6);

        int charge = chargeGroup.isEmpty() ? 1 : Integer.parseInt(chargeGroup.substring(1));
        Mass neutralLoss = neutralLossGroup.isEmpty() ? Mass.ZERO : resolveComposition(neutralLossGroup);
        PeptideFragment fragment = peptide.createFragment(ionType, residueNumber);
        Composition isotopeComposition = isotopeGroup.isEmpty() ? PepFragAnnotation.EMPTY_COMPOSITION : resolveIsotope(massDiffGroup);

        PepFragAnnotation.Builder builder = new PepFragAnnotation.Builder(ionType, charge, fragment)
                .addIsotopeComposition(isotopeComposition)
                .setNeutralLoss(neutralLoss);
        return Optional.of(builder.build());
    }

    private Composition resolveIsotope(String massDiffGroup) {

        double mzDiff = Double.parseDouble(massDiffGroup);

        int c13Count = Math.max(1, (int) Math.round(Math.abs(mzDiff)));
        return new Composition.Builder().add(PeriodicTable.C13, c13Count).build();
    }

    protected Mass resolveComposition(String neutralLossGroup) {

        Composition comp = compositionMap.get(neutralLossGroup);
        if (comp != null)
            return comp;
        else
            return new NumericMass(Double.parseDouble(neutralLossGroup));
    }
}
