package org.expasy.mzjava.proteomics.io.ms.ident;

import com.google.common.base.Optional;
import org.expasy.mzjava.core.mol.MassCalculator;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.proteomics.ms.ident.*;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * @author Thibault Robin
 * @author Markus Muller
 * @version 1.0
 */
public class BiomlPsmReader implements PsmReader {


    private class BiomlPeptideDomain {
        private String id;
        private int start;
        private int end;
        private double expect;
        private double mh;
        private double delta;
        private double hyperscore;
        private double nextscore;
        private final Map<String,Double> ionScores;
        private final Map<String,Integer> ionCounts;
        private String pre;
        private String post;
        private String seq;
        private int missed_cleavages;
        private final List<Character> modifiedAAs;
        private final List<Integer> modifiedAAPos;
        private final List<Double> modifMasses;

        private BiomlPeptideDomain() {
            ionScores = new HashMap<>();
            ionCounts = new HashMap<>();
            modifiedAAs = new ArrayList<>();
            modifiedAAPos = new ArrayList<>();;
            modifMasses = new ArrayList<>();;
        }

        public boolean set(String variableName,String variableValue) {
            switch (variableName) {
                case "id":
                    id = variableValue;
                    break;
                case "start":
                    start = Integer.valueOf(variableValue);
                    break;
                case "end":
                    end = Integer.valueOf(variableValue);
                    break;
                case "expect":
                    expect = Double.valueOf(variableValue);
                    break;
                case "mh":
                    mh = Double.valueOf(variableValue);
                    break;
                case "delta":
                    delta = Double.valueOf(variableValue);
                    break;
                case "hyperscore":
                    hyperscore = Double.valueOf(variableValue);
                    break;
                case "nextscore":
                    nextscore = Double.valueOf(variableValue);
                    break;
                case "pre":
                    pre = variableValue;
                    break;
            }
            if (variableName.equals("post")) {
                post = variableValue;
            } if (variableName.equals("seq")) {
                seq = variableValue;
            } else if (variableName.equals("missed_cleavages")) {
                missed_cleavages = Integer.valueOf(variableValue);
            } else if (variableName.contains("_score")){
                ionScores.put(variableName,Double.valueOf(variableValue));
            } else if (variableName.contains("_ions")){
                ionCounts.put(variableName,Integer.valueOf(variableValue));
            } else {
                return false;
            }

            return true;
        }

        public void addModification(char aa, int pos, double mass) {
            modifiedAAs.add(aa);
            modifiedAAPos.add(pos);
            modifMasses.add(mass);
        }

        public String getId() {
            return id;
        }

        public int getStart() {
            return start;
        }

        public int getEnd() {
            return end;
        }

        public double getExpect() {
            return expect;
        }

        public double getMh() {
            return mh;
        }

        public double getDelta() {
            return delta;
        }

        public double getHyperscore() {
            return hyperscore;
        }

        public double getNextscore() {
            return nextscore;
        }

        public Map<String, Double> getIonScores() {
            return ionScores;
        }

        public Map<String, Integer> getIonCounts() {
            return ionCounts;
        }

        public String getPre() {
            return pre;
        }

        public String getPost() {
            return post;
        }

        public String getSeq() {
            return seq;
        }

        public int getMissed_cleavages() {
            return missed_cleavages;
        }

        public List<Character> getModifiedAAs() {
            return modifiedAAs;
        }

        public List<Integer> getModifiedAAPos() {
            return modifiedAAPos;
        }

        public List<Double> getModifMasses() {
            return modifMasses;
        }

        public void clear() {
            pre = "";
            post = "";
            seq = "";
            ionCounts.clear();
            ionScores.clear();
            start = end = -1;
            modifiedAAPos.clear();
            modifiedAAs.clear();
            modifMasses.clear();
        }
    }

    private class BiomlProtein {

        private double expect;
        private String id;
        private int uid;
        private String label;
        private String description;
        private double sumI;
        private String fastaFile;

        private String seq;
        private int start;
        private int end;
        private final List<BiomlPeptideDomain> peptideDomains;

        private BiomlProtein() {
            this.peptideDomains = new ArrayList<>();
        }

        public boolean set(String variableName,String variableValue) {

            switch (variableName) {
                case "id":
                    id = variableValue;
                    break;
                case "uid":
                    uid = Integer.valueOf(variableValue);
                    break;
                case "expect":
                    expect = Double.valueOf(variableValue);
                    break;
                case "label":
                    label = variableValue;
                    break;
                case "sumI":
                    sumI = Double.valueOf(variableValue);
                    break;
                case "file":
                    fastaFile = variableValue;
                    break;
                case "description":
                    description = variableValue;
                    break;
                case "start":
                    start = Integer.valueOf(variableValue);
                    break;
                case "end":
                    end = Integer.valueOf(variableValue);
                    break;
                case "seq":
                    if (seq == null || seq.isEmpty()) seq = variableValue;
                    else seq += variableValue;
                    break;
                default:
                    return false;
            }

            return true;
        }

        public void addPeptideDomain(BiomlPeptideDomain peptideDomain) {
            peptideDomains.add(peptideDomain);
        }

        public double getExpect() {
            return expect;
        }

        public String getId() {
            return id;
        }

        public int getUid() {
            return uid;
        }

        public String getLabel() {
            return label;
        }

        public String getDescription() {
            return description;
        }

        public double getSumI() {
            return sumI;
        }

        public String getFastaFile() {
            return fastaFile;
        }


        public String getSeq() {
            return seq;
        }

        public int getStart() {
            return start;
        }

        public int getEnd() {
            return end;
        }

        public List<BiomlPeptideDomain> getPeptideDomains() {
            return peptideDomains;
        }

        public void clear() {
            this.seq = "";
            this.fastaFile = "";
            this.description = "";
            this.id = "";
            this.label = "";
            this.start = this.end = -1;
            this.peptideDomains.clear();
        }

    }

    private class BiomlSpectrum {
        private int id;
        private String comment;
        private String label;
        private double mh;
        private int charge;


        public boolean set(String variableName,String variableValue) {

            switch (variableName) {
                case "Description":
                    if (comment == null || comment.isEmpty()) comment = variableValue;
                    else comment += variableValue;
                    break;
                case "label":
                    label = variableValue;
                    break;
                case "M+H":
                    mh = Double.valueOf(variableValue);
                    break;
                case "charge":
                    charge = Integer.valueOf(variableValue);
                    break;
                case "id":
                    id = Integer.valueOf(variableValue);
                    break;
                default:
                    return false;
            }

            return true;
        }

        public String getComment() {
            return comment;
        }

        public double getMh() {
            return mh;
        }

        public int getCharge() {
            return charge;
        }

        public String getLabel() {
            return label;
        }

        public int getId() {
            return id;
        }

        public void clear() {
            comment = "";
        }
    }

    private static class BiomlPeptideMatchConverter {
        public static List<PeptideMatch> convert(List<BiomlProtein> proteins,String decoyTag,ModificationMatchResolver modificationMatchResolver) {

            List<PeptideProteinMatch> proteinMatches =getProteinMatches(proteins,decoyTag);

            List<PeptideMatch> peptideMatches = getPeptideMatches(proteins,modificationMatchResolver);

            for (PeptideMatch peptideMatch : peptideMatches)
                peptideMatch.addProteinMatches(proteinMatches);

            return peptideMatches;
        }

        private static List<PeptideProteinMatch> getProteinMatches(List<BiomlProtein> proteins,String decoyTag) {

            List<PeptideProteinMatch> proteinMatches = new ArrayList<>();

            for (BiomlProtein protein : proteins) {

                //look for the decoy tag in protein headers to determine the hit type
                PeptideProteinMatch.HitType hitType;
                if (protein.getLabel().contains(decoyTag)) {
                    hitType = PeptideProteinMatch.HitType.DECOY;
                } else {
                    hitType = PeptideProteinMatch.HitType.TARGET;
                }

                BiomlPeptideDomain domain = protein.getPeptideDomains().get(0);

                Optional<String> previousAA;
                int last = domain.getPre().length() - 1;
                if (domain.getPre().isEmpty() || domain.getPre().charAt(last) == '[') {
                    previousAA = Optional.absent();
                } else {
                    previousAA = Optional.of(domain.getPre().substring(last));
                }

                Optional<String> nextAA;
                if (domain.getPost().isEmpty() || domain.getPost().charAt(0) == ']') {
                    nextAA = Optional.absent();
                } else {
                    nextAA = Optional.of(domain.getPost().substring(0, 1));
                }

                PeptideProteinMatch proteinMatch = new PeptideProteinMatch(
                        protein.getLabel(),
                        Optional.of(protein.getFastaFile()),
                        previousAA,
                        nextAA,
                        domain.getStart() - 1,
                        domain.getEnd() - 1,
                        hitType
                );

                proteinMatches.add(proteinMatch);
            }

            return proteinMatches;
        }

        private static List<PeptideMatch> getPeptideMatches(List<BiomlProtein> proteins,ModificationMatchResolver modificationMatchResolver) {

            List<PeptideMatch> peptideMatches = new ArrayList<>();

            for (BiomlPeptideDomain peptideDomain : proteins.get(0).getPeptideDomains()) {

                PeptideMatch peptideMatch = new PeptideMatch(peptideDomain.getSeq());
                peptideMatch.setRank(Optional.of(1));
                peptideMatch.setNumMissedCleavages(Optional.of(peptideDomain.getMissed_cleavages()));
                peptideMatch.addScore("hyperscore", peptideDomain.getHyperscore());
                peptideMatch.addScore("nextscore", peptideDomain.getNextscore());
                peptideMatch.addScore("deltascore", peptideDomain.getDelta());
                peptideMatch.addScore("evalue", peptideDomain.getExpect());


                for (int i=0;i<peptideDomain.getModifiedAAs().size();i++) {

                    int index = peptideDomain.getModifiedAAPos().get(i) - peptideDomain.getStart();
                    ModificationMatch modificationMatch = peptideMatch.addModificationMatch(index, peptideDomain.getModifMasses().get(i));
                    resolveMod(modificationMatch,modificationMatchResolver);
                }

                peptideMatches.add(peptideMatch);
            }

            return peptideMatches;
        }

        protected static void resolveMod(ModificationMatch modMatch,ModificationMatchResolver modificationMatchResolver) {

            Optional<Modification> modOpt = modificationMatchResolver.resolve(modMatch);
            if(modOpt.isPresent())
                modMatch.addPotentialModification(modOpt.get());
        }



    }

    private static class BiomlSpectrumConverter {
        public static SpectrumIdentifier convert(BiomlSpectrum spectrum) {

            SpectrumIdentifier identifier = new SpectrumIdentifier(spectrum.getComment());
            identifier.setIndex(spectrum.getId());
            identifier.setAssumedCharge(spectrum.getCharge());
            identifier.setName(spectrum.getLabel());
            identifier.setPrecursorNeutralMass(spectrum.getMh() - MassCalculator.PROTON_MASS);

            return identifier;
        }
    }

    private BiomlProtein protein;
    private BiomlSpectrum spectrum;
    private final List<BiomlProtein> proteinList;
    private BiomlPeptideDomain peptideDomain;
    private PSMReaderCallback callback;

    private final String decoyTag;
    private final Pattern spacePattern = Pattern.compile("\\s");
    private final ModListModMatchResolver modMatchResolver;


    public BiomlPsmReader(String decoyTag, ModListModMatchResolver modMatchResolver) {
        this.decoyTag = decoyTag;
        this.modMatchResolver = modMatchResolver;

        this.proteinList = new ArrayList<>();
    }

    @Override
    public void parse(File file, PSMReaderCallback callback) {
        try {
            parse(new FileInputStream(file), callback);
        } catch (FileNotFoundException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void parse(Reader reader, PSMReaderCallback callback) {
        throw new IllegalArgumentException("BiomlPsmReader.parse(Reader reader, PSMReaderCallback callback) not implemented!");
    }

    @Override
    public void parse(InputStream inputSource, PSMReaderCallback callback) {

        this.callback = callback;
        try {
            XMLInputFactory factory = XMLInputFactory.newInstance();
            XMLStreamReader parser = factory.createXMLStreamReader(inputSource);

            while (parser.hasNext()) {

                int event = parser.next();
                switch (event) {

                    case XMLStreamConstants.START_ELEMENT:
                        if (parser.getLocalName().equals("bioml")) startBioml(parser);
                        break;

                    case XMLStreamConstants.END_DOCUMENT:
                        break;
                } // end switch
            } // end while
            parser.close();
        }
        catch (XMLStreamException ex) {
            //System.out.println(ex);
        }

    }


    private void startBioml(XMLStreamReader parser) throws XMLStreamException{
        //System.out.println("<bioml");

        while (parser.hasNext()) {

            int event = parser.next();
            switch (event) {

                case XMLStreamConstants.START_ELEMENT:
                    if (parser.getLocalName().equals("group") && hasAttributeValue(parser, "model")) startProteinGroup(parser);
                    else if (parser.getLocalName().equals("group")) startOtherGroup(parser, 1);
                    break;

                case XMLStreamConstants.END_ELEMENT:
                    if (parser.getLocalName().equals("bioml")) {
                        endBioml();
                        return;
                    }
                    break;
            } // end switch
        } // end while

        proteinList.clear();
    }

    private void startOtherGroup(XMLStreamReader parser, int level) throws XMLStreamException {
        String tabs = "";
        for (int i=0;i<level;i++) tabs += "\t";

        //System.out.println(tabs + "<Other group");

        while (parser.hasNext()) {

            int event = parser.next();
            switch (event) {

                case XMLStreamConstants.END_ELEMENT:
                    if (parser.getLocalName().equals("group")) {
                        //System.out.println(tabs+"Other group>");
                        return;
                    }
                    break;
            } // end switch
        } // end while
    }

    private void startProteinGroup(XMLStreamReader parser) throws XMLStreamException {
        //System.out.println("\t<protein group");

        while (parser.hasNext()) {

            int event = parser.next();
            switch (event) {

                case XMLStreamConstants.START_ELEMENT:
                    if (parser.getLocalName().equals("protein")) startProtein(parser);
                    else if (parser.getLocalName().equals("group") && hasAttributeValue(parser,"fragment ion mass spectrum")) startSpectrum(parser);
                    else if (parser.getLocalName().equals("group")) startOtherGroup(parser, 2);
                    break;

                case XMLStreamConstants.END_ELEMENT:
                    if (parser.getLocalName().equals("group")) {
                        endProteinGroup();
                        return;
                    }
                    break;
            } // end switch
        } // end while
    }

    private void startProtein(XMLStreamReader parser) throws XMLStreamException {
        //System.out.println("\t\t<protein");

        protein = new BiomlProtein();

        for (int i=0;i<parser.getAttributeCount();i++) {
            protein.set(parser.getAttributeName(i).toString(), parser.getAttributeValue(i));
        }

        while (parser.hasNext()) {

            int event = parser.next();
            switch (event) {

                case XMLStreamConstants.START_ELEMENT:
                    if (parser.getLocalName().equals("note")) startProteinNote(parser);
                    else if (parser.getLocalName().equals("file")) startProteinFile(parser);
                    else if (parser.getLocalName().equals("peptide")) startPeptide(parser);
                    break;

                case XMLStreamConstants.END_ELEMENT:
                    if (parser.getLocalName().equals("protein")) {
                        endProtein();
                        return;
                    }
                    break;

            } // end switch
        } // end while
    }

    private void startProteinNote(XMLStreamReader parser) throws XMLStreamException {
        //System.out.println("\t\t\t<protein note ");

        while (parser.hasNext()) {

            int event = parser.next();
            switch (event) {

                case XMLStreamConstants.CHARACTERS:
                    protein.set("description",parser.getText());
                    break;

                case XMLStreamConstants.END_ELEMENT:
                    if (parser.getLocalName().equals("note")) endProteinNote();
                    return;
            } // end switch
        } // end while
    }

    private void startProteinFile(XMLStreamReader parser) throws XMLStreamException {
        //System.out.println("\t\t\t<protein file ");

        protein.set("file", parser.getAttributeValue(1));

        while (parser.hasNext()) {

            int event = parser.next();
            switch (event) {

                case XMLStreamConstants.END_ELEMENT:
                    if (parser.getLocalName().equals("file")) endProteinFile();
                    return;
            } // end switch
        } // end while
    }

    private void startPeptide(XMLStreamReader parser) throws XMLStreamException {
        //System.out.println("\t\t\t<peptide");

        protein.set("start", parser.getAttributeValue(0));
        protein.set("end", parser.getAttributeValue(1));

        while (parser.hasNext()) {

            int event = parser.next();
            switch (event) {

                case XMLStreamConstants.START_ELEMENT:
                    if (parser.getLocalName().equals("domain")) startDomain(parser);
                    break;

                case XMLStreamConstants.END_ELEMENT:
                    if (parser.getLocalName().equals("peptide")) {
                        endPeptide();
                        return;
                    }
                    break;

                case XMLStreamConstants.CHARACTERS:
                    setProteinSeq(parser);
                    break;

            } // end switch
        } // end while
    }

    private void setProteinSeq(XMLStreamReader parser) {
        String seq = removeSpaces(parser.getText());
        if (!seq.isEmpty()) protein.set("seq",seq);
    }

    private void startDomain(XMLStreamReader parser) throws XMLStreamException {
        //System.out.println("\t\t\t\t<domain");

        peptideDomain = new BiomlPeptideDomain();

        for (int i=0;i<parser.getAttributeCount();i++) {
            peptideDomain.set(parser.getAttributeName(i).toString(), parser.getAttributeValue(i));
        }

        while (parser.hasNext()) {

            int event = parser.next();
            switch (event) {

                case XMLStreamConstants.START_ELEMENT:
                    if (parser.getLocalName().equals("aa")) setModif(parser);
                    break;

                case XMLStreamConstants.END_ELEMENT:
                    if (parser.getLocalName().equals("domain")) {
                        endDomain();
                        return;
                    }
                    break;
            } // end switch
        } // end while
    }

    private void setModif(XMLStreamReader parser) {
        peptideDomain.addModification(parser.getAttributeValue(0).toCharArray()[0], Integer.valueOf(parser.getAttributeValue(1)), Double.valueOf(parser.getAttributeValue(2)));
    }

    private void startSpectrum(XMLStreamReader parser) throws XMLStreamException {
        //System.out.println("\t\t<spectrum group");

        spectrum = new BiomlSpectrum();

        while (parser.hasNext()) {

            int event = parser.next();
            switch (event) {

                case XMLStreamConstants.START_ELEMENT:
                    if (parser.getLocalName().equals("note")) startSpectrumNote(parser);
                    else if (parser.getLocalName().equals("trace")) startSpectrumTrace(parser);
                    break;

                case XMLStreamConstants.END_ELEMENT:
                    if (parser.getLocalName().equals("group")) {
                        endSpectrum();
                        return;
                    }
                    break;
            } // end switch
        } // end while
    }

    private void startSpectrumNote(XMLStreamReader parser) throws XMLStreamException {

        //System.out.println("\t\t\t<spectrum note");

        while (parser.hasNext()) {

            int event = parser.next();
            switch (event) {

                case XMLStreamConstants.CHARACTERS:
                    spectrum.set("Description",parser.getText());
                    break;

                case XMLStreamConstants.END_ELEMENT:
                    if (parser.getLocalName().equals("note")) {
                        endSpectrumNote();
                        return;
                    }
                    break;
            } // end switch
        } // end while
    }

    private void startSpectrumTrace(XMLStreamReader parser) throws XMLStreamException {

        //System.out.println("\t\t\t<spectrum trace");

        for (int i=0;i<parser.getAttributeCount();i++) {
            spectrum.set(parser.getAttributeName(i).toString(), parser.getAttributeValue(i));
        }

        while (parser.hasNext()) {

            int event = parser.next();
            switch (event) {

                case XMLStreamConstants.START_ELEMENT:
                    if (parser.getLocalName().equals("attribute")) startSpectrumAttr(parser);
                    break;

                case XMLStreamConstants.END_ELEMENT:
                    if (parser.getLocalName().equals("trace")) {
                        endSpectrumTrace();
                        return;
                    }
                    break;
            } // end switch
        } // end while
    }

    private void startSpectrumAttr(XMLStreamReader parser) throws XMLStreamException {
        //System.out.println("\t\t\t\t<spectrum attribute");

        String attr = "";
        if (hasAttributeValue(parser, "M+H")) attr = "M+H";
        else if (hasAttributeValue(parser,"charge")) attr = "charge";

        while (parser.hasNext()) {

            int event = parser.next();
            switch (event) {

                case XMLStreamConstants.END_ELEMENT:
                    if (parser.getLocalName().equals("attribute")) {
                        endSpectrumAttr();
                        return;
                    }
                    break;

                case XMLStreamConstants.CHARACTERS:
                    spectrum.set(attr,parser.getText());
                    break;

            } // end switch
        } // end while
    }



    private void endBioml() {
        //System.out.println("bioml>");
    }

    private void endProteinGroup() {
        SpectrumIdentifier identifier = BiomlSpectrumConverter.convert(spectrum);
        spectrum.clear();

        List<PeptideMatch> peptideMatches = BiomlPeptideMatchConverter.convert(proteinList, decoyTag, modMatchResolver);

        for (PeptideMatch peptideMatch: peptideMatches)
            this.callback.resultRead(identifier,peptideMatch);

        proteinList.clear();
        protein.clear();

        //System.out.println("\tprotein group>");
    }

    private void endProtein() {
        //System.out.println("\t\tprotein>");

        proteinList.add(protein);
    }

    private void endPeptide() {
        //System.out.println("\t\t\tpeptide>");
    }

    private void endDomain() {
        //System.out.println("\t\t\t\tdomain>");

        protein.addPeptideDomain(peptideDomain);
    }

    private void endProteinNote() {
        //System.out.println("\t\t\tprotein note>");
    }

    private void endProteinFile() {
        //System.out.println("\t\t\tprotein file>");
    }

    private void endSpectrum() {
        //System.out.println("\t\tspectrum group>");
    }

    private void endSpectrumNote() {
        //System.out.println("\t\t\tspectrum note>");
    }

    private void endSpectrumTrace() {
        //System.out.println("\t\t\tspectrum trace>");
    }

    private void endSpectrumAttr() {
        //System.out.println("\t\t\t\tspectrum attribute>");
    }


    private boolean hasAttributeValue(XMLStreamReader parser, String value) {

        for (int i=0;i<parser.getAttributeCount();i++) {
            if (parser.getAttributeValue(i).equals(value)) return true;
        }

        return  false;
    }

    private String removeSpaces(String org) {

        return spacePattern.matcher(org).replaceAll("");
    }

}
