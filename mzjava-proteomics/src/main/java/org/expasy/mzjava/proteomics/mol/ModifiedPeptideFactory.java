package org.expasy.mzjava.proteomics.mol;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.*;
import org.expasy.mzjava.utils.MixedRadixNtupleGenerator;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * This factory creates modified {@code Peptide}s given an {@code Peptide} source
 * and a set of targeted AAs/variable modifications.
 *
 * @author fnikitin
 * Date: 11/7/13
 */
public class ModifiedPeptideFactory {

    private final SetMultimap<ModifiableTarget, Modification> targets;

    private final SetMultimap<ModSite, Modification> varModifiedSites;

    private final MixedRadixNtupleGenerator generator;

    private final MixedRadixNtupleGenerator.NtupleContainer container;

    public ModifiedPeptideFactory(final Builder builder) {

        targets = builder.targets;

        Predicate<int[]> predicate = new Predicate<int[]>() {

            @Override
            public boolean apply(int[] tuple) {

                int count = countNonZeroElements(tuple);

                return  count >= 1 && count <= builder.limit;
            }
        };

        container = new MixedRadixNtupleGenerator.NtupleContainer(predicate);
        generator = new MixedRadixNtupleGenerator(container);

        varModifiedSites = LinkedHashMultimap.create();
    }

    private static int countNonZeroElements(int[] tuple) {

        checkNotNull(tuple);

        int count = 0;
        for (int element : tuple) {

            if (element > 0)
                count++;
        }
        return count;
    }

    public static class Builder {

        private final SetMultimap<ModifiableTarget, Modification> targets;
        private int limit;

        public Builder() {

            targets = HashMultimap.create();
            limit = Integer.MAX_VALUE;
        }

        public Builder addTarget(Set<AminoAcid> aas, Modification mod) {

            targets.put(new ModifiedPeptideFactory.ModifiableTarget(aas), mod);

            return this;
        }

        public Builder addTarget(Set<AminoAcid> aas, ModAttachment attachment, Modification mod) {

            targets.put(new ModifiedPeptideFactory.ModifiableTarget(aas, Optional.of(attachment)), mod);

            return this;
        }

        public Builder addTarget(ModAttachment attachment, Modification mod) {

            targets.put(new ModifiedPeptideFactory.ModifiableTarget(attachment), mod);

            return this;
        }

        public Builder limit(int varSiteMax) {

            checkArgument(limit>0);

            limit = varSiteMax;

            return this;
        }

        public ModifiedPeptideFactory build() {

            return new ModifiedPeptideFactory(this);
        }
    }

    public List<Peptide> createVarModPeptides(Peptide peptide) {

        List<Peptide> peptides = Lists.newArrayList();

        varModifiedSites.clear();
        container.clear();

        // 1. find all potential sites
        findModifiableSites(peptide);

        List<ModSite> modSites = Lists.newArrayList(varModifiedSites.keySet());

        // 2. generate all C(n,2) combinations of modified sites
        if (!varModifiedSites.isEmpty()) {

            generator.generate(varModifiedSites.size(), 2);

            List<int[]> combs = container.getNtuples();

            // 3. foreach combination create the proper modified peptide
            for (int[] comb : combs) {

                peptides.add(createModPeptide(peptide, comb, modSites));
            }
        }

        return peptides;
    }

    private void findModifiableSites(Peptide peptide) {

        for (int i=0 ; i<peptide.size() ; i++) {

            List<ModifiableTarget> matches = ModifiableTarget.find(targets.keySet(), peptide.getSymbol(i), i, peptide.size());

            for (ModifiableTarget target : matches) {

                Set<Modification> mods = targets.get(target);

                Optional<ModAttachment> modAttachment = target.getAttachmentTarget();

                if (modAttachment.isPresent())

                    varModifiedSites.putAll(new ModSite(i, modAttachment.get()), mods);
                else

                    varModifiedSites.putAll(new ModSite(i, ModAttachment.SIDE_CHAIN), mods);
            }
        }
    }

    private Peptide createModPeptide(Peptide peptide, int[] comb, List<ModSite> modSites) {

        PeptideBuilder builder = new PeptideBuilder(peptide);

        for (int i=0; i<comb.length ; i++) {

            if (comb[i] > 0) {

                ModSite modSite = modSites.get(i);
                Set<Modification> mods = varModifiedSites.get(modSite);

                if (ModAttachment.termSet.contains(modSite.getSecond())) {

                    for (Modification mod : mods)
                        builder.addModification(modSite.getSecond(), mod);
                } else {

                    for (Modification mod : mods)
                        builder.addModification(modSite.getFirst(), mod);
                }

            }
        }

        return builder.build();
    }

    private static class ModSite {

        private final int index;
        private final ModAttachment modAttachment;

        public ModSite(int index, ModAttachment modAttachment) {

            checkNotNull(index);
            checkNotNull(modAttachment);

            this.index = index;
            this.modAttachment = modAttachment;
        }

        public int getFirst() {
            return index;
        }

        public ModAttachment getSecond() {
            return modAttachment;
        }
    }

    private static class ModifiableTarget {

        private final Set<AminoAcid> aas;
        private final Optional<ModAttachment> attachment;

        public ModifiableTarget(Set<AminoAcid> aas) {

            this(aas, Optional.<ModAttachment>absent());
        }

        public ModifiableTarget(ModAttachment attachment) {

            this(Sets.<AminoAcid>newHashSet(), Optional.of(attachment));
        }

        public ModifiableTarget(Set<AminoAcid> aas, Optional<ModAttachment> attachment) {

            checkNotNull(aas);
            checkNotNull(attachment);

            this.aas = Sets.newHashSet(aas);
            this.attachment = attachment;
        }

        public boolean hasAATarget() {

            return !aas.isEmpty();
        }

        public boolean isAttachmentTarget() {

            return attachment.isPresent();
        }



        public Optional<ModAttachment> getAttachmentTarget() {

            return attachment;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ModifiableTarget that = (ModifiableTarget) o;

            return !(aas != null ? !aas.equals(that.aas) : that.aas != null) &&
                    !(attachment != null ? !attachment.equals(that.attachment) : that.attachment != null);

        }

        @Override
        public int hashCode() {
            int result = aas != null ? aas.hashCode() : 0;
            result = 31 * result + (attachment != null ? attachment.hashCode() : 0);
            return result;
        }

        public boolean match(AminoAcid aa, int pos, int peptideSize) {

            // "and" predicate
            if (hasAATarget() && isAttachmentTarget())
                return aas.contains(aa) &&
                       matchModAttachement(getAttachmentTarget().get(), pos, peptideSize);

            // "or" predicates
            else if (hasAATarget())
                return aas.contains(aa);

            else
                return matchModAttachement(getAttachmentTarget().get(), pos, peptideSize);
        }

        private static boolean matchModAttachement(ModAttachment attachment, int pos, int peptideSize) {

            if (ModAttachment.nTermSet.contains(attachment) && pos == 0)
                return true;

            else if (ModAttachment.cTermSet.contains(attachment) && pos == peptideSize-1)
                return true;

            return ModAttachment.SIDE_CHAIN == attachment;
        }

        public static List<ModifiableTarget> find(Collection<ModifiableTarget> targets, AminoAcid aa, int pos, int peptideSize) {

            List<ModifiableTarget> found = Lists.newArrayList();

            for (ModifiableTarget target : targets) {

                if (target.match(aa, pos, peptideSize))

                    found.add(target);
            }

            return found;
        }
    }
}