/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.mol.digest;


import com.google.common.base.Optional;
import org.expasy.mzjava.proteomics.mol.modification.Modification;

import java.util.regex.Pattern;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;


/**
 * This matcher is used to find the sites of cleavage in an amino-acid sequence. The CleavageSiteMatcher is constructed
 * from a string that represents the cleavage pattern. The pattern is based on regex except that the |
 * indicates the cleavage site instead of an or. The pattern consists of the amino acids that are matched
 * to the left of the cut followed by | and then the amino acids matched to the right of the cut. K|X
 * specifies that the cut is after every K (X is any amino acid). Amino acids that should not match can be specified
 * using ^. K|[^P] specifies that the cut should be after every K except is it is followed by a P. As with regex any
 * character class can be used to specify a position not just negation. [KR]|[^P] specifies that the cut should be after
 * every K or R except if followed by P. The pattern can be extended to more than one amino acid either side of the clevage
 * site. For example [^P][FL]X[FLY]|[^P]X[^K] is a valid pattern that checks 4 amino acids to the left and three amino
 * acids to the right of the cleavage site. For Proteases that have two distinct patterns an "or" can be used. To cut before
 * and after K "K|X or X|K" can be used.
 * <p\>
 * The formal definition of the pattern defining the site is:
 * <p/>
 * <h3>Formal definition of a cleavage site</h3>
 * <ul>
 * <li>{@code Site_altern} := {@code Site} (or {@code Site})*</li>
 * <li>{@code Site} := {@code Symbol_aa}+ {@code Cut-token} {@code Symbol_aa}+</li>
 * <li>{@code Symbol_aa} := {@code AA} | {@code AA_class} | {@code TERM_class}</li>
 * <li>{@code TERM_class} := 'ont{'({@code AA} | {@code AA_class})+'}' | 'oct{'({@code AA} | {@code AA_class})+'}'</li>
 * <li>{@code AA} := {@code Symbol_aa_namb} | {@code Symbol_aa_amb}</li>
 * <li>{@code Symbol_aa_namb} := [AC-IK-TVWY]</li>
 * <li>{@code Symbol_aa_amb} := [BJUXZ]</li>
 * <li>{@code AA_class} := '[' {@code AA} ']'</li>
 * <li>{@code Cut-token} := '|'</li>
 * </ul>
 * <p/>
 * Examples:
 * <p>
 * [KR]|[^P] is the pattern for trypsin (cut after K or R except if followed by P)
 * DEVD|X or R|K this cuts to the right of DEVD or between R and K
 * DEBD|X[^P]
 * <p/>
 * To control the matching at the beginning or end of the protein the ont{} and oct{} (<b>o</b>r <b>N</b> <b>t</b>erm &
 * <b>o</b>r <b>C</b> <b>t</b>erm) can be used to match the term or the pattern in the brackets for example:
 * <p>
 * ont{[^P]}X|S cuts ASPASS into A SPAS S
 * <p/>
 * http://web.expasy.org/peptide_cutter/peptidecutter_enzymes.html
 *
 * @author nikitin
 * @author Oliver Horlacher
 */
public class CleavageSiteMatcher {

    /**
     * the cleavage site format
     */
    private final String cleavageFormat;

    /**
     * the cleavage site regex pattern
     */
    private final Pattern cleavagePattern;

    /**
     * the modification of the n-terminal of the released peptide
     */
    private final Optional<Modification> nTermMod;

    /**
     * the modification of the c-terminal of the released peptide
     */
    private final Optional<Modification> cTermMod;

    /**
     * Pattern has a special format:
     * <p/>
     * Pn ... P4 P3 P2 P1 | P1' P2' P3' ... Pm'
     *
     * @throws CleavageSiteParseException if s is not a valid string
     */
    public CleavageSiteMatcher(String cleavageFormat) throws CleavageSiteParseException {

        this(cleavageFormat, Optional.<Modification>absent(), Optional.<Modification>absent());
    }

    /**
     * @param cleavageFormat the cleavage site
     * @param nTermMod       protease may modify peptide digest at nterm
     * @param cTermMod       protease may modify peptide digest at Cterm
     * @throws CleavageSiteParseException if s is not a valid string
     */
    public CleavageSiteMatcher(String cleavageFormat, Optional<Modification> nTermMod, Optional<Modification> cTermMod)
            throws CleavageSiteParseException {

        checkNotNull(cleavageFormat);
        checkArgument(cleavageFormat.length() > 0);
        checkNotNull(nTermMod);
        checkNotNull(cTermMod);

        this.cleavageFormat = cleavageFormat;
        this.cleavagePattern = Pattern.compile(toRegex(cleavageFormat), Pattern.CASE_INSENSITIVE);

        if (nTermMod.isPresent())
            this.nTermMod = nTermMod;
        else
            this.nTermMod = Optional.absent();

        if (cTermMod.isPresent())
            this.cTermMod = cTermMod;
        else
            this.cTermMod = Optional.absent();
    }

    /**
     * Convert the cleavage pattern to 'Lookaround' type pattern.
     *
     * @param pattern the cleavage site pattern to convert.
     * @return the 'Lookaround' type internal pattern.
     * @throws CleavageSiteParseException if s is not a valid string
     */
    private static String toRegex(String pattern) throws CleavageSiteParseException {

        StringBuilder sb = new StringBuilder();
        StringBuilder tmp = new StringBuilder();

        // X(AB(DN)CFGHIJKLMOPRSTUVWYZ(EQ))

        // 1. replace ambiguous symbols
        for (int i = 0; i < pattern.length(); i++) {
            char c = pattern.charAt(i);
            switch (c) {
                case 'X':
                    tmp.append("[A-Z]");
                    break;
                case 'B':
                    tmp.append("[DN]");
                    break;
                case 'J':
                    tmp.append("[IL]");
                    break;
                case 'Z':
                    tmp.append("[EQ]");
                    break;
                default:
                    tmp.append(c);
                    break;
            }
        }

        pattern = tmp.toString();

        // 2. split expressions
        String[] expressions = pattern.split("\\s+or\\s+");

        for (String expr : expressions) {
            // 3. split at cleavage site
            String[] pprimes = expr.split("\\|");

            if (pprimes.length == 2) {
                sb.append("(?<=").append(pprimes[0]).append(")(?=").append(pprimes[1]).append(")|");
            } else if (pprimes.length < 2) {
                throw new CleavageSiteParseException(
                        "cleavage token '|' is missing in '" + pattern
                                + "'!", sb.length()
                );
            } else {
                throw new CleavageSiteParseException(
                        "too many cleavage tokens '|' in '" + pattern
                                + "'!", sb.length()
                );
            }
        }
        sb.delete(sb.length() - 1, sb.length());

        String regex = sb.toString();
        Pattern orCTermPattern = Pattern.compile("oct\\{(.*?)\\}");
        regex = orCTermPattern.matcher(regex).replaceAll("(?:$1|\\$)");

        Pattern orNTermPattern = Pattern.compile("ont\\{(.*?)\\}");
        regex = orNTermPattern.matcher(regex).replaceAll("(?:$1|\\^)");

        return regex;
    }

    public String getFormat() {

        return cleavageFormat;
    }

    public Pattern getRegex() {

        return cleavagePattern;
    }

    /**
     * @return the Nt modification.
     */
    public Optional<Modification> getNtermMod() {

        return nTermMod;
    }

    /**
     * @return the Ct modification.
     */
    public Optional<Modification> getCtermMod() {

        return cTermMod;
    }

    /**
     * @return true if add Nt modification.
     */
    public final boolean hasNtermMod() {

        return nTermMod.isPresent();
    }

    /**
     * @return true if has Ct modification.
     */
    public final boolean hasCtermMod() {

        return cTermMod.isPresent();
    }

    public String toString() {

        StringBuilder sb = new StringBuilder();

        sb.append("Cleavage site: ").append(cleavageFormat);
        sb.append(", regex: ").append(cleavagePattern);
        if (cTermMod.isPresent()) {
            sb.append(", cterm: ").append(cTermMod.get());
        }
        if (nTermMod.isPresent()) {
            sb.append(", nterm: ").append(nTermMod.get());
        }
        return sb.toString();
    }
}
