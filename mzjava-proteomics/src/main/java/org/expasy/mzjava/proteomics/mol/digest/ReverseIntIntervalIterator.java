package org.expasy.mzjava.proteomics.mol.digest;

/**
 * Generates all possible intervals of integers in the closed interval [lower, upper] from [upper-1, upper] to [lower, lower+1]
 *
 * @author fnikitin
 * Date: 10/31/13
 */
class ReverseIntIntervalIterator extends IntIntervalIterator {

    public ReverseIntIntervalIterator(int lower, int upper) {

        super(lower, upper);
    }

    public ReverseIntIntervalIterator(int lower, int upper, int rangeMax) {

        super(lower, upper, rangeMax);
    }

    protected void init() {

        // init to the last index
        i = upper;
        j = upper;
    }

    protected void nextIteration(int rangeMax) {

        i--;

        if (isStopI(rangeMax)) {

            j--;
            i = j-1;

            if (j == 0)
                hasNext = false;
        }
    }

    private boolean isStopI(int rangeMax) {

        return i == -1 || j-i > rangeMax;
    }
}
