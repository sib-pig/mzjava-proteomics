package org.expasy.mzjava.proteomics.mol.digest;

import org.expasy.mzjava.core.mol.SymbolSequence;
import org.expasy.mzjava.proteomics.mol.AminoAcid;

/**
 * @author fnikitin
 * Date: 12/3/13
 */
public interface CleavageSiteVisitor {

    void visit(SymbolSequence<AminoAcid> aaSeq, int index);
}
