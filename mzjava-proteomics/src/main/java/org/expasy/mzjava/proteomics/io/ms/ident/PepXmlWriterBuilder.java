package org.expasy.mzjava.proteomics.io.ms.ident;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.io.Files;
import org.expasy.mzjava.proteomics.ms.ident.PeptideMatch;
import org.expasy.mzjava.proteomics.ms.ident.PeptideMatchComparator;
import org.expasy.mzjava.proteomics.io.ms.ident.pepxml.v117.EngineType;
import org.expasy.mzjava.proteomics.io.ms.ident.pepxml.v117.MassType;
import org.expasy.mzjava.proteomics.io.ms.ident.pepxml.v117.MsmsPipelineAnalysis;
import org.expasy.mzjava.proteomics.io.ms.ident.pepxml.v117.NameValueType;

import java.io.File;
import java.math.BigInteger;
import java.util.Comparator;
import java.util.List;

/**
 * This builder Builds PepXmlWriter instances.
 *
 * <h3>Proper Usage<h3/>
 * Use one of the static methods create(), set your identifications parameters and terminate with
 * build() to create a new instance of PepXmlWriter.
 *
 * <h3>Internal SampleEnzyme builder<h3/>
 * This class also contains static factory methods createEnzyme(String name) and createTrypsin()
 * to create SampleEnzyme needed as a parameter of Builder.enzyme(sampleEnzyme)
 *
 * <h3>Remark<h3/>
 * This written PepXml format has been simplified as it may only contain one search_summary by msms_run_summary.
 * Elements aminoacid_modification of search_summary will be automatically deduced from PeptideMatch collected.
 * Moreover msms_run_summary are also will be automatically deduced from identified MsnSpectrum.
 *
 * @author fnikitin
 * Date: 1/14/14
 */
public class PepXmlWriterBuilder {

    /**
     * Main entry point to build PepXmlWriter
     *
     * @param engineType the engine type
     * @param scoreName the score name as a criterium to sort result hits
     *
     * @return an instance of Builder
     */
    public static Builder create(EngineType engineType, String scoreName) {

        return new Builder(engineType, scoreName);
    }

    /**
     * Main entry point to build PepXmlWriter
     *
     * @param engineType the engine type
     * @param peptideMatchComparator the comparator to sort result hits
     *
     * @return an instance of Builder
     */
    public static Builder create(EngineType engineType, Comparator<PeptideMatch> peptideMatchComparator) {

        return new Builder(engineType, peptideMatchComparator);
    }

    /**
     * Build PepXmlWriter
     */
    public static class Builder {

        final MsmsPipelineAnalysis.MsmsRunSummary msmsRunSummary;
        final EngineType engineType;
        final List<NameValueType> params;
        final Comparator<PeptideMatch> peptideMatchComparator;

        MassType precMassType = MassType.MONOISOTOPIC;
        MassType fragmentMassType = MassType.MONOISOTOPIC;
        File protDbFile;
        MsmsPipelineAnalysis.MsmsRunSummary.SearchSummary.EnzymaticSearchConstraint constraint;
        MsmsPipelineAnalysis.MsmsRunSummary.SampleEnzyme enzyme;
        int searchHitRank = Integer.MAX_VALUE;

        Builder(EngineType engineType, String scoreName) {

            this(engineType, new PeptideMatchComparator(scoreName));
        }

        Builder(EngineType engineType, Comparator<PeptideMatch> peptideMatchComparator) {

            Preconditions.checkNotNull(engineType);
            Preconditions.checkNotNull(peptideMatchComparator);

            this.engineType = engineType;
            this.peptideMatchComparator = peptideMatchComparator;
            this.msmsRunSummary = new MsmsPipelineAnalysis.MsmsRunSummary();
            this.params = Lists.newArrayList();
        }

        /**
         * Set the sample_enzyme element of msms_run_summary (else enzyme by default)
         *
         * @param enzyme the sample enzyme element
         *
         * @return this Builder
         */
        public Builder enzyme(MsmsPipelineAnalysis.MsmsRunSummary.SampleEnzyme enzyme) {

            Preconditions.checkNotNull(enzyme);

            this.enzyme = enzyme;

            return this;
        }

        /**
         * Set the enzymatic_search_constraint child element of search_summary
         *
         * @param maxNumInternalCleavages Maximum number of enzyme cleavage sites allowable within peptide
         * @param minNumberTermini Minimum number of termini compatible with enzymatic cleavage
         * @return this Builder
         */
        public Builder enzymaticSearchConstraint(int maxNumInternalCleavages, int minNumberTermini) {

            Preconditions.checkArgument(maxNumInternalCleavages>=0);
            Preconditions.checkArgument(minNumberTermini>=0);

            constraint = new MsmsPipelineAnalysis.MsmsRunSummary.SearchSummary.EnzymaticSearchConstraint();

            constraint.setMaxNumInternalCleavages(BigInteger.valueOf(maxNumInternalCleavages));
            constraint.setMinNumberTermini(BigInteger.valueOf(minNumberTermini));

            return this;
        }

        /**
         * Set the number of maximum result hits retained
         *
         * @param rankCount the maximum number of retained results
         *
         * @return this Builder
         */
        public Builder searchHitRank(int rankCount) {

            Preconditions.checkArgument(rankCount > 0);

            this.searchHitRank = rankCount;

            return this;
        }

        /**
         * Set the msManufacturer attribute of element msms_run_summary
         *
         * @param msManufacturer Manufacturer of MS/MS instrument
         * @return this Builder
         */
        public Builder msManufacturer(String msManufacturer) {

            Preconditions.checkNotNull(msManufacturer);

            msmsRunSummary.setMsManufacturer(msManufacturer);

            return this;
        }

        /**
         * Set the msModel attribute of element msms_run_summary
         *
         * @param msModel Instrument model
         * @return this Builder
         */
        public Builder msModel(String msModel) {

            Preconditions.checkNotNull(msModel);

            msmsRunSummary.setMsModel(msModel);

            return this;
        }

        /**
         * Set the msIonization attribute of element msms_run_summary
         *
         * @param msIonization Ionization type
         * @return this Builder
         */
        public Builder msIonization(String msIonization) {

            Preconditions.checkNotNull(msIonization);

            msmsRunSummary.setMsIonization(msIonization);

            return this;
        }

        /**
         * Set the msMassAnalyzer attribute of element msms_run_summary
         *
         * @param msMassAnalyzer Ion trap, etc
         * @return this Builder
         */
        public Builder msMassAnalyzer(String msMassAnalyzer) {

            Preconditions.checkNotNull(msMassAnalyzer);

            msmsRunSummary.setMsMassAnalyzer(msMassAnalyzer);

            return this;
        }

        /**
         * Set the msDetector attribute of element msms_run_summary
         *
         * @param msDetector EMT, etc
         * @return this Builder
         */
        public Builder msDetector(String msDetector) {

            Preconditions.checkNotNull(msDetector);

            msmsRunSummary.setMsDetector(msDetector);

            return this;
        }

        /**
         * Set the precursor_mass_type attribute of element search_summary
         *
         * @param massType precursor mass type (monoisotopic by default)
         * @return this Builder
         */
        public Builder precMassType(MassType massType) {

            Preconditions.checkNotNull(massType);

            precMassType = massType;

            return this;
        }

        /**
         * Set the fragment_mass_type attribute of element search_summary
         *
         * @param massType fragment mass type (monoisotopic by default)
         * @return this Builder
         */
        public Builder fragMassType(MassType massType) {

            Preconditions.checkNotNull(massType);

            fragmentMassType = massType;

            return this;
        }

        /**
         * Set the search_database element of element search_summary
         *
         * @param dbFile full path file of database on local computer
         * @return this Builder
         */
        public Builder searchDatabase(File dbFile) {

            Preconditions.checkNotNull(dbFile);

            protDbFile = dbFile;

            return this;
        }

        /**
         * Add a parameter element in element search_summary
         *
         * @param name the parameter name
         * @param value the parameter value
         * @return this Builder
         */
        public Builder parameter(String name, String value) {

            Preconditions.checkNotNull(name);
            Preconditions.checkArgument(name.length()>0);
            Preconditions.checkNotNull(value);
            Preconditions.checkArgument(value.length()>0);

            NameValueType nvt = new NameValueType();

            nvt.setName(name);
            nvt.setValueAttribute(value);

            params.add(nvt);

            return this;
        }

        /**
         * Build a PepXmlWriter
         * @return a new instance of PepXmlWriter
         */
        public PepXmlWriter build() {

            if (enzyme == null)
                enzyme = createTrypsin();

            if (constraint != null)
                constraint.setEnzyme(enzyme.getName());

            return new PepXmlWriter(this);
        }
    }

    /**
     * Static factory method that create a SampleEnzyme builder
     * @param name mandatory enzyme name
     * @return SampleEnzymeBuilder
     */
    public static SampleEnzymeBuilder createEnzyme(String name) {

        return new SampleEnzymeBuilder(name);
    }

    /**
     * Static factory method that create the default SampleEnzyme
     * @return a new trypsin SampleEnzyme
     */
    public static MsmsPipelineAnalysis.MsmsRunSummary.SampleEnzyme createTrypsin() {

        return new SampleEnzymeBuilder("trypsin").specificity("C", "KR").noCut("P").endEnzyme();
    }

    public interface EnzymeEnd {

        MsmsPipelineAnalysis.MsmsRunSummary.SampleEnzyme endEnzyme();
    }

    public static class SampleEnzymeBuilder implements EnzymeEnd {

        private MsmsPipelineAnalysis.MsmsRunSummary.SampleEnzyme enzyme;

        SampleEnzymeBuilder(String name) {

            Preconditions.checkNotNull(name);
            Preconditions.checkArgument(name.length() > 0);

            enzyme = new MsmsPipelineAnalysis.MsmsRunSummary.SampleEnzyme();

            enzyme.setName(name);
        }

        public SpecificityBuilder specificity(String sense, String cut) {

            return new SpecificityBuilder(sense, cut, this);
        }

        public MsmsPipelineAnalysis.MsmsRunSummary.SampleEnzyme endEnzyme() {

            return enzyme;
        }
    }

    public static class SpecificityBuilder implements EnzymeEnd {

        private SampleEnzymeBuilder enclosingBuilder;
        private MsmsPipelineAnalysis.MsmsRunSummary.SampleEnzyme.Specificity currentSpecificity;

        SpecificityBuilder(String sense, String cut, SampleEnzymeBuilder enclosingBuilder) {

            Preconditions.checkNotNull(enclosingBuilder);

            this.enclosingBuilder = enclosingBuilder;
            specificity(sense, cut);
        }

        public SpecificityBuilder specificity(String sense, String cut) {

            Preconditions.checkNotNull(sense);
            Preconditions.checkArgument(sense.length() > 0);
            Preconditions.checkNotNull(cut);
            Preconditions.checkArgument(cut.length() > 0);

            currentSpecificity = new MsmsPipelineAnalysis.MsmsRunSummary.SampleEnzyme.Specificity();
            currentSpecificity.setSense(sense);
            currentSpecificity.setCut(cut);

            enclosingBuilder.enzyme.getSpecificity().add(currentSpecificity);

            return this;
        }

        public SpecificityBuilder noCut(String noCut) {

            Preconditions.checkNotNull(noCut);
            Preconditions.checkArgument(noCut.length() > 0);

            currentSpecificity.setNoCut(noCut);

            return this;
        }

        public MsmsPipelineAnalysis.MsmsRunSummary.SampleEnzyme endEnzyme() {

            return enclosingBuilder.endEnzyme();
        }
    }

    private static MsmsPipelineAnalysis.MsmsRunSummary newMsmsRunSummary(File msFile) {

        Preconditions.checkNotNull(msFile);

        MsmsPipelineAnalysis.MsmsRunSummary msmsRunSummary = new MsmsPipelineAnalysis.MsmsRunSummary();
        msmsRunSummary.setBaseName(Files.getNameWithoutExtension(msFile.getAbsolutePath()));
        msmsRunSummary.setRawDataType("raw");
        msmsRunSummary.setRawData(Files.getFileExtension(msFile.getName()));

        return msmsRunSummary;
    }

    static MsmsPipelineAnalysis.MsmsRunSummary newMsmsRunSummary(File msFile, MsmsPipelineAnalysis.MsmsRunSummary other) {

        Preconditions.checkNotNull(msFile);

        MsmsPipelineAnalysis.MsmsRunSummary msmsRunSummary = newMsmsRunSummary(msFile);

        msmsRunSummary.setMsDetector(other.getMsDetector());
        msmsRunSummary.setMsIonization(other.getMsIonization());
        msmsRunSummary.setMsManufacturer(other.getMsManufacturer());
        msmsRunSummary.setMsMassAnalyzer(other.getMsMassAnalyzer());
        msmsRunSummary.setMsModel(other.getMsModel());
        msmsRunSummary.setSampleEnzyme(other.getSampleEnzyme());

        return msmsRunSummary;
    }
}
