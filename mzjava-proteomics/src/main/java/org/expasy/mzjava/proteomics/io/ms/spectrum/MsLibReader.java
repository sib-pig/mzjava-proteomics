/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.io.ms.spectrum;

import org.expasy.mzjava.core.io.ms.spectrum.AbstractMsReader;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessorChain;
import org.expasy.mzjava.proteomics.io.ms.spectrum.sptxt.AnnotationResolver;
import org.expasy.mzjava.proteomics.io.ms.spectrum.sptxt.LibrarySpectrumBuilder;
import org.expasy.mzjava.proteomics.io.ms.spectrum.sptxt.SpectraLibCommentParser;
import org.expasy.mzjava.proteomics.mol.modification.ModificationResolver;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;
import org.expasy.mzjava.proteomics.ms.spectrum.PepLibPeakAnnotation;

import java.io.IOException;
import java.io.LineNumberReader;
import java.io.Reader;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class MsLibReader extends AbstractMsReader<PepLibPeakAnnotation, PeptideConsensusSpectrum> {

    private final Map<String, PeptideConsensusSpectrum.Status> statusMap;
    private final SpectraLibCommentParser commentParser;
    private final AnnotationResolver annotationResolver;
    private final LibrarySpectrumBuilder spectrumBuilder = new LibrarySpectrumBuilder();
    private final ModificationResolver modResolver;

    public MsLibReader(Reader reader, final URI spectraSource, PeakList.Precision precision,
                       SpectraLibCommentParser commentParser,
                       AnnotationResolver annotationResolver,
                       PeakProcessorChain<PepLibPeakAnnotation> processorChain,
                       ModificationResolver modResolver) throws IOException {

        super(reader, spectraSource, precision, processorChain);
        this.commentParser = commentParser;
        this.annotationResolver = annotationResolver;
        this.modResolver = modResolver;

        PeptideConsensusSpectrum.Status[] values = PeptideConsensusSpectrum.Status.values();
        statusMap = new HashMap<>(values.length);
        for(PeptideConsensusSpectrum.Status status : values) {

            statusMap.put(status.toString().toLowerCase(), status);
        }
    }

    @Override
    protected String parseHeader(ParseContext context) throws IOException {

        return "";
    }

    @Override
    protected PeptideConsensusSpectrum parseNextEntry(ParseContext context) throws IOException {

        LineNumberReader reader = context.getCurrentReader();

        spectrumBuilder.clear();

        boolean readPeaks = false;

        for (String line = reader.readLine(); line != null; line = reader.readLine()) {

            if (line.length() > 0 && Character.isDigit(line.charAt(0))) {

                readPeaks = handlePeakLine(spectrumBuilder, line);
            } else if (line.length() == 0) {

                break;
            } else if(line.contains(":")){

                final int index = line.indexOf(':');
                handleTagLine(line.substring(0, index), line.substring(index + 2, line.length()), spectrumBuilder);
            } else {

                handleUnknownLine(spectrumBuilder, line);
            }
        }

        if (!readPeaks) return null;

        PeptideConsensusSpectrum peptideConsensusSpectrum = spectrumBuilder.build(precision, modResolver, annotationResolver, acceptUnsortedSpectra);
//        librarySpectrum.setSpectrumSource(context.getSource());
        return peptideConsensusSpectrum;
    }

    /**
     * Handle tag value line.  The tag and value is separated by =
     *
     * @param spectrumBuilder the spectrumBuilder currently being read
     * @param tag the string from the start of the lien to the = character
     * @param value the string from the character after the = to the end of the line
     * @return true if the information in the line was processed, false otherwise
     */
    protected boolean handleTagLine(String tag, String value, LibrarySpectrumBuilder spectrumBuilder) {

        if("PrecursorMZ".equals(tag)) {

            return parsePrecursorMZLine(value, spectrumBuilder);
        } else if("Status".equals(tag)) {

            return parseStatusLine(value, spectrumBuilder);
        } else if("NumPeaks".equals(tag)){

            return parseNumPeaksLine(value, spectrumBuilder);
        } else if("Comment".equals(tag)) {

            return parseCommentLine(value, spectrumBuilder);
        } else if("Name".equals(tag)) {

            return parseNameLine(value, spectrumBuilder);
        }
        return parseUnknownTag(tag, value, spectrumBuilder);
    }

    boolean parseNameLine(String value, LibrarySpectrumBuilder spectrumBuilder) {

        StringBuilder buff = new StringBuilder();

        int bracketCount = 0;
        for(int i = 0; i < value.length(); i++) {

            char c = value.charAt(i);
            if(bracketCount == 0 && Character.isUpperCase(c))
                buff.append(c);
            else if(c == '(')
                bracketCount += 1;
            else if(c == ')')
                bracketCount -= 1;
        }

        spectrumBuilder.setPeptideSequence(buff.toString());
        String chargeString = value.substring(value.lastIndexOf('/') + 1);
        spectrumBuilder.setPrecursorCharge(Integer.parseInt(chargeString));

        return true;
    }

    boolean parseCommentLine(String value, LibrarySpectrumBuilder spectrumBuilder) {

        spectrumBuilder.setComment(value);
        return commentParser.parseComment(value, spectrumBuilder);
    }

    boolean parseNumPeaksLine(String value, LibrarySpectrumBuilder spectrumBuilder) {

        spectrumBuilder.ensureCapacity(Integer.parseInt(value));
        return true;
    }

    boolean parseStatusLine(String value, LibrarySpectrumBuilder spectrumBuilder) {

        PeptideConsensusSpectrum.Status status = statusMap.get(value.toLowerCase());
        if(status == null) status = PeptideConsensusSpectrum.Status.UNKNOWN;
        spectrumBuilder.setStatus(status);
        return true;
    }

    boolean parsePrecursorMZLine(String value, LibrarySpectrumBuilder spectrum) {

        spectrum.setPrecursorMz(Double.parseDouble(value));
        return true;
    }

    /**
     * Handle peak line, which is any line that starts with a digit
     *
     * @param builder the builder currently being read
     * @param line the line
     * @return true if the information in the line was processed, false otherwise
     */
    protected boolean handlePeakLine(LibrarySpectrumBuilder builder, String line) {

        int length = line.length();
        int index = 0;

        //get mz column
        String mzColumn = extractColumn(index, line, length);
        if(mzColumn == null)
            throw new IllegalStateException(line + " is not a valid peak line");
        index += mzColumn.length();

        //Consume whitespaces between columns
        index = consumeWhitespace(index, line, length);

        //get intensity column
        String intensityColumn = extractColumn(index, line, length);
        if(intensityColumn == null)
            throw new IllegalStateException(line + " is not a valid peak line");
        index += intensityColumn.length();

        //Consume whitespaces between columns
        index = consumeWhitespace(index, line, length);

        try {
            builder.addPeak(
                    Double.parseDouble(mzColumn),
                    Double.parseDouble(intensityColumn),
                    line.substring(index));
        } catch (NumberFormatException e) {

            throw new IllegalStateException(line + " is not a valid peak line", e);
        }

        return true;
    }

    private String extractColumn(int beginIndex, String line, int length){

        String column = null;
        int index = beginIndex;
        while(index < length) {

            char current = line.charAt(index++);
            if (Character.isWhitespace(current)) {

                column = line.substring(beginIndex, index - 1);
                break;
            }
        }
        return column;
    }

    /**
     * Return the index of the first non whitespace character starting from startIndex
     *
     * @param startIndex the start index
     * @param line the String
     * @param length the length of the string
     * @return the index of the first non whitespace character
     */
    private int consumeWhitespace(int startIndex, String line, int length) {

        while (startIndex < length){

            if(!Character.isWhitespace(line.charAt(startIndex++))) {
                startIndex--;
                break;
            }
        }
        return startIndex;
    }

    /**
     * Handle line that is not a comment, begin ion, tag, peak or end ion
     *
     * @param spectrum the spectrum currently being read
     * @param line the line
     * @return true if the information in the line was processed, false otherwise
     */
    @SuppressWarnings("UnusedParameters") //This is meant to be over ridden by subclasses that want to parse tags that the SptxtReader2 does not know about
    protected boolean handleUnknownLine(LibrarySpectrumBuilder spectrum, String line) {

        return false;
    }

    /**
     * Called if there are any tags that are not handled by SptxtReader2
     *
     * This is meant to be over ridden by subclasses that want to
     * parse tags that the SptxtReader2 does not know about
     *
     *
     * @param tag the characters in the line before the first =
     * @param value the characters in the line after the first =
     * @param spectrum the spectrum that is being read
     * @return true of this handled the tag, false otherwise
     */
    @SuppressWarnings("UnusedParameters") //This is meant to be over ridden by subclasses that want to parse tags that the SptxtReader2 does not know about
    protected boolean parseUnknownTag(String tag, String value, LibrarySpectrumBuilder spectrum) {

        return false;
    }
}
