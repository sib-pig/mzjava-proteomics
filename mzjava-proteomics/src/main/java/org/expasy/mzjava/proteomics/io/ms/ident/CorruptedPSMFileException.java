package org.expasy.mzjava.proteomics.io.ms.ident;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class CorruptedPSMFileException extends RuntimeException  {

    public CorruptedPSMFileException(String errorMsg) {
        super(errorMsg);
    }
}
