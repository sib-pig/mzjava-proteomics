/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.mol.digest;

import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.mol.Protein;

/**
 * DigestionController that excludes peptides that fall outside the length range.
 *
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class LengthDigestionController implements DigestionController {

    private final int min;
    private final int max;

    /**
     * Create a DigestionController that excludes peptides that have a length < <code>min</code> and > <code>max</code>
     *
     * @param min the minimum peptide length, peptides with a length smaller than this are excluded
     * @param max the maximum peptide length, peptides with a length larger than this are excluded
     */
    public LengthDigestionController(int min, int max) {

        this.min = min;
        this.max = max;
    }

    @Override
    public boolean interruptDigestion(Protein protein, int proteinBoundsCount, int inclusiveLowerBoundIndex, int inclusiveUpperBoundIndex) {

        return false;
    }

    @Override
    public boolean makeDigest(Protein protein, int inclusiveProteinLowerIndex, int exclusiveProteinUpperIndex, int missedCleavagesCount) {

        int size = exclusiveProteinUpperIndex - inclusiveProteinLowerIndex;
        return size >= min && size <= max;
    }

    @Override
    public boolean retainDigest(Protein protein, Peptide digest, int digestIndex) {

        return true;
    }

    @Override
    public boolean retainSemiDigest(Peptide digest) {

        int size = digest.size();
        return size >= min && size <= max;
    }
}
