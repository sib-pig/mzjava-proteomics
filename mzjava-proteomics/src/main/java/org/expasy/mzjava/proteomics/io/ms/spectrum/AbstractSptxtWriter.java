/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.io.ms.spectrum;

import com.google.common.base.Preconditions;
import org.expasy.mzjava.core.io.ms.spectrum.PeakListPrecisionFormat;
import org.expasy.mzjava.core.mol.MassCalculator;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;

import java.io.IOException;
import java.io.Writer;
import java.text.NumberFormat;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public abstract class AbstractSptxtWriter {

    private final Writer writer;
    private NumberFormat mzFormat;
    private NumberFormat intensityFormat;

    public AbstractSptxtWriter(Writer writer, PeakList.Precision precision) {

        setMzFormat(PeakListPrecisionFormat.getMzFormat(precision));
        setIntensityFormat(PeakListPrecisionFormat.getIntensityFormat(precision));

        checkNotNull(writer);

        this.writer = writer;
    }

    public void setMzFormat(NumberFormat mzFormat) {

        this.mzFormat = mzFormat;
    }

    public void setIntensityFormat(NumberFormat intensityFormat) {

        this.intensityFormat = intensityFormat;
    }

    public void write(PeakList<? extends PeakAnnotation> peakList, Peptide peptide, Collection<String> proteinAccessionNumbers) throws IOException {

        writeHeader(peakList, peptide, writer, proteinAccessionNumbers);
        writePeaks(peakList, writer);
        writer.write("\n");
    }

    protected void writePeaks(PeakList<? extends PeakAnnotation> peakList, Writer writer) throws IOException {

        if (peakList.isEmpty()) return;

        double intensityScale = 10000 / peakList.getIntensity(peakList.getMostIntenseIndex());

        for (int i = 0; i < peakList.size(); i++) {

            writer.write(formatMz(peakList.getMz(i)));
            writer.write("\t");
            writer.write(formatIntensity(peakList.getIntensity(i) * intensityScale));
            writer.write("\t");
            writer.write(formatAnnotations(i, peakList));
            writer.write("\n");
        }
    }

    protected abstract String formatAnnotations(int peakIndex, PeakList<? extends PeakAnnotation> peakList) throws IOException;

    private void writeHeader(PeakList peakList, Peptide peptide, Writer writer, Collection<String> proteinAccessionNumbers) throws IOException {

        Preconditions.checkArgument(peakList.getPrecursor().getChargeList().length == 1, "The sptxt format can only represent a single charge for a precursor");

        writeName(peakList, peptide, writer);
        writeLibId(peakList, peptide, writer);
        writeMw(peakList, peptide, writer);
        writePrecursorMz(peakList, peptide, writer);
        writeFullName(peakList, peptide, writer);
        writeComment(peakList, peptide, writer, proteinAccessionNumbers); writer.write("\n");

        writeNumPeaks(peakList, writer);
    }

    private void writeNumPeaks(PeakList peakList, Writer writer) throws IOException {

        writer.write("NumPeaks: ");
        writer.write(String.valueOf(peakList.size()));
        writer.write("\n");
    }

    private String toProteinString(Collection<String> accessionNumbers) {

        if(accessionNumbers.isEmpty()) return "unknown";

        StringBuilder buff = new StringBuilder();
        buff.append(accessionNumbers.size());

        for(String acc : accessionNumbers) {

            buff.append("/");
            buff.append(acc);
        }

        return buff.toString();
    }

    protected void writeComment(PeakList peakList, Peptide peptide, Writer writer, Collection<String> proteinAccessionNumbers) throws IOException {

        writer.write("Comment: Mz_exact=");
        writer.write(formatMz((peptide.getMolecularMass() + (peakList.getPrecursor().getCharge() * MassCalculator.PROTON_MASS)) / peakList.getPrecursor().getCharge()));
        writer.write(" Mz_diff=");
        writer.write(formatMz(peakList.getPrecursor().getMz() - peptide.calculateMz(peakList.getPrecursor().getCharge())));
        writer.write(" Parent=");
        writer.write(formatMz(peakList.getPrecursor().getMz()));
        writer.write(" PrecursorIntensity=");
        writer.write(formatIntensity(peakList.getPrecursor().getIntensity()));
        writer.write(" TotalIonCurrent=");
        writer.write(formatMz(peakList.getTotalIonCurrent()));
        writer.write(" Protein=" + toProteinString(proteinAccessionNumbers));

        // Parent: Observed parent m/z
        // SPTXT
        //  PrecursorIntensity=5.2e+05
        // MSP
        //  Mz_diff: Observed–Theoretical m/z
        //  Mz_exact: Exact theoretical m/z for parent ion

        // modifications
        writer.write(" Mods=");

        if (peptide.hasModifications()) {

            int[] indices = peptide.getModificationIndexes(ModAttachment.all);

            writer.write(Integer.toString(peptide.getModificationCount()));

            // Mods=2/4,C,Carbamidomethyl/6,C,Carbamidomethyl
            for (int index : indices) {

                List<Modification> mods;

                if (index==0) {
                    mods = peptide.getModificationsAt(index, EnumSet.of(ModAttachment.N_TERM));
                    for (Modification modification : mods) {

                        writer.write("/");
                        writer.write(Integer.toString(-1));
                        writer.write(",");
                        writer.write(peptide.getSymbol(index).toString());
                        writer.write(",");

                        writer.write(modification.getLabel());
                    }
                }

                // Monoisotopic context
                mods = peptide.getModificationsAt(index, EnumSet.of(ModAttachment.SIDE_CHAIN));

                for (Modification modification : mods) {

                    writer.write("/");
                    writer.write(Integer.toString(index));
                    writer.write(",");
                    writer.write(peptide.getSymbol(index).toString());
                    writer.write(",");

                    writer.write(modification.getLabel());
                }

                if (index==peptide.size()-1) {
                    mods = peptide.getModificationsAt(index, EnumSet.of(ModAttachment.C_TERM));
                    for (Modification modification : mods) {

                        writer.write("/");
                        writer.write(Integer.toString(-2));
                        writer.write(",");
                        writer.write(peptide.getSymbol(index).toString());
                        writer.write(",");

                        writer.write(modification.getLabel());
                    }
                }

            }
        } else {
            writer.write("0");
        }
    }

    private void writeFullName(PeakList peakList, Peptide peptide, Writer writer) throws IOException {

        writer.write("FullName: ");
        writer.write(peptide.toString());
        writer.write("/");
        writer.write(String.valueOf(peakList.getPrecursor().getCharge()));
        writer.write("\n");
    }

    private void writePrecursorMz(PeakList peakList, Peptide peptide, Writer writer) throws IOException {

        writer.write("PrecursorMZ: ");
        // exact mz of the peptide ion
        writer.write(formatMz(peptide.calculateMz(peakList.getPrecursor().getCharge())));
        writer.write("\n");
    }

    private void writeMw(PeakList peakList, Peptide peptide, Writer writer) throws IOException {

        // exact molecular mass of the peptide ion
        writer.write("MW: ");
        writer.write(formatMz(peptide.getMolecularMass() + (peakList.getPrecursor().getCharge() * MassCalculator.PROTON_MASS)));
        writer.write("\n");
    }

    protected abstract void writeLibId(PeakList peakList, Peptide peptide, Writer writer) throws IOException;

    protected void writeName(PeakList peakList, Peptide peptide, Writer writer) throws IOException {

        writer.write("Name: ");
        writer.write(peptide.toSymbolString());
        writer.write("/");
        writer.write(String.valueOf(peakList.getPrecursor().getCharge()));
        writer.write("\n");
    }

    protected String formatMz(double mz) {

        return mzFormat.format(mz);
    }

    protected String formatIntensity(double intensity) {

        return intensityFormat.format(intensity);
    }

    public void close() throws IOException {

        writer.close();
    }
}
