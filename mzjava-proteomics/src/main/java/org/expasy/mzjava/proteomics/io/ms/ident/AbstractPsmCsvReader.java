/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.io.ms.ident;


import com.google.common.base.Optional;
import org.apache.commons.io.FilenameUtils;
import org.expasy.mzjava.proteomics.ms.ident.PeptideMatch;
import org.expasy.mzjava.proteomics.ms.ident.PeptideProteinMatch;
import org.expasy.mzjava.proteomics.ms.ident.SpectrumIdentifier;

import java.io.File;
import java.io.InputStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.*;
import java.util.Collection;
import java.util.Properties;

/**
 * Abstract implementation of {@code PsmReader} with a default parse template method.
 * Each step of this parsing will have to be defined by inherited classes.
 *
 * @author fnikitin
 * Date: 4/9/13
 */
public abstract class AbstractPsmCsvReader implements PsmReader {

    private int lineNumber;

    @Override
    public void parse(File file, PSMReaderCallback callback) {

        parse(file, "\t", callback);
    }

    @Override
    public void parse(InputStream inputStream, PSMReaderCallback callback) {

        throw new UnsupportedOperationException("Can only parse csv from files");
    }

    @Override
    public void parse(Reader reader, PSMReaderCallback callback) {

        throw new UnsupportedOperationException("Can only parse csv from files");
    }

    public void parse(File file, String delimiter, PSMReaderCallback callback) {

        try {
            // load the driver into memory
            Class.forName("org.relique.jdbc.csv.CsvDriver");

            Properties props = new Properties();
            props.put("fileExtension", "." + FilenameUtils.getExtension(file.getName()));
            // create a connection. The first command line parameter is assumed to
            //  be the directory in which the .csv files are held
            try (Connection conn = DriverManager.getConnection("jdbc:relique:csv:" + file.getParent() + "?separator=" + URLEncoder.encode(delimiter, "UTF-8"), props);

                 // create a Statement object to execute the query with
                 Statement stmt = conn.createStatement();

                 // Select the ID and NAME columns from sample.csv
                 ResultSet results = stmt.executeQuery("SELECT * FROM " + FilenameUtils.removeExtension(file.getName()))
            ) {

                lineNumber = 0;

                while (results.next()) {

                    Optional<PeptideMatch> peptideMatch = makePeptideMatch(results);

                    if (peptideMatch.isPresent()) {

                        for (String accessionNumber : getAccessionNumbers(results)) {

                            peptideMatch.get().addProteinMatch(new PeptideProteinMatch(accessionNumber, Optional.absent(), Optional.absent(), Optional.absent(), PeptideProteinMatch.HitType.UNKNOWN));
                        }

                        setValues(peptideMatch.get(), results);

                        callback.resultRead(getSpectrumId(results), peptideMatch.get());
                    }

                    lineNumber++;
                }
            }
        } catch (ClassNotFoundException | SQLException | UnsupportedEncodingException e) {
            throw new IllegalStateException(e);
        }
    }

    public final int getLineNumber() {

        return lineNumber;
    }

    /**
     * Create a new {@code SpectrumIdentifier} from a given {@code ResultSet}
     *
     * @param results the current results to extract identified spectrum infos from
     * @return a new SpectrumIdentifier
     * @throws SQLException
     */
    protected abstract SpectrumIdentifier getSpectrumId(ResultSet results) throws SQLException;

    /**
     * Extract accession numbers
     *
     * @param results the ResultSet to extract accession numbers from
     * @return a collection of accession number
     * @throws SQLException
     */
    protected abstract Collection<String> getAccessionNumbers(ResultSet results) throws SQLException;

    /**
     * Create a new {@code PeptideMatch} from a given {@code ResultSet}
     *
     * @param results the current results to extract PSM infos from
     * @return an optional PeptideMatch instance
     * @throws SQLException
     */
    protected abstract Optional<PeptideMatch> makePeptideMatch(ResultSet results) throws SQLException;

    /**
     * Set {@code PeptideMatch} score values
     *
     * @param peptideMatch the peptide match to set values
     * @param results the ResultSet to extract score values from
     * @throws SQLException
     */
    protected abstract void setValues(PeptideMatch peptideMatch, ResultSet results) throws SQLException;
}
