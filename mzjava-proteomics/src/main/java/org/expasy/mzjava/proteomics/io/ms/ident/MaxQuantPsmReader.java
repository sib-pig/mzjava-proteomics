/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.io.ms.ident;

import com.google.common.base.Optional;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Lists;
import org.expasy.mzjava.core.ms.spectrum.TimeUnit;
import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.proteomics.mol.modification.CompositeModResolver;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.proteomics.mol.modification.ModificationResolver;
import org.expasy.mzjava.proteomics.mol.modification.unimod.UnimodModificationResolver;
import org.expasy.mzjava.proteomics.ms.ident.PeptideMatch;
import org.expasy.mzjava.proteomics.ms.ident.SpectrumIdentifier;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class MaxQuantPsmReader extends AbstractPsmCsvReader {

    private final ModificationResolver modResolver;

    private final Pattern aaPattern = Pattern.compile("(.)\\((..)\\)|([A-Z])");

    public MaxQuantPsmReader() {

        this(makeDefaultModResolver());
    }

    public MaxQuantPsmReader(ModificationResolver modResolver) {

        this.modResolver = new CompositeModResolver(modResolver, makeDefaultModResolver(), modResolver);
    }

    private static UnimodModificationResolver makeDefaultModResolver() {

        UnimodModificationResolver modResolver = new UnimodModificationResolver();
        modResolver.putTranslate("de", "Deamidated");
        modResolver.putTranslate("ox", "Oxidation");
        modResolver.putTranslate("ac", "Acetyl");
        return modResolver;
    }

    @Override
    public Optional<PeptideMatch> makePeptideMatch(ResultSet results) throws SQLException {

        Matcher matcher = aaPattern.matcher(results.getString("Modified sequence"));

        List<AminoAcid> sequence = new ArrayList<>();
        ListMultimap<Object, Modification> modMatchMap = ArrayListMultimap.create();

        int size = 0;
        while (matcher.find()) {

            String modAA = matcher.group(1);
            String mod = matcher.group(2);
            String unModAA = matcher.group(3);

            if (modAA != null) {

                Optional<Modification> modOpt = modResolver.resolve(mod);
                if ("_".equals(modAA)) {

                    if (modOpt.isPresent()) {
                        modMatchMap.put(size == 0 ? ModAttachment.N_TERM : ModAttachment.C_TERM, modOpt.get());
                    }
                } else {

                    sequence.add(AminoAcid.valueOf(modAA));
                    if (modOpt.isPresent()) {
                        modMatchMap.put(size, modOpt.get());
                    }
                    size += 1;
                }
            } else {

                sequence.add(AminoAcid.valueOf(unModAA));
                size += 1;
            }
        }

        PeptideMatch peptideMatch = new PeptideMatch(sequence);
        for (Object key : modMatchMap.keySet()) {

            for (Modification mod : modMatchMap.get(key)) {

                if (key instanceof Integer) {

                    int position = (Integer) key;
                    peptideMatch.addModificationMatch(position, mod);
                } else if (key instanceof ModAttachment) {

                    ModAttachment modAttachment = (ModAttachment) key;
                    peptideMatch.addModificationMatch(modAttachment, mod);
                }
            }
        }

        return Optional.of(peptideMatch);
    }

    @Override
    protected SpectrumIdentifier getSpectrumId(ResultSet results) throws SQLException {

        int charge = results.getInt("Charge");
        int scanNumber = results.getInt("Scan number");

        SpectrumIdentifier identifier = new SpectrumIdentifier(results.getString("Raw file") + "." + scanNumber + "." + scanNumber + "." + charge);
        identifier.setAssumedCharge(charge);
        identifier.setIndex(results.getInt("Scan index"));
        identifier.addScanNumber(scanNumber);
        identifier.setPrecursorNeutralMass(results.getDouble("Mass"));
        identifier.addRetentionTime(results.getDouble("Retention time"), TimeUnit.MINUTE);

        return identifier;
    }

    @Override
    protected Collection<String> getAccessionNumbers(ResultSet results) throws SQLException {

        String proteins = results.getString("Proteins");
        if (proteins != null) {

            String[] accessions = proteins.split(";");
            return Lists.newArrayList(accessions);
        } else {

            return Collections.emptyList();
        }
    }

    @Override
    protected void setValues(PeptideMatch peptideMatch, ResultSet results) throws SQLException {

        peptideMatch.setMassDiff(Optional.fromNullable(results.getDouble("Mass Error [ppm]")));
        peptideMatch.setNumMissedCleavages(Optional.fromNullable(results.getInt("Missed cleavages")));
        peptideMatch.setTotalNumIons(Optional.fromNullable(results.getInt("Number of Matches")));
        peptideMatch.addScore("score", results.getDouble("Score"));
        peptideMatch.addScore("delta-score", results.getDouble("Delta score"));
    }
}
