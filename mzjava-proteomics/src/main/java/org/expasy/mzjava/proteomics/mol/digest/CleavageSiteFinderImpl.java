package org.expasy.mzjava.proteomics.mol.digest;

import com.google.common.base.Optional;
import gnu.trove.list.TIntList;
import org.expasy.mzjava.core.mol.SymbolSequence;
import org.expasy.mzjava.proteomics.mol.AminoAcid;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * The default implementation of CleavageSiteFinder
 *
 * @author fnikitin
 */
public class CleavageSiteFinderImpl implements CleavageSiteFinder {

    private final CleavageSiteMatcher cs;
    private final Pattern regex;

    public CleavageSiteFinderImpl(CleavageSiteMatcher cs) {

        checkNotNull(cs);

        this.cs = cs;
        this.regex = cs.getRegex();
    }

    private class SiteAccumulator implements CleavageSiteVisitor {

        private final TIntList sites;

        public SiteAccumulator(TIntList acc) {

            sites = acc;
        }

        @Override
        public void visit(SymbolSequence<AminoAcid> aaSeq, int index) {

            sites.add(index);
        }
    }

    @Override
    public void find(SymbolSequence<AminoAcid> aaSeq, TIntList sites) {

        SiteAccumulator acc = new SiteAccumulator(sites);

        find(aaSeq, Optional.<CleavageSiteVisitor>of(acc));
    }

    @Override
    public int countCleavageSites(SymbolSequence<AminoAcid> aaSeq) {

        return find(aaSeq, Optional.<CleavageSiteVisitor>absent());
    }

    public final CleavageSiteMatcher getCleavageSiteMatcher() {
        return cs;
    }

    /**
     * Find potential cleavage sites over the given amino-acid sequence
     *
     * @param aaSeq an amino-acid sequence
     * @param visitor an optional CleavageSiteVisitor
     * @return the number of cleavage sites
     */
    public int find(SymbolSequence<AminoAcid> aaSeq, Optional<CleavageSiteVisitor> visitor) {

        Matcher matcher = regex.matcher(aaSeq.toSymbolString());

        int count = 0;
        while (matcher.find()) {

            if (visitor.isPresent())
                visitor.get().visit(aaSeq, matcher.start());

            count++;
        }

        return count;
    }
}
