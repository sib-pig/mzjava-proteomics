/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.io.ms.ident;

import com.google.common.base.Optional;
import org.expasy.mzjava.core.mol.NumericMass;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.proteomics.mol.modification.unimod.UnimodModificationResolver;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The default ProteinPilot Modification Resolver
 *
 * @author fnikitin
 */
public class ProteinPilotModificationResolver extends UnimodModificationResolver {

    private final Pattern pattern = Pattern.compile("(?:Leu)|(?:Ile)");

    public ProteinPilotModificationResolver() {

        super();

        putTranslate("Protein Terminal Acetyl", "Acetyl");
        // Transpeptidation
        putOverrideUnimod("Arg-add", Modification.parseModification("Arg-add:C6H12N4O"));
        putOverrideUnimod("Arg-loss", Modification.parseModification("Arg-loss:C-6H-12N-4O-1"));
        putOverrideUnimod("No Methylthio", Modification.parseModification("No Methylthio:H-2C-1S-1"));
        putOverrideUnimod("GlnGlnGlnThrGlyGly", Modification.parseModification("QQQTGG:H37C23N9O10")); //H(37) C(23) N(9) O(10)
        putOverrideUnimod("Met->Hcy", new Modification("Met->Hcy", new NumericMass(-14.01565006)));
    }

    @Override
    public Optional<Modification> resolve(String input) {

        Matcher matcher = pattern.matcher(input);

        if(matcher.find()) {

            input = matcher.replaceAll("Xle");
        }

        return super.resolve(input);
    }
}
