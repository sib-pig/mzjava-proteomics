package org.expasy.mzjava.proteomics.mol.digest;

/**
 * Generates all possible intervals of integers in the closed interval [lower, upper] from [lower, lower+1] to [upper-1, upper]
 *
 * @author fnikitin
 * Date: 10/31/13
 */
class ForwardIntIntervalIterator extends IntIntervalIterator {

    public ForwardIntIntervalIterator(int lower, int upper) {

        super(lower, upper);
    }

    public ForwardIntIntervalIterator(int lower, int upper, int rangeMax) {

        super(lower, upper, rangeMax);
    }

    protected void init() {

        i = lower;
        j = lower;
    }

    protected void nextIteration(int rangeMax) {

        j++;

        if (isStopJ(rangeMax)) {

            i++;
            j = i+1;

            // last iteration
            if (i == upper)
                hasNext = false;
        }
    }

    private boolean isStopJ(int rangeMax) {

        return j == upper+1 || j-i > rangeMax;
    }
}
