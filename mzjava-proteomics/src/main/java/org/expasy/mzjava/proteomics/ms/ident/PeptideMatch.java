/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.ms.ident;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimap;
import gnu.trove.map.TObjectDoubleMap;
import gnu.trove.map.hash.TObjectDoubleHashMap;
import org.expasy.mzjava.core.mol.SymbolSequence;
import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.ms.spectrum.PeptideSpectrum;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.google.common.base.Preconditions.*;

/**
 * PeptideMatch stores the match information from a peptide spectrum match
 *
 * @author Oliver Horlacher
 * @version 1.0
 */
public class PeptideMatch implements SymbolSequence<AminoAcid>, Comparable<PeptideMatch> {

    private Optional<Integer> rank = Optional.absent();
    private final List<AminoAcid> sequence = new ArrayList<>();
    private Optional<Integer> numMatchedIons = Optional.absent();
    private Optional<Integer> totalNumIons = Optional.absent();
    private Optional<Double> massDiff = Optional.absent();
    private Optional<Integer> numMissedCleavages = Optional.absent();
    private Optional<Boolean> rejected = Optional.absent();
    private final ListMultimap<Integer, ModificationMatch> sideChainMatchMap = ArrayListMultimap.create();
    private final ListMultimap<ModAttachment, ModificationMatch> termMatchMap = ArrayListMultimap.create();
    private List<PeptideProteinMatch> proteinMatches = new ArrayList<>();
    private Optional<Double> neutralPeptideMass = Optional.absent();

    private final TObjectDoubleMap<String> scoreMap = new TObjectDoubleHashMap<>();

    private transient final ModificationMatchResolver modificationMatchResolver = new ModificationMatchResolver() {
        @Override
        public Optional<Modification> resolve(ModificationMatch modMatch) {

            if (modMatch == null) return Optional.absent();

            if (modMatch.getCandidateCount() == 0) return Optional.absent();
            else return Optional.of(modMatch.getModificationCandidate(0));
        }
    };

    /**
     * Create a peptide match given an amino acid sequence
     *
     * @param sequence the amino acid sequence
     */
    public PeptideMatch(AminoAcid... sequence) {

        Collections.addAll(this.sequence, sequence);
    }

    /**
     * Create a peptide match given an amino acid sequence
     *
     * @param sequence the amino acid sequence
     */
    public PeptideMatch(List<AminoAcid> sequence) {

        this.sequence.addAll(sequence);
        neutralPeptideMass = Optional.absent();
    }

    /**
     * Create a peptide match given an amino acid sequence
     *
     * @param peptideSequence the amino acid sequence
     */
    public PeptideMatch(String peptideSequence) {

        neutralPeptideMass = Optional.absent();
        for (char aa : peptideSequence.toCharArray()) {

            try {
                sequence.add(AminoAcid.valueOf(Character.toString(aa)));
            } catch (IllegalArgumentException e) {

                throw new IllegalStateException("peptideSequence = " + peptideSequence, e);
            }
        }
    }

    /**
     * Create a peptide match given an amino acid esequence
     *
     * @param sequence the amino acid sequence
     */
    public PeptideMatch(SymbolSequence<AminoAcid> sequence) {

        checkNotNull(sequence);

        neutralPeptideMass = Optional.absent();
        for (int i = 0; i < sequence.size(); i++) {

            this.sequence.add(sequence.getSymbol(i));
        }
    }

    /**
     * Construct a PeptideMatch from a peptide spectrum and score.
     * <p/>
     * This constructor sets the score and copies the peptide and protein information.
     *
     * @param peptideSpectrum the peptideSequence for which the PeptideMatch is to be created
     * @param scoreName       the name of the score
     * @param score           the score
     */
    public PeptideMatch(PeptideSpectrum peptideSpectrum, String scoreName, double score) {

        this(peptideSpectrum.getPeptide());

        scoreMap.put(scoreName, score);

        List<String> proteinIds = peptideSpectrum.getProteinAccessionNumbers();
        if (!proteinIds.isEmpty()) {

            proteinMatches = new ArrayList<>();

            for (String proteinId : proteinIds) {

                this.proteinMatches.add(new PeptideProteinMatch(proteinId, Optional.<String>absent(), Optional.<String>absent() , Optional.<String>absent(), PeptideProteinMatch.HitType.UNKNOWN));
            }
        }
        neutralPeptideMass = Optional.absent();
    }

    /**
     * Add the score to the score map
     *
     * @param name  the name of the score
     * @param value the value of the score
     */
    public void addScore(String name, double value) {

        Preconditions.checkNotNull(name);

        scoreMap.put(name, value);
    }

    /**
     * Returns a reference to the score map
     *
     * @return a reference to the score map
     */
    public TObjectDoubleMap<String> getScoreMap() {

        return scoreMap;
    }

    /**
     * Returns true if there is a score for <code>scoreName</code>
     *
     * @param scoreName the name of the score to test fore
     * @return true if there is a score for <code>scoreName</code>
     */
    public boolean hasScore(String scoreName) {

        return scoreMap.containsKey(scoreName);
    }

    /**
     * Return the score that is associtaed with <code>scoreName</code>
     *
     * @param scoreName the name of the score
     * @return the score that is associtaed with <code>scoreName</code>
     * @throws IllegalArgumentException if there is no score associated with <code>scoreName</code>
     */
    public double getScore(String scoreName) {

        Preconditions.checkArgument(scoreMap.containsKey(scoreName));

        return scoreMap.get(scoreName);
    }

    /**
     * Return the rank of this PeptideMatch
     *
     * @return the rank of this PeptideMatch
     */
    public Optional<Integer> getRank() {

        return rank;
    }

    /**
     * Set the rank of this PeptideMatch
     *
     * @param rank the rank of the PeptideMatch
     */
    public void setRank(Optional<Integer> rank) {

        this.rank = rank;
    }

    /**
     * Return the number of ions that are were matched
     *
     * @return the number of ions that are were matched
     */
    public Optional<Integer> getNumMatchedIons() {

        return numMatchedIons;
    }

    /**
     * Set the number of ions that were matched
     *
     * @param numMatchedIons the number of ions tha twere matched
     */
    public void setNumMatchedIons(Optional<Integer> numMatchedIons) {

        this.numMatchedIons = numMatchedIons;
    }

    /**
     * Return the total number of ions that were generated for the Peptide
     *
     * @return the total number of ions that were generated for the Peptide
     */
    public Optional<Integer> getTotalNumIons() {

        return totalNumIons;
    }

    /**
     * Set the total number of ion that were generated for the Peptide
     *
     * @param totalNumIons the total number of ions
     */
    public void setTotalNumIons(Optional<Integer> totalNumIons) {

        this.totalNumIons = totalNumIons;
    }

    /**
     * Returns the difference between the theoretical and experimental mass.
     *
     * @return the difference between the theoretical and experimental mass
     */
    public Optional<Double> getMassDiff() {

        return massDiff;
    }

    /**
     * Set the difference between the the theoretical and experimental mass
     *
     * @param massDiff the mass difference
     */
    public void setMassDiff(Optional<Double> massDiff) {

        this.massDiff = massDiff;
    }

    /**
     * Return the number of missed cleavages that the Peptide contains
     *
     * @return the number of missed cleavages that the Peptide contains
     */
    public Optional<Integer> getNumMissedCleavages() {

        return numMissedCleavages;
    }

    /**
     * Set the number of missed cleavages that the Peptide contains
     *
     * @param numMissedCleavages the number of missed cleavages
     */
    public void setNumMissedCleavages(Optional<Integer> numMissedCleavages) {

        this.numMissedCleavages = numMissedCleavages;
    }

    /**
     * Returns true if this PeptideMatch was rejected, false otherwise
     *
     * @return true if this PeptideMatch was rejected, false otherwise
     */
    public Optional<Boolean> isRejected() {

        return rejected;
    }

    /**
     * Set whether this PeptideMatch was rejected or not
     *
     * @param rejected boolean to indicate wether the peptide was rejected or not
     */
    public void setRejected(Optional<Boolean> rejected) {

        this.rejected = rejected;
    }

    public double getNeutralPeptideMass() {

        return neutralPeptideMass.get();
    }

    public void setNeutralPeptideMass(double neutralPeptideMass) {

        this.neutralPeptideMass = Optional.of(neutralPeptideMass);
    }

    /**
     * Add a protein match to this PeptideMatch
     *
     * @param proteinMatch the protein accession to add
     */
    public void addProteinMatch(PeptideProteinMatch proteinMatch) {

        checkNotNull(proteinMatch);

        if (proteinMatches == null) proteinMatches = new ArrayList<>();
        this.proteinMatches.add(proteinMatch);
    }


    /**
     * Add all the accessions in <code>proteinAC</code> to this PeptideMatch
     *
     * @param matchCollection collection containing the accessions to add
     */
    public void addProteinMatches(Collection<PeptideProteinMatch> matchCollection) {

        if (matchCollection == null || matchCollection.isEmpty()) return;

        if (this.proteinMatches == null) this.proteinMatches = new ArrayList<>();
        this.proteinMatches.addAll(matchCollection);
    }

    /**
     * Return a reference to the protein accession list
     *
     * @return a reference to the protein accession list
     */
    public List<PeptideProteinMatch> getProteinMatches() {

        return proteinMatches;
    }

    @Override
    public String toString() {

        final StringBuilder sb = new StringBuilder();

        if (termMatchMap.containsKey(ModAttachment.N_TERM)) {

            appendMods(sb, termMatchMap.get(ModAttachment.N_TERM));
            sb.append('_');
        }

        for (int i = 0; i < size(); i++) {

            sb.append(sequence.get(i));
            if (sideChainMatchMap.containsKey(i)) appendMods(sb, sideChainMatchMap.get(i));
        }

        if (termMatchMap.containsKey(ModAttachment.C_TERM)) {

            sb.append('_');
            appendMods(sb, termMatchMap.get(ModAttachment.C_TERM));
        }

        return "PeptideMatch{" +
                "rank=" + rank +
                ", rejected=" + rejected +
                ", massDiff=" + massDiff +
                ", numMatchedIons=" + numMatchedIons +
                ", numMissedCleavages=" + numMissedCleavages +
                ", totalNumIons=" + totalNumIons +
                ", peptide=" + sb +
                '}';
    }

    /**
     * Used by toString
     *
     * @param sb   the StringBuilder
     * @param mods the modifications
     */
    private void appendMods(StringBuilder sb, List<ModificationMatch> mods) {

        sb.append('(');
        for (ModificationMatch mod : mods) {

            sb.append(mod);
            sb.append(", ");
        }
        int length = sb.length();
        sb.delete(length - 2, length);
        sb.append(')');
    }

    /**
     * Returns true if this PeptideMatch and <code>match</code> have the same peptide
     * sequence and modifications.
     *
     * @param match the PeptideMatch to test
     * @return true if this PeptideMatch and <code>match</code> have the same peptide
     * sequence and modifications
     */
    public boolean sameModPeptide(PeptideMatch match) {

        return termMatchMap.equals(match.termMatchMap) &&
                sideChainMatchMap.equals(match.sideChainMatchMap) &&
                sequence.equals(match.sequence);
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (!(o instanceof PeptideMatch)) return false;

        PeptideMatch that = (PeptideMatch) o;

        return Double.compare(that.massDiff.or(0.0), massDiff.or(0.0)) == 0 &&
                numMatchedIons.equals(that.numMatchedIons) &&
                numMissedCleavages.equals(that.numMissedCleavages) &&
                rank.equals(that.rank) && rejected.equals(that.rejected) &&
                totalNumIons.equals(that.totalNumIons) &&
                sideChainMatchMap.equals(that.sideChainMatchMap) &&
                termMatchMap.equals(that.termMatchMap) &&
                scoreMap.equals(that.scoreMap) &&
                sequence.equals(that.sequence);
    }

    @Override
    public int hashCode() {

        int result;
        long temp;
        result = rank.or(-1);
        result = 31 * result + sequence.hashCode();
        result = 31 * result + numMatchedIons.or(-1);
        result = 31 * result + totalNumIons.or(-1);
        temp = massDiff.or(0.0) != +0.0d ? Double.doubleToLongBits(massDiff.or(0.0)) : 0L;
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + numMissedCleavages.or(-1);
        result = 31 * result + (rejected.or(Boolean.FALSE) ? 1 : 0);
        result = 31 * result + sideChainMatchMap.hashCode();
        result = 31 * result + scoreMap.hashCode();
        return result;
    }

    @Override
    public int compareTo(PeptideMatch o) {

        return Double.compare(rank.or(-1), o.rank.or(-1));
    }

    @Override
    public AminoAcid getSymbol(int index) {

        return sequence.get(index);
    }

    @Override
    public String toSymbolString() {

        StringBuilder buff = new StringBuilder();

        for (AminoAcid aa : sequence) {

            buff.append(aa.getSymbol());
        }

        return buff.toString();
    }

    @Override
    public int size() {

        return sequence.size();
    }

    /**
     * Add a the <code>modificationMatch</code> to the side chain of residue at <code>index</code>
     *
     * @param index             the index of the reside where the ModificationMatch is to be added
     * @param modificationMatch the ModificationMatch that is to be added
     */
    public void addModificationMatch(int index, ModificationMatch modificationMatch) {

        checkNotNull(modificationMatch);
        checkElementIndex(index, size());

        sideChainMatchMap.put(index, modificationMatch);
    }

    /**
     * Convenience method for adding a ModificationMatch.
     *
     * @param index the index of the modification
     * @param modificationMass the mass difference due to the modification
     * @return the ModificationMatch that was added
     */
    public ModificationMatch addModificationMatch(int index, double modificationMass) {

        ModificationMatch modificationMatch = new ModificationMatch(
                modificationMass,
                getSymbol(index),
                index,
                ModAttachment.SIDE_CHAIN
        );
        addModificationMatch(index, modificationMatch);
        return modificationMatch;
    }

    /**
     * Convenience method for adding a ModificationMatch.
     *
     * @param index the index of the modification
     * @param modification the modification
     * @return the ModificationMatch that was added
     */
    public ModificationMatch addModificationMatch(int index, Modification modification) {

        ModificationMatch modificationMatch = new ModificationMatch(
                modification,
                getSymbol(index),
                index,
                ModAttachment.SIDE_CHAIN
        );
        addModificationMatch(index, modificationMatch);
        return modificationMatch;
    }

    /**
     * Add a the <code>modificationMatch</code> to either end of the this peptide match. The end is
     * specified busing the <code>modAttachment</code>
     *
     * @param modAttachment     specifies which end of the peptide the modificationMatch is to be added
     * @param modificationMatch the ModificationMatch that is to be added
     */
    public void addModificationMatch(ModAttachment modAttachment, ModificationMatch modificationMatch) {

        checkNotNull(modAttachment);
        checkNotNull(modificationMatch);
        checkArgument(modAttachment != ModAttachment.SIDE_CHAIN, "To add a side chain modification a residue index is required. Use addModificationMatch(int index, ModificationMatch modificationMatch) instead");

        termMatchMap.put(modAttachment, modificationMatch);
    }

    /**
     * Convenience method for adding a ModificationMatch.
     *
     * @param modAttachment the mod attachment is required to be either ModAttachment.N_TERM or ModAttachment.C_TERM
     * @param modificationMass the mass difference due to the modification
     * @return the ModificationMatch that was added
     */
    public ModificationMatch  addModificationMatch(ModAttachment modAttachment, double modificationMass) {

        int index;
        switch (modAttachment) {
            case N_TERM:

                index = 0;
                break;
            case C_TERM:

                index = size() - 1;
                break;
            case SIDE_CHAIN:
            default:
                throw new IllegalStateException("Cannot add a terminal modification with attachment " + modAttachment);
        }

        ModificationMatch modificationMatch = new ModificationMatch(
                modificationMass,
                getSymbol(index),
                index,
                modAttachment
        );
        addModificationMatch(modAttachment, modificationMatch);
        return modificationMatch;
    }

    /**
     * Convenience method for adding a ModificationMatch.
     *
     * @param modAttachment the mod attachment is required to be either ModAttachment.N_TERM or ModAttachment.C_TERM
     * @param modification the mass modification
     * @return the ModificationMatch that was added
     */
    public ModificationMatch  addModificationMatch(ModAttachment modAttachment, Modification modification) {

        int index;
        switch (modAttachment) {
            case N_TERM:

                index = 0;
                break;
            case C_TERM:

                index = size() - 1;
                break;
            case SIDE_CHAIN:
            default:
                throw new IllegalStateException("Cannot add a terminal modification with attachment " + modAttachment);
        }

        ModificationMatch modificationMatch = new ModificationMatch(
                modification,
                getSymbol(index),
                index,
                modAttachment
        );
        addModificationMatch(modAttachment, modificationMatch);
        return modificationMatch;
    }

    /**
     * Returns the number of modifications that theis peptide match has.
     *
     * @return the number of modifications that theis peptide match has
     */
    public int getModificationCount() {

        return sideChainMatchMap.size() + termMatchMap.size();
    }

    /**
     * Return a list containing all ModificationMatch for ModAttachment's in the <code>attachments</code>
     * set.
     *
     * @param attachments set containing the ModAttachments that are to be retrieved
     * @return a list containing all ModificationMatch for ModAttachment's in the <code>attachments</code>
     * set
     */
    public List<ModificationMatch> getModifications(Set<ModAttachment> attachments) {

        List<ModificationMatch> foundMods = new ArrayList<>();

        if (attachments.contains(ModAttachment.N_TERM)) foundMods.addAll(termMatchMap.get(ModAttachment.N_TERM));
        if (attachments.contains(ModAttachment.C_TERM)) foundMods.addAll(termMatchMap.get(ModAttachment.C_TERM));
        if (attachments.contains(ModAttachment.SIDE_CHAIN)) {

            Set<Integer> indexes = sideChainMatchMap.keySet();
            for (Integer index : indexes) {

                foundMods.addAll(sideChainMatchMap.get(index));
            }
        }

        return foundMods;
    }

    /**
     * Returns a list containing all ModificationMatch for the residue at <code>index</code> that are attached
     * by a ModAttachment contained in the <code>attachments</code> set.
     *
     * @param index       the residue index
     * @param attachments set containing the ModAttachments that are to be retrieved
     * @return a list containing all ModificationMatch for the residue at <code>index</code> that are attached
     * by a ModAttachment contained in the <code>attachments</code> set
     */
    public List<ModificationMatch> getModifications(int index, Set<ModAttachment> attachments) {

        Preconditions.checkElementIndex(index, size());

        List<ModificationMatch> foundMods = new ArrayList<>();

        if (attachments.contains(ModAttachment.SIDE_CHAIN))
            foundMods.addAll(sideChainMatchMap.get(index));

        if (index <= 0 && attachments.contains(ModAttachment.N_TERM))
            foundMods.addAll(termMatchMap.get(ModAttachment.N_TERM));
        else if (index >= size() - 1 && attachments.contains(ModAttachment.C_TERM))
            foundMods.addAll(termMatchMap.get(ModAttachment.C_TERM));

        return foundMods;
    }

    /**
     * Convert this PeptideMatch to a peptide
     *
     * @return the peptide
     * @throws UnresolvableModificationMatchException if a modification match cannot be resolved to a modification
     */
    public Peptide toPeptide() {

        return toPeptide(modificationMatchResolver);
    }

    /**
     * Convert this PeptideMatch to a peptide using the supplied <code>modificationMatchResolver</code>
     * to convert ModificationMatch instances to Modifications.
     *
     * @return the peptide
     * @throws UnresolvableModificationMatchException if a modification match cannot be resolved to a modification
     */
    public Peptide toPeptide(ModificationMatchResolver modMatchResolver) {

        Multimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
        Multimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        for (Integer index : sideChainMatchMap.keySet()) {

            for (ModificationMatch modMatch : sideChainMatchMap.get(index)) {

                Optional<Modification> modOpt = modMatchResolver.resolve(modMatch);
                if (modOpt.isPresent()) {

                    sideChainModMap.put(index, modOpt.get());
                } else {

                    throw new UnresolvableModificationMatchException(modMatch);
                }
            }
        }

        for (ModAttachment attachment : termMatchMap.keySet()) {

            for (ModificationMatch modMatch : termMatchMap.get(attachment)) {

                Optional<Modification> modOpt = modMatchResolver.resolve(modMatch);
                if (modOpt.isPresent()) {

                    termModMap.put(attachment, modOpt.get());
                } else {

                    throw new UnresolvableModificationMatchException(modMatch);
                }
            }
        }

        return new Peptide(sequence, sideChainModMap, termModMap);
    }

    /**
     * Convert this PeptideMatch to a peptide containing only the bare peptide sequence.
     *
     * @return the peptide
     */
    public Peptide toBarePeptide() {

        return new Peptide(sequence);
    }
    /**
     * Returns true if this PeptideMatch contains the <code>proteinAcs</code> protein accession, false otherwise.
     *
     * @param proteinAcs the protein accession to test
     * @return true if this PeptideMatch contains the <code>proteinAcs</code> protein accession, false otherwise
     */
    public boolean containsProtein(String proteinAcs) {

        if (proteinMatches.isEmpty()) return false;

        for (PeptideProteinMatch proteinMatch : proteinMatches) {

            if (proteinMatch.getAccession().equals(proteinAcs))
                return true;
        }

        return false;
    }

    /**
     * Returns true if this PeptideMatch contains a ProteinMatch that has an accession
     * in which the <code>regex</code> pattern can be found.
     *
     * @param regex the regex pattern
     * @return Returns true if this PeptideMatch contains a ProteinMatch that has an accession
     * in which the <code>regex</code> pattern can be found, false otherwise
     */
    public boolean containsProteinMatch(String regex) {

        if (proteinMatches.isEmpty()) return false;

        Pattern pattern = Pattern.compile(regex);
        return containsProteinMatch(pattern);
    }

    /**
     * Returns true if this PeptideMatch contains only ProteinMatch's that have an accession
     * in which the <code>regex</code> pattern can be found.
     *
     * @param regex the regex pattern
     * @return Returns true if this PeptideMatch contains a ProteinMatch that has an accession
     * in which the <code>regex</code> pattern can be found, false otherwise
     */
    public boolean containsOnlyProteinMatch(String regex) {

        if (proteinMatches.isEmpty()) return false;

        Pattern pattern = Pattern.compile(regex);
        return containsOnlyProteinMatch(pattern);
    }

    /**
     * Returns true if this PeptideMatch contains a ProteinMatch that has an accession
     * in which the <code>regex</code> pattern can be found.
     *
     * @param pattern the regex pattern
     * @return Returns true if this PeptideMatch contains a ProteinMatch that has an accession
     * in which the <code>regex</code> pattern can be found, false otherwise
     */
    public boolean containsProteinMatch(Pattern pattern) {

        if (proteinMatches.isEmpty()) return false;

        for (PeptideProteinMatch proteinMatch : proteinMatches) {

            Matcher matcher = pattern.matcher(proteinMatch.getAccession());
            if (matcher.find()) {
                return true;
            }
        }
        return false;
    }


    /**
     * Returns true if this PeptideMatch contains only ProteinMatch's that have an accession
     * in which the <code>regex</code> pattern can be found.
     *
     * @param pattern the regex pattern
     * @return Returns true if this PeptideMatch contains only ProteinMatch's that has an accession
     * in which the <code>regex</code> pattern can be found, false otherwise
     */
    public boolean containsOnlyProteinMatch(Pattern pattern) {

        if (proteinMatches.isEmpty()) return false;

        for (PeptideProteinMatch proteinMatch : proteinMatches) {

            Matcher matcher = pattern.matcher(proteinMatch.getAccession());
            if (!matcher.find()) {
                return false;
            }
        }
        return true;
    }
}
