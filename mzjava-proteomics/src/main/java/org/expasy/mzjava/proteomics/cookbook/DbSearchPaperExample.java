package org.expasy.mzjava.proteomics.cookbook;

import com.google.common.collect.Lists;
import org.expasy.mzjava.core.io.ms.spectrum.MgfReader;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.PpmTolerance;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessorChain;
import org.expasy.mzjava.core.ms.peaklist.peakfilter.CentroidFilter;
import org.expasy.mzjava.core.ms.peaklist.peakfilter.NPeaksPerSlidingWindowFilter;
import org.expasy.mzjava.core.ms.peaklist.peaktransformer.SqrtTransformer;
import org.expasy.mzjava.core.ms.spectrasim.NdpSimFunc;
import org.expasy.mzjava.core.ms.spectrasim.SimFunc;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.proteomics.io.ms.ident.PepXmlWriter;
import org.expasy.mzjava.proteomics.io.ms.ident.PepXmlWriterBuilder;
import org.expasy.mzjava.proteomics.io.ms.ident.pepxml.v117.EngineType;
import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.proteomics.mol.digest.Protease;
import org.expasy.mzjava.proteomics.ms.dbsearch.PeptideSpectrumDB;
import org.expasy.mzjava.proteomics.ms.fragment.BackbonePeakGenerator;
import org.expasy.mzjava.proteomics.ms.fragment.PeptideFragmenter;
import org.expasy.mzjava.proteomics.ms.fragment.PeptideNeutralLossPeakGenerator;
import org.expasy.mzjava.proteomics.ms.ident.PeptideMatch;
import org.expasy.mzjava.proteomics.ms.spectrum.PepFragAnnotation;
import org.expasy.mzjava.proteomics.ms.spectrum.PeptideSpectrum;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
// # $RECIPE$ $NUMBER$ - Simple database search #
//
// ## $PROBLEM$ ##
// You want to use MzJava to perform a database search
//
// ## $SOLUTION$ ##
public class DbSearchPaperExample {

    public static void main(String[] args) throws IOException {

        final PeptideFragmenter peptideFragmenter = new PeptideFragmenter(
                Lists.newArrayList(
                        new BackbonePeakGenerator(EnumSet.of(IonType.b, IonType.y), 50),
                        new PeptideNeutralLossPeakGenerator(Composition.parseComposition("H-2O-1"),
                                EnumSet.of(AminoAcid.S, AminoAcid.T, AminoAcid.D, AminoAcid.E),
                                EnumSet.of(IonType.b, IonType.y), 10.0)
                ), PeakList.Precision.FLOAT);

        final PeptideSpectrumDB peptideSpectrumDB = PeptideSpectrumDB.newBuilder()
                .setPrecursorTolerance(new PpmTolerance(10))
                .setProteinSource(new File("C:\\proteins.fasta")).digestWith(Protease.TRYPSIN).retainPeptidesOfLength(6, 60)
                .setMissedCleavagesTo(2).generateSpectraWith(peptideFragmenter)
                .build();

        final PeakProcessorChain<PeakAnnotation> processorChain = new PeakProcessorChain<>()
                .add(new CentroidFilter<>(0.05, CentroidFilter.IntensityMode.HIGHEST))
                .add(new NPeaksPerSlidingWindowFilter<>(2, 10))
                .add(new SqrtTransformer<>());

        try(final MgfReader reader = new MgfReader(new File("C:\\spectra.mgf"), PeakList.Precision.FLOAT, processorChain)) {

            final SimFunc<PepFragAnnotation, PeakAnnotation> simFunc = new NdpSimFunc<>(5, new AbsoluteTolerance(0.02));
            final PepXmlWriter resultsWriter = PepXmlWriterBuilder.create(EngineType.CUSTOM, "ndp").build();
            while (reader.hasNext()) {

                final MsnSpectrum querySpectrum = reader.next();
                final List<PeptideMatch> peptideMatchList = new ArrayList<>();
                for (PeptideSpectrum theoreticalSpectrum : peptideSpectrumDB.getSpectra(querySpectrum.getPrecursor())) {

                    double score = simFunc.calcSimilarity(theoreticalSpectrum, querySpectrum);
                    peptideMatchList.add(new PeptideMatch(theoreticalSpectrum, "ndp", score));
                }
                resultsWriter.add(querySpectrum, peptideMatchList);
            }

            resultsWriter.write(new File("C:\\results.pep.xml"));
        }
    }
}
