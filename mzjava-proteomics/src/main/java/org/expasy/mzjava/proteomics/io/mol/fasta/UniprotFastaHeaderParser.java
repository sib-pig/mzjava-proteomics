package org.expasy.mzjava.proteomics.io.mol.fasta;


import org.expasy.mzjava.proteomics.mol.Protein;
import org.openide.util.lookup.ServiceProvider;

import java.util.regex.Pattern;

@ServiceProvider(service = FastaHeaderParser.class)
public class UniprotFastaHeaderParser implements FastaHeaderParser {

    private Pattern pattern = null;

    // sp|Q0TET7|Q0TET7_ECOL5 Putative uncharacterized protein OS=Escherichia coli O6:K15:H31 (strain 536 / UPEC) GN=ECP_2553 PE=4 SV=1
    @Override
    public boolean parseHeader(String header, Protein.Builder protein) {

        if (header != null && protein != null) {

            if (pattern == null)
                pattern = Pattern.compile(">\\s?|\\|");

            String[] data = pattern.split(header);

            int offset = data[0].length() == 0 ? 1 : 0;

            if (("sp".equalsIgnoreCase(data[offset]) || "tr".equalsIgnoreCase(data[offset])) && data.length > 1) {

                protein.setAccessionId(data[offset + 1]);

                return true;
            }
        }
        return false;
    }

}
