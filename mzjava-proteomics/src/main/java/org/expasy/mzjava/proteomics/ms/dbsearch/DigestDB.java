/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.ms.dbsearch;

import org.expasy.mzjava.core.ms.Tolerance;

import java.util.Collection;
import java.util.Set;

/**
 * A database that stores digested peptides
 *
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public interface DigestDB {

    /**
     * The action to take if a peptide that have amino acids that are ambiguous (Z, B etc) are returned by the digester
     */
    public enum AmbiguousAminoAcidAction {THROW_EXCEPTION, LOG_AND_SKIP, SKIP}

    /**
     * Retrieves all the peptides that have a mass that is within
     * tolerance of <code>targetMass</code>
     *
     * @param targetMass the target mass
     * @param tolerance the tolerance
     * @return a set containing all the peptides that have a mass that is within tolerance of <code>targetMass</code>
     */
    Set<Digest> getPeptides(double targetMass, Tolerance tolerance);

    /**
     * Add all the peptides that have a mass that is within tolerance of <code>targetMass</code> to
     * the <code>peptides</code> collection
     *
     * @param targetMass the target mass
     * @param tolerance the tolerance
     * @param collection collection to which the peptides are to be added
     */
    void getPeptides(double targetMass, Tolerance tolerance, Collection<Digest> collection);
}
