/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.ms.dbsearch;

import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import org.expasy.mzjava.core.io.IterativeReader;
import org.expasy.mzjava.core.mol.MassCalculator;
import org.expasy.mzjava.core.ms.Tolerance;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessorChain;
import org.expasy.mzjava.core.ms.peaklist.peakfilter.BinnedSpectrumFilter;
import org.expasy.mzjava.core.ms.peaklist.peaktransformer.AddFlankingPeaksTransformer;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.proteomics.io.mol.FastaProteinReader;
import org.expasy.mzjava.proteomics.mol.AAMassCalculator;
import org.expasy.mzjava.proteomics.mol.Protein;
import org.expasy.mzjava.proteomics.mol.digest.DigestionController;
import org.expasy.mzjava.proteomics.mol.digest.LengthDigestionController;
import org.expasy.mzjava.proteomics.mol.digest.Protease;
import org.expasy.mzjava.proteomics.ms.fragment.PeptideFragmenter;
import org.expasy.mzjava.proteomics.ms.fragment.PeptideFragmenterFactory;
import org.expasy.mzjava.proteomics.ms.spectrum.PepFragAnnotation;
import org.expasy.mzjava.proteomics.ms.spectrum.PeptideSpectrum;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * PeptideSpectrumDB is a facade that collects together all the classes that are required by a peptide spectrum
 * database. The spectrum database is backed by a protein database, a PeptideFragmenter to generate theoretical
 * spectra and a PeptideSpectrumCache to allow the generated spectra to be cached.
 * <p>
 * PeptideSpectrumDB can be built by using the constructor and by using the Builder class for configuring
 * a database by calling PeptideSpectrumDB.newBuilder()
 *
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class PeptideSpectrumDB {

    private static final MassCalculator MASS_CALCULATOR = AAMassCalculator.getInstance();

    private final Tolerance precursorTolerance;
    private final PeptideFragmenter fragmenter;
    private final DigestDB digestDB;
    private final PeptideSpectrumCache peptideSpectrumCache;
    private final PeakProcessorChain<PepFragAnnotation> processorChain;

    /**
     * Construct a PeptideSpectrumDB backed by the fragmenter, proteinDB and peptideSpectrumCache.
     *
     * @param precursorTolerance the precursor tolerance
     * @param fragmenter the class that is used to generate theoretical spectra
     * @param digestDB the peptide database
     * @param peptideSpectrumCache the spectrum cache
     * @param processorChain the processor chain that is used to process the generated spectra
     */
    public PeptideSpectrumDB(Tolerance precursorTolerance, PeptideFragmenter fragmenter, DigestDB digestDB, PeptideSpectrumCache peptideSpectrumCache, PeakProcessorChain<PepFragAnnotation> processorChain) {

        this.processorChain = processorChain;

        checkNotNull(precursorTolerance);
        checkNotNull(fragmenter);
        checkNotNull(digestDB);
        checkNotNull(peptideSpectrumCache);

        this.precursorTolerance = precursorTolerance;
        this.fragmenter = fragmenter;
        this.digestDB = digestDB;
        this.peptideSpectrumCache = peptideSpectrumCache;
    }

    /**
     * Collect all peptide spectra with mass closed to the given precursor peak
     *
     * @param precursor the peak precursor query
     * @return a list of PeptideSpectrum
     */
    public List<PeptideSpectrum> getSpectra(Peak precursor) {

        checkNotNull(precursor);
        checkArgument(precursor.getMz() > 0);
        checkArgument(precursor.getChargeList().length > 0);

        List<PeptideSpectrum> collector = Lists.newArrayList();

        getSpectra(precursor, collector);

        return collector;
    }

    /**
     * Add all peptide spectra contained in this database that are within tolerance of the precursor to spectrumCollection
     *
     * @param precursor          the precursor
     * @param spectrumCollection to collection to which the spectra re to be added
     */
    public void getSpectra(Peak precursor, Collection<PeptideSpectrum> spectrumCollection) {

        for (int charge : precursor.getChargeList()) {

            final double targetMass = MASS_CALCULATOR.calculateNeutralMolecularMass(precursor.getMz(), charge);
            for (Digest digest : digestDB.getPeptides(targetMass, precursorTolerance)) {

                Optional<PeptideSpectrum> cachedPeptideSpectrum = peptideSpectrumCache.get(digest.getPeptide(), charge);

                final PeptideSpectrum peptideSpectrum;
                if (cachedPeptideSpectrum.isPresent()) {

                    peptideSpectrum = cachedPeptideSpectrum.get();
                } else {

                    peptideSpectrum = fragmenter.fragment(digest.getPeptide(), charge);
                    peptideSpectrum.addProteinAccessionNumbers(digest.getProteinIds());
                    if(!processorChain.isEmpty()){

                        peptideSpectrum.apply(processorChain);
                        peptideSpectrum.trimToSize();
                    }
                    peptideSpectrumCache.put(peptideSpectrum);
                }

                spectrumCollection.add(peptideSpectrum);
            }
        }
    }

    /**
     * Factory method for creating a builder that can be used to configure and build an in memory PeptideSpectrumDB.
     *
     * @return the new builder.
     */
    public static StartDSL newBuilder(){

        return new Builder();
    }

    /**
     * Fluent interface that starts the build.
     */
    public interface StartDSL {

        /**
         * Set the precursor tolerance
         *
         * @param precursorTolerance the precursor tolerance
         * @return the builder for configuring the protein source
         */
        DigestSourceDSL setPrecursorTolerance(Tolerance precursorTolerance);
    }

    /**
     * Fluent interface for configuring the protein source
     */
    public interface DigestSourceDSL {

        /**
         * Set the fasta format protein file. This uses a FastaStreamReader.
         *
         * @param fastaFile the file contining the protins
         * @return dsl with methods to configure digesting and fragmenting
         */
        DigestDSL setProteinSource(File fastaFile);

        /**
         * Set the IterativeReader <code>reader</code> as the protein source.
         *
         * @param reader the protein source
         * @return dsl with methods to configure digest and fragmentation
         */
        DigestDSL setProteinSource(IterativeReader<Protein> reader);

        FragmentDSL setDigestDB(DigestDB digestDB);
    }

    /**
     * Fluent interface for configuring the protein digestion and fragmentation.
     */
    public interface DigestDSL {

        /**
         * Set the Protease that is used to digest the proteins.
         *
         * @param protease the Protease
         * @return dsl with methods to configure digest and fragmentation
         */
        DigestDSL digestWith(Protease protease);

        /**
         * Set the number of missed cleavages that is allowed by the digester.
         *
         * @param missedCleavages the number of missed cleavages
         * @return dsl with methods to configure digest and fragmentation
         */
        FragmentDSL setMissedCleavagesTo(int missedCleavages);

        /**
         * Only retain peptides that have a length >= <code>min</code> and <= <code>max</code>
         *
         * @param min the minimum peptide length, inclusive
         * @param max the maximum peptide length, inclusive
         * @return dsl with methods to configure digest and fragmentation
         */
        DigestDSL retainPeptidesOfLength(int min, int max);

        /**
         * Set what happens when an ambiguous amino acid is found.
         *
         * @param ambiguousAminoAcidAction the fail behaviour
         * @return dsl with methods to configure digest and fragmentation
         */
        DigestDSL onAmbiguousAminoAcid(DigestDB.AmbiguousAminoAcidAction ambiguousAminoAcidAction);

        /**
         * Set the digestion controller that is used by the digester
         *
         * @param digestionController the digestion controller
         * @return dsl with methods to configure digest and fragmentation
         */
        DigestDSL useDigestionController(DigestionController digestionController);
    }

    /**
     * Fluent interface for configuring the protein digestion and fragmentation.
     */
    public interface FragmentDSL {

        FragmentDSL cachePeptideSpectrumWith(PeptideSpectrumCache cache);

        /**
         * Set the PeptideFragmenter that is used to generate the theoretical spectra.
         *
         * @param peptideFragmenter the fragmenter to use
         * @return dsl with method to build the PeptideSpectrumDB
         */
        EndDSL generateSpectraWith(PeptideFragmenter peptideFragmenter);

        /**
         * Creates a new PeptideFragmenter that generates theoretical spectra using the provided ions.
         *
         * @param first the first ion type
         * @param rest the rest
         * @return dsl with method to build the PeptideSpectrumDB
         */
        EndDSL generateSpectraWithIons(IonType first, IonType... rest);

        /**
         * Creates a new PeptideFragmenter that generates theoretical spectra like SEQUEST does. The generated spectra
         * have peaks from backbone and neutral loss ions. Backbone peaks are generated for b and y ions. Ammonium and
         * water loss peaks are generated for both b and y ions and carbon monoxide losses are generated for b ions.
         * <p>
         * The intensity of the backbone ions is 50 and the intensity of the neutral loss ions is set to 10
         *
         * @return dsl with method to build the PeptideSpectrumDB
         * @param precision
         */
        EndDSL generateSequestCidSpectra(PeakList.Precision precision);

        /**
         * Set the precision of the spectra and ion types that are generated when a theoretical spectrum is created.
         *
         * @param precision the spectrum precision
         * @param first the first ion type
         * @param rest the rest
         * @return dsl with method to build the PeptideSpectrumDB
         */
        EndDSL generateSpectra(PeakList.Precision precision, IonType first, IonType... rest);

        /**
         * Set the processorChain that is used to process the generated spectra
         *
         * @param processorChain the processor chain
         * @return dsl with methods to configure digest and fragmentation
         */
        FragmentDSL processGeneratedSpectraWith(PeakProcessorChain<PepFragAnnotation> processorChain);
    }

    public interface EndDSL {

        /**
         * Build the PeptideSpectrumDB
         *
         * @return the new PeptideSpectrumDB
         */
        PeptideSpectrumDB build();
    }

    private static class Builder implements StartDSL, DigestSourceDSL, DigestDSL, FragmentDSL, EndDSL {

        private Tolerance precursorTolerance;

        private IterativeReader<Protein> proteinReader;
        private Protease protease = Protease.TRYPSIN;
        private int missedCleavages = 0;
        private DigestDB.AmbiguousAminoAcidAction ambiguousAminoAcidAction = DigestDB.AmbiguousAminoAcidAction.THROW_EXCEPTION;
        private DigestionController digestionController = null;

        private DigestDB digestDB = null;

        private PeakProcessorChain<PepFragAnnotation> processorChain = new PeakProcessorChain<PepFragAnnotation>();
        private PeptideSpectrumCache cache = new SoftRefPepSpectrumCache();

        private PeptideFragmenter peptideFragmenter;

        private Builder() {
        }

        @Override
        public DigestSourceDSL setPrecursorTolerance(Tolerance precursorTolerance) {

            checkNotNull(precursorTolerance);
            this.precursorTolerance = precursorTolerance;

            return this;
        }

        @Override
        public DigestDSL setProteinSource(File fastaFile) {

            checkNotNull(fastaFile);
            try {

                proteinReader = new FastaProteinReader(fastaFile);
            } catch (FileNotFoundException e) {

                throw new IllegalStateException(e);
            }

            return this;
        }

        @Override
        public DigestDSL setProteinSource(IterativeReader<Protein> reader) {

            checkNotNull(reader);
            proteinReader = reader;

            return this;
        }

        @Override
        public DigestDSL digestWith(Protease protease) {

            checkNotNull(protease);
            this.protease = protease;

            return this;
        }

        @Override
        public FragmentDSL cachePeptideSpectrumWith(PeptideSpectrumCache cache) {

            checkNotNull(cache);
            this.cache = cache;

            return this;
        }

        @Override
        public FragmentDSL setMissedCleavagesTo(int missedCleavages) {

            checkNotNull(missedCleavages);
            this.missedCleavages = missedCleavages;

            return this;
        }

        @Override
        public FragmentDSL setDigestDB(DigestDB digestDB) {

            checkNotNull(digestDB);
            this.digestDB = digestDB;

            return this;
        }

        @Override
        public DigestDSL retainPeptidesOfLength(int min, int max) {

            checkArgument(min < max);
            digestionController = new LengthDigestionController(min, max);

            return this;
        }

        @Override
        public DigestDSL onAmbiguousAminoAcid(DigestDB.AmbiguousAminoAcidAction ambiguousAminoAcidAction) {

            checkNotNull(ambiguousAminoAcidAction);
            this.ambiguousAminoAcidAction = ambiguousAminoAcidAction;

            return this;
        }

        @Override
        public DigestDSL useDigestionController(DigestionController digestionController) {

            checkNotNull(digestionController);
            this.digestionController = digestionController;

            return this;
        }

        @Override
        public EndDSL generateSpectra(PeakList.Precision precision, IonType first, IonType... rest) {

            checkNotNull(precision);
            checkNotNull(first);
            checkNotNull(rest);
            peptideFragmenter = new PeptideFragmenter(EnumSet.of(first, rest), precision);

            return this;
        }

        @Override
        public FragmentDSL processGeneratedSpectraWith(PeakProcessorChain<PepFragAnnotation> processorChain) {

            checkNotNull(processorChain);
            this.processorChain = processorChain;

            return this;
        }

        @Override
        public EndDSL generateSpectraWith(PeptideFragmenter peptideFragmenter) {

            checkNotNull(peptideFragmenter);
            this.peptideFragmenter = peptideFragmenter;

            return this;
        }

        @Override
        public EndDSL generateSpectraWithIons(IonType first, IonType... rest) {

            checkNotNull(first);
            checkNotNull(rest);

            return generateSpectra(PeakList.Precision.FLOAT, first, rest);
        }

        @Override
        public EndDSL generateSequestCidSpectra(PeakList.Precision precision) {

            peptideFragmenter = PeptideFragmenterFactory.newSequestPeptideFragmenter(precision);

            processorChain = new PeakProcessorChain<PepFragAnnotation>(
                    new BinnedSpectrumFilter<PepFragAnnotation>(100.5, 2000.5, 1.0005, BinnedSpectrumFilter.IntensityMode.HIGHEST))
                    .add(new AddFlankingPeaksTransformer<PepFragAnnotation>(0.5, 30.0));

            return this;
        }

        @Override
        public PeptideSpectrumDB build() {

            DigestDB digestDbToUse;

            if (digestDB == null) {

                final ArrayDigestDB.Builder builder = new ArrayDigestDB.Builder(proteinReader, protease, missedCleavages);
                builder.setAmbiguousAminoAcidAction(ambiguousAminoAcidAction);
                if(digestionController != null) {
                    builder.setDigestionController(digestionController);
                }
                digestDbToUse = builder.build();
            } else {

                digestDbToUse = digestDB;
            }

            return new PeptideSpectrumDB(precursorTolerance, peptideFragmenter, digestDbToUse, cache, processorChain);
        }
    }
}