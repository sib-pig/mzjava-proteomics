package org.expasy.mzjava.proteomics.io.ms.ident;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.io.Files;
import gnu.trove.map.TObjectDoubleMap;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.proteomics.mol.AAMassCalculator;
import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.core.mol.MassCalculator;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.ms.ident.ModificationMatch;
import org.expasy.mzjava.proteomics.ms.ident.PeptideMatch;
import org.expasy.mzjava.proteomics.ms.ident.PeptideProteinMatch;
import org.expasy.mzjava.core.ms.spectrum.*;
import org.expasy.mzjava.proteomics.io.ms.ident.pepxml.AminoacidModificationHashable;
import org.expasy.mzjava.proteomics.io.ms.ident.pepxml.ModificationMap;
import org.expasy.mzjava.proteomics.io.ms.ident.pepxml.v117.EngineType;
import org.expasy.mzjava.proteomics.io.ms.ident.pepxml.v117.MassType;
import org.expasy.mzjava.proteomics.io.ms.ident.pepxml.v117.MsmsPipelineAnalysis;
import org.expasy.mzjava.proteomics.io.ms.ident.pepxml.v117.NameValueType;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.URI;
import java.util.*;
import java.util.logging.Logger;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.expasy.mzjava.proteomics.io.ms.ident.PepXmlWriterBuilder.create;

/**
 * This Writer writes peptide identifications in PepXML format.
 *
 * Use PepXmlWriterBuilder to make instances of this class,
 * add peptide identifications by calling method add(spectrum, peptideMatches),
 * and write to a OutputStream or a file.
 *
 * @author fnikitin
 * Date: 1/8/14
 */
public class PepXmlWriter {

    private static final Logger LOGGER = Logger.getLogger(PepXmlWriter.class.getName());

    private static final MassCalculator AA_MASS_CALCULATOR = AAMassCalculator.getInstance();

    public enum ConsistencyCheck {

        MSN_SPECTRUM_SCAN
    }

    private Marshaller marshaller;

    private final EngineType engineType;
    private final MsmsPipelineAnalysis.MsmsRunSummary templateMsmsRunSummary;
    private final List<NameValueType> params;
    private final MassType fragMassType;
    private final MassType precMassType;
    private final File sequenceDbFile;
    private final MsmsPipelineAnalysis.MsmsRunSummary.SearchSummary.EnzymaticSearchConstraint enzymaticSearchConstraint;
    private final Comparator<PeptideMatch> peptideMatchComparator;
    private final int searchHitRank;

    private final Map<URI, MsmsPipelineAnalysis.MsmsRunSummary> msmsRunSummaryMap;
    private final Map<URI, ModificationMap> modificationMapMap;
    private final Set<ConsistencyCheck> consistencyChecks;

    protected PepXmlWriter(PepXmlWriterBuilder.Builder builder) {

        templateMsmsRunSummary = builder.msmsRunSummary;
        templateMsmsRunSummary.setSampleEnzyme(builder.enzyme);
        enzymaticSearchConstraint = builder.constraint;
        engineType = builder.engineType;
        fragMassType = builder.fragmentMassType;
        precMassType = builder.precMassType;
        sequenceDbFile = builder.protDbFile;
        params = builder.params;
        peptideMatchComparator = builder.peptideMatchComparator;
        searchHitRank = builder.searchHitRank;

        msmsRunSummaryMap = Maps.newHashMap();
        modificationMapMap = Maps.newHashMap();
        consistencyChecks = EnumSet.allOf(ConsistencyCheck.class);
    }

    /**
     * Create an instance of PepXmlWriter that strictly checks for inconsistencies
     *
     * @param engineType  the engine type was used for MS identification
     * @param peptideMatchComparator the peptide match comparator
     *
     * @return PepXmlWriter object
     */
    public static PepXmlWriter newStrictWriter(EngineType engineType, Comparator<PeptideMatch> peptideMatchComparator) {

        return create(engineType, peptideMatchComparator).build();
    }

    /**
     * Create an instance of PepXmlWriter that strictly checks for inconsistencies
     *
     * @param engineType  the engine type was used for MS identification
     * @param scoreName the score name used to compare peptide matches
     *
     * @return PepXmlWriter object
     */
    public static PepXmlWriter newStrictWriter(EngineType engineType, String scoreName) {

        return create(engineType, scoreName).build();
    }

    /**
     * Create an instance of PepXmlWriter that does not check for inconsistencies
     *
     * @param engineType  the engine type was used for MS identification
     * @param peptideMatchComparator the peptide match comparator
     *
     * @return PepXmlWriter object
     */
    public static PepXmlWriter newTolerantWriter(EngineType engineType, Comparator<PeptideMatch> peptideMatchComparator) {

        PepXmlWriter writer = newStrictWriter(engineType, peptideMatchComparator);

        writer.removeConsistencyChecks(EnumSet.allOf(ConsistencyCheck.class));

        return writer;
    }

    /**
     * Create an instance of PepXmlWriter that does not check for inconsistencies
     *
     * @param engineType  the engine type was used for MS identification
     * @param scoreName the score name used to compare peptide matches
     *
     * @return PepXmlWriter object
     */
    public static PepXmlWriter newTolerantWriter(EngineType engineType, String scoreName) {

        PepXmlWriter writer = newStrictWriter(engineType, scoreName);

        writer.removeConsistencyChecks(EnumSet.allOf(ConsistencyCheck.class));

        return writer;
    }

    public void removeConsistencyChecks(Set<ConsistencyCheck> ccs) {

        checkNotNull(ccs);
        consistencyChecks.removeAll(ccs);
    }

    private void initMarshaller(OutputStream os) throws JAXBException {

        Preconditions.checkNotNull(os);

        JAXBContext jc = JAXBContext.newInstance(MsmsPipelineAnalysis.class.getPackage().getName());

        marshaller = jc.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
    }

    /**
     * Convert MsnSpectrum to PepXml element SpectrumQuery
     *
     * @param spectrum the MsnSpectrum to convert
     *
     * @return SpectrumQuery
     * @throws IllegalStateException if spectrum does not contain scan number and
     *   this writer expect have one (see removeConsistencyChecks() to disable the check)
     */
    protected MsmsPipelineAnalysis.MsmsRunSummary.SpectrumQuery toSpectrumQuery(MsnSpectrum spectrum) {

        Preconditions.checkNotNull(spectrum);

        MsmsPipelineAnalysis.MsmsRunSummary.SpectrumQuery sq = new MsmsPipelineAnalysis.MsmsRunSummary.SpectrumQuery();

        if (spectrum.getScanNumbers().isEmpty()) {

            String message = "spectrum "+spectrum.getSpectrumSource().getRawPath()+"."+spectrum.getSpectrumIndex()+" has no scan number";

            if (!consistencyChecks.contains(ConsistencyCheck.MSN_SPECTRUM_SCAN)) {

                sq.setStartScan(spectrum.getSpectrumIndex());
                sq.setEndScan(spectrum.getSpectrumIndex());

                LOGGER.warning(message+" - start_scan and end_scan assigned with spectrum index");
            } else {

                throw new IllegalStateException(message);
            }
        } else {

            ScanNumberList scanNumbers = spectrum.getScanNumbers();

            sq.setStartScan(scanNumbers.getFirst().getMinScanNumber());
            sq.setEndScan(scanNumbers.getLast().getMaxScanNumber());
        }

        Peak precursor = spectrum.getPrecursor();

        sq.setSpectrum(buildSpectrumQuerySpectrumAttribute(spectrum));
        sq.setPrecursorNeutralMass((float) AA_MASS_CALCULATOR.calculateNeutralMolecularMass(precursor));
        sq.setAssumedCharge(BigInteger.valueOf(spectrum.getPrecursor().getCharge()));
        sq.setIndex(spectrum.getSpectrumIndex());
        if (!spectrum.getRetentionTimes().isEmpty())
            sq.setRetentionTimeSec((float)spectrum.getRetentionTimes().getFirst().getTime());
        if (precursor.getIntensity()>0)
            sq.setPrecursorIntensity((float)precursor.getIntensity());

        return sq;
    }

    /**
     * Format MsnSpectrum id for element spectrum attribute of spectrum_query element.
     * The default format is defined in PepXmlWriter.toDefaultSpectrumQuerySpectrumAttribute() that this method is calling
     *
     * Needs to be overriden to change format.
     */
    protected String buildSpectrumQuerySpectrumAttribute(MsnSpectrum spectrum) {

        return toDefaultSpectrumQuerySpectrumAttribute(spectrum);
    }

    /**
     * Create the following spectrum format 'basename.scanNumberStart.scanNumberEnd.charge' or
     * 'basename.scanNumberIndex.scanNumberIndex.charge' if no scan numbers
     *
     * @param spectrum the spectrum to create the id from
     *
     * @return the spectrum attribute value
     */
    public static String toDefaultSpectrumQuerySpectrumAttribute(MsnSpectrum spectrum) {

        Preconditions.checkNotNull(spectrum);

        StringBuilder sb = new StringBuilder();

        File source = new File(spectrum.getSpectrumSource());

        sb.append(Files.getNameWithoutExtension(source.getName()));
        sb.append(".");
        if (spectrum.getScanNumbers().isEmpty()) {

            sb.append(spectrum.getSpectrumIndex());
            sb.append(".");
            sb.append(spectrum.getSpectrumIndex());

        } else {

            sb.append(spectrum.getScanNumbers().getFirst().getValue());
            sb.append(".");
            sb.append(spectrum.getScanNumbers().getLast().getValue());
        }
        sb.append(".");
        sb.append(spectrum.getPrecursor().getCharge());

        return sb.toString();
    }

    /**
     * Convert PeptideMatch to PepXml element SearchHit
     *
     * @param peptideMatch the PeptideMatch to convert
     *
     * @return SearchHit
     */
    MsmsPipelineAnalysis.MsmsRunSummary.SpectrumQuery.SearchResult.SearchHit toSearchHit(PeptideMatch peptideMatch) {

        Preconditions.checkNotNull(peptideMatch);

        MsmsPipelineAnalysis.MsmsRunSummary.SpectrumQuery.SearchResult.SearchHit searchHit =
                new MsmsPipelineAnalysis.MsmsRunSummary.SpectrumQuery.SearchResult.SearchHit();

        Peptide peptide = peptideMatch.toPeptide();

        searchHit.setPeptide(peptide.toSymbolString());
        searchHit.setCalcNeutralPepMass((float) peptide.getMolecularMass());
        searchHit.setHitRank(peptideMatch.getRank().get());
        searchHit.setNumMatchedIons(BigInteger.valueOf(peptideMatch.getNumMatchedIons().get()));
        searchHit.setTotNumIons(BigInteger.valueOf(peptideMatch.getTotalNumIons().get()));
        searchHit.setMassdiff(String.valueOf(peptideMatch.getMassDiff().get()));
        searchHit.setNumMissedCleavages(BigInteger.valueOf(peptideMatch.getNumMissedCleavages().get()));
        searchHit.setIsRejected( (peptideMatch.isRejected().get()) ? BigInteger.ONE : BigInteger.ZERO);

        List<PeptideProteinMatch> proteins = peptideMatch.getProteinMatches();

        if (!proteins.isEmpty()) {

            searchHit.setProtein(proteins.get(0).getAccession());
            PeptideProteinMatch proteinMatch = peptideMatch.getProteinMatches().get(0);
            Optional<String> previousAA = proteinMatch.getPreviousAA();
            if (previousAA.isPresent() && !previousAA.get().isEmpty())
                searchHit.setPeptidePrevAa(previousAA.get());
            Optional<String> nextAA = proteinMatch.getNextAA();
            if (nextAA.isPresent() && !nextAA.get().isEmpty())
                searchHit.setPeptideNextAa(nextAA.get());
        }

        if (proteins.size()>1) {

            for (int i=1 ; i<proteins.size() ; i++) {

               MsmsPipelineAnalysis.MsmsRunSummary.SpectrumQuery.SearchResult.SearchHit.AlternativeProtein alternativeProtein =
                       new MsmsPipelineAnalysis.MsmsRunSummary.SpectrumQuery.SearchResult.SearchHit.AlternativeProtein();
               alternativeProtein.setProtein(proteins.get(i).getAccession());

               searchHit.getAlternativeProtein().add(alternativeProtein);
            }
        }
        searchHit.setNumTotProteins(proteins.size());

        // handle modifications
        if (peptideMatch.getModificationCount()>0) {

            MsmsPipelineAnalysis.MsmsRunSummary.SpectrumQuery.SearchResult.SearchHit.ModificationInfo modificationInfo =
                    new MsmsPipelineAnalysis.MsmsRunSummary.SpectrumQuery.SearchResult.SearchHit.ModificationInfo();

            modificationInfo.getModAminoacidMass().addAll(fetchModAminoacidMassList(peptideMatch));

            if (!peptideMatch.getModifications(ModAttachment.nTermSet).isEmpty())
                modificationInfo.setModNtermMass(fetchModNtermMass(peptideMatch));

            if (!peptideMatch.getModifications(ModAttachment.cTermSet).isEmpty())
                modificationInfo.setModCtermMass(fetchModCtermMass(peptideMatch));

            searchHit.setModificationInfo(modificationInfo);
        }

        TObjectDoubleMap<String> scoreMap = peptideMatch.getScoreMap();

        for (String name : scoreMap.keySet()) {

            NameValueType score = new NameValueType();

            score.setName(name);
            score.setValueAttribute(String.valueOf((float)scoreMap.get(name)));

            searchHit.getSearchScore().add(score);
        }

        return searchHit;
    }

    /**
     * Fetch ModAminoacidMass list from PeptideMatch
     * @param peptideMatch the peptide match to get mods from
     * @return list of ModAminoacidMass
     */
    private List<MsmsPipelineAnalysis.MsmsRunSummary.SpectrumQuery.SearchResult.SearchHit.ModificationInfo.ModAminoacidMass> fetchModAminoacidMassList(PeptideMatch peptideMatch) {

        List<MsmsPipelineAnalysis.MsmsRunSummary.SpectrumQuery.SearchResult.SearchHit.ModificationInfo.ModAminoacidMass> aminoacidModifications =
                Lists.newArrayList();

        for (int i=0 ; i<peptideMatch.size() ; i++) {

            List<ModificationMatch> internalModMatchs = peptideMatch.getModifications(i, ModAttachment.sideChainSet);

            AminoAcid aa = peptideMatch.getSymbol(i);

            for (ModificationMatch internalModMatch : internalModMatchs) {

                float totalMass = (float) (aa.getMassOfMonomer() + internalModMatch.getMassShift());

                aminoacidModifications.add(toModAaMass(totalMass, i));
            }
        }

        return aminoacidModifications;
    }

    /**
     * Fetch N terminal mass shift from PeptideMatch
     * @param peptideMatch the peptide match to get mass shift from
     * @return mass shift
     */
    private double fetchModNtermMass(PeptideMatch peptideMatch) {

        Preconditions.checkNotNull(peptideMatch);

        double mass = 0;

        for (ModificationMatch ntermMod : peptideMatch.getModifications(ModAttachment.nTermSet)) {

            mass += ntermMod.getMassShift();
        }

        return mass;
    }

    /**
     * Fetch C terminal mass shift from PeptideMatch
     * @param peptideMatch the peptide match to get mass shift from
     * @return mass shift
     */
    private double fetchModCtermMass(PeptideMatch peptideMatch) {

        Preconditions.checkNotNull(peptideMatch);

        double mass = 0;

        for (ModificationMatch ctermMod : peptideMatch.getModifications(ModAttachment.cTermSet)) {

            mass += ctermMod.getMassShift();
        }

        return mass;
    }

    /**
     * Convert totalMass float to ModAminoacidMass
     * @param totalMass the total mass
     * @param index the aa index
     * @return ModAminoacidMass
     */
    MsmsPipelineAnalysis.MsmsRunSummary.SpectrumQuery.SearchResult.SearchHit.ModificationInfo.ModAminoacidMass toModAaMass(float totalMass, int index) {

        MsmsPipelineAnalysis.MsmsRunSummary.SpectrumQuery.SearchResult.SearchHit.ModificationInfo.ModAminoacidMass modAminoacidMass =
                 new MsmsPipelineAnalysis.MsmsRunSummary.SpectrumQuery.SearchResult.SearchHit.ModificationInfo.ModAminoacidMass();

        // total mass
        modAminoacidMass.setMass(totalMass);
        modAminoacidMass.setPosition(BigInteger.valueOf(index + 1));

        return modAminoacidMass;
    }

    MsmsPipelineAnalysis.MsmsRunSummary.SearchSummary.AminoacidModification toAminoacidModification(AminoAcid aa, MsmsPipelineAnalysis.MsmsRunSummary.SpectrumQuery.SearchResult.SearchHit.ModificationInfo.ModAminoacidMass modAminoacidMass) {

        Preconditions.checkNotNull(modAminoacidMass);

        MsmsPipelineAnalysis.MsmsRunSummary.SearchSummary.AminoacidModification aminoacidModification =
                new MsmsPipelineAnalysis.MsmsRunSummary.SearchSummary.AminoacidModification();

        float massShift = (float) (modAminoacidMass.getMass() - aa.getMassOfMonomer());

        aminoacidModification.setAminoacid(aa.getSymbol());
        aminoacidModification.setMassdiff(String.valueOf(massShift));
        aminoacidModification.setMass((float) modAminoacidMass.getMass());
        // set a default value to this required attribute
        aminoacidModification.setVariable("N");

        return aminoacidModification;
    }

    private MsmsPipelineAnalysis.MsmsRunSummary.SearchSummary.TerminalModification toTerminalModification(float terminalMass, AminoAcid aa, boolean isNTerm) {

        Preconditions.checkNotNull(aa);

        MsmsPipelineAnalysis.MsmsRunSummary.SearchSummary.TerminalModification terminalModification =
                new MsmsPipelineAnalysis.MsmsRunSummary.SearchSummary.TerminalModification();

        float massShift = (float) (terminalMass - aa.getMassOfMonomer());

        if (isNTerm)
            terminalModification.setTerminus("n");
        else
            terminalModification.setTerminus("c");
        terminalModification.setMassdiff(String.valueOf(massShift));
        terminalModification.setMass(terminalMass);
        // set a default value to this required attribute
        terminalModification.setVariable("N");

        return terminalModification;
    }

    /**
     * Get or create new MsmsRunSummary instance based on Ms data source URI
     *
     * @param source the Ms data source URI
     *
     * @return an instance of MsmsRunSummary
     */
    private MsmsPipelineAnalysis.MsmsRunSummary getOrNewMsmsRunSummary(URI source) {

        if (!msmsRunSummaryMap.containsKey(source)) {

            // create a new msms_run_summary
            MsmsPipelineAnalysis.MsmsRunSummary mmrs = PepXmlWriterBuilder.newMsmsRunSummary(new File(source), templateMsmsRunSummary);

            // new search summary
            mmrs.getSearchSummary().add(newSearchSummary(mmrs.getBaseName()));

            msmsRunSummaryMap.put(source, mmrs);
        }

        return msmsRunSummaryMap.get(source);
    }

    private MsmsPipelineAnalysis.MsmsRunSummary.SearchSummary newSearchSummary(String basename) {

        Preconditions.checkNotNull(basename);

        MsmsPipelineAnalysis.MsmsRunSummary.SearchSummary searchSummary = new MsmsPipelineAnalysis.MsmsRunSummary.SearchSummary();

        searchSummary.setSearchEngine(engineType);
        // set default values
        searchSummary.setPrecursorMassType(precMassType);
        searchSummary.setFragmentMassType(fragMassType);

        if (sequenceDbFile != null) {

            MsmsPipelineAnalysis.MsmsRunSummary.SearchSummary.SearchDatabase db = new MsmsPipelineAnalysis.MsmsRunSummary.SearchSummary.SearchDatabase();

            db.setLocalPath(sequenceDbFile.getAbsolutePath());
            db.setType("AA");

            searchSummary.setSearchDatabase(db);
        }

        if (enzymaticSearchConstraint != null)
            searchSummary.setEnzymaticSearchConstraint(enzymaticSearchConstraint);

        searchSummary.setBaseName(basename);

        searchSummary.getParameter().addAll(params);

        searchSummary.setSearchId(1);

        return searchSummary;
    }

    /**
     * Update the internal Map of ModificationMap here from potential mods found in SearchHit
     * @param source the MS data source URI
     * @param searchHit the SearchHit to update mods from
     */
    private void updateAminoacidModificationMap(URI source, MsmsPipelineAnalysis.MsmsRunSummary.SpectrumQuery.SearchResult.SearchHit searchHit) {

        if (!modificationMapMap.containsKey(source))
            modificationMapMap.put(source, new ModificationMap());

        ModificationMap modificationMap = modificationMapMap.get(source);

        MsmsPipelineAnalysis.MsmsRunSummary.SpectrumQuery.SearchResult.SearchHit.ModificationInfo modInfo =
                searchHit.getModificationInfo();

        String peptide = searchHit.getPeptide();

        if (modInfo != null) {

            for (MsmsPipelineAnalysis.MsmsRunSummary.SpectrumQuery.SearchResult.SearchHit.ModificationInfo.ModAminoacidMass maam : modInfo.getModAminoacidMass()) {

                modificationMap.add(toAminoacidModification(AminoAcid.valueOf(peptide.charAt(maam.getPosition().intValue()-1)), maam));
            }

            if (modInfo.getModNtermMass() != null)
                modificationMap.setTerminal(toTerminalModification(modInfo.getModNtermMass().floatValue(), AminoAcid.valueOf(peptide.charAt(0)), true));

            if (modInfo.getModCtermMass() != null)
                modificationMap.setTerminal(toTerminalModification(modInfo.getModCtermMass().floatValue(), AminoAcid.valueOf(peptide.charAt(peptide.length()-1)), false));
        }
    }

    /**
     * Collect the list of peptide identifications for the given spectrum
     * @param spectrum the spectrum
     * @param peptideMatchList the list of peptide identifications
     */
    public void add(MsnSpectrum spectrum, List<PeptideMatch> peptideMatchList) {

        Preconditions.checkNotNull(spectrum);
        Preconditions.checkNotNull(peptideMatchList);

        if (peptideMatchList.isEmpty())
            return;

        // get the correct msms_run_summary
        MsmsPipelineAnalysis.MsmsRunSummary msmsRunSummary = getOrNewMsmsRunSummary(spectrum.getSpectrumSource());

        // new spectrum_query
        MsmsPipelineAnalysis.MsmsRunSummary.SpectrumQuery spectrumQuery = toSpectrumQuery(spectrum);

        MsmsPipelineAnalysis.MsmsRunSummary.SpectrumQuery.SearchResult searchResult =
                new MsmsPipelineAnalysis.MsmsRunSummary.SpectrumQuery.SearchResult();

        spectrumQuery.getSearchResult().add(searchResult);

        Collections.sort(peptideMatchList, peptideMatchComparator);

        int size = Math.min(peptideMatchList.size(), searchHitRank);

        for (int i=0 ; i<size ; i++) {

            MsmsPipelineAnalysis.MsmsRunSummary.SpectrumQuery.SearchResult.SearchHit searchHit =
                    toSearchHit(peptideMatchList.get(i));

            searchHit.setHitRank(i+1);

            searchResult.getSearchHit().add(searchHit);

            updateAminoacidModificationMap(spectrum.getSpectrumSource(), searchHit);
        }

        msmsRunSummary.getSpectrumQuery().add(spectrumQuery);
    }

    /**
     * Collect the list of peptide identifications for the given spectrum
     * @param spectrum the spectrum
     * @param peptideMatch the peptide identification(s)
     */
    public void add(MsnSpectrum spectrum, PeptideMatch... peptideMatch) {

        Preconditions.checkNotNull(peptideMatch);

        add(spectrum, Lists.newArrayList(peptideMatch));
    }

    /**
     * Write the PepXml after peptide identifications been collected (see add(spectrum, peptideMatches))
     * @param os the output stream to flush PepXml into
     * @param pepXmlURI the PepXml URI
     * @throws IOException
     */
    public void write(OutputStream os, URI pepXmlURI) throws IOException {

        try {
            initMarshaller(os);
        } catch (JAXBException e) {

            throw new IOException(e);
        }

        MsmsPipelineAnalysis mpa = newMsmsPipelineAnalysis(pepXmlURI);

        try {

            marshaller.marshal(mpa, os);
        } catch (JAXBException e) {

            throw new IOException(e);
        }
    }

    /**
     * Write the PepXml after peptide identifications been collected (see add(spectrum, peptideMatches))
     * @param pepXmlFile the output file to flush PepXml into
     * @throws IOException
     */
    public void write(File pepXmlFile) throws IOException {

        write(new FileOutputStream(pepXmlFile), pepXmlFile.toURI());
    }

    private MsmsPipelineAnalysis newMsmsPipelineAnalysis(URI pepXmlURI) {

        Preconditions.checkNotNull(pepXmlURI);

        MsmsPipelineAnalysis pipelineAnalysis = new MsmsPipelineAnalysis();

        pipelineAnalysis.setSummaryXml(pepXmlURI.getPath());

        try {

            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(new Date());

            XMLGregorianCalendar date = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
            pipelineAnalysis.setDate(date);
        } catch (DatatypeConfigurationException e) {

            throw new IllegalStateException("invalid date cannot build MsmsPipelineAnalysis", e);
        }

        updateSearchSummary(pipelineAnalysis);

        return pipelineAnalysis;
    }

    private void updateSearchSummary(MsmsPipelineAnalysis mpa) {

        for (URI source : msmsRunSummaryMap.keySet()) {

            MsmsPipelineAnalysis.MsmsRunSummary msmsRunSummary = msmsRunSummaryMap.get(source);

            MsmsPipelineAnalysis.MsmsRunSummary.SearchSummary searchSummary = msmsRunSummary.getSearchSummary().get(0);

            searchSummary.getAminoacidModification().clear();
            searchSummary.getTerminalModification().clear();

            ModificationMap modificationMap = modificationMapMap.get(source);

            Map<AminoAcid, Set<AminoacidModificationHashable>> map = modificationMap.getAminoacidModificationMap();

            for (Set<AminoacidModificationHashable> set : map.values()) {

                for (AminoacidModificationHashable aamh : set) {

                    searchSummary.getAminoacidModification().add(aamh);
                }
            }

            Optional<MsmsPipelineAnalysis.MsmsRunSummary.SearchSummary.TerminalModification> ntermMod = modificationMap.getNterminalModification();

            if (ntermMod.isPresent())
                searchSummary.getTerminalModification().add(ntermMod.get());

            Optional<MsmsPipelineAnalysis.MsmsRunSummary.SearchSummary.TerminalModification> ctermMod = modificationMap.getCterminalModification();

            if (ctermMod.isPresent())
                searchSummary.getTerminalModification().add(ctermMod.get());
        }

        mpa.getMsmsRunSummary().addAll(msmsRunSummaryMap.values());
    }
}
