/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.io.ms.spectrum.sptxt;

import com.google.common.base.Optional;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import gnu.trove.list.array.TDoubleArrayList;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.proteomics.mol.modification.ModificationResolver;
import org.expasy.mzjava.proteomics.mol.PeptideBuilder;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.UnsortedPeakListException;

import java.util.*;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class LibrarySpectrumBuilder {

    private double precursorMz;
    private int precursorCharge;
    private String peptideSequence;
    private final List<String> nTermModList = new ArrayList<>();
    private final List<String> cTermModList = new ArrayList<>();
    private final Multimap<Integer, String> sideChainModMap = ArrayListMultimap.create();
    private final PeptideBuilder peptideBuilder = new PeptideBuilder();
    private String comment = "";
    private PeptideConsensusSpectrum.Status status = PeptideConsensusSpectrum.Status.UNKNOWN;
    private final TDoubleArrayList mzList = new TDoubleArrayList();
    private final TDoubleArrayList intensityList = new TDoubleArrayList();
    private final Set<String> proteinAccessionNumbers = new HashSet<>();
    private final List<String> annotationList = new ArrayList<>();

    public void clear() {

        peptideSequence = null;
        nTermModList.clear();
        cTermModList.clear();
        sideChainModMap.clear();
        precursorMz = 0;
        precursorCharge = 0;
        comment = "";
        status = PeptideConsensusSpectrum.Status.UNKNOWN;
        mzList.resetQuick();
        intensityList.resetQuick();
        proteinAccessionNumbers.clear();
        annotationList.clear();
    }

    public void addProteinAccessionNumber(String accessionNumber){

        proteinAccessionNumbers.add(accessionNumber);
    }

    public void setPeptideSequence(String peptideSequence) {

        if(this.peptideSequence != null && !this.peptideSequence.equals(peptideSequence))
            throw new IllegalStateException("Peptide sequence miss match. First sequence read was " + this.peptideSequence + " new sequence read is " + peptideSequence);

        this.peptideSequence = peptideSequence;
    }

    public void addMod(int index, String mod) {

        sideChainModMap.put(index, mod);
    }

    public void addMod(ModAttachment modAttachment, String mod) {

        switch (modAttachment) {

            case N_TERM:
                nTermModList.add(mod);
                break;
            case C_TERM:
                cTermModList.add(mod);
                break;
            default:
                throw new IllegalStateException("Cannot add modification attached to " + modAttachment);
        }
    }

    public void setStatus(PeptideConsensusSpectrum.Status status) {

        this.status = status;
    }

    public void setPrecursorMz(double precursorMz) {

        this.precursorMz = precursorMz;
    }

    public void setPrecursorCharge(int precursorCharge) {

        this.precursorCharge = precursorCharge;
    }

    private void addMods(PeptideBuilder builder, ModAttachment modAttachment, List<String> modList, ModificationResolver modResolver) {

        for(String mod : modList) {

            builder.addModification(modAttachment, resolveOrFail(mod, modResolver));
        }
    }

    private Modification resolveOrFail(String mod, ModificationResolver modResolver) {

        Optional<Modification> opt = modResolver.resolve(mod);
        Modification modification;
        if (opt.isPresent()) {

            modification = opt.get();
        } else {

            throw new IllegalStateException("Could not resolve the modification " + mod);
        }
        return modification;
    }

    public void ensureCapacity(int capacity) {

        mzList.ensureCapacity(capacity);
        intensityList.ensureCapacity(capacity);
    }

    public void addPeak(double mz, double intensity, String annotations) {

        mzList.add(mz);
        intensityList.add(intensity);
        annotationList.add(annotations);
    }

    public void setComment(String comment) {

        this.comment = comment;
    }

    public PeptideConsensusSpectrum build(PeakList.Precision precision, ModificationResolver modificationResolver, AnnotationResolver annotationResolver, boolean acceptUnsortedSpectra){

        peptideBuilder.clear();
        peptideBuilder.parseAndAdd(peptideSequence);
        addMods(peptideBuilder, ModAttachment.N_TERM, nTermModList, modificationResolver);
        for(Integer index : sideChainModMap.keySet()) {

            for(String mod : sideChainModMap.get(index)) {

                peptideBuilder.addModification(index, resolveOrFail(mod, modificationResolver));
            }
        }
        addMods(peptideBuilder, ModAttachment.C_TERM, cTermModList, modificationResolver);

        PeptideConsensusSpectrum spectrum = new PeptideConsensusSpectrum(peptideBuilder.build(), precision,new HashSet<UUID>());
        spectrum.setComment(comment);
        spectrum.setStatus(status);
        spectrum.getPrecursor().setMzAndCharge(precursorMz, precursorCharge);
        spectrum.setMsLevel(2);

        spectrum.addProteinAccessionNumbers(proteinAccessionNumbers);

        Peptide peptide = spectrum.getPeptide();
        double lastMz = -1;
        for(int i = 0; i < mzList.size(); i++) {

            double mz = mzList.get(i);
            if(!acceptUnsortedSpectra && mz < lastMz)
                throw new UnsortedPeakListException("Cannot read unsorted spectrum!", spectrum.size() - 1);

            String annotations = annotationList.get(i);
            if(Character.isWhitespace(annotations.charAt(annotations.length() - 1)))
                annotations = annotations.trim();

            spectrum.add(mz, intensityList.get(i), annotationResolver.resolveAnnotations(annotations, peptide));
            lastMz = mz;
        }

        return spectrum;
    }

}
