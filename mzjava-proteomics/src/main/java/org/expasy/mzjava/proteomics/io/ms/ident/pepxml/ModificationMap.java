package org.expasy.mzjava.proteomics.io.ms.ident.pepxml;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.proteomics.io.ms.ident.pepxml.v117.MsmsPipelineAnalysis;

import java.io.File;
import java.net.URI;
import java.util.*;

/**
 * @author fnikitin
 * Date: 2/16/14
 */
public class ModificationMap {

    private Map<AminoAcid, Set<AminoacidModificationHashable>> aminoAcidMultimapMap;
    private Optional<MsmsPipelineAnalysis.MsmsRunSummary.SearchSummary.TerminalModification> nterminalModification;
    private Optional<MsmsPipelineAnalysis.MsmsRunSummary.SearchSummary.TerminalModification> cterminalModification;

    public ModificationMap() {

        aminoAcidMultimapMap = Maps.newHashMap();
        nterminalModification = Optional.absent();
        cterminalModification = Optional.absent();
    }

    public void add(MsmsPipelineAnalysis.MsmsRunSummary.SearchSummary.AminoacidModification aminoacidModification) {

        Preconditions.checkNotNull(aminoacidModification);

        AminoAcid aa = AminoAcid.valueOf(aminoacidModification.getAminoacid());

        if (!aminoAcidMultimapMap.containsKey(aa))
            aminoAcidMultimapMap.put(aa, Sets.<AminoacidModificationHashable>newHashSet());

        aminoAcidMultimapMap.get(aa).add(new AminoacidModificationHashable(aminoacidModification));
    }

    public void setTerminal(MsmsPipelineAnalysis.MsmsRunSummary.SearchSummary.TerminalModification terminalModification) {

        Preconditions.checkNotNull(terminalModification);

        if ("n".equals(terminalModification.getTerminus()))
            nterminalModification = Optional.of(terminalModification);

        if ("c".equals(terminalModification.getTerminus()))
            cterminalModification = Optional.of(terminalModification);
    }

    public Optional<MsmsPipelineAnalysis.MsmsRunSummary.SearchSummary.TerminalModification> getNterminalModification() {
        return nterminalModification;
    }

    public Optional<MsmsPipelineAnalysis.MsmsRunSummary.SearchSummary.TerminalModification> getCterminalModification() {
        return cterminalModification;
    }

    public Map<AminoAcid, Set<AminoacidModificationHashable>> getAminoacidModificationMap() {

        return aminoAcidMultimapMap;
    }

    public Set<AminoacidModificationHashable> getAminoacidModificationSet(AminoAcid aa) {

        Preconditions.checkNotNull(aa);

        if (aminoAcidMultimapMap.containsKey(aa))
            return Collections.unmodifiableSet(aminoAcidMultimapMap.get(aa));

        return Sets.newHashSet();
    }

    public static void loadModifs(Collection<MsmsPipelineAnalysis.MsmsRunSummary> runSummaryCollection, Map<URI, ModificationMap> modificationMapMap) {

        Preconditions.checkNotNull(runSummaryCollection);
        Preconditions.checkNotNull(modificationMapMap);

        for (MsmsPipelineAnalysis.MsmsRunSummary runSummary : runSummaryCollection) {

            List<MsmsPipelineAnalysis.MsmsRunSummary.SearchSummary.AminoacidModification> aamList
                    = runSummary.getSearchSummary().get(0).getAminoacidModification();

            List<MsmsPipelineAnalysis.MsmsRunSummary.SearchSummary.TerminalModification> tmList = runSummary.getSearchSummary().get(0).getTerminalModification();

            if (aamList.size() + tmList.size()>0) {

                File msDataFile = new File(runSummary.getBaseName());

                ModificationMap modificationMap = new ModificationMap();

                for (MsmsPipelineAnalysis.MsmsRunSummary.SearchSummary.AminoacidModification aam : aamList)
                    modificationMap.add(aam);

                for (MsmsPipelineAnalysis.MsmsRunSummary.SearchSummary.TerminalModification tm : tmList)
                    modificationMap.setTerminal(tm);

                modificationMapMap.put(msDataFile.toURI(), modificationMap);
            }
        }
    }
}
