package org.expasy.mzjava.proteomics.mol.digest;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import gnu.trove.list.TIntList;
import org.expasy.mzjava.core.mol.SymbolSequence;
import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.proteomics.mol.modification.Modification;

/**
 * http://web.expasy.org/peptide_cutter/peptidecutter_enzymes.html
 * <p>
 * The bottom of the page has a table of the rules.
 *
 * @author fnikitin
 * @author Oliver Horlacher
 */
public enum Protease implements CleavageSiteFinder {

    // "Trypsin predominantly cleaves proteins at the carboxyl side (or "C-terminal side") of the amino acids lysine
    // and arginine except when either is bound to a C-terminal proline., although large-scale mass spectrometry data
    // suggest cleavage occurs even with proline"
    // [Rodriguez J, Gupta N, Smith RD, Pevzner PA (2008). "Does trypsin cut before proline?". J. Proteome Res. 7 (1): 300–305. doi:10.1021/pr0705035. PMID 18067249]
    TRYPSIN (new CleavageSiteFinderImpl(new CleavageSiteMatcher("[KR]|[^P]"))),
    CASPASE_1(new CleavageSiteFinderImpl(new CleavageSiteMatcher("[FWYL]X[HAT]D|[^PEDQKR]"))),
    CASPASE_2(new CleavageSiteFinderImpl(new CleavageSiteMatcher("DVAD|[^PEDQKR]"))),
    CASPASE_3(new CleavageSiteFinderImpl(new CleavageSiteMatcher("DMQD|[^PEDQKR]"))),
    CASPASE_4(new CleavageSiteFinderImpl(new CleavageSiteMatcher("LEVD|[^PEDQKR]"))),
    CASPASE_5(new CleavageSiteFinderImpl(new CleavageSiteMatcher("[LW]EHD|X"))),
    CASPASE_6(new CleavageSiteFinderImpl(new CleavageSiteMatcher("VE[HI]D|[^PEDQKR]"))),
    CASPASE_7(new CleavageSiteFinderImpl(new CleavageSiteMatcher("DEVD|[^PEDQKR]"))),
    CASPASE_8(new CleavageSiteFinderImpl(new CleavageSiteMatcher("[IL]ETD|[^PEDQKR]"))),
    CASPASE_9(new CleavageSiteFinderImpl(new CleavageSiteMatcher("LEHD|X"))),
    CASPASE_10(new CleavageSiteFinderImpl(new CleavageSiteMatcher("IEAD|X"))),
    CHYMOTRYPSIN_FYLW(new CleavageSiteFinderImpl(new CleavageSiteMatcher("[FYLW]|[^P]"))),
    CHYMOTRYPSIN_FYL(new CleavageSiteFinderImpl(new CleavageSiteMatcher("[FYL]|[^P]"))),
    CHYMOTRYPSIN_HIGH_SPEC(new CleavageSiteFinderImpl(new CleavageSiteMatcher("[FY]|[^P] or W|[^MP]"))),
    CHYMOTRYPSIN_LOW_SPEC(new CleavageSiteFinderImpl(new CleavageSiteMatcher("[FLY]|[^P] or W|[^MP] or M|[^PY] or H|[^DMPW]"))),
    ENTEROKINASE(new CleavageSiteFinderImpl(new CleavageSiteMatcher("[DE][DE][DE]K|X"))),
    LYS_C(new CleavageSiteFinderImpl(new CleavageSiteMatcher("K|X"))),
    ARG_C(new CleavageSiteFinderImpl(new CleavageSiteMatcher("R|X"))),
    CNBR(new CleavageSiteFinderImpl(new CleavageSiteMatcher("M|X", Optional.<Modification>absent(), Optional.of(Modification.parseModification("C-1H-4S-1(+)"))))),
    ASP_N(new CleavageSiteFinderImpl(new CleavageSiteMatcher("X|D"))),
    BNPS_SKATOLE(new CleavageSiteFinderImpl(new CleavageSiteMatcher("W|X"))),
    GLU_C_BICARBONATE(new CleavageSiteFinderImpl(new CleavageSiteMatcher("E|[^PE]"))),
    GLU_C_PHOSPHATE(new CleavageSiteFinderImpl(new CleavageSiteMatcher("[DE]|[^PE]"))),
    PEPSINE_PH_1_3(new CleavageSiteFinderImpl(new CleavageSiteMatcher("ont{[^HKR]}ont{[^P]}[FL]|[X]oct{[^P]} or ont{[^HKR]}ont{[^P]}[^R]|[FL]oct{[^P]}"))),
    PEPSINE_PH_GT_2(new CleavageSiteFinderImpl(new CleavageSiteMatcher("ont{[^HKR]}ont{[^P]}[FLWY]|[X]oct{[^P]} or ont{[^HKR]}ont{[^P]}[^R]|[FLWY]oct{[^P]}"))),
    PROTEINASE_K(new CleavageSiteFinderImpl(new CleavageSiteMatcher("[AEFILTVWY]|X"))),
    THERMOLYSINE(new CleavageSiteFinderImpl(new CleavageSiteMatcher("[^DE]|[AFILMV]")))
    ;

    private final CleavageSiteFinder cleavageSiteFinder;

    Protease(CleavageSiteFinder finder) {

        Preconditions.checkNotNull(finder);

        cleavageSiteFinder = finder;
    }

    @Override
    public void find(SymbolSequence<AminoAcid> aaSeq, TIntList sites) {

        cleavageSiteFinder.find(aaSeq, sites);
    }

    public int countCleavageSites(SymbolSequence<AminoAcid> aaSeq) {

        return cleavageSiteFinder.countCleavageSites(aaSeq);
    }

    public CleavageSiteMatcher getCleavageSiteMatcher() {

        return cleavageSiteFinder.getCleavageSiteMatcher();
    }
}

