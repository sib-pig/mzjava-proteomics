package org.expasy.mzjava.proteomics.io.mol;

import org.expasy.mzjava.proteomics.mol.Protein;
import org.expasy.mzjava.core.io.IterativeReader;
import org.expasy.mzjava.proteomics.io.mol.fasta.FastaHeaderParser;
import org.openide.util.Lookup;

import java.io.*;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.List;

/**
 * A reader for FASTA formatted files.  Information from the FASTA header can be extracted
 * by adding an implementation of FastaHeaderParser. To make the header parser available automatically
 * register the FastaHeaderParser as a @ServiceProvider(service = FastaHeaderParser.class).  See
 * {@link org.expasy.mzjava.proteomics.io.mol.fasta.UniprotFastaHeaderParser} for an example.
 *
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class FastaProteinReader implements IterativeReader<Protein> {

    private final LineReader reader;

    private final List<FastaHeaderParser> headerParsers;
    private FastaHeaderParser currentHeaderParser;

    private static final int INITIAL_PROTEIN_CAPACITY = 100;
    //Using a soft reference because the builder has an AminoAcid array that only grows. If there is a Protein with
    //an exceptionally long sequence it is beneficial if the VM can garbage collect the builder if memory becomes scares.
    private final SoftReference<Protein.Builder> builderRef = new SoftReference<Protein.Builder>(new Protein.Builder(INITIAL_PROTEIN_CAPACITY));

    /**
     * Construct a FastaReader that reads from a Reader
     *
     * @param reader the reader to read from
     */
    public FastaProteinReader(Reader reader) {

        this.reader = new LineReader(reader);

        headerParsers = new ArrayList<FastaHeaderParser>();

        Lookup lookup = Lookup.getDefault();

        for (FastaHeaderParser headerParser : lookup.lookupAll(FastaHeaderParser.class)) {

            headerParsers.add(headerParser);
        }

        if (headerParsers.isEmpty()) {

            throw new IllegalStateException("No HeaderParser implementation class found: org.openide.util.Lookup was not able to work correctly " +
                    "(hint: check that Annotation Compiler Processing is enable)");
        } else {

            currentHeaderParser = headerParsers.get(0);
        }
    }

    /**
     * Constructs a FastaReader that reads from a file.
     *
     * @param file the file to read
     * @throws java.io.FileNotFoundException
     */
    public FastaProteinReader(File file) throws FileNotFoundException {

        this(new FileReader(file));
    }

    /**
     * Add a header parser
     *
     * @param parser the parser to add
     */
    public void addHeaderParser(FastaHeaderParser parser) {

        headerParsers.add(parser);
    }

    @Override
    public boolean hasNext() {

        try {

            return reader.peek() != null;
        } catch (IOException e) {

            throw new IllegalStateException(e);
        }
    }

    @Override
    public Protein next() throws IOException {

        String header = reader.readLine();

        if(header.length() < 1 || header.charAt(0) != '>')
            throw new IllegalStateException(header + " is not a valid fasta header");

        Protein.Builder builder = builderRef.get();

        if(builder == null)
            builder = new Protein.Builder(INITIAL_PROTEIN_CAPACITY);

        parseHeader(header, builder);
        for(String line = reader.peek(); line != null && line.length() > 0 && line.charAt(0) != '>'; line = reader.peek()) {

            builder.appendSequence(line);
            reader.readLine();
        }

        //Skip empty lines
        for(String peek = reader.peek(); peek != null && peek.length() == 0; peek = reader.peek()) {

            reader.readLine();
        }

        Protein protein = builder.build();
        builder.reset();
        return protein;
    }

    @Override
    public void close() throws IOException {

        reader.close();
    }

    protected void parseHeader(String header, Protein.Builder builder) {

        boolean parsed = false;
        if (currentHeaderParser != null) {

            parsed = currentHeaderParser.parseHeader(header, builder);
        }

        if (!parsed) {

            for (FastaHeaderParser parser : headerParsers) {

                if (parser.parseHeader(header, builder)) {

                    currentHeaderParser = parser;
                    break;
                }
            }
        }

        if(builder.getAccessionId() == null)
            builder.setAccessionId(header);
    }

    private static class LineReader {

        private final BufferedReader reader;
        private String last = null;

        private LineReader(Reader reader) {

            if (reader instanceof BufferedReader) {

                this.reader = (BufferedReader)reader;
            } else {

                this.reader = new BufferedReader(reader);
            }
        }

        public String readLine() throws IOException {

            String line;
            if (last == null) {

                line = reader.readLine();
            } else {

                line = last;
                last = null;
            }

            return line;
        }

        public String peek() throws IOException {

            last = readLine();
            return last;
        }

        public void close() throws IOException {

            reader.close();
        }
    }
}
