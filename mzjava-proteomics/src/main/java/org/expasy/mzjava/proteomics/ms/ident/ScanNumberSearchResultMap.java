/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.ms.ident;

import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;

import java.io.File;
import java.util.*;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class ScanNumberSearchResultMap extends SearchResultMap {

    private final Map<String, TIntObjectMap<List<PeptideMatch>>> fileMap = new HashMap<>();

    @Override
    public List<PeptideMatch> getResults(MsnSpectrum spectrum) {

        File source = new File(spectrum.getSpectrumSource());

        String  fileName = removeExtension(source.getName());

        TIntObjectMap<List<PeptideMatch>> resultsMap = fileMap.get(fileName);

        if (resultsMap == null) {

            return Collections.emptyList();
        } else {

            List<PeptideMatch> searchResults = resultsMap.get(spectrum.getScanNumbers().getFirst().getValue());
            return searchResults == null ? Collections.<PeptideMatch>emptyList() : searchResults;
        }
    }

    @Override
    protected void doPut(SpectrumIdentifier identifier, PeptideMatch searchResult) {

        String spectrum = identifier.getSpectrum();

        String file = spectrum.substring(0, spectrum.indexOf('.'));
        TIntObjectMap<List<PeptideMatch>> indexMap = fileMap.get(file);
        if(indexMap == null) {

            indexMap = new TIntObjectHashMap<>();
            fileMap.put(file, indexMap);
        }

        int scanNumber = identifier.getScanNumbers().getFirst().getValue();
        List<PeptideMatch> resultsForIndex = indexMap.get(scanNumber);
        if(resultsForIndex == null) {

            resultsForIndex = new ArrayList<>(2);
            indexMap.put(scanNumber, resultsForIndex);
        }
        resultsForIndex.add(searchResult);
    }
}
