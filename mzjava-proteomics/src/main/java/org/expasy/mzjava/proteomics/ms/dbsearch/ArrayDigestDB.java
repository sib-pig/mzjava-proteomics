/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.ms.dbsearch;

import org.expasy.mzjava.core.io.IterativeReader;
import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.core.mol.Weighable;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.mol.Protein;
import org.expasy.mzjava.proteomics.mol.digest.DigestionController;
import org.expasy.mzjava.proteomics.mol.digest.Protease;
import org.expasy.mzjava.proteomics.mol.digest.ProteinDigester;
import org.expasy.mzjava.core.ms.Tolerance;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A peptide digest db that stores peptide digests in ram and is backed by an array.
 *
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class ArrayDigestDB implements DigestDB {

    private final static Logger LOGGER = Logger.getLogger(ArrayDigestDB.class.getName());

    private final AmbiguousAminoAcidAction ambiguousAminoAcidAction;

    private final Digest[] digests;
    private final MassComparator massComparator = new MassComparator();
    private final WeighableKey weighableKey = new WeighableKey();

    /**
     * Construct a new ArrayDigestDB. Proteins are read from the <code>proteinRead</code> and digested using the
     * <code>proteinDigester</code>.
     *
     * @param proteinReader the source of the proteins
     * @param proteinDigester the protein digester
     * @param ambiguousAminoAcidAction the action to take if any paptides contain ambiguous amino acids such as (X, B, etc.)
     */
    public ArrayDigestDB(IterativeReader<Protein> proteinReader, ProteinDigester proteinDigester, AmbiguousAminoAcidAction ambiguousAminoAcidAction) {

        this.ambiguousAminoAcidAction = ambiguousAminoAcidAction;
        digests = populatePeptideDB(proteinReader, proteinDigester);

        Arrays.sort(digests, massComparator);
    }

    private Digest[] populatePeptideDB(final IterativeReader<Protein> proteinReader, final ProteinDigester digester) {

        final ProteinCallback cb = new ProteinCallback(digester, ambiguousAminoAcidAction);
        try {

            cb.read(proteinReader);
        } catch (IOException e) {

            throw new IllegalStateException(e);
        }

        return cb.getDigests();
    }

    @Override
    public Set<Digest> getPeptides(double targetMass, Tolerance tolerance) {

        Set<Digest> peptideSet = new HashSet<Digest>();

        getPeptides(targetMass, tolerance, peptideSet);

        return peptideSet;
    }

    @Override
    public void getPeptides(double targetMass, Tolerance tolerance, Collection<Digest> collection) {

        double minMass = tolerance.getMin(targetMass);
        double maxMass = tolerance.getMax(targetMass);

        weighableKey.setMolecularMass(minMass);
        int index = Arrays.binarySearch(digests, weighableKey, massComparator);
        if (index < 0) index = -1 * (index + 1);

        final int length = digests.length;
        do {

            Digest peptide = digests[index++];
            if (peptide.getMolecularMass() <= maxMass) {

                collection.add(peptide);
            } else {

                break;
            }

        } while (index < length);
    }

    /**
     * Return the number of digests that this DigestDB contains
     *
     * @return the number of digests that this DigestDB contains
     */
    public int size() {

        return digests.length;
    }

    /**
     * Builder
     */
    public static class Builder {

        private final IterativeReader<Protein> reader;
        private final Protease protease;
        private AmbiguousAminoAcidAction ambiguousAminoAcidAction = AmbiguousAminoAcidAction.THROW_EXCEPTION;
        private DigestionController digestionController;
        private int missedCleavages = 0;

        /**
         * Create a bulder that build a ArrayDigestDB that digests proteins from the <code>reader</code> using the
         * <code>protease</code> with <code>missedCleavages</code> missed cleavages.
         *
         * @param reader the reader
         * @param protease the protease
         * @param missedCleavages the number of mised cleavages
         */
        public Builder(IterativeReader<Protein> reader, Protease protease, int missedCleavages) {

            this.reader = reader;
            this.protease = protease;
            this.missedCleavages = missedCleavages;
        }

        /**
         * Build the ArrayDigestDB
         *
         * @return the new ArrayDigestDB
         */
        public ArrayDigestDB build() {

            return new ArrayDigestDB(reader, newProteinDigester(), ambiguousAminoAcidAction);
        }

        private ProteinDigester newProteinDigester() {

            ProteinDigester.Builder digesterBuilder = new ProteinDigester.Builder(protease)
                    .missedCleavageMax(missedCleavages);

            if (digestionController != null)
                digesterBuilder.controller(digestionController);

            return digesterBuilder.build();
        }

        /**
         * Set the controller for the digest.
         *
         * @param digestionController the controller for the digest
         * @return the builder
         */
        public Builder setDigestionController(DigestionController digestionController) {

            this.digestionController = digestionController;

            return this;
        }

        /**
         * Set what action the ArrayDigestDB is to take if ambiguous amino acids are encountered.
         *
         * @param ambiguousAminoAcidAction the action that is be take if the ArrayDigestDB encounters ambiguous amino acids
         * @return the builder
         */
        public Builder setAmbiguousAminoAcidAction(AmbiguousAminoAcidAction ambiguousAminoAcidAction) {

            this.ambiguousAminoAcidAction = ambiguousAminoAcidAction;
            return this;
        }
    }

    private static class MassComparator implements Comparator<Weighable>, Serializable {
        @Override
        public int compare(Weighable p1, Weighable p2) {

            return Double.compare(p1.getMolecularMass(), p2.getMolecularMass());
        }
    }

    private static final class WeighableKey implements Weighable {

        private double molecularMass = 0;

        public void setMolecularMass(double molecularMass) {

            this.molecularMass = molecularMass;
        }

        public double getMolecularMass() {

            return molecularMass;
        }
    }

    /**
     * This class is responsible for the digestion. Because we are expecting a large number
     * of digested peptides there are issues with using HashMap due to key collisions and the
     * map getting to large.
     * <p>
     * To get around this problem the HashMap is split into AminoAcid.values()^2 bins and the
     * first two amino acids are used to determine which HashMap bin a particular peptide
     * belongs to.
     */
    private static class ProteinCallback {

        private final ProteinDigester digester;
        private final AmbiguousAminoAcidAction ambiguousAminoAcidAction;
        private final Map<Peptide, Digest>[] digestMapArray;

        private final AminoAcid[] aminoAcids = AminoAcid.values();

        private int size = 0;

        private ProteinCallback(ProteinDigester digester, AmbiguousAminoAcidAction ambiguousAminoAcidAction) {

            this.digester = digester;
            this.ambiguousAminoAcidAction = ambiguousAminoAcidAction;

            //noinspection unchecked
            digestMapArray = new HashMap[(int)Math.pow(aminoAcids.length + 1, 2)];
            for (int i = 0, digestMapArraysLength = digestMapArray.length; i < digestMapArraysLength; i++) {

                digestMapArray[i] = new HashMap<Peptide, Digest>(1000);
            }
        }

        public void digest(Protein protein) {

            for (Peptide peptide : digester.digest(protein)) {

                // cannot calculate mw of ambiguous residues
                if (!peptide.hasAmbiguousAminoAcids()) {

                    int index;
                    if(peptide.size() == 1){

                        index = peptide.getSymbol(0).ordinal() + 1;
                    } else {

                        index = ((peptide.getSymbol(0).ordinal() + 1) * aminoAcids.length) + peptide.getSymbol(1).ordinal() + 1;
                    }

                    Map<Peptide, Digest> digestMap = digestMapArray[index];

                    Digest digest = digestMap.get(peptide);
                    if (digest == null) {

                        digest = new Digest(peptide);
                        size += 1;
                        digestMap.put(peptide, digest);
                    }

                    digest.addProteinId(protein.getAccessionId());
                } else {

                    String message = "Peptide " + peptide + " from protein " + protein.getAccessionId() + " contains the following ambiguous amino acids " + peptide.getAmbiguousAminoAcids();
                    if (ambiguousAminoAcidAction == AmbiguousAminoAcidAction.THROW_EXCEPTION) {

                        throw new IllegalStateException(message + ".\n\nTo change what action is taken on ambiguous amino acids call setAmbiguousAminoAcidAction(LOG_AND_SKIP) when building the ArrayPeptideDB.");
                    } else if (ambiguousAminoAcidAction == AmbiguousAminoAcidAction.LOG_AND_SKIP) {

                        LOGGER.log(Level.WARNING, message);
                    }
                }
            }
        }

        Digest[] getDigests() {

            int index = 0;
            Digest[] digests = new Digest[size];
            for (Map<Peptide, Digest> digestMap : digestMapArray) {

                for (Digest digest : digestMap.values()) {

                    digest.compact();
                    digests[index++] = digest;
                }
            }
            return digests;
        }

        public void read(IterativeReader<Protein> proteinReader) throws IOException {

            while (proteinReader.hasNext()) {

                digest(proteinReader.next());
            }
            proteinReader.close();
        }
    }
}
