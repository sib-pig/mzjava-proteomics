# README #

This repository contains classes for the proteomics modules of [MzJava](https://bitbucket.org/sib-pig/mzjava).

MzJava is an on-going open-source project providing classes for the analysis of mass spectrometry data and enabling rapid bioinformatics application development in the Java programming language.

To allow modular development the project has been split into separate repositories. The parent repository is [mzjava](https://bitbucket.org/sib-pig/mzjava) and there are separate repositories for [mzjava-core](https://bitbucket.org/sib-pig/mzjava-core), [mzjava-proteomics](https://bitbucket.org/sib-pig/mzjava-proteomics) and [mzjava-glycomics](https://bitbucket.org/sib-pig/mzjava-glycomics). 